package es.uji.apps.se.services;

import es.uji.apps.se.dao.MaterialDAO;
import es.uji.apps.se.dto.MaterialDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialService {

    private MaterialDAO materialDAO;

    @Autowired
    public MaterialService(MaterialDAO materialDAO) {

        this.materialDAO = materialDAO;
    }

    public List<MaterialDTO> getMateriales() {
        return materialDAO.getMateriales();
    }
}
