package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.TarjetaDeportivaTipoDAO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TarjetaDeportivaTipoService {
    @InjectParam
    private TarjetaDeportivaTipoDAO tarjetaDeportivaTipoDAO;

    public List<TarjetaDeportivaTipoDTO> getTarjetasDeportivasTiposActivas() {
        return tarjetaDeportivaTipoDAO.getTarjetasDeportivasTiposActivas();
    }

    @Role("ADMIN")
    public TarjetaDeportivaTipoDTO insert(TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO, Long connectedUserId) {
        return tarjetaDeportivaTipoDAO.insert(tarjetaDeportivaTipoDTO);
    }

    @Role("ADMIN")
    public void delete(Long tarjetaDeportivaTipoId, Long connectedUserId) {
        tarjetaDeportivaTipoDAO.delete(TarjetaDeportivaTipoDTO.class, tarjetaDeportivaTipoId);
    }

    @Role("ADMIN")
    public TarjetaDeportivaTipoDTO update(TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO, Long connectedUserId) {
        return tarjetaDeportivaTipoDAO.update(tarjetaDeportivaTipoDTO);
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportivaTipoById(Long tipoTarjetaId) {
        return tarjetaDeportivaTipoDAO.getTarjetaDeportivaTipoById(tipoTarjetaId);
    }

    public void reordenarItems(Long tarjetaDeportivaOrigenId,
                               Long tarjetaDeportivaDestinoId) {
        TarjetaDeportivaTipoDTO tarjetaDeportivaTipoOrigen = getTarjetaDeportivaTipoById(tarjetaDeportivaOrigenId);
        TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDestino = getTarjetaDeportivaTipoById(tarjetaDeportivaDestinoId);

        Long ordenDestino = ParamUtils.isNotNull(tarjetaDeportivaTipoOrigen.getOrdenVisualizacion()) ? tarjetaDeportivaTipoOrigen.getOrdenVisualizacion() : tarjetaDeportivaTipoDestino.getOrdenVisualizacion()+10L;
        Long ordenOrigen = ParamUtils.isNotNull(tarjetaDeportivaTipoDestino.getOrdenVisualizacion()) ? tarjetaDeportivaTipoDestino.getOrdenVisualizacion() : tarjetaDeportivaTipoOrigen.getOrdenVisualizacion()+10L;
        tarjetaDeportivaTipoDestino.setOrdenVisualizacion(ordenDestino);
        tarjetaDeportivaTipoOrigen.setOrdenVisualizacion(ordenOrigen);

        tarjetaDeportivaTipoDAO.update(tarjetaDeportivaTipoOrigen);
        tarjetaDeportivaTipoDAO.update(tarjetaDeportivaTipoDestino);

    }
}
