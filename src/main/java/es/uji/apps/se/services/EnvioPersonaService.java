package es.uji.apps.se.services;

import es.uji.apps.se.dao.EnvioPersonaDAO;
import es.uji.apps.se.model.EnvioPersona;
import es.uji.apps.se.model.Paginacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnvioPersonaService {

    private EnvioPersonaDAO envioPersonaDAO;

    @Autowired
    public EnvioPersonaService(EnvioPersonaDAO envioPersonaDAO) {
        this.envioPersonaDAO = envioPersonaDAO;
    }

    public List<EnvioPersona> getEnvioPersonasByEnvioId(Long envioId, Paginacion paginacion) {
        return envioPersonaDAO.getEnvioPersonasByEnvioId(envioId, paginacion);
    }
}
