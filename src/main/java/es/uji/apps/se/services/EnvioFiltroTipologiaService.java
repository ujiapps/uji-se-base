package es.uji.apps.se.services;

import es.uji.apps.se.dao.EnvioFiltroDAO;
import es.uji.apps.se.dto.EnvioDTO;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.model.EnvioFiltroTipologia;
import es.uji.apps.se.model.domains.TipoFiltroTipologiaEnvio;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnvioFiltroTipologiaService {

    private EnvioFiltroDAO envioFiltroDAO;

    @Autowired
    public EnvioFiltroTipologiaService(EnvioFiltroDAO EnvioFiltroDAO) {
        this.envioFiltroDAO = EnvioFiltroDAO;
    }

    public ResponseMessage addFiltroTipologiaAEnvio(EnvioFiltroTipologia envioFiltroTipologia) {

        envioFiltroDAO.deleteEnvioFiltroTipologiaByEnvioId(envioFiltroTipologia.getEnvio());

        EnvioDTO envio = new EnvioDTO();
        envio.setId(envioFiltroTipologia.getEnvio());

        if (ParamUtils.isNotNull(envioFiltroTipologia.getVinculos())) {
            for (Long vinculo : envioFiltroTipologia.getVinculos()) {
                if (ParamUtils.isNotNull(vinculo)) {
                    EnvioFiltroDTO envioFiltroTipologiaDTO = new EnvioFiltroDTO();
                    envioFiltroTipologiaDTO.setEnvio(envio);
                    envioFiltroTipologiaDTO.setKey(vinculo);
                    envioFiltroTipologiaDTO.setTipoFiltro(TipoFiltroTipologiaEnvio.VINCULOS.getId());
                    envioFiltroDAO.insert(envioFiltroTipologiaDTO);
                }
            }
        }

        if (ParamUtils.isNotNull(envioFiltroTipologia.isUsuarios())) {
            EnvioFiltroDTO envioFiltroTipologiaDTO = new EnvioFiltroDTO();
            envioFiltroTipologiaDTO.setEnvio(envio);
            if (envioFiltroTipologia.isUsuarios()){
                envioFiltroTipologiaDTO.setKey(1L);
            }
            else{
                envioFiltroTipologiaDTO.setKey(0L);
            }
            envioFiltroTipologiaDTO.setTipoFiltro(TipoFiltroTipologiaEnvio.USUARIOS.getId());
            envioFiltroDAO.insert(envioFiltroTipologiaDTO);
        }

        if (ParamUtils.isNotNull(envioFiltroTipologia.isMonitores())) {
            EnvioFiltroDTO envioFiltroTipologiaDTO = new EnvioFiltroDTO();
            envioFiltroTipologiaDTO.setEnvio(envio);
            if (envioFiltroTipologia.isMonitores()){
                envioFiltroTipologiaDTO.setKey(1L);
            }
            else{
                envioFiltroTipologiaDTO.setKey(0L);
            }
            envioFiltroTipologiaDTO.setTipoFiltro(TipoFiltroTipologiaEnvio.MONITORES.getId());
            envioFiltroDAO.insert(envioFiltroTipologiaDTO);
        }

        if (ParamUtils.isNotNull(envioFiltroTipologia.getElites())) {
            EnvioFiltroDTO envioFiltroTipologiaDTO = new EnvioFiltroDTO();
            envioFiltroTipologiaDTO.setEnvio(envio);
            if (envioFiltroTipologia.getElites().isElite()){
                envioFiltroTipologiaDTO.setKey(1L);

                if (ParamUtils.isNotNull(envioFiltroTipologia.getElites().getCursoAcademico())) {
                    String valueJson = "[{";
                    valueJson += "\"cursoAcademico\": " + envioFiltroTipologia.getElites().getCursoAcademico();
                    valueJson += "}]";
                    envioFiltroTipologiaDTO.setValue(valueJson);
                }

            }
            else{
                envioFiltroTipologiaDTO.setKey(0L);
                envioFiltroTipologiaDTO.setValue(null);
            }
            envioFiltroTipologiaDTO.setTipoFiltro(TipoFiltroTipologiaEnvio.ELITE.getId());
            envioFiltroDAO.insert(envioFiltroTipologiaDTO);
        }


        if (ParamUtils.isNotNull(envioFiltroTipologia.isMorosos())) {
            EnvioFiltroDTO envioFiltroTipologiaDTO = new EnvioFiltroDTO();
            envioFiltroTipologiaDTO.setEnvio(envio);
            if (envioFiltroTipologia.isMorosos()){
                envioFiltroTipologiaDTO.setKey(1L);
            }
            else{
                envioFiltroTipologiaDTO.setKey(0L);
            }
            envioFiltroTipologiaDTO.setTipoFiltro(TipoFiltroTipologiaEnvio.MOROSO.getId());
            envioFiltroDAO.insert(envioFiltroTipologiaDTO);
        }

        ResponseMessage res = new ResponseMessage();
        res.setSuccess(true);
        res.setMessage("Filtro añadido");
        return res;
    }

    public List<EnvioFiltroDTO> getEnvioFiltroTipologiasByEnvioId(Long envioId) {

        return envioFiltroDAO.getEnvioFiltrosTipologiaByEnvioId(envioId);
    }
}
