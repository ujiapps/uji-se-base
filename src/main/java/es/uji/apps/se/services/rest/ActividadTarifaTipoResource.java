package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.services.TipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("tarifatipo")
public class ActividadTarifaTipoResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(ActividadTarifaTipoResource.class);

    @InjectParam
    TipoService tipoService;

    @Path("{tarifatipoId}/tarifa")
    public ActividadTarifaResource getPlatformItem(
            @InjectParam ActividadTarifaResource actividadTarifaResource) {
        return actividadTarifaResource;
    }

    Long CLASIFICACION_ACTIVIDADES = 7329L;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTarifasTipos(@QueryParam("start") @DefaultValue("0") Long start,
                                           @QueryParam("limit") @DefaultValue("25") Long limit,
                                           @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<TipoDTO> tarifasTipoDTO = tipoService.getTiposByUso("TARIFA1", CLASIFICACION_ACTIVIDADES, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(tarifasTipoDTO)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getTarifasTipos", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertTarifaTipo(UIEntity entity){
        return UIEntity.toUI(tipoService.insertTipoTarifaActividades(entity));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTarifaTipo(UIEntity entity) {
        return UIEntity.toUI(tipoService.updateTipo(entity));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTarifaTipo(@PathParam("id") Long tarifaTipoId) {
        tipoService.deleteTipo(tarifaTipoId);
    }
}
