package es.uji.apps.se.services.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones.EquipacionDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones.EquipacionInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones.EquipacionUpdateException;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.EquipacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@Path("equipacion")
public class EquipacionResource extends CoreBaseService {

    @InjectParam
    EquipacionService equipacionService;

    @Path("/modelo")
    public EquipacionModeloResource getPlatformItem(
            @InjectParam EquipacionModeloResource equipacionModeloResource) {
        return equipacionModeloResource;
    }

    @Path("/tipo")
    public EquipacionTipoResource getPlatformItem(
            @InjectParam EquipacionTipoResource equipacionTipoResource) {
        return equipacionTipoResource;
    }

    @Path("/grupos")
    public EquipacionGruposResource getPlatformItem(
            @InjectParam EquipacionGruposResource equipacionesGruposResource) {
        return equipacionesGruposResource;
    }

    @Path("/generos")
    public EquipacionGeneroResource getPlatformItem(
            @InjectParam EquipacionGeneroResource equipacionGeneroResource) {
        return equipacionGeneroResource;
    }

    @Path("/pendientes")
    public EquipacionesPendientesResource getPlatformItem(
            @InjectParam EquipacionesPendientesResource equipacionesPendientesResource) {
        return equipacionesPendientesResource;
    }

    @Path("/tipos-movimiento")
    public EquipacionTipoMovimientoResource getPlatformItem(
            @InjectParam EquipacionTipoMovimientoResource equipacionTipoMovimientoResource) {
        return equipacionTipoMovimientoResource;
    }

    @Path("/tipos-agrupaciones")
    public EquipacionTiposAgrupacionesResource getPlatformItem(
            @InjectParam EquipacionTiposAgrupacionesResource equipacionTiposAgrupacionesResource) {
        return equipacionTiposAgrupacionesResource;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEquipaciones(@QueryParam("start") @DefaultValue("0") Long start,
                                           @QueryParam("limit") @DefaultValue("25") Long limit,
                                           @QueryParam("filter") @DefaultValue("[]") String filterJson,
                                           @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        List<Map<String, String>> filtros = null;
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            if (!filterJson.equals("[]")) {
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {});
            }

            List<UIEntity> equipaciones = UIEntity.toUI(equipacionService.getEquipaciones(paginacion, filtros));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(equipaciones);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEquipacion(UIEntity entity) throws EquipacionInsertException {
        equipacionService.addEquipacion(entity);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEquipacion(UIEntity entity) throws EquipacionUpdateException {
        equipacionService.updateEquipacion(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEquipacion(@PathParam("id") Long id) throws EquipacionDeleteException {
        equipacionService.deleteEquipacion(id);
        return Response.ok().build();
    }
}
