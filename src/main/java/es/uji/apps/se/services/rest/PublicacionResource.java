package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.*;
import es.uji.apps.se.services.rest.app.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

@Path("app")
public class PublicacionResource extends CoreBaseService {


    @InjectParam
    PersonaIbanService personaIbanService;

    @Path("tarjetas")
    public AppTarjetasResource getPlatformItem(
            @InjectParam AppTarjetasResource appTarjetasResource) {
        return appTarjetasResource;
    }

    @Path("actividades")
    public AppActividadesResource getPlatformItem(
            @InjectParam AppActividadesResource appActividadesResource) {
        return appActividadesResource;
    }

    @Path("reservas")
    public AppReservasResource getPlatformItem(
            @InjectParam AppReservasResource appReservasResource) {
        return appReservasResource;
    }

    @Path("reservas-historico")
    public AppReservasHistoricoResource getPlatformItem(
            @InjectParam AppReservasHistoricoResource appReservasHistoricoResource) {
        return appReservasHistoricoResource;
    }

    @Path("pagotarjeta")
    public AppPagoTarjetasResource getPlatformItem(
            @InjectParam AppPagoTarjetasResource appPagoTarjetasResource) {
        return appPagoTarjetasResource;
    }

    @Path("calendario")
    public AppCalendarioResource getPlatformItem(
            @InjectParam AppCalendarioResource appCalendarioResource) {
        return appCalendarioResource;
    }

    @GET
    public Response home() {
        return Response.seeOther(UriBuilder.fromPath("app/reservas").build()).build();
    }

    @POST
    @Path("nuevoiban")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage creaNuevaPersonaIban(@FormParam("cuentaBancariaNueva") String iban) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaIbanService.anyadirPersonaIban(connectedUserId, iban);
        return new ResponseMessage(true);
    }
}
