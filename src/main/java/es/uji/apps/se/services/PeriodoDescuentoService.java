package es.uji.apps.se.services;

import es.uji.apps.se.dao.PeriodoDescuentoDAO;
import es.uji.apps.se.dto.PeriodoDescuentoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeriodoDescuentoService {

    private PeriodoDescuentoDAO periodoDescuentoDAO;


    @Autowired
    public PeriodoDescuentoService(PeriodoDescuentoDAO periodoDescuentoDAO) {

        this.periodoDescuentoDAO = periodoDescuentoDAO;
    }

    public List<PeriodoDescuentoDTO> getDescuentosByPeriodo(Long periodo) {
        return periodoDescuentoDAO.getDescuentosByPeriodo(periodo);
    }

    public PeriodoDescuentoDTO addDescuento(PeriodoDescuentoDTO periodoDescuentoDTO) {
        return periodoDescuentoDAO.insert(periodoDescuentoDTO);
    }

    public void deleteDescuento(Long descuentoId) {
        periodoDescuentoDAO.delete(PeriodoDescuentoDTO.class, descuentoId);
    }

    public PeriodoDescuentoDTO updateDescuento(PeriodoDescuentoDTO periodoDescuentoDTO) {
        return periodoDescuentoDAO.update(periodoDescuentoDTO);
    }
}
