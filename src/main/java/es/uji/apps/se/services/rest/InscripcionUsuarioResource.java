package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.InscripcionDTO;
import es.uji.apps.se.dto.PeriodoDTO;
import es.uji.apps.se.dto.views.VWActividadesAsistenciaDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.GrupoInscripcion;
import es.uji.apps.se.model.InscripcionUsuario;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.PlazaInscripcion;
import es.uji.apps.se.services.CursoAcademicoService;
import es.uji.apps.se.services.InscripcionService;
import es.uji.apps.se.services.PeriodoService;
import es.uji.apps.se.services.PersonaSancionService;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class InscripcionUsuarioResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(InscripcionUsuarioResource.class);

    @InjectParam
    InscripcionService inscripcionService;
    @InjectParam
    PeriodoService periodoService;
    @InjectParam
    CursoAcademicoService cursoAcademicoService;
    @InjectParam
    PersonaSancionService personaSancionService;

    @GET
    @Path("curso/{cursoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getInscripcionesUsuarioByCurso(@PathParam("personaId") Long persona, @PathParam("cursoId") Long cursoAca) {
        ResponseMessage responseMessage = new ResponseMessage(false);
        try {
            List<InscripcionUsuario> inscripcionesUsuarios = inscripcionService.getInscripcionesUsuarioByCurso(persona, cursoAca);
            responseMessage.setData(UIEntity.toUI(inscripcionesUsuarios));
            responseMessage.setTotalCount(inscripcionesUsuarios.size());
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            throw new RuntimeException(e);
        }
        return responseMessage;
    }


    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInscripcionesUsuario(@PathParam("personaId") Long persona) {
        return UIEntity.toUI(inscripcionService.getInscripcionesUsuario(persona));
    }

    @GET
    @Path("{inscripcionId}/fechabaja")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFechaBaja(@PathParam("inscripcionId") Long inscripcionId) {
        return Response.ok(inscripcionService.getFechaBaja(inscripcionId)).build();
    }

    @GET
    @Path("{inscripcionId}/cadu")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCadu(@PathParam("inscripcionId") Long inscripcionId,
                            @PathParam("personaId") Long personaId) {
        Long cadu = inscripcionService.getCadu(inscripcionId, personaId);
        return Response.ok(cadu).build();
    }

    @GET
    @Path("{inscripcionId}/diasasis")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDiasAsis(@PathParam("inscripcionId") Long inscripcionId) {
        Long diasAsis = inscripcionService.getDiasAsis(inscripcionId);
        return Response.ok(diasAsis).build();
    }

    @GET
    @Path("{ofertaId}/diastotal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDiasTotal(@PathParam("ofertaId") Long ofertaId) {
        Long dias = inscripcionService.getDiasTotal(ofertaId);
        return Response.ok(dias).build();
    }

    @GET
    @Path("{ofertaId}/cadu/diastotal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCaduDiasTotal(@PathParam("ofertaId") Long ofertaId) {
        Long diasCadu = inscripcionService.getCaduDiasTotal(ofertaId);
        return Response.ok(diasCadu).build();
    }

    @GET
    @Path("{ofertaId}/{fecha}/{fechaBaja}/diasactiv")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDiasActiv(@PathParam("ofertaId") Long ofertaId,
                                 @PathParam("fecha") Long f1,
                                 @PathParam("fechaBaja") Long f2) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = formato.format(new Date(f1));
        String fechaBaja = f2 != 123 ? formato.format(new Date(f2)) : null;
        Long diasActiv = inscripcionService.getDiasActiv(ofertaId, fecha, fechaBaja);
        return Response.ok(diasActiv).build();
    }




    @GET
    @Path("combo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInscripcionesUsuarioCurso(@QueryParam("usuario") Long persona,
                                                       @QueryParam("curso") Long cursoAca) {
        return UIEntity.toUI(inscripcionService.getInscripcionesUsuarioCurso(persona, cursoAca));
    }

    @GET
    @Path("consulta")
    @Produces(MediaType.TEXT_HTML)
    public Template getPremiumAlta(@QueryParam("persona") Long persona) throws ParseException {
        Template template = AppInfo.buildPagina("se/base", "ca");
        template.put("contenido", "se/inscripciones_usuario/listado_inscripciones");

        List<PeriodoDTO> periodosConInscripcionUsuario = periodoService.getPeriodosConInscripcionUsuario(persona);
        template.put("listadoPeriodos", periodosConInscripcionUsuario);
        for (PeriodoDTO periodoDTO : periodosConInscripcionUsuario) {
            List<InscripcionDTO> inscripcionesPersona = inscripcionService.getInscripcionesUsuarioPorPeriodo(persona, periodoDTO.getId());
            template.put("listadoInscripciones", inscripcionesPersona);
        }

        return template;
    }

    @GET
    @Path("{inscripcionId}/gruposdisponibles/")
    public ResponseMessage getGruposDisponibles(@PathParam("inscripcionId") Long inscripcionId) {
        ResponseMessage responseMessage = new ResponseMessage(false);
        try {
            List<GrupoInscripcion> disponibles = inscripcionService.getGruposDisponibles(inscripcionId);
            responseMessage.setData(UIEntity.toUI(disponibles));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            log.error("getGruposDisponibles", e);
            throw new RuntimeException(e);
        }
        return responseMessage;
    }

    @GET
    @Path("bloqueada")
    @Produces(MediaType.APPLICATION_JSON)
    public Response bloqueaanyadirinscripcion(@PathParam("personaId") Long personaId) {
        Boolean bloqueaanyadirinscripcion = personaSancionService.tieneSancionActivaQueBloqueaInscripcion(personaId);
        return Response.ok(bloqueaanyadirinscripcion).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{inscripcionId}/modificagrupo/{ofertaId}")
    public Response modificaGrupoInscripcion(@PathParam("inscripcionId") Long inscripcionId,
                                             @PathParam("ofertaId" )Long ofertaId) {
        try {
            inscripcionService.modificaGrupoInscripcion(inscripcionId, ofertaId);
            return Response.ok().build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @GET
    @Path("actividadesasistencia/{inscripcionId}/{ofertaId}/{fecha}/{fechaBaja}")
    public ResponseMessage getActividadesAsistencia(@PathParam("inscripcionId") Long inscripcionId,
                                                    @PathParam("ofertaId") Long ofertaId,
                                                    @PathParam("fecha") Long f1,
                                                    @PathParam("fechaBaja") Long f2) {
        ResponseMessage responseMessage = new ResponseMessage(false);
        try {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            String fecha = formato.format(new Date(f1));
            String fechaBaja = f2 != 123 ? formato.format(new Date(f2)) : null;

            List<VWActividadesAsistenciaDTO> actividadesAsistencia = inscripcionService.getActividadesAsistencia(inscripcionId, ofertaId, fecha, fechaBaja);
            responseMessage.setData(UIEntity.toUI(actividadesAsistencia));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            log.error("getActividadesAsistencia", e);
            throw new RuntimeException(e);
        }
        return responseMessage;
    }

    @POST
    @Path("actividadesasistencia")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDeleteActividadAsistencia(UIEntity entity) {
        inscripcionService.addDeleteActividadAsistencia(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("oferta/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteInscripcion(UIEntity entity) throws CommonException {
        inscripcionService.deleteInscripcion(entity);
        return Response.ok().build();
    }

    @GET
    @Path("historico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getHistoricoInscripciones(@QueryParam("start") @DefaultValue("0") Long start,
                                                     @QueryParam("limit") @DefaultValue("25") Long limit,
                                                     @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                                     @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            Long cursoAcademico = cursoAcademicoService.getCursoAcademico(AccessManager.getConnectedUserId(request)).getId();
            responseMessage.setData(UIEntity.toUI(inscripcionService.getHistoricoInscripciones(paginacion, personaId, cursoAcademico)));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("{periodoId}/{actividadId}/plazas")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getPlazasInscripciones(@PathParam("personaId") Long personaId,
                                                  @PathParam("periodoId") Long periodoId,
                                                  @PathParam("actividadId") Long actividadId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<PlazaInscripcion> plazas = inscripcionService.getPlazasInscripciones(personaId, periodoId, actividadId);
            responseMessage.setTotalCount(plazas.size());
            responseMessage.setData(UIEntity.toUI(plazas));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error ("Error al recoger el número de plazas en las inscripciones", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
