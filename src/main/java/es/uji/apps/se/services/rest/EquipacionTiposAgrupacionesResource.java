package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones.TiposAgrupacionesDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones.TiposAgrupacionesInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones.TiposAgrupacionesUpdateException;
import es.uji.apps.se.services.TipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class EquipacionTiposAgrupacionesResource extends CoreBaseService {

    @InjectParam
    TipoService tipoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTiposAgrupaciones() {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            List<UIEntity> tiposAgrupaciones = UIEntity.toUI(tipoService.getTiposAgrupaciones());
            responseMessage.setTotalCount(tiposAgrupaciones.size());
            responseMessage.setData(tiposAgrupaciones);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEquipacionGrupo(UIEntity entity) throws TiposAgrupacionesInsertException {
        tipoService.addTiposAgrupaciones(entity);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEquipacionGrupo(UIEntity entity) throws TiposAgrupacionesUpdateException {
        tipoService.updateTiposAgrupaciones(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEquipacionGrupo(@PathParam("id") Long tipoId) throws TiposAgrupacionesDeleteException {
        tipoService.deleteTiposAgrupaciones(tipoId);
        return Response.ok().build();
    }
}