package es.uji.apps.se.services.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.BicicletaEnvioDTO;
import es.uji.apps.se.model.Envio;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.BicicletaEnvioService;
import es.uji.apps.se.services.EnvioService;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Path("bicicletaenvio")
public class BicicletaEnvioResource extends CoreBaseService {

    @InjectParam
    BicicletaEnvioService bicicletaEnvioService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEnvios() {
            return UIEntity.toUI(bicicletaEnvioService.getEnvios());
    }

    @GET
    @Path("{id}")
    public UIEntity getEnvioById(@PathParam("id") Long envioId) {
        return UIEntity.toUI(bicicletaEnvioService.getEnvioById(envioId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addEnvio(@FormParam("asunto") String asunto, @FormParam("cuerpo") String cuerpo, @FormParam("tipoFecha") Long tipoFecha, @FormParam("dias") Long dias) throws ParseException {

        return UIEntity.toUI(bicicletaEnvioService.addEnvio(asunto, cuerpo, tipoFecha, dias));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("{id}")
    public UIEntity updateEnvio(@PathParam("id") Long envioId, @FormParam("asunto") String asunto, @FormParam("cuerpo") String cuerpo, @FormParam("tipoFecha") Long tipoFecha, @FormParam("dias") Long dias) throws ParseException {

        return UIEntity.toUI(bicicletaEnvioService.updateEnvio(envioId, asunto, cuerpo, tipoFecha, dias));

    }

    @DELETE
    @Path("{id}")
    public void deleteEnvio(@PathParam("id") Long envioId) {
        bicicletaEnvioService.deleteEnvio(envioId);
    }
}
