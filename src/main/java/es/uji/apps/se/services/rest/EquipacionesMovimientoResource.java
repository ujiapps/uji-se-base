package es.uji.apps.se.services.rest;

import au.com.bytecode.opencsv.CSVWriter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosDeleteException;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosInsertException;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosUpdateException;
import es.uji.apps.se.model.EquipacionesMovimientoVW;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.EquipacionesMovimientoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Path("equipaciones-movimientos")
public class EquipacionesMovimientoResource extends CoreBaseService {

    public final static Logger LOG = LoggerFactory.getLogger(EquipacionesMovimientoResource.class);

    @InjectParam
    EquipacionesMovimientoService equipacionMovimientoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEquipacionesMovimientos(@QueryParam("start") @DefaultValue("0") Long start,
                                          @QueryParam("limit") @DefaultValue("25") Long limit,
                                          @QueryParam("filter") @DefaultValue("[]") String filterJson,
                                          @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        List<Map<String, String>> filtros = null;
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            if (!filterJson.equals("[]")) {
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {});
            }

            List<EquipacionesMovimientoVW> equipacionesMovimientos = equipacionMovimientoService.getEquipacionesMovimientos(paginacion, filtros);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(equipacionesMovimientos));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            LOG.error("Error en l'obtenció dels moviments", e);
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEquipacionMovimiento(UIEntity entity) throws EquipacionesMovimientosInsertException {
        equipacionMovimientoService.addEquipacionMovimiento(entity);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEquipacionMovimiento(UIEntity entity) throws EquipacionesMovimientosUpdateException {
        equipacionMovimientoService.updateEquipacionMovimiento(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEquipacionMovimiento(@PathParam("id") Long id) throws EquipacionesMovimientosDeleteException {
        equipacionMovimientoService.deleteEquipacionMovimiento(id);
        return Response.ok().build();
    }

    @GET
    @Path("descargar/csv")
    @Produces("application/vnd.ms-excel")
    public Response descargarEquipacionesMovimientosToCSV(@QueryParam("filterJson") String filterJson) throws CommonException {
        try {
            List<Map<String, String>> filtros = null;
            if (!filterJson.equals("[]")) {
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {
                });
            }
            Response.ResponseBuilder res = Response.ok();
            List<EquipacionesMovimientoVW> equipacionesMovimientoVW = equipacionMovimientoService.getEquipacionesMovimientos(null, filtros);

            res.header("Content-Disposition", "attachment; filename = equipacionesMovimientos.csv");
            res.entity(entityToCSV(equipacionesMovimientoVW));
            res.type(MediaType.TEXT_PLAIN);

            return res.build();
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    private String entityToCSV(List<EquipacionesMovimientoVW> equipacionesMovimientoVW) throws CommonException {
        final char DELIMETER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer, DELIMETER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

        try {

            List<String[]> records = new ArrayList<>();
            List<String> record = new ArrayList<>(Arrays.asList("Equipació", "Model", "Gènere", "Talla", "Dorsal",
                    "Tipus", "Identificació", "Quantitat", "Data", "Personal", "Observacions"));

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (EquipacionesMovimientoVW vw : equipacionesMovimientoVW) {
                List<String> recordMov = new ArrayList<>(
                        Arrays.asList(vw.getEquipacionTipoNombre() != null ? vw.getEquipacionTipoNombre() : "",
                                vw.getEquipacionModeloNombre() != null ? vw.getEquipacionModeloNombre() : "",
                                vw.getEquipacionGeneroNombre() != null ? vw.getEquipacionGeneroNombre() : "",
                                vw.getTalla() != null ? vw.getTalla() : "",
                                vw.getDorsal() != null ? vw.getDorsal().toString() : "",
                                vw.getMovimiento() != null ? vw.getMovimiento() : "",
                                vw.getIdentificacion() != null ? vw.getIdentificacion() : "",
                                vw.getCantidad() != null ? vw.getCantidad().toString() : "",
                                vw.getFecha() != null ? vw.getFecha().toString() : "",
                                vw.getPersonaNombre() != null ? vw.getPersonaNombre() : "",
                                vw.getObservaciones() != null ? vw.getObservaciones() : ""));
                String[] recordArrayMov = new String[recordMov.size()];
                recordMov.toArray(recordArrayMov);
                records.add(recordArrayMov);
            }
            csvWriter.writeAll(records);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
        return writer.toString();
    }
}
