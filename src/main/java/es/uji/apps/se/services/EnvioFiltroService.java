package es.uji.apps.se.services;

import es.uji.apps.se.dao.EnvioFiltroDAO;
import es.uji.apps.se.dto.EnvioDTO;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.model.*;
import es.uji.apps.se.model.domains.TipoFiltroEnvio;
import es.uji.apps.se.model.filtros.FiltroActividades;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnvioFiltroService {

    private EnvioFiltroDAO envioFiltroDAO;
    private AlquilerService alquilerService;
    private ActividadService actividadService;

    @Autowired
    public EnvioFiltroService(EnvioFiltroDAO envioFiltroDAO, AlquilerService alquilerService, ActividadService actividadService) {
        this.envioFiltroDAO = envioFiltroDAO;
        this.alquilerService = alquilerService;
        this.actividadService = actividadService;
    }

    public ResponseMessage addFiltroAEnvio(EnvioFiltro envioFiltro) {

        envioFiltroDAO.deleteEnvioFiltroByEnvioId(envioFiltro.getEnvio());

        EnvioDTO envio = new EnvioDTO();
        envio.setId(envioFiltro.getEnvio());

        if (ParamUtils.isNotNull(envioFiltro.getTarjetas())) {
            for (Long tarjetaTipo : envioFiltro.getTarjetas()) {
                if (ParamUtils.isNotNull(tarjetaTipo)) {
                    EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
                    envioFiltroDTO.setEnvio(envio);
                    envioFiltroDTO.setKey(tarjetaTipo);
                    envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.TARJETA.getId());
                    envioFiltroDAO.insert(envioFiltroDTO);
                }
            }
        }
        if (ParamUtils.isNotNull(envioFiltro.getBonos())) {
            for (Long carnetBonoTipo : envioFiltro.getBonos()) {
                if (ParamUtils.isNotNull(carnetBonoTipo)) {
                    EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
                    envioFiltroDTO.setEnvio(envio);
                    envioFiltroDTO.setKey(carnetBonoTipo);
                    envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.BONOS.getId());
                    envioFiltroDAO.insert(envioFiltroDTO);
                }
            }
        }

        // Filtro fechas clases
        if (ParamUtils.isNotNull(envioFiltro.getFechaInicioClases())) {
            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envio);
            envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.CLASES.getId());
            envioFiltroDTO.setKey(-1L);
            envioFiltroDTO.setFechaInicio(envioFiltro.getFechaInicioClases());
            if (ParamUtils.isNotNull(envioFiltro.getFechaFinClases()))
                envioFiltroDTO.setFechaFin(envioFiltro.getFechaFinClases());
            else
                envioFiltroDTO.setFechaFin(envioFiltro.getFechaInicioClases());
            envioFiltroDTO.setHoraInicio(envioFiltro.getHoraInicioClases());
            envioFiltroDTO.setHoraFin(envioFiltro.getHoraFinClases());

            String valueJson = "[{";
            valueJson += "\"asisten\": " + envioFiltro.isAsisten() + ", ";
            valueJson += " \"noAsisten\": " + envioFiltro.isNoAsisten();
            valueJson += "}]";
            envioFiltroDTO.setValue(valueJson);

            envioFiltroDAO.insert(envioFiltroDTO);
        }

//        if (ParamUtils.isNotNull(envioFiltro.getFiltroClasesInstalaciones())) {
        for (FiltroClasesInstalacion filtroClasesInstalacion : envioFiltro.getFiltroClasesInstalaciones()) {
            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envio);
            envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.CLASESINSTALACIONES.getId());
            envioFiltroDTO.setKey(filtroClasesInstalacion.getInstalacion());
            envioFiltroDTO.setFechaInicio(filtroClasesInstalacion.getFechaInicioInstalacion());
            if (ParamUtils.isNotNull(filtroClasesInstalacion.getFechaFinInstalacion()))
                envioFiltroDTO.setFechaFin(filtroClasesInstalacion.getFechaFinInstalacion());
            else
                envioFiltroDTO.setFechaFin(filtroClasesInstalacion.getFechaInicioInstalacion());
            envioFiltroDTO.setHoraInicio(filtroClasesInstalacion.getHoraInicioInstalacion());
            envioFiltroDTO.setHoraFin(filtroClasesInstalacion.getHoraFinInstalacion());

            String valueJson = "[{";
            valueJson += "\"asisten\": " + filtroClasesInstalacion.isAsisten() + ", ";
            valueJson += " \"noAsisten\": " + filtroClasesInstalacion.isNoAsisten();
            valueJson += "}]";
            envioFiltroDTO.setValue(valueJson);

            envioFiltroDAO.insert(envioFiltroDTO);
        }
//        }

//        if (ParamUtils.isNotNull(envioFiltro.getFiltroClasesClases())) {
        for (FiltroClasesClases filtroClasesClases : envioFiltro.getFiltroClasesClases()) {
            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envio);
            envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.CLASESCLASES.getId());
            envioFiltroDTO.setKey(filtroClasesClases.getClase());
            envioFiltroDTO.setFechaInicio(filtroClasesClases.getFechaInicioFiltroClases());
            if (ParamUtils.isNotNull(filtroClasesClases.getFechaFinFiltroClases()))
                envioFiltroDTO.setFechaFin(filtroClasesClases.getFechaFinFiltroClases());
            else
                envioFiltroDTO.setFechaFin(filtroClasesClases.getFechaInicioFiltroClases());
            envioFiltroDTO.setHoraInicio(filtroClasesClases.getHoraInicioFiltroClases());
            envioFiltroDTO.setHoraFin(filtroClasesClases.getHoraFinFiltroClases());

            String valueJson = "[{";
            valueJson += "\"asisten\": " + filtroClasesClases.isAsisten() + ", ";
            valueJson += " \"noAsisten\": " + filtroClasesClases.isNoAsisten();
            valueJson += "}]";
            envioFiltroDTO.setValue(valueJson);

            envioFiltroDAO.insert(envioFiltroDTO);
        }
//        }

        // Filtro personas
        for (FiltroPersonas filtroPersonas : envioFiltro.getFiltroPersonas()) {
            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envio);
            envioFiltroDTO.setValue(filtroPersonas.getEmail());
            envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.PERSONAS.getId());
            envioFiltroDAO.insert(envioFiltroDTO);

        }

        for (FiltroEnvioActividades filtroEnvioActividades : envioFiltro.getFiltroActividades()) {
            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envio);
            envioFiltroDTO.setKey(filtroEnvioActividades.getActividad());
//            if (ParamUtils.isNotNull(filtroEnvioActividades.getGrupo()))
            String valueJson = "[{";
            valueJson += "\"grupo\": " + filtroEnvioActividades.getGrupo().toString() + ", ";
            valueJson += " \"apto\": " + filtroEnvioActividades.isApto() + ", ";
            valueJson += " \"noApto\": " + filtroEnvioActividades.isNoApto() + ", ";
            valueJson += " \"espera\": " + filtroEnvioActividades.isEspera() + ", ";
            valueJson += " \"inscritos\": " + filtroEnvioActividades.isInscritos();
            valueJson += "}]";
            envioFiltroDTO.setValue(valueJson);

            envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.ACTIVIDADES.getId());
            envioFiltroDAO.insert(envioFiltroDTO);
        }

        for (FiltroAlquileres filtroAlquileres : envioFiltro.getFiltroAlquileres()) {
            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envio);
            envioFiltroDTO.setKey(filtroAlquileres.getAlquiler());
            envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.ALQUILER.getId());
            envioFiltroDAO.insert(envioFiltroDTO);

        }

        if (ParamUtils.isNotNull(envioFiltro.getFiltroBicicletas())) {

            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envio);
            String valueJson = "[{";
            valueJson += "\"reserva\": " + envioFiltro.getFiltroBicicletas().isReserva() + ", ";
            valueJson += " \"espera\": " + envioFiltro.getFiltroBicicletas().isEspera() + ", ";
            valueJson += " \"devolucion\": " + envioFiltro.getFiltroBicicletas().isDevolucion();
            valueJson += "}]";
            envioFiltroDTO.setValue(valueJson);

            envioFiltroDTO.setTipoFiltro(TipoFiltroEnvio.BICICLETA.getId());
            envioFiltroDAO.insert(envioFiltroDTO);
        }

        ResponseMessage res = new ResponseMessage();
        res.setSuccess(true);
        res.setMessage("Filtro añadido");
        return res;
    }

    public List<EnvioFiltroDTO> getEnvioFiltrosByEnvioId(Long envioId) {

        return envioFiltroDAO.getEnvioFiltrosByEnvioId(envioId);
    }

    public ResponseMessage addFiltroTodosAEnvio(Long envioId) {

        List<EnvioFiltroDTO> envioFiltrosDTO = getEnvioFiltrosByEnvioId(envioId).stream().filter(envio -> envio.getTipoFiltro().equals(11L)).collect(Collectors.toList());
        if (envioFiltrosDTO.isEmpty()) {
            EnvioDTO envioDto = new EnvioDTO();
            envioDto.setId(envioId);

            EnvioFiltroDTO envioFiltroDTO = new EnvioFiltroDTO();
            envioFiltroDTO.setEnvio(envioDto);
            envioFiltroDTO.setTipoFiltro(11L);
            envioFiltroDAO.insert(envioFiltroDTO);
        } else {
            envioFiltroDAO.deleteEnvioFiltroTodos(envioId);
        }


        ResponseMessage res = new ResponseMessage();
        res.setSuccess(true);
        res.setMessage("Filtro añadido");
        return res;

    }

    public List<UIEntity> muestraAlquileresTodos(Long connectUserId) {

        List<Alquiler> alquileres = alquilerService.getAlquileres(connectUserId);

        return alquileres.stream().map(al -> {
            UIEntity entity = new UIEntity();
            entity.put("alquiler", al.getId());
            return entity;
        }).collect(Collectors.toList());
    }

    public List<UIEntity> muestraActividadesFiltro(Long periodoId, Long clasificacionId, Long actividadId, Long grupoId, Boolean apto, Boolean noApto, Boolean inscritos, Boolean espera) {
        FiltroActividades filtroActividades = new FiltroActividades(periodoId, clasificacionId, actividadId, grupoId);
        List<Oferta> actividades = actividadService.getActividadesEnvio(filtroActividades);
        return actividades.stream().map(oferta -> {
            UIEntity entity = new UIEntity();
            entity.put("actividad", oferta.getActividad());
            entity.put("grupo", oferta.getId());
            entity.put("apto", apto);
            entity.put("noApto", noApto);
            entity.put("inscritos", inscritos);
            entity.put("espera", espera);
            return entity;
        }).collect(Collectors.toList());
    }

    public EnvioFiltroDTO addEnvioFiltro(EnvioFiltroDTO envioFiltroDTO) {
        return envioFiltroDAO.insert(envioFiltroDTO);
    }
}
