package es.uji.apps.se.services;

import es.uji.apps.se.dao.AdjuntoDAO;
import es.uji.apps.se.dao.EnvioDAO;
import es.uji.apps.se.dto.AdjuntoDTO;
import es.uji.apps.se.dto.EnvioDTO;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.model.Adjunto;
import es.uji.apps.se.model.Envio;
import es.uji.apps.se.model.EnvioFiltro;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnvioService {

    private EnvioDAO envioDAO;
    private AdjuntoService adjuntoService;
    private AdjuntoDAO adjuntoDAO;
    private EnvioFiltroService envioFiltroService;
    private EnvioFiltroTipologiaService envioFiltroTipologiaService;

    @Autowired
    public EnvioService(EnvioDAO envioDAO, AdjuntoService adjuntoService, AdjuntoDAO adjuntoDAO, EnvioFiltroService envioFiltroService, EnvioFiltroTipologiaService envioFiltroTipologiaService) {
        this.envioDAO = envioDAO;
        this.adjuntoService = adjuntoService;
        this.adjuntoDAO = adjuntoDAO;
        this.envioFiltroService = envioFiltroService;
        this.envioFiltroTipologiaService = envioFiltroTipologiaService;
    }

    public List<Envio> getEnvios(Long connectedUserId, Paginacion paginacion) {
        return envioDAO.getEnvios(connectedUserId, paginacion);
    }

    public ResponseMessage deleteEnvio(Long envioId) {

        ResponseMessage r = new ResponseMessage();

        EnvioDTO envio = getEnvioById(envioId);
        if (!ParamUtils.isNotNull(envio.getFechaEnvio())) {
            envioDAO.delete(EnvioDTO.class, envioId);
            r.setSuccess(true);
            r.setMessage("Esborrat realitzat");

            return r;
        }

        r.setSuccess(false);
        r.setMessage("L'enviament ja ha sigut realitzat i no es pot eliminar");

        return r;
    }

    public EnvioDTO getEnvioById(Long envioId) {
        return envioDAO.getEnvioById(envioId);
    }

    public EnvioDTO addEnvio(Long personaId, String nombre, String cuerpo, Date fechaEnvio) {

        EnvioDTO envio = new EnvioDTO();

        envio.setCuerpo(cuerpo);
        envio.setNombre(nombre);
        envio.setFecha(new Date());
        envio.setEnviado(Boolean.FALSE);
        envio.setFechaEnvio(fechaEnvio);

        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(personaId);
        envio.setPersonaCrea(persona);

        return envioDAO.insert(envio);
    }

    public EnvioDTO updateEnvio(Long personaId, Long envioId, String nombre, String cuerpo, Date fechaEnvio) {

        EnvioDTO envio = getEnvioById(envioId);
        envio.setNombre(nombre);
        envio.setCuerpo(cuerpo);
        envio.setFechaEnvio(fechaEnvio);

        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(personaId);
        envio.setPersonaCrea(persona);

        return envioDAO.update(envio);
    }

    public void envioPrueba(Long envioId, String correo) throws MessageNotSentException {
        if (ParamUtils.isNotNull(correo)) {

            EnvioDTO envio = getEnvioById(envioId);

            MailMessage mensaje = new MailMessage("SE");

            mensaje.setTitle(envio.getNombre());
            mensaje.setContentType("text/html; charset=UTF-8");

            String cuerpo = anyadirAdjuntos(envio);
            mensaje.setContent(cuerpo);

            mensaje.setReplyTo("noreply@uji.es");
            mensaje.setSender("Servei d'esports <noreply@uji.es>");
            mensaje.addToRecipient(correo);

            MessagingClient cliente = new MessagingClient();
            cliente.send(mensaje);
        }
    }

    private String anyadirAdjuntos(EnvioDTO envio) {

        List<Adjunto> adjuntos = adjuntoService.getAdjuntosByEnvioId(envio.getId());
        String cuerpo = envio.getCuerpo();

        if (!adjuntos.isEmpty()) {
            cuerpo = cuerpo + "<br /><br /><br /> --------- Llistat d'adjunts al correu ---------- <br />";
        }

        for (Adjunto adjunto : adjuntos) {
            cuerpo = cuerpo + "<br />" + "<a href='https://ujiapps.uji.es/ade/rest/storage/" + adjunto.getReferencia() + "'>" + adjunto.getNombre() + "</a>";
        }

        return cuerpo;
    }

    public ResponseMessage enviarEnvio(Long personaId, Long envioId) {
        EnvioDTO envio = getEnvioById(envioId);
        envio.setFechaEnvio(new Date());

        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(personaId);
        envio.setPersonaCrea(persona);

        envioDAO.update(envio);

        ResponseMessage res = new ResponseMessage();
        res.setSuccess(true);
        res.setMessage("Enviament realitzat");
        return res;
    }

    public List<Envio> getEnviosByBusqueda(Long perfil, String asunto, Long persona, Long connectedUserId, Paginacion paginacion) {
        return envioDAO.getEnviosByBusqueda(perfil, asunto, persona, connectedUserId, paginacion);
    }

    public ResponseMessage duplicaEnvio(Long connectedUserId, Long envioId) {
        EnvioDTO envio = getEnvioById(envioId);
        EnvioDTO dupli = new EnvioDTO();
        dupli.setFecha(new Date());
        dupli.setCuerpo(envio.getCuerpo());
        dupli.setNombre("ENVIO DUPLICADO - " + envio.getNombre());

        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(connectedUserId);
        dupli.setPersonaCrea(persona);

        dupli.setOrigen(envio.getOrigen());
        dupli.setReferencia(envio.getReferencia());
        dupli.setEnviado(Boolean.FALSE);

        envioDAO.insert(dupli);

        List<Adjunto> adjuntos = adjuntoService.getAdjuntosByEnvioId(envioId);
        adjuntos.stream().forEach(adjunto -> {
            AdjuntoDTO dupliAdjunto = new AdjuntoDTO();
            dupliAdjunto.setNombre(adjunto.getNombre());
            dupliAdjunto.setReferencia(adjunto.getReferencia());
            dupliAdjunto.setEnvioDTO(dupli);
            adjuntoDAO.insert(dupliAdjunto);

        });

        List<EnvioFiltroDTO> envioFiltros = envioFiltroService.getEnvioFiltrosByEnvioId(envioId);
        envioFiltros.stream().forEach(filtro -> {
            EnvioFiltroDTO dupliEnvioFiltro = new EnvioFiltroDTO();
            dupliEnvioFiltro.setEnvio(dupli);
            dupliEnvioFiltro.setKey(filtro.getKey());
            dupliEnvioFiltro.setTipoFiltro(filtro.getTipoFiltro());
            dupliEnvioFiltro.setValue(filtro.getValue());
            dupliEnvioFiltro.setHoraFin(filtro.getHoraFin());
            dupliEnvioFiltro.setHoraInicio(filtro.getHoraInicio());
            dupliEnvioFiltro.setFechaFin(filtro.getFechaFin());
            dupliEnvioFiltro.setFechaInicio(filtro.getFechaInicio());

            envioFiltroService.addEnvioFiltro(dupliEnvioFiltro);

        });

        List<EnvioFiltroDTO> envioFiltrosTipologia = envioFiltroTipologiaService.getEnvioFiltroTipologiasByEnvioId(envioId);
        envioFiltrosTipologia.stream().forEach(filtro -> {
            EnvioFiltroDTO dupliEnvioFiltro = new EnvioFiltroDTO();
            dupliEnvioFiltro.setEnvio(dupli);
            dupliEnvioFiltro.setKey(filtro.getKey());
            dupliEnvioFiltro.setTipoFiltro(filtro.getTipoFiltro());
            dupliEnvioFiltro.setValue(filtro.getValue());
            dupliEnvioFiltro.setHoraFin(filtro.getHoraFin());
            dupliEnvioFiltro.setHoraInicio(filtro.getHoraInicio());
            dupliEnvioFiltro.setFechaFin(filtro.getFechaFin());
            dupliEnvioFiltro.setFechaInicio(filtro.getFechaInicio());

            envioFiltroService.addEnvioFiltro(dupliEnvioFiltro);

        });


        ResponseMessage res = new ResponseMessage();
        res.setSuccess(true);
        res.setMessage("Enviament duplicat");
        return res;
    }
}
