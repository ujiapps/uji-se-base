package es.uji.apps.se.services;

import es.uji.apps.se.dao.AlumnoAsignaturaDAO;
import es.uji.apps.se.dto.AlumnoAsignaturaDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlumnoAsignaturaService {

    private AlumnoAsignaturaDAO alumnoAsignaturaDAO;

    @Autowired
    public AlumnoAsignaturaService(AlumnoAsignaturaDAO alumnoAsignaturaDAO) {
        this.alumnoAsignaturaDAO = alumnoAsignaturaDAO;
    }

    public List<AlumnoAsignaturaDTO> getAsignaturasByCursoAndPersonaId(Long curso, Long connectedUserId) {
        return alumnoAsignaturaDAO.getAsignaturasByCursoAndPersonaId(curso, connectedUserId);
    }

    public List<PersonaUjiDTO> getProfesorAsignaturaByAsignatura(String asignatura, Long curso) {
        return alumnoAsignaturaDAO.getProfesorAsignaturaByAsignatura(asignatura, curso);
    }
}
