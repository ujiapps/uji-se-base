package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.indicadoresSatisfaccion.IndicadoresSatisfaccionDeleteException;
import es.uji.apps.se.exceptions.indicadoresSatisfaccion.IndicadoresSatisfaccionInsertException;
import es.uji.apps.se.exceptions.indicadoresSatisfaccion.IndicadoresSatisfaccionUpdateException;
import es.uji.apps.se.model.IndicadorSatisfaccion;
import es.uji.apps.se.services.ClaseService;
import es.uji.apps.se.services.IndicadorActividadCSVService;
import es.uji.apps.se.services.IndicadorSatisfaccionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class IndicadorSatisfaccionResource extends CoreBaseService {

    @InjectParam
    IndicadorSatisfaccionService indicadorSatisfaccionService;
    @InjectParam
    IndicadorActividadCSVService indicadorActividadCSVService;
    @InjectParam
    ClaseService claseService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getIndicadoresSatisfaccion(@QueryParam("cursoId") Long cursoId) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            List<IndicadorSatisfaccion> indicadoresSatisfaccionDTO = indicadorSatisfaccionService.getIndicadoresSatisfaccion(cursoId);
            responseMessage.setTotalCount(indicadoresSatisfaccionDTO.size());
            responseMessage.setData(UIEntity.toUI(indicadoresSatisfaccionDTO));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("export")
    @Produces("application/vnd.ms-excel")
    public Response generarExcelIndicadoresSatisfaccion(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("cursoId") Long cursoId) throws IOException {
        List<IndicadorSatisfaccion> indicadoresSatisfaccion = indicadorSatisfaccionService.getIndicadoresSatisfaccion(cursoId);
        ByteArrayOutputStream ostream = indicadorActividadCSVService.generateIndicadoresSatisfaccionExcel(indicadoresSatisfaccion, claseService.getRecuentoClasesDirigidasActivas(), claseService.getRecuentoClasesDirigidas(cursoId));
        return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = IndicadoresSatisfaccion" + cursoId + ".xls").build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addIndicadorSatisfaccion(UIEntity entity) throws IndicadoresSatisfaccionInsertException {
        IndicadorSatisfaccion indicadorSatisfaccion = entity.toModel(IndicadorSatisfaccion.class);
        indicadorSatisfaccionService.addIndicadorSatisfaccion(indicadorSatisfaccion);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateIndicadorSatisfaccion(UIEntity entity) throws IndicadoresSatisfaccionUpdateException {
        IndicadorSatisfaccion indicadorSatisfaccion = entity.toModel(IndicadorSatisfaccion.class);
        indicadorSatisfaccionService.updateIndicadorSatisfaccion(indicadorSatisfaccion);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteIndicadorSatisfaccion(UIEntity entity) throws IndicadoresSatisfaccionDeleteException {
        IndicadorSatisfaccion indicadorSatisfaccion = entity.toModel(IndicadorSatisfaccion.class);
        indicadorSatisfaccionService.deleteIndicadorSatisfaccion(indicadorSatisfaccion);
        return Response.ok().build();
    }
}
