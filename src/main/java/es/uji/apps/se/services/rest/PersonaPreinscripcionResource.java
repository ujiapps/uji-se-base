package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.services.InscripcionService;
import es.uji.apps.se.services.PersonaPreinscripcionService;
import es.uji.apps.se.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

public class PersonaPreinscripcionResource extends CoreBaseService {

    @InjectParam
    PersonaPreinscripcionService personaPreinscripcionService;
    @InjectParam
    PersonaService personaService;

    @DELETE
    @Path("{preinscripcionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletePreinscripcion(@PathParam("personaId") Long personaId,
                                         @PathParam("preinscripcionId") Long preinscripcionId) throws CommonException {
        personaPreinscripcionService.deletePreinscripcion(preinscripcionId);
        return Response.ok().build();
    }


    @POST
    @Path("{grupoId}")
    public Response addPreinscripcion(@PathParam("personaId") Long personaId,
                                      @PathParam("grupoId") Long grupoId,
                                      UIEntity entity) throws CommonException {
        if (personaPreinscripcionService.compruebaPreinscripcion(personaId, grupoId) == 0) {
            personaPreinscripcionService.addPreinscripcion(entity, personaId, grupoId);
        }

        return Response.ok().build();
    }

    @GET
    @Path("{grupoId}/{tarjeta}/importe")
    public UIEntity getImportePreinscripciones(@PathParam("personaId") Long personaId,
                                               @PathParam("grupoId") Long grupoId,
                                               @PathParam("tarjeta") Long tarjeta) {
        String xml = personaService.dameImpInscPer(grupoId, personaId, "ALL", new Date());
        UIEntity entity = new UIEntity();
        entity.put("importeTexto", personaService.getImporte(xml, "texto"));
        entity.put("importe", personaService.getImporte(xml, "importe"));
        return entity;
    }
}
