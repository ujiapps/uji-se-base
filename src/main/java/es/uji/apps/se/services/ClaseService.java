package es.uji.apps.se.services;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.CalendarioClaseDAO;
import es.uji.apps.se.dto.CalendarioClaseDTO;
import es.uji.apps.se.dto.ClaseDTO;
import es.uji.apps.se.exceptions.BorradoClaseDirigidaException;
import es.uji.apps.se.utils.Csv;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.se.dao.ClaseDAO;
import es.uji.apps.se.model.Paginacion;

@Service
public class ClaseService {

    @InjectParam
    Csv csv;

    private final ClaseDAO claseDAO;
    private final CalendarioClaseService calendarioClaseService;
    private final ClaseDirigidaTipoService claseDirigidaTipoService;
    private final TarjetaDeportivaClaseService tarjetaDeportivaClaseService;
    private final CalendarioClaseDAO calendarioClaseDAO;
    private final CalendarioClaseInstalacionService calendarioClaseInstalacionService;

    @Autowired
    public ClaseService(ClaseDAO claseDAO,
                        CalendarioClaseService calendarioClaseService,
                        ClaseDirigidaTipoService claseDirigidaTipoService,
                        TarjetaDeportivaClaseService tarjetaDeportivaClaseService,
                        CalendarioClaseDAO calendarioClaseDAO,
                        CalendarioClaseInstalacionService calendarioClaseInstalacionService) {
        this.claseDAO = claseDAO;
        this.calendarioClaseService = calendarioClaseService;
        this.claseDirigidaTipoService = claseDirigidaTipoService;
        this.tarjetaDeportivaClaseService = tarjetaDeportivaClaseService;
        this.calendarioClaseDAO = calendarioClaseDAO;
        this.calendarioClaseInstalacionService = calendarioClaseInstalacionService;
    }


    public List<ClaseDTO> getClases(Paginacion paginacion) {
        return claseDAO.getClases(paginacion);
    }

    public List<ClaseDTO> getClasesParaExport() {
        return claseDAO.getClasesParaExport();
    }

    public ClaseDTO insertClase(ClaseDTO claseDTO) {
        return claseDAO.insert(claseDTO);
    }

    public ClaseDTO updateClase(ClaseDTO claseDTO) {
        return claseDAO.update(claseDTO);
    }

    public void deleteClase(Long claseId) throws BorradoClaseDirigidaException {
        calendarioClaseService.deleteCalendarioClaseByClaseId(claseId);
        claseDirigidaTipoService.deleteClaseDirigidaTipoByClase(claseId);
        claseDAO.delete(ClaseDTO.class, claseId);
    }

    public List<ClaseDTO> getClasesActivas() {

        return claseDAO.getClasesActivas();
    }

    public Long getRecuentoClasesDirigidas(Long cursoAcademico) {
        return claseDAO.getRecuentoClasesDirigidas(cursoAcademico);
    }

    public Long getRecuentoClasesDirigidasActivas() {
        return claseDAO.getRecuentoClasesDirigidasActivas();
    }

    public void deleteClaseMultiple(List<Long> clases, String fechaInicio, String fechaFin, String horaInicio, String horaFin, List<Integer> diasSemana) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDate fechaIni = LocalDate.parse(fechaInicio, formatter);
        LocalDate fechaF = LocalDate.parse(fechaFin, formatter).plusDays(1);

        for (LocalDate date = fechaIni; date.isBefore(fechaF); date = date.plusDays(1)) {
            if (diasSemana.contains(date.getDayOfWeek().getValue()) || !ParamUtils.isNotNull(diasSemana.get(0))) {
                LocalDate finalDate = date;
                if (!clases.isEmpty() && ParamUtils.isNotNull(clases.get(0))) {
                    clases.forEach(claseId -> {
                        List<CalendarioClaseDTO> calendariosClase = calendarioClaseDAO.getCalendarioClaseByClaseIdAndFecha(claseId, Date.from(finalDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
                        eliminaClasesCalendario(horaInicio, horaFin, calendariosClase);
                    });
                } else {
                    List<CalendarioClaseDTO> calendariosClase = calendarioClaseDAO.getCalendarioClasesByFechas(Date.from(finalDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), Date.from(finalDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), null);
                    eliminaClasesCalendario(horaInicio, horaFin, calendariosClase);
                }
            }
        }

    }

    private void eliminaClasesCalendario(String horaInicio, String horaFin, List<CalendarioClaseDTO> calendariosClase) {
        calendariosClase.forEach(calendarioClaseDTO -> {
            String horaI = horaInicio;
            String horaF = horaFin;

            if (!ParamUtils.isNotNull(horaInicio)) horaI = calendarioClaseDTO.getHoraInicio();
            if (!ParamUtils.isNotNull(horaFin)) horaF = calendarioClaseDTO.getHoraFin();
            if (ParamUtils.isNotNull(calendarioClaseDTO.getHoraInicio())
                    && ParamUtils.isNotNull(calendarioClaseDTO.getHoraFin())
                    && calendarioClaseDTO.getHoraInicio().equals(horaI)
                    && calendarioClaseDTO.getHoraFin().equals(horaF)) {
                tarjetaDeportivaClaseService.deleteTarjetaDeportivaClasesByCalendarioId(calendarioClaseDTO.getId());
                calendarioClaseInstalacionService.deleteCalendarioClaseInstalacionByCalendarioClase(calendarioClaseDTO);
                try {
                    calendarioClaseService.deleteCalendarioClase(calendarioClaseDTO.getId());
                } catch (BorradoClaseDirigidaException e) {
                    throw new RuntimeException(e);
                }
            }

            if ((!ParamUtils.isNotNull(calendarioClaseDTO.getHoraInicio()) && !ParamUtils.isNotNull(horaInicio))
                    && (!ParamUtils.isNotNull(calendarioClaseDTO.getHoraFin()) && !ParamUtils.isNotNull(horaFin))){
                tarjetaDeportivaClaseService.deleteTarjetaDeportivaClasesByCalendarioId(calendarioClaseDTO.getId());
                calendarioClaseInstalacionService.deleteCalendarioClaseInstalacionByCalendarioClase(calendarioClaseDTO);
                try {
                    calendarioClaseService.deleteCalendarioClase(calendarioClaseDTO.getId());
                } catch (BorradoClaseDirigidaException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public String entityClaseToCSV(List<ClaseDTO> lista) {
        ArrayList<String> cabeceras = new ArrayList<>(Arrays.asList("Nom", "Descripció", "activa",
                "places", "icono"));

        List<List<String>> records = new ArrayList<>();
        for (ClaseDTO clase : lista) {
            List<String> recordMov = new ArrayList<>(creaListaString(clase));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(ClaseDTO clase) {
        return Arrays.asList(clase.getNombre(),
                clase.getDescripcion(),
                (clase.getActiva()) ? "SI" : "NO",
                clase.getPlazas().toString(),
                clase.getIcono());
    }
}
