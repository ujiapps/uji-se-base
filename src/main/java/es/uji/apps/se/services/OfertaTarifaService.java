package es.uji.apps.se.services;

import es.uji.apps.se.dao.OfertaTarifaDAO;
import es.uji.apps.se.dto.OfertaDTO;
import es.uji.apps.se.dto.OfertaTarifaDTO;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfertaTarifaService {

    private OfertaTarifaDAO ofertaTarifaDAO;

    @Autowired
    public OfertaTarifaService(OfertaTarifaDAO ofertaTarifaDAO) {
        this.ofertaTarifaDAO = ofertaTarifaDAO;
    }

    public List<OfertaTarifaDTO> getTarifasByOfertaId(Long ofertaId, Paginacion paginacion) {
        return  ofertaTarifaDAO.getOfertasTarifasByOferta(ofertaId, paginacion);
    }

    public OfertaTarifaDTO updateTarifa(UIEntity entity, Long ofertaId) {
        return ofertaTarifaDAO.update(uiEntityToModel(entity, ofertaId));
    }

    public void deleteTarifa(Long tarifaId) {
        ofertaTarifaDAO.delete(OfertaTarifaDTO.class, tarifaId);
    }

    public OfertaTarifaDTO insertTarifa(UIEntity entity, Long ofertaId) {
        return ofertaTarifaDAO.insert(uiEntityToModel(entity, ofertaId));
    }

    private OfertaTarifaDTO uiEntityToModel(UIEntity entity, Long ofertaId) {
        OfertaTarifaDTO ofertaTarifaDTO = entity.toModel(OfertaTarifaDTO.class);

        OfertaDTO ofertaDTO = new OfertaDTO();
        ofertaDTO.setId(ofertaId);
        ofertaTarifaDTO.setOferta(ofertaDTO);

        TipoDTO vinculo = new TipoDTO();
        vinculo.setId(ParamUtils.parseLong(entity.get("vinculoId")));
        ofertaTarifaDTO.setVinculo(vinculo);

        ofertaTarifaDTO.setFechaInicio(entity.getDate("fechaInicio"));

        if (ParamUtils.isNotNull(entity.get("fechaFin"))) {
            ofertaTarifaDTO.setFechaFin(entity.getDate("fechaFin"));
        }

        return ofertaTarifaDTO;
    }
}
