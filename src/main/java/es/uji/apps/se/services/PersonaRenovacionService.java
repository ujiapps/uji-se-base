package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PersonaRenovacionService {
    private final ParametroDAO parametroDAO;
    private final PersonaRenovacionDAO personaRenovacionDAO;
    private final PersonaDAO personaDAO;

    @Autowired
    public PersonaRenovacionService(PersonaRenovacionDAO personaRenovacionDAO, PersonaDAO personaDAO, ParametroDAO parametroDAO) {
        this.personaRenovacionDAO = personaRenovacionDAO;
        this.personaDAO = personaDAO;
        this.parametroDAO = parametroDAO;
    }

    public List<CarnetBonoTipo> getRenovacionesZona(Long personaId, Boolean isTodasZonas) {
        return personaRenovacionDAO.getRenovacionesZona(personaId, getAnyoNacimiento(personaId), isTodasZonas, parametroDAO.getParametroGlobal("TARIFAS-CARNETS-BONOS").getValor());
    }

    public List<CarnetBonoTipo> getRenovacionesTD(Long personaId) {
        return personaRenovacionDAO.getRenovacionesTD(personaId, getAnyoNacimiento(personaId), parametroDAO.getParametroGlobal("TARIFAS-CARNETS-BONOS").getValor());
    }

    public Long getAnyoNacimiento(Long personaId) {
        return personaDAO.getAnyoNacimiento(personaId);
    }

}
