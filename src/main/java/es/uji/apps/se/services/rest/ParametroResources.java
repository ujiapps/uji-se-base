package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.ParametroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("parametro")
public class ParametroResources extends CoreBaseService {

    @InjectParam
    ParametroService parametroService;

    @GET
    public ResponseMessage getParametrosPersonales(@QueryParam("page") Long page, @QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                                   @QueryParam("sort") String ordenacion) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Paginacion paginacion = new Paginacion(start, limit, ordenacion);

        List<ParametroDTO> lista = parametroService.getParametrosPersonales(connectedUserId, paginacion);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(UIEntity.toUI(lista));

        return responseMessage;

    }

    @GET
    @Path("globales")
    public ResponseMessage getParametrosGlobales(@QueryParam("page") Long page, @QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                                 @QueryParam("sort") String ordenacion) throws IOException {

        Paginacion paginacion = new Paginacion(start, limit, ordenacion);

        List<ParametroDTO> lista = parametroService.getParametrosGlobales(paginacion);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(UIEntity.toUI(lista));

        return responseMessage;
    }

    @GET
    @Path("cursoacademico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getParametroCursoAcademico() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(parametroService.getParametroCursoAcademico(connectedUserId));
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("sancion/clasedirigida/tiempo")
    public UIEntity getParametroSancionClaseDirigidaTiempo (){
        return UIEntity.toUI(parametroService.getParametroSancionClaseDirigidaTiempo());
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("sancion/clasedirigida/tiempo")
    public UIEntity updateParametroSancionClaseDirigidaTiempo (@FormParam("tiempoSancion") Long tiempoSancion){
        return UIEntity.toUI(parametroService.updateParametroSancionClaseDirigidaTiempo(tiempoSancion));
    }

    @POST
    @Path("globales")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addParametroGlobal(UIEntity entity) {

        ParametroDTO parametroDTO = entity.toModel(ParametroDTO.class);
        return UIEntity.toUI(parametroService.addParametro(parametroDTO));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addParametro(UIEntity entity) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParametroDTO parametroDTO = entity.toModel(ParametroDTO.class);
        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(connectedUserId);
        parametroDTO.setPersona(persona);
        return UIEntity.toUI(parametroService.addParametro(parametroDTO));
    }

    @PUT
    @Path("globales/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateParametroGlobal(UIEntity entity) {
        ParametroDTO parametroDTO = entity.toModel(ParametroDTO.class);
        return UIEntity.toUI(parametroService.updateParametro(parametroDTO));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateParametro(UIEntity entity) {

        ParametroDTO parametroDTO = entity.toModel(ParametroDTO.class);

        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(ParamUtils.parseLong(entity.get("personaId")));
        parametroDTO.setPersona(persona);

        return UIEntity.toUI(parametroService.updateParametro(parametroDTO));
    }

    @PUT
    @Path("cursoacademico")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateParametroCursoAcademico(@FormParam("cursoacademico") Long cursoAcademico) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(parametroService.updateParametroCursoAcademico(connectedUserId, cursoAcademico));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteParametro(@PathParam("id") Long parametroId) {
        parametroService.deleteParametro(parametroId);
    }

    @DELETE
    @Path("globales/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteParametroGlobal(@PathParam("id") Long parametroId) {
        parametroService.deleteParametro(parametroId);
    }
}

