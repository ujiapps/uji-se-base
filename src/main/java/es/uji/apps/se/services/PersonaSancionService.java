package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.HistoricoSancion;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.Sancion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class PersonaSancionService {
    private final PersonaSancionDAO personaSancionDAO;
    private final PersonaSancionDetalleService personaSancionDetalleService;
    public final static Logger log = LoggerFactory.getLogger(PersonaSancionService.class);

    @Autowired
    public PersonaSancionService(PersonaSancionDAO personaSancionDAO, PersonaSancionDetalleService personaSancionDetalleService) {
        this.personaSancionDetalleService = personaSancionDetalleService;
       this.personaSancionDAO = personaSancionDAO;
    }
    public List<Sancion> getSanciones(Long personaId, Paginacion paginacion) {
        return personaSancionDAO.getSanciones(personaId, paginacion);
    }

    public void addSancion(UIEntity entity, Long personaId, Long connectedUserId) throws CommonException {
        try {
            Sancion sancion = getSancion(entity);
            personaSancionDAO.addSancion(sancion, personaId, connectedUserId);
        }
        catch (Exception e) {
            log.error("No s'ha pogut inserir aquesta sanció", e);
            throw new CommonException("No s'ha pogut inserir aquesta sanció");
        }
    }

    public void updateSancion(UIEntity entity, Long sancionId, Long connectedUserId) throws CommonException {
        try {
            Sancion sancion = getSancion(entity);
            TipoDTO tipoDTO = new TipoDTO(sancion.getNivelId(), sancion.getNivelNombre());
            personaSancionDAO.updateSancion(sancion, sancionId, tipoDTO, connectedUserId);
        }
        catch (Exception e) {
            log.error("No s'ha pogut actualitzar aquesta sanció", e);
            throw new CommonException("No s'ha pogut actualitzar aquesta sanció");
        }
    }

    public Sancion getSancionById (Long sancionId){
        return new Sancion (personaSancionDAO.getSancionById(sancionId));
    }

    public void deleteSancion(Long sancionId, Long connectedUserId) throws CommonException {

        try {
            personaSancionDetalleService.deleteDetallesSancion(sancionId);
            Sancion sancion = getSancionById(sancionId);
            TipoDTO tipoDTO = new TipoDTO(sancion.getNivelId(), sancion.getNivelNombre());
            personaSancionDAO.updateSancion(sancion, sancionId, tipoDTO, connectedUserId);
            personaSancionDAO.deleteSancion(sancionId);
        } catch (Exception e) {
            log.error("No s'ha pogut esborrar aquesta sanció", e);
            throw new CommonException("No s'ha pogut esborrar aquesta sanció");
        }
    }

    private Sancion getSancion(UIEntity entity) throws ParseException {
        Sancion sancion = entity.toModel(Sancion.class);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        if(entity.get("fecha") != null) {
            sancion.setFecha(sdf.parse(entity.get("fecha")));
        }

        if(entity.get("fechaFin") != null) {
            sancion.setFechaFin(sdf.parse(entity.get("fechaFin")));
        }

        if(entity.get("motivo").isEmpty()) {
            sancion.setMotivo(null);
        }

        return sancion;
    }

    public List<HistoricoSancion> getHistoricoSanciones(Long personaId, Long conectedUserId) {
        return personaSancionDAO.getHistoricoSanciones(personaId, conectedUserId);
    }

    public Boolean tieneSancion(Long personaId) {
        return getSanciones(personaId, null)
                .stream()
                .anyMatch(sancion -> sancion.getFechaFin() == null || sancion.getFechaFin().after(new Date()));
    }


    public Sancion getSancionBloqueaTodoOrClasesDirigidas(Long personaId) {
        Date fechaActual = new Date();
        List<Sancion> sanciones = getSanciones(personaId, null);
        return sanciones.stream()
                .filter(s -> s.getFecha().before(fechaActual) && (!ParamUtils.isNotNull(s.getFechaFin()) || s.getFechaFin().after(fechaActual)) && (s.getTipo().equals("ALL") || s.getTipo().equals("CLA")))
                .findFirst().orElse(null);
    }

    public Boolean tieneSancionActivaQueBloqueaInscripcion(Long personaId) {
        List<Sancion> sanciones = getSanciones(personaId, null);
        return sanciones.stream().filter(t -> (t.getOrigen().equals("DIRECTAS") && t.getTipo().equals("ALL")) || t.getOrigen().equals("RECIBO"))
                .anyMatch(t -> t.getFechaFin().after(new Date()) || (t.getFechaFin() == null));
    }
}
