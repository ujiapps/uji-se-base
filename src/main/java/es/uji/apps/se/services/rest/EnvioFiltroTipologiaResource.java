package es.uji.apps.se.services.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.exceptions.ErrorFiltroEnviosClases;
import es.uji.apps.se.model.EnvioFiltroTipologia;
import es.uji.apps.se.services.EnvioFiltroTipologiaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

public class EnvioFiltroTipologiaResource extends CoreBaseService {

    @InjectParam
    EnvioFiltroTipologiaService envioFiltroTipologiaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public EnvioFiltroTipologia getFiltro(@PathParam("envioId") Long envioId) {
        List<EnvioFiltroDTO> envioFiltroTipologias = envioFiltroTipologiaService.getEnvioFiltroTipologiasByEnvioId(envioId);

        EnvioFiltroTipologia envioFiltroTipologia = new EnvioFiltroTipologia(envioId, envioFiltroTipologias);
        return envioFiltroTipologia;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage addFiltroAEnvio(@PathParam("envioId") Long envioId,
                                           @FormParam("data") String data) throws ErrorFiltroEnviosClases {

        EnvioFiltroTipologia envioFiltroTipologia;
        try {
            envioFiltroTipologia = new ObjectMapper().readValue(data, EnvioFiltroTipologia.class);
            envioFiltroTipologia.setEnvio(envioId);
            return envioFiltroTipologiaService.addFiltroTipologiaAEnvio(envioFiltroTipologia);
        } catch (IOException e) {
            throw new ErrorFiltroEnviosClases(e.getMessage());
        }
    }

}
