package es.uji.apps.se.services;

import es.uji.apps.se.dao.PersonaTarjetaBonoDAO;
import es.uji.apps.se.dao.TarjetaDeportivaDAO;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.ReservaException;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.model.ui.UITarjetaDeportivaAdmin;
import es.uji.apps.se.model.filtros.FiltroTarjetaDeportiva;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TarjetaDeportivaService {

    private final TarjetaDeportivaDAO tarjetaDeportivaDAO;
    private final TarjetaDeportivaTipoService tarjetaDeportivaTipoService;
    private final ReciboService reciboService;
    private final PersonaUjiService personaUjiService;
    private final PersonaService personaService;
    private final PersonaTarjetaBonoDAO personaTarjetaBonoDAO;

    @Autowired
    public TarjetaDeportivaService(TarjetaDeportivaDAO tarjetaDeportivaDAO,
                                   TarjetaDeportivaTipoService tarjetaDeportivaTipoService,
                                   ReciboService reciboService,
                                   PersonaUjiService personaUjiService,
                                   PersonaService personaService, PersonaTarjetaBonoDAO personaTarjetaBonoDAO) {
        this.tarjetaDeportivaDAO = tarjetaDeportivaDAO;
        this.tarjetaDeportivaTipoService = tarjetaDeportivaTipoService;
        this.reciboService = reciboService;
        this.personaUjiService = personaUjiService;
        this.personaService = personaService;
        this.personaTarjetaBonoDAO = personaTarjetaBonoDAO;
    }

    public final static Logger log = LoggerFactory.getLogger(TarjetaDeportivaService.class);

    public List<TarjetaDeportivaDTO> getAll() {
        return tarjetaDeportivaDAO.get(TarjetaDeportivaDTO.class);
    }

    @Role("ADMIN")
    @Transactional
    public UITarjetaDeportivaAdmin insert(Long personaId, Long tarjetaDeportivaTipoId, String fechaAlta, String fechaBaja, String comentarios, Long connectedUserId) {
        try {

            TarjetaDeportivaDTO tarjetaDeportivaDTO = new TarjetaDeportivaDTO();

            TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoService.getTarjetaDeportivaTipoById(tarjetaDeportivaTipoId);
            tarjetaDeportivaDTO.setTarjetaDeportivaTipo(tarjetaDeportivaTipoDTO);

            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

            Date fechaA = new Date();
            if (ParamUtils.isNotNull(fechaAlta)) {
                fechaA = formato.parse(fechaAlta);
            }
            tarjetaDeportivaDTO.setFechaAlta(fechaA);

            if (ParamUtils.isNotNull(fechaBaja)) {
                Date fechaB = formato.parse(fechaBaja);
                tarjetaDeportivaDTO.setFechaBaja(fechaB);
            } else
                tarjetaDeportivaDTO.setFechaBaja(recalculaFechaBaja(tarjetaDeportivaDTO.getFechaAlta(), tarjetaDeportivaTipoDTO));

            tarjetaDeportivaDTO.setGenerarRecibo(Boolean.TRUE);
            tarjetaDeportivaDTO.setPersonaAccion(connectedUserId);

            PersonaDTO personaSE = personaService.getPersonaById(personaId);
            tarjetaDeportivaDTO.setPersonaSE(personaSE);

            PersonaUjiDTO personaUjiDTO = personaUjiService.getPersonaById(personaId);
            tarjetaDeportivaDTO.setPersonaUji(personaUjiDTO);

            tarjetaDeportivaDTO.setComentarios(comentarios);

            tarjetaDeportivaDAO.insert(tarjetaDeportivaDTO);

            TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getTarifaByVinculo(personaUjiDTO.getVinculoId());

            if (ParamUtils.isNotNull(tarjetaDeportivaTarifaDTO.getImporte()) && !tarjetaDeportivaTarifaDTO.getImporte().equals(0F)) {
                try {
                    UIEntity recibo = reciboService.generaRecibo(null, "1", tarjetaDeportivaDTO, tarjetaDeportivaDTO.getTarjetaDeportivaTipo(),
                            personaUjiDTO, tarjetaDeportivaTarifaDTO);
                    return new UITarjetaDeportivaAdmin(tarjetaDeportivaDTO, recibo);

                } catch (Exception e) {
                    log.error("Error al generar recibo", e);
                    return new UITarjetaDeportivaAdmin(tarjetaDeportivaDTO, null);
                }
            } else {
                tarjetaDeportivaDTO.setGenerarRecibo(Boolean.FALSE);
                tarjetaDeportivaDAO.update(tarjetaDeportivaDTO);
                return new UITarjetaDeportivaAdmin(tarjetaDeportivaDTO, null);
            }
        } catch (Exception e) {
            log.error("Error al generar tarjeta", e);
            return null;
        }
    }

    @Role("ADMIN")
    public void delete(Long tarjetaDeportivaId, Long connectedUserId) {
        TarjetaDeportivaDTO tarjetaDeportivaDTO = getTarjetaDeportivaById(tarjetaDeportivaId);
        tarjetaDeportivaDTO.setPersonaAccion(connectedUserId);
        tarjetaDeportivaDAO.update(tarjetaDeportivaDTO);
        tarjetaDeportivaDAO.delete(TarjetaDeportivaDTO.class, tarjetaDeportivaId);
    }

    @Role("ADMIN")
    public TarjetaDeportivaDTO update(Long tarjetaDeportivaId, String fechaAlta, String fechaBaja, String comentarios, Long connectedUserId) throws ParseException {

        TarjetaDeportivaDTO tarjetaDeportivaDTO = getTarjetaDeportivaById(tarjetaDeportivaId);

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        Date fechaA = null;
        if (ParamUtils.isNotNull(fechaAlta))
            fechaA = formato.parse(fechaAlta);
        tarjetaDeportivaDTO.setFechaAlta(fechaA);

        Date fechaB = null;
        if (ParamUtils.isNotNull(fechaBaja))
            fechaB = formato.parse(fechaBaja);
        tarjetaDeportivaDTO.setFechaBaja(fechaB);

        tarjetaDeportivaDTO.setComentarios(comentarios);

        tarjetaDeportivaDTO.setPersonaAccion(connectedUserId);

        return tarjetaDeportivaDAO.update(tarjetaDeportivaDTO);
    }

    public List<TarjetaDeportivaDTO> getBySearch(FiltroTarjetaDeportiva filtro, Paginacion paginacion) {
        return tarjetaDeportivaDAO.getBySearch(filtro, paginacion);
    }


    public TarjetaDeportivaDTO getTarjetaDeportivaById(Long tarjetaDeportivaId) {
        return tarjetaDeportivaDAO.getTarjetaDeportivaById(tarjetaDeportivaId);
    }

    public TarjetaDeportivaDTO getTarjetaDeportivaDTOActivaByPersonaId(Long personaId) {
        return tarjetaDeportivaDAO.getTarjetaDeportivaDTOActivaByPersonaId(personaId);
    }

    public TarjetaDeportiva getTarjetaDeportivaActivaByPersonaId(Long personaId) {
        return tarjetaDeportivaDAO.getTarjetaDeportivaActivaByPersonaId(personaId);
    }

    public TarjetaDeportivaDTO getTarjetaDeportivaFuturaByPersonaId(Long personaId) {
        return tarjetaDeportivaDAO.getTarjetaDeportivaFuturaByPersonaId(personaId);
    }

    @Transactional
    public ResponseMessage addTarjetaDeportiva(Long personaId, Long tipoTarjetaId, Long bonificacionId, String metodoPago) {
        ResponseMessage responseMessage = new ResponseMessage();
        List<UIEntity> list = new ArrayList<>();

        try {
            TarjetaDeportivaDTO tarjetaDeportivaDTO = getTarjetaDeportivaDTOActivaByPersonaId(personaId);

            tarjetaDeportivaDTO = compruebaSiEsAltaDeTarjetaSuperior(tipoTarjetaId, tarjetaDeportivaDTO);

            if (tarjetaDeportivaDTO != null && !tarjetaDeportivaDTO.isPeriodoRenovacion()) {
                throw new ReservaException(MessageFormat.format("Ja hi ha donada d''alta una targeta esportiva {0} amb vigencia del {1} al {2}",
                        tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getNombre(),
                        DateExtensions.getDateAsString(tarjetaDeportivaDTO.getFechaAlta()),
                        DateExtensions.getDateAsString(tarjetaDeportivaDTO.getFechaBaja())));
            }

            TarjetaDeportivaDTO tarjetaDeportivaFuturaDTO = getTarjetaDeportivaFuturaByPersonaId(personaId);
            if (tarjetaDeportivaFuturaDTO != null) {
                throw new ReservaException(MessageFormat.format("Ja hi ha donada una nova targeta esportiva per a quan caduqui l'actual {0} amb vigencia del {1} al {2}",
                        tarjetaDeportivaFuturaDTO.getTarjetaDeportivaTipo().getNombre(),
                        DateExtensions.getDateAsString(tarjetaDeportivaFuturaDTO.getFechaAlta()),
                        DateExtensions.getDateAsString(tarjetaDeportivaFuturaDTO.getFechaBaja())));
            }

            Date fechaAlta = (tarjetaDeportivaDTO == null) ? DateExtensions.getCurrentDate() : DateExtensions.addDays(DateExtensions.truncDate(tarjetaDeportivaDTO.getFechaBaja()), 1);

            TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoService.getTarjetaDeportivaTipoById(tipoTarjetaId);

            tarjetaDeportivaDTO = new TarjetaDeportivaDTO();
            tarjetaDeportivaDTO.setFechaAlta(fechaAlta);

            tarjetaDeportivaDTO.setFechaBaja(recalculaFechaBaja(tarjetaDeportivaDTO.getFechaAlta(), tarjetaDeportivaTipoDTO));

            tarjetaDeportivaDTO.setTarjetaDeportivaTipo(tarjetaDeportivaTipoDTO);
            tarjetaDeportivaDTO.setGenerarRecibo(Boolean.TRUE);

            PersonaUjiDTO personaUjiDTO = personaUjiService.getPersonaById(personaId);
            tarjetaDeportivaDTO.setPersonaUji(personaUjiDTO);
            tarjetaDeportivaDTO.setPersonaAccion(personaId);

            PersonaDTO personaSE = personaService.getPersonaById(personaId);
            tarjetaDeportivaDTO.setPersonaSE(personaSE);

            tarjetaDeportivaDTO = tarjetaDeportivaDAO.insert(tarjetaDeportivaDTO);

            TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO = tarjetaDeportivaTipoDTO.getTarifaByVinculo(personaUjiDTO.getVinculoId());

            if (ParamUtils.isNotNull(tarjetaDeportivaTarifaDTO.getImporte()) && tarjetaDeportivaTarifaDTO.getImporte().longValue() != 0L) {
                UIEntity entity = reciboService.generaRecibo(bonificacionId, metodoPago, tarjetaDeportivaDTO, tarjetaDeportivaTipoDTO, personaUjiDTO, tarjetaDeportivaTarifaDTO);
                list.add(entity);
            } else {
                tarjetaDeportivaDTO.setGenerarRecibo(Boolean.FALSE);
            }
            responseMessage.setSuccess(true);
        } catch (Exception e
        ) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Error al tramitar la targeta, possat en contacte amb el SE.");
        }
        responseMessage.setData(list);
        return responseMessage;

    }


    private TarjetaDeportivaDTO compruebaSiEsAltaDeTarjetaSuperior(Long tipoTarjetaId, TarjetaDeportivaDTO
            tarjetaDeportivaDTO) {
        if (ParamUtils.isNotNull(tarjetaDeportivaDTO)) {
            for (TarjetaDeportivaSuperiorDTO tarjetaDeportivaTipoSuperior : tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getTarjetaDeportivaSuperiores()) {
                if (tarjetaDeportivaTipoSuperior.getTarjetaDeportivaTipoSuperior().getId().equals(tipoTarjetaId)) {
                    tarjetaDeportivaDTO.setFechaBaja(new Date());
                    tarjetaDeportivaDAO.update(tarjetaDeportivaDTO);
                    return null;
                }
            }
        }
        return tarjetaDeportivaDTO;
    }

    public List<TarjetaDeportivaDTO> getTarjetaDeportivaByPersonaId(Long personaId, Paginacion paginacion) {
        return tarjetaDeportivaDAO.getTarjetaDeportivaByPersonaId(personaId, paginacion);
    }

    private Date recalculaFechaBaja(Date fechaAlta, TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO) {
        if (tarjetaDeportivaTipoDTO.getFechaCaducidad() != null) {
            return tarjetaDeportivaTipoDTO.getFechaCaducidad();
        } else {
            int dias = ParamUtils.isNotNull(tarjetaDeportivaTipoDTO.getDias()) ? tarjetaDeportivaTipoDTO.getDias().intValue() : 0,
                    meses = ParamUtils.isNotNull(tarjetaDeportivaTipoDTO.getMeses()) ? tarjetaDeportivaTipoDTO.getMeses().intValue() : 0,
                    anyos = ParamUtils.isNotNull(tarjetaDeportivaTipoDTO.getAnyos()) ? tarjetaDeportivaTipoDTO.getAnyos().intValue() : 0;


            Date fechaBaja = new Date(fechaAlta.getTime());
            if (meses > 0 || anyos > 0)
                fechaBaja = DateExtensions.addDays(DateExtensions.addMonths(DateExtensions.addYears(fechaBaja, anyos), meses), -1);

            fechaBaja = DateExtensions.addDays(fechaBaja, dias);

            return fechaBaja;
        }
    }

    public Boolean tieneTarjeta(Long personaId, String tipoId, Long id) {
        return personaTarjetaBonoDAO.tieneTarjeta(personaId, tipoId, id);
    }
}
