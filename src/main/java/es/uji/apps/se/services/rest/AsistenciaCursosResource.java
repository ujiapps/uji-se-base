package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.AsistenciaCursosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("asistenciacursos")
public class AsistenciaCursosResource extends CoreBaseService {
    @InjectParam
    AsistenciaCursosService asistenciaCursosService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getAsistenciaCursos(@QueryParam("personaId") Long perId) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            responseMessage.setData(UIEntity.toUI(asistenciaCursosService.getAsistenciaCursos(perId)));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("listadoFaltas")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getListadoFaltasCursos(@QueryParam("personaId") Long perId,
                                                  @QueryParam("cursoId") Long cursoId) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            responseMessage.setData(UIEntity.toUI(asistenciaCursosService.getListadoFaltasCursos(perId, cursoId)));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("{cursoId}/diasactiv")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDiasActiv(@PathParam("cursoId") Long cursoId) {
        Long diasActiv = asistenciaCursosService.getDiasActiv(cursoId);
        return Response.ok(diasActiv).build();
    }

    @GET
    @Path("{cursoId}/{personaId}/diasasis")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDiasAsis(@PathParam("cursoId") Long cursoId,
                                @PathParam("personaId") Long personaId) {
        Long diasAsis = asistenciaCursosService.getDiasAsis(cursoId, personaId);
        return Response.ok(diasAsis).build();
    }
}
