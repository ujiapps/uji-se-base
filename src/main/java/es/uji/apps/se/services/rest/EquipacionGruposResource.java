package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes.GruposDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes.GruposInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes.GruposUpdateException;
import es.uji.apps.se.model.EquipacionGrupos;
import es.uji.apps.se.services.EquipacionGruposService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class EquipacionGruposResource extends CoreBaseService {

    @InjectParam
    EquipacionGruposService equipacionGruposService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEquipacionModelos() {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            List<EquipacionGrupos> equipacionGrupos = equipacionGruposService.getEquipacionGrupos();
            responseMessage.setTotalCount(equipacionGrupos.size());
            responseMessage.setData(UIEntity.toUI(equipacionGrupos));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEquipacionGrupo(UIEntity entity) throws GruposInsertException {
        EquipacionGrupos equipacionGrupo = entity.toModel(EquipacionGrupos.class);
        equipacionGruposService.addEquipacionGrupo(equipacionGrupo);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEquipacionGrupo(UIEntity entity) throws GruposUpdateException {
        EquipacionGrupos equipacionGrupo = entity.toModel(EquipacionGrupos.class);
        equipacionGruposService.updateEquipacionGrupo(equipacionGrupo);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEquipacionGrupo(UIEntity entity) throws GruposDeleteException {
        EquipacionGrupos equipacionGrupo = entity.toModel(EquipacionGrupos.class);
        equipacionGruposService.deleteEquipacionGrupo(equipacionGrupo);
        return Response.ok().build();
    }
}