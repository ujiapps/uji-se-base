package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.CursosDAO;
import es.uji.apps.se.model.Curso;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CursosService {
    @InjectParam
    private CursosDAO cursosDAO;

    public List<Curso> getCursos(Long perId) {
        List<Curso> list = cursosDAO.getCursos(perId);
        list.forEach(curso -> curso.setComentario(cursosDAO.getComentario(curso, perId)));
        return list;
    }
}
