package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.JaulaPersonaDAO;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.filtros.FiltroJaula;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JaulaPersonaService {

    @InjectParam
    JaulaPersonaDAO jaulaPersonaDAO;
    @InjectParam
    PersonaUjiService personaUjiService;
    @InjectParam
    PersonaService personaService;
    @InjectParam
    ParametroService parametroService;

    public final static Logger log = LoggerFactory.getLogger(JaulaPersonaService.class);

    public List<JaulaPersonaVistaDTO> getJaulasPersonas(Paginacion paginacion, FiltroJaula filtro) {
        return jaulaPersonaDAO.getJaulasPersonas(paginacion, filtro);
    }

    public JaulaPersonaDTO insertJaulaPersona(UIEntity entity) {

        JaulaPersonaDTO jaulaPersonaDTO = insertaJaulaPersona(ParamUtils.parseLong(entity.get("personaUjiId")));

        jaulaPersonaDTO.setFechaInicio(entity.getDate("fechaInicio"));
        jaulaPersonaDTO.setFechaFin(entity.getDate("fechaFin"));

        return jaulaPersonaDAO.update(jaulaPersonaDTO);
    }

    public JaulaPersonaDTO updateJaulaPersona(UIEntity entity) {

        PersonaDTO personaDTO = personaService.getPersonaById(ParamUtils.parseLong(entity.get("personaUjiId")));

        JaulaPersonaDTO jaulaPersonaDTO = entity.toModel(JaulaPersonaDTO.class);
        jaulaPersonaDTO.setFechaInicio(entity.getDate("fechaInicio"));
        jaulaPersonaDTO.setFechaFin(entity.getDate("fechaFin"));
        PersonaUjiDTO personaUjiDTO = personaUjiService.getPersonaById(ParamUtils.parseLong(entity.get("personaUjiId")));
        jaulaPersonaDTO.setPersonaUji(personaUjiDTO);
        jaulaPersonaDTO.setFechaLlavero(entity.getDate("fechaLlavero"));
        jaulaPersonaDTO = jaulaPersonaDAO.update(jaulaPersonaDTO);


//        try {
//            MailMessage mensaje = new MailMessage("SE");
//
//            mensaje.setTitle("Gestió aparca bicicletes");
//            mensaje.setContentType("text/html; charset=UTF-8");
//            String cuerpo = parametroService.getParametroGlobal("CORREOJAULAACTIVA").getValor();
//            cuerpo = cuerpo.replace("$[fecha_inicio]", DateExtensions.getDateAsString(jaulaPersonaDTO.getFechaInicio()));
//            cuerpo = cuerpo.replace("$[fecha_fin]", DateExtensions.getDateAsString(jaulaPersonaDTO.getFechaFin()));
//
//            mensaje.setContent(cuerpo);
//            mensaje.setReplyTo("noreply@uji.es");
//            mensaje.setSender("Servei d'esports <noreply@uji.es>");
//            mensaje.addToRecipient((ParamUtils.isNotNull(personaDTO.getMail()) ? personaDTO.getMail() : personaDTO.getPersonaUji().getMail()));
//            MessagingClient cliente = new MessagingClient();
//            cliente.send(mensaje);
//        } catch (Exception e) {
//            log.error("No se puede enviar un correo desde devel", e);
//        }
        return jaulaPersonaDTO;
    }

    public void deleteJaulaPersona(Long jaulaPersonaId) {
        jaulaPersonaDAO.delete(JaulaPersonaDTO.class, jaulaPersonaId);
    }

    public JaulaPersonaDTO insertaJaulaPersona(Long personaId) {

        PersonaDTO personaDTO = personaService.getPersonaById(personaId);

        JaulaPersonaDTO jaulaPersonaDTO = new JaulaPersonaDTO(personaId);
        jaulaPersonaDTO = jaulaPersonaDAO.insert(jaulaPersonaDTO);

        try {
            MailMessage mensaje = new MailMessage("SE");

            mensaje.setTitle("Gestió aparca bicicletes");
            mensaje.setContentType("text/html; charset=UTF-8");
            String cuerpo;
            if (personaDTO.getPersonaUji().tieneTarjetaSalto()) {
                cuerpo = parametroService.getParametroGlobal("CORREOJAULASALTACONSALTO").getValor();
            } else {
                cuerpo = parametroService.getParametroGlobal("CORREOJAULASALTASINSALTO").getValor();
            }

            mensaje.setContent(cuerpo);
            mensaje.setReplyTo("noreply@uji.es");
            mensaje.setSender("Servei d'esports <noreply@uji.es>");
            mensaje.addToRecipient((ParamUtils.isNotNull(personaDTO.getMail()) ? personaDTO.getMail() : personaDTO.getPersonaUji().getMail()));
            MessagingClient cliente = new MessagingClient();
            cliente.send(mensaje);
        } catch (Exception e) {
            log.error("No se puede enviar un correo desde devel", e);
        }
        return jaulaPersonaDTO;
    }
}
