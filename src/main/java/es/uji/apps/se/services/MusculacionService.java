package es.uji.apps.se.services;

import es.uji.apps.se.dao.MonitorMusculacionDAO;
import es.uji.apps.se.dto.MonitorMusculacionDTO;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.exceptions.ParametroException;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.ui.UIMusculacion;
import es.uji.apps.se.model.domains.TipoParametro;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MusculacionService {

    private MonitorMusculacionDAO monitorMusculacionDAO;
    private ParametroService parametroService;
    private InstalacionService instalacionService;

    @Autowired
    public MusculacionService(MonitorMusculacionDAO monitorMusculacionDAO, ParametroService parametroService, InstalacionService instalacionService) {

        this.monitorMusculacionDAO = monitorMusculacionDAO;
        this.parametroService = parametroService;
        this.instalacionService = instalacionService;
    }

    public List<MonitorMusculacionDTO> getMonitores(Long connectedUserId) throws ParametroException {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        return monitorMusculacionDAO.getMonitores(cursoAcademico.getId());
    }

    public MonitorMusculacionDTO addMonitor(UIEntity entity) {
        MonitorMusculacionDTO monitorMusculacionDTO = uiToModel(entity);
        return monitorMusculacionDAO.insert(monitorMusculacionDTO);
    }


    public MonitorMusculacionDTO updateMonitor(UIEntity entity) {
        MonitorMusculacionDTO monitorMusculacionDTO = uiToModel(entity);
        return monitorMusculacionDAO.update(monitorMusculacionDTO);
    }

    private MonitorMusculacionDTO uiToModel(UIEntity entity) {
        MonitorMusculacionDTO monitorMusculacionDTO = entity.toModel(MonitorMusculacionDTO.class);
        monitorMusculacionDTO.setFechaAlta(entity.getDate("fechaAlta"));
        monitorMusculacionDTO.setFechaBaja(entity.getDate("fechaBaja"));
        return monitorMusculacionDTO;
    }

    public void deleteMonitorMusculacion(Long monitorMusculacionId) {
        monitorMusculacionDAO.delete(MonitorMusculacionDTO.class, monitorMusculacionId);
    }

    public void updateMusculacion(Long aforo, Long instalacionId, String horaInicio, String horaFin) {

        ParametroDTO parametroAforoDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONAFORO.getNombre());
        if (ParamUtils.isNotNull(aforo)) {
            parametroAforoDTO.setValor(aforo.toString());
            parametroService.updateParametro(parametroAforoDTO);
        } else {
            parametroService.deleteParametro(parametroAforoDTO.getId());
        }

        ParametroDTO parametroInstalacionDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONINSTALACION.getNombre());
        if (ParamUtils.isNotNull(instalacionId)) {
            parametroInstalacionDTO.setValor(instalacionId.toString());
            parametroService.updateParametro(parametroInstalacionDTO);
        } else {
            parametroService.deleteParametro(parametroInstalacionDTO.getId());
        }

        ParametroDTO parametroHoraInicioDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONHORAINICIO.getNombre());
        if (ParamUtils.isNotNull(horaInicio)) {
            parametroHoraInicioDTO.setValor(horaInicio);
            parametroService.updateParametro(parametroHoraInicioDTO);
        } else {
            parametroService.deleteParametro(parametroHoraInicioDTO.getId());
        }

        ParametroDTO parametroHoraFinDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONHORAFIN.getNombre());
        if (ParamUtils.isNotNull(horaFin)) {
            parametroHoraFinDTO.setValor(horaFin);
            parametroService.updateParametro(parametroHoraFinDTO);
        } else {
            parametroService.deleteParametro(parametroHoraFinDTO.getId());
        }
    }

    public UIEntity getDatosMusculacion() {
        UIMusculacion musculacion = new UIMusculacion();

        ParametroDTO parametroAforoDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONAFORO.getNombre());
        musculacion.setAforo(ParamUtils.parseLong(parametroAforoDTO.getValor()));

        ParametroDTO parametroInstalacionDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONINSTALACION.getNombre());
        musculacion.setInstalacion(ParamUtils.parseLong(parametroInstalacionDTO.getValor()));

        ParametroDTO parametroHoraInicioDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONHORAINICIO.getNombre());
        musculacion.setHoraInicio(parametroHoraInicioDTO.getValor());

        ParametroDTO parametroHoraFinDTO = parametroService.getParametroGlobal(TipoParametro.MUSCULACIONHORAFIN.getNombre());
        musculacion.setHoraFin(parametroHoraFinDTO.getValor());

        return UIEntity.toUI(musculacion);
    }
}
