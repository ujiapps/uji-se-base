package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.ParametroException;
import es.uji.apps.se.services.MusculacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("musculacion")
public class MusculacionResource extends CoreBaseService {

    @InjectParam
    MusculacionService musculacionService;

    @GET
    @Path("monitor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMonitores() throws ParametroException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(musculacionService.getMonitores(connectedUserId));
    }

    @POST
    @Path("monitor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addMonitor(UIEntity entity){
        return UIEntity.toUI(musculacionService.addMonitor(entity));
    }


    @GET
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity getDatosMusculacion(){
        return musculacionService.getDatosMusculacion();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void updateMusculacion(@FormParam("aforo") Long aforo, @FormParam("instalacion") Long instalacionId, @FormParam("horaInicio") String horaInicio, @FormParam("horaFin") String horaFin){
        musculacionService.updateMusculacion(aforo, instalacionId, horaInicio, horaFin);
    }

    @PUT
    @Path("monitor/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateMonitor(UIEntity entity){
        return UIEntity.toUI(musculacionService.updateMonitor(entity));
    }

    @DELETE
    @Path("monitor/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteMonitorMusculacion(@PathParam("id") Long monitorMusculacionId){
        musculacionService.deleteMonitorMusculacion(monitorMusculacionId);
    }

    @Path("monitor/{monitorMusculacionId}/musculacionFichajes")
    public MusculacionFichajeResource getMonitorMusculacionFichajeResource(
            @InjectParam MusculacionFichajeResource musculacionFichajeResource)
    {
        return musculacionFichajeResource;
    }
}
