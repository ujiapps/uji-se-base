package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.ActividadDAO;
import es.uji.apps.se.dto.ActividadDTO;
import es.uji.apps.se.dto.TipoReciboVWDTO;
import es.uji.apps.se.model.Oferta;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.model.filtros.FiltroActividades;
import es.uji.apps.se.utils.Csv;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ActividadService {

    @InjectParam
    Csv csv;

    private final ActividadDAO actividadDAO;

    @Autowired
    public ActividadService(ActividadDAO actividadDAO) {

        this.actividadDAO = actividadDAO;
    }

    public List<ActividadDTO> getActividades(FiltroActividades filtroActividades, Paginacion paginacion) {
        return actividadDAO.getActividades(filtroActividades, paginacion);
    }

    public List<Oferta> getActividadesEnvio(FiltroActividades filtroActividades) {
        return actividadDAO.getActividadesEnvio(filtroActividades);
    }

    public ActividadDTO updateActividad(UIEntity entity) {
        return actividadDAO.update(UIToModel(entity));
    }

    public ActividadDTO insertActividad(UIEntity entity) {
        return actividadDAO.insert(UIToModel(entity));
    }

    public void deleteActividad(Long actividadId){
        actividadDAO.delete(ActividadDTO.class, actividadId);
    }

    private ActividadDTO UIToModel(UIEntity entity){
        ActividadDTO actividadDTO = entity.toModel(ActividadDTO.class);

        if (ParamUtils.isNotNull(entity.get("tipoTarifaId"))){
            TipoDTO tipoTarifaDTO = new TipoDTO();
            tipoTarifaDTO.setId(ParamUtils.parseLong(entity.get("tipoTarifaId")));
            actividadDTO.setTipoTarifa(tipoTarifaDTO);
        }

        if (ParamUtils.isNotNull(entity.get("tipoReciboId"))){
            TipoReciboVWDTO tipoReciboVWDTO = new TipoReciboVWDTO();
            tipoReciboVWDTO.setId(ParamUtils.parseLong(entity.get("tipoReciboId")));
            actividadDTO.setTipoRecibo(tipoReciboVWDTO);
        }


        return actividadDTO;
    }

    public List<ActividadDTO> getActividadesCombobox() {
        return actividadDAO.getActividades(null, null);
    }

    public List<Oferta> getGruposActividad(FiltroActividades filtroActividades) {
        return actividadDAO.getGruposActividad(filtroActividades);
    }

    public Long getActividadesConProfesorado(Long cursoAcademico) {
        return actividadDAO.getActividadesConProfesorado(cursoAcademico);
    }

    public List<ActividadDTO> getActividadesCurso(Long cursoId) {
        return actividadDAO.getActividadesCurso(cursoId);
    }

    public List<Oferta> getGruposActividadCurso(Long actividadId, Long cursoId) {
        return actividadDAO.getGruposActividadCurso(actividadId, cursoId);
    }

    public List<ActividadDTO> getActividadesParaCsv(Long periodoId, Long clasificacionId, String busquedaNombre) {
        return actividadDAO.getActividadesParaCsv(periodoId, clasificacionId, busquedaNombre);
    }

    public Response getClasesParaExport(Long periodoId, Long clasificacionId, String busquedaNombre) {
        List<ActividadDTO> lista = getActividadesParaCsv(periodoId, clasificacionId, busquedaNombre);
        Response.ResponseBuilder res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = actividades.csv");
        res.entity(actividadDTOToCSV(lista));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    private String actividadDTOToCSV(List<ActividadDTO> lista) {
        ArrayList<String> cabeceras = new ArrayList<>(Arrays.asList("Nom", "Descripció", "Etiqueta", "Comentari agenda", "Tarifa",
                "Tipos rebut", "Activa", "Permet duplicitats", "Mostrar comentari assistències", "Places", "Edat Minima"));

        List<List<String>> records = new ArrayList<>();
        for (ActividadDTO actividad : lista) {
            List<String> recordMov = new ArrayList<>(creaListaString(actividad));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(ActividadDTO actividad) {
        return Arrays.asList(actividad.getNombre(),
                actividad.getDescripcion(),
                actividad.getEtiquetaInscripcionExterna(),
                actividad.getComentarioAgenda(),
                ParamUtils.isNotNull(actividad.getTipoTarifa())?actividad.getTipoTarifa().getNombre():"",
                ParamUtils.isNotNull(actividad.getTipoRecibo())?actividad.getTipoRecibo().getNombre():"",
                (actividad.getActiva())?"SI":"NO",
                (actividad.getPermiteDuplicidadInscripcion())?"SI":"NO",
                (actividad.getMostrarComentarioAsistencias())?"SI":"NO",
                ParamUtils.isNotNull(actividad.getPlazas())?actividad.getPlazas().toString():"",
                ParamUtils.isNotNull(actividad.getEdadMinimaInscripcion())?actividad.getEdadMinimaInscripcion().toString():"");
    }
}
