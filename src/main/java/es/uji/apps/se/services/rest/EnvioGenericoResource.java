package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.EnvioGenericoDTO;
import es.uji.apps.se.services.EnvioGenericoService;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("enviogenerico")
public class EnvioGenericoResource extends CoreBaseService {

    @InjectParam
    EnvioGenericoService envioGenericoService;

    @GET
    @Path("profelite")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getEnvioGenericoProfesoradoElite() {
        return UIEntity.toUI(envioGenericoService.getEnvioGenerico("PROF-ELITE"));
    }

    @GET
    @Path("decanos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getEnvioGenericoDecanosElite() {
        return UIEntity.toUI(envioGenericoService.getEnvioGenerico("DECANOS-ELITE"));
    }

    @POST
    @Path("profelite")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addEnvioGenericoProfesoradoElite(UIEntity entity) {

        return addEnvioGenerico(entity, "PROF-ELITE");
    }

    @POST
    @Path("decanos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addEnvioGenericoDecanosElite(UIEntity entity) {

        return addEnvioGenerico(entity, "DECANOS-ELITE");
    }

    private UIEntity addEnvioGenerico(UIEntity entity, String s) {
        if (ParamUtils.isNotNull(entity.get("id"))) {
            EnvioGenericoDTO envioGenericoDTO = entity.toModel(EnvioGenericoDTO.class);

            if (!ParamUtils.isNotNull(envioGenericoDTO.getAsunto()) && !ParamUtils.isNotNull(envioGenericoDTO.getCuerpo())) {
                envioGenericoService.deleteEnvioGenerico(envioGenericoDTO.getId());
                return UIEntity.toUI(new UIEntity());
            }

            envioGenericoDTO.setTipoEnvio(s);
            return UIEntity.toUI(envioGenericoService.updateEnvioGenerico(envioGenericoDTO));
        } else {
            EnvioGenericoDTO envioGenericoDTO = new EnvioGenericoDTO();
            envioGenericoDTO.setTipoEnvio(s);
            envioGenericoDTO.setAsunto(entity.get("asunto"));
            envioGenericoDTO.setCuerpo(entity.get("cuerpo"));
            return UIEntity.toUI(envioGenericoService.insertEnvioGenerico(envioGenericoDTO));
        }
    }

    @POST
    @Path("profelite/enviar")
    public void sendEnvioProfesoradoElite() throws MessageNotSentException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        envioGenericoService.sendEnvioProfesoradoElite(connectedUserId);
    }

    @POST
    @Path("profelite/enviarprueba")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void sendEnvioProfesoradoElitePrueba(@FormParam("profesorId") Long profesorId) throws MessageNotSentException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        envioGenericoService.sendEnvioProfesoradoElitePrueba(connectedUserId, profesorId);
    }

    @POST
    @Path("decanos/enviar")
    public void sendEnvioDecanosElite() throws MessageNotSentException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        envioGenericoService.sendEnvioDecanosElite(connectedUserId);
    }
}
