package es.uji.apps.se.services;

import es.uji.apps.se.dao.InstalacionDAO;
import es.uji.apps.se.dto.InstalacionDTO;
import es.uji.apps.se.model.Tipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstalacionService {

    private InstalacionDAO instalacionDAO;

    @Autowired
    public InstalacionService(InstalacionDAO instalacionDAO) {
        this.instalacionDAO = instalacionDAO;
    }

    public List<InstalacionDTO> getInstalaciones() {
        return instalacionDAO.getInstalaciones();
    }

    public List<Tipo> getUsuarioInstalaciones() {
        return instalacionDAO.getUsuarioInstalaciones();
    }

    public String getInstalacionPartido(Long partidoId){
        return instalacionDAO.getInstalacionPartido(partidoId);
    }
}
