package es.uji.apps.se.services;

import es.uji.apps.se.dao.ActividadTarifaDescuentoDAO;
import es.uji.apps.se.dto.ActividadTarifaDTO;
import es.uji.apps.se.dto.ActividadTarifaDescuentoDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActividadTarifaDescuentoService {

    private ActividadTarifaDescuentoDAO actividadTarifaDescuentoDAO;

    @Autowired
    public ActividadTarifaDescuentoService(ActividadTarifaDescuentoDAO actividadTarifaDescuentoDAO) {

        this.actividadTarifaDescuentoDAO = actividadTarifaDescuentoDAO;
    }
    public List<ActividadTarifaDescuentoDTO> getDescuentosTarifas(Long actividadTarifaId, Paginacion paginacion) {
        return actividadTarifaDescuentoDAO.getDescuentosTarifas(actividadTarifaId, paginacion);
    }


    public ActividadTarifaDescuentoDTO insertActividadTarifaDescuento(UIEntity entity, Long actividadTarifaId) {
        ActividadTarifaDescuentoDTO actividadTarifaDescuentoDTO = UIEntityToModel(entity, actividadTarifaId);
        return actividadTarifaDescuentoDAO.insert(actividadTarifaDescuentoDTO);
    }

    public ActividadTarifaDescuentoDTO updateActividadTarifaDescuento(UIEntity entity) {
        ActividadTarifaDescuentoDTO actividadTarifaDescuentoDTO = UIEntityToModel(entity, ParamUtils.parseLong(entity.get("actividadTarifaId")));
        return actividadTarifaDescuentoDAO.update(actividadTarifaDescuentoDTO);
    }

    private ActividadTarifaDescuentoDTO UIEntityToModel(UIEntity entity, Long actividadTarifaId) {
        ActividadTarifaDescuentoDTO actividadTarifaDescuentoDTO = entity.toModel(ActividadTarifaDescuentoDTO.class);

        ActividadTarifaDTO actividadTarifaDTO = new ActividadTarifaDTO();
        actividadTarifaDTO.setId(actividadTarifaId);
        actividadTarifaDescuentoDTO.setActividadTarifa(actividadTarifaDTO);

        TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO = new TarjetaDeportivaTipoDTO();
        tarjetaDeportivaTipoDTO.setId(ParamUtils.parseLong(entity.get("tarjetaDeportivaId")));
        actividadTarifaDescuentoDTO.setTarjetaDeportiva(tarjetaDeportivaTipoDTO);

        actividadTarifaDescuentoDTO.setFechaInicio(entity.getDate("fechaInicio"));

        if (ParamUtils.isNotNull(entity.get("fechaFin"))) {
            actividadTarifaDescuentoDTO.setFechaFin(entity.getDate("fechaFin"));
        }
        return actividadTarifaDescuentoDTO;
    }


    public void deleteActividadTarifaDescuento(Long actividadTarifaDescuentoId) {
        actividadTarifaDescuentoDAO.delete(ActividadTarifaDescuentoDTO.class, actividadTarifaDescuentoId);
    }
}
