package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.ActividadDTO;
import es.uji.apps.se.model.Oferta;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.filtros.FiltroActividades;
import es.uji.apps.se.services.ActividadService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("actividad")
public class ActividadResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(ActividadResource.class);

    @InjectParam
    ActividadService actividadService;

    @Path("{actividadId}/curso/{cursoId}/oferta")
    public OfertaResource getPlatformItem(
            @InjectParam OfertaResource ofertaResource) {
        return ofertaResource;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getActividades(@QueryParam("start") @DefaultValue("0") Long start,
                                          @QueryParam("limit") @DefaultValue("25") Long limit,
                                          @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                          @QueryParam("periodoId") Long periodoId, @QueryParam("clasificacionId") Long clasificacionId,
                                          @QueryParam("busquedaNombre") String busquedaNombre) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            FiltroActividades filtroActividades = new FiltroActividades(periodoId, clasificacionId, busquedaNombre);

            List<ActividadDTO> actividades = actividadService.getActividades(filtroActividades, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(actividades)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getActividades", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("{cursoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getActividadesCurso(@PathParam("cursoId") Long cursoId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<ActividadDTO> actividades = actividadService.getActividadesCurso(cursoId);
            responseMessage.setData((UIEntity.toUI(actividades)));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return responseMessage;
    }

    @GET
    @Path("{actividadId}/curso/{cursoId}/grupos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getGruposActividadCurso(@PathParam("actividadId") Long actividadId,
                                                   @PathParam("cursoId") Long cursoId){
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<Oferta> actividades = actividadService.getGruposActividadCurso(actividadId, cursoId);
            responseMessage.setData((UIEntity.toUI(actividades)));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return responseMessage;
    }

    @GET
    @Path("clasificacion/{clasificacionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActividadesFiltro(@PathParam("periodoId") Long periodoId, @PathParam("clasificacionId") Long clasificacionId) {
        FiltroActividades filtroActividades = new FiltroActividades(periodoId, clasificacionId, null);

        List<ActividadDTO> actividades = actividadService.getActividades(filtroActividades, null);
        return UIEntity.toUI(actividades);
    }


    @GET
    @Path("clasificacion/{clasificacionId}/actividad/{actividadId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActividadesFiltro(@PathParam("periodoId") Long periodoId, @PathParam("clasificacionId") Long clasificacionId, @PathParam("actividadId") Long actividadId) {
        FiltroActividades filtroActividades = new FiltroActividades(periodoId, clasificacionId, actividadId, null);

        List<Oferta> ofertas = actividadService.getGruposActividad(filtroActividades);
        return UIEntity.toUI(ofertas);
    }

    @GET
    @Path("combobox")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActividadesCombo() {

        List<ActividadDTO> actividades = actividadService.getActividadesCombobox();
        return UIEntity.toUI(actividades);
    }

    @GET
    @Path("export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClasesParaExport(@QueryParam("periodo") Long periodoId,
                                        @QueryParam("clasificacion") Long clasificacionId,
                                        @QueryParam("busquedaNombre") String busquedaNombre) {

        return actividadService.getClasesParaExport(periodoId, clasificacionId, busquedaNombre);
    }


    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertActividad(@FormParam("nombre") String nombre, @FormParam("descripcion") String descripcion, @FormParam("tipoTarifaId") Long tipoTarifaId,
                                    @FormParam("plazas") Long plazas, @FormParam("edadMinimaInscripcion") Long edadMinimaInscripcion, @FormParam("activa") Boolean activa,
                                    @FormParam("permiteDuplicidadInscripcion") Boolean permiteDuplicidadInscripcion, @FormParam("mostrarComentarioAsistencias") Boolean mostrarComentarioAsistencias,
                                    @FormParam("etiquetaInscripcionExterna") String etiquetaInscripcionExterna, @FormParam("comentarioAgenda") String comentarioAgenda, @FormParam("tipoReciboId") Long tipoReciboId) {

        UIEntity entity = new UIEntity();
        entity.put("nombre", nombre);
        entity.put("descripcion", descripcion);
        entity.put("tipoTarifaId", tipoTarifaId);
        entity.put("plazas", plazas);
        entity.put("edadMinimaInscripcion", edadMinimaInscripcion);
        entity.put("activa", activa);
        entity.put("permiteDuplicidadInscripcion", permiteDuplicidadInscripcion);
        entity.put("mostrarComentarioAsistencias", mostrarComentarioAsistencias);
        entity.put("etiquetaInscripcionExterna", etiquetaInscripcionExterna);
        entity.put("comentarioAgenda", comentarioAgenda);
        entity.put("tipoReciboId", tipoReciboId);

        return UIEntity.toUI(actividadService.insertActividad(entity));
    }

    @PUT
    @Path("{actividadId}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateActividad(@PathParam("actividadId") Long actividadId, @FormParam("nombre") String nombre, @FormParam("descripcion") String descripcion, @FormParam("tipoTarifaId") Long tipoTarifaId,
                                    @FormParam("plazas") Long plazas, @FormParam("edadMinimaInscripcion") Long edadMinimaInscripcion, @FormParam("activa") Boolean activa,
                                    @FormParam("permiteDuplicidadInscripcion") Boolean permiteDuplicidadInscripcion, @FormParam("mostrarComentarioAsistencias") Boolean mostrarComentarioAsistencias,
                                    @FormParam("etiquetaInscripcionExterna") String etiquetaInscripcionExterna, @FormParam("comentarioAgenda") String comentarioAgenda, @FormParam("tipoReciboId") Long tipoReciboId) {

        UIEntity entity = new UIEntity();
        entity.put("id", actividadId);
        entity.put("nombre", nombre);
        entity.put("descripcion", descripcion);
        entity.put("tipoTarifaId", tipoTarifaId);
        entity.put("plazas", plazas);
        entity.put("edadMinimaInscripcion", edadMinimaInscripcion);
        entity.put("activa", activa);
        entity.put("permiteDuplicidadInscripcion", permiteDuplicidadInscripcion);
        entity.put("mostrarComentarioAsistencias", mostrarComentarioAsistencias);
        entity.put("etiquetaInscripcionExterna", etiquetaInscripcionExterna);
        entity.put("comentarioAgenda", comentarioAgenda);
        entity.put("tipoReciboId", tipoReciboId);

        return UIEntity.toUI(actividadService.updateActividad(entity));
    }

    @DELETE
    @Path("{actividadId}")
    public void deleteActividad(@PathParam("actividadId") Long actividadId) {
        actividadService.deleteActividad(actividadId);
    }

    @GET
    @Path("/actividades-profesorado")
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity getActividadesConProfesorado(@QueryParam("cursoAcademico") Long cursoAcademico){
        UIEntity entity = new UIEntity();
        entity.put("actividadesConProfesorado", actividadService.getActividadesConProfesorado(cursoAcademico));
        return entity;
    }
}
