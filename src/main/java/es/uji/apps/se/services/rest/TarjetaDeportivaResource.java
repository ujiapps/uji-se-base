package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TarjetaDeportivaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.apps.se.model.ui.UITarjetaDeportivaAdmin;
import es.uji.apps.se.model.filtros.FiltroTarjetaDeportiva;
import es.uji.apps.se.dto.views.TarjetaDeportivaCSV;
import es.uji.apps.se.services.TarjetaDeportivaCsvService;
import es.uji.apps.se.services.TarjetaDeportivaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.*;

@Path("tarjetadeportiva")
public class TarjetaDeportivaResource extends CoreBaseService {

    private static final Logger log = LoggerFactory.getLogger(TarjetaDeportivaResource.class);

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;
    @InjectParam
    TarjetaDeportivaCsvService tarjetaDeportivaCsvService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTarjetasDeportivasByBusqueda(@QueryParam("query") String query, @QueryParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTipoId,
                                                           @QueryParam("fechaAltaDesde") String fechaAltaDesdeParam, @QueryParam("fechaAltaHasta") String fechaAltaHastaParam,
                                                           @QueryParam("fechaBajaDesde") String fechaBajaDesdeParam, @QueryParam("fechaBajaHasta") String fechaBajaHastaParam,
                                                           @QueryParam("searchSancionados") Long searchSancionados,
                                                           @QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                                           @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            FiltroTarjetaDeportiva filtro = new FiltroTarjetaDeportiva(query, tarjetaDeportivaTipoId, new UIDia(fechaAltaDesdeParam).getFecha(),
                    new UIDia(fechaAltaHastaParam).getFecha(), new UIDia(fechaBajaDesdeParam).getFecha(), new UIDia(fechaBajaHastaParam).getFecha(), searchSancionados);


            List<TarjetaDeportivaDTO> lista = tarjetaDeportivaService.getBySearch(filtro, paginacion);

            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(lista));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getTarjetasDeportivasByBusqueda", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("{personaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTarjetasDeportivasByPersona(@PathParam("personaId") Long personaId,
                                                          @QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                                          @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<TarjetaDeportivaDTO> lista = tarjetaDeportivaService.getTarjetaDeportivaByPersonaId(personaId, paginacion);

            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(lista));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getTarjetasDeportivasByPersona", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSVTarjetasDeportivasByBusqueda(@QueryParam("query") String query,
                                                       @QueryParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTipoId,
                                                       @QueryParam("fechaAltaDesde") String fechaAltaDesdeParam,
                                                       @QueryParam("fechaAltaHasta") String fechaAltaHastaParam,
                                                       @QueryParam("fechaBajaDesde") String fechaBajaDesdeParam,
                                                       @QueryParam("fechaBajaHasta") String fechaBajaHastaParam,
                                                       @QueryParam("searchSancionados") Long searchSancionados,
                                                       @QueryParam("personaId") Long personaId
    )
            throws ParseException {

        if (ParamUtils.isNotNull(personaId)) {
            List<TarjetaDeportivaCSV> lista = tarjetaDeportivaCsvService.getCsvByPersonaId(personaId);
            Response.ResponseBuilder res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = tarjetas.csv");
            res.entity(tarjetaDeportivaCsvService.entityTarjetaDeportivaToCSV(lista));
            res.type(MediaType.TEXT_PLAIN);
            return res.build();

        }
        FiltroTarjetaDeportiva filtro = new FiltroTarjetaDeportiva(query, tarjetaDeportivaTipoId, new UIDia(fechaAltaDesdeParam).getFecha(),
                new UIDia(fechaAltaHastaParam).getFecha(), new UIDia(fechaBajaDesdeParam).getFecha(), new UIDia(fechaBajaHastaParam).getFecha(), searchSancionados);

        List<TarjetaDeportivaCSV> lista = tarjetaDeportivaCsvService.getCsvBySearch(filtro);
        Response.ResponseBuilder res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = tarjetas.csv");
        res.entity(tarjetaDeportivaCsvService.entityTarjetaDeportivaToCSV(lista));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UITarjetaDeportivaAdmin insert(@FormParam("nombrePersona") Long personaId, @FormParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTipoId,
                                          @FormParam("fechaAlta") String fechaAlta, @FormParam("fechaBaja") String fechaBaja, @FormParam("comentarios") String comentarios) {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        return tarjetaDeportivaService.insert(personaId, tarjetaDeportivaTipoId, fechaAlta, fechaBaja, comentarios, conectedUserId);
    }

    @PUT
    @Path("{tarjetaDeportivaId}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTarjeta(@PathParam("tarjetaDeportivaId") Long tarjetaDeportivaId, @FormParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTipoId,
                                  @FormParam("fechaAlta") String fechaAlta, @FormParam("fechaBaja") String fechaBaja, @FormParam("comentarios") String comentarios) throws ParseException {
        Long conectedUserId = AccessManager.getConnectedUserId(request);

        TarjetaDeportivaDTO tarjetaDeportivaDTO = tarjetaDeportivaService.update(tarjetaDeportivaId, fechaAlta, fechaBaja, comentarios, conectedUserId);
        return UIEntity.toUI(tarjetaDeportivaDTO);
    }

    @DELETE
    @Path("{tarjetaDeportivaId}")
    public void delete(@PathParam("tarjetaDeportivaId") Long tarjetaDeportivaId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        tarjetaDeportivaService.delete(tarjetaDeportivaId, connectedUserId);
    }

    @Path("{tarjetaDeportivaId}/tarjetadeportivareserva")
    public TarjetaDeportivaReservaResource getTarjetaDeportivaTarifaResource(
            @InjectParam TarjetaDeportivaReservaResource tarjetaDeportivaReservaResource) {
        return tarjetaDeportivaReservaResource;
    }

    @Path("{tarjetaDeportivaId}/tarjetadeportivaclase")
    public TarjetaDeportivaClaseResource getTarjetaDeportivaClaseResource(
            @InjectParam TarjetaDeportivaClaseResource tarjetaDeportivaClaseResource) {
        return tarjetaDeportivaClaseResource;
    }

    @Path("{tarjetaDeportivaId}/historicocheck")
    public HistoricoCheckResource getHistoricoCheckResource(
            @InjectParam HistoricoCheckResource historicoCheckResource) {
        return historicoCheckResource;
    }
}
