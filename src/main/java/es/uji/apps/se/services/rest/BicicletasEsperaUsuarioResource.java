package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.BicicletaListaEspera;
import es.uji.apps.se.services.BicicletasEsperaUsuarioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("espera")
public class BicicletasEsperaUsuarioResource extends CoreBaseService {
    @InjectParam
    BicicletasEsperaUsuarioService bicicletasEsperaUsuarioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEsperaUsuario(@QueryParam("personaId") Long perId,
                                            @QueryParam("cursoAca") Long cursoAca) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            List<BicicletaListaEspera> list = new ArrayList<>();
            list.add(bicicletasEsperaUsuarioService.getEsperaUsuario(perId, cursoAca));
            responseMessage.setData(UIEntity.toUI(list));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Path("asignarbicicleta")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response asignarBicicletasEspera(UIEntity entity) throws CommonException {
        Long personaId = entity.getLong("personaId");
        Long reservaEspera = entity.getLong("reservaEspera");
        bicicletasEsperaUsuarioService.asignarBicicletasEspera(reservaEspera, personaId);
        return Response.ok().build();
    }

    @POST
    @Path("ponerlistaespera/{personaId}")
    public Response ponerListaEspera(@PathParam("personaId") Long perId) throws CommonException {
        bicicletasEsperaUsuarioService.ponerListaEspera(perId);
        return Response.ok().build();
    }
}
