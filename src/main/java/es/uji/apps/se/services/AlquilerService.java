package es.uji.apps.se.services;

import es.uji.apps.se.dao.AlquilerDAO;
import es.uji.apps.se.dto.AlquilerDTO;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.model.Alquiler;
import es.uji.apps.se.model.CursoAcademico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlquilerService {

    private AlquilerDAO alquilerDAO;
    private ParametroService parametroService;

    @Autowired
    public AlquilerService(AlquilerDAO alquilerDAO, ParametroService parametroService) {

        this.alquilerDAO = alquilerDAO;
        this.parametroService = parametroService;
    }

    public List<Alquiler> getAlquileres(Long connectUserId) {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectUserId);
        return alquilerDAO.getAlquileres(cursoAcademico);
    }
}
