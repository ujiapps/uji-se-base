package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionGeneroDAO;
import es.uji.apps.se.model.EquipacionGenero;
import es.uji.commons.rest.CoreBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipacionGeneroService extends CoreBaseService {

    @Autowired
    EquipacionGeneroDAO equipacionGeneroDAO;

    public List<EquipacionGenero> getGenerosEquipacion() {
        return equipacionGeneroDAO.getGenerosEquipacion();
    }
}
