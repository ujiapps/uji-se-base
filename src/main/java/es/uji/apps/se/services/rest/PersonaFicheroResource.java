package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.se.dto.PersonaFicheroDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.se.services.ADEClienteService;
import es.uji.apps.se.services.PersonaFicheroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;

public class PersonaFicheroResource extends CoreBaseService {
    @InjectParam
    ADEClienteService adeClienteService;

    @InjectParam
    PersonaFicheroService personaFicheroService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDocumentosPersona(@PathParam("id") Long personaId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            responseMessage.setData(UIEntity.toUI(personaFicheroService.getDocumentosPersona(personaId)));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseMessage addDocumentoPersona(@PathParam("id") Long personaId, FormDataMultiPart form) throws IOException, ErrorSubiendoDocumentoException {
        PersonaFicheroDTO ficheroDTO = new PersonaFicheroDTO();

        String mimeType = form.getBodyParts().get(0).getHeaders().getFirst("Content-Type");
        String nombre = form.getBodyParts().get(0).getContentDisposition().getFileName();

        BodyPartEntity fileV = (BodyPartEntity) form.getBodyParts().get(0).getEntity();
        InputStream documento = fileV.getInputStream();
        byte[] contenido = IOUtils.toByteArray(documento);

        ficheroDTO.setTipoFichero(mimeType);
        ficheroDTO.setNombreFichero(nombre);
        ficheroDTO.setPersonaUji(new PersonaUjiDTO(personaId));
        ficheroDTO.setReferencia(adeClienteService.addDocumento(nombre, mimeType, contenido));
        personaFicheroService.addDocumentoPersona(ficheroDTO);

        return new ResponseMessage(true);
    }

    @DELETE
    @Path("{docuId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteDocumentoPersona(@PathParam("docuId") Long docuId) {
        personaFicheroService.deleteDocumentoPersona(docuId);
        return Response.ok().build();
    }
}
