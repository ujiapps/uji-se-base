package es.uji.apps.se.services.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades.EquipacionesActividadesDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades.EquipacionesActividadesInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades.EquipacionesActividadesUpdateException;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.EquipacionActividadService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@Path("equipacion-actividad")
public class EquipacionActividadResource extends CoreBaseService {

    @InjectParam
    EquipacionActividadService equipacionActividadService;

    @GET
    @Path("/genericas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getActividadesGenericas(@QueryParam("start") @DefaultValue("0") Long start,
                                                   @QueryParam("limit") @DefaultValue("25") Long limit,
                                                   @QueryParam("filter") @DefaultValue("[]") String filterJson,
                                                   @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        List<Map<String, String>> filtros = null;
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            if (!filterJson.equals("[]")) {
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {});
            }

            List<UIEntity> actividadesGenericas = UIEntity.toUI(equipacionActividadService.getActividadesGenericas(paginacion, filtros));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(actividadesGenericas);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("/especificas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getActividadesEspecificas(@QueryParam("start") @DefaultValue("0") Long start,
            @QueryParam("limit") @DefaultValue("25") Long limit,
            @QueryParam("filter") @DefaultValue("[]") String filterJson,
            @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        List<Map<String, String>> filtros = null;
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            if (!filterJson.equals("[]")) {
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {});
            }

            Long userId = AccessManager.getConnectedUserId(request);
            List<UIEntity> actividadesEspecificas = UIEntity.toUI(equipacionActividadService.getActividadesEspecificas(userId, filtros, paginacion));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(actividadesEspecificas);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Path("/genericas")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEquipacionActividadGenerica(UIEntity entity) throws EquipacionesActividadesInsertException {
        equipacionActividadService.addEquipacionActividadGenerica(entity);
        return Response.ok().build();
    }

    @POST
    @Path("/especificas")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEquipacionActividadEspecifica(UIEntity entity) throws EquipacionesActividadesInsertException {
        Long userId = AccessManager.getConnectedUserId(request);
        equipacionActividadService.addEquipacionActividadEspecifica(entity, userId);
        return Response.ok().build();
    }


    @PUT
    @Path("/genericas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEquipacionActividadGenerica(UIEntity entity) throws EquipacionesActividadesUpdateException {
        equipacionActividadService.updateEquipacionActividadGenerica(entity);
        return Response.ok().build();
    }

    @PUT
    @Path("/especificas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEquipacionActividadEspecifica(UIEntity entity) throws EquipacionesActividadesUpdateException {
        Long userId = AccessManager.getConnectedUserId(request);
        equipacionActividadService.updateEquipacionActividadEspecifica(entity, userId);
        return Response.ok().build();
    }

    @DELETE
    @Path("/genericas/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEquipacionActividad(@PathParam("id") Long id) throws EquipacionesActividadesDeleteException {
        equipacionActividadService.deleteEquipacionActividad(id);
        return Response.ok().build();
    }

    @DELETE
    @Path("/especificas/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEquipacionActividadEspecifica(@PathParam("id") Long id) throws EquipacionesActividadesDeleteException {
        equipacionActividadService.deleteEquipacionActividad(id);
        return Response.ok().build();
    }
}
