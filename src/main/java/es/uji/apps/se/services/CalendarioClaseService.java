package es.uji.apps.se.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.BorradoClaseDirigidaException;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.se.dao.CalendarioClaseDAO;

@Service
public class CalendarioClaseService {

    @InjectParam
    private MonitorCalendarioService monitorCalendarioService;

    private final CalendarioClaseDAO calendarioClaseDAO;
    private final ParametroService parametroService;
    private final CalendarioService calendarioService;
    private final TarjetaDeportivaClaseService tarjetaDeportivaClaseService;
    private final CalendarioClaseInstalacionService calendarioClaseInstalacionService;

    @Autowired
    public CalendarioClaseService(CalendarioClaseDAO calendarioClaseDAO,
                                  ParametroService parametroService,
                                  CalendarioService calendarioService,
                                  TarjetaDeportivaClaseService tarjetaDeportivaClaseService,
                                  CalendarioClaseInstalacionService calendarioClaseInstalacionService) {
        this.calendarioClaseDAO = calendarioClaseDAO;
        this.parametroService = parametroService;
        this.calendarioService = calendarioService;
        this.tarjetaDeportivaClaseService = tarjetaDeportivaClaseService;
        this.calendarioClaseInstalacionService = calendarioClaseInstalacionService;
    }

    public List<CalendarioClaseDTO> getCalendariosClase(Long clase) {
        return calendarioClaseDAO.getCalendariosClase(clase);
    }

    public List<CalendarioClaseDTO> getCalendarioClasesByFechas(Date start, Date end, Long instalacionId) {
        return calendarioClaseDAO.getCalendarioClasesByFechas(start, end, instalacionId);
    }

    public List<CalendarioClaseDTO> getCalendariosClase(Long connectedUserId, Long clase, Paginacion paginacion, String columna, String orden) {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        return calendarioClaseDAO.getCalendariosClase(cursoAcademico, clase, paginacion, columna, orden);
    }

    public CalendarioClaseDTO updateCalendarioClase(Long calendarioClaseId, UIEntity entity) {
        CalendarioClaseDTO calendarioClaseDTO = entityToModel(calendarioClaseId, entity);
        calendarioClaseDAO.update(calendarioClaseDTO);

        calendarioClaseInstalacionService.deleteCalendarioClaseInstalacionByCalendarioClase(calendarioClaseDTO);

        if (ParamUtils.isNotNull(entity.get("instalacionId"))) {
            List<String> instalacionesId = entity.getArray("instalacionId");
            List<Long> instalacionIds = instalacionesId.stream().map(ParamUtils::parseLong).collect(Collectors.toList());
            instalacionIds.forEach(instalacionId -> {
                InstalacionDTO instalacion = new InstalacionDTO(instalacionId);
                CalendarioClaseInstalacionDTO calendarioClaseInstalacionDTO = new CalendarioClaseInstalacionDTO(calendarioClaseDTO, instalacion);
                calendarioClaseInstalacionService.insertCalendarioClaseInstalacion(calendarioClaseInstalacionDTO);
            });
        }

        return calendarioClaseDTO;
    }

    public void deleteCalendarioClase(Long calendarioClaseId) throws BorradoClaseDirigidaException {
        try {
            monitorCalendarioService.deleteMonitorByCalendarioClase(calendarioClaseId);
            calendarioClaseDAO.delete(CalendarioClaseDTO.class, calendarioClaseId);
        } catch (Exception e) {

            CalendarioClaseDTO calendarioClaseDTO = getCalendarioClaseById(calendarioClaseId);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String strDate = dateFormat.format(calendarioClaseDTO.getCalendario().getDia());

            throw new BorradoClaseDirigidaException("No se pot eliminar el dia del calendari " + strDate + " perque té asistencies o checks asignades.");
        }
    }

    private CalendarioClaseDTO getCalendarioClaseById(Long calendarioClaseId) {
        return calendarioClaseDAO.getCalendarioClaseById(calendarioClaseId);
    }

    public void insertClaseMultiple(Long claseId, String fechaInicio, String fechaFin, String horaInicio, String horaFin, Long diasFestivos, List<Integer> diasSemana, List<Long> arrayInstalacion) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDate fechaIni = LocalDate.parse(fechaInicio, formatter);
        LocalDate fechaF = LocalDate.parse(fechaFin, formatter).plusDays(1);


        for (LocalDate date = fechaIni; date.isBefore(fechaF); date = date.plusDays(1)) {
            if (diasSemana.contains(date.getDayOfWeek().getValue()) || !ParamUtils.isNotNull(diasSemana.get(0))) {
                CalendarioDTO calendarioDTO = calendarioService.getCalendarioByFecha(Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
                if (calendarioDTO.getEstado().equals("L") || diasFestivos.equals(1L)) {
                    CalendarioClaseDTO calendarioClaseDTO = new CalendarioClaseDTO();

                    ClaseDTO claseDTO = new ClaseDTO();
                    claseDTO.setId(claseId);
                    calendarioClaseDTO.setClase(claseDTO);
                    calendarioClaseDTO.setCalendario(calendarioDTO);
                    calendarioClaseDTO.setHoraFin(horaFin);
                    calendarioClaseDTO.setHoraInicio(horaInicio);
                    calendarioClaseDTO = calendarioClaseDAO.insert(calendarioClaseDTO);

                    for (Long instalacionId : arrayInstalacion) {

                        //Guardar la instalación en la nueva tabla calendarioClasesDirigidasInstalaciones
                        InstalacionDTO instalacionDTO = new InstalacionDTO(instalacionId);
                        CalendarioClaseInstalacionDTO calendarioClaseInstalacionDTO = new CalendarioClaseInstalacionDTO(calendarioClaseDTO, instalacionDTO);
                        calendarioClaseInstalacionService.insertCalendarioClaseInstalacion(calendarioClaseInstalacionDTO);
                    }
                }
            }
        }
    }


    public void deleteClaseMultiple(Long claseId, String fechaInicio, String fechaFin, String horaInicio, String horaFin, List<Integer> diasSemana) throws BorradoClaseDirigidaException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDate fechaIni = LocalDate.parse(fechaInicio, formatter);
        LocalDate fechaF = LocalDate.parse(fechaFin, formatter).plusDays(1);
        String horaI = horaInicio;
        String horaF = horaFin;

        for (LocalDate date = fechaIni; date.isBefore(fechaF); date = date.plusDays(1)) {
            if (diasSemana.contains(date.getDayOfWeek().getValue()) || !ParamUtils.isNotNull(diasSemana.get(0))) {
                List<CalendarioClaseDTO> calendariosClase = calendarioClaseDAO.getCalendarioClaseByClaseIdAndFecha(claseId, Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
                for (CalendarioClaseDTO calendarioClaseDTO : calendariosClase) {
                    if (!ParamUtils.isNotNull(horaInicio)) horaI = calendarioClaseDTO.getHoraInicio();
                    if (!ParamUtils.isNotNull(horaFin)) horaF = calendarioClaseDTO.getHoraFin();
                    if (calendarioClaseDTO.getHoraInicio().equals(horaI) && calendarioClaseDTO.getHoraFin().equals(horaF)) {
                        tarjetaDeportivaClaseService.deleteTarjetaDeportivaClasesByCalendarioId(calendarioClaseDTO.getId());
                        calendarioClaseInstalacionService.deleteCalendarioClaseInstalacionByCalendarioClase(calendarioClaseDTO);
                        deleteCalendarioClase(calendarioClaseDTO.getId());
                    }
                }
            }
        }

    }

    public List<CalendarioClaseVistaDTO> getProximasClases(Long connectedUserId, int dias) {
        Date fechaInicial = DateExtensions.getCurrentDate();
        Date fechaFinal = DateExtensions.addDays(fechaInicial, dias);
        return calendarioClaseDAO.getCalendariosClaseByRangoFechas(fechaInicial, fechaFinal, connectedUserId);
    }

    private CalendarioClaseDTO entityToModel(Long calendarioClaseId, UIEntity entity) {
        CalendarioClaseDTO calendarioClaseDTO = entity.toModel(CalendarioClaseDTO.class);
        CalendarioDTO calendarioDTO = calendarioService.getCalendarioByFecha(entity.getDate("fecha"));

        if (ParamUtils.isNotNull(calendarioDTO)) {
            calendarioClaseDTO.setCalendario(calendarioDTO);

            ClaseDTO claseDTO = new ClaseDTO();
            claseDTO.setId(ParamUtils.parseLong(entity.get("claseId")));
            calendarioClaseDTO.setClase(claseDTO);

            calendarioClaseDTO.setId(calendarioClaseId);
            return calendarioClaseDTO;
        } else return null;
    }

    public void deleteCalendarioClaseByClaseId(Long claseId) throws BorradoClaseDirigidaException {
        List<CalendarioClaseDTO> calendarioClasesDTO = getCalendariosClase(claseId);
        for (CalendarioClaseDTO calendarioClaseDTO : calendarioClasesDTO) {
            deleteCalendarioClase(calendarioClaseDTO.getId());
        }
    }

    public List<CalendarioClaseVistaDTO> getCalendariosClaseByFecha(Date fecha, Long personaId) {
        return calendarioClaseDAO.getCalendariosClaseByFecha(fecha, personaId);
    }
}
