package es.uji.apps.se.services.rest.app;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TarjetaDeportivaClaseDTO;
import es.uji.apps.se.exceptions.ReservaException;
import es.uji.apps.se.model.ReservaClasesDirigidas;
import es.uji.apps.se.model.Sancion;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.services.*;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class AppReservasResource extends CoreBaseService {

    @InjectParam
    PersonaSancionService personaSancionService;

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;

    @InjectParam
    ReservasService reservasService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template listadoClasesFuturas(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Sancion sancion = personaSancionService.getSancionBloqueaTodoOrClasesDirigidas(connectedUserId);
        TarjetaDeportiva tarjetaDeportiva = tarjetaDeportivaService.getTarjetaDeportivaActivaByPersonaId(connectedUserId);

        Template template = AppInfo.buildPaginaAplicacionClases("se/app/reservas", idioma);

        anyadirSancionReservaAlTemplate(sancion, template);

        template.put("tarjetaDeportiva", tarjetaDeportiva);
        template.put("titulo", "Llistat de reserves");
        List<ReservaClasesDirigidas> reservas = reservasService.getReservasClasesDirigidasFuturasByPersonaId(connectedUserId);
        template.put("reservas", reservas);

        return template;
    }

    @DELETE
    @Path("{reservaId}")
    public ResponseMessage borrarClase(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("reservaId") Long reservaId)
            throws ReservaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reservasService.borrarReservaClaseDirigida(reservaId, connectedUserId);
        return new ResponseMessage(true);
    }

    @POST
    public ResponseMessage anyadirClase(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, UIEntity uiEntity)
            throws ReservaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long actividadId = uiEntity.getLong("actividadId");
        List<TarjetaDeportivaClaseDTO> tarjetaDeportivaClaseDTO = new ArrayList<>();
        tarjetaDeportivaClaseDTO.add(reservasService.addReservaClaseDirigida(actividadId, connectedUserId, connectedUserId));

        ResponseMessage responseMessage = new ResponseMessage(true);

        responseMessage.setData(UIEntity.toUI(tarjetaDeportivaClaseDTO));

        return responseMessage;
    }

    private void anyadirSancionReservaAlTemplate(Sancion sancion, Template template) {
        if (ParamUtils.isNotNull(sancion)) {
            if (sancion.getTipo().equals("CLA") || sancion.getTipo().equals("ALL")) {
                String motivo = ParamUtils.isNotNull(sancion.getMotivo())? sancion.getMotivo() : "No s'ha especificat el motiu";
                if (ParamUtils.isNotNull(sancion.getFechaFin())) {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins al dia {0}. Motiu: {1}", DateExtensions.getDateAsString(sancion.getFechaFin()), motivo));
                } else {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins que resolgues la incidència: {0}", motivo));
                }
            } else
                template.put("mensajeInfo", null);
        } else {
            template.put("mensajeInfo", null);
        }

        template.put("classSancion", "");
    }
}
