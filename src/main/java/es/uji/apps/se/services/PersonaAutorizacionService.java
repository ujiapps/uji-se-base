package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.model.*;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PersonaAutorizacionService {
    private PersonaAutorizacionDAO personaAutorizacionDAO;

    public PersonaAutorizacionService(PersonaAutorizacionDAO personaAutorizacionDAO) {
        this.personaAutorizacionDAO = personaAutorizacionDAO;
    }

    public List<Autorizacion> getAutorizaciones(Paginacion paginacion, Long personaId) {
        return personaAutorizacionDAO.getAutorizaciones(paginacion, personaId);
    }
}
