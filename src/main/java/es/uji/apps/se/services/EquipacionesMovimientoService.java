package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.EquipacionesMovimientoDAO;
import es.uji.apps.se.dto.EquipacionesMovimientoDTO;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosDeleteException;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosGetException;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosInsertException;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosUpdateException;
import es.uji.apps.se.model.EquipacionesMovimiento;
import es.uji.apps.se.model.EquipacionesMovimientoVW;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EquipacionesMovimientoService {

    @Autowired
    EquipacionesMovimientoDAO equipacionesMovimientoDAO;

    @InjectParam
    EquipacionesStockService equipacionesStockService;

    @InjectParam
    EquipacionTipoMovimientoService equipacionTipoMovimientoService;

    public List<EquipacionesMovimientoVW> getEquipacionesMovimientos(Paginacion paginacion, List<Map<String, String>> filtros) throws EquipacionesMovimientosGetException {
        return equipacionesMovimientoDAO.getEquipacionesMovimientos(paginacion, filtros);
    }

    public void addEquipacionMovimiento(UIEntity entity) throws EquipacionesMovimientosInsertException {
        try {
            EquipacionesMovimiento equipacionesMovimiento = new EquipacionesMovimiento(entity);
            if (!equipacionTipoMovimientoService.isAltaOrDevolucion(equipacionesMovimiento.getMovimientoId())) {
                Long stock = equipacionesStockService.getStockDeEquipacion(equipacionesMovimiento.getEquipacionId(), equipacionesMovimiento.getTalla(), equipacionesMovimiento.getDorsal()) - equipacionesMovimiento.getCantidad();
                if (stock < 0L)
                    throw new EquipacionesMovimientosInsertException("No hi ha stock suficient per a realitzar aquest moviment");
            }
            equipacionesMovimientoDAO.insert(new EquipacionesMovimientoDTO(equipacionesMovimiento));
        } catch (EquipacionesMovimientosInsertException e) {
            throw new EquipacionesMovimientosInsertException(e.getMessage());
        } catch (Exception e) {
            throw new EquipacionesMovimientosInsertException();
        }
    }

    public void updateEquipacionMovimiento(UIEntity entity) throws EquipacionesMovimientosUpdateException {
        try {
            EquipacionesMovimiento equipacionesMovimiento = new EquipacionesMovimiento(entity);
            if (!equipacionTipoMovimientoService.isAltaOrDevolucion(equipacionesMovimiento.getMovimientoId())) {
                Long stock = equipacionesStockService.getStockDeEquipacion(equipacionesMovimiento.getEquipacionId(), equipacionesMovimiento.getTalla(), equipacionesMovimiento.getDorsal()) - equipacionesMovimiento.getCantidad();
                if (stock < 0L)
                    throw new EquipacionesMovimientosInsertException("No hi ha stock suficient per a actualitzar aquest moviment");
            }
            equipacionesMovimientoDAO.update(new EquipacionesMovimientoDTO(equipacionesMovimiento));
        } catch (EquipacionesMovimientosInsertException e) {
            throw new EquipacionesMovimientosUpdateException(e.getMessage());
        } catch (Exception e) {
            throw new EquipacionesMovimientosUpdateException();
        }
    }

    public void deleteEquipacionMovimiento(Long id) throws EquipacionesMovimientosDeleteException {
        try {
            equipacionesMovimientoDAO.delete(EquipacionesMovimientoDTO.class, id);
        } catch (Exception e) {
            throw new EquipacionesMovimientosDeleteException();
        }
    }
}