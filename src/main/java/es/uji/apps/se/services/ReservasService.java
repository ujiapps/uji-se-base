package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.NoAutorizadoException;
import es.uji.apps.se.exceptions.ReservaException;
import es.uji.apps.se.model.ReservaClasesDirigidas;
import es.uji.apps.se.model.ui.*;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static es.uji.apps.se.model.domains.TipoParametro.*;

@Service
public class ReservasService {

    @Autowired
    protected CalendarioClaseDAO calendarioClaseDAO;

    @Autowired
    protected CalendarioDAO calendarioDAO;

    @Autowired
    protected TarjetaDeportivaDAO tarjetaDeportivaDAO;

    @Autowired
    protected TarjetaDeportivaClaseDAO tarjetaDeportivaClaseDAO;

    @Autowired
    protected PersonaUjiService personaUjiService;

    @Autowired
    protected PersonaService personaService;

    @Autowired
    protected AutorizadoAppDAO autorizadoAppDAO;

    @Autowired
    protected CheckDAO checkDAO;

    @Autowired
    protected ParametroDAO parametroDAO;

    private List<UIFranjaHoraria> franjaHorarias;

    public ReservasService() {
        franjaHorarias = new ArrayList<>();
        franjaHorarias.add(new UIFranjaHoraria(1L, "7:00", "9:00"));
        franjaHorarias.add(new UIFranjaHoraria(2L, "9:00", "13:30"));
        franjaHorarias.add(new UIFranjaHoraria(3L, "13:30", "17:30"));
        franjaHorarias.add(new UIFranjaHoraria(4L, "17:30", "21:00"));
        franjaHorarias.add(new UIFranjaHoraria(5L, "21:00", "23:00"));
    }

    public List<ReservaClasesDirigidas> getReservasClasesDirigidasFuturasByPersonaId(Long personaId) {
        List<ReservaClasesDirigidas> clases = tarjetaDeportivaClaseDAO.getReservaClasesDirigidasFuturasByPersonaId(personaId);
        return clases.stream()
                .sorted(Comparator.comparing(ReservaClasesDirigidas::getHoraInicio))
                .collect(Collectors.toList());
    }

    public List<ReservaClasesDirigidas> getClasesPasadasByPersonaId(Long personaId) {
        List<ReservaClasesDirigidas> clases = tarjetaDeportivaClaseDAO.getReservaClasesDirigidasPasadasByPersonaId(personaId);
        return clases.stream().filter(c -> c.getFechaHoraInicio().before(new Date()))
                .sorted(Comparator.comparing(ReservaClasesDirigidas::getFechaHoraInicio))
                .collect(Collectors.toList());
    }

    public Map<Integer, Map<Integer, List<LocalDate>>> getDiasPuedeReservarUsuario() {
        Long numeroDias = getDiasAntelacionReserva();

        return calendarioDAO.getDiasNoFestivos(DateExtensions.getCurrentDate(), numeroDias).stream().map(calendario -> calendario.getDia().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .collect(Collectors.groupingBy(LocalDate::getMonthValue, Collectors.groupingBy(LocalDate::getDayOfMonth)));

    }

    private Long getDiasAntelacionReserva() {
        ParametroDTO parametroDTO = parametroDAO.getParametroGlobal(MAXRESERVASDIA.getNombre());
        return Long.parseLong(parametroDTO.getValor());
    }

    public List<UIFranjaHoraria> getFranjasHorarias() {
        return franjaHorarias;
    }

    public List<UIEvento> getActividadesCalendario(Long personaId, Date desde, Date hasta) {
        Date primeraFechaLimiteReserva = calendarioDAO.getPrimeraFechaLimiteReserva(getDiasAntelacionReserva());
        int minutosLimiteReserva = Integer.parseInt(parametroDAO.getParametroGlobal(MINUTOSLIMITERESERVA.getNombre()).getValor());
        int minutosLimiteAnulacion = Integer.parseInt(parametroDAO.getParametroGlobal(MINUTOSLIMITEANULACION.getNombre()).getValor());

        List<CalendarioClaseVistaDTO> calendarioClases = calendarioClaseDAO.getCalendariosClaseByRangoFechas(desde, hasta, personaId)
                .stream()
                .sorted(Comparator.comparing(CalendarioClaseVistaDTO::getFechaHoraInicio))
                .collect(
                Collectors.toList());

        return calendarioClases.stream().map(cal -> new UIEvento(cal, primeraFechaLimiteReserva, minutosLimiteReserva, minutosLimiteAnulacion)).collect(
                Collectors.toList());
    }

    public void borrarReservaClaseDirigida(Long reservaId, Long personaId) throws ReservaException {
        TarjetaDeportivaClaseDTO clase = tarjetaDeportivaClaseDAO.getClaseById(reservaId);
        TarjetaDeportivaDTO tarjetaDeportivaDTO = tarjetaDeportivaDAO.getTarjetaDeportivaDTOActivaByPersonaId(personaId);

        compruebaSePuedeBorrarReservaClaseDirigida(clase, tarjetaDeportivaDTO);

        tarjetaDeportivaClaseDAO.delete(TarjetaDeportivaClaseDTO.class, reservaId);
    }

    private void compruebaSePuedeBorrarReservaClaseDirigida(TarjetaDeportivaClaseDTO clase,
                                                            TarjetaDeportivaDTO tarjetaDeportivaDTO) throws ReservaException {

        PersonaDTO personaDTO = personaService.getPersonaById(tarjetaDeportivaDTO.getPersonaUji().getId());
        SancionVistaDTO sancion = personaDTO.getPersonaUji().getSancionActiva();

        ParametroDTO parametroDTO = parametroDAO.getParametroGlobal(TIEMPOPREVIO.getNombre());
        int minutosLimiteAnulacion = Integer.parseInt(parametroDAO.getParametroGlobal(MINUTOSLIMITEANULACION.getNombre()).getValor());

        Date currentDate = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);

        calendar.add(Calendar.MINUTE, Integer.parseInt(parametroDTO.getValor()));
        Date nuevaFecha = calendar.getTime();

        if (tarjetaDeportivaDTO == null) {
            throw new ReservaException("No tens cap targeta activa");
        }

        if (clase == null) {
            throw new ReservaException("No s'ha trovat la reserva");
        }

        if (!clase.getTarjetaDeportiva().getPersonaUji().getId().equals(tarjetaDeportivaDTO.getPersonaUji().getId())) {
            throw new ReservaException("No s'ha pogut esborrar la reserva");
        }


        if (clase.getCalendarioClase().getUltimaFechaLimiteAnulacion(minutosLimiteAnulacion).before(nuevaFecha) || (ParamUtils.isNotNull(sancion) && clase.getCalendarioClase().getFechaHoraInicio().before(new Date()))) {
            throw new ReservaException("No pots anul·lar la reserva perquè ha passat la data límit d''anul·lació");
        }

        //Que no deje anular si ya tiene fichaje realizado para esta reserva
        if (clase.isAsiste() || checkDAO.existeCheckTarjetaDeportivaClase(clase)) {
            throw new ReservaException("No pots anul·lar la reserva perquè ja s'ha marcar l'assitència");
        }
    }

    public TarjetaDeportivaClaseDTO addReservaClaseDirigida(Long actividadId, Long personaId, Long connectUserId)
            throws ReservaException {
        CalendarioClaseDTO calendarioClaseDTO = calendarioClaseDAO.getCalendariosClaseById(actividadId);
        TarjetaDeportivaDTO tarjetaDeportivaDTO = tarjetaDeportivaDAO.getTarjetaDeportivaActivaByPersonaIdAndFecha(personaId, calendarioClaseDTO.getCalendario().getDia());

        compruebaEsReservable(actividadId, personaId, tarjetaDeportivaDTO, calendarioClaseDTO);

        TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO = new TarjetaDeportivaClaseDTO();
        tarjetaDeportivaClaseDTO.setTarjetaDeportiva(tarjetaDeportivaDTO);
        tarjetaDeportivaClaseDTO.setCalendarioClase(calendarioClaseDTO);
        tarjetaDeportivaClaseDTO.setAsiste(false);
        tarjetaDeportivaClaseDTO.setPersonaAccion(connectUserId);

        return tarjetaDeportivaClaseDAO.insert(tarjetaDeportivaClaseDTO);
    }

    private void compruebaEsReservable(Long actividadId, Long personaId,
                                       TarjetaDeportivaDTO tarjetaDeportivaDTO, CalendarioClaseDTO calendarioClaseDTO)
            throws ReservaException {
        if (tarjetaDeportivaDTO == null) {
            throw new ReservaException("No pots reservar perquè no tens cap targeta esportiva activa");
        }

        if (!tarjetaDeportivaDTO.getTarjetaDeportivaTipo().isPermiteClasesDirigidas()) {
            throw new ReservaException("No pots reservar perquè la targeta esportiva no permet reserva en classes dirigides");
        }

        if (calendarioClaseDTO.getCalendario().getDia().after(tarjetaDeportivaDTO.getFechaBaja())) {
            throw new ReservaException("No pots reservar perquè l'activitat s'imparteix en una data on la teva targeta esportiva no està vigent");
        }

        TarjetaDeportivaClaseDTO tarjetaDeportivaClaseExistenteDTO = tarjetaDeportivaDTO.getTarjetaDeportivaClases().stream().filter(tdc -> tdc.getCalendarioClase().getId().equals(actividadId)).findFirst().orElse(null);
        if (tarjetaDeportivaClaseExistenteDTO != null) {
            throw new ReservaException("Ja estàs apuntat a aquesta activitat");
        }

        PersonaUjiDTO personaUjiDTO = personaUjiService.getPersonaById(personaId);
        SancionVistaDTO sancion = personaUjiDTO.getSancionActiva();
        if (sancion != null) {
            throw new ReservaException(MessageFormat.format("No pots reservar fins al dia {0} motiu: {1}", DateExtensions.getDateAsString(sancion.getFechaFin()), sancion.getMotivo()));
        }

        Long diasAntelacion = getDiasAntelacionReserva();
        Date fechaLimiteReserva = calendarioDAO.getPrimeraFechaLimiteReserva(diasAntelacion);
        if (calendarioClaseDTO.getFechaHoraInicio().after(fechaLimiteReserva)) {
            throw new ReservaException("No pots fer la reserva amb aquesta antelació");
        }

        int minutosLimiteReserva = Integer.parseInt(parametroDAO.getParametroGlobal(MINUTOSLIMITERESERVA.getNombre()).getValor());

        if (calendarioClaseDTO.getUltimaFechaLimiteReserva(minutosLimiteReserva).before(new Date())) {
            throw new ReservaException(MessageFormat.format("No pots reservar l''activitat perquè han passat mes de {0} minuts del seu començament", minutosLimiteReserva));
        }

        if (!calendarioClaseDTO.quedanPlazasLibres()) {
            throw new ReservaException("No pots reservar perquè no queden places lliures");
        }

        CalendarioClaseDTO calendarioClaseSolapeDTO = tarjetaDeportivaDTO.getCalendarioClaseSolapado(calendarioClaseDTO);
        if (calendarioClaseSolapeDTO != null) {
            throw new ReservaException(MessageFormat.format("No pots reservar aquesta activitat perquè coincideixen en horari amb l''activitat {0} {1}-{2}",
                    calendarioClaseSolapeDTO.getClase().getNombre(), calendarioClaseSolapeDTO.getHoraInicio(), calendarioClaseSolapeDTO.getHoraFin()));
        }

        Long numeroReservasDia = tarjetaDeportivaDTO.getTarjetaDeportivaClases().stream().filter(tdc -> tdc.getCalendarioClase().getCalendario().getDia().equals(calendarioClaseDTO.getCalendario().getDia())).count();
         ParametroDTO parametroDTO = parametroDAO.getParametroGlobal(MAXRESERVASDIARIAS.getNombre());
        if (numeroReservasDia >= Long.parseLong(parametroDTO.getValor())) {
            throw new ReservaException(MessageFormat.format("No pots reservar aquesta activitat perquè ja tens {0} reserves el dia {1}", numeroReservasDia, DateExtensions.getDateAsString(calendarioClaseDTO.getCalendario().getDia())));
        }
    }

    public void checkPuedeAcceder(Long personaId) throws NoAutorizadoException {
        AutorizadoAppDTO autorizadoAppDTO = autorizadoAppDAO.getAutorizadoById(personaId);
        if (autorizadoAppDTO == null) {
            throw new NoAutorizadoException("No tens permís per accedir a l'aplicació");
        }
    }
}
