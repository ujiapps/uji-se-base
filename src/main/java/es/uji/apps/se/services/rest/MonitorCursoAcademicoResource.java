package es.uji.apps.se.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.se.dto.MonitorCursoAcademicoDTO;
import es.uji.apps.se.services.MonitorCursoAcademicoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("monitorcursoacademico")
public class MonitorCursoAcademicoResource extends CoreBaseService {
    @InjectParam
    MonitorCursoAcademicoService monitorCursoAcademicoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMonitores() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<MonitorCursoAcademicoDTO> listaMonitores = monitorCursoAcademicoService.getMonitoresCursoAcademico(connectedUserId);
        if (listaMonitores.size() > 0)
            return modelToUI(listaMonitores);
        else return new ArrayList<UIEntity>();
    }

    private UIEntity modelToUI(MonitorCursoAcademicoDTO monitorCursoAcademicoDTO) {
        UIEntity entity = UIEntity.toUI(monitorCursoAcademicoDTO);
        if (ParamUtils.isNotNull(monitorCursoAcademicoDTO.getMonitor().getPersonaUji().getNombre())) {
            entity.put("nombre", monitorCursoAcademicoDTO.getMonitor().getPersonaUji().getApellidosNombre());
        }
        return entity;
    }

    private List<UIEntity> modelToUI(List<MonitorCursoAcademicoDTO> monitoresCursoAcademico) {
        List<UIEntity> lista = new ArrayList<UIEntity>();
        for (MonitorCursoAcademicoDTO monitorCursoAcademicoDTO : monitoresCursoAcademico) {
            lista.add(modelToUI(monitorCursoAcademicoDTO));
        }
        return lista;
    }
}
