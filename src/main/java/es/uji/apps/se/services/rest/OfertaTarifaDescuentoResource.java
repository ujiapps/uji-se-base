package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.OfertaTarifaDescuentoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.OfertaTarifaDescuentoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("ofertadescuento")
public class OfertaTarifaDescuentoResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(OfertaTarifaDescuentoResource.class);

    @InjectParam
    OfertaTarifaDescuentoService ofertaTarifaDescuentoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDescuentosTarifas(@PathParam("ofertaId") Long ofertaId, @PathParam("ofertaTarifaId") Long ofertaTarifaId,
                                         @QueryParam("start") @DefaultValue("0") Long start,
                                         @QueryParam("limit") @DefaultValue("25") Long limit,
                                         @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<OfertaTarifaDescuentoDTO> tarifasDescuento = ofertaTarifaDescuentoService.getDescuentosTarifas(ofertaTarifaId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(tarifasDescuento)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getDescuentosTarifas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertOfertaTarifaDescuento (UIEntity entity, @PathParam("ofertaTarifaId") Long ofertaTarifaId){
        return UIEntity.toUI(ofertaTarifaDescuentoService.insertOfertaTarifaDescuento(entity, ofertaTarifaId));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateOfertaTarifaDescuento (UIEntity entity){
        return UIEntity.toUI(ofertaTarifaDescuentoService.updateOfertaTarifaDescuento(entity));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteOfertaTarifaDescuento (@PathParam("id") Long ofertaTarifaDescuentoId){
        ofertaTarifaDescuentoService.deleteOfertaTarifaDescuento(ofertaTarifaDescuentoId);
    }
}
