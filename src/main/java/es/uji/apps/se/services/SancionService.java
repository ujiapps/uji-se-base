package es.uji.apps.se.services;

import es.uji.apps.se.dao.SancionNivelDAO;
import es.uji.apps.se.dao.SancionFaltaDAO;
import es.uji.apps.se.dao.SancionPeriodoDAO;
import es.uji.apps.se.dto.SancionFaltaDTO;
import es.uji.apps.se.dto.SancionPeriodoDTO;
import es.uji.apps.se.model.NivelSancion;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SancionService {

    private SancionPeriodoDAO sancionPeriodoDAO;
    private SancionFaltaDAO sancionFaltaDAO;
    private SancionNivelDAO sancionNivelDAO;

    @Autowired
    public SancionService(SancionPeriodoDAO sancionPeriodoDAO, SancionFaltaDAO sancionFaltaDAO, SancionNivelDAO sancionNivelDAO) {
        this.sancionPeriodoDAO = sancionPeriodoDAO;
        this.sancionFaltaDAO = sancionFaltaDAO;
        this.sancionNivelDAO = sancionNivelDAO;
    }

    public List<SancionFaltaDTO> getFaltas(Paginacion paginacion) {
        return sancionFaltaDAO.getFaltas(paginacion);
    }

    public List<SancionPeriodoDTO> getPeriodos(Paginacion paginacion) {
        return sancionPeriodoDAO.getPeriodos(paginacion);
    }

    public SancionPeriodoDTO addSancionPeriodo(UIEntity entity) {

        SancionPeriodoDTO sancionPeriodoDTO = uiToModelSancionPeriodo(null, ParamUtils.parseLong(entity.get("diaInicio")),
                ParamUtils.parseLong(entity.get("mesInicio")), ParamUtils.parseLong(entity.get("diaFin")),
                ParamUtils.parseLong(entity.get("mesFin")), Boolean.parseBoolean(entity.get("activo")));
        return sancionPeriodoDAO.insert(sancionPeriodoDTO);
    }

    public SancionPeriodoDTO updateSancionPeriodo(Long sancionPeriodoId, UIEntity entity) {

        SancionPeriodoDTO sancionPeriodoDTO = uiToModelSancionPeriodo(sancionPeriodoId, ParamUtils.parseLong(entity.get("diaInicio")),
                ParamUtils.parseLong(entity.get("mesInicio")), ParamUtils.parseLong(entity.get("diaFin")),
                ParamUtils.parseLong(entity.get("mesFin")), Boolean.parseBoolean(entity.get("activo")));
        return sancionPeriodoDAO.update(sancionPeriodoDTO);

    }

    private SancionPeriodoDTO uiToModelSancionPeriodo(Long sancionPeriodoId, Long diaInicio, Long mesInicio, Long diaFin, Long mesFin, Boolean activo) {
        SancionPeriodoDTO sancionPeriodoDTO = new SancionPeriodoDTO();

        if (ParamUtils.isNotNull(sancionPeriodoId)) {
            sancionPeriodoDTO = sancionPeriodoDAO.getSancionPeriodoById(sancionPeriodoId);
        }

        sancionPeriodoDTO.setDiaInicio(diaInicio);
        sancionPeriodoDTO.setMesInicio(mesInicio);
        sancionPeriodoDTO.setDiaFin(diaFin);
        sancionPeriodoDTO.setMesFin(mesFin);
        sancionPeriodoDTO.setActivo(activo);

        return sancionPeriodoDTO;

    }

    public void deleteSancionPeriodo(Long sancionPeriodoId) {
        sancionPeriodoDAO.delete(SancionPeriodoDTO.class, sancionPeriodoId);
    }

    public SancionFaltaDTO addSancionFalta(Long orden, String correo, Boolean activo, String asunto) {
        SancionFaltaDTO sancionFaltaDTO = uiToModelSancionFalta(null, orden, correo, activo, asunto);
        return sancionFaltaDAO.insert(sancionFaltaDTO);
    }

    public SancionFaltaDTO updateSancionFalta(Long sancionFaltaId, Long orden, String correo, Boolean activo, String asunto) {
        SancionFaltaDTO sancionFaltaDTO = uiToModelSancionFalta(sancionFaltaId, orden, correo, activo, asunto);
        return sancionFaltaDAO.update(sancionFaltaDTO);
    }

    public void deleteSancionFalta(Long sancionFaltaId) {
        sancionFaltaDAO.delete(SancionFaltaDTO.class, sancionFaltaId);
    }

    private SancionFaltaDTO uiToModelSancionFalta(Long sancionFaltaId, Long orden, String correo, Boolean activo, String asunto) {
        SancionFaltaDTO sancionFaltaDTO = new SancionFaltaDTO();

        if (ParamUtils.isNotNull(sancionFaltaId)) {
            sancionFaltaDTO = sancionFaltaDAO.getSancionFaltaById(sancionFaltaId);
        }

        sancionFaltaDTO.setOrden(orden);
        sancionFaltaDTO.setCorreo(correo);
        sancionFaltaDTO.setActivo(activo);
        sancionFaltaDTO.setAsunto(asunto);

        return sancionFaltaDTO;

    }

    public List<NivelSancion> getNiveles() {
        return sancionNivelDAO.getNiveles();
    }
}
