package es.uji.apps.se.services.rest;

import au.com.bytecode.opencsv.CSVWriter;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.services.EliteService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Path("elite")
public class EliteResource extends CoreBaseService {

    @InjectParam
    EliteService eliteService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getListadoDeportistasElite() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<EliteDTO> elitesDTO = eliteService.getListadoDeportistasElite(connectedUserId);
        return modelToUI(elitesDTO);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{personaId}")
    public ResponseMessage getEliteByPersona(@PathParam("personaId") Long personaId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            responseMessage.setData(UIEntity.toUI(eliteService.getEliteByPersona(personaId)));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Path("{personaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addElitePersona(@PathParam("personaId") Long personaId,
                                    UIEntity entity) throws CommonException {
        try {
            eliteService.addElitePersona(personaId, entity);
            return Response.ok().build();
        } catch (CommonException e) {
            throw new CommonException("Ha ocorregut un error al crear un registre. Per favor, revisa les dades introduïdes.");
        }
    }

    @PUT
    @Path("{personaId}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateElitePersona(@PathParam("personaId") Long personaId,
                                       @PathParam("id") Long id,
                                       UIEntity entity) throws CommonException {
        eliteService.updateElitePersona(id, entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("{personaId}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteElitePersona(@PathParam("id") Long id) throws CommonException {
        eliteService.deleteElitePersona(id);
        return Response.ok().build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/titulacion")
    public List<UIEntity> getTitulacionesElite(@PathParam("id") Long eliteId) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PersonaTitulacionDTO> titulaciones = eliteService.getTitulacionesElite(connectedUserId, eliteId);
        return UIEntity.toUI(titulaciones);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/documento")
    public List<UIEntity> getDocumentosElite(@PathParam("id") Long eliteId) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PersonaFicheroDTO> documentos = eliteService.getDocumentosElite(eliteId);
        return UIEntity.toUI(documentos);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("matricula")
    public List<UIEntity> getListadoDeportistasEliteMatricula() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<EliteMatriculaDTO> elites = eliteService.getListadoDeportistasEliteMatricula(connectedUserId);
        return UIEntity.toUI(elites);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("profesor")
    public List<UIEntity> getListadoProfesoresConAlumnosElite() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<UIEntity> profesoresConAlumnosElite = eliteService.getListadoProfesoresConAlumnosElite(connectedUserId);

        return profesoresConAlumnosElite;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("profesor/{id}/alumnos")
    public List<UIEntity> getListadoAlumnosElitePorProfesor(@PathParam("id") Long profesorId) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<AlumnoAsignaturaDTO> alumnosElite = eliteService.getListadoAlumnosElitePorProfesor(connectedUserId, profesorId);

        return UIEntity.toUI(alumnosElite);
    }

    @GET
    @Path("matricula/descargar/csv")
    public Response descargarListadoDeportistasEliteMatricula() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<EliteMatriculaDTO> elites = eliteService.getListadoDeportistasEliteMatricula(connectedUserId);

        Response.ResponseBuilder res = null;

        res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = eliteMatricula.csv");

        res.entity(entityMatriculaToCSV(elites));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("decano")
    public List<UIEntity> getListadoDecanosConAlumnosElite() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(eliteService.getListadoDecanosConAlumnosElite(connectedUserId));
    }

    @GET
    @Path("descargar/csv")
    public Response descargarListadoDeportistasElite() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<EliteDTO> elitesDTO = eliteService.getListadoDeportistasElite(connectedUserId);

        Response.ResponseBuilder res = null;

        res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = elite.csv");

        res.entity(entityToCSV(elitesDTO));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    private String entityMatriculaToCSV(List<EliteMatriculaDTO> elites) {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();
            List<String> record = new ArrayList<String>(Arrays.asList("Nombre", "Identificacion", "email", "movil", "titulacion"));

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (EliteMatriculaDTO elite : elites) {
                List<String> recordMov = new ArrayList<String>(
                        Arrays.asList(elite.getApellidosNombre(), elite.getIdentificacion(), elite.getMail(), elite.getMovil(), elite.getTitulacion()));
                String[] recordArrayMov = new String[recordMov.size()];
                recordMov.toArray(recordArrayMov);
                records.add(recordArrayMov);
            }
            csvWriter.writeAll(records);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

    private String entityToCSV(List<EliteDTO> elitesDTO) {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();
            List<String> record = new ArrayList<String>(Arrays.asList("Nombre", "Identificacion", "Criterio", "Modalidad", "Fecha Nacimiento", "email",
                    "movil", "titulacion", "sexo"));

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (EliteDTO eliteDTO : elitesDTO) {
                String nombre = eliteDTO.getPersonaUji().getApellidosNombre();
                String identificacion = eliteDTO.getPersonaUji().getIdentificacion();
                String criterioAdmision = "";
                if (ParamUtils.isNotNull(eliteDTO.getCriterioAdmision()))
                    criterioAdmision = eliteDTO.getCriterioAdmision().getNombre();
                String modalidadDeportiva = "";
                if (ParamUtils.isNotNull(eliteDTO.getModalidadDeportiva()))
                    modalidadDeportiva = eliteDTO.getModalidadDeportiva().getNombre();

                List<String> recordMov = new ArrayList<String>(
                        Arrays.asList(nombre, identificacion, criterioAdmision, modalidadDeportiva, eliteDTO.getPersonaUji().getFechaNacimiento().toString(),
                                eliteDTO.getPersonaUji().getMail(), eliteDTO.getPersonaUji().getMovil(), eliteDTO.getPersonaUji().getTitulaciones().iterator().next().getTitulacionNombre(),
                                eliteDTO.getPersonaUji().getSexo()));

                String[] recordArrayMov = new String[recordMov.size()];
                recordMov.toArray(recordArrayMov);
                records.add(recordArrayMov);
            }
            csvWriter.writeAll(records);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

    private UIEntity modelToUI(EliteDTO eliteDTO) {

        UIEntity entity = UIEntity.toUI(eliteDTO);
        entity.put("personaId", eliteDTO.getPersonaUji().getId());;
        entity.put("identificacion", eliteDTO.getPersonaUji().getIdentificacion());
        entity.put("apellidosNombre", eliteDTO.getPersonaUji().getApellidosNombre());
        if (ParamUtils.isNotNull(eliteDTO.getCriterioAdmision()))
            entity.put("criterioAdmision", eliteDTO.getCriterioAdmision().getNombre());
        if (ParamUtils.isNotNull(eliteDTO.getModalidadDeportiva()))
            entity.put("modalidadDeportiva", eliteDTO.getModalidadDeportiva().getNombre());
        return entity;
    }

    private List<UIEntity> modelToUI(List<EliteDTO> persona) {
        return persona.stream().map((EliteDTO t) -> modelToUI(t)).collect(Collectors.toList());
    }
}
