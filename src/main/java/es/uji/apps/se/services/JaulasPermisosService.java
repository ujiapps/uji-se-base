package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.JaulasPermisosDAO;
import es.uji.apps.se.dto.JaulasPermisosDTO;
import es.uji.apps.se.exceptions.jaulasPermisos.JaulasPermisosDeleteException;
import es.uji.apps.se.exceptions.jaulasPermisos.JaulasPermisosInsertException;
import es.uji.commons.rest.UIEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class JaulasPermisosService {

    @InjectParam
    JaulasPermisosDAO jaulasPermisosDAO;

    public List<JaulasPermisosDTO> getJaulasPermisos() {
        return jaulasPermisosDAO.getJaulasPermisos();
    }

    public JaulasPermisosDTO addJaulasPermisos(UIEntity entity) throws JaulasPermisosInsertException {
        JaulasPermisosDTO jaulasPermisosDTO = entity.toModel(JaulasPermisosDTO.class);
        return jaulasPermisosDAO.addJaulasPermisos(jaulasPermisosDTO);
    }

    public void deleteJaulasPermisos(Long id) throws JaulasPermisosDeleteException {
        jaulasPermisosDAO.deleteJaulasPermisos(id);
    }

    public Boolean tieneAccesoJaulas(Long vinculoId) {
        return jaulasPermisosDAO.tieneAccesoJaulas(vinculoId) != null;
    }
}