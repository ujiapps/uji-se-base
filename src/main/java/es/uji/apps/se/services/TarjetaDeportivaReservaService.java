package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.TarjetaDeportivaReservaDAO;
import es.uji.apps.se.dto.TarjetaDeportivaReservaDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TarjetaDeportivaReservaService
{
    @InjectParam
    private TarjetaDeportivaReservaDAO tarjetaDeportivaReservaDAO;

    public List<TarjetaDeportivaReservaDTO> getReservasByTarjetaDeportivaId(Long tarjetaDeportivaId)
    {
        return tarjetaDeportivaReservaDAO.getReservasByTarjetaDeportivaId(tarjetaDeportivaId);
    }
}
