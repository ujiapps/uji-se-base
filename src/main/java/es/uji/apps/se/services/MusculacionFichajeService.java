package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.MonitorMusculacionFichajeDAO;
import es.uji.apps.se.dto.MonitorMusculacionFichajeDTO;
import es.uji.apps.se.exceptions.ValidacionException;
import es.uji.commons.rest.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MusculacionFichajeService
{
    @InjectParam
    private MonitorMusculacionFichajeDAO monitorMusculacionFichajeDAO;

    public List<MonitorMusculacionFichajeDTO> getFichajesByMonitorId(Long monitorId)
    {
        return monitorMusculacionFichajeDAO.getFichajesByMonitorId(monitorId);
    }

    @Role("ADMIN")
    public MonitorMusculacionFichajeDTO addFichaje(MonitorMusculacionFichajeDTO monitorMusculacionFichajeDTO, Long connectedUserId)
            throws ValidacionException
    {
        monitorMusculacionFichajeDTO.checkIsValid();;
        return monitorMusculacionFichajeDAO.insert(monitorMusculacionFichajeDTO);
    }

    @Role("ADMIN")
    public MonitorMusculacionFichajeDTO updateFichaje(MonitorMusculacionFichajeDTO monitorMusculacionFichajeDTO, Long connectedUserId)
            throws ValidacionException
    {
        monitorMusculacionFichajeDTO.checkIsValid();;
        return monitorMusculacionFichajeDAO.update(monitorMusculacionFichajeDTO);
    }

    @Role("ADMIN")
    public void deleteMonitorMusculacion(Long fichajeId, Long connectedUserId)
    {
        monitorMusculacionFichajeDAO.delete(MonitorMusculacionFichajeDTO.class, fichajeId);
    }
}
