package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TarjetaDeportivaSuperiorDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.apps.se.services.TarjetaDeportivaSuperiorService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class TarjetaDeportivaSuperiorResource extends CoreBaseService
{

    @PathParam("tarjetaDeportivaTipoId")
    Long tarjetaDeportivaTipoId;

    @InjectParam
    TarjetaDeportivaSuperiorService tarjetaDeportivaSuperiorService;

    @GET
    public List<UIEntity> getTarjetasDeportivasSuperiores()
    {
        return UIEntity.toUI(tarjetaDeportivaSuperiorService.getAllByTarjetaDeportivaTipoId(tarjetaDeportivaTipoId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insert(UIEntity entity) {
        TarjetaDeportivaSuperiorDTO tarjetaDeportivaSuperiorDTO = entity.toModel(TarjetaDeportivaSuperiorDTO.class);
        tarjetaDeportivaSuperiorDTO.setTarjetaDeportivaTipo(new TarjetaDeportivaTipoDTO(tarjetaDeportivaTipoId));
//        tarjetaDeportivaSuperior.setTarjetaDeportivaTipoSuperior(new TarjetaDeportivaTipo(ParamUtils.parseLong(entity.get("tarjetaDeportivaSuperior"))));
        return UIEntity.toUI(tarjetaDeportivaSuperiorService.insert(tarjetaDeportivaSuperiorDTO));
    }

    @PUT
    @Path("{tarjetaDeportivaSuperiorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("tarjetaDeportivaSuperiorId") Long tarjetaDeportivaSuperiorId, UIEntity entity) {
        TarjetaDeportivaSuperiorDTO tarjetaDeportivaSuperiorDTO = entity.toModel(TarjetaDeportivaSuperiorDTO.class);
        return UIEntity.toUI(tarjetaDeportivaSuperiorService.update(tarjetaDeportivaSuperiorDTO));
    }

    @DELETE
    @Path("{tarjetaDeportivaSuperiorId}")
    public void delete(@PathParam("tarjetaDeportivaSuperiorId") Long tarjetaDeportivaSuperiorId) {
        tarjetaDeportivaSuperiorService.delete(tarjetaDeportivaSuperiorId);
    }
}
