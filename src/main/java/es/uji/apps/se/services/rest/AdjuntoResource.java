package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.se.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.se.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.se.model.Adjunto;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.AdjuntoService;
import es.uji.commons.rest.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

public class AdjuntoResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(AdjuntoResource.class);

    @InjectParam
    AdjuntoService adjuntoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getAdjuntos(@QueryParam("start") @DefaultValue("0") Long start,
                                       @QueryParam("limit") @DefaultValue("25") Long limit,
                                       @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                       @PathParam("envioId") Long envioId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<Adjunto> adjuntos = adjuntoService.getAdjuntosByEnvioId(envioId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(adjuntos)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getAdjuntos", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseMessage addAdjunto(@PathParam("envioId") Long envioId, FormDataMultiPart multiPart) throws IOException, ErrorSubiendoDocumentoException {

        return adjuntoService.addAdjunto(envioId, multiPart);
    }

    @DELETE
    @Path("{id}")
    public void deleteAdjunto(@PathParam("id") Long adjuntoId) throws MalformedURLException, ErrorEnBorradoDeDocumentoException {
        adjuntoService.deleteAdjunto(adjuntoId);
    }
}
