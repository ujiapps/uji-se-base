package es.uji.apps.se.services;

import es.uji.apps.se.dao.CursoAcademicoDAO;
import es.uji.apps.se.dto.CursoAcademicoDTO;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CursoAcademicoService {

    private CursoAcademicoDAO cursoAcademicoDAO;
    private CalendarioService calendarioService;
    private PeriodoService periodoService;
    private MonitorCursoAcademicoService monitorCursoAcademicoService;
    private ParametroService parametroService;


    @Autowired
    public CursoAcademicoService(CursoAcademicoDAO cursoAcademicoDAO, CalendarioService calendarioService, PeriodoService periodoService,
                                 MonitorCursoAcademicoService monitorCursoAcademicoService, ParametroService parametroService) {

        this.cursoAcademicoDAO = cursoAcademicoDAO;
        this.calendarioService = calendarioService;
        this.periodoService = periodoService;
        this.monitorCursoAcademicoService = monitorCursoAcademicoService;
        this.parametroService = parametroService;
    }

    public List<CursoAcademicoDTO> getCursosAcademicos(int start, int limit, String property, String direction) {
        return cursoAcademicoDAO.getPaginated(CursoAcademicoDTO.class, property, direction.toUpperCase(), start, limit);
    }

    public CursoAcademicoDTO addCursoAcademico(CursoAcademicoDTO cursoAcademicoDTO) {
        this.cursoAcademicoDAO.insert(cursoAcademicoDTO);
        calendarioService.addCalendarioDias(cursoAcademicoDTO.getId());
        return cursoAcademicoDTO;
    }

    public Integer getTotalCursosAcademicos() {
        return this.cursoAcademicoDAO.get(CursoAcademicoDTO.class).size();
    }

    @Transactional
    public void deleteCursoAcademico(Long cursoAcademico) {
        periodoService.eliminaPeriodoCursoAcademico(cursoAcademico);
        monitorCursoAcademicoService.eliminaMonitorCursoAcademico(cursoAcademico);

        this.cursoAcademicoDAO.delete(CursoAcademicoDTO.class, cursoAcademico);
    }

    public CursoAcademicoDTO updateCursoAcademico(CursoAcademicoDTO cursoAcademicoDTO) {

        return this.cursoAcademicoDAO.update(cursoAcademicoDTO);
    }

    public CursoAcademicoDTO getCursoAcademicoById(Long valor) {
//        return this.cursoAcademicoDAO.get(CursoAcademicoDTO.class, valor).get(0);
        return cursoAcademicoDAO.getCursoAcademico(valor);
    }

    public List<CursoAcademicoDTO> getCursosAcademicosUsuario(Long connectedUserId) {

        return this.cursoAcademicoDAO.getCursosAcademicosUsuario(connectedUserId);
    }

    public CursoAcademico getCursoAcademico(Long connectedUserId) {
        CursoAcademico curso = parametroService.getParametroCursoAcademico(connectedUserId);
        return curso;
    }
}

