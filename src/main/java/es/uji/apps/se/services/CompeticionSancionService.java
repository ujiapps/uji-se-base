package es.uji.apps.se.services;

import es.uji.apps.se.dao.CompeticionSancionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CompeticionSancionService {

    private final CompeticionSancionDAO competicionSancionDAO;

    @Autowired
    public CompeticionSancionService(CompeticionSancionDAO competicionSancionDAO) {
        this.competicionSancionDAO = competicionSancionDAO;
    }

    public void updateCompeticionSancion(Long inscripcionId, Long partidoId, String tarjetas){

        competicionSancionDAO.updateCompeticionSancion(inscripcionId, partidoId, tarjetas);

    }

    public void deleteCompeticionSancion(Long inscripcionId, Long partidoId){

        competicionSancionDAO.deleteCompeticionSancion(inscripcionId, partidoId);

    }

    public Map<Long, String> getCompeticionSancion(Long partidoId){

        return competicionSancionDAO.getCompeticionSancionLista(partidoId);

    }

    public boolean getEstaSancionado(Long miembroId, Long partidoId){

        return competicionSancionDAO.getEstaSancionado(miembroId, partidoId);

    }
}
