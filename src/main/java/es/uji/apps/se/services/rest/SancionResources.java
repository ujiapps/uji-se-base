package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.SancionFaltaDTO;
import es.uji.apps.se.model.NivelSancion;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.SancionPeriodoDTO;
import es.uji.apps.se.model.SancionDetalle;
import es.uji.apps.se.services.PersonaSancionDetalleService;
import es.uji.apps.se.services.SancionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("sancion")
public class SancionResources extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(EquipacionGeneroResource.class);

    @InjectParam
    SancionService sancionService;
    @InjectParam
    PersonaSancionDetalleService personaSancionDetalleService;

    @GET
    @Path("falta")
    public ResponseMessage getFaltas(@QueryParam("start") @DefaultValue("0") Long start,
                                     @QueryParam("limit") @DefaultValue("25") Long limit,
                                     @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<SancionFaltaDTO> lista = sancionService.getFaltas(paginacion);

            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(lista)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getFaltas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;

    }


    @POST
    @Path("falta")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addSancionFalta (@FormParam("orden") Long orden, @FormParam("correo") String correo, @FormParam("activo") Boolean activo, @FormParam("asunto") String asunto){

        return UIEntity.toUI(sancionService.addSancionFalta(orden, correo, activo, asunto));
    }

    @PUT
    @Path("falta/{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity updateSancionFalta (@PathParam("id") Long sancionFaltaId, @FormParam("orden") Long orden, @FormParam("correo") String correo,
                                        @FormParam("activo") Boolean activo, @FormParam("asunto") String asunto){

        return UIEntity.toUI(sancionService.updateSancionFalta(sancionFaltaId, orden, correo, activo, asunto));
    }

    @DELETE
    @Path("falta/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteSancionFalta (@PathParam("id") Long sancionFaltaId){

        sancionService.deleteSancionFalta(sancionFaltaId);
    }

    @GET
    @Path("periodo")
    public ResponseMessage getPeriodos(@QueryParam("start") @DefaultValue("0") Long start,
                                       @QueryParam("limit") @DefaultValue("25") Long limit,
                                       @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<SancionPeriodoDTO> lista = sancionService.getPeriodos(paginacion);

            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(lista)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getPeriodos", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }


    @POST
    @Path("periodo")
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addSancionPeriodo (UIEntity entity){

        return UIEntity.toUI(sancionService.addSancionPeriodo(entity));
    }

    @PUT
    @Path("periodo/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateSancionPeriodo (@PathParam("id") Long sancionPeriodoId, UIEntity entity){

        return UIEntity.toUI(sancionService.updateSancionPeriodo(sancionPeriodoId, entity));
    }

    @DELETE
    @Path("periodo/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteSancionPeriodo (@PathParam("id") Long sancionPeriodoId){

        sancionService.deleteSancionPeriodo(sancionPeriodoId);
    }

    @GET
    @Path("niveles")
    public ResponseMessage getNiveles() {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<NivelSancion> niveles = sancionService.getNiveles();
            responseMessage.setTotalCount(niveles.size());
            responseMessage.setData(UIEntity.toUI(niveles));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            log.error("niveles", e);
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @GET
    @Path("{sancionId}/detalle")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDetallesSancion(@QueryParam("start") @DefaultValue("0") Long start,
                                              @QueryParam("limit") @DefaultValue("25") Long limit,
                                              @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                              @PathParam("sancionId") Long sancionId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<SancionDetalle> detallesSancion = personaSancionDetalleService.getDetallesSancion(sancionId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(detallesSancion));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getDetallesSancion", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}

