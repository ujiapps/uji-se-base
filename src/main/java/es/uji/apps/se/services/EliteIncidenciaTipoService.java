package es.uji.apps.se.services;

import es.uji.apps.se.dao.EliteIncidenciaTipoDAO;
import es.uji.apps.se.dto.EliteIncidenciaTipoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EliteIncidenciaTipoService {

    private EliteIncidenciaTipoDAO eliteIncidenciaTipoDAO;

    @Autowired
    public EliteIncidenciaTipoService(EliteIncidenciaTipoDAO eliteIncidenciaTipoDAO) {
        this.eliteIncidenciaTipoDAO = eliteIncidenciaTipoDAO;
    }


    public List<EliteIncidenciaTipoDTO> getEliteIncidenciasTipo() {
        return eliteIncidenciaTipoDAO.get(EliteIncidenciaTipoDTO.class);
    }
}
