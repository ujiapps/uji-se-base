package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionEnvioDAO;
import es.uji.apps.se.dto.EquipacionEnvioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipacionEnvioService {
    private EquipacionEnvioDAO equipacionEnvioDAO;

    @Autowired
    public EquipacionEnvioService(EquipacionEnvioDAO equipacionEnvioDAO) {
        this.equipacionEnvioDAO = equipacionEnvioDAO;
    }

    public List<EquipacionEnvioDTO> getEnvios() {
        return equipacionEnvioDAO.getEnvios();
    }

    public EquipacionEnvioDTO getEnvioById(Long envioId) {
        return equipacionEnvioDAO.getEnvioById(envioId);
    }

    public EquipacionEnvioDTO addEnvio(String asunto, String cuerpo, Long dias) {

        EquipacionEnvioDTO equipacionEnvio = new EquipacionEnvioDTO();
        equipacionEnvio.setCuerpo(cuerpo);
        equipacionEnvio.setAsunto(asunto);
        equipacionEnvio.setDias(dias);

        return equipacionEnvioDAO.insert(equipacionEnvio);
    }

    public EquipacionEnvioDTO updateEnvio(Long envioId, String asunto, String cuerpo, Long dias) {

        EquipacionEnvioDTO equipacionEnvio = getEnvioById(envioId);
        equipacionEnvio.setAsunto(asunto);
        equipacionEnvio.setCuerpo(cuerpo);
        equipacionEnvio.setDias(dias);

        return equipacionEnvioDAO.update(equipacionEnvio);
    }

    public void deleteEnvio(Long envioId) {

        equipacionEnvioDAO.delete(EquipacionEnvioDTO.class, envioId);

    }
}
