package es.uji.apps.se.services;

import es.uji.apps.se.dao.AdjuntoDAO;
import es.uji.apps.se.dao.BicicletaEnvioDAO;
import es.uji.apps.se.dao.EnvioDAO;
import es.uji.apps.se.dto.BicicletaEnvioDTO;
import es.uji.apps.se.model.Paginacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public class BicicletaEnvioService {

    private BicicletaEnvioDAO bicicletaEnvioDAO;

    @Autowired
    public BicicletaEnvioService(BicicletaEnvioDAO bicicletaEnvioDAO) {
        this.bicicletaEnvioDAO = bicicletaEnvioDAO;
    }

    public List<BicicletaEnvioDTO> getEnvios() {
        return bicicletaEnvioDAO.getEnvios();
    }

    public BicicletaEnvioDTO getEnvioById(Long envioId) {
        return bicicletaEnvioDAO.getEnvioById(envioId);
    }

    public BicicletaEnvioDTO addEnvio(String asunto, String cuerpo, Long tipoFecha, Long dias) {

        BicicletaEnvioDTO bicicletaEnvio = new BicicletaEnvioDTO();
        bicicletaEnvio.setCuerpo(cuerpo);
        bicicletaEnvio.setAsunto(asunto);
        bicicletaEnvio.setTipoFecha(tipoFecha);
        bicicletaEnvio.setDias(dias);

        return bicicletaEnvioDAO.insert(bicicletaEnvio);
    }

    public BicicletaEnvioDTO updateEnvio(Long envioId, String asunto, String cuerpo, Long tipoFecha, Long dias) {

        BicicletaEnvioDTO bicicletaEnvio = getEnvioById(envioId);
        bicicletaEnvio.setAsunto(asunto);
        bicicletaEnvio.setCuerpo(cuerpo);
        bicicletaEnvio.setTipoFecha(tipoFecha);
        bicicletaEnvio.setDias(dias);

        return bicicletaEnvioDAO.update(bicicletaEnvio);
    }

    public void deleteEnvio(Long envioId) {

        bicicletaEnvioDAO.delete(BicicletaEnvioDTO.class, envioId);

    }
}
