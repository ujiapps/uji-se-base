package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.ClaseCSVDAO;
import es.uji.apps.se.dto.views.ClaseCSV;
import es.uji.apps.se.utils.Csv;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class ClaseCSVService {
    private ClaseCSVDAO claseCSVDAO;

    @InjectParam
    Csv csv;

    @Autowired
    public ClaseCSVService(ClaseCSVDAO claseCSVDAO) {

        this.claseCSVDAO = claseCSVDAO;
    }

    public List<ClaseCSV> getCsvBySearch(List<Long> clases, List<Long> instalaciones, Date fechaInicio, Date fechaFin) {
        return claseCSVDAO.getCsvBySearch(clases, instalaciones, fechaInicio, fechaFin);
    }


    public String entityClaseToCSV(List<ClaseCSV> clasesCSV) {

        ArrayList<String> cabeceras = new ArrayList<String>(Arrays.asList("Tipus Targeta", "Nom", "Identificació", "email", "movil", "vinculació", "genere", "data naixement",
                "clase", "dia", "asiste", "hora inicio", "instalacion"));

        List<List<String>> records = new ArrayList<List<String>>();
        for (ClaseCSV claseCSV : clasesCSV) {
            List<String> recordMov = new ArrayList<String>(creaListaString(claseCSV));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(ClaseCSV claseCSV) {
        return Arrays.asList(
                claseCSV.getTipoTarjeta(),
                claseCSV.getApellidosNombre(),
                claseCSV.getIdentificacion(),
                claseCSV.getMail(),
                claseCSV.getMovil(),
                claseCSV.getVinculoActual(),
                claseCSV.getSexo(),
                ParamUtils.isNotNull(claseCSV.getFechaNacimiento())?claseCSV.getFechaNacimiento().toString():"",
                claseCSV.getClase(),
                ParamUtils.isNotNull(claseCSV.getDia())?claseCSV.getDia().toString():"",
                (claseCSV.isAsiste())?"SI":"NO",
                claseCSV.getHoraInicio(),
                claseCSV.getInstalacion()
        );
    }
}
