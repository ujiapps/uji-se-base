package es.uji.apps.se.services;

import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.se.dao.AdjuntoDAO;
import es.uji.apps.se.dto.AdjuntoDTO;
import es.uji.apps.se.dto.EnvioDTO;
import es.uji.apps.se.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.se.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.se.model.Adjunto;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class AdjuntoService {

    private AdjuntoDAO adjuntoDAO;
    private ADEClienteService adeClienteService;

    @Autowired
    public AdjuntoService(AdjuntoDAO adjuntoDAO, ADEClienteService adeClienteService) {
        this.adjuntoDAO = adjuntoDAO;
        this.adeClienteService = adeClienteService;
    }

    public List<Adjunto> getAdjuntosByEnvioId(Long envioId) {
        return adjuntoDAO.getAdjuntosByEnvioId(envioId, null);
    }

    public List<Adjunto> getAdjuntosByEnvioId(Long envioId, Paginacion paginacion) {
        return adjuntoDAO.getAdjuntosByEnvioId(envioId, paginacion);
    }

    public ResponseMessage addAdjunto(Long envioId, FormDataMultiPart multiPart) throws IOException, ErrorSubiendoDocumentoException {
        ResponseMessage response = new ResponseMessage();
        response.setSuccess(true);
        response.setMessage("Adjunt pujat");
        AdjuntoDTO adjunto = new AdjuntoDTO();
        Map<String, Object> datosDocumento = extraeDatosDeMultipart(multiPart);

        if (ParamUtils.isNotNull(datosDocumento))
        {

            EnvioDTO envioDTO = new EnvioDTO();
            envioDTO.setId(envioId);
            adjunto.setEnvioDTO(envioDTO);

            if (ParamUtils.isNotNull(datosDocumento.get("nombre")))
            {
                adjunto.setNombre(datosDocumento.get("nombre").toString());
            }
            else
            {
                adjunto.setNombre("No imagen");
            }

            String ref = adeClienteService.addDocumento(datosDocumento.get("nombre").toString(),
                    datosDocumento.get("mimetype").toString(),
                    (byte[]) datosDocumento.get("contenido"));

            adjunto.setReferencia(ref);
            adjunto.setTipo(datosDocumento.get("mimetype").toString());
            adjuntoDAO.insert(adjunto);
            return response;
        }

        response.setSuccess(false);
        response.setMessage("No s'ha pogut pujar l'adjunt");
        return response;
    }

    private Map<String, Object> extraeDatosDeMultipart(FormDataMultiPart multiPart)
            throws IOException {
        Map<String, Object> datos = new HashMap<>();

        String fileName = "";
        String mimeType = "";
        InputStream documento = null;

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (ParamUtils.isNotNull(mimeType))
            {
                if (mimeType != null && !mimeType.isEmpty())
                {
                    String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                    Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                    Matcher m = fileNamePattern.matcher(header);
                    if (m.matches())
                    {
                        fileName = m.group(1);
                    }
                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    documento = bpe.getInputStream();
                }
            }
        }

        if (ParamUtils.isNotNull(fileName) && ParamUtils.isNotNull(documento))
        {
            datos.put("nombre", fileName);
            datos.put("mimetype", mimeType);
            datos.put("contenido", StreamUtils.inputStreamToByteArray(documento));

            return datos;
        }
        else
        {
            return null;
        }
    }

    public void deleteAdjunto(Long adjuntoId) throws MalformedURLException, ErrorEnBorradoDeDocumentoException {
        Adjunto adjunto = adjuntoDAO.getAdjuntoById(adjuntoId);
        List<Adjunto> adjuntos = adjuntoDAO.getAdjuntosByReferencia(adjunto.getReferencia());
        if (adjuntos.size() == 1L) {
            adeClienteService.deleteDocumento(adjunto.getReferencia());
        }
        adjuntoDAO.delete(AdjuntoDTO.class, adjuntoId);
    }
}
