package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.exceptions.ValidacionException;
import es.uji.apps.se.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class IndicadorInstalacionesService {

    private AccesosPorPerfilDAO accesosPorPerfilDAO;
    private AccesosPorTipologiaDAO accesosPorTipologiaDAO;
    private HorasReservadasPorTipologiaDAO horasReservadasPorTipologiaDAO;
    private AccesosFranjaHorariaDAO accesosFranjaHorariaDAO;
    private UsosMaterialDAO usosMaterialDAO;
    private OcupacionEspacioDAO ocupacionEspacioDAO;

    @Autowired
    public IndicadorInstalacionesService(AccesosPorPerfilDAO accesosPorPerfilDAO, AccesosPorTipologiaDAO accesosPorTipologiaDAO, HorasReservadasPorTipologiaDAO horasReservadasPorTipologiaDAO,
                                         AccesosFranjaHorariaDAO accesosFranjaHorariaDAO, UsosMaterialDAO usosMaterialDAO, OcupacionEspacioDAO ocupacionEspacioDAO) {
        this.accesosPorPerfilDAO = accesosPorPerfilDAO;
        this.accesosPorTipologiaDAO = accesosPorTipologiaDAO;
        this.horasReservadasPorTipologiaDAO = horasReservadasPorTipologiaDAO;
        this.accesosFranjaHorariaDAO = accesosFranjaHorariaDAO;
        this.usosMaterialDAO = usosMaterialDAO;
        this.ocupacionEspacioDAO = ocupacionEspacioDAO;
    }

    public List<AccesosPorPerfil> getAccesosPorPerfil(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        return accesosPorPerfilDAO.getAccesosPorPerfil(fechaDesde, fechaHasta, horaDesde, horaHasta);
    }

    private AccesosPorPerfil calculaTotalesPerfil(List<AccesosPorPerfil> accesos){
        AccesosPorPerfil totales = new AccesosPorPerfil(-1L, "Total");
        accesos.stream().forEach(accesosPorPerfil -> {
            totales.setCountZonaPiscina(totales.getCountZonaPiscina() + accesosPorPerfil.getCountZonaPiscina());
            totales.setCountZonaAireLibre(totales.getCountZonaAireLibre() + accesosPorPerfil.getCountZonaAireLibre());
            totales.setCountZonaRaquetas(totales.getCountZonaRaquetas() + accesosPorPerfil.getCountZonaRaquetas());
            totales.setCountZonaPabellon(totales.getCountZonaPabellon() + accesosPorPerfil.getCountZonaPabellon());
            totales.setCountTotal(totales.getCountTotal() + accesosPorPerfil.getCountTotal());
        });
        return totales;
    }

    public List<AccesosPorPerfil> getAccesosPorPerfilExport(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        List<AccesosPorPerfil> accesos = accesosPorPerfilDAO.getAccesosPorPerfil(fechaDesde, fechaHasta, horaDesde, horaHasta);
        accesos.add(calculaTotalesPerfil(accesos));
        return accesos;
    }


    public List<AccesosPorTipologia> getAccesosPorTipologia(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        return accesosPorTipologiaDAO.getAccesosPorTipologia(fechaDesde, fechaHasta, horaDesde, horaHasta);
    }

    private AccesosPorTipologia calculaTotalesTipologia(List<AccesosPorTipologia> accesos){
        AccesosPorTipologia totales = new AccesosPorTipologia(-1L, "Total");
        accesos.stream().forEach(accesosPorPerfil -> {
            totales.setCountZonaPiscina(totales.getCountZonaPiscina() + accesosPorPerfil.getCountZonaPiscina());
            totales.setCountZonaAireLibre(totales.getCountZonaAireLibre() + accesosPorPerfil.getCountZonaAireLibre());
            totales.setCountZonaRaquetas(totales.getCountZonaRaquetas() + accesosPorPerfil.getCountZonaRaquetas());
            totales.setCountZonaPabellon(totales.getCountZonaPabellon() + accesosPorPerfil.getCountZonaPabellon());
            totales.setCountTotal(totales.getCountTotal() + accesosPorPerfil.getCountTotal());
        });
        return totales;
    }

    public List<AccesosPorTipologia> getAccesosPorTipologiaExport(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        List<AccesosPorTipologia> accesos = accesosPorTipologiaDAO.getAccesosPorTipologia(fechaDesde, fechaHasta, horaDesde, horaHasta);
        accesos.add(calculaTotalesTipologia(accesos));
        return accesos;
    }

    private HorasReservadasPorTipologia calculaTotalesHoras(List<HorasReservadasPorTipologia> horas){
        HorasReservadasPorTipologia totales = new HorasReservadasPorTipologia("Total", -10L, "Total");
        horas.stream().forEach(hora -> {
            totales.setCountZonaPiscina(totales.getCountZonaPiscina() + hora.getCountZonaPiscina());
            totales.setCountZonaAireLibre(totales.getCountZonaAireLibre() + hora.getCountZonaAireLibre());
            totales.setCountZonaRaquetas(totales.getCountZonaRaquetas() + hora.getCountZonaRaquetas());
            totales.setCountZonaPabellon(totales.getCountZonaPabellon() + hora.getCountZonaPabellon());
            totales.setCountTotal(totales.getCountTotal() + hora.getCountTotal());
        });
        return totales;
    }

    public List<HorasReservadasPorTipologia> getHorasReservadasPorTipologia(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        return horasReservadasPorTipologiaDAO.getHorasReservadasPorTipologia(fechaDesde, fechaHasta, horaDesde, horaHasta);
    }

    public List<HorasReservadasPorTipologia> getHorasReservadasPorTipologiaExport(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        List<HorasReservadasPorTipologia> horas = horasReservadasPorTipologiaDAO.getHorasReservadasPorTipologia(fechaDesde, fechaHasta, horaDesde, horaHasta);
        horas.add(calculaTotalesHoras(horas));
        return horas;
    }

    public List<AccesosFranjaHoraria> getAccesosPorFranjaHoraria(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        return accesosFranjaHorariaDAO.getAccesosPorFranjaHoraria(fechaDesde, fechaHasta, horaDesde, horaHasta);
    }

    private AccesosFranjaHoraria calculaTotalesAccesosFranjaHoraria(List<AccesosFranjaHoraria> horas){
        AccesosFranjaHoraria totales = new AccesosFranjaHoraria("Total");
        horas.stream().forEach(hora -> {
            totales.setCountZonaPiscina(totales.getCountZonaPiscina() + hora.getCountZonaPiscina());
            totales.setCountZonaAireLibre(totales.getCountZonaAireLibre() + hora.getCountZonaAireLibre());
            totales.setCountZonaRaquetas(totales.getCountZonaRaquetas() + hora.getCountZonaRaquetas());
            totales.setCountZonaPabellon(totales.getCountZonaPabellon() + hora.getCountZonaPabellon());
            totales.setCountTotal(totales.getCountTotal() + hora.getCountTotal());
        });
        return totales;
    }

    public List<AccesosFranjaHoraria> getAccesosPorFranjaHorariaExport(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        List<AccesosFranjaHoraria> accesos = accesosFranjaHorariaDAO.getAccesosPorFranjaHoraria(fechaDesde, fechaHasta, horaDesde, horaHasta);
        accesos.add(calculaTotalesAccesosFranjaHoraria(accesos));
        return accesos;
    }

    public List<UsosMaterial> getUsosMaterial(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        return usosMaterialDAO.getUsosMaterial(fechaDesde, fechaHasta, horaDesde, horaHasta);
    }

    private UsosMaterial calculaTotalesUsoMaterial(List<UsosMaterial> usos){
        UsosMaterial totales = new UsosMaterial("Total", 0L);
        usos.stream().forEach(uso -> {
            totales.setCount(totales.getCount() + uso.getCount());

        });
        return totales;
    }

    public List<UsosMaterial> getUsosMaterialExport(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        List<UsosMaterial> usos = usosMaterialDAO.getUsosMaterial(fechaDesde, fechaHasta, horaDesde, horaHasta);
        usos.add(calculaTotalesUsoMaterial(usos));
        return usos;
    }

    public List<OcupacionEspacio> getOcupacionEspacios(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) throws ValidacionException {
        return ocupacionEspacioDAO.getOcupacionEspacios(fechaDesde, fechaHasta, horaDesde, horaHasta);
    }

    private OcupacionEspacio calculaTotalesOcupacion(List<OcupacionEspacio> ocupaciones){
        OcupacionEspacio totales = new OcupacionEspacio(-1L, "Total");
        ocupaciones.stream().forEach(ocupacion -> {
            totales.setHoras(totales.getHoras()+ocupacion.getHoras());
            totales.setHorasLibres(totales.getHorasLibres()+ocupacion.getHorasLibres());
        });
        totales.setPorcentajeHoras(totales.getHoras()*100/(totales.getHoras()+ totales.getHorasLibres()));
        totales.setPorcentajeHorasLibres(totales.getHorasLibres()*100/(totales.getHoras()+ totales.getHorasLibres()));
        return totales;
    }

    public List<OcupacionEspacio> getOcupacionEspaciosExport(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) throws ValidacionException {
        List<OcupacionEspacio> ocupaciones = ocupacionEspacioDAO.getOcupacionEspacios(fechaDesde, fechaHasta, horaDesde, horaHasta);
        ocupaciones.add(calculaTotalesOcupacion(ocupaciones));
        return ocupaciones;
    }
}
