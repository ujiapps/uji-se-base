package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionActividadDAO;
import es.uji.apps.se.dto.EquipacionActividadDTO;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades.EquipacionesActividadesDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades.EquipacionesActividadesInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades.EquipacionesActividadesUpdateException;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.EquipacionActividad;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EquipacionActividadService {

    @Autowired
    EquipacionActividadDAO equipacionActividadDAO;

    @Autowired
    ParametroService parametroService;

    public EquipacionActividadService(EquipacionActividadDAO equipacionActividadDAO) {
        this.equipacionActividadDAO = equipacionActividadDAO;
    }

    public List<EquipacionActividad> getActividadesGenericas(Paginacion paginacion, List<Map<String, String>> filtros) {
        return equipacionActividadDAO.getEquipacionesActividadesGenericas(paginacion, filtros);
    }

    public List<EquipacionActividad> getActividadesEspecificas(Long userId, List<Map<String, String>> filtros, Paginacion paginacion) {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(userId);
        return equipacionActividadDAO.getEquipacionesActividadesEspecificas(cursoAcademico.getId(), filtros, paginacion);
    }

    public String getIdCompuestoEquipActividad(EquipacionActividad ea){
        return "".concat(String.valueOf(ea.getEquipacionId())).concat(" ").concat(String.valueOf(ea.getActividadId())).concat(" ").concat(String.valueOf(ea.getVez()));
    }

    public void addEquipacionActividadGenerica(UIEntity entity) throws EquipacionesActividadesInsertException {
        try {
            EquipacionActividad equipacionActividad = entity.toModel(EquipacionActividad.class);
            equipacionActividadDAO.insert(new EquipacionActividadDTO(equipacionActividad));
        } catch (Exception e) {
            throw new EquipacionesActividadesInsertException();
        }
    }

    public void addEquipacionActividadEspecifica(UIEntity entity, Long userId) throws EquipacionesActividadesInsertException {
        try {
            CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(userId);
            EquipacionActividad equipacionActividad = entity.toModel(EquipacionActividad.class);
            equipacionActividad.setCursoAcademico(cursoAcademico.getId());
            EquipacionActividadDTO equipActDuplicado = equipacionActividadDAO.getEquipacionActividadDuplicado(equipacionActividad);
            if (equipActDuplicado != null) {
                throw new EquipacionesActividadesInsertException("Ja existeix una línea amb aquestes dades");
            }
            equipacionActividadDAO.insert(new EquipacionActividadDTO(equipacionActividad));
        } catch (EquipacionesActividadesInsertException e) {
            throw new EquipacionesActividadesInsertException(e.getMessage());
        } catch (Exception e) {
            throw new EquipacionesActividadesInsertException();
        }
    }

    @Transactional
    public void updateEquipacionActividadGenerica(UIEntity entity) throws EquipacionesActividadesUpdateException {
        try {
            EquipacionActividad equipacionActividad = entity.toModel(EquipacionActividad.class);
            equipacionActividadDAO.updateEquipacionActividad(equipacionActividad);
        } catch (Exception e) {
            throw new EquipacionesActividadesUpdateException();
        }
    }

    @Transactional
    public void updateEquipacionActividadEspecifica(UIEntity entity, Long userId) throws EquipacionesActividadesUpdateException {
        try {
            CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(userId);
            EquipacionActividad equipacionActividad = entity.toModel(EquipacionActividad.class);
            if (equipacionActividad.getCursoAcademico() == null){
                equipacionActividad.setId(null);
                equipacionActividad.setCursoAcademico(cursoAcademico.getId());
                EquipacionActividadDTO equipActDuplicado = equipacionActividadDAO.getEquipacionActividadDuplicado(equipacionActividad);
                if (equipActDuplicado != null) {
                    throw new EquipacionesActividadesUpdateException("Ja existeix una línea amb aquestes dades");
                }
                equipacionActividadDAO.insert(new EquipacionActividadDTO(equipacionActividad));
            } else {
                equipacionActividadDAO.updateEquipacionActividad(equipacionActividad);
            }
        } catch (EquipacionesActividadesUpdateException e) {
            throw new EquipacionesActividadesUpdateException(e.getMessage());
        } catch (Exception e) {
            throw new EquipacionesActividadesUpdateException();
        }
    }

    public void deleteEquipacionActividad(Long id) throws EquipacionesActividadesDeleteException {
        try {
            equipacionActividadDAO.delete(EquipacionActividadDTO.class, id);
        } catch (Exception e) {
            throw new EquipacionesActividadesDeleteException();
        }
    }
}
