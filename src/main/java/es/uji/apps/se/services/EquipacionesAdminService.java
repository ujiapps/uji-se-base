package es.uji.apps.se.services;

import java.util.Date;
import java.util.List;

import es.uji.apps.se.exceptions.CommonException;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.se.dao.EquipacionesAdminDAO;
import es.uji.apps.se.dao.EquipacionesMovimientoDAO;
import es.uji.apps.se.dto.EquipacionDTO;
import es.uji.apps.se.dto.EquipacionTipoMovimientoDTO;
import es.uji.apps.se.dto.EquipacionesMovimientoDTO;
import es.uji.apps.se.model.EquipacionesAdmin;

@Service
public class EquipacionesAdminService
{
    @InjectParam
    private EquipacionesAdminDAO equipacionesAdminDAO;
    @InjectParam
    private EquipacionesMovimientoDAO equipacionesMovimientoDAO;

    public List<EquipacionesAdmin> getEquipacionesAdmin(Long persona)
    {
        return equipacionesAdminDAO.getEquipacionesAdmin(persona);
    }

    public void postEquipacionesAdmin(Long persona, Long equipacion, String talla)
    {
        Long MOVADMIN = 9704070L;
        EquipacionesMovimientoDTO equipacionesMovimientoDTO;
        equipacionesMovimientoDTO = new EquipacionesMovimientoDTO();

        EquipacionDTO equipacionDTO = new EquipacionDTO();
        equipacionDTO.setId(equipacion);
        equipacionesMovimientoDTO.setEquipacionDTO(equipacionDTO);

        equipacionesMovimientoDTO.setPersonaId(91558L);

        EquipacionTipoMovimientoDTO equipacionTipoMovimientoDTO = new EquipacionTipoMovimientoDTO();
        equipacionTipoMovimientoDTO.setId(MOVADMIN);
        equipacionesMovimientoDTO.setEquipacionTipoMovimientoDTO(equipacionTipoMovimientoDTO);

        equipacionesMovimientoDTO.setFecha(new Date());
        equipacionesMovimientoDTO.setCantidad(1L);
        equipacionesMovimientoDTO.setTalla(talla);

        equipacionesMovimientoDAO.insert(equipacionesMovimientoDTO);
    }

    public void deleteEquipacionAdmin(Long equipacionAdminId) throws CommonException {
        try {
            equipacionesMovimientoDAO.delete(EquipacionesMovimientoDTO.class, equipacionAdminId);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    public EquipacionesMovimientoDTO putEquipacionesAdmin(Long id, Long equipacionId, String talla) {
        EquipacionesMovimientoDTO movimiento = equipacionesMovimientoDAO.getEquipacionMovimiento(id);

        EquipacionDTO equipacionDTO = new EquipacionDTO();
        equipacionDTO.setId(equipacionId);
        movimiento.setEquipacionDTO(equipacionDTO);

        movimiento.setTalla(talla);
        return equipacionesMovimientoDAO.update(movimiento);
    }
}
