package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.Envio;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.EnvioService;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Path("envio")
public class EnvioResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(EnvioResource.class);

    @InjectParam
    EnvioService envioService;

    @Path("{envioId}/adjunto")
    public AdjuntoResource getPlatformItem(
            @InjectParam AdjuntoResource adjuntoResource) {
        return adjuntoResource;
    }

    @Path("{envioId}/persona")
    public EnvioPersonaResource getPlatformItem(
            @InjectParam EnvioPersonaResource envioPersonaResource) {
        return envioPersonaResource;
    }

    @Path("{envioId}/filtro")
    public EnvioFiltroResource getPlatformItem(
            @InjectParam EnvioFiltroResource envioFiltroResource) {
        return envioFiltroResource;
    }

    @Path("{envioId}/filtrotipologia")
    public EnvioFiltroTipologiaResource getPlatformItem(
            @InjectParam EnvioFiltroTipologiaResource envioFiltroTipologiaResource) {
        return envioFiltroTipologiaResource;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEnvios(@QueryParam("start") @DefaultValue("0") Long start,
                                               @QueryParam("limit") @DefaultValue("25") Long limit,
                                               @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                               @QueryParam("perfil") Long perfil,
                                               @QueryParam("asunto") String asunto,
                                               @QueryParam("persona") Long persona) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<Envio> envios = envioService.getEnviosByBusqueda(perfil, asunto, persona, connectedUserId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(envios)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getEnvios", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("{id}")
    public UIEntity getEnvioById(@PathParam("id") Long envioId) {
        return UIEntity.toUI(envioService.getEnvioById(envioId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addEnvio(@FormParam("nombre") String nombre, @FormParam("cuerpo") String cuerpo,
                             @FormParam("fechaEnvio") String fechaEnvio,
                             @FormParam("horaEnvio") String horaEnvio) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyyHH:mm");
        String fecha;
        Date fechaValida = null;

        if (ParamUtils.isNotNull(fechaEnvio)) {
            if (ParamUtils.isNotNull(horaEnvio)) {
                fecha = fechaEnvio.concat(horaEnvio);
            } else {
                fecha = fechaEnvio.concat("00:00");
            }
            fechaValida = formateadorFecha.parse(fecha);
        }
        return UIEntity.toUI(envioService.addEnvio(connectedUserId, nombre, cuerpo, fechaValida));
    }

    @POST
    @Path("{id}/duplicar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity duplicaEnvio(@PathParam("id") Long envioId) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(envioService.duplicaEnvio(connectedUserId, envioId));
    }

    @POST
    @Path("prueba/{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void envioPrueba(@PathParam("id") Long envioId, @FormParam("correo") String correo) throws MessageNotSentException {
        envioService.envioPrueba(envioId, correo);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("{id}")
    public UIEntity updateEnvio(@PathParam("id") Long envioId, @FormParam("nombre") String nombre, @FormParam("cuerpo") String cuerpo,
                                @FormParam("fechaEnvio") String fechaEnvio,
                                @FormParam("horaEnvio") String horaEnvio) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyyHH:mm");
        String fecha;
        Date fechaValida = null;

        if (ParamUtils.isNotNull(fechaEnvio)) {
            if (ParamUtils.isNotNull(horaEnvio)) {
                fecha = fechaEnvio.concat(horaEnvio);
            } else {
                fecha = fechaEnvio.concat("00:00");
            }
            fechaValida = formateadorFecha.parse(fecha);
        }
        return UIEntity.toUI(envioService.updateEnvio(connectedUserId, envioId, nombre, cuerpo, fechaValida));

    }

    @PUT
    @Path("enviar/{id}")
    public ResponseMessage enviarEnvio(@PathParam("id") Long envioId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return envioService.enviarEnvio(connectedUserId, envioId);
    }

    @DELETE
    @Path("{id}")
    public ResponseMessage deleteEnvio(@PathParam("id") Long envioId) {
        return envioService.deleteEnvio(envioId);
    }
}
