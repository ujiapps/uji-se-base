package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionGruposDAO;
import es.uji.apps.se.dto.EquipacionGruposDTO;
import es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes.GruposDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes.GruposInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes.GruposUpdateException;
import es.uji.apps.se.model.EquipacionGrupos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EquipacionGruposService {

    @Autowired
    EquipacionGruposDAO equipacionGruposDAO;

    public EquipacionGruposService(EquipacionGruposDAO equipacionGruposDAO) {
        this.equipacionGruposDAO = equipacionGruposDAO;
    }

    public List<EquipacionGrupos> getEquipacionGrupos() {
        return equipacionGruposDAO.getEquipacionGrupos();
    }

    @Transactional
    public void addEquipacionGrupo(EquipacionGrupos equipacionGrupo) throws GruposInsertException {
        try {
            equipacionGruposDAO.insert(new EquipacionGruposDTO(equipacionGrupo));
        } catch (Exception e) {
            throw new GruposInsertException();
        }
    }

    @Transactional
    public void updateEquipacionGrupo(EquipacionGrupos equipacionGrupo) throws GruposUpdateException {
        try {
            equipacionGruposDAO.update(new EquipacionGruposDTO(equipacionGrupo));
        } catch (Exception e) {
            throw new GruposUpdateException();
        }
    }

    @Transactional
    public void deleteEquipacionGrupo(EquipacionGrupos equipacionGrupo) throws GruposDeleteException {
        try {
            equipacionGruposDAO.deleteEquipacionGrupos(equipacionGrupo.getId());
        } catch (Exception e) {
            throw new GruposDeleteException();
        }
    }
}