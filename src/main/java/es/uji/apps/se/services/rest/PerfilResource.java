package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.PeriodoCountActividadYGrupoDTO;
import es.uji.apps.se.dto.PeriodoDTO;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.services.PerfilService;
import es.uji.apps.se.services.PeriodoCountService;
import es.uji.apps.se.services.PeriodoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Path("perfil")
public class PerfilResource extends CoreBaseService {

    @InjectParam
    PerfilService perfilService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPerfiles() {
        return UIEntity.toUI(perfilService.getPerfiles());
    }
}
