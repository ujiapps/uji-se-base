package es.uji.apps.se.services;

import es.uji.apps.se.dao.MonitorOfertaDAO;
import es.uji.apps.se.dto.MonitorCursoAcademicoDTO;
import es.uji.apps.se.dto.MonitorOfertaDTO;
import es.uji.apps.se.dto.OfertaDTO;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MonitorOfertaService {

    private MonitorOfertaDAO monitorOfertaDAO;
    private MonitorCursoAcademicoService monitorCursoAcademicoService;

    @Autowired
    public MonitorOfertaService(MonitorOfertaDAO monitorOfertaDAO, MonitorCursoAcademicoService monitorCursoAcademicoService) {

        this.monitorOfertaDAO = monitorOfertaDAO;
        this.monitorCursoAcademicoService = monitorCursoAcademicoService;
    }

    public void generaMonitorOfertaDestino(OfertaDTO ofertaDTO, OfertaDTO ofertaDestinoDTO) {


        List<MonitorOfertaDTO> monitoresOferta = getMonitoresActivosByOferta(ofertaDTO);
        if (!ofertaDTO.getPeriodo().getCursoAcademico().equals(ofertaDestinoDTO.getPeriodo().getCursoAcademico())) {
            for (MonitorOfertaDTO monitorOfertaOrigenDTO : monitoresOferta) {

                MonitorCursoAcademicoDTO monitorCursoAcademicoDTO = monitorCursoAcademicoService.getMonitorCursoByMonitorAndCurso(monitorOfertaOrigenDTO.getMonitorCursoAcademico().getMonitor(), ofertaDestinoDTO.getPeriodo().getCursoAcademico());

                if (!ParamUtils.isNotNull(monitorCursoAcademicoDTO)) {
                    monitorCursoAcademicoDTO = monitorCursoAcademicoService.insertMonitorCursoAcademico(ofertaDestinoDTO, monitorOfertaOrigenDTO);
                }

                insertMonitorOferta(ofertaDestinoDTO, monitorOfertaOrigenDTO, monitorCursoAcademicoDTO);
            }
        } else {
            for (MonitorOfertaDTO monitorOfertaOrigenDTO : monitoresOferta) {
                insertMonitorOferta(ofertaDestinoDTO, monitorOfertaOrigenDTO, monitorOfertaOrigenDTO.getMonitorCursoAcademico());
            }
        }

    }


    private void insertMonitorOferta(OfertaDTO ofertaDestinoDTO, MonitorOfertaDTO monitorOfertaOrigenDTO, MonitorCursoAcademicoDTO monitorCursoAcademicoDTO) {

        MonitorOfertaDTO monitorOfertaDestinoDTO = new MonitorOfertaDTO();

        monitorOfertaDestinoDTO.setPrincipal(monitorOfertaOrigenDTO.getPrincipal());
        monitorOfertaDestinoDTO.setOferta(ofertaDestinoDTO);
        monitorOfertaDestinoDTO.setMonitorCursoAcademico(monitorCursoAcademicoDTO);
        monitorOfertaDestinoDTO.setFechaAlta(new Date());

        monitorOfertaDAO.insert(monitorOfertaDestinoDTO);
    }

    private List<MonitorOfertaDTO> getMonitoresActivosByOferta(OfertaDTO ofertaDTO) {

        return monitorOfertaDAO.getMonitoresActivosByOferta(ofertaDTO);
    }

    public void eliminaMonitorOferta(OfertaDTO ofertaDTO) {
        monitorOfertaDAO.delete(MonitorOfertaDTO.class, "oferta_id = " + ofertaDTO.getId());
    }
}
