package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.DatosPersona;
import es.uji.apps.se.services.CursoAcademicoService;
import es.uji.apps.se.services.DatosPersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;

@Path("datospersona")
public class DatosPersonaResource extends CoreBaseService {

    private static final Logger log = LoggerFactory.getLogger(DatosPersonaResource.class);
    @InjectParam
    DatosPersonaService datosPersonaService;

    @InjectParam
    CursoAcademicoService cursoAcademicoService;

    @GET
    public ResponseMessage getDatosPersona(@QueryParam("personaId") Long personaId) {
        ResponseMessage response = new ResponseMessage();
        try {
            Long conectedUserId = AccessManager.getConnectedUserId(request);
            Long cursoAcademico = cursoAcademicoService.getCursoAcademico(conectedUserId).getId();
            response.setData(UIEntity.toUI(datosPersonaService.getDatosPersona(personaId, cursoAcademico)));
            response.setSuccess(true);

        } catch (Exception e) {
            log.error("getDatosPersona", e);
            response.setSuccess(false);
        }
        return response;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDatosPersona(@PathParam("id") Long id, UIEntity entity) {

        Long conectedUserId = AccessManager.getConnectedUserId(request);
        Long cursoAcademico = cursoAcademicoService.getCursoAcademico(conectedUserId).getId();
        datosPersonaService.updateDatosPersona(entity, id, cursoAcademico);
        return Response.ok().build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage addDatosPersona(UIEntity entity) {
        ResponseMessage responseMessage = new ResponseMessage(false);
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        Long cursoAcademico = cursoAcademicoService.getCursoAcademico(conectedUserId).getId();
        try {
            DatosPersona datosPersona = entity.toModel(DatosPersona.class);
            if (entity.get("fechaNacimiento") != null) {
                datosPersona.setFechaNacimiento(new SimpleDateFormat("dd/MM/yyyy").parse(entity.get("fechaNacimiento")));
            }
            Long usuarioId = datosPersonaService.addDatosPersona(datosPersona, cursoAcademico);
            responseMessage.setMessage( usuarioId + "");
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return responseMessage;
    }

    @GET
    @Path("{identificacion}")
    public ResponseMessage existeIdentificacion(@PathParam("identificacion") String identificacion) {
        ResponseMessage responseMessage = new ResponseMessage(false);
        try {
            responseMessage.setTotalCount(Math.toIntExact(datosPersonaService.existeIdentificacion(identificacion)));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            log.error("existeIdentificaicon", e);
            throw new RuntimeException(e);
        }
        return responseMessage;
    }

    @POST
    @Path("{id}/cambiopassword")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cambiaPassword(@PathParam("id") Long id, UIEntity entity) {
        String clave = entity.get("clave");
        try {
            datosPersonaService.cambiaPassword(id, clave);
            return Response.ok().build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
