package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.List;


@Service
public class PersonaService {

    private final ParametroDAO parametroDAO;
    private final ActividadDAO actividadDAO;
    private final PersonaDAO personaDAO;
    private final BicicletasUsuarioDAO bicicletasUsuarioDAO;
    private final ReciboDAO reciboDAO;
    private final PersonaUjiService personaUjiService;
    private final PersonaVinculoService personaVinculoService;
    private final PersonaSancionService personaSancionService;
    private final InscripcionDAO inscripcionDAO;

    @Autowired
    public PersonaService(PersonaDAO personaDAO,
                          BicicletasUsuarioDAO bicicletasUsuarioDAO,
                          ReciboDAO reciboDAO,
                          PersonaUjiService personaUjiService,
                          ParametroDAO parametroDAO,
                          ActividadDAO actividadDAO,
                          PersonaVinculoService personaVinculoService,
                          PersonaSancionService personaSancionService, InscripcionDAO inscripcionDAO) {
        this.personaDAO = personaDAO;
        this.reciboDAO = reciboDAO;
        this.bicicletasUsuarioDAO = bicicletasUsuarioDAO;
        this.personaUjiService = personaUjiService;
        this.parametroDAO = parametroDAO;
        this.actividadDAO = actividadDAO;
        this.personaVinculoService = personaVinculoService;
        this.personaSancionService = personaSancionService;
        this.inscripcionDAO = inscripcionDAO;
    }

    public PersonaDTO getPersonaById(Long personaId) {
        PersonaDTO personaDTO = personaDAO.getPersonaById(personaId);
        if (!ParamUtils.isNotNull(personaDTO)) {
            personaDTO = new PersonaDTO();
            personaDTO.setId(personaId);
            personaDTO.setRecibirCorreo(Boolean.TRUE);
            PersonaUjiDTO personaUjiDTO = personaUjiService.getPersonaById(personaId);
            if (ParamUtils.isNotNull(personaUjiDTO)) {
                personaDTO.setPersonaUji(personaUjiDTO);
                personaDTO = personaDAO.insert(personaDTO);
            }
        }
        return personaDTO;
    }

    public void updatePersona(Long connectedUserId, String fechaNacimiento, String mail, String telefono, Long sexo) throws ParseException {
        PersonaDTO personaDTO = personaDAO.getPersonaById(connectedUserId);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        personaDTO.setFechaNacimiento(dateFormat.parse(fechaNacimiento));
        personaDTO.setMail(mail);
        personaDTO.setMovil(telefono);
        personaDTO.setSexo(sexo);
        personaDAO.update(personaDTO);
    }

    public void compruebaSiExisteUsuarioEnDeportes(Long personaId) throws CommonException {
        try {
            //Comprueba si el usuario existe en uji y en deportes y si no existe lo crea
            PersonaDTO personaDTO = getPersonaById(personaId);
            personaVinculoService.insertaOActualizaVinculoGenericoDeportes(personaDTO);
        }
        catch (Exception e) {
            throw new CommonException("Error comprovant la vinculació amb el servei d'esports");
        }
    }

    public List<ActividadPersona> getActividades(Long personaId, Long periodoId, Long tipoId) {
        return actividadDAO.getActividades(personaId, periodoId, tipoId);
    }

    public Boolean esMoroso(Long personaId) {
        return personaDAO.esMoroso(personaId);
    }

    public Boolean esAdmin(Long personaId) {
        return personaDAO.isAdmin(personaId);
    }

    public Boolean tienePerfil(Long conectedUserId, Long perfil) {
        return personaDAO.tienePerfil(conectedUserId, perfil);
    }

    public String puedeReservar(Long perId, Long vinculo, Long cursoAca) throws ParseException {
        if (personaSancionService.tieneSancion(perId)){
            return "N-No pot reservar bicicletes per tindre una sanció activa. Consulta al Servei d'Esports.";
        }
        Long listaEsperaActiva;
        Date fechaIni;
        Date fechaFin;
        try {
            BicicletasPeriodosDTO bicicletaPeriodosDTO = bicicletasUsuarioDAO.getBicicletaPeriodos(cursoAca);
            listaEsperaActiva = bicicletaPeriodosDTO.getListaEsperaActiva();
            fechaIni = bicicletaPeriodosDTO.getFechaINIListaEspera();
            fechaFin = bicicletaPeriodosDTO.getFechaFinListaEspera();
        } catch (Exception e) {
            return "N-No hi ha ningún periode donat d'alta per a reservar bicicletes a aquest curs acadèmic. Consulta al Servei d'Esports.";
        }
        if (listaEsperaActiva == 0L) return "N-No es poden reservar bicicletes.";
        else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaIni);
            cal.set(Calendar.HOUR_OF_DAY, 8);
            Date dateConverted = cal.getTime();
            if (dateConverted.after(new Date()) || fechaFin.before(new Date()))
                return "N-No es trobes el periode de reserves.";
            else {
                Long numVinculos = bicicletasUsuarioDAO.getNumeroVinculos(perId, cursoAca, vinculo);
                Long numTarjetas = bicicletasUsuarioDAO.getNumeroTarjetas(perId, cursoAca);
                Long numActividades = bicicletasUsuarioDAO.getNumeroActividades(perId, cursoAca);

                if (numVinculos == 0L && numTarjetas == 0L && numActividades == 0L) return "V-No tens vinculació activa que et permeta reservar. Consulta al Servei d'Esports.";

                if (numVinculos > 0L) {
                    BicicletasFechasTipoReservas fechasTipoReservas = bicicletasUsuarioDAO.getFechasBicicletasTipoReservasVinculo(vinculo, cursoAca, fechaIni, fechaFin);
                    fechaIni = fechasTipoReservas.getFechaIni();
                    fechaFin = fechasTipoReservas.getFechaFin();
                }

                if (numTarjetas > 0L) {
                    BicicletasFechasTipoReservas fechasTipoReservas = bicicletasUsuarioDAO.getFechasBicicletasTipoReservasTarjeta(perId, cursoAca, fechaIni, fechaFin);
                    fechaIni = fechasTipoReservas.getFechaIni();
                    fechaFin = fechasTipoReservas.getFechaFin();
                }

                if (numActividades > 0L) {
                    BicicletasFechasTipoReservas fechasTipoReservas = bicicletasUsuarioDAO.getFechasBicicletasTipoReservasActividades(perId, cursoAca, fechaIni, fechaFin);
                    fechaIni = fechasTipoReservas.getFechaIni();
                    fechaFin = fechasTipoReservas.getFechaFin();
                }

                cal = Calendar.getInstance();
                cal.setTime(fechaIni);
                cal.set(Calendar.HOUR_OF_DAY, 8);
                dateConverted = cal.getTime();
                if (dateConverted.after(new Date()) || fechaFin.before(new Date())) {
                    return "N-Les dades de reserva vinculades al vincle actual s''encontren fora de plaç.";
                }
            }
        }
        return "S";
    }

    public String dameImpInscPer(Long ofertaId, Long personaId, String tipo, Date fecha) {
        return inscripcionDAO.dameImpInscPer(ofertaId, personaId, tipo, fecha);
    }

    public String getImporte(String xml, String tipo) {
        return personaDAO.getImporte(xml, tipo);

    }

    public Long getImporteTarjeta(Long tipoId, Long personaId) {
        Long importe = reciboDAO.getImporteTarjeta();

        if (importe == null) {
            importe = dameImpTdybPer(personaId, tipoId, "IMPORTE");
        }

        return importe;
    }

    public Long dameImpTdybPer(Long personaId, Long tipoId, String tipo) {
        return personaDAO.dameImpTdybPer(personaId, tipoId, tipo, parametroDAO.getParametroGlobal("TARIFAS-CARNETS-BONOS").getValor());
    }

}
