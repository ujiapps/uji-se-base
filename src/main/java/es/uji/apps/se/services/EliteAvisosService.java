package es.uji.apps.se.services;

import es.uji.apps.se.dao.EliteAvisosDAO;
import es.uji.apps.se.dto.EliteAvisoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EliteAvisosService {

    private EliteAvisosDAO eliteAvisosDAO;

    @Autowired
    public EliteAvisosService(EliteAvisosDAO eliteAvisosDAO) {
        this.eliteAvisosDAO = eliteAvisosDAO;
    }

    public EliteAvisoDTO addEliteAvisos(EliteAvisoDTO eliteAvisoDTO) {
       return eliteAvisosDAO.insert(eliteAvisoDTO);
    }
}
