package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.DetalleHistoricoTarjetaBono;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.TarjetaHorario;
import es.uji.apps.se.services.CarnetBonoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("carnetbono")
public class CarnetBonoResource extends CoreBaseService {

    private static final Logger log = LoggerFactory.getLogger(CarnetBonoResource.class);

    @InjectParam
    CarnetBonoService carnetBonoService;

    @GET
    @Path("{tarjetaBonoId}/detalles")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDetallesTarjetaBono(@PathParam("tarjetaBonoId") Long tarjetaBonoId,
                                                  @QueryParam("start") @DefaultValue("0") Long start,
                                                  @QueryParam("limit") @DefaultValue("25") Long limit,
                                                  @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<UIEntity> usosTarjetaBono = UIEntity.toUI(carnetBonoService.getDetallesTarjetaBono(tarjetaBonoId, paginacion));
            responseMessage.setData(usosTarjetaBono);
            responseMessage.setTotalCount(usosTarjetaBono.size());
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("{tarjetaBonoId}/detalles/historico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDetallesHistoricoTarjetaBono(@PathParam("tarjetaBonoId") Long tarjetaBonoId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<DetalleHistoricoTarjetaBono> detalleHistoricoTarjetaBonos = carnetBonoService.getDetallesHistoricoTarjetaBono(tarjetaBonoId);
            responseMessage.setData(UIEntity.toUI(detalleHistoricoTarjetaBonos));
            responseMessage.setTotalCount(detalleHistoricoTarjetaBonos.size());
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getDetallesHistoricoTarjetaBono", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("{tarjetaBonoId}/horarios")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage fichaTarjetaHorario(@PathParam("tarjetaBonoId") Long tarjetaBonoId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<TarjetaHorario> tarjetaHorarios = carnetBonoService.fichaTarjetaHorario(tarjetaBonoId);

            if(tarjetaHorarios != null) {
                responseMessage.setData(UIEntity.toUI(tarjetaHorarios));
                responseMessage.setTotalCount(tarjetaHorarios.size());
            }

            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("fichaTarjetaHorario", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Path("{tarjetaBonoId}/detalles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDetalleTarjetaBono(@PathParam("tarjetaBonoId") Long tarjetaBonoId) throws CommonException {
        carnetBonoService.addUso(tarjetaBonoId);
        return Response.ok().build();
    }

    @PUT
    @Path("{tarjetaBonoId}/detalles/{detalleTarjetaBonoId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDetalleTarjetaBono(@PathParam("tarjetaBonoId") Long tarjetaBonoId,
                                             @PathParam("detalleTarjetaBonoId") Long detalleTarjetaBonoId,
                                             UIEntity entity) throws CommonException {
        carnetBonoService.updateDetalleTarjetaBono(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("{tarjetaBonoId}/detalles/{detalleTarjetaBonoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteDetalleTarjetaBono(@PathParam("tarjetaBonoId") Long tarjetaBonoId,
                                             @PathParam("detalleTarjetaBonoId") Long detalleTarjetaBonoId) throws CommonException {
        carnetBonoService.deleteDetalleTarjetaBono(detalleTarjetaBonoId);
        return Response.ok().build();
    }

    @DELETE
    @Path("{tarjetaBonoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response bajaTarjetaBono(@PathParam("tarjetaBonoId") Long tarjetaBonoId) throws CommonException {
        carnetBonoService.bajaTarjetaBono(tarjetaBonoId);
        return Response.ok().build();
    }


}
