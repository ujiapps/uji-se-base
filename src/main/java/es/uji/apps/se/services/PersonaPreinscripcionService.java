package es.uji.apps.se.services;

import es.uji.apps.se.dao.PersonaPreinscripcionDAO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.PlazaInscripcion;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;

@Service
public class PersonaPreinscripcionService {

    private static final Logger log = LoggerFactory.getLogger(PersonaPreinscripcionService.class);

    private final PersonaPreinscripcionDAO personaPreinscripcionDAO;

    @Autowired
    public PersonaPreinscripcionService(PersonaPreinscripcionDAO personaPreinscripcionDAO) {
        this.personaPreinscripcionDAO = personaPreinscripcionDAO;
    }

    public void deletePreinscripcion(Long preinscripcionId) throws CommonException {
        try {
            personaPreinscripcionDAO.deletePreinscripcion(preinscripcionId);
        } catch (Exception e) {
            log.error("No s'ha pogut esborrar aquesta preinscripció", e);
            throw new CommonException("No s'ha pogut esborrar aquesta preinscripció");
        }
    }

    public Long compruebaPreinscripcion(Long personaId, Long grupoId) {
        return personaPreinscripcionDAO.compruebaPreinscripcion(personaId, grupoId);
    }

    public void addPreinscripcion(UIEntity entity, Long personaId, Long grupoId) throws CommonException {
        try {
            PlazaInscripcion plazaInscripcion = entity.toModel(PlazaInscripcion.class);

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            if (entity.get("fechaInicioValida") != null) {
                plazaInscripcion.setFechaInicioValida(formato.parse(entity.get("fechaInicioValida")));
            }

            if (entity.get("fechaFinValida") != null) {
                plazaInscripcion.setFechaFinValida(formato.parse(entity.get("fechaFinValida")));
            }
            personaPreinscripcionDAO.addPreinscripcion(plazaInscripcion, personaId, grupoId);
        } catch (Exception e) {
            log.error("No s'ha pogut inserir aquesta inscripció", e);
            throw new CommonException("No s'ha pogut inserir aquesta inscripció");
        }
    }
}
