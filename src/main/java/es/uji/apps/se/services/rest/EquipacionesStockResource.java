package es.uji.apps.se.services.rest;

import au.com.bytecode.opencsv.CSVWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.EquipacionesStock;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.EquipacionesStockService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;

@Path("equipaciones-stock")
public class EquipacionesStockResource extends CoreBaseService {

    public final static Logger LOG = LoggerFactory.getLogger(EquipacionesStockResource.class);

    @InjectParam
    EquipacionesStockService equipacionesStockService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEquipacionesStock(@QueryParam("start") @DefaultValue("0") Long start,
                                                @QueryParam("limit") @DefaultValue("25") Long limit,
                                                @QueryParam("filter") @DefaultValue("[]") String filterJson,
                                                @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        List<Map<String, String>> filtros = null;
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            if (!filterJson.equals("[]")) {
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {
                });
            }

            List<EquipacionesStock> equipacionesStock = equipacionesStockService.getEquipacionesStock(paginacion, filtros);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(equipacionesStock));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            LOG.error("Error en l'obtenció del stock de les equipacions", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("/descargar/csv")
    @Produces("application/vnd.ms-excel")
    public Response descargarListadoControlStock(@QueryParam("filterJson") @DefaultValue("[]") String filterJson) throws CommonException {
        try {
            List<Map<String, String>> filtros = null;

            if (!filterJson.equals("[]")) {
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {
                });
            }

            List<EquipacionesStock> equipacionesStock =
                    equipacionesStockService.getEquipacionesStock(null, filtros);

            Response.ResponseBuilder res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = controlStockEquipaciones.csv");

            res.entity(entityEquipacionesStockToCSV(equipacionesStock));
            res.type(MediaType.TEXT_PLAIN);

            return res.build();
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    @GET
    @Path("/descargar/csv/finsA")
    @Produces("application/vnd.ms-excel")
    public Response descargarListadoControlStockFinsA(@QueryParam("filterJson") @DefaultValue("[]") String filterJson,
                                                      @QueryParam("dateFilter") @DefaultValue("[]") String dateFilter) throws CommonException {
        try {
            Date fechaFin;
            List<Map<String, String>> filtros = null;

            if (!filterJson.equals("[]"))
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {});

            try {
                String value = new ObjectMapper().readTree(dateFilter).get(0).get("value").asText();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                fechaFin = dateFormat.parse(value);
            } catch (Exception e) {
                throw new CommonException(e.getMessage());
            }

            List<EquipacionesStock> equipacionesStocks = equipacionesStockService.getEquipacionesStockFinsA(filtros, null, fechaFin);

            Response.ResponseBuilder res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = controlStockEquipacionesFinsA" + new SimpleDateFormat("dd/MM/yyyy").format(fechaFin) + ".csv");

            res.entity(entityEquipacionesStockToCSV(equipacionesStocks));
            res.type(MediaType.TEXT_PLAIN);

            return res.build();
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    @GET
    @Path("/descargar/csv/desDeFinsA")
    @Produces("application/vnd.ms-excel")
    public Response descargarListadoControlStockDesdeFinsA(@QueryParam("filterJson") @DefaultValue("[]") String filterJson,
                                                      @QueryParam("dateFilter") @DefaultValue("[]") String dateFilter) throws CommonException {
        try {
            Date fechaIni;
            Date fechaFin;
            List<Map<String, String>> filtros = null;

            if (!filterJson.equals("[]"))
                filtros = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>() {});

            try {
                String value = new ObjectMapper().readTree(dateFilter).get(0).get("value").asText();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                fechaIni = dateFormat.parse(value);

                value = new ObjectMapper().readTree(dateFilter).get(1).get("value").asText();
                fechaFin = dateFormat.parse(value);
            } catch (Exception e) {
                throw new CommonException(e.getMessage());
            }

            List<EquipacionesStock> equipacionesStocks = equipacionesStockService.getEquipacionesStockFinsA(filtros, fechaIni, fechaFin);

            Response.ResponseBuilder res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = controlStockEquipacionesDesDe" + new SimpleDateFormat("dd/MM/yyyy").format(fechaIni) + "FinsA" + new SimpleDateFormat("dd/MM/yyyy").format(fechaFin) + ".csv");

            res.entity(entityEquipacionesStockIntervalToCSV(equipacionesStocks));
            res.type(MediaType.TEXT_PLAIN);

            return res.build();
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }



    private String entityEquipacionesStockToCSV(List<EquipacionesStock> equipacionesStock) throws CommonException {
        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter;

        try {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<>();
            List<String> record = new ArrayList<>(Arrays.asList("Nom d'equipació", "Model", "Gènere", "Talla", "Dorsal", "Stock"));

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (EquipacionesStock equipacion : equipacionesStock) {
                List<String> recordMov = new ArrayList<>(
                        Arrays.asList(equipacion.getEquipacionTipoNombre() != null ? equipacion.getEquipacionTipoNombre() : "",
                                equipacion.getEquipacionModeloNombre() != null ? equipacion.getEquipacionModeloNombre() : "",
                                equipacion.getEquipacionGeneroNombre() != null ? equipacion.getEquipacionGeneroNombre() : "",
                                equipacion.getTalla() != null ? equipacion.getTalla() : "",
                                equipacion.getDorsal() != null ? equipacion.getDorsal().toString() : "",
                                equipacion.getStock() != null ? equipacion.getStock().toString() : ""));
                String[] recordArrayMov = new String[recordMov.size()];
                recordMov.toArray(recordArrayMov);
                records.add(recordArrayMov);
            }
            csvWriter.writeAll(records);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }

        return writer.toString();
    }

    private String entityEquipacionesStockIntervalToCSV(List<EquipacionesStock> equipacionesStock) throws CommonException {
        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter;

        try {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<>();
            List<String> record = new ArrayList<>(Arrays.asList("Nom d'equipació", "Model", "Gènere", "Talla", "Dorsal", "Stock", "Estado"));

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (EquipacionesStock equipacion : equipacionesStock) {
                List<String> recordMov = new ArrayList<>(
                        Arrays.asList(equipacion.getEquipacionTipoNombre() != null ? equipacion.getEquipacionTipoNombre() : "",
                                equipacion.getEquipacionModeloNombre() != null ? equipacion.getEquipacionModeloNombre() : "",
                                equipacion.getEquipacionGeneroNombre() != null ? equipacion.getEquipacionGeneroNombre() : "",
                                equipacion.getTalla() != null ? equipacion.getTalla() : "",
                                equipacion.getDorsal() != null ? equipacion.getDorsal().toString() : "",
                                equipacion.getStock() != null ? String.valueOf(Math.abs(equipacion.getStock())) : "",
                                equipacion.getStock() == null || equipacion.getStock() == 0? "Sense canvis": (equipacion.getStock() < 0? "Reducció": "Augment")));
                String[] recordArrayMov = new String[recordMov.size()];
                recordMov.toArray(recordArrayMov);
                records.add(recordArrayMov);
            }
            csvWriter.writeAll(records);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }

        return writer.toString();
    }


}