package es.uji.apps.se.services;

import es.uji.apps.se.dao.PersonaSancionDetalleDAO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.SancionDetalle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaSancionDetalleService {
    private PersonaSancionDetalleDAO personaSancionDetalleDAO;

    @Autowired
    public PersonaSancionDetalleService(PersonaSancionDetalleDAO personaSancionDetalleDAO) {
        this.personaSancionDetalleDAO = personaSancionDetalleDAO;
    }

//    public List<SancionDetalle> getDetallesSancion(Long sancionId) {
//        return personaSancionDetalleDAO.getDetallesSancion(sancionId, null).stream().map(
//                sancionDetalleDTO ->
//                new SancionDetalle(sancionDetalleDTO.getId(), sancionDetalleDTO.getSancionId(), sancionDetalleDTO.getMotivo()))
//                .collect(Collectors.toList());
//    }

    public List<SancionDetalle> getDetallesSancion(Long sancionId, Paginacion paginacion) {
        return personaSancionDetalleDAO.getDetallesSancion(sancionId, paginacion);
    }

    public void deleteDetallesSancion(Long sancionId) {
        List<SancionDetalle> detallesSancion = getDetallesSancion(sancionId, null);
        detallesSancion.forEach(detalleSancion -> {
            personaSancionDetalleDAO.deleteDetalleSancion(detalleSancion.getId());
        });
    }
}
