package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.AlquilerService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("alquiler")
public class AlquilerResource extends CoreBaseService {

    @InjectParam
    AlquilerService alquilerService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAlquileres(){
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(alquilerService.getAlquileres(connectedUserId));
    }
}
