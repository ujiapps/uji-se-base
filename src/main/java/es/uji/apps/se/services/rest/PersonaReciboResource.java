package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class PersonaReciboResource extends CoreBaseService {
    @InjectParam
    PersonaReciboService personaReciboService;
    @InjectParam
    ReciboService reciboService;
    @InjectParam
    PersonaService personaService;
    @InjectParam
    InscripcionService inscripcionService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRecibos(@QueryParam("start") @DefaultValue("0") Long start,
                                      @QueryParam("limit") @DefaultValue("25") Long limit,
                                      @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                      @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<Recibo> recibos = personaReciboService.getRecibosPersona(paginacion, personaId);
            responseMessage.setData(UIEntity.toUI(recibos));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @GET
    @Path("pendientesact")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRecibosPendientesActividades(@QueryParam("start") @DefaultValue("0") Long start,
                                                           @QueryParam("limit") @DefaultValue("25") Long limit,
                                                           @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                                           @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<ReciboPendienteActividad> recibosPendientes = personaReciboService.getRecibosPendientesActividades(paginacion, personaId);

            if(!recibosPendientes.isEmpty()) {
                recibosPendientes.forEach(reciboPendiente -> {
                    SimpleDateFormat formatoInicial = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat formatoFinal = new SimpleDateFormat("dd/MM/yyyy");

                    String dto;
                    String tipoDTO;
                    String[] tmp = formatoInicial.format(reciboPendiente.getFecha()).split("-");
                    String f = tmp[2] + '/' + tmp[1] + '/' + tmp[0];

                    try {
                        dto = personaService.dameImpInscPer(reciboPendiente.getOfertaId(), personaId, "DTO", formatoFinal.parse(f));
                        tipoDTO = personaService.dameImpInscPer(reciboPendiente.getOfertaId(), personaId, "TIPODTO", formatoFinal.parse(f));
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }

                    Long importe;
                    String importeTexto = null;

                    try {
                        importe = reciboService.getImporte(reciboPendiente.getId());
                        importeTexto = "";

                        if(!"0".equals(dto)) {
                            if(!"EUROS".equals(tipoDTO)) {
                                tipoDTO = "%";
                                importeTexto = String.valueOf(importe);
                            }
                        }
                    }
                    catch (Exception e) {
                        try {
                            importe = Long.valueOf(personaService.dameImpInscPer(reciboPendiente.getOfertaId(), personaId, "IMPORTE", formatoFinal.parse(f)));
                        } catch (ParseException ex) {
                            throw new RuntimeException(ex);
                        }

                        if(!"0".equals(dto)) {
                            if("EUROS".equals(tipoDTO))
                                importeTexto = String.valueOf(importe - ParamUtils.parseLong(dto));
                            else
                                importeTexto = String.valueOf(importe * (Math.round((1 - (double) Long.parseLong(dto) / 100) * 100.0) / 100.0));

                            dto = dto.concat("EUROS".equals(tipoDTO) ? tipoDTO : "%");
                        }
                    }

                    if(importe != null && importe != 0) {
                        reciboPendiente.setDto(dto);
                        reciboPendiente.setImporte(importe);
                        reciboPendiente.setImporteTexto(importeTexto);
                    }
                });
            }

            responseMessage.setData(UIEntity.toUI(recibosPendientes));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("pendientesactcap")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRecibosPendientesCapitanActividades(@QueryParam("start") @DefaultValue("0") Long start,
                                                                  @QueryParam("limit") @DefaultValue("25") Long limit,
                                                                  @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                                                  @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<ReciboPendienteCapitanActividad> recibosPendientesCapitanActividades = personaReciboService.getRecibosPendientesCapitanActividades(paginacion, personaId);

            if(!recibosPendientesCapitanActividades.isEmpty()) {
                recibosPendientesCapitanActividades.forEach(recibo -> {
                    recibo.setCuenta(inscripcionService.getCuenta(recibo.getInscripcionId(), personaId));
                });

                responseMessage.setData(UIEntity.toUI(recibosPendientesCapitanActividades));
                responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            }

            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("pendientestar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRecibosPendientesTarjetas(@QueryParam("start") @DefaultValue("0") Long start,
                                                        @QueryParam("limit") @DefaultValue("25") Long limit,
                                                        @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                                        @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<ReciboPendienteTarjeta> recibosPendientesTarjetas = personaReciboService.getRecibosPendientesTarjetas(paginacion, personaId);

            if(!recibosPendientesTarjetas.isEmpty()) {
                recibosPendientesTarjetas.forEach(recibo -> {
                    recibo.setImporte(personaService.getImporteTarjeta(recibo.getTipoId(), personaId));

                    if(recibo.getImporte() != null && recibo.getImporte() != 0) {
                        Long plazos = personaService.dameImpTdybPer(personaId, recibo.getTipoId(), "PLAZOS");
                        recibo.setPlazos(plazos);
                    }
                });

                responseMessage.setData(UIEntity.toUI(recibosPendientesTarjetas));
                responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            }

            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Path("equipo")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response creaReciboEquipo(@PathParam("personaId") Long personaId,
                                     UIEntity entity) throws CommonException {
        reciboService.creaReciboEquipo(personaId, entity);
        return Response.ok().build();
    }

    @POST
    @Path("nuevo")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response creaReciboNew(@PathParam("personaId") Long personaId) throws CommonException {
        reciboService.creaReciboNew(personaId);
        return Response.ok().build();
    }


}
