package es.uji.apps.se.services;

import es.uji.apps.se.dao.MonitorCalendarioDAO;
import es.uji.apps.se.dto.CalendarioClaseDTO;
import es.uji.apps.se.dto.MonitorClaseCalendarioDTO;
import es.uji.apps.se.dto.MonitorCursoAcademicoDTO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class MonitorCalendarioService {

    public final static Logger log = LoggerFactory.getLogger(MonitorCalendarioService.class);
    private MonitorCalendarioDAO monitorCalendarioDAO;
    private CalendarioClaseService calendarioClaseService;

    @Autowired
    public MonitorCalendarioService(MonitorCalendarioDAO monitorCalendarioDAO, CalendarioClaseService calendarioClaseService) {
        this.monitorCalendarioDAO = monitorCalendarioDAO;
        this.calendarioClaseService = calendarioClaseService;
    }

    public List<MonitorClaseCalendarioDTO> getMonitoresCalendario(Long calendarioClaseId) {
        return monitorCalendarioDAO.getMonitoresCalendario(calendarioClaseId);
    }

    public MonitorClaseCalendarioDTO insertMonitorCalendario(Long calendarioClaseId, UIEntity entity) {
        return monitorCalendarioDAO.insert(entityToModel(calendarioClaseId, entity));
    }

    public MonitorClaseCalendarioDTO updateMonitorCalendarioClase(Long calendarioClaseId, UIEntity entity) {
        return monitorCalendarioDAO.update(entityToModel(calendarioClaseId, entity));

    }

    public void deleteMonitorCalendarioClase(Long monitorCalendarioClaseId) {
        monitorCalendarioDAO.delete(MonitorClaseCalendarioDTO.class, monitorCalendarioClaseId);
    }


    private MonitorClaseCalendarioDTO entityToModel(Long calendarioClaseId, UIEntity entity) {
        MonitorClaseCalendarioDTO monitorClaseCalendarioDTO = entity.toModel(MonitorClaseCalendarioDTO.class);

        CalendarioClaseDTO calendarioClaseDTO = new CalendarioClaseDTO();
        calendarioClaseDTO.setId(calendarioClaseId);
        monitorClaseCalendarioDTO.setCalendarioClase(calendarioClaseDTO);

        if (ParamUtils.isNotNull(entity.get("monitorCursoAcademicoId"))) {
            MonitorCursoAcademicoDTO monitorCursoAcademicoDTO = new MonitorCursoAcademicoDTO();
            monitorCursoAcademicoDTO.setId(ParamUtils.parseLong(entity.get("monitorCursoAcademicoId")));
            monitorClaseCalendarioDTO.setMonitorCursoAcademico(monitorCursoAcademicoDTO);
        }

        return monitorClaseCalendarioDTO;
    }

    public void insertMonitorMultipleClase(Long connectedUserId, Long claseId, String fechaInicio, String fechaFin, String horaInicio, String horaFin, List<Integer> diasSemana, List<Long> monitoresCursoAcademico) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDate fechaIni = LocalDate.parse(fechaInicio, formatter).minusDays(1);
        LocalDate fechaF = LocalDate.parse(fechaFin, formatter).plusDays(1);
        String horaIni = horaInicio;
        String horaF = horaFin;

        List<CalendarioClaseDTO> calendarioClasesDTO = calendarioClaseService.getCalendariosClase(connectedUserId, claseId, null, null, null);

        for (CalendarioClaseDTO calendarioClaseDTO : calendarioClasesDTO) {
            if (fechaIni.isBefore(calendarioClaseDTO.getCalendario().getDia().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                    && fechaF.isAfter(calendarioClaseDTO.getCalendario().getDia().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())) {
                if (diasSemana.contains(calendarioClaseDTO.getCalendario().getDia().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getDayOfWeek().getValue())
                        || !ParamUtils.isNotNull(diasSemana.get(0))) {
                    if (!ParamUtils.isNotNull(horaInicio)) horaIni = calendarioClaseDTO.getHoraInicio();
                    if (!ParamUtils.isNotNull(horaFin)) horaF = calendarioClaseDTO.getHoraFin();

                    if (calendarioClaseDTO.getHoraInicio().equals(horaIni) && calendarioClaseDTO.getHoraFin().equals(horaF)) {

                        for (Long monitorCursoAcademicoId : monitoresCursoAcademico) {
                            try {
                                MonitorClaseCalendarioDTO monitorClaseCalendarioDTO = new MonitorClaseCalendarioDTO();
                                monitorClaseCalendarioDTO.setCalendarioClase(calendarioClaseDTO);

                                MonitorCursoAcademicoDTO monitorCursoAcademicoDTO = new MonitorCursoAcademicoDTO();
                                monitorCursoAcademicoDTO.setId(monitorCursoAcademicoId);
                                monitorClaseCalendarioDTO.setMonitorCursoAcademico(monitorCursoAcademicoDTO);

                                monitorCalendarioDAO.insert(monitorClaseCalendarioDTO);
                            } catch (Exception e) {
                                log.error("Error al insertar monitor porque ya existe", e);
                            }
                        }
                    }
                }
            }
        }
    }

    public void deleteMonitorByCalendarioClase(Long calendarioClaseId) {
        monitorCalendarioDAO.delete(MonitorClaseCalendarioDTO.class, "calendario_clase_dirigida_id = " + calendarioClaseId);
    }
}
