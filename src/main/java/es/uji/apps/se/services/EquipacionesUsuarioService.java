package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.EquipacionesUsuarioDAO;
import es.uji.apps.se.model.DorsalDisponible;
import es.uji.apps.se.model.EquipacionesUsuario;
import es.uji.apps.se.model.Talla;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipacionesUsuarioService {
    @InjectParam
    private EquipacionesUsuarioDAO equipacionesUsuarioDAO;

    public List<EquipacionesUsuario> getEquipacionesUsuario(Long cursoAca, Long persona, Boolean general) {
        List<EquipacionesUsuario> list = equipacionesUsuarioDAO.getEquipacionesUsuario(cursoAca, persona, general);
        list.forEach(e -> {
            e.setTieneEquipacion(equipacionesUsuarioDAO.getTieneEquipacion(e.getEquipacionId(), persona, cursoAca));
        });
        return list;
    }

    public List<Talla> getTallas(Long equipId) {
        return equipacionesUsuarioDAO.getTallas(equipId);
    }

    public List<Talla> getTallasDisponibles(Long equipId) {
        return equipacionesUsuarioDAO.getTallasDisponibles(equipId);
    }

    public List<DorsalDisponible> getDorsales(Long equipId, String talla) {
        return equipacionesUsuarioDAO.getDorsales(equipId, talla);
    }

    public List<DorsalDisponible> getAllDorsales(Long equipId, String talla) {
        return equipacionesUsuarioDAO.getAllDorsales(equipId, talla);
    }
}
