package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.TipoIdentificacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;

@Path("tipoidentificacionresource")
public class TipoIdentificacionResource extends CoreBaseService {

    @InjectParam
    TipoIdentificacionService tipoIdentificacionService;

    @GET
    public ResponseMessage getTipoIdentificacion() {
        ResponseMessage response = new ResponseMessage();

        try {
            response.setData(UIEntity.toUI(tipoIdentificacionService.getTipoIdentificacion()));
            response.setSuccess(true);

        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }

}
