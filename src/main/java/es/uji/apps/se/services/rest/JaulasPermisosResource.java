package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.jaulasPermisos.JaulasPermisosDeleteException;
import es.uji.apps.se.exceptions.jaulasPermisos.JaulasPermisosInsertException;
import es.uji.apps.se.services.JaulasPermisosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("jaulas-permisos")
public class JaulasPermisosResource extends CoreBaseService {

    @InjectParam
    JaulasPermisosService jaulasPermisosService;

    @GET
    public List<UIEntity> getJaulasPermisos() {
        return UIEntity.toUI(jaulasPermisosService.getJaulasPermisos());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addJaulasPermisos(UIEntity entity) throws JaulasPermisosInsertException {
        return UIEntity.toUI(jaulasPermisosService.addJaulasPermisos(entity));
    }

    @DELETE
    @Path("{id}")
    public void deleteJaulasPermisos(@PathParam("id") Long id) throws JaulasPermisosDeleteException {
        jaulasPermisosService.deleteJaulasPermisos(id);
    }
}