package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.maestrosEquipaciones.modelos.ModelosDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.modelos.ModelosInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.modelos.ModelosUpdateException;
import es.uji.apps.se.model.EquipacionesModelo;
import es.uji.apps.se.services.EquipacionModeloService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("modelo")
public class EquipacionModeloResource extends CoreBaseService {

    @InjectParam
    EquipacionModeloService equipacionModeloService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEquipacionModelos(){
        return UIEntity.toUI(equipacionModeloService.getEquipacionModelos());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addEquipacionModelo(UIEntity entity) throws ModelosInsertException {
        try {
            EquipacionesModelo equipacionesModelo = entity.toModel(EquipacionesModelo.class);
            equipacionesModelo.setId(null);
            return UIEntity.toUI(equipacionModeloService.addEquipacionModelo(equipacionesModelo));
        } catch (Exception e){
            throw new ModelosInsertException("Ha hagut un problema al insertar el nou model");
        }
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateEquipacionModelo (@PathParam("id") Long modeloId, UIEntity entity) throws ModelosUpdateException {
        try {
            EquipacionesModelo equipacionesModelo = entity.toModel(EquipacionesModelo.class);
            equipacionesModelo.setId(modeloId);
            return UIEntity.toUI(equipacionModeloService.updateEquipacionModelo(equipacionesModelo));
        } catch (Exception e) {
            throw new ModelosUpdateException("Ha hagut un problema al actualitzar el model");
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteEquipacionModelo (@PathParam("id") Long modeloId) throws ModelosDeleteException {
        try {
            equipacionModeloService.deleteEquipacionModelo(modeloId);
        } catch (Exception e) {
            throw new ModelosDeleteException("Ha hagut un problema al esborrar el model");
        }
    }
}
