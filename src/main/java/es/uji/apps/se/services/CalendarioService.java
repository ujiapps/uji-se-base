package es.uji.apps.se.services;

import es.uji.apps.se.dao.CalendarioDAO;
import es.uji.apps.se.dto.CalendarioAcademicoDTO;
import es.uji.apps.se.dto.CalendarioDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class CalendarioService {

    private static final Logger log = LoggerFactory.getLogger(CalendarioService.class);
    private final CalendarioAcademicoService calendarioAcademicoService;
    private final CalendarioDAO calendarioDAO;

    @Autowired
    public CalendarioService(CalendarioDAO calendarioDAO, CalendarioAcademicoService calendarioAcademicoService) {

        this.calendarioDAO = calendarioDAO;
        this.calendarioAcademicoService = calendarioAcademicoService;
    }

    public void addCalendarioDias(Long cursoAcademicoId) {
        List<CalendarioAcademicoDTO> calendariosAcademico = calendarioAcademicoService.getCalendarioAcademico(cursoAcademicoId + 1);
        for (CalendarioAcademicoDTO calendarioAcademicoDTO : calendariosAcademico) {
            try {
                CalendarioDTO calendarioDTO = new CalendarioDTO();
                calendarioDTO.setDia(calendarioAcademicoDTO.getFecha());
                calendarioDTO.setEstado(calendarioAcademicoDTO.getTipo());
                calendarioDTO.setId(calendarioAcademicoDTO.getId());
                this.calendarioDAO.insert(calendarioDTO);
            } catch (Exception e) {
                log.error("Error al añadir un día al calendario del curso académico", e);
            }
        }
    }

    public List<CalendarioDTO> getDiasSemanaByFechaInicioAndFechaFin(Date fechaInicio, Date fechaFin, Integer diaSemana) {
        return calendarioDAO.getDiasSemanaByFechaInicioAndFechaFin(fechaInicio, fechaFin, diaSemana);
    }

    public CalendarioDTO getCalendarioByFecha(Date fecha) {
        return calendarioDAO.getCalendarioByFecha(fecha);
    }

    public Map<Integer, Map<Integer, List<LocalDate>>> getDiasFestivos(int anyo, int mes) {

        return calendarioDAO.getDiasFestivos(anyo, mes).stream().map(calendario -> calendario.getDia().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .collect(Collectors.groupingBy(LocalDate::getMonthValue, Collectors.groupingBy(LocalDate::getDayOfMonth)));

    }
}
