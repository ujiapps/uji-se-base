package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.MiembrosPuntosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("miembros")
public class MiembrosPuntosResource extends CoreBaseService {

    @InjectParam
    MiembrosPuntosService miembrosPuntosService;
    @Path("puntos")
    @GET
    public UIEntity getMiembrosPuntos(@QueryParam("personaId") Long personaId, @QueryParam("partidoId") Long partidoId) {
        return UIEntity.toUI(miembrosPuntosService.getMiembrosPuntos(personaId, partidoId));
    }
    @Path("total")
    @GET
    public UIEntity getMiembrosTotal(@QueryParam("equipoId") Long equipoId, @QueryParam("partidoId") Long partidoId) {
        return UIEntity.toUI(miembrosPuntosService.getMiembrosTotal(equipoId, partidoId));
    }
}
