package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.ViaComboService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("viacomboresource")
public class ViaComboResource extends CoreBaseService {

    @InjectParam
    ViaComboService viaComboService;

    @GET
    public ResponseMessage getViaCombo() {
        ResponseMessage response = new ResponseMessage();
        try {
            response.setData(UIEntity.toUI(viaComboService.getViaCombo()));
            response.setSuccess(true);

        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }
}