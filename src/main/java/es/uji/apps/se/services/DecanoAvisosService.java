package es.uji.apps.se.services;

import es.uji.apps.se.dao.DecanoAvisoDAO;
import es.uji.apps.se.dto.DecanoAvisoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DecanoAvisosService {

    private DecanoAvisoDAO decanoAvisoDAO;

    @Autowired
    public DecanoAvisosService(DecanoAvisoDAO decanoAvisoDAO) {
        this.decanoAvisoDAO = decanoAvisoDAO;
    }

    public DecanoAvisoDTO addDecanoAvisos(DecanoAvisoDTO decanoAvisoDTO) {
       return decanoAvisoDAO.insert(decanoAvisoDTO);
    }
}
