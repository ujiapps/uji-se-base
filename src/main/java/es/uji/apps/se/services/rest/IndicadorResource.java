package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.commons.rest.CoreBaseService;

import javax.ws.rs.*;

@Path("indicador")
public class IndicadorResource extends CoreBaseService {

    @Path("instalaciones")
    public IndicadorInstalacionesResource getPlatformItem(
            @InjectParam IndicadorInstalacionesResource indicadorInstalacionesResource) {
        return indicadorInstalacionesResource;
    }

    @Path("actividades")
    public IndicadorActividadesResource getPlatformItem(
            @InjectParam IndicadorActividadesResource indicadorActividadesResource) {
        return indicadorActividadesResource;
    }

}
