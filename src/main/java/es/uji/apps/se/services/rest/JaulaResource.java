package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.JaulaPersonaDTO;
import es.uji.apps.se.dto.PersonaDTO;
import es.uji.apps.se.services.DateExtensions;
import es.uji.apps.se.services.JaulaService;
import es.uji.apps.se.services.JaulasPermisosService;
import es.uji.apps.se.services.PersonaService;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.text.MessageFormat;
import java.text.ParseException;

@Path("jaula")
public class JaulaResource extends CoreBaseService {

    @InjectParam
    JaulaService jaulaService;
    @InjectParam
    JaulasPermisosService jaulasPermisosService;
    @InjectParam
    PersonaService personaService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getTemplateJaula(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        PersonaDTO personaDTO = personaService.getPersonaById(connectedUserId);
        Template template = AppInfo.buildPaginaSinMenuLateral("se/base", idioma);
        template.put("contenido", "se/jaula/default");
        template.put("footer", "se/jaula/footer");

        if (jaulasPermisosService.tieneAccesoJaulas(personaDTO.getPersonaUji().getVinculoId())) {
//            if (Boolean.TRUE) {
            JaulaPersonaDTO jaulaPersonaDTO = jaulaService.getAccesoJaula(connectedUserId);
            if (ParamUtils.isNotNull(jaulaPersonaDTO)) {
                if (ParamUtils.isNotNull(jaulaPersonaDTO.getFechaFin())) {
                    if (DateExtensions.getCurrentDate().before(jaulaPersonaDTO.getFechaFin())) {
                        if (DateExtensions.addDays(jaulaPersonaDTO.getFechaFin(), -8).before(DateExtensions.getCurrentDate())) {
                            template.put("renovacion", Boolean.TRUE);
                        } else {
                            template.put("aviso", MessageFormat.format("El teu accés a l''aparcament de bicicletes caducarà el pròxim {0}", DateExtensions.getDateAsString(jaulaPersonaDTO.getFechaFin())));
                        }
                    }
                } else
                    template.put("aviso", "Tens una sol·licitud activa pendent de tramitar.");
            }
        } else {
            template.put("aviso", "No disposes de la vinculació necessària amb l'UJI per a accedir al servei d'aparcament de bicicletes.");
        }
        return template;

    }

    @POST
    public ResponseMessage solicitaAccesoJaula(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws MessageNotSentException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        jaulaService.solicitaAccesoJaula(connectedUserId);
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);
        return responseMessage;
    }
}
