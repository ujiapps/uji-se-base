package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dao.Recibo;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CarnetBonoService {

    private static final Logger log = LoggerFactory.getLogger(CarnetBonoService.class);

    private final CarnetBonoDAO carnetBonoDAO;
    private final Recibo recibosDAO;

    @Autowired
    public CarnetBonoService(CarnetBonoDAO carnetBonoDAO,
                             Recibo recibosDAO) {
        this.carnetBonoDAO = carnetBonoDAO;
        this.recibosDAO = recibosDAO;
    }

    public List<DetalleTarjetaBono> getDetallesTarjetaBono(Long tarjetaBonoId, Paginacion paginacion) {
        return carnetBonoDAO.getDetallesTarjetaBono(tarjetaBonoId, paginacion);
    }

    public List<DetalleHistoricoTarjetaBono> getDetallesHistoricoTarjetaBono(Long tarjetaBonoId) {
        return carnetBonoDAO.getDetallesHistoricoTarjetaBono(tarjetaBonoId);
    }

    public List<TarjetaHorario> fichaTarjetaHorario(Long tarjetaBonoId) {
        return carnetBonoDAO.fichaTarjetaHorario(tarjetaBonoId);
    }

    public void updateDetalleTarjetaBono(UIEntity entity) throws CommonException {
        try {
            DetalleTarjetaBono detalleTarjetaBono = getDetalleTarjetaBono(entity);
            carnetBonoDAO.updateDetalleTarjetaBono(detalleTarjetaBono);
        } catch (Exception e) {
            log.error("No s'ha pogut actualitzar aquest ús de targeta", e);
            throw new CommonException("No s'ha pogut actualitzar aquest ús de targeta");
        }
    }

    public void deleteDetalleTarjetaBono(Long detalleTarjetaBonoId) throws CommonException {
        try {
            carnetBonoDAO.deleteDetalleTarjetaBono(detalleTarjetaBonoId);
        } catch (Exception e) {
            log.error("No s'ha pogut esborrar aquest ús de targeta", e);
            throw new CommonException("No s'ha pogut esborrar aquest ús de targeta");
        }
    }

    public void bajaTarjetaBono(Long tarjetaBonoId) throws CommonException {
        try {
            Long reciboId = carnetBonoDAO.getReciboId(tarjetaBonoId);

            if(recibosDAO.reciboBloqueado(reciboId).equals("0")) {
                recibosDAO.borraRecibo(reciboId);
                carnetBonoDAO.bajaTarjetaBono(tarjetaBonoId);
            }
        }
        catch (Exception e) {
            log.error("No s'ha pogut donar de baixa la targeta", e);
            throw new CommonException("No s'ha pogut donar de baixa la targeta");
        }
    }

    public void addUso(Long tarjetaBonoId) throws CommonException {
        try {
            Long tipoInstalacionId = carnetBonoDAO.getZona(tarjetaBonoId);

            if (tipoInstalacionId == null)
                tipoInstalacionId = 293537L;

            carnetBonoDAO.addUso(tipoInstalacionId, tarjetaBonoId);
        }
        catch (Exception e) {
            log.error("No s'ha pogut inserir aquest ús", e);
            throw new CommonException("No s'ha pogut inserir aquest ús");
        }
    }

    private DetalleTarjetaBono getDetalleTarjetaBono(UIEntity entity) throws ParseException {
        DetalleTarjetaBono detalleTarjetaBono = entity.toModel(DetalleTarjetaBono.class);

        SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");

        Date fechaEntrada = fecha.parse(entity.get("fechaEntrada"));

        detalleTarjetaBono.setFechaEntrada(new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(fecha.format(fechaEntrada).concat(" ").concat(entity.get("horaEntrada"))));

        if(entity.get("fechaSalida") != null) {
            Date fechaSalida = fecha.parse(entity.get("fechaSalida"));
            detalleTarjetaBono.setFechaSalida(new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(fecha.format(fechaSalida).concat(" ").concat(entity.get("horaSalida"))));
        }

        return detalleTarjetaBono;
    }


}
