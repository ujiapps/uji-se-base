package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.ActividadService;
import es.uji.apps.se.services.IncidenciaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("incidencia")
public class IncidenciaResource extends CoreBaseService {

    @InjectParam
    IncidenciaService incidenciaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIncidencias() {
        return UIEntity.toUI(incidenciaService.getIncidencias());
    }
}
