package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.PersonaSancionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

public class PersonaSancionResource extends CoreBaseService {
    @InjectParam
    PersonaSancionService personaSancionService;

    @GET
    @Path("activo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response tieneSancionUsuario(@PathParam("personaId") Long personaId) {
        Boolean tieneSancion = personaSancionService.tieneSancion(personaId);
        return Response.ok(tieneSancion).build();
    }

    @GET
    @Path("historico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getHistoricoSanciones(@QueryParam("start") @DefaultValue("0") Long start,
                                                 @QueryParam("limit") @DefaultValue("25") Long limit,
                                                 @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                                 @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Long conectedUserId = AccessManager.getConnectedUserId(request);
            List<HistoricoSancion> historicoSanciones = personaSancionService.getHistoricoSanciones(personaId, conectedUserId);
            responseMessage.setTotalCount(historicoSanciones.size());
            responseMessage.setData(UIEntity.toUI(historicoSanciones));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getSanciones(@QueryParam("start") @DefaultValue("0") Long start,
                                        @QueryParam("limit") @DefaultValue("25") Long limit,
                                        @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                        @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<Sancion> sanciones = personaSancionService.getSanciones(personaId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(sanciones));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addSancion(@PathParam("personaId") Long personaId,
                               UIEntity entity) throws CommonException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaSancionService.addSancion(entity, personaId, connectedUserId);
        return Response.ok().build();
    }

    @PUT
    @Path("{sancionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateSancion(@PathParam("sancionId") Long sancionId,
                                  UIEntity entity) throws CommonException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaSancionService.updateSancion(entity, sancionId, connectedUserId);
        return Response.ok().build();
    }

    @DELETE
    @Path("{sancionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteSancion(@PathParam("sancionId") Long sancionId,
                                  UIEntity entity) throws CommonException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaSancionService.deleteSancion(sancionId, connectedUserId);
        return Response.ok().build();
    }
}
