package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.*;
import es.uji.apps.se.dao.Recibo;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.ReciboPendienteCapitanActividad;
import es.uji.apps.se.model.ui.*;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ReciboService {

    private TarjetaDeportivaDAO tarjetaDeportivaDAO;
    private TarjetaDeportivaTipoDAO tarjetaDeportivaTipoDAO;
    private PersonaUjiDAO personaUjiDAO;
    private Recibo recibo;
    private ReciboDAO reciboDAO;

    @InjectParam
    private BonificacionService bonificacionService;

    @InjectParam
    private BonificacionUsoService bonificacionUsoService;

    @Autowired
    public ReciboService(TarjetaDeportivaDAO tarjetaDeportivaDAO, TarjetaDeportivaTipoDAO tarjetaDeportivaTipoDAO, PersonaUjiDAO personaUjiDAO,
                         Recibo recibo, ReciboDAO reciboDAO) {
        this.personaUjiDAO = personaUjiDAO;
        this.tarjetaDeportivaDAO = tarjetaDeportivaDAO;
        this.tarjetaDeportivaTipoDAO = tarjetaDeportivaTipoDAO;
        this.recibo = recibo;
        this.reciboDAO = reciboDAO;
    }

    public UIRecibosPendientes getRecibosPendientesByPersonaId(Long connectedUserId) {
        TarjetaDeportivaDTO tarjetaDeportivaDTO = tarjetaDeportivaDAO.getTarjetaDeportivaDTOActivaByPersonaId(connectedUserId);
        PersonaUjiDTO personaUjiDTO = personaUjiDAO.getPersonaById(connectedUserId);
        TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoDAO.getTarjetaDeportivaTipoById(tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getId());
        return new UIRecibosPendientes(tarjetaDeportivaDTO.getId(), tarjetaDeportivaTipoDTO, personaUjiDTO);
    }

    public UIEntity getReciboByReferenciaId(Long referenciaId) {
        UIEntity entity = new UIEntity();
        try {
            String reciboId = recibo.getReciboByReferenciaId(referenciaId);
            String urlPago = recibo.getUrlPagoDirecto(ParamUtils.parseLong(reciboId));
            entity.put("reciboId", reciboId);
            entity.put("urlPago", urlPago);
        } catch (Exception e) {
            return null;
        }
        return entity;
    }

    public UIEntity generaRecibo(Long bonificacionId, String metodoPago, TarjetaDeportivaDTO tarjetaDeportivaDTO, TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO, PersonaUjiDTO personaUjiDTO, TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO) {


        Recibo recibo = new Recibo();
        recibo.init();

        Long tipoCobro = (metodoPago.equals("1")) ? 5L : 2L;
        String origen = "TD";
        Long plazos = 1L;
        Long importe = tarjetaDeportivaTarifaDTO.getImporte().longValue();
        Long vinculoId = personaUjiDTO.getVinculoId();
        String cuentaBancaria = (metodoPago.equals("1")) ? "" : metodoPago;
        Long dto = null;
        String tipoDto = "";
        String cuentaAbono = "";
        String etiqueta = "";
        Date fechaLimite = new Date();
        String correo = tarjetaDeportivaDTO.getPersonaUji().getMail();
        String correoSE = tarjetaDeportivaDTO.getPersonaSE().getMail();
        Long linea = null;

        if (ParamUtils.isNotNull(bonificacionId)) {
            BonificacionActivaDTO bonificacion = bonificacionService.getBonificacionById(bonificacionId);
            if (ParamUtils.isNotNull(bonificacion.getImporte())) {
                tipoDto = "EUROS";
                dto = (bonificacion.getImporteFloat().longValue() > importe) ? importe : bonificacion.getImporteFloat().longValue();
            } else {
                tipoDto = "%";
                dto = bonificacion.getPorcentaje();
            }
            bonificacionUsoService.anyadeUsoBonificacion(bonificacionId, (ParamUtils.isNotNull(bonificacion.getOrigen())? bonificacion.getOrigen() : "REC"), dto, tarjetaDeportivaDTO.getId());
        }

        recibo.generarRecibo(tipoCobro, origen, tarjetaDeportivaDTO.getId(), plazos, importe, tarjetaDeportivaDTO.getPersonaUji().getId(), vinculoId, tarjetaDeportivaTipoDTO.getNombre(), "CA", cuentaBancaria,
                dto, tipoDto, cuentaAbono, etiqueta, fechaLimite, (ParamUtils.isNotNull(correoSE))?correoSE:correo, linea);


        if ((ParamUtils.isNotNull(bonificacionId) && tipoDto.equals("EUROS") && importe.equals(dto)) || importe.equals(0L)) {
            UIEntity entity = new UIEntity();
            entity.put("urlPago", "/se/rest/app/tarjetas");
            return entity;
        } else {
            UIEntity entity = getReciboByReferenciaId(tarjetaDeportivaDTO.getId());
            return entity;
        }
    }

    public void creaReciboEquipo(Long personaId, UIEntity entity) throws CommonException {
        try {
            ReciboPendienteCapitanActividad reciboPendiente = entity.toModel(ReciboPendienteCapitanActividad.class);
            recibo.creaReciboEquipo(reciboPendiente.getEquipoId(), personaId, reciboPendiente.getOfertaId(), reciboPendiente.getActividadNombre());
        }
        catch (Exception e) {
            throw new CommonException("No s'ha pogut crear el rebut d'equip");
        }
    }

    public void creaReciboNew(Long personaId) throws CommonException {
        try {
            recibo.creaReciboNew(personaId);
        }
        catch (Exception e) {
            throw new CommonException("No s'ha pogut crear el rebut d'activitat");
        }
    }

//    public String getReciboByReferenciaId(Long referenciaId) {
//        return recibo.getReciboByReferenciaId(referenciaId);
//    }

    public Long getImporte(Long reciboId) {
        return reciboDAO.getImporte(reciboId);
    }

//    public UIEntity getReciboById(Long reciboId) {
//        UIEntity entity = new UIEntity();
//        try {
//            String urlPago = recibo.getUrlPagoDirecto(reciboId);
//            entity.put("reciboId", reciboId);
//            entity.put("urlPago", urlPago);
//        } catch (Exception e) {
//            return null;
//        }
//        return entity;
//    }
}
