package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dao.Recibo;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonaTarjetaBonoService {

    private static final Logger log = LoggerFactory.getLogger(PersonaTarjetaBonoService.class);

    private final PersonaTarjetaBonoDAO personaTarjetaBonoDAO;
    private final PersonaSancionService personaSancionService;
    private final PersonaService personaService;
    private final PersonasUJIVinculoDAO personasUJIVinculoDAO;
    private final ParametroDAO parametroDAO;
    private final CarnetBonoDAO carnetBonoDAO;
    private final ParametroService parametroService;
    private final BonificacionService bonificacionService;
    private final Recibo recibo;
    private final BonificacionUsoDAO bonificacionUsoDAO;
    private final CarnetBonoTipoService carnetBonoTipoService;

    @Autowired
    public PersonaTarjetaBonoService(PersonaTarjetaBonoDAO personaTarjetaBonoDAO,
                                     PersonaSancionService personaSancionService,
                                     PersonaService personaService,
                                     PersonasUJIVinculoDAO personasUJIVinculoDAO,
                                     ParametroDAO parametroDAO,
                                     CarnetBonoDAO carnetBonoDAO,
                                     ParametroService parametroService,
                                     BonificacionService bonificacionService,
                                     Recibo recibo, BonificacionUsoDAO bonificacionUsoDAO, CarnetBonoTipoService carnetBonoTipoService) {
        this.personaTarjetaBonoDAO = personaTarjetaBonoDAO;
        this.personaSancionService = personaSancionService;
        this.personaService = personaService;
        this.personasUJIVinculoDAO = personasUJIVinculoDAO;
        this.parametroDAO = parametroDAO;
        this.carnetBonoDAO = carnetBonoDAO;
        this.parametroService = parametroService;
        this.bonificacionService = bonificacionService;
        this.recibo = recibo;
        this.bonificacionUsoDAO = bonificacionUsoDAO;
        this.carnetBonoTipoService = carnetBonoTipoService;
    }

    public List<TarjetaBono> getTarjetasBonos(Paginacion paginacion, Long personaId) {
        return personaTarjetaBonoDAO.getTarjetasBonos(paginacion, personaId);
    }

    public TarjetaBono getTarjetaBonoById(Long id) {
        return personaTarjetaBonoDAO.getTarjetaBonoById(id);
    }

    public List<TarjetaBono> getHistoricoTarjetasBonos(Long personaId, Long cursoAcademico) {
        return personaTarjetaBonoDAO.getHistoricoTarjetasBonos(personaId, cursoAcademico);
    }

    public void updateTarjetaBono(UIEntity entity, Long tarjetaBonoId, Boolean esBecario) throws CommonException {
        try {
            TarjetaBono tarjetaBono = getTarjetaBono(entity, esBecario);
            personaTarjetaBonoDAO.updateTarjetaBono(tarjetaBono, tarjetaBonoId);
        } catch (Exception e) {
            log.error("No s'ha pogut actualitzar aquesta tarjeta", e);
            throw new CommonException("No s'ha pogut actualitzar aquesta tarjeta");
        }
    }

    private TarjetaBono getTarjetaBono(UIEntity entity, Boolean esBecario) throws ParseException {
        TarjetaBono tarjetaBono = entity.toModel(TarjetaBono.class);
        String estado = tarjetaBono.getEstado().split("-")[0];

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        if (entity.get("fechaInicio") != null)
            tarjetaBono.setFechaInicio(sdf.parse(entity.get("fechaInicio")));

        if (entity.get("fechaFin") != null)
            tarjetaBono.setFechaFin(sdf.parse(entity.get("fechaFin")));

        if (!estado.equals("BA") && !estado.equals("CA") && !esBecario)
            tarjetaBono.setFechaBaja(DateExtensions.getCurrentDate());
        else if (entity.get("fechaBaja") != null)
            tarjetaBono.setFechaBaja(sdf.parse(entity.get("fechaBaja")));

        if (entity.get("observaciones").isEmpty()) {
            tarjetaBono.setObservaciones(null);
        }

        return tarjetaBono;
    }

    private void verificarSancion(Long personaId) throws CommonException {
        Boolean tieneSancion = personaSancionService.tieneSancion(personaId);
        if (tieneSancion) {
            throw new CommonException("Aquesta persona té una sanció i no pot obtindre aquest abonament");
        }
    }

    private CarnetBonoDTO crearCarnetBono(Long personaId, Long tipoBonoId, Long connectedUserId) {
        Long cursoAcademicoId = parametroService.getParametroCursoAcademico(connectedUserId).getId();
        CarnetBonoTipoDTO carnetBonoTipoDTO = carnetBonoTipoService.getCarnetBonoTipoById(tipoBonoId);
        CarnetBonoDTO carnetBonoDTO = new CarnetBonoDTO(personaId, cursoAcademicoId, carnetBonoTipoDTO);
        return carnetBonoDAO.insert(carnetBonoDTO);
    }

    private Long calcularImporteDescuento(BonificacionActivaDTO bonificacionDTO, TarjetaBono tarjetaBono) {
        Long importeDescuento = ParamUtils.isNotNull(bonificacionDTO.getImporte()) ?
                ParamUtils.parseLong(bonificacionDTO.getImporte()) :
                bonificacionDTO.getPorcentaje();

        if ("EUROS".equals(determinarTipoDescuento(bonificacionDTO)) && importeDescuento >= tarjetaBono.getNeto()) {
            importeDescuento = tarjetaBono.getNeto();
        }
        return importeDescuento;
    }

    private String determinarTipoDescuento(BonificacionActivaDTO bonificacionDTO) {
        return ParamUtils.isNotNull(bonificacionDTO.getImporte()) ? "EUROS" : "PORCENTAJE";
    }

    private void aplicarBonificacionSiEsNecesaria(CarnetBonoDTO carnetBonoDTO, Long tipoBonificacionId, Long importeDescuento, String tipoDescuento) {
        if (ParamUtils.isNotNull(tipoBonificacionId)) {
            BonificacionActivaDTO bonificacionDTO = bonificacionService.getBonificacionById(tipoBonificacionId);
            TarjetaBono tarjetaBono = getTarjetaBonoById(carnetBonoDTO.getId());

            importeDescuento = calcularImporteDescuento(bonificacionDTO, tarjetaBono);
            tipoDescuento = determinarTipoDescuento(bonificacionDTO);

            BonificacionUsoDTO bonificacionUsoDTO = new BonificacionUsoDTO(
                    importeDescuento, tipoDescuento, carnetBonoDTO.getId(), new Date(), tipoBonificacionId, null);
            bonificacionUsoDAO.insert(bonificacionUsoDTO);
        }
    }

    private void compruebaSiTieneCarnetBonoActivo(Long personaId, Long tipoBonoId) throws CommonException {
        Boolean tieneCarnetBono = personaTarjetaBonoDAO.tieneTarjetaTipo(personaId, tipoBonoId);
        if (tieneCarnetBono) {
            throw new CommonException("Aquesta persona ja té un carnet o bono d'aquest tipus");
        }
    }


    public void addBono(Long personaId, Long tipoBonoId, Long tipoBonificacionId, Long connectedUserId) throws CommonException {
        verificarSancion(personaId);
        compruebaSiTieneCarnetBonoActivo(personaId, tipoBonoId);
        try {
            personaService.compruebaSiExisteUsuarioEnDeportes(personaId);
            CarnetBonoDTO carnetBonoDTO = crearCarnetBono(personaId, tipoBonoId, connectedUserId);

            Long importeDescuento = null;
            String tipoDescuento = null;
            aplicarBonificacionSiEsNecesaria(carnetBonoDTO, tipoBonificacionId, importeDescuento, tipoDescuento);
            TarjetaBono tarjetaBono = getTarjetaBonoById(carnetBonoDTO.getId());

            TipoDTO vinculo = personasUJIVinculoDAO.getVinculoActualPersona(personaId, parametroDAO.getParametroGlobal("TARIFAS-ACTIVIDADES").getValor());
            recibo.generarRecibo(5L, "TD", carnetBonoDTO.getId(), 1L, tarjetaBono.getNeto(), personaId, vinculo.getId(), tarjetaBono.getNombre(), null, null, importeDescuento, tipoDescuento, null, null, new Date(), personaService.getPersonaById(personaId).getMail(), null);
        } catch (Exception e) {
            log.error("No s'ha pogut inserir aquest abonament", e);
            throw new CommonException("No s'ha pogut inserir aquest abonament");
        }
    }

    public Boolean tieneTarjetaDeportivaActiva(Long personaId) {
        return personaTarjetaBonoDAO.tieneTarjetaDeportiva(personaId);
    }


    public List<CarnetBonoTipo> getTarjetasBonosTiposPuedeDarseDeAlta(Long personaId) {
        List<CarnetBonoTipo> listaCarnetBonoTipo = personaTarjetaBonoDAO.getTarjetasBonosTiposPuedeDarseDeAlta(personaId);
        listaCarnetBonoTipo.forEach(carnetBonoTipo -> {
            Long importe = personaService.dameImpTdybPer(personaId, carnetBonoTipo.getId(), "IMPORTE");
            carnetBonoTipo.setImporte(importe);
        });
        return listaCarnetBonoTipo.stream().filter(carnetBonoTipo -> carnetBonoTipo.getImporte() >= 0L).collect(Collectors.toList());
    }
}
