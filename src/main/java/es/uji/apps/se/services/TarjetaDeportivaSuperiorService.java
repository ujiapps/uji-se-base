package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.TarjetaDeportivaSuperiorDAO;
import es.uji.apps.se.dto.TarjetaDeportivaSuperiorDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TarjetaDeportivaSuperiorService {

    @InjectParam
    private TarjetaDeportivaSuperiorDAO tarjetaDeportivaSuperiorDAO;

    public List<TarjetaDeportivaSuperiorDTO> getAllByTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId) {
        return tarjetaDeportivaSuperiorDAO.getAllByTarjetaDeportivaTipoId(tarjetaDeportivaTipoId);
    }

    public TarjetaDeportivaSuperiorDTO insert(TarjetaDeportivaSuperiorDTO tarjetaDeportivaSuperiorDTO) {
        return tarjetaDeportivaSuperiorDAO.insert(tarjetaDeportivaSuperiorDTO);
    }

    public void delete(Long tarjetaDeportivaSuperiorId) {
        tarjetaDeportivaSuperiorDAO.delete(TarjetaDeportivaSuperiorDTO.class, tarjetaDeportivaSuperiorId);
    }

    public TarjetaDeportivaSuperiorDTO update(TarjetaDeportivaSuperiorDTO tarjetaDeportivaSuperiorDTO) {
        return tarjetaDeportivaSuperiorDAO.update(tarjetaDeportivaSuperiorDTO);
    }
}
