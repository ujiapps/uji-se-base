package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.ClaseDTO;
import es.uji.apps.se.dto.ClaseDirigidaTipoDTO;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.services.ClaseDirigidaTipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("clasedirigidatipo")
public class ClaseDirigidaTipoResource extends CoreBaseService {

    @InjectParam
    ClaseDirigidaTipoService claseDirigidaTipoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClasesDirigidasTipo(@PathParam("tipoId") Long tipoId) {
        return UIEntity.toUI(claseDirigidaTipoService.getClasesDirigidasTipoByTipo(tipoId));

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertClaseDirigidaTipo (@PathParam("tipoId") Long tipoId, UIEntity entity){
        return UIEntity.toUI(claseDirigidaTipoService.insertClaseDirigidaTipo(entityToModel(entity, tipoId)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateClaseDirigidaTipo (@PathParam("tipoId") Long tipoId, UIEntity entity){
        return UIEntity.toUI(claseDirigidaTipoService.updateClaseDirigidaTipo(entityToModel(entity, tipoId)));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteClaseDirigidaTipo (@PathParam("id") Long claseDirigidaTipoId){
        claseDirigidaTipoService.deleteClaseDirigidaTipo(claseDirigidaTipoId);
    }

    private ClaseDirigidaTipoDTO entityToModel (UIEntity entity, Long tipoId){

        ClaseDirigidaTipoDTO claseDirigidaTipoDTO = entity.toModel(ClaseDirigidaTipoDTO.class);

        TipoDTO tipoDTO = new TipoDTO();
        tipoDTO.setId(tipoId);
        claseDirigidaTipoDTO.setTipo(tipoDTO);

        ClaseDTO claseDTO = new ClaseDTO();
        claseDTO.setId(ParamUtils.parseLong(entity.get("claseId")));
        claseDirigidaTipoDTO.setClase(claseDTO);

        return claseDirigidaTipoDTO;

    }

}
