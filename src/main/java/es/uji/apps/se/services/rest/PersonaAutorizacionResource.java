package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public class PersonaAutorizacionResource extends CoreBaseService {
    @InjectParam
    private PersonaAutorizacionService personaAutorizacionService;


    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getAutorizaciones(@QueryParam("start") @DefaultValue("0") Long start,
                                             @QueryParam("limit") @DefaultValue("25") Long limit,
                                             @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                             @PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            responseMessage.setData(UIEntity.toUI(personaAutorizacionService.getAutorizaciones(paginacion, personaId)));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
