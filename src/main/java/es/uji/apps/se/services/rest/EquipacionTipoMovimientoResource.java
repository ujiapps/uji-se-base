package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tipoMovimiento.TipoMovimientoDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tipoMovimiento.TipoMovimientoInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tipoMovimiento.TipoMovimientoUpdateException;
import es.uji.apps.se.model.EquipacionTipoMovimiento;
import es.uji.apps.se.services.EquipacionTipoMovimientoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class EquipacionTipoMovimientoResource extends CoreBaseService {

    public final static Logger LOG = LoggerFactory.getLogger(EquipacionTipoMovimientoResource.class);

    @InjectParam
    EquipacionTipoMovimientoService equipacionTipoMovimientoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTiposMovimiento() {
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(null);

        try {
            List<EquipacionTipoMovimiento> tiposMovimiento = equipacionTipoMovimientoService.getTiposMovimiento();
            responseMessage.setData(UIEntity.toUI(tiposMovimiento));
            responseMessage.setTotalCount(tiposMovimiento.size());
        } catch (Exception e) {
            LOG.error("Error en l'obtenció dels tipus de moviments", e);
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @POST
    public UIEntity addTipoMovimiento(UIEntity entity) throws TipoMovimientoInsertException {
        try {
            EquipacionTipoMovimiento tipoMovimiento = entity.toModel(EquipacionTipoMovimiento.class);
            return UIEntity.toUI(equipacionTipoMovimientoService.addTipoMovimiento(tipoMovimiento));
        }
        catch (Exception e) {
            throw new TipoMovimientoInsertException("Ha hagut un problema al esborrar un tipus de moviment");
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteTipoMovimiento(@PathParam("id") Long id) throws TipoMovimientoDeleteException {
        try {
            equipacionTipoMovimientoService.deleteTipoMovimiento(id);
            return Response.ok().build();
        }
        catch (Exception e) {
            throw new TipoMovimientoDeleteException("Ha hagut un problema al esborrar un tipus de moviment");
        }
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoMovimiento(UIEntity entity) throws TipoMovimientoUpdateException {
        try {
            EquipacionTipoMovimiento tipoMovimiento = entity.toModel(EquipacionTipoMovimiento.class);
            equipacionTipoMovimientoService.updateTipoMovimiento(tipoMovimiento);
            return UIEntity.toUI(tipoMovimiento);
        }
        catch (Exception e) {
            throw new TipoMovimientoUpdateException("Ha hagut un problema al actualitzar un tipus de moviment");
        }
    }
}
