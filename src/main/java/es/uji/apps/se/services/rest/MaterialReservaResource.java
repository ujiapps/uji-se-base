package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.MaterialReservaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("materialreserva")
public class MaterialReservaResource extends CoreBaseService {


    @InjectParam
    MaterialReservaService materialReservaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMaterialesReservaClase(@QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                                    @QueryParam("sort") String ordenacion, @PathParam("id") Long claseId) throws JSONException{
        return UIEntity.toUI(materialReservaService.getMaterialesReservaClase(claseId.toString()));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addMaterialReservaClase(@PathParam("id") Long claseId, UIEntity entity){
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(materialReservaService.addMaterialReservaClase(entity, claseId, connectedUserId));
    }

    @PUT
    @Path("{materialreservaid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateMaterialReserva(@PathParam("materialreservaid") Long materialReservaId, UIEntity entity){
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(materialReservaService.updateMaterialReserva(entity, connectedUserId));
    }

    @DELETE
    @Path("{materialreservaid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteMaterialReserva(@PathParam("materialreservaid") Long materialReservaId, UIEntity entity){
        materialReservaService.deleteMaterialReserva(materialReservaId);
    }


}
