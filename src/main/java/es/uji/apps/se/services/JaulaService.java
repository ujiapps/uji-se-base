package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.JaulaPersonaDAO;
import es.uji.apps.se.dto.JaulaPersonaDTO;
import es.uji.commons.messaging.client.MessageNotSentException;
import org.springframework.stereotype.Service;

@Service
public class JaulaService {

    @InjectParam
    JaulaPersonaDAO jaulaPersonaDAO;

    @InjectParam
    JaulaPersonaService jaulaPersonaService;

    public JaulaPersonaDTO solicitaAccesoJaula(Long connectedUserId) throws MessageNotSentException {

        return jaulaPersonaService.insertaJaulaPersona(connectedUserId);
    }

    public JaulaPersonaDTO getAccesoJaula(Long personaId) {
        return jaulaPersonaDAO.getAccesoJaula(personaId);
    }
}
