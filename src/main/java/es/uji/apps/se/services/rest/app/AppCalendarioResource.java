package es.uji.apps.se.services.rest.app;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.PersonaDTO;
import es.uji.apps.se.exceptions.NoAutorizadoException;
import es.uji.apps.se.exceptions.VinculoNoContempladoException;
import es.uji.apps.se.model.Sancion;
import es.uji.apps.se.model.ui.UICalendario;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.services.*;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class AppCalendarioResource extends CoreBaseService {

    @InjectParam
    PersonaService personaService;

    @InjectParam
    ReservasService reservasService;

    @InjectParam
    CalendarioService calendarioService;

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;

    @InjectParam
    PersonaSancionService personaSancionService;


    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template calendario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws NoAutorizadoException, ParseException, VinculoNoContempladoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        TarjetaDeportiva tarjetaDeportiva = tarjetaDeportivaService.getTarjetaDeportivaActivaByPersonaId(connectedUserId);
        Sancion sancion = personaSancionService.getSancionBloqueaTodoOrClasesDirigidas(connectedUserId);

        Template template = AppInfo.buildPaginaAplicacionClases("se/app/calendario", idioma);
        anyadirSancionReservaAlTemplate(sancion, template);
        template.put("titulo", "Calendari d'activitats");
        template.put("tarjetaDeportiva", tarjetaDeportiva);

        UICalendario calendario = new UICalendario(LocalDate.now(), idioma);
        template.put("calendario", calendario);
        template.put("dias", reservasService.getDiasPuedeReservarUsuario());
        template.put("festivos", calendarioService.getDiasFestivos(calendario.getAnyo(), calendario.getMes()));
        return template;
    }

    @GET
    @Path("eventos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> eventosCalendario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @QueryParam("start") String start, @QueryParam("end") String end)
            throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
//        PersonaDTO personaDTO = personaService.getPersonaById(connectedUserId);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date startDate = formatter.parse(start);
        Date endDate = formatter.parse(end);
        return UIEntity.toUI(reservasService.getActividadesCalendario(connectedUserId, startDate, endDate));
    }

    private void anyadirSancionReservaAlTemplate(Sancion sancion, Template template) {
        if (ParamUtils.isNotNull(sancion)) {
            if (sancion.getTipo().equals("CLA") || sancion.getTipo().equals("ALL")) {
                String motivo = ParamUtils.isNotNull(sancion.getMotivo())? sancion.getMotivo() : "No s'ha especificat el motiu";
                if (ParamUtils.isNotNull(sancion.getFechaFin())) {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins al dia {0}. Motiu: {1}", DateExtensions.getDateAsString(sancion.getFechaFin()), motivo));
                } else {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins que resolgues la incidència: {0}", motivo));
                }
            } else
                template.put("mensajeInfo", null);
        } else {
            template.put("mensajeInfo", null);
        }

        if (ParamUtils.isNotNull(sancion) && (sancion.getTipo().equals("CLA") || sancion.getTipo().equals("ALL"))) {
            template.put("classSancion", "disabled");
        } else {
            template.put("classSancion", "");
        }
    }
}
