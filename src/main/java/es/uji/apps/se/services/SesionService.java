package es.uji.apps.se.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.se.dao.SesionDAO;

@Service
public class SesionService
{

    private SesionDAO sesionDAO;

    @Autowired
    public SesionService(SesionDAO sesionDAO)
    {
        this.sesionDAO = sesionDAO;
    }

    public Long getPersonaByHash(String hash)
    {
        return sesionDAO.getPersonaByHash(hash);
    }
}
