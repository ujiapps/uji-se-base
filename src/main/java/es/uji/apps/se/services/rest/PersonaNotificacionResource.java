package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public class PersonaNotificacionResource extends CoreBaseService {

    @InjectParam
    PersonaNotificacionService personaNotificacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getNotificacionesPersona(@PathParam("personaId") Long personaId,
                                                    @QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                                    @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            responseMessage.setData(UIEntity.toUI(personaNotificacionService.getNotificacionesPersona(personaId, paginacion)));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
