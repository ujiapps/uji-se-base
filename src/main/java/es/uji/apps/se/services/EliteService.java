package es.uji.apps.se.services;

import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.ModalidadDeportiva;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EliteService {

    private es.uji.apps.se.dao.EliteDAO eliteDAO;
    private ParametroService parametroService;
    private PersonaTitulacionService personaTitulacionService;
    private PersonaFicheroService personaFicheroService;

    @Autowired
    public EliteService(es.uji.apps.se.dao.EliteDAO eliteDAO, ParametroService parametroService, PersonaTitulacionService personaTitulacionService, PersonaFicheroService personaFicheroService) {

        this.eliteDAO = eliteDAO;
        this.parametroService = parametroService;
        this.personaTitulacionService = personaTitulacionService;
        this.personaFicheroService = personaFicheroService;
    }

    public List<EliteDTO> getListadoDeportistasElite(Long connectedUserId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        return eliteDAO.getListadoDeportistasElite(cursoAcademico);
    }

    public List<EliteMatriculaDTO> getListadoDeportistasEliteMatricula(Long connectedUserId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        return eliteDAO.getListadoDeportistasEliteMatricula(cursoAcademico);
    }

    public List<UIEntity> getListadoProfesoresConAlumnosElite(Long connectedUserId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        return eliteDAO.getListadoProfesoresConAlumnosElite(cursoAcademico);
    }

    public List<UIEntity> getListadoProfesoresConAlumnosEliteFaltanEnvios(ParametroDTO cursoAcademico) {

//        Parametro cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        return eliteDAO.getListadoProfesoresConAlumnosEliteFaltanEnvios(cursoAcademico);
    }

    public List<AlumnoAsignaturaDTO> getListadoAlumnosElitePorProfesor(Long connectedUserId, Long profesorId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        return eliteDAO.getListadoAlumnosElitePorProfesor(cursoAcademico, profesorId);
    }

    public List<DecanoEliteDTO> getListadoDecanosConAlumnosElite(Long connectedUserId) {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        return eliteDAO.getListadoDecanosConAlumnosElite(cursoAcademico);
    }

    public List<DecanoEliteDTO> getListadoDecanosConAlumnosEliteFaltaEnvio(Long connectedUserId) {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        return eliteDAO.getListadoDecanosConAlumnosEliteFaltaEnvio(cursoAcademico);
    }

    public List<PersonaTitulacionDTO> getTitulacionesElite(Long connectedUserId, Long eliteId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        EliteDTO eliteDTO = getEliteById(eliteId);

        return personaTitulacionService.getTitulacionesElite(eliteDTO.getPersonaUji().getId(), cursoAcademico.getId());
    }

    private EliteDTO getEliteById(Long eliteId) {
        return eliteDAO.getEliteById(eliteId);
    }

    public List<PersonaFicheroDTO> getDocumentosElite(Long eliteId) {
        EliteDTO eliteDTO = getEliteById(eliteId);

        return personaFicheroService.getDocumentosElite(eliteDTO.getPersonaUji().getId());
    }

    public EliteDTO getDeporistaEliteByPersonaAndCurso(Long personaId, CursoAcademicoDTO cursoAcademicoDTO) {
        return eliteDAO.getDeporistaEliteByPersonaAndCurso(personaId, cursoAcademicoDTO);
    }

    public List<ModalidadDeportiva> getEliteByPersona(Long personaId) {
        return eliteDAO.getEliteByPersona(personaId);
    }

    public void updateElitePersona(Long id, UIEntity entity) throws CommonException {
        try {
            Long cursoAca = Long.decode(entity.get("cursoAca"));
            Long modalidadNombre = Long.decode(entity.get("modalidadNombre"));

            eliteDAO.updateElitePersona(id, cursoAca, modalidadNombre);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException(e.getMessage());
        }
    }

    public void addElitePersona(Long personaId, UIEntity entity) throws CommonException {
        try {
            Long cursoAca = Long.decode(entity.get("cursoAca"));
            Long modalidadNombre = Long.decode(entity.get("modalidadNombre"));

            EliteDTO eliteDTO = new EliteDTO(personaId, cursoAca, modalidadNombre);
            eliteDAO.insert(eliteDTO);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    public void deleteElitePersona(Long id) throws CommonException {
        try {
            eliteDAO.deleteElitePersona(id);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }
}
