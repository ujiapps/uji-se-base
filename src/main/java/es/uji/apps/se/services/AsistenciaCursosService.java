package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.AsistenciaCursosDAO;
import es.uji.apps.se.model.AsistenciaCursos;
import es.uji.apps.se.model.ListadoFaltasCursos;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AsistenciaCursosService {
    @InjectParam
    private AsistenciaCursosDAO asistenciaCursosDAO;

    public List<AsistenciaCursos> getAsistenciaCursos(Long perId) {
        return asistenciaCursosDAO.getAsistenciaCursos(perId);
    }

    public List<ListadoFaltasCursos> getListadoFaltasCursos(Long perId, Long cursoId) {
        return asistenciaCursosDAO.getListadoFaltasCursos(perId, cursoId);
    }

    public Long getDiasActiv(Long cursoId) {
        return asistenciaCursosDAO.getDiasActiv(cursoId);
    }

    public Long getDiasAsis(Long cursoId, Long personaId) {
        return asistenciaCursosDAO.getDiasAsis(cursoId, personaId);
    }
}
