package es.uji.apps.se.services;

import es.uji.apps.se.dao.CarnetBonoTipoDAO;
import es.uji.apps.se.dto.CarnetBonoTipoDTO;
import es.uji.apps.se.model.Bono;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public class CarnetBonoTipoService {

    private final CarnetBonoTipoDAO carnetBonoTiposDAO;
    @Autowired
    public CarnetBonoTipoService(CarnetBonoTipoDAO carnetBonoTiposDAO){
        this.carnetBonoTiposDAO = carnetBonoTiposDAO;
    }

    public List<CarnetBonoTipoDTO> getCarnetBonoTipos() {
        return carnetBonoTiposDAO.getCarnetBonoTipos();
    }

    public List<Bono> getUsuarioTarjetas() throws ParseException {
        return carnetBonoTiposDAO.getUsuarioTarjetas();
    }

    public CarnetBonoTipoDTO getCarnetBonoTipoById(Long tipoBonoId) {
        return carnetBonoTiposDAO.getCarnetBonoTipoById(tipoBonoId);
    }
}
