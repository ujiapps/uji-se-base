package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.MonitorCalendarioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("monitorcalendario")
public class MonitorCalendarioResource extends CoreBaseService
{
    @InjectParam
    MonitorCalendarioService monitorCalendarioService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMonitoresByCalendarioClase(@PathParam("id") Long calendarioClaseId){
        return UIEntity.toUI(monitorCalendarioService.getMonitoresCalendario(calendarioClaseId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertMonitorClase(@PathParam("id") Long calendarioClaseId, UIEntity entity)
    {
        return UIEntity.toUI(monitorCalendarioService.insertMonitorCalendario(calendarioClaseId, entity));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public void insertMonitorMultipleClase(@PathParam("claseId") Long claseId, @FormParam("fechaInicio") String fechaInicio, @FormParam("fechaFin") String fechaFin, @FormParam("horaInicio") String horaInicio,
                                             @FormParam("horaFin") String horaFin, @FormParam("diasSemana") List<Integer> diasSemana, @FormParam("monitorCursoAcademicoId") List<Long> monitoresCursoAcademico)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        monitorCalendarioService.insertMonitorMultipleClase(connectedUserId, claseId, fechaInicio, fechaFin, horaInicio, horaFin, diasSemana, monitoresCursoAcademico);
    }

    @PUT
    @Path("{monitorCalendarioClaseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertMonitorClase(@PathParam("id") Long calendarioClaseId, @PathParam("monitorCalendarioClaseId") Long monitorCalendarioClaseId,
                                       UIEntity entity)
    {
        return UIEntity.toUI(monitorCalendarioService.updateMonitorCalendarioClase(calendarioClaseId, entity));
    }

    @DELETE
    @Path("{monitorCalendarioClaseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteMonitorClase(@PathParam("monitorCalendarioClaseId") Long monitorCalendarioClaseId)
    {
        monitorCalendarioService.deleteMonitorCalendarioClase(monitorCalendarioClaseId);
    }
}
