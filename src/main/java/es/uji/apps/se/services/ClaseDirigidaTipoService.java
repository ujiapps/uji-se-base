package es.uji.apps.se.services;

import es.uji.apps.se.dao.ClaseDirigidaTiposDAO;
import es.uji.apps.se.dto.ClaseDirigidaTipoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClaseDirigidaTipoService {

    private ClaseDirigidaTiposDAO claseDirigidaTiposDAO;

    @Autowired
    public ClaseDirigidaTipoService(ClaseDirigidaTiposDAO claseDirigidaTiposDAO) {
        this.claseDirigidaTiposDAO = claseDirigidaTiposDAO;
    }

    public List<ClaseDirigidaTipoDTO> getClasesDirigidasTipoByTipo(Long tipoId) {
        return claseDirigidaTiposDAO.getClasesDirigidasTipoByTipo(tipoId);
    }

    public ClaseDirigidaTipoDTO insertClaseDirigidaTipo(ClaseDirigidaTipoDTO claseDirigidaTipoDTO) {

        return claseDirigidaTiposDAO.insert(claseDirigidaTipoDTO);

    }

    public ClaseDirigidaTipoDTO updateClaseDirigidaTipo(ClaseDirigidaTipoDTO claseDirigidaTipoDTO) {
        return claseDirigidaTiposDAO.update(claseDirigidaTipoDTO);
    }

    public void deleteClaseDirigidaTipo(Long claseDirigidaTipoId) {
        claseDirigidaTiposDAO.delete(ClaseDirigidaTipoDTO.class, claseDirigidaTipoId);
    }

    public void deleteClaseDirigidaTipoByClase(Long claseId) {
        claseDirigidaTiposDAO.deleteClaseDirigidaTipoByClaseId(claseId);
    }
}
