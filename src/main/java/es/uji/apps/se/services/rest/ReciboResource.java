package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.ReciboService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("recibo")
public class ReciboResource extends CoreBaseService {

    private static final Logger log = LoggerFactory.getLogger(ReciboResource.class);

    @InjectParam
    ReciboService reciboService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{referenciaId}")
    public UIEntity getUrlRecibo(@PathParam("referenciaId") Long referenciaId) {
        UIEntity response = reciboService.getReciboByReferenciaId(referenciaId);

        if (ParamUtils.isNotNull(response.get("reciboId")))
            response.put("url", "https://e-ujier.uji.es/pls/www/!ficha_recibo.inicio?p_recibo=" + response.get("reciboId"));
        return response;
    }
}
