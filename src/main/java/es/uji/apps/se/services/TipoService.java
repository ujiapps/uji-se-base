package es.uji.apps.se.services;

import es.uji.apps.se.dao.TipoDAO;
import es.uji.apps.se.dto.GrupoDTO;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones.TiposAgrupacionesDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones.TiposAgrupacionesInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones.TiposAgrupacionesUpdateException;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.Tipo;
import es.uji.apps.se.model.domains.TipoTaxonomia;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoService {

    private TipoDAO tipoDAO;
    private GrupoService grupoService;
    Long CLASIFICACION_ACTIVIDADES = 7329L;


    @Autowired
    public TipoService(TipoDAO tipoDAO, GrupoService grupoService) {

        this.tipoDAO = tipoDAO;
        this.grupoService = grupoService;
    }

    public List<TipoDTO> getTiposClasificacion() {
        return tipoDAO.getTiposClasificacion();
    }

    public List<Tipo> getTipos(Long clasificacionId) {
        return tipoDAO.getTipos(clasificacionId);
    }

    public List<TipoDTO> getTiposTaxonomias(TipoTaxonomia tipoTaxonomia) {
        return tipoDAO.getTiposTaxonomias(tipoTaxonomia);
    }

    public TipoDTO insertTipoTaxonomia(TipoDTO tipoDTO, TipoTaxonomia tipoTaxonomia, Long padreId) {

        if (padreId.equals(0L)) {
            GrupoDTO grupoDTO = grupoService.getGrupoByTaxonomia(tipoTaxonomia);
            tipoDTO.setGrupo(grupoDTO);
        } else {

            TipoDTO padre = new TipoDTO();
            padre.setId(padreId);
            tipoDTO.setTipoPadreDTO(padre);
        }

        tipoDTO.setId(null);

        return tipoDAO.insert(tipoDTO);
    }

    public TipoDTO updateTipo(UIEntity entity) {
        TipoDTO tipoDTO = entity.toModel(TipoDTO.class);
        return tipoDAO.update(tipoDTO);
    }

    public TipoDTO updateTipo(TipoDTO tipoDTO) {
        return tipoDAO.update(tipoDTO);
    }

    public TipoDTO updateTipo(Long tipoId, String newValue){
        TipoDTO tipoDTO = getTipoById(tipoId);
        tipoDTO.setNombre(newValue);
        return tipoDAO.update(tipoDTO);
    }


    public void deleteTipo(Long tipoId) {
        tipoDAO.delete(TipoDTO.class, tipoId);
    }

    public List<TipoDTO> getTipos(Long node, TipoTaxonomia tipoTaxonomia) {
        if (node.equals(0L)) {
            return tipoDAO.getTiposTaxonomias(tipoTaxonomia);
        } else {
            return tipoDAO.getTiposByParentId(node);
        }
    }

    public Boolean tieneHijos(Long tipoId) {
        return tipoDAO.tieneHijos(tipoId);
    }

    public List<TipoDTO> getVinculosTarifas() {
        return tipoDAO.getVinculosTarifas();
    }

    public TipoDTO getTipoById(Long tipoId) {
        return tipoDAO.getTipoById(tipoId);
    }

    public List<TipoDTO> getTiposByUso(String uso, Long grupoId, Paginacion paginacion) {
        return tipoDAO.getTiposByUso(uso, grupoId, paginacion);
    }

    public TipoDTO insertTipoTarifaActividades(UIEntity entity) {
        TipoDTO tipoDTO = entity.toModel(TipoDTO.class);
        TipoDTO tipoPadreDTO = tipoDAO.getTipoPadre("TARIFA1", CLASIFICACION_ACTIVIDADES);
        tipoDTO.setTipoPadreDTO(tipoPadreDTO);
        return tipoDAO.insert(tipoDTO);
    }

    public List<TipoDTO> getTiposByPadreId(Long tipoPadreId) {

        return tipoDAO.getTiposByParentId(tipoPadreId);
    }

    public List<TipoDTO> getTiposByPeriodoId(Long periodoId) {
        return tipoDAO.getTiposByParentId(periodoId);
    }

    public List<TipoDTO> getZonas() {
        return tipoDAO.getZonas();
    }

    public List<TipoDTO> getTiposAgrupaciones() {
        return tipoDAO.getTiposAgrupaciones();
    }

    public TipoDTO addTiposAgrupaciones(UIEntity entity) throws TiposAgrupacionesInsertException {
        Long GRUPO_TIPOS_GRUPO = 9266975L;

        try {
            TipoDTO tipoDTO = entity.toModel(TipoDTO.class);
            GrupoDTO grupo = grupoService.getGrupoByGrupoId(GRUPO_TIPOS_GRUPO);
            tipoDTO.setGrupo(grupo);
            tipoDTO.setOrden(0L);
            return tipoDAO.insert(tipoDTO);
        } catch (Exception e) {
            throw new TiposAgrupacionesInsertException();
        }
    }

    public TipoDTO updateTiposAgrupaciones(UIEntity entity) throws TiposAgrupacionesUpdateException {
        Long GRUPO_TIPOS_GRUPO = 9266975L;

        try {
            TipoDTO tipoDTO = entity.toModel(TipoDTO.class);
            tipoDTO.setGrupo(grupoService.getGrupoByGrupoId(GRUPO_TIPOS_GRUPO));
            tipoDTO.setOrden(0L);
            return tipoDAO.update(tipoDTO);
        } catch (Exception e) {
            throw new TiposAgrupacionesUpdateException();
        }
    }

    public void deleteTiposAgrupaciones(Long tipoId) throws TiposAgrupacionesDeleteException {
        try {
            tipoDAO.delete(TipoDTO.class, tipoId);
        } catch (Exception e) {
            throw new TiposAgrupacionesDeleteException();
        }
    }
}
