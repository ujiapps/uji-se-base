package es.uji.apps.se.services.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.exceptions.ErrorFiltroEnviosClases;
import es.uji.apps.se.model.EnvioFiltro;
import es.uji.apps.se.services.EnvioFiltroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

public class EnvioFiltroResource extends CoreBaseService {

    @InjectParam
    EnvioFiltroService envioFiltroService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public EnvioFiltro getFiltro(@PathParam("envioId") Long envioId) throws JSONException {
        List<EnvioFiltroDTO> envioFiltros = envioFiltroService.getEnvioFiltrosByEnvioId(envioId);

        EnvioFiltro envioFiltro = new EnvioFiltro(envioId, envioFiltros);
        return envioFiltro;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage addFiltroAEnvio(@PathParam("envioId") Long envioId,
                                           @FormParam("data") String data) throws ErrorFiltroEnviosClases {

        EnvioFiltro envioFiltro;
        try {
            envioFiltro = new ObjectMapper().readValue(data, EnvioFiltro.class);
            envioFiltro.setEnvio(envioId);
            return envioFiltroService.addFiltroAEnvio(envioFiltro);
        } catch (IOException e) {
            throw new ErrorFiltroEnviosClases(e.getMessage());
        }
    }

    @POST
    @Path("todos")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addFiltroTodosAEnvio(@PathParam("envioId") Long envioId) throws ErrorFiltroEnviosClases {

        return envioFiltroService.addFiltroTodosAEnvio(envioId);
    }

    @GET
    @Path("alquiler/todos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> muestraAlquileresTodos(@PathParam("envioId") Long envioId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return envioFiltroService.muestraAlquileresTodos(connectedUserId);
    }

    @GET
    @Path("actividad/filtro")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> muestraActividadesFiltro(@PathParam("envioId") Long envioId, @QueryParam("periodoId") Long periodoId,
                                                   @QueryParam("clasificacionId") Long clasificacionId, @QueryParam("actividadId") Long actividadId,
                                                   @QueryParam("grupoId") Long grupoId, @QueryParam("apto") Boolean apto,  @QueryParam("noApto") Boolean noApto,
                                                   @QueryParam("inscritos") Boolean inscritos,  @QueryParam("espera") Boolean espera) {

        return envioFiltroService.muestraActividadesFiltro(periodoId, clasificacionId, actividadId, grupoId, apto, noApto, inscritos, espera);
    }
}
