package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.PersonaIbanDAO;
import es.uji.apps.se.dto.PersonaIbanDTO;
import es.uji.apps.se.dao.GenerarPersonaIban;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaIbanService {

    @InjectParam
    GenerarPersonaIban generarIban;

    private PersonaIbanDAO personaIbanDAO;

    @Autowired
    public PersonaIbanService(PersonaIbanDAO personaIbanDAO) {

        this.personaIbanDAO = personaIbanDAO;
    }

    public List<PersonaIbanDTO> getListaPersonaIbanByPersonaId(Long personaId) {
        return personaIbanDAO.getListaPersonaIbanByPersonaId(personaId);
    }

    public void anyadirPersonaIban(Long connectedUserId, String iban) {
        generarIban.generarIban(connectedUserId, iban);
    }
}
