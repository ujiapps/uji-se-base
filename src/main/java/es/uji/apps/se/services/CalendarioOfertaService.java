package es.uji.apps.se.services;

import es.uji.apps.se.dao.CalendarioOfertaDAO;
import es.uji.apps.se.dto.CalendarioDTO;
import es.uji.apps.se.dto.CalendarioOfertaDTO;
import es.uji.apps.se.dto.OfertaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class CalendarioOfertaService {


    private CalendarioOfertaDAO calendarioOfertaDAO;
    private CalendarioService calendarioService;


    @Autowired
    public CalendarioOfertaService(CalendarioOfertaDAO calendarioOfertaDAO, CalendarioService calendarioService) {

        this.calendarioOfertaDAO = calendarioOfertaDAO;
        this.calendarioService = calendarioService;
    }

    @Transactional
    public void generarCalendariosOfertaDestino(OfertaDTO ofertaDTO, OfertaDTO ofertaDestinoDTO) {

        List<Map<String, String>> diasCalendariosOfertaOrigen = calendarioOfertaDAO.getDiasYHorasCalendariosOfertaByOferta(ofertaDTO);

        for (Map<String, String> diaCalendariosOfertaOrigen : diasCalendariosOfertaOrigen) {
            List<CalendarioDTO> diasCalendarioDestinoDTO = calendarioService.getDiasSemanaByFechaInicioAndFechaFin(ofertaDestinoDTO.getPeriodo().getFechaInicio(), ofertaDestinoDTO.getPeriodo().getFechaFin(), Integer.valueOf(diaCalendariosOfertaOrigen.get("diaSemana")));

            for (CalendarioDTO calendarioDTO : diasCalendarioDestinoDTO) {
                CalendarioOfertaDTO calendarioOfertaDTO = new CalendarioOfertaDTO();
                calendarioOfertaDTO.setOferta(ofertaDestinoDTO);
                calendarioOfertaDTO.setHoraFin(diaCalendariosOfertaOrigen.get("horaFin"));
                calendarioOfertaDTO.setHoraInicio(diaCalendariosOfertaOrigen.get("horaInicio"));
                calendarioOfertaDTO.setCalendario(calendarioDTO);
                calendarioOfertaDTO.setAsistencia(Boolean.FALSE);
                calendarioOfertaDAO.insert(calendarioOfertaDTO);
            }
        }
    }

    public void eliminaCalendariosOfertaOferta(OfertaDTO ofertaDTO) {
        calendarioOfertaDAO.delete(CalendarioOfertaDTO.class, "oferta_id = " + ofertaDTO.getId());
    }
}
