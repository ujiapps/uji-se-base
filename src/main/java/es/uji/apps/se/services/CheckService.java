package es.uji.apps.se.services;

import es.uji.apps.se.dao.CheckDAO;
import es.uji.apps.se.dto.CheckDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CheckService {

    private CheckDAO checkDAO;

    @Autowired
    public CheckService(CheckDAO checkDAO) {

        this.checkDAO = checkDAO;
    }

    public List<CheckDTO> getCheckByTarjetaDeportivaId(Long tarjetaDeportivaId) {
        return checkDAO.getCheckByTarjetaDeportivaId(tarjetaDeportivaId);
    }
}
