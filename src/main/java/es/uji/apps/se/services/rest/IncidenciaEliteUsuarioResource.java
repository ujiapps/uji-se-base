package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.EliteIncidenciaTipoDTO;
import es.uji.apps.se.services.AlumnoAsignaturaService;
import es.uji.apps.se.services.CursoAcademicoService;
import es.uji.apps.se.services.EliteIncidenciaTipoService;
import es.uji.apps.se.services.IncidenciaEliteUsuarioService;
import es.uji.apps.se.ui.IncidenciaEliteUI;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Path("incidenciaeliteusuario")
public class IncidenciaEliteUsuarioResource extends CoreBaseService {

    @InjectParam
    IncidenciaEliteUsuarioService incidenciaEliteUsuarioService;
    @InjectParam
    EliteIncidenciaTipoService eliteIncidenciaTipoService;
    @InjectParam
    CursoAcademicoService cursoAcademicoService;
    @InjectParam
    AlumnoAsignaturaService alumnoAsignaturaService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getIncidencias(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Template template = AppInfo.buildPagina("se/base", idioma);
        template.put("contenido", "se/elite/incidencias");
        template.put("listadoTipoIncidencias", eliteIncidenciaTipoService.getEliteIncidenciasTipo().stream().map(tipo -> {
            EliteIncidenciaTipoDTO incidenciaTipo = new EliteIncidenciaTipoDTO();
            incidenciaTipo.setId(tipo.getId());
            incidenciaTipo.setNombre(tipo.getNombre(idioma));
            return incidenciaTipo;
        }).collect(Collectors.toList()));
        template.put("tipoFormularioIncidencia", "se/elite/vacio");

        return template;

    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("listado")
    public Template getListadoIncidencias(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Template template = AppInfo.buildPagina("se/base", idioma);
        template.put("contenido", "se/elite/listadoincidencias");
        template.put("listadoIncidencias", incidenciaEliteUsuarioService.getIncidenciasByPersonaId(connectedUserId));

        return template;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("tipoformulario/{id}")
    public Template getFormulario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("id") int tipoFormulario) throws ParseException {

        Template template;
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        switch (tipoFormulario) {
            case 1:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/grupos", idioma);
                break;
            case 2:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/examen", idioma);
                break;
            case 3:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/exencion", idioma);
                break;
            case 4:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/becas", idioma);
                break;
            case 5:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/convocatorias", idioma);
                break;
            case 6:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/permanencia", idioma);
                break;
            case 7:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/asistencia", idioma);
                break;
            case 8:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/otros", idioma);
                break;
            default:
                template = AppInfo.buildPagina("se/elite/formulariosIncidencias/error", idioma);
                break;
        }
        template.put("listadoCursosAcademicos", cursoAcademicoService.getCursosAcademicosUsuario(connectedUserId));
        return template;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage insertDetalleFormulario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @FormParam("tipoIncidenciaId") Long tipoIncidenciaId,
                                                   @FormParam("motivo") String motivoIncidencia, @FormParam("detalle") String detalleIncidencia, @FormParam("curso") Long curso,
                                                   @FormParam("asignatura") String asignatura, @FormParam("fecha") String fecha, @FormParam("profesor") Long profesorId,
                                                   @FormParam("fechaCambio") String fechaCambio, @FormParam("asignaturasMatricula") String asignaturasMatricula,
                                                   @FormParam("asignaturasAnula") String asignaturasAnula) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        IncidenciaEliteUI incidenciaEliteUI = new IncidenciaEliteUI();
        incidenciaEliteUI.setAsignatura(asignatura);
        incidenciaEliteUI.setCursoAcademico(curso);
        incidenciaEliteUI.setDetalle(detalleIncidencia);
        incidenciaEliteUI.setFechaCambio(fechaCambio);
        incidenciaEliteUI.setFecha(fecha);
        incidenciaEliteUI.setMotivo(motivoIncidencia);
        incidenciaEliteUI.setPersona(connectedUserId);
        incidenciaEliteUI.setProfesor(profesorId);
        incidenciaEliteUI.setTipoIncidencia(tipoIncidenciaId);
        incidenciaEliteUI.setAsignaturasMatricula(asignaturasMatricula);
        incidenciaEliteUI.setAsignaturasAnula(asignaturasAnula);

        incidenciaEliteUsuarioService.insertarIncidenciaElite(incidenciaEliteUI);

        return new ResponseMessage(true);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("asignaturascurso/curso/{id}")
    public List<UIEntity> getAsignaturasByCursoAndPersonaId(@PathParam("id") Long curso) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(alumnoAsignaturaService.getAsignaturasByCursoAndPersonaId(curso, connectedUserId));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("profesorasignatura/asignatura/{id}/curso/{curso}")
    public List<UIEntity> getProfesorAsignaturaByAsignatura(@PathParam("id") String asignatura, @PathParam("curso") Long curso) {
        return UIEntity.toUI(alumnoAsignaturaService.getProfesorAsignaturaByAsignatura(asignatura, curso));
    }
}
