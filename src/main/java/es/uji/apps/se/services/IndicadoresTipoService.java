package es.uji.apps.se.services;

import es.uji.apps.se.dao.IndicadoresTiposDAO;
import es.uji.apps.se.model.IndicadoresTipos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndicadoresTipoService {

    @Autowired
    private IndicadoresTiposDAO indicadoresTiposDAO;

    public List<IndicadoresTipos> getIndicadoresTipo() {
        return indicadoresTiposDAO.getIndicadoresTipo();
    }
}
