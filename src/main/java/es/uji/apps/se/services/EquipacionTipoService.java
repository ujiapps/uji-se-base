package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionTipoDAO;
import es.uji.apps.se.dto.EquipacionTipoDTO;
import es.uji.apps.se.model.EquipacionTipo;
import es.uji.commons.rest.CoreBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EquipacionTipoService extends CoreBaseService {

    @Autowired
    EquipacionTipoDAO equipacionTipoDAO;

    public List<EquipacionTipo> getTiposEquipacion() {
        return equipacionTipoDAO.getTiposEquipacion();
    }

    @Transactional
    public EquipacionTipo addTipoEquipacion(EquipacionTipo tipo) {
        return new EquipacionTipo(equipacionTipoDAO.insert(new EquipacionTipoDTO(tipo)));
    }

    public void deleteTipoEquipacion(Long id) {
        equipacionTipoDAO.deleteTipoEquipacion(id);
    }

    public void updateTipoEquipacion(EquipacionTipo tipo) {
        equipacionTipoDAO.updateTipoEquipacion(tipo);
    }
}
