package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.VinculacionesDAO;
import es.uji.apps.se.model.Vinculaciones;
import org.springframework.stereotype.Service;

@Service
public class VinculacionesService {
    @InjectParam
    private VinculacionesDAO vinculacionesDAO;

    public Vinculaciones getVinculacionesPersona(Long personaId, Long curso) {
        return vinculacionesDAO.getVinculacionesPersona(personaId, curso);
    }
}
