package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.*;

public class PersonaBonificacionResource extends CoreBaseService {

    private static final Logger log = LoggerFactory.getLogger(PersonaBonificacionResource.class);

    @InjectParam
    BonificacionService bonificacionService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage getBonificaciones(@PathParam("personaId") Long personaId) {
        ResponseMessage responseMessage = new ResponseMessage(false);

        try {
            List<Bonificacion> bonificaciones = bonificacionService.getBonificaciones(personaId);
            responseMessage.setData(UIEntity.toUI(bonificaciones));
            responseMessage.setSuccess(true);
            return responseMessage;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @GET
    @Path("activas")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getBonificacionesActivas(@PathParam("personaId") Long personaId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<BonificacionActiva> bonificacionActivas = bonificacionService.getBonificacionesAsociadasAActividadesDePersonaByAdmin(personaId);
            responseMessage.setTotalCount(bonificacionActivas.size());
            responseMessage.setData(UIEntity.toUI(bonificacionActivas));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getBonificacionesActivas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addBonificacion(@PathParam("personaId") Long personaId) {
        try {
            bonificacionService.addBonificacion(personaId);
            return Response.ok().build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @DELETE
    @Path("{bonificacionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteBonificacion(@PathParam("bonificacionId") Long bonificacionId) {
        try {
            bonificacionService.deleteBonificacion(bonificacionId);
            return Response.ok().build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PUT
    @Path("{bonificacionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateBonificacion(@PathParam("bonificacionId") Long bonificacionId,
                                       UIEntity entity) throws CommonException {
        bonificacionService.updateBonificacion(entity);
        return Response.ok().build();
    }

    @GET
    @Path("{bonificacionId}/detalle")
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage getBonificacionDetalles(@PathParam("bonificacionId") Long bonificacionId) {
        ResponseMessage responseMessage = new ResponseMessage(false);

        try {
            List<BonificacionDetalle> bonificaciones = bonificacionService.getBonificacionDetalles(bonificacionId);
            responseMessage.setData(UIEntity.toUI(bonificaciones));
            responseMessage.setSuccess(true);
            return responseMessage;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PUT
    @Path("asignar")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response asignarBonificacion(UIEntity entity) {
        try {
            bonificacionService.asignarBonificacion(entity.getLong("referenciaId"), entity.getLong("bonificacionId"), entity.get("origen"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return Response.ok().build();
    }

    @GET
    @Path("tipocarnet/{tipoBonoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getBonificacionesCarnetsOBonoPersonaByAdmin(@PathParam("personaId") Long personaId,
                                                 @PathParam("tipoBonoId") Long tipoBonoId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<BonificacionActiva> bonificaciones = bonificacionService.getBonificacionesCarnetsOBonoPersonaByAdmin(personaId, tipoBonoId);
            responseMessage.setTotalCount(bonificaciones.size());
            responseMessage.setData(UIEntity.toUI(bonificaciones));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getBonificacionesCarnetsOBonoPersonaByAdmin", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
