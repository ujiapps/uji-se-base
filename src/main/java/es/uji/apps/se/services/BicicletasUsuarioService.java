package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.BicicletaTiposReservasDTO;
import es.uji.apps.se.dto.BicicletasReservasDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.HistoricoBicicletas;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.ReservaBicicletas;
import es.uji.apps.se.model.TipoIdentificacion;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class BicicletasUsuarioService {
    @InjectParam
    private BicicletasUsuarioDAO bicicletasUsuarioDAO;
    @InjectParam
    private BicicletasGestionDAO bicicletasGestionDAO;
    @InjectParam
    private PersonaDAO personaDAO;
    @InjectParam
    private PersonaSancionDAO personaSancionDAO;

    public List<ReservaBicicletas> getListadoReservas(Long perId, Long cursoAca) {
        return bicicletasUsuarioDAO.getListadoReservas(perId, cursoAca);
    }

    public void updateReservaBicicleta(Long reservaId, Long tipoReservaId, Date fechaFin, Date fechaPrevDevol, String observacion) throws CommonException {
        try {
            bicicletasUsuarioDAO.updateReservaBicicleta(reservaId, tipoReservaId, fechaFin, fechaPrevDevol, observacion);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException(e.getMessage());
        }
    }

    public void deleteReservaBicicleta(Long reservaId) throws CommonException {
        try {
            bicicletasUsuarioDAO.deleteReservaBicicleta(reservaId);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    public void darBicicleta(Long reservaId) {
        Date fechaPrevista;
        try {
            BicicletaTiposReservasDTO result = bicicletasGestionDAO.getBicicletaTipoReserva(reservaId);
            if (result.getDiasDuracion() == null) {
                if (result.getFechaFinPrestamo() == null) fechaPrevista = result.getFechaFin();
                else fechaPrevista = result.getFechaFinPrestamo();
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, result.getDiasDuracion().intValue());
                fechaPrevista = calendar.getTime();
            }
        } catch (Exception e) {
            fechaPrevista = new Date();
        }
        bicicletasGestionDAO.darBicicleta(reservaId, fechaPrevista);
    }

    public void cambiarBicicleta(Long bicicletaAntigua, Long reservaId) throws CommonException {
        try {
            bicicletasGestionDAO.cambiarBicicleta(bicicletaAntigua, reservaId);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    public void devolverBicicleta(Long reservaId, Long bicicleta, Long estado, Long conectedUser) throws CommonException {
        try {
            Long persona;
            long diasRetraso;
            BicicletasReservasDTO result = bicicletasGestionDAO.getBicicletasReservas(reservaId);

            persona = result.getPerId();
            diasRetraso = ((((new Date().getTime() - result.getFechaPrevDevol().getTime()) / 1000) / 60) / 60) / 24;

            if(estado == 10298030 && personaDAO.isAdmin(conectedUser))
                personaSancionDAO.insertSancionValor(persona, 1L, "BIC", "Sanció per bicicleta robada");

            if (diasRetraso > 0 && personaDAO.isAdmin(conectedUser))
                personaSancionDAO.insertSancionValor(persona, diasRetraso, "BIC", "Sanció per retrás en retornar bicicleta");

            bicicletasGestionDAO.devolverBicicleta(reservaId, bicicleta, estado);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    public List<HistoricoBicicletas> getListadoBicicletasHistorico(Long perId, Paginacion paginacion) {
        return bicicletasUsuarioDAO.getListadoBicicletasHistorico(perId, paginacion);
    }

    public List<TipoIdentificacion> getListadoTipoReservasByYear(Long cursoAca) {
        return bicicletasUsuarioDAO.getListadoTipoReservasByYear(cursoAca);
    }
}
