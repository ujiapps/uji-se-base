package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.IncidenciaEliteDTO;
import es.uji.apps.se.dto.MailIncidenciaDTO;
import es.uji.apps.se.services.IncidenciaEliteService;
import es.uji.apps.se.ui.ConfiguracionIncidenciaEliteUI;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Path("incidenciaelite")
public class IncidenciaEliteResource extends CoreBaseService {

    @InjectParam
    IncidenciaEliteService incidenciaEliteService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public List<UIEntity> getIncidenciasElite() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return modelToUI(incidenciaEliteService.getIncidenciasElite(connectedUserId));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")

    public UIEntity getIncidenciaEliteById(@PathParam("id") Long incidenciaEliteId) {

        return modelToUI(incidenciaEliteService.getIncidenciaEliteById(incidenciaEliteId));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/mail")
    public UIEntity getMailIncidencia(@PathParam("id") Long incidenciaEliteId) {

        MailIncidenciaDTO mailIncidenciaDTO = incidenciaEliteService.getMailIncidencia(incidenciaEliteId);
        return UIEntity.toUI(mailIncidenciaDTO);
    }

    @GET
    @Path("configuracion")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getConfiguracionIncidenciaEliteUsuario() {

        ConfiguracionIncidenciaEliteUI configuracionIncidenciaEliteUI = incidenciaEliteService.setConfiguracionIncidenciaElite();
        return UIEntity.toUI(configuracionIncidenciaEliteUI);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public UIEntity saveDatosIncidencias(@PathParam("id") Long incidenciaEliteId, UIEntity entity) throws ParseException {

        IncidenciaEliteDTO incidenciaEliteDTO = incidenciaEliteService.getIncidenciaEliteById(incidenciaEliteId);
        incidenciaEliteDTO.setComentarios(entity.get("comentarios"));

        if (ParamUtils.isNotNull(entity.get("fechaResolucion"))) {
            SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");
            Date fechaResolucion = formateadorFecha.parse(entity.get("fechaResolucion"));

            incidenciaEliteDTO.setFechaResolucion(fechaResolucion);
        } else incidenciaEliteDTO.setFechaResolucion(null);

        return UIEntity.toUI(incidenciaEliteService.saveDatosIncidencia(incidenciaEliteDTO));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/mail/enviar")
    public UIEntity sendMailIncidencia(@PathParam("id") Long incidenciaEliteId, UIEntity entity) throws MessageNotSentException {

        MailIncidenciaDTO mailIncidenciaDTO = UIToModel(entity);
        return UIEntity.toUI(incidenciaEliteService.sendMailIncidencia(incidenciaEliteId, mailIncidenciaDTO));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/mail")
    public UIEntity saveMailIncidencia(@PathParam("id") Long incidenciaEliteId, UIEntity entity) {

        MailIncidenciaDTO mailIncidenciaDTO = UIToModel(entity);

        return UIEntity.toUI(incidenciaEliteService.saveMailIncidencia(incidenciaEliteId, mailIncidenciaDTO));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("configuracion")
    public UIEntity saveConfiguracionIncidenciaElite(UIEntity entity) {

        return UIEntity.toUI(incidenciaEliteService.saveConfiguracionIncidenciaElite(entity));
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public void deleteIncidenciaElite(@PathParam("id") Long incidenciaEliteId) {
        incidenciaEliteService.deleteIncidenciaElite(incidenciaEliteId);
    }


    private MailIncidenciaDTO UIToModel(UIEntity entity) {

        MailIncidenciaDTO mailIncidenciaDTO = new MailIncidenciaDTO();

        mailIncidenciaDTO.setAsunto(entity.get("asunto"));
        mailIncidenciaDTO.setCorreo(entity.get("correo"));
        mailIncidenciaDTO.setCuerpo(entity.get("cuerpo"));
        mailIncidenciaDTO.setDesde(entity.get("desde"));
        mailIncidenciaDTO.setResponder(entity.get("responder"));

        if (ParamUtils.isNotNull(entity.get("id")))
            mailIncidenciaDTO.setId(ParamUtils.parseLong(entity.get("id")));

        mailIncidenciaDTO.setFechaEnvio(entity.getDate("fechaEnvio"));
        return mailIncidenciaDTO;
    }

    private List<UIEntity> modelToUI(List<IncidenciaEliteDTO> incidenciasEliteDTO) {
        return incidenciasEliteDTO.stream().map((IncidenciaEliteDTO t) -> modelToUI(t)).collect(Collectors.toList());
    }

    private UIEntity modelToUI(IncidenciaEliteDTO incidenciaEliteDTO) {
        UIEntity entity = UIEntity.toUI(incidenciaEliteDTO);

        MailIncidenciaDTO mail = incidenciaEliteService.getMailIncidencia(incidenciaEliteDTO.getId());

        entity.put("tipoIncidencia", incidenciaEliteDTO.getTipoIncidencia().getNombre());
        if (ParamUtils.isNotNull(incidenciaEliteDTO.getFechaResolucion())) {
            SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");
            entity.put("fechaResolucion", formateadorFecha.format(incidenciaEliteDTO.getFechaResolucion()));
        }
        entity.put("persona", incidenciaEliteDTO.getPersona().getApellidosNombre());
        entity.put("mail", mail.getCuerpo());
        entity.put("fechaEnvio", mail.getFechaEnvio());
        if (ParamUtils.isNotNull(incidenciaEliteDTO.getProfesor()))
            entity.put("profesor", incidenciaEliteDTO.getProfesor().getApellidosNombre());
        return entity;
    }
}
