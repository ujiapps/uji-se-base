package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.MaterialService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("material")
public class MaterialResource extends CoreBaseService {


    @InjectParam
    MaterialService materialService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMateriales(@QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                        @QueryParam("sort") String ordenacion) throws JSONException{
        return UIEntity.toUI(materialService.getMateriales());
    }
}
