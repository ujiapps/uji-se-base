package es.uji.apps.se.services;

import es.uji.apps.se.exceptions.ValidacionException;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateExtensions
{
    public static Date getCurrentDate()
    {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date truncDate(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date addYears(Date date, int years)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, years);
        return cal.getTime();
    }

    public static Date addMonths(Date date, int months)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months);
        return cal.getTime();
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static Date addHours(Date date, int hours)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours);
        return cal.getTime();
    }

    public static Date addMinutes(Date date, int minutes)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }

    public static String getDayNameOfWeek(Date date)
    {
        String[] monthNames = {"", "Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendes", "Dissabte"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return monthNames[cal.get(Calendar.DAY_OF_WEEK)];
    }

    public static String getDateAsString(Date date)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }

    public static Date getFechaConHora(Date fecha, String hora) throws ValidacionException
    {
        try
        {
            String[] partesHora = hora.split(":");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fecha);
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(partesHora[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(partesHora[1]));
            calendar.set(Calendar.SECOND, 0);
            return calendar.getTime();
        }
        catch (Exception e)
        {
            throw new ValidacionException(MessageFormat.format("Error al processar la data {0} {1}", fecha, hora));
        }
    }
}
