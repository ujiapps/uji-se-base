package es.uji.apps.se.services;

import es.uji.apps.se.dao.PerfilDAO;
import es.uji.apps.se.model.Perfil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerfilService {

    private PerfilDAO perfilDAO;

    @Autowired
    public PerfilService(PerfilDAO perfilDAO) {
        this.perfilDAO = perfilDAO;
    }

    public List<Perfil> getPerfiles() {
        return perfilDAO.getPerfiles();
    }
}
