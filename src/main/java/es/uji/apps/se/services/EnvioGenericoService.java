package es.uji.apps.se.services;

import es.uji.apps.se.dao.EnvioGenericoDAO;
import es.uji.apps.se.dao.ProfesorEliteAvisoDAO;
import es.uji.apps.se.dto.*;

import es.uji.apps.se.model.CursoAcademico;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;

@Service
public class EnvioGenericoService {

    private EnvioGenericoDAO envioGenericoDAO;
    private ParametroService parametroService;
    private EliteService eliteService;
    private EliteAvisosService eliteAvisosService;
    private ProfesorEliteService profesorEliteService;
    private CursoAcademicoService cursoAcademicoService;
    private DecanoAvisosService decanoAvisosService;
    private ProfesorEliteAvisoDAO profesorEliteAvisoDAO;

    @Autowired
    public EnvioGenericoService(EnvioGenericoDAO envioGenericoDAO, ParametroService parametroService, EliteService eliteService, DecanoAvisosService decanoAvisosService,
                                EliteAvisosService eliteAvisosService, ProfesorEliteService profesorEliteService, CursoAcademicoService cursoAcademicoService,
                                ProfesorEliteAvisoDAO profesorEliteAvisoDAO) {
        this.envioGenericoDAO = envioGenericoDAO;
        this.parametroService = parametroService;
        this.eliteService = eliteService;
        this.eliteAvisosService = eliteAvisosService;
        this.profesorEliteService = profesorEliteService;
        this.cursoAcademicoService = cursoAcademicoService;
        this.decanoAvisosService = decanoAvisosService;
        this.profesorEliteAvisoDAO = profesorEliteAvisoDAO;
    }

    public EnvioGenericoDTO getEnvioGenerico(String tipoEnvio) {
        return envioGenericoDAO.getEnvioGenerico(tipoEnvio);
    }

    public EnvioGenericoDTO insertEnvioGenerico(EnvioGenericoDTO envioGenericoDTO) {
        return envioGenericoDAO.insert(envioGenericoDTO);
    }

    public EnvioGenericoDTO updateEnvioGenerico(EnvioGenericoDTO envioGenericoDTO) {
        return envioGenericoDAO.update(envioGenericoDTO);
    }

    public void deleteEnvioGenerico(Long id) {
        envioGenericoDAO.delete(EnvioGenericoDTO.class, id);
    }

    public void sendEnvioProfesoradoElitePrueba(Long connectedUserId, Long profesorId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        ProfesorEliteAvisoDTO profesorEliteAvisoDTO = new ProfesorEliteAvisoDTO();
        profesorEliteAvisoDTO.setCursoAcademico(cursoAcademico.getId());
        profesorEliteAvisoDTO.setPersonaId(connectedUserId);
        profesorEliteAvisoDTO.setFechaSolicitaEnvio(new Date());
        profesorEliteAvisoDTO.setEnviarPrueba(Boolean.TRUE);
        profesorEliteAvisoDTO.setProfesor(profesorId);

        profesorEliteAvisoDAO.insert(profesorEliteAvisoDTO);
    }

    public void sendEnvioProfesoradoElite(Long connectedUserId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        ProfesorEliteAvisoDTO profesorEliteAvisoDTO = new ProfesorEliteAvisoDTO();
        profesorEliteAvisoDTO.setCursoAcademico(cursoAcademico.getId());
        profesorEliteAvisoDTO.setPersonaId(connectedUserId);
        profesorEliteAvisoDTO.setFechaSolicitaEnvio(new Date());
        profesorEliteAvisoDTO.setEnviarPrueba(Boolean.FALSE);

        profesorEliteAvisoDAO.insert(profesorEliteAvisoDTO);
    }

    public void sendEnvioDecanosElite(Long connectedUserId) throws MessageNotSentException {

        EnvioGenericoDTO envioGenericoDTO = getEnvioGenerico("DECANOS-ELITE");
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        MailMessage mensaje = new MailMessage("SE");

        mensaje.setTitle(envioGenericoDTO.getAsunto().replace("$[curso]", cursoAcademico.getId().toString()));
        mensaje.setContentType("text/html; charset=UTF-8");

        String cuerpo = envioGenericoDTO.getCuerpo();
        cuerpo = cuerpo.replace("$[curso]", cursoAcademico.getId().toString());

        mensaje.setContent(cuerpo);
        mensaje.setReplyTo("noreply@uji.es");
        mensaje.setSender("Servei d'esports <noreply@uji.es>");

        List<DecanoEliteDTO> listaDecanosElite = eliteService.getListadoDecanosConAlumnosEliteFaltaEnvio(connectedUserId);

        for (DecanoEliteDTO decanoEliteDTO : listaDecanosElite) {

            if (ParamUtils.isNotNull(decanoEliteDTO.getCorreo())) {

                mensaje.addToRecipient(decanoEliteDTO.getCorreo());

                MessagingClient cliente = new MessagingClient();
                cliente.send(mensaje);

                DecanoAvisoDTO decanoAvisoDTO = new DecanoAvisoDTO();
                decanoAvisoDTO.setCursoAcademico(cursoAcademicoService.getCursoAcademicoById(cursoAcademico.getId()));
                PersonaUjiDTO personaUjiDTO = new PersonaUjiDTO();
                personaUjiDTO.setId(decanoEliteDTO.getId());
                decanoAvisoDTO.setPersonaUji(personaUjiDTO);
                decanoAvisoDTO.setFechaEnvio(new Date());

                decanoAvisosService.addDecanoAvisos(decanoAvisoDTO);
            }
        }
    }
}
