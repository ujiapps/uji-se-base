package es.uji.apps.se.services.rest.app;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.services.*;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

public class AppPagoTarjetasResource extends CoreBaseService {

    @InjectParam
    PersonaService personaService;

    @InjectParam
    TarjetaDeportivaTipoService tarjetaDeportivaTipoService;

    @InjectParam
    BonificacionService bonificacionService;

    @InjectParam
    TratamientoRGPDService tratamientoRGPDService;

    @InjectParam
    PersonaIbanService personaIbanService;


    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template listadoRecibos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("id") Long tipoTarjetaId)
            throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        PersonaDTO personaDTO = personaService.getPersonaById(connectedUserId);
        TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoService.getTarjetaDeportivaTipoById(tipoTarjetaId);

        if (!ParamUtils.isNotNull(tarjetaDeportivaTipoDTO)) {
            Template templatError = AppInfo.buildPaginaAplicacionClases("se/app/error", idioma);

            templatError.put("texto", "Targeta deportiva no valida");
            templatError.put("href", "/se/rest/app/tarjetas");
            return templatError;
        }

        Template template = AppInfo.buildPaginaAplicacionClases("se/app/pagos", idioma);

        template.put("titulo", "Pagaments");
        template.put("tarjetaDeportiva", new TarjetaDeportiva(tarjetaDeportivaTipoDTO, personaDTO.getPersonaUji(), null));

        //Mostrar listado de bonificaciones si tiene activas y son compatibles
        List<BonificacionActivaDTO> listaBonificaciones = bonificacionService.getBonificacionesActivasGenericasByPersonaId(connectedUserId);
        List<BonificacionActivaDTO> listaBonificacionesTarjeta = bonificacionService.getBonificacionesActivasByTipoTarjetaAndPersonaId(connectedUserId, tarjetaDeportivaTipoDTO);
        listaBonificaciones.addAll(listaBonificacionesTarjeta);
        template.put("bonificaciones", listaBonificaciones.stream().map(l -> {
            DecimalFormat df = new DecimalFormat("0.00");

            if (ParamUtils.isNotNull(l.getImporteFloat()))
                l.setImporte(df.format(l.getImporteFloat()));
            return l;
        }).collect(Collectors.toList()));

        template.put("tratamientoRPGD", tratamientoRGPDService.visualizaTratamientoPLSQL("U034", "SNN", idioma));

        //Mostrar pago domiciliado
        template.put("admitePagoDomiciliado", personaDTO.getPersonaUji().isComunidadUniversitaria());

        //Añadir cuentas bancarias si admite pago domiciliado
        if (personaDTO.getPersonaUji().isComunidadUniversitaria()) {
            List<PersonaIbanDTO> personaCuentasBancarias = personaIbanService.getListaPersonaIbanByPersonaId(connectedUserId);
            template.put("personaCuentasBancarias", personaCuentasBancarias);
        }

        return template;
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Response redirectPago() throws URISyntaxException {
        URI url = new URI(AppInfo.getHost() + "/se/rest/app/tarjetas");
        return Response.seeOther(url).build();
    }
}
