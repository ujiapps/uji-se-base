package es.uji.apps.se.services;

import es.uji.apps.se.dao.DatosPersonaDAO;
import es.uji.apps.se.dao.EncriptaClave;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.DatosPersona;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DatosPersonaService {

    private static final Logger log = LoggerFactory.getLogger(DatosPersonaService.class);
    private final PersonaService personaService;
    private final DatosPersonaDAO datosPersonaDAO;
    private final EncriptaClave encriptaClave;

    public DatosPersonaService(PersonaService personaService, DatosPersonaDAO datosPersonaDAO, EncriptaClave encriptaClave) {
        this.personaService = personaService;
        this.datosPersonaDAO = datosPersonaDAO;
        this.encriptaClave = encriptaClave;
    }

    public List<DatosPersona> getDatosPersona(Long userId, Long cursoAcademico) {

        Long rgpd = datosPersonaDAO.getRgpd(userId, cursoAcademico);
        Long elite = datosPersonaDAO.getElite(userId, cursoAcademico);

        return datosPersonaDAO.getDatosPersona(userId, rgpd > 0, elite > 0);
    }

    public void updateDatosPersona(UIEntity uiPersona, Long personaId, Long curso) {
        try {
            DatosPersona datosPersona = uiPersona.toModel(DatosPersona.class);
            if (uiPersona.get("fechaNacimiento") != null) {
                datosPersona.setFechaNacimiento(new SimpleDateFormat("dd/MM/yyyy").parse(uiPersona.get("fechaNacimiento")));
            }
            datosPersonaDAO.updateDatosPersona(datosPersona, personaId, curso);
        } catch (Exception e) {
            log.error("No se puede actualizar los datos de persona", e);
        }
    }

    public Long existeIdentificacion(String identificacion) {
        return datosPersonaDAO.existeIdentificacion(identificacion);
    }

    @Transactional
    public Long addDatosPersona(DatosPersona datosPersona, Long cursoAcademico) {

        Long VINCULOUJI = 15L;
        Long SUBVINCULOUJI = 1L;

        PerPersonasDTO perPersonasDTO = new PerPersonasDTO(datosPersona.getNombre(), datosPersona.getApellido1(),
                datosPersona.getApellido2(), datosPersona.getIdentificacion(), datosPersona.getTipoIdentificacion(), "F", "E", 1L);

        PerPersonasDTO persona = datosPersonaDAO.insert(perPersonasDTO);
        PersonaDTO personaUjiDTO = personaService.getPersonaById(persona.getId());

        PersonasSubVinculosDTO personasSubVinculosDTO = new PersonasSubVinculosDTO();
        personasSubVinculosDTO.setId(new PersonasSubVinculoDTOPK(personaUjiDTO.getPersonaUji(), VINCULOUJI, SUBVINCULOUJI));
        personasSubVinculosDTO.setFecha(new Date());
        datosPersonaDAO.insert(personasSubVinculosDTO);

        datosPersonaDAO.updateDatosPersona(datosPersona, persona.getId(), cursoAcademico);
        return persona.getId();
    }

    public void cambiaPassword(Long id, String clave) {
        encriptaClave.encriptarClave(clave, id);
    }
}
