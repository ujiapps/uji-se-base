package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.PeriodoCountActividadYGrupoDTO;
import es.uji.apps.se.dto.PeriodoDTO;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.services.PeriodoCountService;
import es.uji.apps.se.services.PeriodoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Path("periodo")
public class PeriodoResource extends CoreBaseService {


    @PathParam("curso")
    private Long curso;

    @InjectParam
    PeriodoService periodoService;
    @InjectParam
    PeriodoCountService periodoCountService;

    @Path("{periodoId}/descuento")
    public PeriodoDescuentoResource getPlatformItem(
            @InjectParam PeriodoDescuentoResource periodoDescuentoResource) {
        return periodoDescuentoResource;
    }

    @Path("{periodoId}/actividad")
    public ActividadResource getPlatformItem(
            @InjectParam ActividadResource actividadResource) {
        return actividadResource;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPeriodosByCurso() {
        return modelToUI(periodoService.getPeriodosByCurso(this.curso));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateOrInsertPeriodo(UIEntity entity) throws ParseException {

        PeriodoDTO periodoDTO = UIToModel(entity);
        periodoService.updateOrInsertPeriodo(this.curso, periodoDTO);
        return UIEntity.toUI(periodoDTO);
    }

    @POST
    @Path("duplicar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity duplicarPeriodo(@PathParam("curso") Long cursoActualId, UIEntity entity) {
        return UIEntity.toUI(periodoService.duplicarPeriodo(cursoActualId, ParamUtils.parseLong(entity.get("periodo")), entity.get("nombre"),
                entity.getDate("fechaInicio"), entity.getDate("fechaFin")));
    }

    @DELETE
    @Path("{periodoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deletePeriodo(@PathParam("periodoId") Long periodoId) {
        periodoService.deletePeriodo(periodoId);
    }

    private UIEntity modelToUI(PeriodoDTO periodoDTO) {
        UIEntity entity = UIEntity.toUI(periodoDTO);

        PeriodoCountActividadYGrupoDTO periodoCount = periodoCountService.getTotalesByPeriodo(periodoDTO.getId());
        if (ParamUtils.isNotNull(periodoCount)) {
            entity.put("numActividades", periodoCount.getNumeroActividades());
            entity.put("numOfertas", periodoCount.getNumeroOfertas());
        }
        else{
            entity.put("numActividades", 0L);
            entity.put("numOfertas", 0L);
        }
        return entity;
    }

    private List<UIEntity> modelToUI(List<PeriodoDTO> periodosDTO) {

        List<UIEntity> lista = new ArrayList<UIEntity>();
        for (PeriodoDTO periodoDTO : periodosDTO) {
            lista.add(modelToUI(periodoDTO));
        }

        return lista;
    }

    private PeriodoDTO UIToModel(UIEntity entity) throws ParseException {

        PeriodoDTO periodoDTO = new PeriodoDTO();

        periodoDTO.setNombre(entity.get("nombre"));

        if (ParamUtils.isNotNull(entity.get("id")))
            periodoDTO.setId(ParamUtils.parseLong(entity.get("id")));

        if (ParamUtils.isNotNull(entity.get("numeroDiasAviso")))
            periodoDTO.setNumeroDiasAviso(ParamUtils.parseLong(entity.get("numeroDiasAviso")));

        if (ParamUtils.isNotNull(entity.get("periodoReferenciaId"))) {
            PeriodoDTO periodoReferenciaDTO = new PeriodoDTO();
            periodoReferenciaDTO.setId(ParamUtils.parseLong(entity.get("periodoReferenciaId")));
            periodoDTO.setPeriodoReferencia(periodoReferenciaDTO);
        }

        if (ParamUtils.isNotNull(entity.get("clasificacionId"))) {
            TipoDTO clasificacion = new TipoDTO();
            clasificacion.setId(ParamUtils.parseLong(entity.get("clasificacionId")));
            periodoDTO.setClasificacion(clasificacion);
        }

        periodoDTO.setFechaInicio(entity.getDate("fechaInicio"));
        periodoDTO.setFechaFin(entity.getDate("fechaFin"));

        periodoDTO.setFechaInicioInscripcion(entity.getDate("fechaInicioInscripcion"));
        periodoDTO.setFechaFinInscripcion(entity.getDate("fechaFinInscripcion"));

        periodoDTO.setFechaInicioPreinscripcion(entity.getDate("fechaInicioPreinscripcion"));
        periodoDTO.setFechaFinPreinscripcion(entity.getDate("fechaFinPreinscripcion"));

        periodoDTO.setFechaInicioRenovacion(entity.getDate("fechaInicioRenovacion"));
        periodoDTO.setFechaFinRenovacion(entity.getDate("fechaFinRenovacion"));

        periodoDTO.setFechaInicioValidacion(entity.getDate("fechaInicioValidacion"));
        periodoDTO.setFechaFinValidacion(entity.getDate("fechaFinValidacion"));

        periodoDTO.setFechaInicioWeb(entity.getDate("fechaInicioWeb"));
        periodoDTO.setFechaFinWeb(entity.getDate("fechaFinWeb"));

        periodoDTO.setFechaInicioCobroRecibos(entity.getDate("fechaInicioCobroRecibos"));

        return periodoDTO;
    }
}
