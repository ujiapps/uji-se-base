package es.uji.apps.se.services;

import es.uji.apps.se.dao.PeriodoCountDAO;
import es.uji.apps.se.dto.PeriodoCountActividadYGrupoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeriodoCountService {

    private PeriodoCountDAO periodoCountDAO;

    @Autowired
    public PeriodoCountService(PeriodoCountDAO periodoCountDAO) {
        this.periodoCountDAO = periodoCountDAO;
    }

    public PeriodoCountActividadYGrupoDTO getTotalesByPeriodo(Long periodoId) {
        return periodoCountDAO.getTotalesByPeriodo(periodoId);
    }
}
