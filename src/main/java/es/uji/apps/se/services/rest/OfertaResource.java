package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.OfertaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.OfertaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("oferta")
public class OfertaResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(OfertaResource.class);

    @Path("{ofertaId}/ofertatarifa")
    public OfertaTarifaResource getPlatformItem(
            @InjectParam OfertaTarifaResource ofertaTarifaResource) {
        return ofertaTarifaResource;
    }

    @InjectParam
    OfertaService ofertaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getOfertasByActividad(@PathParam("actividadId") Long actividadId, @PathParam("cursoId") Long cursoId,
                                                 @QueryParam("start") @DefaultValue("0") Long start,
                                                 @QueryParam("limit") @DefaultValue("25") Long limit,
                                                 @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            if (ParamUtils.isNotNull(actividadId) && ParamUtils.isNotNull(cursoId)) {

                List<OfertaDTO> ofertasDTO = ofertaService.getOfertasByActividad(actividadId, cursoId, paginacion);
                responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
                responseMessage.setData((UIEntity.toUI(ofertasDTO)));
                responseMessage.setSuccess(true);
            } else {
                if (ParamUtils.isNotNull(cursoId)) {
                    List<OfertaDTO> ofertasDTO = ofertaService.getOfertasByCursoId(cursoId);
                    responseMessage.setTotalCount(ofertasDTO.size());
                    responseMessage.setData(ofertasDTO.stream().map(ofertaDTO -> {
                        UIEntity entity = new UIEntity(ofertaDTO);
                        entity.put("id", ofertaDTO.getId());
                        entity.put("nombre", ofertaDTO.getNombre());
                        entity.put("actividadId", ofertaDTO.getActividad().getId());
                        return entity;
                    }).collect(Collectors.toList()));
                    responseMessage.setSuccess(true);
                } else {
                    List<OfertaDTO> ofertasDTO = ofertaService.getOfertas();
                    responseMessage.setTotalCount(ofertasDTO.size());
                    responseMessage.setData(ofertasDTO.stream().map(ofertaDTO -> {
                        UIEntity entity = new UIEntity(ofertaDTO);
                        entity.put("id", ofertaDTO.getId());
                        entity.put("nombre", ofertaDTO.getNombre());
                        entity.put("actividadId", ofertaDTO.getActividad().getId());
                        entity.put("cursoId", ofertaDTO.getPeriodo().getCursoAcademico().getId());
                        return entity;
                    }).collect(Collectors.toList()));
                    responseMessage.setSuccess(true);
                }
            }

        } catch (Exception e) {
            log.error("getOfertasByActividad", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

}
