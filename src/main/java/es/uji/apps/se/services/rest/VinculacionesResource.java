package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.Vinculaciones;
import es.uji.apps.se.services.CursoAcademicoService;
import es.uji.apps.se.services.VinculacionesService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("vinculaciones")
public class VinculacionesResource extends CoreBaseService {
    @InjectParam
    VinculacionesService vinculacionesService;
    @InjectParam
    CursoAcademicoService cursoAcademicoService;

    @GET
    public UIEntity getVinculacionesPersona(@QueryParam("personaId") Long personaId) {
        try {
            Long cursoAcademico = cursoAcademicoService.getCursoAcademico(personaId).getId();
            Vinculaciones vinculaciones = vinculacionesService.getVinculacionesPersona(personaId, cursoAcademico);
            UIEntity entity = new UIEntity();
            entity.put("vinculo", vinculaciones.getVinculacionesActivas());
            entity.put("vinculoUji", vinculaciones.getVinculacionesUji());
            entity.put("titulacionesActivas", vinculaciones.getTitulacionesActivas());
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
