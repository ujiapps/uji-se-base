package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.BonificacionUsoDAO;
import es.uji.apps.se.dto.BonificacionUsoDTO;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BonificacionUsoService {

    @InjectParam
    private BonificacionUsoDAO bonificacionUsoDAO;
    public void anyadeUsoBonificacion(Long bonificacionId, String uso, Long dto, Long tarjetaDeportivaId) {
        BonificacionUsoDTO bonificacionUsoDTO = new BonificacionUsoDTO();
        bonificacionUsoDTO.setOrigen(uso);
        bonificacionUsoDTO.setReferencia(tarjetaDeportivaId);
        bonificacionUsoDTO.setBonificacionId(bonificacionId);
        bonificacionUsoDTO.setFecha(new Date());
        bonificacionUsoDTO.setValor(dto);

        bonificacionUsoDAO.insert(bonificacionUsoDTO);
    }
}
