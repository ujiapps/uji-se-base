package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.AccesoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.AccesoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class PersonaAccesoTornoResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(PersonaAccesoTornoResource.class);

    @InjectParam
    AccesoService accesoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getAccesos(@QueryParam("start") @DefaultValue("0") Long start,
                                      @QueryParam("limit") @DefaultValue("25") Long limit,
                                      @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                      @PathParam("personaId") Long personaId){

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<AccesoDTO> accesosDTO = accesoService.getAccesosByPersona(paginacion, personaId);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(accesosDTO)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getAccesosPersona", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
