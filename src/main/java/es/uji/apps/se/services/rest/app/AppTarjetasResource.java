package es.uji.apps.se.services.rest.app;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.PersonaDTO;
import es.uji.apps.se.dto.TarjetaDeportivaDTO;
import es.uji.apps.se.exceptions.NoAutorizadoException;
import es.uji.apps.se.exceptions.ReservaException;
import es.uji.apps.se.model.Sancion;
import es.uji.apps.se.model.ui.UIPersona;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.services.*;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class AppTarjetasResource extends CoreBaseService {
    @InjectParam
    PersonaService personaService;

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;

    @InjectParam
    TarjetaDeportivaTipoService tarjetaDeportivaTipoService;

    @InjectParam
    PersonaSancionService personaSancionService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template tarjetaDeportiva(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws NoAutorizadoException, ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        PersonaDTO personaDTO = personaService.getPersonaById(connectedUserId);
        Sancion sancion = personaSancionService.getSancionBloqueaTodoOrClasesDirigidas(connectedUserId);
        Template template = null;

        UIPersona uIPersona = new UIPersona(personaDTO);
        if (uIPersona.completarDatos()) {
            template = AppInfo.buildPaginaAplicacionClases("se/app/formulario-datos-usuario", idioma);
            template.put("titulo", "Afegir dades");
            template.put("persona", uIPersona);
        } else {

            TarjetaDeportiva tarjetaDeportiva = tarjetaDeportivaService.getTarjetaDeportivaActivaByPersonaId(connectedUserId);
            TarjetaDeportivaDTO tarjetaDeportivaFuturaDTO = tarjetaDeportivaService.getTarjetaDeportivaFuturaByPersonaId(connectedUserId);
            template = AppInfo.buildPaginaAplicacionClases("se/app/tarjeta", idioma);
            anyadirSancionAlTemplate(sancion, template);
            template.put("titulo", "La meua targeta esportiva");
            template.put("tarjetaDeportiva", tarjetaDeportiva);
            template.put("tarjetaDeportivaFutura", (tarjetaDeportivaFuturaDTO != null) ? new TarjetaDeportiva(tarjetaDeportivaFuturaDTO) : null);

            if (!ParamUtils.isNotNull(tarjetaDeportivaFuturaDTO)) {
                List<TarjetaDeportiva> tarjetaDeportivaTipos = tarjetaDeportivaTipoService.getTarjetasDeportivasTiposActivas().stream().filter(tarjetaDeportivaTipo -> {

                    Boolean tarjetaCaducada = ParamUtils.isNotNull(tarjetaDeportivaTipo.getFechaCaducidad()) &&
                            tarjetaDeportivaTipo.getFechaCaducidad().before(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()));

                    if (!ParamUtils.isNotNull(tarjetaDeportiva)) {
                        return tarjetaDeportivaTipo.isMostrarUsuario() && tarjetaDeportivaTipo.isActiva() && !tarjetaCaducada;
                    }

                    return tarjetaDeportivaTipo.isMostrarUsuario() && tarjetaDeportivaTipo.isActiva() && !tarjetaCaducada &&
                            (!tarjetaDeportivaTipo.getId().equals(tarjetaDeportiva.getTarjetaDeportivaTipoId())
                                    || (ParamUtils.isNotNull(tarjetaDeportiva) && tarjetaDeportiva.getPeriodoRenovacion()));
                }).map(t -> new TarjetaDeportiva(t, personaDTO.getPersonaUji(), tarjetaDeportiva)).collect(
                        Collectors.toList());

                template.put("tarjetaDeportivaTipos", tarjetaDeportivaTipos);
            }
        }
        return template;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage addTarjetaDeportiva(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @FormParam("tipoTarjeta") Long tipoTarjeta,
                                               @FormParam("bonificacion") Long bonificacion, @FormParam("metodoPago") String metodoPago)
            throws ReservaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return tarjetaDeportivaService.addTarjetaDeportiva(connectedUserId, tipoTarjeta, bonificacion, metodoPago);
    }

    @POST
    @Path("datos")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updatePersonaDatos(@FormParam("fecha_nacimiento") String fechaNacimiento, @FormParam("mail") String mail, @FormParam("telefono") String telefono, @FormParam("sexo") Long sexo) throws URISyntaxException, ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaService.updatePersona(connectedUserId, fechaNacimiento, mail, telefono, sexo);
        URI url = new URI(AppInfo.getHost() + "/se/rest/app/tarjetas");
        return Response.seeOther(url).build();
    }

    private void anyadirSancionAlTemplate(Sancion sancion, Template template) {
        if (ParamUtils.isNotNull(sancion)) {
            if (sancion.getTipo().equals("ALL")) {
                String motivo = ParamUtils.isNotNull(sancion.getMotivo())? sancion.getMotivo() : "No s'ha especificat el motiu";
                if (ParamUtils.isNotNull(sancion.getFechaFin())) {
                    template.put("mensajeInfo", MessageFormat.format("No pots tramitar una targeta esportiva nova fins al dia {0}. Motiu: {1}", DateExtensions.getDateAsString(sancion.getFechaFin()), motivo));
                } else {
                    template.put("mensajeInfo", MessageFormat.format("No pots tramitar una targeta esportiva nova fins que resolgues la incidència: {0}", motivo));
                }
            } else
                template.put("mensajeInfo", null);
        } else {
            template.put("mensajeInfo", null);
        }

        if (ParamUtils.isNotNull(sancion) && sancion.getTipo().equals("ALL")) {
            template.put("classSancion", "disabled");
        } else {
            template.put("classSancion", "");
        }
    }

}
