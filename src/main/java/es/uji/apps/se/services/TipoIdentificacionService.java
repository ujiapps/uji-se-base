package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.TipoIdentificacionDAO;
import es.uji.apps.se.model.TipoIdentificacion;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoIdentificacionService {
    @InjectParam
    private TipoIdentificacionDAO tipoIdentificacionDAO;

    public List<TipoIdentificacion> getTipoIdentificacion() {
        return tipoIdentificacionDAO.getTipoIdentificacion();
    }
}
