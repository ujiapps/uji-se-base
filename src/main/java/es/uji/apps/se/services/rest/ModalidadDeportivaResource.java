package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.ModalidadDeportivaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("modalidadDeportiva")
public class ModalidadDeportivaResource extends CoreBaseService {
    @InjectParam
    ModalidadDeportivaService modalidadDeportivaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getModalidadesDeportivas() {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            responseMessage.setData(UIEntity.toUI(modalidadDeportivaService.getModalidadesDeportivas()));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
