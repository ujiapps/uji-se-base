package es.uji.apps.se.services;

import es.uji.apps.se.dao.OfertaTarifaDescuentoDAO;
import es.uji.apps.se.dto.OfertaTarifaDTO;
import es.uji.apps.se.dto.OfertaTarifaDescuentoDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfertaTarifaDescuentoService {

    private OfertaTarifaDescuentoDAO ofertaTarifaDescuentoDAO;

    @Autowired
    public OfertaTarifaDescuentoService(OfertaTarifaDescuentoDAO ofertaTarifaDescuentoDAO) {

        this.ofertaTarifaDescuentoDAO = ofertaTarifaDescuentoDAO;
    }


    public List<OfertaTarifaDescuentoDTO> getDescuentosTarifas(Long ofertaTarifaId, Paginacion paginacion) {
        return ofertaTarifaDescuentoDAO.getDescuentosTarifas(ofertaTarifaId, paginacion);
    }

    public OfertaTarifaDescuentoDTO insertOfertaTarifaDescuento(UIEntity entity, Long ofertaTarifaId) {
        OfertaTarifaDescuentoDTO ofertaTarifaDescuentoDTO = UIEntityToModel(entity, ofertaTarifaId);
        return ofertaTarifaDescuentoDAO.insert(ofertaTarifaDescuentoDTO);
    }

    public OfertaTarifaDescuentoDTO updateOfertaTarifaDescuento(UIEntity entity) {
        OfertaTarifaDescuentoDTO ofertaTarifaDescuentoDTO = UIEntityToModel(entity, ParamUtils.parseLong(entity.get("ofertaTarifaId")));
        return ofertaTarifaDescuentoDAO.update(ofertaTarifaDescuentoDTO);
    }

    public void deleteOfertaTarifaDescuento(Long ofertaTarifaDescuentoId) {
        ofertaTarifaDescuentoDAO.delete(OfertaTarifaDescuentoDTO.class, ofertaTarifaDescuentoId);
    }

    private OfertaTarifaDescuentoDTO UIEntityToModel(UIEntity entity, Long ofertaTarifaId) {
        OfertaTarifaDescuentoDTO ofertaTarifaDescuentoDTO = entity.toModel(OfertaTarifaDescuentoDTO.class);

        OfertaTarifaDTO ofertaTarifaDTO = new OfertaTarifaDTO();
        ofertaTarifaDTO.setId(ofertaTarifaId);
        ofertaTarifaDescuentoDTO.setOfertaTarifa(ofertaTarifaDTO);

        TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO = new TarjetaDeportivaTipoDTO();
        tarjetaDeportivaTipoDTO.setId(ParamUtils.parseLong(entity.get("tarjetaDeportivaId")));
        ofertaTarifaDescuentoDTO.setTarjetaDeportiva(tarjetaDeportivaTipoDTO);

        ofertaTarifaDescuentoDTO.setFechaInicio(entity.getDate("fechaInicio"));

        if (ParamUtils.isNotNull(entity.get("fechaFin"))) {
            ofertaTarifaDescuentoDTO.setFechaFin(entity.getDate("fechaFin"));
        }
        return ofertaTarifaDescuentoDTO;
    }
}
