package es.uji.apps.se.services;

import es.uji.apps.se.dao.MaterialReservaDAO;
import es.uji.apps.se.dto.MaterialDTO;
import es.uji.apps.se.dto.MaterialReservaDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.model.MaterialReserva;
import es.uji.apps.se.model.domains.TipoOrigenReservaMateriales;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialReservaService {

    private final MaterialReservaDAO materialReservaDAO;

    @Autowired
    public MaterialReservaService(MaterialReservaDAO materialReservaDAO) {

        this.materialReservaDAO = materialReservaDAO;
    }


    public List<MaterialReserva> getMaterialesReservaClase(String claseId) {
        return materialReservaDAO.getMaterialesReservaClase(claseId);
    }

    public MaterialReservaDTO addMaterialReservaClase(UIEntity materialReserva, Long claseId, Long connectedUserId) {
        return materialReservaDAO.insert(uiToModel(materialReserva, claseId, connectedUserId));
    }

    private MaterialReservaDTO uiToModel(UIEntity entity, Long claseId, Long connectedUserId){

        MaterialReservaDTO materialReservaDTO = entity.toModel(MaterialReservaDTO.class);
        materialReservaDTO.setOrigen(TipoOrigenReservaMateriales.CLASEGENERICO.getValue());

        MaterialDTO materialDTO = new MaterialDTO();
        materialDTO.setId(ParamUtils.parseLong(entity.get("material")));
        materialReservaDTO.setMaterial(materialDTO);

        materialReservaDTO.setReferencia(claseId.toString());

        PersonaUjiDTO personaUjiDTO = new PersonaUjiDTO();
        personaUjiDTO.setId(connectedUserId);
        materialReservaDTO.setPersonaUji(personaUjiDTO);

        return materialReservaDTO;
    }

    public MaterialReservaDTO updateMaterialReserva(UIEntity materialReserva, Long connectedUserId) {

        return materialReservaDAO.update(uiToModel(materialReserva, ParamUtils.parseLong(materialReserva.get("referencia")), connectedUserId));
    }

    public void deleteMaterialReserva(Long materialReservaId) {
        materialReservaDAO.delete(MaterialReservaDTO.class, materialReservaId);
    }
}
