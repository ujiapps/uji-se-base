package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

public class PersonaCarnetBonoResource extends CoreBaseService {
    @InjectParam
    PersonaTarjetaBonoService personaTarjetaBonoService;
    @InjectParam
    PersonaService personaService;
    @InjectParam
    CursoAcademicoService cursoAcademicoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTarjetasBonos(@PathParam("personaId") Long personaId,
                                            @QueryParam("start") @DefaultValue("0") Long start,
                                            @QueryParam("limit") @DefaultValue("25") Long limit,
                                            @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            responseMessage.setData(UIEntity.toUI(personaTarjetaBonoService.getTarjetasBonos(paginacion, personaId)));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("alta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTarjetasBonosTiposPuedeDarseDeAlta(@PathParam("personaId") Long personaId) {
        return UIEntity.toUI(personaTarjetaBonoService.getTarjetasBonosTiposPuedeDarseDeAlta(personaId));
    }

    @GET
    @Path("historico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getHistoricoTarjetasBonos(@PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Long cursoAcademico = cursoAcademicoService.getCursoAcademico(AccessManager.getConnectedUserId(request)).getId();
            List<TarjetaBono> historicoTarjetasBonos = personaTarjetaBonoService.getHistoricoTarjetasBonos(personaId, cursoAcademico);
            responseMessage.setData(UIEntity.toUI(historicoTarjetasBonos));
            responseMessage.setTotalCount(historicoTarjetasBonos.size());
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @PUT
    @Path("{tarjetaBonoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateTarjetaBono(@PathParam("personaId") Long personaId,
                                      @PathParam("tarjetaBonoId") Long tarjetaBonoId,
                                      UIEntity entity) throws CommonException {
        Long perfilBecario = 4L;
        Boolean esBecario = personaService.tienePerfil(AccessManager.getConnectedUserId(request), perfilBecario);
        personaTarjetaBonoService.updateTarjetaBono(entity, tarjetaBonoId, esBecario);
        return Response.ok().build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addBono(@PathParam("personaId") Long personaId,
                            @FormParam("tipoBonoId") Long tipoBonoId,
                            @FormParam("tipoBonificacionId") Long tipoBonificacionId) throws CommonException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        personaTarjetaBonoService.addBono(personaId, tipoBonoId, tipoBonificacionId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    @Path("alta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response altaTarjetaBonoTipoAPersona(@PathParam("personaId") Long personaId, UIEntity entity) throws CommonException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        personaTarjetaBonoService.addBono(personaId, ParamUtils.parseLong(entity.get("tipo")), null, connectedUserId);
        return Response.ok().build();
    }
}
