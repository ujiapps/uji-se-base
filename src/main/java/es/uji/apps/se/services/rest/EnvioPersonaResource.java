package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.EnvioPersona;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.EnvioPersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class EnvioPersonaResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(EnvioPersonaResource.class);

    @InjectParam
    EnvioPersonaService envioPersonaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getPersonas(@QueryParam("start") @DefaultValue("0") Long start,
                                       @QueryParam("limit") @DefaultValue("25") Long limit,
                                       @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                       @PathParam("envioId") Long envioId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<EnvioPersona> envioPersonas = envioPersonaService.getEnvioPersonasByEnvioId(envioId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(envioPersonas)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getPersonas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

}
