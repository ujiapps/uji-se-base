package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.CursoAcademicoDTO;
import es.uji.apps.se.services.CursoAcademicoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("cursoacademico")
public class CursoAcademicoResource extends CoreBaseService {

    @InjectParam
    CursoAcademicoService cursoAcademicoService;

    @Path("{curso}/periodo")
    public PeriodoResource getPlatformItem(
            @InjectParam PeriodoResource periodoResource) {
        return periodoResource;
    }

    @Path("{cursoId}/oferta")
    public OfertaResource getPlatformItem(
            @InjectParam OfertaResource ofertaResource) {
        return ofertaResource;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getCursosAcademicos(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sort") JSONArray sorts) throws JSONException {

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(cursoAcademicoService.getTotalCursosAcademicos());
        if (ParamUtils.isNotNull(sorts)) {
            JSONObject jsonObject = sorts.getJSONObject(0);
            responseMessage.setData(UIEntity.toUI(cursoAcademicoService.getCursosAcademicos(start, limit, jsonObject.getString("property"), jsonObject.getString("direction"))));
        }
        else{
            responseMessage.setData(UIEntity.toUI(cursoAcademicoService.getCursosAcademicos(start, limit, "id", "DESC")));
        }
        return responseMessage;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("actual")
    public UIEntity getCursoAcademico() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(cursoAcademicoService.getCursoAcademico(connectedUserId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addCursoAcademico(UIEntity entity) {
        CursoAcademicoDTO cursoAcademicoDTO = UIToModel(entity);
        cursoAcademicoDTO = cursoAcademicoService.addCursoAcademico(cursoAcademicoDTO);

        return UIEntity.toUI(cursoAcademicoDTO);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateCursoAcademico(@PathParam("id") Long cursoAcademicoId, UIEntity entity) {

        CursoAcademicoDTO cursoAcademicoDTO = UIToModel(entity);
        return UIEntity.toUI(cursoAcademicoService.updateCursoAcademico(cursoAcademicoDTO));

    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteCursoAcademico(@PathParam("id") Long cursoAcademicoId) {
        cursoAcademicoService.deleteCursoAcademico(cursoAcademicoId);
    }

    private CursoAcademicoDTO UIToModel(UIEntity entity) {
        CursoAcademicoDTO cursoAcademicoDTO = entity.toModel(CursoAcademicoDTO.class);
        cursoAcademicoDTO.setFechaInicio(entity.getDate("fechaInicio"));
        cursoAcademicoDTO.setFechaFin(entity.getDate("fechaFin"));
        return cursoAcademicoDTO;
    }
}
