package es.uji.apps.se.services;

import es.uji.apps.se.dao.TratamientosRGPD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TratamientoRGPDService {
    private TratamientosRGPD tratamientosRGPD;

    @Autowired
    public TratamientoRGPDService(TratamientosRGPD tratamientosRGPD) {
        this.tratamientosRGPD = tratamientosRGPD;
    }


    public String visualizaTratamientoPLSQL(String codigo, String tipo, String idioma) {
        try {
            return tratamientosRGPD.visualizaTratamientoPLSQL(codigo, tipo, idioma);
        } catch (Exception e) {
            return null;
        }
    }

    public String registraTratamientoPLSQL(String codigo, Long personaId) {
        return tratamientosRGPD.registraTratamientoPLSQL(codigo, personaId);
    }

    public String estaRegistradoTratamientoPLSQL(String codigo, Long personaId) {
        try {
            return tratamientosRGPD.estaRegistradoTratamientoPLSQL(codigo, personaId);
        } catch (Exception e) {
            return null;
        }
    }

}
