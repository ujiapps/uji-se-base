package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.HistoricoBicicletas;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.BicicletasUsuarioService;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Path("bicicletasusuario")
public class BicicletasUsuarioResource extends CoreBaseService {
    @InjectParam
    BicicletasUsuarioService bicicletasUsuarioService;

    @Path("espera")
    public BicicletasEsperaUsuarioResource BicicletasEsperaUsuarioResource(@InjectParam BicicletasEsperaUsuarioResource bicicletasEsperaUsuarioResource) {
        return bicicletasEsperaUsuarioResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getListadoReservas(@QueryParam("personaId") Long perId,
                                              @QueryParam("cursoAca") Long cursoAca) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            responseMessage.setData(UIEntity.toUI(bicicletasUsuarioService.getListadoReservas(perId, cursoAca)));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateReservaBicicleta(@PathParam("id") Long reservaId,
                                           UIEntity uiEntity) throws CommonException, ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date fechaFin = null;

        if (uiEntity.get("fechaFin") != null && uiEntity.get("horaFin") != null) {
            fechaFin = dateFormat.parse(uiEntity.get("fechaFin"));
            String[] hm = uiEntity.get("horaFin").split(":");
            fechaFin = DateExtensions.addHours(fechaFin, Integer.parseInt(hm[0]));
            fechaFin = DateExtensions.addMinutes(fechaFin, Integer.parseInt(hm[1]));
        } else if (uiEntity.get("fechaFin") != null) {
            fechaFin = dateFormat.parse(uiEntity.get("fechaFin"));
        }

        Date fechaPrevDevol = null;

        if (uiEntity.get("fechaPrevDevol") != null)
            fechaPrevDevol = dateFormat.parse(uiEntity.get("fechaPrevDevol"));

        bicicletasUsuarioService.updateReservaBicicleta(reservaId, Long.parseLong(uiEntity.get("tipoReservaId")), fechaFin, fechaPrevDevol, uiEntity.get("observaciones"));
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteReservaBicicleta(@PathParam("id") Long reservaId) throws CommonException {
        bicicletasUsuarioService.deleteReservaBicicleta(reservaId);
        return Response.ok().build();
    }

    @PUT
    @Path("darBicicleta/{reservaId}")
    public Response darBicicleta(@PathParam("reservaId") Long reservaId) throws CommonException {
        try {
            bicicletasUsuarioService.darBicicleta(reservaId);
            return Response.ok().build();
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    @PUT
    @Path("cambiarBicicleta/{bicicletaAntigua}/{reservaId}")
    public Response cambiarBicicleta(@PathParam("bicicletaAntigua") Long bicicletaAntigua,
                                     @PathParam("reservaId") Long reservaId) throws CommonException {
        bicicletasUsuarioService.cambiarBicicleta(bicicletaAntigua, reservaId);
        return Response.ok().build();
    }

    @PUT
    @Path("devolverBicicleta/{reservaId}/{bicicleta}/{estado}")
    public Response devolverBicicleta(@PathParam("reservaId") Long reservaId,
                                      @PathParam("bicicleta") Long bicicleta,
                                      @PathParam("estado") Long estado) throws CommonException {
        Long conectedUser = AccessManager.getConnectedUserId(request);
        bicicletasUsuarioService.devolverBicicleta(reservaId, bicicleta, estado, conectedUser);
        return Response.ok().build();
    }

    @GET
    @Path("historico/{personaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getListadoBicicletasHistorico(@PathParam("personaId") Long perId,
                                                         @QueryParam("start") @DefaultValue("0") Long start,
                                                         @QueryParam("limit") @DefaultValue("25") Long limit,
                                                         @QueryParam("sort") @DefaultValue("[]") String sort) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            Paginacion paginacion = new Paginacion(start, limit, sort);
            List<HistoricoBicicletas> result = bicicletasUsuarioService.getListadoBicicletasHistorico(perId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(result));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("tipoReservas/{cursoAca}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getListadoTipoReservasByYear(@PathParam("cursoAca") Long cursoAca) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            responseMessage.setData(UIEntity.toUI(bicicletasUsuarioService.getListadoTipoReservasByYear(cursoAca)));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
