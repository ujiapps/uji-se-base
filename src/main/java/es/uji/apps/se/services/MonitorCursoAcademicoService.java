package es.uji.apps.se.services;

import es.uji.apps.se.dao.MonitorCursoAcademicoDAO;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.commons.rest.ParamUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class MonitorCursoAcademicoService {

    private MonitorCursoAcademicoDAO monitorCursoAcademicoDAO;
    private ParametroService parametroService;

    @Autowired
    public MonitorCursoAcademicoService(MonitorCursoAcademicoDAO monitorCursoAcademicoDAO, ParametroService parametroService) {

        this.monitorCursoAcademicoDAO = monitorCursoAcademicoDAO;
        this.parametroService = parametroService;
    }

    public List<MonitorCursoAcademicoDTO> getMonitoresCursoAcademicoByOferta(OfertaDTO ofertaDTO) {
        return monitorCursoAcademicoDAO.getMonitoresCursoAcademicoByOferta(ofertaDTO);
    }

    public MonitorCursoAcademicoDTO insertMonitorCursoAcademico(OfertaDTO ofertaDestinoDTO, MonitorOfertaDTO monitorOfertaOrigenDTO) {

        MonitorCursoAcademicoDTO monitorCursoAcademicoDTO;
        monitorCursoAcademicoDTO = new MonitorCursoAcademicoDTO();
        monitorCursoAcademicoDTO.setMonitor(monitorOfertaOrigenDTO.getMonitorCursoAcademico().getMonitor());
        monitorCursoAcademicoDTO.setFechaAlta(new Date());
        monitorCursoAcademicoDTO.setCursoAcademico(ofertaDestinoDTO.getPeriodo().getCursoAcademico());

        return monitorCursoAcademicoDAO.insert(monitorCursoAcademicoDTO);
    }

    public MonitorCursoAcademicoDTO getMonitorCursoByMonitorAndCurso(MonitorDTO monitorDTO, CursoAcademicoDTO cursoAcademicoDTO) {
        return monitorCursoAcademicoDAO.getMonitorCursoByMonitorAndCurso(monitorDTO, cursoAcademicoDTO);
    }

    public List<MonitorCursoAcademicoDTO> getMonitoresCursoAcademico(Long connectedUserId)
    {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        return monitorCursoAcademicoDAO.getMonitoresByCursoAcademico(cursoAcademico.getId());
    }

    @Transactional
    public void eliminaMonitorCursoAcademico(Long cursoAcademico) {
        monitorCursoAcademicoDAO.delete(MonitorCursoAcademicoDTO.class, "curso_aca = " + cursoAcademico);
    }
}
