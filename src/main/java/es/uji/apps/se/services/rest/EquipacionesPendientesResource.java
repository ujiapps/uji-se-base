package es.uji.apps.se.services.rest;

import au.com.bytecode.opencsv.CSVWriter;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.ListaEquipacionesPendientesItemsVWDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.EquipacionesPendientes;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.EquipacionesPendientesService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EquipacionesPendientesResource extends CoreBaseService {
    private static final Logger log = LoggerFactory.getLogger(EquipacionesPendientesResource.class);

    @InjectParam
    EquipacionesPendientesService equipacionesPendientesService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getPendientesDevolucionEquipacion(@QueryParam("fechaHasta") String fecha,
                                                             @QueryParam("sort") @DefaultValue("[]") String sortJson) throws ParseException, IOException {

        Paginacion paginacion = new Paginacion(0L, 1000000L, sortJson);

        Long conectedUserId = AccessManager.getConnectedUserId(request);

        ResponseMessage responseMessage = new ResponseMessage(true);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaHasta;
        if(fecha != null) {
            fechaHasta = sdf.parse(fecha);
        }else {
            fechaHasta = new Date();
        }
        try {
            List<EquipacionesPendientes> listaPendientes = equipacionesPendientesService.getPendientesDevolucionEquipacion(fechaHasta, conectedUserId, paginacion);
            responseMessage.setData(UIEntity.toUI(listaPendientes));
        } catch (Exception e) {
            log.error("getPendientesDevolucionEquipacion", e);
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("imprimirCarta/{cursoAca}")
    public UIEntity imprimirCarta(@PathParam("cursoAca") Long cursoAcademico) {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        UIEntity response = new UIEntity();
        response.put("url", "https://e-ujier.uji.es/cocoon/SE/EQUIPACIONES/MOROSOS/" + conectedUserId + "/" + cursoAcademico + "/informe.pdf");
        return response;
    }

    @GET
    @Path("{cursoAca}/descargar/csv")
    @Produces("application/vnd.ms-excel")
    public Response descargarListadoExpedientesPendientes(@PathParam("cursoAca") Long cursoAca,
                                                          @QueryParam("fecha") String fecha,
                                                          @QueryParam("sort") @DefaultValue("[]") String sortJson) throws ParseException, CommonException {


        SimpleDateFormat formatoOriginal = new SimpleDateFormat("dd/MM/yyyy");
        Date filtroFecha = formatoOriginal.parse(fecha);

        List<ListaEquipacionesPendientesItemsVWDTO> equipacionesPendientes = equipacionesPendientesService.getListadoEquipacionesPendientes(cursoAca, filtroFecha);

        Response.ResponseBuilder res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = listadoEquipacionesPendientes.csv");

        res.entity(listadoExpedientesPendientesToCSV(equipacionesPendientes));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    private String listadoExpedientesPendientesToCSV(List<ListaEquipacionesPendientesItemsVWDTO> equipacionesPendientes) throws CommonException {
        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

        try {

            List<String[]> records = new ArrayList<String[]>();
            List<String> record = new ArrayList<String>(Arrays.asList("Apellidos Nombre", "Identificación", "Curso Académico", "Fecha Devolución", "Teléfono", "Nombre"));

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (ListaEquipacionesPendientesItemsVWDTO pendientes : equipacionesPendientes) {
                List<String> recordMov = new ArrayList<String>(
                        Arrays.asList(
                                pendientes.getNombreCompleto() /*!= null ? pendientes.getNombreCompleto() : ""*/,
                                pendientes.getIdentificacion() /*!= null ? pendientes.getIdentificacion() : ""*/,
                                pendientes.getCurso().toString() /*!= null ? pendientes.getCurso() : 0L*/,
                                pendientes.getFechaDevolucion().toString(),
                                pendientes.getTelefono() /*!= null ? pendientes.getTelefono() : ""*/,
                                pendientes.getNombreEquipacion() /*!= null ? pendientes.getNombreEquipacion() : ""*/));
                String[] recordArrayMov = new String[recordMov.size()];
                recordMov.toArray(recordArrayMov);
                records.add(recordArrayMov);
            }
            csvWriter.writeAll(records);
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
        return writer.toString();
    }
}
