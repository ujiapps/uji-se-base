package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaReciboService {
    private PersonaReciboDAO personaReciboDAO;

    @Autowired
    public PersonaReciboService(PersonaReciboDAO personaReciboDAO) {
        this.personaReciboDAO = personaReciboDAO;
    }

    public List<es.uji.apps.se.model.Recibo> getRecibosPersona(Paginacion paginacion, Long personaId) {
        return personaReciboDAO.getRecibos(paginacion, personaId);
    }

    public List<ReciboPendienteActividad> getRecibosPendientesActividades(Paginacion paginacion, Long personaId) {
        return personaReciboDAO.getRecibosPendientesActividades(paginacion, personaId);
    }

    public List<ReciboPendienteTarjeta> getRecibosPendientesTarjetas(Paginacion paginacion, Long personaId) {
        return personaReciboDAO.getRecibosPendientesTarjetas(paginacion, personaId);
    }

    public List<ReciboPendienteCapitanActividad> getRecibosPendientesCapitanActividades(Paginacion paginacion, Long personaId) {
        return personaReciboDAO.getRecibosPendientesCapitanActividades(paginacion, personaId);
    }
}
