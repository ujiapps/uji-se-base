package es.uji.apps.se.services;

import es.uji.apps.se.dao.PeriodoDAO;
import es.uji.apps.se.dto.CursoAcademicoDTO;
import es.uji.apps.se.dto.PeriodoDTO;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class PeriodoService {

    private PeriodoDAO periodoDAO;
    private OfertaService ofertaService;

    @Autowired
    public PeriodoService(PeriodoDAO periodoDAO, OfertaService ofertaService) {

        this.periodoDAO = periodoDAO;
        this.ofertaService = ofertaService;
    }

    public List<PeriodoDTO> getPeriodosByCurso(Long curso) {
        return this.periodoDAO.getPeriodosByCurso(curso);
    }

    public PeriodoDTO getPeriodoById(Long periodoId) {
        return this.periodoDAO.get(PeriodoDTO.class, periodoId).get(0);
    }

    public PeriodoDTO updateOrInsertPeriodo(Long curso, PeriodoDTO periodoDTO) {

        CursoAcademicoDTO cursoAcademicoDTO = new CursoAcademicoDTO();
        cursoAcademicoDTO.setId(curso);
        periodoDTO.setCursoAcademico(cursoAcademicoDTO);

        if (ParamUtils.isNotNull(periodoDTO.getId()))
            return updatePeriodo(periodoDTO);
        else {
            return addPeriodo(periodoDTO);
        }
    }

    private PeriodoDTO addPeriodo(PeriodoDTO periodoDTO) {
        return this.periodoDAO.insert(periodoDTO);
    }

    private PeriodoDTO updatePeriodo(PeriodoDTO periodoDTO) {
        return this.periodoDAO.update(periodoDTO);
    }

    @Transactional
    public void deletePeriodo(Long periodoId) {

        PeriodoDTO periodoDTO = new PeriodoDTO();
        periodoDTO.setId(periodoId);
        ofertaService.eliminaOfertasPeriodo(periodoDTO);

        this.periodoDAO.delete(PeriodoDTO.class, periodoId);
    }


    @Transactional
    public PeriodoDTO duplicarPeriodo(Long cursoActualId, Long periodoId, String nombre, Date fechaInicio, Date fechaFin) {

        PeriodoDTO periodoOrigenDTO = getPeriodoById(periodoId);

        PeriodoDTO periodoDestinoDTO = new PeriodoDTO();

        periodoDestinoDTO.setNombre(nombre);
        periodoDestinoDTO.setFechaInicio(fechaInicio);
        periodoDestinoDTO.setFechaFin(fechaFin);
        periodoDestinoDTO.setClasificacion(periodoOrigenDTO.getClasificacion());

        CursoAcademicoDTO cursoAcademicoDTO = new CursoAcademicoDTO();
        cursoAcademicoDTO.setId(cursoActualId);
        periodoDestinoDTO.setCursoAcademico(cursoAcademicoDTO);
        this.periodoDAO.insert(periodoDestinoDTO);

        ofertaService.generaOfertasDesdePeriodoOrigenAPeriodoDestino(periodoOrigenDTO, periodoDestinoDTO);

        return periodoDestinoDTO;

    }

    @Transactional
    public void eliminaPeriodoCursoAcademico(Long cursoAcademico) {
        List<PeriodoDTO> periodosDTO = getPeriodosByCurso(cursoAcademico);
        for (PeriodoDTO periodoDTO : periodosDTO) {
            deletePeriodo(periodoDTO.getId());
        }
    }

    public List<PeriodoDTO> getPeriodosConInscripcionUsuario(Long persona) {

        return periodoDAO.getPeriodosConInscripcionUsuario(persona);

    }

    public List<PeriodoDTO> getPeriodosActivoRenovaciones()
    {
        return periodoDAO.getPeriodosActivoRenovaciones();
    }
}

