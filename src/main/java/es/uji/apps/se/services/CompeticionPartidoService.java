package es.uji.apps.se.services;

import es.uji.apps.se.dao.CompeticionMiembroDAO;
import es.uji.apps.se.dao.CompeticionPartidoDAO;
import es.uji.apps.se.model.CompeticionPartido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompeticionPartidoService {

    private CompeticionPartidoDAO competicionPartidoDAO;

    @Autowired
    public CompeticionPartidoService(CompeticionPartidoDAO competicionPartidoDAO) {
        this.competicionPartidoDAO = competicionPartidoDAO;
    }

    public CompeticionPartido getCompeticionPartidoById(Long competicionId) {
        return competicionPartidoDAO.getCompeticionPartidoById(competicionId);
    }

    public void updateObservacionesPartido(Long partId, String observaciones){

        competicionPartidoDAO.updateObservacionesPartido(partId, observaciones);

    }

    public CompeticionPartido getCompeticionPartidoByIdEntero(Long competicionId) {
        return competicionPartidoDAO.getCompeticionPartidoByIdEntero(competicionId);
    }
}
