package es.uji.apps.se.services;

import es.uji.apps.se.dao.IncidenciaDAO;
import es.uji.apps.se.dto.IncidenciaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncidenciaService {

    private IncidenciaDAO incidenciaDAO;

    @Autowired
    public IncidenciaService(IncidenciaDAO incidenciaDAO) {

        this.incidenciaDAO = incidenciaDAO;
    }

    public List<IncidenciaDTO> getIncidencias() {
        return incidenciaDAO.getIncidencias();
    }
}
