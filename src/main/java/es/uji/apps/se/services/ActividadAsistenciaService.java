package es.uji.apps.se.services;

import es.uji.apps.se.dao.ActividadAsistenciaDAO;
import es.uji.apps.se.dto.ActividadAsistenciaDTO;
import es.uji.apps.se.dto.CompeticionPartidoDTO;
import es.uji.apps.se.dto.InscripcionDTO;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActividadAsistenciaService {

    private ActividadAsistenciaDAO actividadAsistenciaDAO;

    @Autowired
    public ActividadAsistenciaService(ActividadAsistenciaDAO actividadAsistenciaDAO) {

        this.actividadAsistenciaDAO = actividadAsistenciaDAO;
    }

    public ActividadAsistenciaDTO insertaAsistenciaByInscripcionYPartido(Long inscripcionId, Long partidoId, String tipo) {

        ActividadAsistenciaDTO actividadAsistenciaDTO = getAsistenciaByInscripcionYPartido(inscripcionId, partidoId);

        if (ParamUtils.isNotNull(actividadAsistenciaDTO)) {
            borraAsistencia(inscripcionId, partidoId);
            return null;
        } else {
            return insertaAsistencia(inscripcionId, partidoId, tipo);
        }
    }

    private void borraAsistencia(Long inscripcionId, Long partidoId) {
        actividadAsistenciaDAO.delete(ActividadAsistenciaDTO.class, "partido_id = " + partidoId + " and inscripcion_id = " + inscripcionId);
    }

    public ActividadAsistenciaDTO insertaAsistencia(Long inscripcionId, Long partidoId, String tipo) {

        ActividadAsistenciaDTO actividadAsistenciaDTO = new ActividadAsistenciaDTO();

        actividadAsistenciaDTO.setTipoEntrada(tipo);

        CompeticionPartidoDTO competicionPartidoDTO = new CompeticionPartidoDTO();
        competicionPartidoDTO.setId(partidoId);
        actividadAsistenciaDTO.setPartido(competicionPartidoDTO);

        InscripcionDTO inscripcionDTO = new InscripcionDTO();
        inscripcionDTO.setId(inscripcionId);
        actividadAsistenciaDTO.setInscripcion(inscripcionDTO);

        return actividadAsistenciaDAO.insert(actividadAsistenciaDTO);
    }

    private ActividadAsistenciaDTO getAsistenciaByInscripcionYPartido(Long inscripcionId, Long partidoId) {
        return actividadAsistenciaDAO.getAsistenciaByInscripcionYPartido(inscripcionId, partidoId);
    }
}
