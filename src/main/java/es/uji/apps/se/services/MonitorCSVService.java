package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.MonitorCSVDAO;
import es.uji.apps.se.dto.views.MonitorCSV;
import es.uji.apps.se.utils.Csv;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class MonitorCSVService {
    private MonitorCSVDAO monitorCSVDAO;

    @InjectParam
    Csv csv;

    @Autowired
    public MonitorCSVService(MonitorCSVDAO monitorCSVDAO) {

        this.monitorCSVDAO = monitorCSVDAO;
    }

    public List<MonitorCSV> getCsvBySearch(List<Long> clases, List<Long> instalaciones, Date fechaInicio, Date fechaFin) {
        return monitorCSVDAO.getCsvBySearch(clases, instalaciones, fechaInicio, fechaFin);
    }


    public String entityMonitorToCSV(List<MonitorCSV> monitoresCSV) {

        ArrayList<String> cabeceras = new ArrayList<String>(Arrays.asList("Nom", "classe", "dia", "hora inici", "instalació", "tipus entrada", "places", "reserves", "assistències"));

        List<List<String>> records = new ArrayList<List<String>>();
        for (MonitorCSV monitorCSV : monitoresCSV) {
            List<String> recordMov = new ArrayList<String>(creaListaString(monitorCSV));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(MonitorCSV monitorCSV) {
        return Arrays.asList(
                monitorCSV.getApellidosNombre(),
                monitorCSV.getClase(),
                ParamUtils.isNotNull(monitorCSV.getDia()) ? monitorCSV.getDia().toString() : "",
                monitorCSV.getHoraInicio(),
                monitorCSV.getInstalacion(),
                monitorCSV.getTipoEntrada(),
                monitorCSV.getPlazas().toString(),
                monitorCSV.getReservas().toString(),
                monitorCSV.getAsistencias().toString()
        );
    }
}
