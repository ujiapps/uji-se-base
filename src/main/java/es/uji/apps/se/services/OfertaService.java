package es.uji.apps.se.services;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.se.dto.InscripcionDTO;
import es.uji.apps.se.dto.OfertaDTO;
import es.uji.apps.se.dto.PeriodoDTO;
import es.uji.apps.se.model.Paginacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.se.dao.OfertaDAO;
import es.uji.apps.se.model.ui.UIRenovacion;
import es.uji.commons.rest.ParamUtils;

@Service
public class OfertaService
{
    @InjectParam
    private PeriodoService periodoService;

    private OfertaDAO ofertaDAO;
    private MonitorOfertaService monitorOfertaService;
    private OfertaTarifaService ofertaTarifaService;
    private CalendarioOfertaService calendarioOfertaService;
    private InscripcionService inscripcionService;

    @Autowired
    public OfertaService(OfertaDAO ofertaDAO, MonitorOfertaService monitorOfertaService, OfertaTarifaService ofertaTarifaService,
                         CalendarioOfertaService calendarioOfertaService,
                         InscripcionService inscripcionService)
    {

        this.ofertaDAO = ofertaDAO;
        this.monitorOfertaService = monitorOfertaService;
        this.ofertaTarifaService = ofertaTarifaService;
        this.calendarioOfertaService = calendarioOfertaService;
        this.inscripcionService = inscripcionService;
    }

    public void generaOfertasDesdePeriodoOrigenAPeriodoDestino(PeriodoDTO periodoOrigenDTO, PeriodoDTO periodoDestinoDTO)
    {
        List<OfertaDTO> ofertasDTO = getOfertasByPeriodo(periodoOrigenDTO);
        for (OfertaDTO ofertaDTO : ofertasDTO)
        {
            OfertaDTO ofertaDestinoDTO = generaOfertaDestino(periodoDestinoDTO, ofertaDTO);
            monitorOfertaService.generaMonitorOfertaDestino(ofertaDTO, ofertaDestinoDTO);
//            ofertaTarifaService.generaOfertasTarifasDestino(oferta, ofertaDestino);
            calendarioOfertaService.generarCalendariosOfertaDestino(ofertaDTO, ofertaDestinoDTO);
        }
    }

    private OfertaDTO generaOfertaDestino(PeriodoDTO periodoDestinoDTO, OfertaDTO ofertaDTO)
    {

        OfertaDTO ofertaDestinoDTO = new OfertaDTO();
        ofertaDestinoDTO.setAceptaCreditos(ofertaDTO.getAceptaCreditos());
        ofertaDestinoDTO.setActividad(ofertaDTO.getActividad());
        ofertaDestinoDTO.setComentarioAsistencias(ofertaDTO.getComentarioAsistencias());
        ofertaDestinoDTO.setContabilizarAsistencia(ofertaDTO.getContabilizarAsistencia());
        ofertaDestinoDTO.setDaEquipacion(ofertaDTO.getDaEquipacion());
        ofertaDestinoDTO.setDescripcion(ofertaDTO.getDescripcion());
        ofertaDestinoDTO.setFechaFinEquipacion(ofertaDTO.getFechaFinEquipacion());
        ofertaDestinoDTO.setFechaFinInscripcion(ofertaDTO.getFechaFinInscripcion());
        ofertaDestinoDTO.setFechaFinNacimiento(ofertaDTO.getFechaFinNacimiento());
        ofertaDestinoDTO.setFechaFinPreinscripcion(ofertaDTO.getFechaFinPreinscripcion());
        ofertaDestinoDTO.setFechaFinRenovacion(ofertaDTO.getFechaFinRenovacion());
        ofertaDestinoDTO.setFechaFinValidacion(ofertaDTO.getFechaFinValidacion());
        ofertaDestinoDTO.setFechaFinWeb(ofertaDTO.getFechaFinWeb());
        ofertaDestinoDTO.setFechaInicioCobroRecibos(ofertaDTO.getFechaInicioCobroRecibos());
        ofertaDestinoDTO.setFechaInicioEquipacion(ofertaDTO.getFechaInicioEquipacion());
        ofertaDestinoDTO.setFechaInicioInscripcion(ofertaDTO.getFechaInicioInscripcion());
        ofertaDestinoDTO.setFechaInicioNacimiento(ofertaDTO.getFechaInicioNacimiento());
        ofertaDestinoDTO.setFechaInicioPreinscripcion(ofertaDTO.getFechaInicioPreinscripcion());
        ofertaDestinoDTO.setFechaInicioRenovacion(ofertaDTO.getFechaInicioRenovacion());
        ofertaDestinoDTO.setFechaInicioValidacion(ofertaDTO.getFechaInicioValidacion());
        ofertaDestinoDTO.setFechaInicioWeb(ofertaDTO.getFechaInicioWeb());
        ofertaDestinoDTO.setHorario(ofertaDTO.getHorario());
        ofertaDestinoDTO.setIbanAbono(ofertaDTO.getIbanAbono());
        ofertaDestinoDTO.setMirarSolapamientoInstaciones(ofertaDTO.getMirarSolapamientoInstaciones());
        ofertaDestinoDTO.setNombre(ofertaDTO.getNombre());
        ofertaDestinoDTO.setPeriodo(periodoDestinoDTO);
        ofertaDestinoDTO.setPermiteInscripcionWeb(ofertaDTO.getPermiteInscripcionWeb());
        ofertaDestinoDTO.setPlazas(ofertaDTO.getPlazas());
        ofertaDestinoDTO.setPlazasMaximo(ofertaDTO.getPlazasMaximo());
        ofertaDestinoDTO.setPremitePreinscripcion(ofertaDTO.getPremitePreinscripcion());
        ofertaDestinoDTO.setValorDefectoApto(ofertaDTO.getValorDefectoApto());

        return ofertaDAO.insert(ofertaDestinoDTO);
    }

    private List<OfertaDTO> getOfertasByPeriodo(PeriodoDTO periodoDTO)
    {
        return ofertaDAO.getOfertasByPeriodo(periodoDTO.getId());
    }

    @Transactional
    public void eliminaOfertasPeriodo(PeriodoDTO periodoDTO)
    {

        List<OfertaDTO> ofertasDTO = getOfertasByPeriodo(periodoDTO);

        for (OfertaDTO ofertaDTO : ofertasDTO)
        {
            monitorOfertaService.eliminaMonitorOferta(ofertaDTO);
//            ofertaTarifaService.eliminarOfertaTarifaOferta(oferta);
            calendarioOfertaService.eliminaCalendariosOfertaOferta(ofertaDTO);
            ofertaDAO.delete(OfertaDTO.class, ofertaDTO.getId());

        }
    }

    public List<UIRenovacion> getOfertasPendientesRenovacionByUsuario(Long connectedUserId)
    {
        List<PeriodoDTO> periodosActual = periodoService.getPeriodosActivoRenovaciones();

        if (ParamUtils.isNotNull(periodosActual))
        {
            List<UIRenovacion> ofertasPendientes = new ArrayList<UIRenovacion>();

            for (PeriodoDTO periodoActualDTO : periodosActual)
            {
                if (ParamUtils.isNotNull(periodoActualDTO.getPeriodoReferencia()))
                {

                    List<InscripcionDTO> inscripciones = inscripcionService.getInscripcionesUsuarioPorPeriodo(connectedUserId, periodoActualDTO.getPeriodoReferencia().getId());

                    for (InscripcionDTO inscripcionDTO : inscripciones)
                    {
                        OfertaDTO ofertaNuevaDTO = getOfertaByActividadByNombreByPeriodo(inscripcionDTO.getOferta().getActividad().getId(),
                                inscripcionDTO.getOferta().getNombre(), periodoActualDTO.getId());
                        if (ParamUtils.isNotNull(ofertaNuevaDTO))
                        {
                            UIRenovacion renovacion = new UIRenovacion(ofertaNuevaDTO);
                            renovacion.setInscrito(inscripcionService.getInscripcionUsuario(connectedUserId, ofertaNuevaDTO.getId()));
                            renovacion.setDevolucion(inscripcionService.getDevolucionUsuario(connectedUserId, ofertaNuevaDTO.getId()));
                            renovacion.setTieneRenovacion(Boolean.TRUE);
                            ofertasPendientes.add(renovacion);
                        }
                        else{
                            UIRenovacion renovacion = new UIRenovacion(inscripcionDTO.getOferta());
                            renovacion.setTieneRenovacion(Boolean.FALSE);
                            renovacion.setDevolucion(inscripcionService.getDevolucionUsuario(connectedUserId, inscripcionDTO.getOferta().getId()));
                            ofertasPendientes.add(renovacion);
                        }
                    }

                }
            }
            return ofertasPendientes;
        }
        else
        {
            return null;
        }


    }

    private OfertaDTO getOfertaByActividadByNombreByPeriodo(Long actividadId, String ofertaNombre, Long periodoId)
    {
        return ofertaDAO.getOfertaByActividadByNombreByPeriodo(actividadId, ofertaNombre, periodoId);
    }

    public List<OfertaDTO> getOfertasByActividad(Long actividadId, Long cursoId, Paginacion paginacion) {
        return ofertaDAO.getOfertasByActividad(actividadId, cursoId, paginacion);
    }

    public List<OfertaDTO> getOfertas() {

        return ofertaDAO.getOfertas();
    }

    public List<OfertaDTO> getOfertasByCursoId(Long cursoId) {
        return ofertaDAO.getOfertasByCursoId(cursoId);
    }
}
