package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.TarjetaDeportivaReservaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import java.util.List;

@Path("tarjetadeportivareserva")
public class TarjetaDeportivaReservaResource extends CoreBaseService
{
    @InjectParam
    TarjetaDeportivaReservaService tarjetaDeportivaReservaService;

    @PathParam("tarjetaDeportivaId")
    Long tarjetaDeportivaId;

    @GET
    public List<UIEntity> getReservasByTarjetaDeportivaId()
    {
        return UIEntity.toUI(tarjetaDeportivaReservaService.getReservasByTarjetaDeportivaId(tarjetaDeportivaId));
    }
}
