package es.uji.apps.se.services.rest;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.se.exceptions.BorradoClaseDirigidaException;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.apps.se.dto.views.ClaseCSV;
import es.uji.apps.se.dto.views.MonitorCSV;
import es.uji.apps.se.services.ClaseCSVService;
import es.uji.apps.se.services.MonitorCSVService;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.se.dto.ClaseDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.ClaseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("clase")
public class ClaseResource extends CoreBaseService {

    @Path("{id}/calendario")
    public CalendarioClaseResource getPlatformItem(
            @InjectParam CalendarioClaseResource calendarioClaseResource) {
        return calendarioClaseResource;
    }

    @Path("{id}/materialreserva")
    public MaterialReservaResource getPlatformItem(
            @InjectParam MaterialReservaResource materialReservaResource) {
        return materialReservaResource;
    }

    @Path("{claseId}/monitorcalendario")
    public MonitorCalendarioResource getPlatformItem(
            @InjectParam MonitorCalendarioResource monitorCalendarioResource) {
        return monitorCalendarioResource;
    }

    @InjectParam
    ClaseService claseService;
    @InjectParam
    ClaseCSVService claseCSVService;
    @InjectParam
    MonitorCSVService monitorCSVService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getClases(@QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                     @QueryParam("sort") String ordenacion) throws IOException {

        Paginacion paginacion = new Paginacion(start, limit, ordenacion);
        List<ClaseDTO> lista = claseService.getClases(paginacion);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(UIEntity.toUI(lista));
        return responseMessage;
    }

    @GET
    @Path("maestros/export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClasesParaExport() {

        List<ClaseDTO> lista = claseService.getClasesParaExport();
        Response.ResponseBuilder res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = clases.csv");
        res.entity(claseService.entityClaseToCSV(lista));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    @GET
    @Path("activa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClasesActivas() {
        List<ClaseDTO> lista = claseService.getClasesActivas();
        return modelToUI(lista);
    }

    @GET
    @Path("export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSVClasesDirigidas(@QueryParam("clases") String clasesParam, @QueryParam("fechaInicio") String fechaInicio,
                                          @QueryParam("fechaFin") String fechaFin, @QueryParam("instalaciones") String instalacionesParam) throws ParseException {

        List<Long> clases = null;
        if (ParamUtils.isNotNull(clasesParam))
            clases = Arrays.stream(clasesParam.split(",")).map(ParamUtils::parseLong).collect(Collectors.toList());

        List<Long> instalaciones = null;
        if (ParamUtils.isNotNull(instalacionesParam))
            instalaciones = Arrays.stream(instalacionesParam.split(",")).map(ParamUtils::parseLong).collect(Collectors.toList());

        Date fechaInicioDate = new UIDia(fechaInicio).getFecha();
        Date fechaFinDate = new UIDia(fechaFin).getFecha();

        List<ClaseCSV> lista = claseCSVService.getCsvBySearch(clases, instalaciones, fechaInicioDate, fechaFinDate);

        Response.ResponseBuilder res = Response.ok();

        res.header("Content-Disposition", "attachment; filename = clases.csv");

        res.entity(claseCSVService.entityClaseToCSV(lista));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    @GET
    @Path("monitor/export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSVMonitoresClasesDirigidas(@QueryParam("clases") String clasesParam, @QueryParam("fechaInicio") String fechaInicio,
                                                   @QueryParam("fechaFin") String fechaFin, @QueryParam("instalaciones") String instalacionesParam) throws ParseException {

        List<Long> clases = null;
        if (ParamUtils.isNotNull(clasesParam))
            clases = Arrays.stream(clasesParam.split(",")).map(ParamUtils::parseLong).collect(Collectors.toList());

        List<Long> instalaciones = null;
        if (ParamUtils.isNotNull(instalacionesParam))
            instalaciones = Arrays.stream(instalacionesParam.split(",")).map(ParamUtils::parseLong).collect(Collectors.toList());

        Date fechaInicioDate = new UIDia(fechaInicio).getFecha();
        Date fechaFinDate = new UIDia(fechaFin).getFecha();

        List<MonitorCSV> lista = monitorCSVService.getCsvBySearch(clases, instalaciones, fechaInicioDate, fechaFinDate);

        Response.ResponseBuilder res = Response.ok();

        res.header("Content-Disposition", "attachment; filename = clases.csv");

        res.entity(monitorCSVService.entityMonitorToCSV(lista));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }


    private List<UIEntity> modelToUI(List<ClaseDTO> lista) {
        List<UIEntity> listaUI = new ArrayList<>();
        for (ClaseDTO claseDTO : lista) {
            listaUI.add(modelToUI(claseDTO));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ClaseDTO claseDTO) {
        UIEntity entity = UIEntity.toUI(claseDTO);
        entity.put("Name", claseDTO.getNombre());
        entity.put("Id", claseDTO.getId());
        entity.put("Color", "#cf2424");

        return entity;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertClase(UIEntity entity) {
        return UIEntity.toUI(claseService.insertClase(entity.toModel(ClaseDTO.class)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertClase(@PathParam("id") Long claseId, UIEntity entity) {
        return UIEntity.toUI(claseService.updateClase(entity.toModel(ClaseDTO.class)));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteClase(@PathParam("id") Long claseId) throws BorradoClaseDirigidaException {
        claseService.deleteClase(claseId);
    }

    @GET
    @Path("/clases-dirigidas")
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity getRecuentoClasesDirigidas(@QueryParam("cursoAcademico") Long cursoAcademico) {
        UIEntity entity = new UIEntity();
        entity.put("clasesDirigidas", claseService.getRecuentoClasesDirigidas(cursoAcademico));
        return entity;
    }

    @GET
    @Path("/clases-dirigidas/activas")
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity getRecuentoClasesDirigidasActivas() {
        UIEntity entity = new UIEntity();
        entity.put("clasesDirigidasActivas", claseService.getRecuentoClasesDirigidasActivas());
        return entity;
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("calendario")
    public void deleteClaseMultiple(@FormParam("claseDirigida") List<Long> claseId,
                                    @FormParam("fechaInicio") String fechaInicio,
                                    @FormParam("fechaFin") String fechaFin,
                                    @FormParam("horaInicio") String horaInicio,
                                    @FormParam("horaFin") String horaFin,
                                    @FormParam("diasSemana") List<Integer> diasSemana) {
        claseService.deleteClaseMultiple(claseId, fechaInicio, fechaFin, horaInicio, horaFin, diasSemana);
    }

}
