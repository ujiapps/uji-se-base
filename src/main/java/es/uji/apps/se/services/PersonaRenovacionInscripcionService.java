package es.uji.apps.se.services;

import es.uji.apps.se.dao.PersonaRenovacionInscripcionDAO;
import es.uji.apps.se.exceptions.CommonException;
import org.springframework.stereotype.Service;

@Service
public class PersonaRenovacionInscripcionService {

    private final PersonaRenovacionInscripcionDAO personaRenovacionInscripcionDAO;

    public PersonaRenovacionInscripcionService(PersonaRenovacionInscripcionDAO personaRenovacionInscripcionDAO) {
        this.personaRenovacionInscripcionDAO = personaRenovacionInscripcionDAO;
    }

    public void fichaRenovacion(Long personaId, Long grupoId) throws CommonException {
        try {
            personaRenovacionInscripcionDAO.fichaRenovacion(personaId, grupoId, null);
        }
        catch (Exception e) {
            throw new CommonException("No s'ha pogut renovar aquesta inscripció");
        }
    }
}
