package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.BonificacionDTO;
import es.uji.apps.se.dto.BonificacionActivaDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.Bonificacion;
import es.uji.apps.se.model.BonificacionActiva;
import es.uji.apps.se.model.BonificacionDetalle;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class BonificacionService {

    private final BonificacionDAO bonificacionDAO;
    private final ActividadDAO actividadDAO;
    private final CarnetBonoTipoDAO carnetBonoTiposDAO;
    private final TarjetaDeportivaTipoDAO tarjetaDeportivaTipoDAO;
    private final TipoDAO tipoDAO;

    @Autowired
    public BonificacionService(BonificacionDAO bonificacionDAO,
                               ActividadDAO actividadDAO,
                               CarnetBonoTipoDAO carnetBonoTiposDAO,
                               TipoDAO tipoDAO,
                               TarjetaDeportivaTipoDAO tarjetaDeportivaTipoDAO) {
        this.bonificacionDAO = bonificacionDAO;
        this.actividadDAO = actividadDAO;
        this.carnetBonoTiposDAO = carnetBonoTiposDAO;
        this.tipoDAO = tipoDAO;
        this.tarjetaDeportivaTipoDAO = tarjetaDeportivaTipoDAO;
    }

    public List<BonificacionActivaDTO> getBonificacionesActivasGenericasByPersonaId(Long personaId) {
        return bonificacionDAO.getBonificacionesActivasGenericasByPersonaId(personaId);
    }

    public List<BonificacionActiva> getBonificacionesAsociadasAActividadesDePersonaByAdmin(Long personaId) {
        return bonificacionDAO.getBonificacionesAsociadasAActividadesDePersonaByAdmin(personaId);
    }

    public BonificacionActivaDTO getBonificacionById(Long bonificacionId) {
        return bonificacionDAO.getBonificacionById(bonificacionId);
    }

    public List<BonificacionActivaDTO> getBonificacionesActivasByTipoTarjetaAndPersonaId(Long personaId, TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO) {
        return bonificacionDAO.getBonificacionesActivasByTipoTarjetaAndPersonaId(personaId, tarjetaDeportivaTipoDTO);
    }

    public List<Bonificacion> getBonificaciones(Long personaId) {
        List<Bonificacion> bonificaciones = bonificacionDAO.getBonificaciones(personaId);
        bonificaciones.forEach(bonificacion -> {
            if (Objects.equals(bonificacion.getOrigen(), "ACT")) {
                if (bonificacion.getReferencia() != null) {
                    String asignacion = actividadDAO.getActividadByReferenciaId(bonificacion.getReferencia());
                    String asignacionCadena = asignacion.split(", ")[0] + " grup " + asignacion.split(",")[1];
                    bonificacion.setAsignacion("Bonificació asignada a l'activitat " + asignacionCadena.substring(1, asignacionCadena.length() - 1));
                } else {
                    bonificacion.setAsignacion("Bonificació asignada a qualsevol activitat");
                }
            } else if (Objects.equals(bonificacion.getOrigen(), "TAR")) {
                if (bonificacion.getReferencia() != null) {
                    String asignacion = tarjetaDeportivaTipoDAO.getTarjetaDeportivaTipoNombreByReferenciaId(bonificacion.getReferencia());
                    bonificacion.setAsignacion("Bonificació asignada a la targeta o abonament " + asignacion);
                } else {
                    bonificacion.setAsignacion("Bonificació asignada a targetes esportives");
                }

            } else if (Objects.equals(bonificacion.getOrigen(), "TB")) {
                String asignacion = carnetBonoTiposDAO.getCarnetByReferenciaId(bonificacion.getReferencia());
                bonificacion.setAsignacion("Bonificació asignada a la targeta o abonament " + asignacion);
            } else if (Objects.equals(bonificacion.getOrigen(), "UBI")) {
                String asignacion = tipoDAO.getUbicacionByReferenciaId(bonificacion.getReferencia());
                if (!ParamUtils.isNotNull(asignacion)) {
                    bonificacion.setAsignacion("Bonificació asignada a totes les instalacions");
                } else {
                    bonificacion.setAsignacion("Bonificació asignada a la ubicació " + asignacion);
                }
            }
        });
        return bonificaciones;
    }

    public List<BonificacionDetalle> getBonificacionDetalles(Long bonificacionId) {
        return bonificacionDAO.getBonificacionDetalles(bonificacionId);
    }

    public void asignarBonificacion(Long referenciaId, Long bonificacionId, String origen) {
        bonificacionDAO.asignarBonificacion(referenciaId, bonificacionId, origen);
    }

    public void deleteBonificacion(Long bonificacionId) {
        bonificacionDAO.deleteBonificacion(bonificacionId);
    }

    public void updateBonificacion(UIEntity entity) throws CommonException {
        try {
            Bonificacion bonificacion = entity.toModel(Bonificacion.class);
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

            if (entity.get("fechaIni") != null)
                bonificacion.setFechaIni(formato.parse(entity.get("fechaIni")));

            if (entity.get("fechaFin") != null)
                bonificacion.setFechaFin(formato.parse(entity.get("fechaFin")));

            bonificacionDAO.updateBonificacion(bonificacion);
        } catch (Exception e) {
            throw new CommonException("No s'ha pogut actualitzar la bonificació");
        }
    }

    @Transactional
    public void addBonificacion(Long personaId) {
        BonificacionDTO bonificacionDTO = new BonificacionDTO();
        bonificacionDTO.setPersonaUji(new PersonaUjiDTO(personaId));
        bonificacionDTO.setFechaIni(new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        bonificacionDTO.setFechaFin(calendar.getTime());
        bonificacionDTO.setMostrar(0L);

        bonificacionDAO.insert(bonificacionDTO);
    }

    public List<BonificacionActiva> getBonificacionesCarnetsOBonoPersonaByAdmin(Long personaId, Long tipoBonoId) {
        List<BonificacionActiva> bonificaciones = bonificacionDAO.getBonificacionesAsociadasACarnetsOBonoTipoDePersonaByAdmin(personaId, tipoBonoId);
        bonificaciones.addAll(bonificacionDAO.getBonificacionesAsociadasACarnetsOBonosDePersonaByAdmin(personaId));
        return bonificaciones;
    }
}
