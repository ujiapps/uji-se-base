package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.*;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class PersonaVinculoService {

    PersonasUJIVinculoDAO personasUJIVinculoDAO;

    @Autowired
    public PersonaVinculoService(PersonasUJIVinculoDAO personasUJIVinculoDAO) {
        this.personasUJIVinculoDAO = personasUJIVinculoDAO;
    }

    public void insertaOActualizaVinculoGenericoDeportes(PersonaDTO persona) {

        Long VINCULODEPORTES = 15L;
        Long SUBVINCULODEPORTES = 1L;

        PersonasSubVinculosDTO personaSubVinculoDTO = personasUJIVinculoDAO.getPersonaVinculoUJIByVinculoYSubvinculo(persona.getPersonaUji().getId(), VINCULODEPORTES, SUBVINCULODEPORTES);

        if (ParamUtils.isNotNull(personaSubVinculoDTO)){
            if (ParamUtils.isNotNull(personaSubVinculoDTO.getFechaFin())){
               personaSubVinculoDTO.setFechaFin(null);
               personaSubVinculoDTO.setFecha(new Date());
                personasUJIVinculoDAO.update(personaSubVinculoDTO);
            }
            return;
        }

        personaSubVinculoDTO.setId(new PersonasSubVinculoDTOPK(persona.getPersonaUji(), VINCULODEPORTES, SUBVINCULODEPORTES));
        personaSubVinculoDTO.setFecha(new Date());
        personasUJIVinculoDAO.insert(personaSubVinculoDTO);
    }
}
