package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.OfertaTarifaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.OfertaTarifaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("ofertatarifa")
public class OfertaTarifaResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(OfertaTarifaResource.class);

    @Path("{ofertaTarifaId}/ofertadescuento")
    public OfertaTarifaDescuentoResource getPlatformItem(
            @InjectParam OfertaTarifaDescuentoResource ofertaTarifaDescuentoResource) {
        return ofertaTarifaDescuentoResource;
    }

    @InjectParam
    OfertaTarifaService ofertaTarifaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTarifas(@PathParam("ofertaId") Long ofertaId, @QueryParam("start") @DefaultValue("0") Long start,
                                              @QueryParam("limit") @DefaultValue("25") Long limit,
                                              @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<OfertaTarifaDTO> tarifas = ofertaTarifaService.getTarifasByOfertaId(ofertaId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(tarifas)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getTarifas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertTarifa(@PathParam("ofertaId") Long ofertaId, UIEntity entity){
        return UIEntity.toUI(ofertaTarifaService.insertTarifa(entity, ofertaId));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTarifa(@PathParam("ofertaId") Long ofertaId, UIEntity entity) {
        return UIEntity.toUI(ofertaTarifaService.updateTarifa(entity, ofertaId));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTarifaTipo(@PathParam("id") Long tarifaId) {
        ofertaTarifaService.deleteTarifa(tarifaId);
    }
}
