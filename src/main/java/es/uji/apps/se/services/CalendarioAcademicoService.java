package es.uji.apps.se.services;

import es.uji.apps.se.dao.CalendarioAcademicoDAO;
import es.uji.apps.se.dto.CalendarioAcademicoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalendarioAcademicoService {


    private CalendarioAcademicoDAO calendarioAcademicoDAO;


    @Autowired
    public CalendarioAcademicoService(CalendarioAcademicoDAO calendarioAcademicoDAO) {

        this.calendarioAcademicoDAO = calendarioAcademicoDAO;
    }

    public List<CalendarioAcademicoDTO> getCalendarioAcademico(Long cursoAcademicoId) {
        return this.calendarioAcademicoDAO.getCalendariosAcademicosByAnyo(cursoAcademicoId);
    }
}
