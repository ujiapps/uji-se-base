package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.model.domains.TipoTaxonomia;
import es.uji.apps.se.services.TipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("tipo")
public class TipoResource extends CoreBaseService {

    @InjectParam
    TipoService tipoService;

    @Path("{tipoId}/clasedirigidatipo")
    public ClaseDirigidaTipoResource getPlatformItem(
            @InjectParam ClaseDirigidaTipoResource claseDirigidaTipoResource) {
        return claseDirigidaTipoResource;
    }

    @GET
    @Path("{id}/periodo/root")
    public List<UIEntity> getTiposByPeriodoId(@PathParam("id") Long periodoId){
        List<TipoDTO> tiposDTO = tipoService.getTiposByPeriodoId(periodoId);

        return tiposDTO.stream().map(a -> {
            UIEntity ui = UIEntity.toUI(a);
            ui.put("text", a.getNombre());
            ui.put("leaf", !tipoService.tieneHijos(a.getId()));

            if (ParamUtils.isNotNull(a.getTipoPadreDTO())) {
                ui.put("parentId", a.getTipoPadreDTO().getId());
            } else {
                ui.put("parentId", 0);
            }
            return ui;
        }).collect(Collectors.toList());
    }

    @GET
    @Path("{id}/periodo/{clasificacionId}")
    public List<UIEntity> getTiposByClasificacionId(@PathParam("clasificacionId") Long clasificacionId){
        List<TipoDTO> tiposDTO = tipoService.getTiposByPeriodoId(clasificacionId);

        return tiposDTO.stream().map(a -> {
            UIEntity ui = UIEntity.toUI(a);
            ui.put("text", a.getNombre());

            ui.put("leaf", !tipoService.tieneHijos(a.getId()));

            if (ParamUtils.isNotNull(a.getTipoPadreDTO())) {
                ui.put("parentId", a.getTipoPadreDTO().getId());
            } else {
                ui.put("parentId", 0);
            }
            return ui;
        }).collect(Collectors.toList());
    }

    @GET
    @Path("clasificacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposClasificacion () {

        return UIEntity.toUI(tipoService.getTiposClasificacion());
    }

    @GET
    @Path("{clasificacionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipos(@PathParam("clasificacionId") Long clasificacionId) {
        return UIEntity.toUI(tipoService.getTipos(clasificacionId));
    }

    @GET
    @Path("clase/tree/{node}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTree(@PathParam("node") @DefaultValue("0") Long node) {

        List<TipoDTO> lista = tipoService.getTipos(node, TipoTaxonomia.CLASE);
        return lista.stream().map(a -> {
            UIEntity ui = UIEntity.toUI(a);
            ui.put("text", a.getNombre());

            ui.put("leaf", !tipoService.tieneHijos(a.getId()));

            if (ParamUtils.isNotNull(a.getTipoPadreDTO())) {
                ui.put("parentId", a.getTipoPadreDTO().getId());
            } else {
                ui.put("parentId", 0);
            }
            return ui;
        }).collect(Collectors.toList());
    }

    @GET
    @Path("clase")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposTaxonomiasClase() {
        return UIEntity.toUI(tipoService.getTiposTaxonomias(TipoTaxonomia.CLASE));
    }

    @GET
    @Path("clase/tarjeta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposTaxonomiasClaseUsoTarjetas() {

        List<TipoDTO> lista = tipoService.getTiposByUso("TARJETAS-CLASES", null, null);
        return UIEntity.toUI(lista);
    }

    @GET
    @Path("vinculotarifa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVinculosTarifas () {

        return UIEntity.toUI(tipoService.getVinculosTarifas());
    }

    @GET
    @Path("zona")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getZonas () {

        return UIEntity.toUI(tipoService.getZonas());
    }

    @POST
    @Path("clase")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertTipoTaxonomiaClase(UIEntity entity) {

        return UIEntity.toUI(tipoService.insertTipoTaxonomia(entity.toModel(TipoDTO.class), TipoTaxonomia.CLASE, ParamUtils.parseLong(entity.get("parentId"))));
    }

    @PUT
    @Path("clase/{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoTaxonomiaClase(@PathParam("id") Long tipoId, @FormParam("newValue") String newValue) {


        return UIEntity.toUI(tipoService.updateTipo(tipoId, newValue));
    }

    @DELETE
    @Path("clase/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteTipoTaxonomiaClase(@PathParam("id") Long id) {

        tipoService.deleteTipo(id);
    }


    @DELETE
    @Path("clase/tree/{node}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteNode(@PathParam("node") Long node) {
        tipoService.deleteTipo(node);
    }


}
