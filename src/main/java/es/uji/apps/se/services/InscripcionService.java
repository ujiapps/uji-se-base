package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.dto.views.VWActividadesAsistenciaDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.apps.se.model.domains.TipoEstadoInscripcion;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InscripcionService {

    public final static Logger log = LoggerFactory.getLogger(InscripcionService.class);

    private final ParametroDAO parametroDAO;
    private final TipoDAO tipoDAO;
    private final OfertaDAO ofertaDAO;
    private final PersonaDAO personaDAO;
    private final InscripcionDAO inscripcionDAO;
    private final BonificacionDAO bonificacionDAO;
    private final DevolucionCovidDAO devolucionCovidDAO;
    private final InscripcionPlazasLibresDAO inscripcionPlazasLibresDAO;
    private final PersonaSancionService personaSancionService;

    @Autowired
    public InscripcionService(OfertaDAO ofertaDAO,
                              PersonaDAO personaDAO,
                              InscripcionDAO inscripcionDAO,
                              BonificacionDAO bonificacionDAO,
                              DevolucionCovidDAO devolucionCovidDAO,
                              InscripcionPlazasLibresDAO inscripcionPlazasLibresDAO,
                              ParametroDAO parametroDAO,
                              TipoDAO tipoDAO,
                              PersonaSancionService personaSancionService) {
        this.ofertaDAO = ofertaDAO;
        this.personaDAO = personaDAO;
        this.inscripcionDAO = inscripcionDAO;
        this.bonificacionDAO = bonificacionDAO;
        this.devolucionCovidDAO = devolucionCovidDAO;
        this.inscripcionPlazasLibresDAO = inscripcionPlazasLibresDAO;
        this.parametroDAO = parametroDAO;
        this.tipoDAO = tipoDAO;
        this.personaSancionService = personaSancionService;
    }

    @InjectParam
    EnviaNotificacion enviaNotificacion;


    public List<InscripcionDTO> getInscripcionesUsuario(Long personaId) {
        return inscripcionDAO.getInscripcionesUsuario(personaId);

    }

    public List<InscripcionDTO> getInscripcionesUsuarioPorPeriodo(Long persona, Long periodo) {
        return inscripcionDAO.getInscripcionesUsuarioPorPeriodo(persona, periodo);
    }

    public void altaInscripcionUsuarioCovid(Long connectedUserId, Long ofertaId) {
        InscripcionDTO inscripcionDTO = new InscripcionDTO();
        TipoDTO definitivo = new TipoDTO();
        definitivo.setId(TipoEstadoInscripcion.DEFINITIVO.getId());
        inscripcionDTO.setEstado(definitivo);

        inscripcionDTO.setFecha(new Date());
        inscripcionDTO.setGratuita(Boolean.TRUE);
        inscripcionDTO.setObservaciones("RENOVACION COVID19");

        PersonaUjiDTO personaUjiDTO = new PersonaUjiDTO();
        personaUjiDTO.setId(connectedUserId);
        inscripcionDTO.setPersonaUji(personaUjiDTO);

        OfertaDTO ofertaDTO = new OfertaDTO();
        ofertaDTO.setId(ofertaId);
        inscripcionDTO.setOferta(ofertaDTO);

        inscripcionDTO.setOrigen("COVID19");
        inscripcionDTO.setNumeroInscripcion(1L);
        inscripcionDAO.insert(inscripcionDTO);
    }

    public Boolean getInscripcionUsuario(Long connectedUserId, Long ofertaId) {
        return inscripcionDAO.getInscripcionUsuario(connectedUserId, ofertaId);
    }

    public void altaDevolucionUsuarioCovid(Long connectedUserId, Long ofertaId, String iban) {

        DevolucionCovidDTO devolucionCovidDTO = new DevolucionCovidDTO();
        devolucionCovidDTO.setIBAN(iban);

        OfertaDTO ofertaDTO = new OfertaDTO();
        ofertaDTO.setId(ofertaId);
        devolucionCovidDTO.setOferta(ofertaDTO);

        PersonaDTO personaDTO = new PersonaDTO();
        personaDTO.setId(connectedUserId);
        devolucionCovidDTO.setPersona(personaDTO);

        devolucionCovidDAO.insert(devolucionCovidDTO);

    }

    public Boolean getDevolucionUsuario(Long connectedUserId, Long ofertaId) {
        return devolucionCovidDAO.getDevolucionUsuario(connectedUserId, ofertaId);
    }

    public List<InscripcionCombo> getInscripcionesUsuarioCurso(Long persona, Long cursoAca) {
        return inscripcionDAO.getInscripcionesUsuarioCurso(persona, cursoAca);
    }

    public List<InscripcionUsuario> getInscripcionesUsuarioByCurso(Long persona, Long cursoAca) {
        return inscripcionDAO.getInscripcionesUsuarioByCurso(persona, cursoAca);
    }

    public Long getCuenta(Long inscripcionId, Long personaId) {
        return inscripcionDAO.getCuenta(inscripcionId, personaId);
    }

    public List<GrupoInscripcion> getGruposDisponibles(Long inscripcionId) {
        return inscripcionDAO.getGruposDisponibles(inscripcionId).stream().map(grupoInscripcion -> {
            Long plazas = Long.valueOf(inscripcionPlazasLibresDAO.plazasLibresOferta(grupoInscripcion.getId()));
            grupoInscripcion.setVacantes(plazas);
            return grupoInscripcion;
        }).collect(Collectors.toList());
    }

    public void modificaGrupoInscripcion(Long inscripcionId, Long ofertaId) {
        inscripcionDAO.modificaGrupoInscripcion(inscripcionId, ofertaId);
    }

    public List<VWActividadesAsistenciaDTO> getActividadesAsistencia(Long inscripcionId, Long ofertaId, String fecha, String fechaBaja) {
        return inscripcionDAO.getActividadesAsistencia(inscripcionId, ofertaId, fecha, fechaBaja);
    }

    public String getFechaBaja(Long inscripcionId) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").format(inscripcionDAO.getFechaBaja(inscripcionId));
        } catch (Exception e) {
            return null;
        }
    }

    public Long getCadu(Long inscripcionId, Long personaId) {
        return inscripcionDAO.getCadu(inscripcionId, personaId);
    }

    public Long getDiasAsis(Long inscripcionId) {
        return inscripcionDAO.getDiasAsis(inscripcionId);
    }

    public Long getCaduDiasTotal(Long ofertaId) {
        return inscripcionDAO.getCaduDiasTotal(ofertaId);
    }

    public Long getDiasTotal(Long ofertaId) {
        return inscripcionDAO.getDiasTotal(ofertaId);
    }

    public Long getDiasActiv(Long ofertaId, String fecha, String fechaBaja) throws ParseException {
        return inscripcionDAO.getDiasActiv(ofertaId, fecha, fechaBaja);
    }

    public void addDeleteActividadAsistencia(UIEntity entity) {
        ActividadAsistencia actividadAsistencia = entity.toModel(ActividadAsistencia.class);
        try {
            inscripcionDAO.addActividadAsistencia(actividadAsistencia);
        } catch (Exception e) {
            inscripcionDAO.deleteActividadAsistencia(actividadAsistencia.getAsistenciaId());
        }
    }

    public void deleteInscripcion(UIEntity entity) throws CommonException {
        try {
            InscripcionUsuario inscripcion = entity.toModel(InscripcionUsuario.class);
            Long[] reciboYLinea = inscripcionDAO.getReciboYLinea(inscripcion.getPersonaId(), inscripcion.getId());
            Long reciboId = reciboYLinea[0], lineaId = reciboYLinea[1];

            if (reciboId != null) {
                if (!inscripcionDAO.isReciboBloqueado(reciboId)) {
                    inscripcionDAO.deleteRecibo(reciboId);
                    inscripcionDAO.deleteInscripcion(inscripcion.getId());
                } else {
                    throw new CommonException("No es pot esborrar aquesta inscripció perquè té rebuts emesos");
                }
            } else {
                Long capitan = inscripcionDAO.getCapitanByInscripcionId(inscripcion.getId());
                if (capitan != null && capitan > 0L)
                    throw new CommonException("No es pot esborrar aquesta inscripció perquè l'usuari es capità d'un equip");
                else {
                    Long asistencias = inscripcionDAO.getAsistenciasByInscripcionId(inscripcion.getId());
                    if (asistencias > 0)
                        throw new CommonException("No es pot esborrar aquesta inscripció perquè l'usuari ha asistit a l'activitat");
                    else {
                        inscripcionDAO.deleteCompeticionesMiembrosFromInscripcion(inscripcion.getId());
                        inscripcionDAO.deleteLineasRecibo(lineaId);
                        inscripcionDAO.deleteInscripcion(inscripcion.getId());

                        Long[] inscripcionesYPlazas = inscripcionDAO.getInscripcionesYPlazas(inscripcion.getOfertaId());
                        Long inscripciones = inscripcionesYPlazas[0], plazas = inscripcionesYPlazas[1];
                        if (plazas > inscripciones) {
                            if (inscripcionDAO.getPlazaEspera(inscripcion.getOfertaId()) != null) {
                                try {
                                    InscripcionEsperaDTO inscripcionEsperaDTO = inscripcionDAO.getSiguienteEnEspera(inscripcion.getOfertaId());
                                    actualizaListaDeEspera(inscripcionEsperaDTO);
                                    PersonaDTO personaDTO = personaDAO.getPersonaById(inscripcionEsperaDTO.getPersonaId());
                                    if (personaDTO.isRecibirCorreo()) {
                                        String[] actividadYGrupo = inscripcionDAO.getNombreYGrupoByOfertaId(inscripcionEsperaDTO.getOfertaId());
                                        enviaCorreoNotificacion(inscripcionEsperaDTO.getPersonaId(), actividadYGrupo[0], actividadYGrupo[1]);
                                    }
                                } catch (Exception e) {
                                    log.error("Inscripció esborrada, però amb error actualitzant la llista d'espera", e);
                                    throw new CommonException("Inscripció esborrada, però amb error actualitzant la llista d'espera");
                                }
                            }
                        }
                    }
                }
            }
            inscripcionDAO.deleteInscripcionesExtras(inscripcionDAO.getInscripcionesOfertasExtra(inscripcion.getOfertaId()), inscripcion.getId());
        } catch (CommonException e) {
            throw new CommonException(e.getMessage());
        } catch (Exception e) {
            log.error("Error al esborrar la insripció", e);
            throw new CommonException("Error al esborrar la insripció");
        }
    }

    private void actualizaListaDeEspera(InscripcionEsperaDTO inscripcionEsperaDTO) throws CommonException {
        try {
            inscripcionDAO.actualizaEstadoInscripcionAManual(inscripcionEsperaDTO);
            PersonaDTO persona = new PersonaDTO();
            persona.setId(inscripcionEsperaDTO.getPersonaId());
            persona.setPersonaUji(new PersonaUjiDTO(inscripcionEsperaDTO.getPersonaId()));
            if (personaDAO.getPersonaById(inscripcionEsperaDTO.getPersonaId()) == null) personaDAO.insert(persona);
            Boolean estaInscrito = false;
            if (!inscripcionDAO.permiteDuplicados(inscripcionEsperaDTO.getOfertaId()))
                estaInscrito = getInscripcionUsuario(inscripcionEsperaDTO.getPersonaId(), inscripcionEsperaDTO.getOfertaId());
            if (!estaInscrito) {
                InscripcionDTO inscripcion = new InscripcionDTO();
                inscripcion.setOferta(ofertaDAO.getOfertaById(inscripcionEsperaDTO.getOfertaId()));
                inscripcion.setPersonaUji(new PersonaUjiDTO(inscripcionEsperaDTO.getPersonaId()));
                inscripcion.setFecha(new Date());
                inscripcion.setEstado(new TipoDTO(TipoEstadoInscripcion.PENDIENTE.getId(), TipoEstadoInscripcion.PENDIENTE.getNombre()));
                inscripcion.setNumeroInscripcion(1L);
                inscripcion.setApto(ofertaDAO.getOfertaById(inscripcionEsperaDTO.getOfertaId()).getValorDefectoApto());
                inscripcion.setOrigen(inscripcionEsperaDTO.getOrigen());
                inscripcionDAO.insert(inscripcion);
                bonificacionDAO.addBonificaciones(inscripcionDAO.getBonificacionesAuto(inscripcionEsperaDTO.getOfertaId()), inscripcionEsperaDTO.getPersonaId());
            } else if (inscripcionEsperaDTO.getPosEspera() == null) {
                throw new CommonException("Aquest usuari ja està inscrit en aquesta oferta i no pot tindre duplicats");
            } else {
                throw new CommonException("Aquest usuari ja està inscrit i està en espera en aquesta oferta.");
            }
        } catch (Exception e) {
            throw new CommonException("Error actualitzant la llista d'espera");
        }
    }

    private void enviaCorreoNotificacion(Long personaId, String actividad, String grupo) {
        String asunto = "Llista d'espera - Servei d'Esports";
        String cuerpo = "Et comuniquem que has estat inscrit o inscrita provisionalment a l'activitat " + actividad + " al grup " + grupo + ".\n\n" +
                "Cal que accedisques a l'adreça següent per tal d'acceptar o rebutjar la inscripció.\n\n" +
                "El termini màxim per a acceptar la inscripció és de 3 dies, si no has confirmat és donarà de baixa.\n\n\n" +
                "Quan t'inscrius en una activitat queda constància que estàs d'acord amb els termes d'ús dels espais esportius " +
                "així com amb el Reglament del Servei d'Esports de la UJI.\n\n\n https://e-ujier.uji.es/pls/www/!uji_sports.usuari";
        enviaNotificacion.enviaNotificacion(personaId, asunto, cuerpo, null, null, new Date(), null);
    }

    public List<PlazaInscripcion> getPlazasInscripciones(Long personaId, Long periodoId, Long actividadId) throws ParseException {
        String valorParametroGlobal = parametroDAO.getParametroGlobal("COMPETICIONES-EXTERNAS").getValor();
        Long tipoCompeticionExterna = tipoDAO.getPadreEtiqueta(452802L, valorParametroGlobal);
        Boolean tieneSancion = personaSancionService.tieneSancion(personaId);
        return inscripcionDAO.getPlazasInscripciones(personaId, periodoId, actividadId, tipoCompeticionExterna, tieneSancion);
    }

    public List<HistoricoInscripcion> getHistoricoInscripciones(Paginacion paginacion, Long personaId, Long cursoAcademico) {
        return inscripcionDAO.getHistoricoInscripciones(paginacion, personaId, cursoAcademico);
    }
}
