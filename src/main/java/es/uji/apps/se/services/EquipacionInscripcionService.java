package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionesInscripcionesDAO;
import es.uji.apps.se.dto.EquipacionesInscripcionesDTO;
import es.uji.apps.se.exceptions.EquipacionInscripcionException;
import es.uji.apps.se.model.EquipacionInscripcion;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sun.jersey.api.core.InjectParam;

import java.time.Instant;
import java.util.Date;

@Service
public class EquipacionInscripcionService extends CoreBaseService {
    @Autowired
    EquipacionesInscripcionesDAO equipacionesInscripcionesDAO;

    @InjectParam
    EquipacionesStockService equipacionesStockService;

    public void addEquipacionesInscripciones(UIEntity entity) throws EquipacionInscripcionException {
        try {
            EquipacionInscripcion equipacionInscripcion = entity.toModel(EquipacionInscripcion.class);
            Long dorsal;
            try {
                dorsal = Long.parseLong(equipacionInscripcion.getDorsal());
            } catch (Exception e) {
                dorsal = null;
                equipacionInscripcion.setDorsal(null);
            }

            if (equipacionInscripcion.getTalla().equals("Talla única"))
                equipacionInscripcion.setTalla(null);

            Long stock = equipacionesStockService.getStockDeEquipacion(equipacionInscripcion.getEquipacionId(), equipacionInscripcion.getTalla(), dorsal);
            if (stock < 0)
                throw new EquipacionInscripcionException("No hi ha stock per a aquesta equipació");
            equipacionInscripcion.setFecha(new Date());
            if (ParamUtils.isNotNull(entity.get("fechaDevolucion")))
                equipacionInscripcion.setFechaDev(Date.from(Instant.parse(entity.get("fechaDevolucion"))));
            equipacionesInscripcionesDAO.insert(new EquipacionesInscripcionesDTO(equipacionInscripcion));
        } catch (EquipacionInscripcionException e) {
            throw new EquipacionInscripcionException(e.getMessage());
        } catch (Exception e) {
            throw new EquipacionInscripcionException("S'ha produït un error");
        }
    }

    public void updateEquipacionesInscripciones(UIEntity entity) throws EquipacionInscripcionException {
        try {
            EquipacionInscripcion equipacionInscripcion = entity.toModel(EquipacionInscripcion.class);
            equipacionInscripcion.setFecha(new Date());
            if (ParamUtils.isNotNull(entity.get("fechaDevolucion")))
                equipacionInscripcion.setFechaDev(Date.from(Instant.parse(entity.get("fechaDevolucion"))));

            try {
                Long.parseLong(equipacionInscripcion.getDorsal());
            } catch (Exception e) {
                equipacionInscripcion.setDorsal(null);
            }
            if (equipacionInscripcion.getTalla().equals("Talla única"))
                equipacionInscripcion.setTalla(null);

            equipacionesInscripcionesDAO.updateEquipacionesInscripciones(equipacionInscripcion);
        } catch (Exception e) {
            throw new EquipacionInscripcionException(e.getCause().getCause().getMessage());
        }
    }

    public void deleteEquipacionesInscripciones(Long id) throws EquipacionInscripcionException {
        try {
            equipacionesInscripcionesDAO.deleteEquipacionesInscripciones(id);
        } catch (Exception e) {
            throw new EquipacionInscripcionException(e.getCause().getCause().getMessage());
        }
    }
}
