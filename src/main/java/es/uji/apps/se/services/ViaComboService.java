package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.ViaComboDAO;
import es.uji.apps.se.model.ProvinciaCombo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ViaComboService {

    @InjectParam
    private ViaComboDAO viaComboDAO;

    public List<ProvinciaCombo> getViaCombo() {
        return viaComboDAO.getViaCombo();
    }

}