package es.uji.apps.se.services;

import es.uji.apps.se.model.CompeticionMiembro;
import es.uji.apps.se.model.CompeticionPartido;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.web.template.Template;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ActaCompeticionService {

    private final CompeticionPartidoService competicionPartidoService;
    private final InstalacionService instalacionService;
    private final MiembrosPuntosService miembrosPuntosService;
    private final CompeticionMiembroService competicionMiembroService;
    private final ActividadAsistenciaService actividadAsistenciaService;
    private final CompeticionSancionService competicionSancionService;

    @Autowired
    public ActaCompeticionService(CompeticionPartidoService competicionPartidoService,
                                  InstalacionService instalacionService,
                                  MiembrosPuntosService miembrosPuntosService,
                                  CompeticionMiembroService competicionMiembroService,
                                  ActividadAsistenciaService actividadAsistenciaService,
                                  CompeticionSancionService competicionSancionService) {
        this.competicionPartidoService = competicionPartidoService;
        this.instalacionService = instalacionService;
        this.miembrosPuntosService = miembrosPuntosService;
        this.competicionMiembroService = competicionMiembroService;
        this.actividadAsistenciaService = actividadAsistenciaService;
        this.competicionSancionService = competicionSancionService;
    }

    public Template getActa(Long competicionId, String idioma) throws ParseException {

        Template template = AppInfo.buildPaginaActa("se/acta/base", idioma);
        template.put("contenido", "se/acta/acta");

        CompeticionPartido competicionPartido = competicionPartidoService.getCompeticionPartidoById(competicionId);
        CompeticionPartido competicionPartidoEntero = competicionPartidoService.getCompeticionPartidoByIdEntero(competicionId);
        Long golesTotalesLocal = competicionPartidoEntero.getResultadoCasa();
        Long golesTotalesFuera = competicionPartidoEntero.getResultadoFuera();
        String valorComentarios = competicionPartidoEntero.getComentarios();
        String instalacion = instalacionService.getInstalacionPartido(competicionId);

        Long golesFantasmaLocal = miembrosPuntosService.getMiembrosFantasmaPuntos(1L, competicionId);
        Long golesFantasmaFuera = miembrosPuntosService.getMiembrosFantasmaPuntos(2L, competicionId);

        List<CompeticionMiembro> equipoCasa = competicionMiembroService.getCompeticionMiembrosByEquipoId(competicionPartido.getEquipoCasa(), competicionId);
        List<CompeticionMiembro> equipoFuera = competicionMiembroService.getCompeticionMiembrosByEquipoId(competicionPartido.getEquipoFuera(), competicionId);

        List<Long> equipoCasaGoles = new ArrayList<Long>();
        List<Long> equipoFueraGoles = new ArrayList<Long>();
        List<Boolean> equipoCasaExluidos = new ArrayList<Boolean>();
        List<Boolean> equipoFueraExluidos = new ArrayList<Boolean>();

        Map<Long, String> diccionarioTarjetas = competicionSancionService.getCompeticionSancion(competicionId);

        recogeDatosEquipo(competicionId, equipoCasa, equipoCasaGoles, equipoCasaExluidos);
        recogeDatosEquipo(competicionId, equipoFuera, equipoFueraGoles, equipoFueraExluidos);

        template.put("equipoCasa", equipoCasa);
        template.put("equipoFuera", equipoFuera);
        template.put("partido", competicionId);
        template.put("defectoLocal", golesTotalesLocal);
        template.put("listaLocal", equipoCasaGoles);
        template.put("excluidosLocal", equipoCasaExluidos);
        template.put("defectoFuera", golesTotalesFuera);
        template.put("listaFuera", equipoFueraGoles);
        template.put("excluidosFuera", equipoFueraExluidos);
        template.put("diccionarioTarjetas", diccionarioTarjetas);
        template.put("golesFantasmaLocal", golesFantasmaLocal);
        template.put("golesFantasmaFuera", golesFantasmaFuera);
        template.put("valorComentarios", valorComentarios);
        template.put("campoPartido", instalacion);

        template.put("footer", "se/acta/footer");

        return template;
    }

    private void recogeDatosEquipo(Long competicionId, List<CompeticionMiembro> equipoMiembros, List<Long> equipoGoles, List<Boolean> equipoExluidos) {
        for (CompeticionMiembro miembrolocal : equipoMiembros) {
            Long goleslocal = miembrosPuntosService.getMiembrosPuntos(miembrolocal.getId(), competicionId);
            equipoGoles.add(goleslocal != null ? goleslocal : 0L);

            boolean sancionLocal = competicionSancionService.getEstaSancionado(miembrolocal.getId(), competicionId);
            equipoExluidos.add(sancionLocal);
        }
    }


    public ResponseMessage insertaAsistenciaByInscripcionYPartido(Long inscripcionId, Long partidoId, String tipo) {
        actividadAsistenciaService.insertaAsistenciaByInscripcionYPartido(inscripcionId, partidoId, tipo);

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);
        responseMessage.setMessage("Asistencia insertada");
//        responseMessage.setData(Collections.singletonList(entity));

        return responseMessage;
    }

    public Response updateMiembrosPuntos(Long inscripcionId, Long partidoId, Long goles) {
        miembrosPuntosService.updateMiembrosPuntos(inscripcionId, partidoId, goles);

        return Response.ok().build();
    }

    public Response updateMiembrosFantasmaPuntos (Long equipoId, Long partidoId, Long goles) {
        miembrosPuntosService.updateMiembrosFantasmaPuntos(equipoId, partidoId, goles);
        return Response.ok().build();
    }

    public Response insertaGolesTotales(Long equipoId, Long partidoId, String tipo) throws JSONException {

        Long nuevoTotal = miembrosPuntosService.updateGolesTotales(equipoId, partidoId, tipo);

        JSONObject data = new JSONObject();
        data.put("nuevoTotal", nuevoTotal);
        data.put("equipoId", equipoId);

        return Response.ok(data.toString()).build();
    }

    public Response insertaSancion (Long inscripcionId, Long partidoId, String sancion){

        if (sancion != null)
            competicionSancionService.updateCompeticionSancion(inscripcionId, partidoId, sancion);
        else
            competicionSancionService.deleteCompeticionSancion(inscripcionId, partidoId);

        return Response.ok().build();
    }

    public Response actualizaDatos (Long partidoId, String observaciones){

        competicionPartidoService.updateObservacionesPartido(partidoId, observaciones);
        return Response.ok().build();
    }
}
