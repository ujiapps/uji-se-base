package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionTipoMovimentoDAO;
import es.uji.apps.se.dto.EquipacionTipoMovimientoDTO;
import es.uji.apps.se.model.EquipacionTipoMovimiento;
import es.uji.commons.rest.CoreBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EquipacionTipoMovimientoService extends CoreBaseService {

    @Autowired
    EquipacionTipoMovimentoDAO equipacionTipoMovimentoDAO;

    public List<EquipacionTipoMovimiento> getTiposMovimiento() {
        return equipacionTipoMovimentoDAO.getTiposMovimiento();
    }

    @Transactional
    public EquipacionTipoMovimiento addTipoMovimiento(EquipacionTipoMovimiento tipoMovimiento) {
        return new EquipacionTipoMovimiento(equipacionTipoMovimentoDAO.insert(new EquipacionTipoMovimientoDTO(tipoMovimiento)));
    }

    public void deleteTipoMovimiento(Long id) {
        equipacionTipoMovimentoDAO.deleteTipoMovimiento(id);
    }

    public void updateTipoMovimiento(EquipacionTipoMovimiento tipoMovimiento) {
        equipacionTipoMovimentoDAO.updateTipoMovimiento(tipoMovimiento);
    }

    public Boolean isAltaOrDevolucion(Long equipacionTipoMovimientoId) {
        return equipacionTipoMovimentoDAO.isAltaOrDevolucion(equipacionTipoMovimientoId);
    }
}
