package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.Bono;
import es.uji.apps.se.services.CarnetBonoTipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import java.util.List;

@Path("carnetbonotipo")
public class CarnetBonoTipoResource extends CoreBaseService {

    @InjectParam
    CarnetBonoTipoService carnetBonoTipoService;

    @GET
    public List<UIEntity> getCarnetBonoTipos() {
        return UIEntity.toUI(carnetBonoTipoService.getCarnetBonoTipos());
    }

    @GET
    @Path("{personaId}")
    public ResponseMessage getUsuarioTarjetas(@PathParam("personaId") Long personaId){
        ResponseMessage responseMessage = new ResponseMessage(false);
        try {
            List<Bono> tarjetas = carnetBonoTipoService.getUsuarioTarjetas();
            responseMessage.setData(UIEntity.toUI(tarjetas));
            responseMessage.setSuccess(true);
            return responseMessage;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
