package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.TarjetaDeportivaClaseDAO;
import es.uji.apps.se.dto.TarjetaDeportivaClaseDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.utils.Csv;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.Role;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TarjetaDeportivaClaseService {

    @InjectParam
    private TarjetaDeportivaClaseDAO tarjetaDeportivaClaseDAO;
    @InjectParam
    Csv csv;

    public List<TarjetaDeportivaClaseDTO> getClasesByTarjetaDeportivaId(Long tarjetaDeportivaId) {
        return tarjetaDeportivaClaseDAO.getClasesByTarjetaDeportivaId(tarjetaDeportivaId);
    }

    public void insert(TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO, Long connectedUserId) {
        tarjetaDeportivaClaseDTO.setPersonaAccion(connectedUserId);
        tarjetaDeportivaClaseDAO.insert(tarjetaDeportivaClaseDTO);
    }

    public TarjetaDeportivaClaseDTO update(TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO, Long connectedUserId) {
        tarjetaDeportivaClaseDTO.setPersonaAccion(connectedUserId);
        return tarjetaDeportivaClaseDAO.update(tarjetaDeportivaClaseDTO);
    }

    public void delete(Long tarjetaDeportivaClaseId) {
        tarjetaDeportivaClaseDAO.delete(TarjetaDeportivaClaseDTO.class, tarjetaDeportivaClaseId);
    }

    public TarjetaDeportivaClaseDTO getTarjetaDeportivaClaseById(Long tarjetaDeportivaClaseId) {
        return tarjetaDeportivaClaseDAO.getClaseById(tarjetaDeportivaClaseId);
    }

    public void deleteTarjetaDeportivaClasesByCalendarioId(Long calendarioClaseId) {
        List<TarjetaDeportivaClaseDTO> lista = getTarjetaDeportivaClasesByCalendarioClaseId(calendarioClaseId);
        for (TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO : lista) {
            delete(tarjetaDeportivaClaseDTO.getId());
        }
    }

    private List<TarjetaDeportivaClaseDTO> getTarjetaDeportivaClasesByCalendarioClaseId(Long calendarioClaseId) {
        return tarjetaDeportivaClaseDAO.getTarjetaDeportivaClasesByCalendarioClaseId(calendarioClaseId);
    }

    public List<TarjetaDeportivaClaseDTO> getClasesByCalendarioId(Long calendarioId, Paginacion paginacion) {
        return tarjetaDeportivaClaseDAO.getClasesByCalendarioId(calendarioId, paginacion);
    }


    public List<TarjetaDeportivaClaseDTO> getClasesByCalendarioId(Long calendarioId) {
        return tarjetaDeportivaClaseDAO.getClasesByCalendarioId(calendarioId);
    }

    public Response getReservasTarjetasCSV(Long calendarioId) {
        List<TarjetaDeportivaClaseDTO> list = getClasesByCalendarioId(calendarioId);

        Response.ResponseBuilder res = Response.ok();

        res.header("Content-Disposition", "attachment; filename = reservasClase.csv");

        res.entity(entityTarjetaDeportivaClaseToCSV(list));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    private String entityTarjetaDeportivaClaseToCSV(List<TarjetaDeportivaClaseDTO> tarjetaDeportivaClasesDTO) {

        List<List<String>> records = new ArrayList<>();
        ArrayList<String> cabeceras = new ArrayList<>(Arrays.asList("Nombre", "Identificacion", "email", "movil", "fecha Nacimiento", "vinculo", "asiste"));

        for (TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO : tarjetaDeportivaClasesDTO) {
            List<String> recordMov = new ArrayList<>(creaListaString(tarjetaDeportivaClaseDTO));
            records.add(recordMov);
        }
        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO) {
        return Arrays.asList(tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getApellidosNombre(),
                tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getIdentificacion(),
                ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaSE().getMail()) ? tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaSE().getMail() : tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getMail(),
                ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaSE().getMovil()) ? tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaSE().getMovil() : tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getMovil(),
                ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getFechaNacimiento()) ? tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getFechaNacimiento().toString() : "",
                ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getPersonaVinculo()) ? tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getPersonaVinculo().getVinculo().getNombre() : "SENSE VINCLE",
                tarjetaDeportivaClaseDTO.isAsiste() ? "SI" : "NO");
    }

    public TarjetaDeportivaClaseDTO updateAsistencia(Long tarjetaDeportivaClaseId, Long connectedUserId) {
        TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO = getTarjetaDeportivaClaseById(tarjetaDeportivaClaseId);
        tarjetaDeportivaClaseDTO.setAsiste(!tarjetaDeportivaClaseDTO.isAsiste());
        tarjetaDeportivaClaseDTO.setPersonaAccion(connectedUserId);
        return update(tarjetaDeportivaClaseDTO,connectedUserId);
    }
}
