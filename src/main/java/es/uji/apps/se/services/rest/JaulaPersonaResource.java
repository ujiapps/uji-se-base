package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.JaulaPersonaVistaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.filtros.FiltroJaula;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.apps.se.services.JaulaPersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("jaulapersona")
public class JaulaPersonaResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(JaulaPersonaResource.class);

    @InjectParam
    JaulaPersonaService jaulaPersonaService;

    @GET
    public ResponseMessage getJaulasPersonas(@QueryParam("query") String query,
                                             @QueryParam("fechaInicioDesde") String fechaInicioDesdeParam,
                                             @QueryParam("fechaFinDesde") String fechaFinDesdeParam,
                                             @QueryParam("fechaLlaveroDesde") String fechaLlaveroDesdeParam,
                                             @QueryParam("fechaInicioHasta") String fechaInicioHastaParam,
                                             @QueryParam("fechaFinHasta") String fechaFinHastaParam,
                                             @QueryParam("fechaLlaveroHasta") String fechaLlaveroHastaParam,
                                             @QueryParam("start") @DefaultValue("0") Long start,
                                             @QueryParam("limit") @DefaultValue("25") Long limit,
                                             @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);
            FiltroJaula filtro = new FiltroJaula(query, new UIDia(fechaInicioDesdeParam).getFecha(),
                    new UIDia(fechaFinDesdeParam).getFecha(), new UIDia(fechaInicioHastaParam).getFecha(),
                    new UIDia(fechaFinHastaParam).getFecha(),
                    new UIDia(fechaLlaveroDesdeParam).getFecha(),
                    new UIDia(fechaLlaveroHastaParam).getFecha());


            List<JaulaPersonaVistaDTO> jaulaPersonas = jaulaPersonaService.getJaulasPersonas(paginacion, filtro);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((toUI(jaulaPersonas)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getJaulasPersonas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    private List<UIEntity> toUI(List<JaulaPersonaVistaDTO> jaulaPersonas) {

        return jaulaPersonas.stream()
                .map(jaulaPersona -> {
                    UIEntity uiEntity = UIEntity.toUI(jaulaPersona);
                    uiEntity.put("vinculoNombre", jaulaPersona.getPersonaUji().getPersonaVinculo().getVinculo().getNombre());
                    return uiEntity;
                }).collect(Collectors.toList());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertJaulaPersona(UIEntity entity) {
        return UIEntity.toUI(jaulaPersonaService.insertJaulaPersona(entity));
    }

    @PUT
    @Path("{jaulaPersonaId}")
    public UIEntity updateJaulaPersona(UIEntity entity) {
        return UIEntity.toUI(jaulaPersonaService.updateJaulaPersona(entity));
    }

    @DELETE
    @Path("{jaulaPersonaId}")
    public void deleteJaulaPersona(@PathParam("jaulaPersonaId") Long jaulaPersonaId) {
        jaulaPersonaService.deleteJaulaPersona(jaulaPersonaId);
    }
}
