package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.BicicletasListaEsperaDTO;
import es.uji.apps.se.dto.PersonaDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.BicicletaListaEspera;
import es.uji.apps.se.model.BicicletasDisponiblesTexto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;

@Service
public class BicicletasEsperaUsuarioService {

    private final BicicletasEsperaUsuarioDAO bicicletasEsperaUsuarioDAO;
    private final VinculacionesDAO vinculacionesDAO;
    private final BicicletasGestionDAO bicicletasGestionDAO;
    private final PersonaDAO personaDAO;
    private final PersonaService personaService;
    private final EnviaNotificacion enviaNotificacion;

    @Autowired
    public BicicletasEsperaUsuarioService(BicicletasEsperaUsuarioDAO bicicletasEsperaUsuarioDAO,
                                          VinculacionesDAO vinculacionesDAO,
                                          BicicletasGestionDAO bicicletasGestionDAO,
                                          PersonaDAO personaDAO,
                                          PersonaService personaService,
                                          EnviaNotificacion enviaNotificacion) {
        this.bicicletasEsperaUsuarioDAO = bicicletasEsperaUsuarioDAO;
        this.vinculacionesDAO = vinculacionesDAO;
        this.bicicletasGestionDAO = bicicletasGestionDAO;
        this.personaDAO = personaDAO;
        this.personaService = personaService;
        this.enviaNotificacion = enviaNotificacion;
    }

    public BicicletaListaEspera getEsperaUsuario(Long perId, Long cursoAca) throws ParseException {
        Long vinculacion = vinculacionesDAO.dameVinculoPersona(perId, "TARIFAS-ACTIVIDADES");
        String puedeReservas = personaService.puedeReservar(perId, vinculacion, cursoAca);
        BicicletaListaEspera listaEspera = new BicicletaListaEspera();

        try {
            listaEspera = bicicletasEsperaUsuarioDAO.getBicicletasListaEspera(perId);
            if (puedeReservas.charAt(0) == 'V') {
                listaEspera.setAccion("V");
                listaEspera.setComentario("L'usuari a pesar de tener una reserva feta ja no disposa de vinculació que l'acredite");
            } else if (puedeReservas.charAt(0) == 'S') {
                listaEspera.setAccion("S");
                BicicletasDisponiblesTexto disponiblesTexto = bicicletasGestionDAO.getBicicletasDisponiblesTexto();
                listaEspera.setComentario("L'usuari es troba en la posició " + listaEspera.getPosicionLista() + " i hi ha " + disponiblesTexto.getNumBicis() + " " + disponiblesTexto.getTexto() + ".");
                listaEspera.setNumBicis(disponiblesTexto.getNumBicis());
            } else {
                listaEspera.setAccion("N");
                listaEspera.setComentario(puedeReservas.substring(2));
            }
        } catch (Exception e) {
            if (puedeReservas.charAt(0) == 'V') {
                listaEspera.setAccion("V");
                listaEspera.setComentario(puedeReservas.substring(2));
            } else if (puedeReservas.charAt(0) == 'S') {
                listaEspera.setAccion("ExS");
                Long numReservasActivas = bicicletasGestionDAO.getNumeroReservasActivas(perId);
                listaEspera.setNumReservasActivas(numReservasActivas);
                if (numReservasActivas == 0L) {
                    listaEspera.setComentario("L'usuari no disposa de cap sol·licitud de bicicleta");
                } else {
                    listaEspera.setComentario("Aquest usuari disposa d'una reserva activa");
                }
            } else {
                listaEspera.setAccion("N");
                listaEspera.setComentario(puedeReservas.substring(2));
            }
        }
        return listaEspera;
    }

    public void asignarBicicletasEspera(Long reservaEspera, Long personaId) throws CommonException {
        PersonaDTO persona = personaDAO.getPersonaById(personaId);
//        Boolean puedeRecibirCorreo = personaDAO.puedeRecibirCorreo(personaId);
        try {
            if (reservaEspera == null) {
                bicicletasEsperaUsuarioDAO.asignarBicicletasEspera();
            } else {
                bicicletasEsperaUsuarioDAO.asignarBicicletasEsperaIfNotNull(reservaEspera);
                if (persona.isRecibirCorreo()) {
                    String cuerpo = "T''hem assignat una bicicleta. Tens un termini de 72 hores per arreplegar-la, " +
                            "superat aquest termini el Servei d'Esports podrà assignar-la a un altre usuari que es trobe " +
                            "en la llista d''espera. \n\n" +
                            "Se te ha asignado una bicicleta, tienes un plazo de 72 horas para pasar a recogerla, " +
                            "superado este plazo el Servicio de Deportes podrá asignarla a otro usuario que se encuentre en " +
                            "la lista de espera.";
                    enviaNotificacion.enviaNotificacion(personaId, "Reserves Bicicletes - Servei d''Esports",
                            cuerpo, null, null, new Date(), "N");
                }
            }
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }

    public void ponerListaEspera(Long perId) throws CommonException {
        PersonaDTO persona = personaDAO.getPersonaById(perId);
        try {
            BicicletasListaEsperaDTO listaEsperaDTO = new BicicletasListaEsperaDTO();
            listaEsperaDTO.setPerId(perId);
            listaEsperaDTO.setFecha(new Date());
            listaEsperaDTO.setTipoUso(9560316L);
            bicicletasEsperaUsuarioDAO.insert(listaEsperaDTO);
            if (persona.isRecibirCorreo()) {
                String cuerpo = "S'ha realitzat la teua inscripció en la llista d'espera. Quan t'assignem una bicicleta, rebràs un correu electrònic informant-te que pots arreplegar-la al Servei d'Esports.\n\n" +
                        "Has sido inscrito en la lista de espera, cuando te sea asignada una bicicleta recibirás un correo electrónico informándote que puedes pasar a recogerla por el Servicio de Deportes.";
                enviaNotificacion.enviaNotificacion(perId, "Reserves Bicicletes - Servei d'Esports",
                        cuerpo, null, null, new Date(), "N");
            }
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
    }
}
