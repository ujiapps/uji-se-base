package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionesStockDAO;
import es.uji.apps.se.model.EquipacionesStock;
import es.uji.apps.se.model.Paginacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EquipacionesStockService {

    @Autowired
    EquipacionesStockDAO equipacionesStockDAO;

    public List<EquipacionesStock> getEquipacionesStock(Paginacion paginacion, List<Map<String, String>> filtros) {
        return equipacionesStockDAO.getEquipacionesStock(paginacion, filtros);
    }

    public Long getStockDeEquipacion(Long equipacionId, String talla, Long dorsal) {
        return equipacionesStockDAO.getStockByEquipacionId(equipacionId, talla, dorsal);
    }

    public List<EquipacionesStock> getEquipacionesStockFinsA(List<Map<String, String>> filtros, Date fechaIni, Date fechaFin) {
        return equipacionesStockDAO.getEquipacionesStockFinsA(filtros, fechaIni, fechaFin);
    }
}