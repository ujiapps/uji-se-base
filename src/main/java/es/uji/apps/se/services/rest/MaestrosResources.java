package es.uji.apps.se.services.rest;

import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("maestros")
public class MaestrosResources   extends CoreBaseService {

    @GET
    public UIEntity getMaestros(){
        return new UIEntity();
    }
}
