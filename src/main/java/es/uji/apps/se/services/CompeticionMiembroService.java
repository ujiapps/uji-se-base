package es.uji.apps.se.services;

import es.uji.apps.se.dao.CompeticionMiembroDAO;
import es.uji.apps.se.model.CompeticionMiembro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompeticionMiembroService {

    private CompeticionMiembroDAO competicionMiembroDAO;

    @Autowired
    public CompeticionMiembroService(CompeticionMiembroDAO competicionMiembroDAO) {
        this.competicionMiembroDAO = competicionMiembroDAO;
    }

    public List<CompeticionMiembro> getCompeticionMiembrosByEquipoId(Long equipoId, Long partidoId) {
        return competicionMiembroDAO.getCompeticionMiembrosByEquipoId(equipoId, partidoId);
    }
}
