package es.uji.apps.se.services.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import es.uji.apps.se.dto.CalendarioClaseDTO;
import es.uji.apps.se.dto.CalendarioClaseVistaDTO;
import es.uji.apps.se.exceptions.BorradoClaseDirigidaException;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.se.services.CalendarioClaseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("calendario")
public class CalendarioClaseResource extends CoreBaseService {


    @Path("{id}/monitorcalendario")
    public MonitorCalendarioResource getPlatformItem(
            @InjectParam MonitorCalendarioResource monitorCalendarioResource) {
        return monitorCalendarioResource;
    }

    @Path("{id}/tarjetadeportivaclase")
    public TarjetaDeportivaClaseResource getPlatformItem(
            @InjectParam TarjetaDeportivaClaseResource tarjetaDeportivaClaseResource) {
        return tarjetaDeportivaClaseResource;
    }

    @InjectParam
    CalendarioClaseService calendarioClaseService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCalendariosClase(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate,
                                              @QueryParam("instalacionId") Long instalacionId) throws ParseException {

        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date start = formato.parse(startDate);
        Date end = formato.parse(endDate);

        List<CalendarioClaseDTO> lista = calendarioClaseService.getCalendarioClasesByFechas(start, end, instalacionId);

        return modelToUI(lista);

    }

    @GET
    @Path("fecha")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCalendariosClaseByFecha(@QueryParam("fecha") String fecha, @QueryParam("personaId") Long personaId) throws ParseException {

        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formato.parse(fecha);

        List<CalendarioClaseVistaDTO> lista = calendarioClaseService.getCalendariosClaseByFecha(date, personaId);

        return UIEntity.toUI(lista);

    }

    @GET
    @Path("proximas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProximasClases() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<UIEntity> resultado = new ArrayList<>();
        for (CalendarioClaseVistaDTO calendarioClase : calendarioClaseService.getProximasClases(connectedUserId, 7).stream().filter(CalendarioClaseVistaDTO::quedanPlazasLibres).sorted(
                Comparator.comparing(CalendarioClaseVistaDTO::getFechaHoraInicio)).collect(Collectors.toList())) {
            UIEntity uiEntity = UIEntity.toUI(calendarioClase);
            uiEntity.put("nombre", calendarioClase);
            resultado.add(uiEntity);
        }
        return resultado;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void insertClaseMultiple(@PathParam("id") Long claseId, @FormParam("fechaInicio") String fechaInicio, @FormParam("fechaFin") String fechaFin, @FormParam("horaInicio") String horaInicio,
                                    @FormParam("horaFin") String horaFin, @FormParam("diasFestivos") Long diasFestivos, @FormParam("diasSemana") List<Integer> diasSemana,
                                    @FormParam("instalacion") List<Long> instalacionId) {

        calendarioClaseService.insertClaseMultiple(claseId, fechaInicio, fechaFin, horaInicio, horaFin, diasFestivos, diasSemana, instalacionId);
    }

    @PUT
    @Path("{calendarioClaseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertCalendarioClase(@PathParam("id") Long claseId, @PathParam("calendarioClaseId") Long calendarioClaseId, UIEntity entity) {
        return UIEntity.toUI(calendarioClaseService.updateCalendarioClase(calendarioClaseId, entity));
    }

    @DELETE
    @Path("{calendarioId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteCalendarioClase(@PathParam("calendarioId") Long calendarioClaseId) throws BorradoClaseDirigidaException {
        calendarioClaseService.deleteCalendarioClase(calendarioClaseId);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void deleteClaseMultiple(@PathParam("id") Long claseId, @FormParam("fechaInicio") String fechaInicio, @FormParam("fechaFin") String fechaFin, @FormParam("horaInicio") String horaInicio,
                                    @FormParam("horaFin") String horaFin, @FormParam("diasSemana") List<Integer> diasSemana) throws BorradoClaseDirigidaException {
        calendarioClaseService.deleteClaseMultiple(claseId, fechaInicio, fechaFin, horaInicio, horaFin, diasSemana);
    }


    private UIEntity modelToUI(CalendarioClaseDTO calendarioClaseDTO) {
        UIEntity entity = UIEntity.toUI(calendarioClaseDTO);
        entity.put("fecha", calendarioClaseDTO.getCalendario().getDia());
        entity.put("StartDate", calendarioClaseDTO.getFechaHoraInicio());
        entity.put("EndDate", calendarioClaseDTO.getFechaHoraFin());
        if (!calendarioClaseDTO.getCalendarioClaseInstalacionesDTO().isEmpty())
            entity.put("Name", calendarioClaseDTO.getClase().getNombre() + " (" + calendarioClaseDTO.getInstalacionesNombres() + ")");
        else
            entity.put("Name", calendarioClaseDTO.getClase().getNombre());

        entity.put("ResourceId", calendarioClaseDTO.getClase().getId());
        entity.put("claseId", calendarioClaseDTO.getClase().getId());

        List<String> listaInstalaciones = calendarioClaseDTO.getCalendarioClaseInstalacionesDTO().stream().map(calendarioClaseInstalacionDTO -> {
            return calendarioClaseInstalacionDTO.getInstalacionDTO().getId().toString();
        }).collect(Collectors.toList());

        entity.put("instalacionId", listaInstalaciones);
        return entity;
    }

    private List<UIEntity> modelToUI(List<CalendarioClaseDTO> lista) {
        List<UIEntity> listaUI = new ArrayList<>();
        for (CalendarioClaseDTO calendarioClaseDTO : lista) {
            listaUI.add(modelToUI(calendarioClaseDTO));
        }
        return listaUI;
    }

}
