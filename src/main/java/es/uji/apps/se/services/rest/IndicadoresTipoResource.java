package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.IndicadoresTipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("indicadores-tipo")
public class IndicadoresTipoResource extends CoreBaseService {

    @InjectParam
    IndicadoresTipoService indicadoresTipoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIndicadoresTipo() {
        return UIEntity.toUI(indicadoresTipoService.getIndicadoresTipo());
    }

}
