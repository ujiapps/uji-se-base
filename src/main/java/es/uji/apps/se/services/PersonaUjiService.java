package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.PersonaUjiDAO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import org.springframework.stereotype.Service;

@Service
public class PersonaUjiService {

    @InjectParam
    PersonaUjiDAO personaUjiDAO;

    public PersonaUjiDTO getPersonaById(Long personaId) {

        return personaUjiDAO.getPersonaById(personaId);
    }
}
