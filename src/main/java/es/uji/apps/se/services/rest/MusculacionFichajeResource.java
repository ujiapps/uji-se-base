package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.MonitorMusculacionDTO;
import es.uji.apps.se.dto.MonitorMusculacionFichajeDTO;
import es.uji.apps.se.exceptions.ValidacionException;
import es.uji.apps.se.services.MusculacionFichajeService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("musculacionFichajes")
public class MusculacionFichajeResource extends CoreBaseService {

    @PathParam("monitorMusculacionId")
    Long monitorMusculacionId;

    @InjectParam MusculacionFichajeService musculacionFichajeService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMonitores()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(musculacionFichajeService.getFichajesByMonitorId(monitorMusculacionId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addMonitor(UIEntity entity) throws ValidacionException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        MonitorMusculacionFichajeDTO monitorMusculacionFichajeDTO = entity.toModel(MonitorMusculacionFichajeDTO.class);
        monitorMusculacionFichajeDTO.setFecha(entity.getDate("fecha"));
        monitorMusculacionFichajeDTO.setMonitorMusculacion(new MonitorMusculacionDTO(monitorMusculacionId));
        return UIEntity.toUI(musculacionFichajeService.addFichaje(monitorMusculacionFichajeDTO, connectedUserId));
    }

    @PUT
    @Path("{fichajeId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateMonitor(@PathParam("fichajeId") Long fichajeId, UIEntity entity)
            throws ValidacionException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        MonitorMusculacionFichajeDTO monitorMusculacionFichajeDTO = entity.toModel(MonitorMusculacionFichajeDTO.class);
        monitorMusculacionFichajeDTO.setFecha(entity.getDate("fecha"));
        return UIEntity.toUI(musculacionFichajeService.updateFichaje(monitorMusculacionFichajeDTO, connectedUserId));
    }

    @DELETE
    @Path("{fichajeId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteMonitorMusculacion(@PathParam("fichajeId") Long fichajeId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        musculacionFichajeService.deleteMonitorMusculacion(fichajeId, connectedUserId);
    }

}
