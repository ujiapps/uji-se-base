package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionesPendientesDAO;
import es.uji.apps.se.dto.ListaEquipacionesPendientesItemsVWDTO;
import es.uji.apps.se.model.EquipacionesPendientes;
import es.uji.apps.se.model.Paginacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EquipacionesPendientesService {

    @Autowired
    private EquipacionesPendientesDAO equipacionesPendientesDAO;
    @Autowired
    private ParametroService parametroService;

    @Autowired
    public EquipacionesPendientesService(EquipacionesPendientesDAO equipacionesPendientesDAO){
        this.equipacionesPendientesDAO = equipacionesPendientesDAO;
    }

    public List<EquipacionesPendientes> getPendientesDevolucionEquipacion(Date fechaHasta, Long conectedUserId, Paginacion paginacion) {
        Long cursoAcademico = parametroService.getParametroCursoAcademico(conectedUserId).getId();
        return equipacionesPendientesDAO.getPendientesDevolucionEquipacion(fechaHasta, cursoAcademico, paginacion);
    }

    public List<ListaEquipacionesPendientesItemsVWDTO> getListadoEquipacionesPendientes(Long cursoAca, Date filtroFecha) {
        return equipacionesPendientesDAO.getListadoEquipacionesPendientes(cursoAca, filtroFecha);
    }
}
