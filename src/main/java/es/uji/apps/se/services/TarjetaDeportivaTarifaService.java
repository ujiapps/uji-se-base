package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.TarjetaDeportivaTarifaDAO;
import es.uji.apps.se.dto.TarjetaDeportivaTarifaDTO;
import es.uji.commons.rest.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TarjetaDeportivaTarifaService
{
    @InjectParam
    private TarjetaDeportivaTarifaDAO tarjetaDeportivaTarifaDAO;

    public List<TarjetaDeportivaTarifaDTO> getAllByTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId)
    {
        return tarjetaDeportivaTarifaDAO.getAllByTarjetaDeportivaTipoId(tarjetaDeportivaTipoId);
    }

    @Role("ADMIN")
    public void insert(TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO, Long connectedUserId)
    {
        tarjetaDeportivaTarifaDAO.insert(tarjetaDeportivaTarifaDTO);
    }

    @Role("ADMIN")
    public void delete(Long tarjetaDeportivaTipoId, Long connectedUserId)
    {
        tarjetaDeportivaTarifaDAO.delete(TarjetaDeportivaTarifaDTO.class, tarjetaDeportivaTipoId);
    }

    @Role("ADMIN")
    public void update(TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO, Long connectedUserId)
    {
        tarjetaDeportivaTarifaDAO.update(tarjetaDeportivaTarifaDTO);
    }
}
