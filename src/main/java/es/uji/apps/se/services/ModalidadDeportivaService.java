package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.ModalidadDeportivaDAO;
import es.uji.apps.se.dto.ModalidadDeportivaDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModalidadDeportivaService {
    @InjectParam
    private ModalidadDeportivaDAO modalidadDeportivaDAO;

    public List<ModalidadDeportivaDTO> getModalidadesDeportivas() {
        return modalidadDeportivaDAO.getModalidadesDeportivas();
    }
}
