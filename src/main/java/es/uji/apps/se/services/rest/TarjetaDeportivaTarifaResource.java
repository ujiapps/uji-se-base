package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TarjetaDeportivaTarifaDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.apps.se.services.TarjetaDeportivaTarifaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class TarjetaDeportivaTarifaResource extends CoreBaseService
{
    @InjectParam
    TarjetaDeportivaTarifaService tarjetaDeportivaTarifaService;

    @PathParam("tarjetaDeportivaTipoId")
    Long tarjetaDeportivaTipoId;

    @GET
    public List<UIEntity> getTarjetasDeportivasTarifas()
    {
        return UIEntity.toUI(tarjetaDeportivaTarifaService.getAllByTarjetaDeportivaTipoId(tarjetaDeportivaTipoId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insert(UIEntity entity) {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO = entity.toModel(TarjetaDeportivaTarifaDTO.class);
        tarjetaDeportivaTarifaDTO.setTarjetaDeportivaTipo(new TarjetaDeportivaTipoDTO(tarjetaDeportivaTipoId));
        tarjetaDeportivaTarifaService.insert(tarjetaDeportivaTarifaDTO, conectedUserId);
        return UIEntity.toUI(tarjetaDeportivaTarifaDTO);
    }

    @PUT
    @Path("{tarjetaDeportivaTipoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTarifaId, UIEntity entity) {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO = entity.toModel(TarjetaDeportivaTarifaDTO.class);
        tarjetaDeportivaTarifaService.update(tarjetaDeportivaTarifaDTO, conectedUserId);
        return UIEntity.toUI(tarjetaDeportivaTarifaDTO);
    }

    @DELETE
    @Path("{tarjetaDeportivaTipoId}")
    public void delete(@PathParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTarifaId) {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        tarjetaDeportivaTarifaService.delete(tarjetaDeportivaTarifaId, conectedUserId);
    }
}
