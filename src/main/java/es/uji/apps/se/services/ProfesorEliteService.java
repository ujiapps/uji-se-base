package es.uji.apps.se.services;

import es.uji.apps.se.dao.ProfesorEliteDAO;
import es.uji.apps.se.dto.ProfesorEliteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfesorEliteService {

    private ProfesorEliteDAO profesorEliteDAO;


    @Autowired
    public ProfesorEliteService(ProfesorEliteDAO profesorEliteDAO) {
        this.profesorEliteDAO = profesorEliteDAO;

    }

    public ProfesorEliteDTO getProfesorEliteById(Long profesorElite) {
        return profesorEliteDAO.getProfesorEliteById(profesorElite);
    }
}
