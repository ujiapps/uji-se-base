package es.uji.apps.se.services;

import es.uji.apps.se.dao.PersonaTitulacionDAO;
import es.uji.apps.se.dto.PersonaTitulacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaTitulacionService {

    private PersonaTitulacionDAO personaTitulacionDAO;

    @Autowired
    public PersonaTitulacionService(PersonaTitulacionDAO personaTitulacionDAO) {

        this.personaTitulacionDAO = personaTitulacionDAO;
    }

    public List<PersonaTitulacionDTO> getTitulacionesElite(Long personaId, Long cursoAcademico) {
        return personaTitulacionDAO.getTitulacionesElite(personaId, cursoAcademico);
    }
}
