package es.uji.apps.se.services;

import es.uji.apps.se.dao.TipoReciboDAO;
import es.uji.apps.se.dto.TipoReciboVWDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoReciboService {

    private TipoReciboDAO tipoReciboDAO;

    @Autowired
    public TipoReciboService(TipoReciboDAO tipoReciboDAO) {
        this.tipoReciboDAO = tipoReciboDAO;
    }

    public List<TipoReciboVWDTO> getTiposRecibo() {
        return tipoReciboDAO.getTiposRecibo();
    }
}
