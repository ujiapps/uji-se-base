package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.DorsalDisponible;
import es.uji.apps.se.model.EquipacionesUsuario;
import es.uji.apps.se.model.Talla;
import es.uji.apps.se.services.EquipacionesUsuarioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("equipacionesusuario")
public class EquipacionesUsuarioResource extends CoreBaseService {
    @InjectParam
    EquipacionesUsuarioService equipacionesUsuarioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEquipacionesUsuario(@QueryParam("cursoAca") Long cursoAca, @QueryParam("perId") Long persona,
                                                  @QueryParam("general") @DefaultValue("false") Boolean general,
                                                  @QueryParam("page") Long page,
                                                  @QueryParam("start") @DefaultValue("0") Long start,
                                                  @QueryParam("limit") @DefaultValue("25") Long limit) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<EquipacionesUsuario> list = equipacionesUsuarioService.getEquipacionesUsuario(cursoAca, persona, general);
            responseMessage.setData(UIEntity.toUI(list));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("tallas")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTallas(@QueryParam("equipacionId") Long equipId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<Talla> list = equipacionesUsuarioService.getTallas(equipId);
            responseMessage.setData(UIEntity.toUI(list));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("tallasDisponibles")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTallasDisponibles(@QueryParam("equipacionId") Long equipId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<Talla> list = equipacionesUsuarioService.getTallasDisponibles(equipId);
            responseMessage.setData(UIEntity.toUI(list));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("allDorsales")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getAllDorsales(@QueryParam("equipacionId") Long equipId,
                                       @QueryParam("talla") String talla) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            if (talla.equals("Talla única") || talla.isEmpty()) talla = null;
            List<DorsalDisponible> list = equipacionesUsuarioService.getAllDorsales(equipId, talla);
            responseMessage.setData(UIEntity.toUI(list));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("dorsales")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDorsales(@QueryParam("equipacionId") Long equipId,
                                       @QueryParam("talla") String talla) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            if (talla.isEmpty()) talla = null;
            List<DorsalDisponible> list = equipacionesUsuarioService.getDorsales(equipId, talla);
            responseMessage.setData(UIEntity.toUI(list));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("dorsalesDisponibles")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDorsalesDisponibles(@QueryParam("equipacionId") Long equipId,
                                                  @QueryParam("talla") String talla,
                                                  @QueryParam("dorsalActual") Long dorsalActual) {
        ResponseMessage responseMessage = new ResponseMessage();
        String dorsalActualString = dorsalActual != null ? dorsalActual.toString() : "Sense numeració";

        try {
            List<DorsalDisponible> list = equipacionesUsuarioService.getDorsales(equipId, talla);
            if (list == null || list.isEmpty()) {
                list = new ArrayList<>();
                list.add(new DorsalDisponible(dorsalActual, dorsalActualString));
            } else if(list.stream().noneMatch(dorsal -> dorsal.getId() == dorsalActual))
                list.add(new DorsalDisponible(dorsalActual, dorsalActualString));

            responseMessage.setData(UIEntity.toUI(list));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
