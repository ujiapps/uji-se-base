package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.CheckDTO;
import es.uji.apps.se.services.CheckService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.ArrayList;
import java.util.List;

@Path("historicocheck")

public class HistoricoCheckResource extends CoreBaseService {

    @InjectParam
    CheckService checkService;

    @PathParam("tarjetaDeportivaId")
    Long tarjetaDeportivaId;

    @GET
    public List<UIEntity> getHistoricoCheckByTarjetaDeportivaId()
    {
        return modelToUI(checkService.getCheckByTarjetaDeportivaId(tarjetaDeportivaId));
    }

    private UIEntity modelToUI (CheckDTO checkDTO){
        UIEntity entity = UIEntity.toUI(checkDTO);
        entity.put("claseDirigida", checkDTO.getCalendarioClase().getClase().getNombre());
        entity.put("fechaClase", checkDTO.getCalendarioClase().getCalendario().getDia());
        entity.put("horaClase", checkDTO.getCalendarioClase().getHoraInicio());
        return entity;
    }

    private List<UIEntity> modelToUI(List<CheckDTO> checksDTO){
        List<UIEntity> uiEntities = new ArrayList<UIEntity>();
        for (CheckDTO checkDTO : checksDTO){
            uiEntities.add(modelToUI(checkDTO));
        }
        return uiEntities;
    }
}
