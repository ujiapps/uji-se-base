package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.ParametroDAO;
import es.uji.apps.se.dto.CursoAcademicoDTO;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.domains.TipoParametro;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParametroService {

    private final ParametroDAO parametroDAO;
    @InjectParam
    private CursoAcademicoService cursoAcademicoService;

    @Autowired
    public ParametroService(ParametroDAO parametroDAO) {

        this.parametroDAO = parametroDAO;
    }

    public List<ParametroDTO> getParametrosGlobales(Paginacion paginacion) {
        return parametroDAO.getParametrosGlobales(paginacion);
    }

    public List<ParametroDTO> getParametrosPersonales(Long connectedUserId, Paginacion paginacion) {
        return parametroDAO.getParametrosPersonales(connectedUserId, paginacion);
    }

    public ParametroDTO getParametroGlobal(String nombre) {
        return parametroDAO.getParametroGlobal(nombre);
    }

    public ParametroDTO getParametro(String nombre, Long personaId) {
        return parametroDAO.getParametro(nombre, personaId);
    }

    public CursoAcademico getParametroCursoAcademico(Long personaId) {

        ParametroDTO cursoAcademicoDTO = getParametro(TipoParametro.CURSOACADEMICOUSUARIO.getNombre(), personaId);
        if (!ParamUtils.isNotNull(cursoAcademicoDTO.getId())) {
            cursoAcademicoDTO = getParametroGlobal(TipoParametro.CURSOACADEMICOGLOBAL.getNombre());
        }
        CursoAcademicoDTO curso = cursoAcademicoService.getCursoAcademicoById(ParamUtils.parseLong(cursoAcademicoDTO.getValor()));
        return new CursoAcademico(ParamUtils.parseLong(cursoAcademicoDTO.getValor()), curso.getFechaInicio(), curso.getFechaFin());
    }

    public ParametroDTO addParametro(ParametroDTO parametroDTO) {
        return parametroDAO.insert(parametroDTO);
    }

    public ParametroDTO updateParametro(ParametroDTO parametroDTO) {
        return parametroDAO.update(parametroDTO);
    }

    public void deleteParametro(Long parametroId) {
        parametroDAO.delete(ParametroDTO.class, parametroId);
    }

    public ParametroDTO updateParametroCursoAcademico(Long connectedUserId, Long cursoAcademico) {
        ParametroDTO curso = getParametro(TipoParametro.CURSOACADEMICOUSUARIO.getNombre(), connectedUserId);
        curso.setValor(cursoAcademico.toString());
        if (ParamUtils.isNotNull(curso.getId())) {
            return updateParametro(curso);
        }
        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(connectedUserId);
        curso.setPersona(persona);
        curso.setNombre(TipoParametro.CURSOACADEMICOUSUARIO.getNombre());
        return addParametro(curso);
    }

    public ParametroDTO getParametroSancionClaseDirigidaTiempo() {
        return getParametroGlobal(TipoParametro.SANCIONCLASEDIRIGIDATIEMPO.getNombre());
    }

    public ParametroDTO updateParametroSancionClaseDirigidaTiempo(Long tiempo) {
        ParametroDTO paramTiempo = getParametroGlobal(TipoParametro.SANCIONCLASEDIRIGIDATIEMPO.getNombre());
        paramTiempo.setValor(tiempo.toString());
        return updateParametro(paramTiempo);
    }
}
