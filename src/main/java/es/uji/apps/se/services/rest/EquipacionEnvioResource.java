package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.EquipacionEnvioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("equipacionenvio")
public class EquipacionEnvioResource extends CoreBaseService {

    @InjectParam
    EquipacionEnvioService equipacionEnvioService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEnvios() {
            return UIEntity.toUI(equipacionEnvioService.getEnvios());
    }

    @GET
    @Path("{id}")
    public UIEntity getEnvioById(@PathParam("id") Long envioId) {
        return UIEntity.toUI(equipacionEnvioService.getEnvioById(envioId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addEnvio(@FormParam("asunto") String asunto,
                             @FormParam("cuerpo") String cuerpo,
                             @FormParam("dias") Long dias) {

        return UIEntity.toUI(equipacionEnvioService.addEnvio(asunto, cuerpo, dias));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("{id}")
    public UIEntity updateEnvio(@PathParam("id") Long envioId,
                                @FormParam("asunto") String asunto,
                                @FormParam("cuerpo") String cuerpo,
                                @FormParam("dias") Long dias){

        return UIEntity.toUI(equipacionEnvioService.updateEnvio(envioId, asunto, cuerpo, dias));

    }

    @DELETE
    @Path("{id}")
    public void deleteEnvio(@PathParam("id") Long envioId) {
        equipacionEnvioService.deleteEnvio(envioId);
    }
}
