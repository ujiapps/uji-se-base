package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TarjetaDeportivaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.PersonaTarjetaBonoService;
import es.uji.apps.se.services.TarjetaDeportivaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class PersonaTarjetaDeportivaResource extends CoreBaseService {

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;
    @InjectParam
    PersonaTarjetaBonoService personaTarjetaBonoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTarjetasDeportivasByPersona(@PathParam("personaId") Long personaId,
                                                          @QueryParam("start") Long start, @QueryParam("limit") Long limit,
                                                          @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<TarjetaDeportivaDTO> lista = tarjetaDeportivaService.getTarjetaDeportivaByPersonaId(personaId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());

            if (lista != null) {
                responseMessage.setData(UIEntity.toUI(lista));
            }

            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("tiene")
    @Produces(MediaType.APPLICATION_JSON)
    public Response tieneTarjetaDeportiva(@PathParam("personaId") Long personaId) {
        Boolean tieneTarjetaDeportiva = personaTarjetaBonoService.tieneTarjetaDeportivaActiva(personaId);
        return Response.ok(tieneTarjetaDeportiva).build();
    }

    @GET
    @Path("tiene/{tipoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response tieneTarjeta(@PathParam("personaId") Long personaId,
                                 @PathParam("tipoId") String tipoId) {
        Boolean tieneTarjeta = tarjetaDeportivaService.tieneTarjeta(personaId, tipoId, null);
        return Response.ok(tieneTarjeta).build();
    }

}
