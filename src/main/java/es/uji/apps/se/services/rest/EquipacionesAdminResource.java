package es.uji.apps.se.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.se.dto.EquipacionesMovimientoDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.EquipacionesAdmin;
import es.uji.apps.se.model.EquipacionesMovimiento;
import es.uji.apps.se.services.EquipacionesAdminService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("equipacionesadmin")
public class EquipacionesAdminResource extends CoreBaseService
{
    @InjectParam
    EquipacionesAdminService equipacionesAdminService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEquipacionesAdmin(@QueryParam("perId") Long persona,
                                                @QueryParam("page") Long page,
                                                @QueryParam("start") @DefaultValue("0") Long start,
                                                @QueryParam("limit") @DefaultValue("25") Long limit)
    {
        ResponseMessage responseMessage = new ResponseMessage();
        try
        {
            List<EquipacionesAdmin> list = equipacionesAdminService.getEquipacionesAdmin(persona);
            responseMessage.setData(UIEntity.toUI(list));
            responseMessage.setSuccess(true);
        }
        catch (Exception e)
        {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postEquipacionesAdmin(@QueryParam("perId") Long persona, UIEntity entity){
        equipacionesAdminService.postEquipacionesAdmin(persona, Long.valueOf(entity.get("equipacionNombre")), entity.get("talla"));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity putEquipacionesAdmin(@PathParam("id") Long id, UIEntity entity){
        return UIEntity.toUI(equipacionesAdminService.putEquipacionesAdmin(id, ParamUtils.parseLong(entity.get("equipacionId")), entity.get("talla")));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteEquipacionesAdmin (@PathParam("id") Long equipacionAdminId) throws CommonException {
        equipacionesAdminService.deleteEquipacionAdmin(equipacionAdminId);
    }
}
