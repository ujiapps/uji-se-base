package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.Tipo;
import es.uji.apps.se.services.InstalacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("instalacion")
public class InstalacionResource extends CoreBaseService {

    @InjectParam
    InstalacionService instalacionService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInstalaciones() {
        return UIEntity.toUI(instalacionService.getInstalaciones());
    }

    @GET
    @Path("{personaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getUsuarioInstalaciones(@PathParam("personaId") Long personaId) {
        ResponseMessage responseMessage = new ResponseMessage(false);

        try {
            List <Tipo> ubicaciones = instalacionService.getUsuarioInstalaciones();
            responseMessage.setData(UIEntity.toUI(ubicaciones));
            responseMessage.setSuccess(true);
            return responseMessage;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
