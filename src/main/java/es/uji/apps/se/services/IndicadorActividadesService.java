package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.model.*;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class IndicadorActividadesService {

    private AsistenciasClasesDirigidasDAO asistenciasClasesDirigidasDAO;
    private PersonasUJIVinculoDAO personasUJIVinculoDAO;
    private UsoDeportesPersonaDAO usoDeportesPersonaDAO;
    private CarnetBonoUsoDAO carnetBonoUsoDAO;

    @Autowired
    public IndicadorActividadesService(AsistenciasClasesDirigidasDAO asistenciasClasesDirigidasDAO, PersonasUJIVinculoDAO personasUJIVinculoDAO,
                                       UsoDeportesPersonaDAO usoDeportesPersonaDAO, CarnetBonoUsoDAO carnetBonoUsoDAO) {
        this.asistenciasClasesDirigidasDAO = asistenciasClasesDirigidasDAO;
        this.personasUJIVinculoDAO = personasUJIVinculoDAO;
        this.usoDeportesPersonaDAO = usoDeportesPersonaDAO;
        this.carnetBonoUsoDAO = carnetBonoUsoDAO;
    }

    public List<AsistenciasClasesDirigidas> getAsistencias(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        try {
            return asistenciasClasesDirigidasDAO.getAsistencias(fechaDesde, fechaHasta, horaDesde, horaHasta);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<UIEntity> getPersonasUJI() {
        return personasUJIVinculoDAO.getPersonasUJI();
    }

    public List<UsoDeportesPersona> getInscripcionesyPersonas(Date fechaDesde, Date fechaHasta) {
        return usoDeportesPersonaDAO.getUsosDeportesPersona(fechaDesde, fechaHasta);
    }

    public List<UsoDeportesPersonaDetalle> getInscripcionesyPersonasDetalle(String uso, String vinculoNombre, String sexo, String sFechaDesde, String sFechaHasta, Paginacion paginacion) throws ParseException {
        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return usoDeportesPersonaDAO.getInscripcionesyPersonasDetalle(uso, vinculoNombre, sexo, fechaDesde, fechaHasta, paginacion);
    }

    public List<TarjetaUsos> getTarjetasUsos(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        return carnetBonoUsoDAO.getTarjetasUsos(fechaDesde, fechaHasta, horaDesde, horaHasta);
    }

    public List<UsoDeportesPersona> getInscripcionesyPersonasSinUso(Date fechaDesde, Date fechaHasta) {
        return usoDeportesPersonaDAO.getInscripcionesyPersonasSinUso(fechaDesde, fechaHasta);
    }
}
