package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.IndicadorSatisfaccionDTO;
import es.uji.apps.se.exceptions.indicadoresSatisfaccion.IndicadoresSatisfaccionDeleteException;
import es.uji.apps.se.exceptions.indicadoresSatisfaccion.IndicadoresSatisfaccionInsertException;
import es.uji.apps.se.exceptions.indicadoresSatisfaccion.IndicadoresSatisfaccionUpdateException;
import es.uji.apps.se.model.IndicadorSatisfaccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class IndicadorSatisfaccionService {

    private IndicadorSatisfaccionDAO indicadorSatisfaccionDAO;
    private OfertaDAO ofertaDAO;
    private PeriodoDAO periodoDAO;
    private ActividadDAO actividadDAO;
    private IndicadoresTiposDAO indicadoresTiposDAO;

    @Autowired
    public IndicadorSatisfaccionService(IndicadorSatisfaccionDAO indicadorSatisfaccionDAO, OfertaDAO ofertaDAO, PeriodoDAO periodoDAO, ActividadDAO actividadDAO, IndicadoresTiposDAO indicadoresTiposDAO) {
        this.indicadorSatisfaccionDAO = indicadorSatisfaccionDAO;
        this.ofertaDAO = ofertaDAO;
        this.periodoDAO = periodoDAO;
        this.actividadDAO = actividadDAO;
        this.indicadoresTiposDAO = indicadoresTiposDAO;
    }

    public List<IndicadorSatisfaccion> getIndicadoresSatisfaccion(Long cursoId) {
        return indicadorSatisfaccionDAO.getIndicadoresSatisfaccion(cursoId);
    }

    @Transactional
    public void addIndicadorSatisfaccion(IndicadorSatisfaccion indicadorSatisfaccion) throws IndicadoresSatisfaccionInsertException {
        try {
            IndicadorSatisfaccionDTO indicadorSatisfaccionDTO = new IndicadorSatisfaccionDTO(indicadorSatisfaccion);
            indicadorSatisfaccionDTO.setOfertaDTO(ofertaDAO.getOfertaById(indicadorSatisfaccion.getOfertaId()));
            indicadorSatisfaccionDTO.setPeriodoDTO(periodoDAO.getPeriodoById(indicadorSatisfaccion.getPeriodoId()));
            indicadorSatisfaccionDTO.setIndicadorTipoId(indicadoresTiposDAO.getIndicadorTipoById(indicadorSatisfaccion.getIndicadorTipoId()));
            indicadorSatisfaccionDTO.setActividadDTO(actividadDAO.getActividadById(indicadorSatisfaccion.getActividadId()));

            indicadorSatisfaccionDAO.insert(indicadorSatisfaccionDTO);
        } catch (Exception e) {
            throw new IndicadoresSatisfaccionInsertException("Error al inserir un nou indicador de satisfacció");
        }
    }

    @Transactional
    public void updateIndicadorSatisfaccion(IndicadorSatisfaccion indicadorSatisfaccion) throws IndicadoresSatisfaccionUpdateException {
        try {
            IndicadorSatisfaccionDTO indicadorSatisfaccionDTO = new IndicadorSatisfaccionDTO(indicadorSatisfaccion);
            indicadorSatisfaccionDTO.setOfertaDTO(ofertaDAO.getOfertaById(indicadorSatisfaccion.getOfertaId()));
            indicadorSatisfaccionDTO.setPeriodoDTO(periodoDAO.getPeriodoById(indicadorSatisfaccion.getPeriodoId()));
            indicadorSatisfaccionDTO.setIndicadorTipoId(indicadoresTiposDAO.getIndicadorTipoById(indicadorSatisfaccion.getIndicadorTipoId()));
            indicadorSatisfaccionDTO.setActividadDTO(actividadDAO.getActividadById(indicadorSatisfaccion.getActividadId()));

            indicadorSatisfaccionDAO.update(indicadorSatisfaccionDTO);
        } catch (Exception e) {
            throw new IndicadoresSatisfaccionUpdateException("Error actualitzant aquest indicador de satisfacció");
        }
    }

    @Transactional
    public void deleteIndicadorSatisfaccion(IndicadorSatisfaccion indicadorSatisfaccion) throws IndicadoresSatisfaccionDeleteException {
        try {
            indicadorSatisfaccionDAO.deleteIndicadorSatisfaccion(indicadorSatisfaccion.getId());
        } catch (Exception e) {
            throw new IndicadoresSatisfaccionDeleteException("Error al eliminar el indicador de satisfacció seleccionat");
        }
    }
}
