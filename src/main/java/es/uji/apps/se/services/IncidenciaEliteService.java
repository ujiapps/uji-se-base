package es.uji.apps.se.services;

import es.uji.apps.se.dao.IncidenciaEliteDAO;
import es.uji.apps.se.dao.MailIncidenciaDAO;
import es.uji.apps.se.dto.IncidenciaEliteDTO;
import es.uji.apps.se.dto.MailIncidenciaDTO;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.ui.ConfiguracionIncidenciaEliteUI;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;

@Service
public class IncidenciaEliteService {

    private IncidenciaEliteDAO incidenciaEliteDAO;
    private ParametroService parametroService;
    private MailIncidenciaDAO mailIncidenciaDAO;

    @Autowired
    public IncidenciaEliteService(IncidenciaEliteDAO incidenciaEliteDAO, ParametroService parametroService, MailIncidenciaDAO mailIncidenciaDAO) {
        this.incidenciaEliteDAO = incidenciaEliteDAO;
        this.parametroService = parametroService;
        this.mailIncidenciaDAO = mailIncidenciaDAO;
    }

    public List<IncidenciaEliteDTO> getIncidenciasElite(Long connectedUserId) {

        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);

        return incidenciaEliteDAO.getIncidenciasElite(cursoAcademico);
    }

    public MailIncidenciaDTO sendMailIncidencia(Long incidenciaEliteId, MailIncidenciaDTO mailIncidenciaDTO) throws MessageNotSentException {

        MailMessage mensaje = new MailMessage("SE");

        mensaje.setTitle(mailIncidenciaDTO.getAsunto());
        mensaje.setContentType("text/html; charset=UTF-8");
        String cuerpo = mailIncidenciaDTO.getCuerpo();

        mensaje.setContent(cuerpo);

        String para = mailIncidenciaDTO.getResponder();

        if (!ParamUtils.isNotNull(para)) {
            para = "noreply@uji.es";
//            mensaje.setReplyTo("noreply@uji.es");
//        } else {
//            mensaje.setReplyTo(para);
        }

        mensaje.setReplyTo(para);
        String desde = mailIncidenciaDTO.getDesde();
        if (!ParamUtils.isNotNull(desde)) {
            mensaje.setSender("Servei d'esports <noreply@uji.es>");
        } else {
            mensaje.setSender(desde + " <" + para + ">");
        }


        mensaje.addToRecipient(mailIncidenciaDTO.getCorreo());
        mensaje.addBccRecipient(parametroService.getParametroGlobal("CORREOADMINISTRACIONINCIDENCIASELITE").getValor());

        MessagingClient cliente = new MessagingClient();
        cliente.send(mensaje);

        mailIncidenciaDTO.setFechaEnvio(new Date());
        return saveMailIncidencia(incidenciaEliteId, mailIncidenciaDTO);
    }

    public MailIncidenciaDTO saveMailIncidencia(Long incidenciaEliteId, MailIncidenciaDTO mailIncidenciaDTO) {

        IncidenciaEliteDTO incidenciaEliteDTO = new IncidenciaEliteDTO();
        incidenciaEliteDTO.setId(incidenciaEliteId);
        mailIncidenciaDTO.setIncidenciaElite(incidenciaEliteDTO);

        if (ParamUtils.isNotNull(mailIncidenciaDTO.getId()))
            mailIncidenciaDAO.update(mailIncidenciaDTO);
        else
            mailIncidenciaDAO.insert(mailIncidenciaDTO);

        return mailIncidenciaDTO;
    }


    public MailIncidenciaDTO getMailIncidencia(Long incidenciaEliteId) {

        List<MailIncidenciaDTO> mailsIncidencia = mailIncidenciaDAO.get(MailIncidenciaDTO.class, "incidencia_elite_id = " + incidenciaEliteId);

        if (mailsIncidencia.size() > 0) {
            return mailsIncidencia.get(0);
        }

        return new MailIncidenciaDTO();
    }

    public IncidenciaEliteDTO saveDatosIncidencia(IncidenciaEliteDTO incidenciaEliteDTO) {
        return incidenciaEliteDAO.update(incidenciaEliteDTO);
    }

    public IncidenciaEliteDTO getIncidenciaEliteById(Long incidenciaEliteId) {
        return incidenciaEliteDAO.get(IncidenciaEliteDTO.class, incidenciaEliteId).get(0);
    }

    public void deleteIncidenciaElite(Long incidenciaEliteId) {
        List<MailIncidenciaDTO> mails = mailIncidenciaDAO.get(MailIncidenciaDTO.class, "incidencia_elite_id = " + incidenciaEliteId);
        for (MailIncidenciaDTO mail : mails) {
            mailIncidenciaDAO.delete(MailIncidenciaDTO.class, mail.getId());
        }

        incidenciaEliteDAO.delete(IncidenciaEliteDTO.class, incidenciaEliteId);
    }

    public ConfiguracionIncidenciaEliteUI setConfiguracionIncidenciaElite() {
        ConfiguracionIncidenciaEliteUI configuracionIncidenciaEliteUI = new ConfiguracionIncidenciaEliteUI();
        configuracionIncidenciaEliteUI.setCorreoAdministracionIncidenciasElite(parametroService.getParametroGlobal("CORREOADMINISTRACIONINCIDENCIASELITE").getValor());
        return configuracionIncidenciaEliteUI;
    }

    public UIEntity saveConfiguracionIncidenciaElite(UIEntity entity) {

        ParametroDTO parametroDTO = parametroService.getParametroGlobal("CORREOADMINISTRACIONINCIDENCIASELITE");
        parametroDTO.setNombre("CORREOADMINISTRACIONINCIDENCIASELITE");
        parametroDTO.setValor(entity.get("correoAdministracionIncidenciasElite"));
        parametroService.updateParametro(parametroDTO);

        return entity;
    }
}
