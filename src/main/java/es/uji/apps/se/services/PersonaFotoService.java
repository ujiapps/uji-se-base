package es.uji.apps.se.services;

import es.uji.apps.se.dao.PersonaFotoDAO;
import es.uji.apps.se.dto.FotoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PersonaFotoService {

    PersonaFotoDAO personaFotoDAO;

    @Autowired
    public PersonaFotoService(PersonaFotoDAO personaFotoDAO){
        this.personaFotoDAO = personaFotoDAO;
    }

    public FotoDTO getPersonaFotoByClienteId(Long personaId) {
        return personaFotoDAO.getPersonaFotoByClienteId(personaId);
    }
}
