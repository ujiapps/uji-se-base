package es.uji.apps.se.services.rest.app;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.ReservaClasesDirigidas;
import es.uji.apps.se.model.Sancion;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.services.DateExtensions;
import es.uji.apps.se.services.PersonaSancionService;
import es.uji.apps.se.services.ReservasService;
import es.uji.apps.se.services.TarjetaDeportivaService;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.List;

public class AppReservasHistoricoResource extends CoreBaseService {

    @InjectParam
    PersonaSancionService personaSancionService;

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;

    @InjectParam
    ReservasService reservasService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template listadoReservasHistorico(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Sancion sancion = personaSancionService.getSancionBloqueaTodoOrClasesDirigidas(connectedUserId);
        TarjetaDeportiva tarjetaDeportiva = tarjetaDeportivaService.getTarjetaDeportivaActivaByPersonaId(connectedUserId);

        Template template = AppInfo.buildPaginaAplicacionClases("se/app/reservas-historico", idioma);

        anyadirSancionReservaAlTemplate(sancion, template);
        template.put("tarjetaDeportiva", tarjetaDeportiva);
        template.put("titulo", "Històric de reserves");
        List<ReservaClasesDirigidas> reservasPasadas = reservasService.getClasesPasadasByPersonaId(connectedUserId);
        template.put("reservasPasadas", reservasPasadas);
        return template;
    }

    private void anyadirSancionReservaAlTemplate(Sancion sancion, Template template) {
        if (ParamUtils.isNotNull(sancion)) {
            if (sancion.getTipo().equals("CLA") || sancion.getTipo().equals("ALL")) {
                String motivo = ParamUtils.isNotNull(sancion.getMotivo())? sancion.getMotivo() : "No s'ha especificat el motiu";
                if (ParamUtils.isNotNull(sancion.getFechaFin())) {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins al dia {0}. Motiu: {1}", DateExtensions.getDateAsString(sancion.getFechaFin()), motivo));
                } else {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins que resolgues la incidència: {0}", motivo));
                }
            } else
                template.put("mensajeInfo", null);
        } else {
            template.put("mensajeInfo", null);
        }

        if (ParamUtils.isNotNull(sancion) && (sancion.getTipo().equals("CLA") || sancion.getTipo().equals("ALL"))) {
            template.put("classSancion", "disabled");
        } else {
            template.put("classSancion", "");
        }
    }
}
