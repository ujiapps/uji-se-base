package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.*;
import es.uji.apps.se.dto.AccesoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.utils.Csv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class AccesoService {

    @InjectParam
    Csv csv;

    private final AccesoDAO accesoDAO;

    @Autowired
    public AccesoService(AccesoDAO accesoDAO) {
        this.accesoDAO = accesoDAO;
    }

    public List<AccesoDTO> getAccesos(Paginacion paginacion, String busqueda, Long zonaId, Date fechaDesde, Date fechaHasta) {
        return accesoDAO.getAccesos(paginacion, busqueda, zonaId, fechaDesde, fechaHasta);
    }

    public List<AccesoDTO> getAccesosByPersona(Paginacion paginacion, Long personaId) {
        return accesoDAO.getAccesosByPersona(paginacion, personaId);
    }

    public Response getCSVAccesosByBusqueda(String searchTexto, Long searchZona, Date fechaDesde, Date fechaHasta) {
        List<AccesoDTO> lista = getAccesosCsvBySearch(searchTexto, searchZona, fechaDesde, fechaHasta);
        Response.ResponseBuilder res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = accesos.csv");
        res.entity(accesoDTOToCSV(lista));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    private String accesoDTOToCSV(List<AccesoDTO> lista) {
        ArrayList<String> cabeceras = new ArrayList<>(Arrays.asList("Identificació", "Nom Usuari", "Data", "Entrada", "Eixida",
                "Zona", "Destí"));

        List<List<String>> records = new ArrayList<>();
        for (AccesoDTO acceso : lista) {
            List<String> recordMov = new ArrayList<>(creaListaString(acceso));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(AccesoDTO acceso) {
        return Arrays.asList(acceso.getIdentificacion(),
                acceso.getNombre(),
                acceso.getDia().toString(),
                acceso.getHoraEntrada().toString(),
                acceso.getHoraSalida().toString(),
                acceso.getZona(),
                acceso.getDestino());
    }

    private List<AccesoDTO> getAccesosCsvBySearch(String searchTexto, Long searchZona, Date fechaDesde, Date fechaHasta) {
        return accesoDAO.getAccesosCsvBySearch(searchTexto, searchZona, fechaDesde, fechaHasta);
    }
}
