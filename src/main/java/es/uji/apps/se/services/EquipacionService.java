package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionDAO;
import es.uji.apps.se.dto.EquipacionDTO;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones.EquipacionDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones.EquipacionInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones.EquipacionUpdateException;
import es.uji.apps.se.model.Equipacion;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EquipacionService {

    @Autowired
    EquipacionDAO equipacionDAO;

    public EquipacionService(EquipacionDAO equipacionDAO) {
        this.equipacionDAO = equipacionDAO;
    }

    public List<Equipacion> getEquipaciones(Paginacion paginacion, List<Map<String, String>> filtros) {
        return equipacionDAO.getEquipaciones(paginacion, filtros);
    }

    public void addEquipacion(UIEntity entity) throws EquipacionInsertException {
        try {
            Equipacion equipacion = new Equipacion(entity);
            equipacionDAO.insert(new EquipacionDTO(equipacion));
        } catch (Exception e) {
            throw new EquipacionInsertException();
        }
    }

    public void updateEquipacion(UIEntity entity) throws EquipacionUpdateException {
        try {
            Equipacion equipacion = new Equipacion(entity);
            equipacionDAO.update(new EquipacionDTO(equipacion));
        } catch (Exception e) {
            throw new EquipacionUpdateException();
        }
    }

    public void deleteEquipacion(Long id) throws EquipacionDeleteException {
        try {
            equipacionDAO.delete(EquipacionDTO.class, id);
        } catch (Exception e) {
            throw new EquipacionDeleteException();
        }
    }
}