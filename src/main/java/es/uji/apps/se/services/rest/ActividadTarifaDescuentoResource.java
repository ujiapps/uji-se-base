package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.ActividadTarifaDescuentoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.ActividadTarifaDescuentoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("descuento")
public class ActividadTarifaDescuentoResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(ActividadTarifaDescuentoResource.class);

    @InjectParam
    ActividadTarifaDescuentoService actividadTarifaDescuentoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getDescuentosTarifas(@PathParam("tarifaTipoId") Long tarifaId, @PathParam("actividadTarifaId") Long actividadTarifaId,
                                         @QueryParam("start") @DefaultValue("0") Long start,
                                         @QueryParam("limit") @DefaultValue("25") Long limit,
                                         @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<ActividadTarifaDescuentoDTO> tarifasDescuento = actividadTarifaDescuentoService.getDescuentosTarifas(actividadTarifaId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(tarifasDescuento)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getDescuentosTarifas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertActividadTarifaDescuento (UIEntity entity, @PathParam("actividadTarifaId") Long actividadTarifaId){
        return UIEntity.toUI(actividadTarifaDescuentoService.insertActividadTarifaDescuento(entity, actividadTarifaId));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateActividadTarifaDescuento (UIEntity entity){
        return UIEntity.toUI(actividadTarifaDescuentoService.updateActividadTarifaDescuento(entity));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteActividadTarifaDescuento (@PathParam("id") Long actividadTarifaDescuentoId){
        actividadTarifaDescuentoService.deleteActividadTarifaDescuento(actividadTarifaDescuentoId);
    }
}
