package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.AsistenciasClasesDirigidas;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.TarjetaUsos;
import es.uji.apps.se.model.UsoDeportesPersona;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.apps.se.services.IndicadorActividadCSVService;
import es.uji.apps.se.services.IndicadorActividadesService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class IndicadorActividadesResource extends CoreBaseService {

    @InjectParam
    IndicadorActividadesService indicadorActividadesService;
    @InjectParam
    IndicadorActividadCSVService indicadorActividadCSVService;

    @GET
    @Path("asistencia")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsistencias(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                         @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorActividadesService.getAsistencias(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("asistencia/export")
    @Produces("application/vnd.ms-excel")
    public Response generarCSVAsistencias(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                          @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException, IOException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<AsistenciasClasesDirigidas> asistencias = indicadorActividadesService.getAsistencias(fechaDesde, fechaHasta, horaDesde, horaHasta);

        ByteArrayOutputStream ostream = indicadorActividadCSVService.generateAsistenciasExcel(asistencias);
        return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = Asistencias.xls").build();
    }

    @GET
    @Path("tarjeta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTarjetasUsos(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                         @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorActividadesService.getTarjetasUsos(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("tarjeta/export")
    @Produces("application/vnd.ms-excel")
    public Response generarCSVTarjetas(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                          @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException, IOException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<TarjetaUsos> tarjetaUsos = indicadorActividadesService.getTarjetasUsos(fechaDesde, fechaHasta, horaDesde, horaHasta);

        ByteArrayOutputStream ostream = indicadorActividadCSVService.generateTarjetasUsosExcel(tarjetaUsos);
        return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = TarjetaUsos.xls").build();

    }

    @GET
    @Path("personasuji")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPersonasUJI() {

        return indicadorActividadesService.getPersonasUJI();
    }

    @GET
    @Path("personasuji/export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSVPersonasUJI() {

        List<UIEntity> personasUJI = indicadorActividadesService.getPersonasUJI();
        Response.ResponseBuilder res = null;

        res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = personasUJI.csv");

        res.entity(indicadorActividadCSVService.entityPersonasUJIToCSV(personasUJI));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    @GET
    @Path("inscripciones/uso")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInscripcionesyPersonas(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta) throws ParseException {
        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorActividadesService.getInscripcionesyPersonas(fechaDesde, fechaHasta));
    }

    @GET
    @Path("inscripciones/uso/detalle")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getInscripcionesyPersonasDetalle(@QueryParam("start") @DefaultValue("0") Long start,
                                                            @QueryParam("limit") @DefaultValue("20") Long limit,
                                                            @QueryParam("uso") String uso,
                                                            @QueryParam("vinculoNombre") String vinculoNombre,
                                                            @QueryParam("sexo") String sexo,
                                                            @QueryParam("fechaDesde") String sFechaDesde,
                                                            @QueryParam("fechaHasta") String sFechaHasta) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit);
            responseMessage.setData(UIEntity.toUI(indicadorActividadesService.getInscripcionesyPersonasDetalle(uso, vinculoNombre, sexo, sFechaDesde, sFechaHasta, paginacion)));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("inscripciones")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInscripcionesyPersonasSinUso(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta) throws ParseException {
        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorActividadesService.getInscripcionesyPersonasSinUso(fechaDesde, fechaHasta));
    }

    @GET
    @Path("inscripciones/export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSVInscripcionesyPersonas(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<UsoDeportesPersona> usos = indicadorActividadesService.getInscripcionesyPersonas(fechaDesde, fechaHasta);

        Response.ResponseBuilder res = null;

        res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = usosInscripciones.csv");

        res.entity(indicadorActividadCSVService.entityUsosDeportesPersonaToCSV(usos));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();

    }

    @GET
    @Path("inscripciones/uso/export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSVInscripcionesyPersonas2(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<UsoDeportesPersona> usos = indicadorActividadesService.getInscripcionesyPersonas(fechaDesde, fechaHasta);

        Response.ResponseBuilder res = null;

        res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = usosInscripciones.csv");

        res.entity(indicadorActividadCSVService.entityUsosDeportesPersonaToCSV(usos));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();

    }

    @Path("satisfaccion")
    public IndicadorSatisfaccionResource getPlatformItem(
            @InjectParam IndicadorSatisfaccionResource indicadorSatisfaccionResource) {
        return indicadorSatisfaccionResource;
    }
}
