package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.ActividadTarifaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.services.ActividadTarifaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("tarifa")
public class ActividadTarifaResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(ActividadTarifaResource.class);

    @Path("{actividadTarifaId}/descuento")
    public ActividadTarifaDescuentoResource getPlatformItem(
            @InjectParam ActividadTarifaDescuentoResource actividadTarifaDescuentoResource) {
        return actividadTarifaDescuentoResource;
    }

    @InjectParam
    ActividadTarifaService actividadTarifaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTarifasVinculos(@PathParam("tarifatipoId") Long tarifaTipoId, @QueryParam("start") @DefaultValue("0") Long start,
                                              @QueryParam("limit") @DefaultValue("25") Long limit,
                                              @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<ActividadTarifaDTO> tarifas = actividadTarifaService.getTarifasByTarifaTipo(tarifaTipoId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(tarifas)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getTarifasVinculos", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertTarifa(@PathParam("tarifatipoId") Long tarifaTipoId, UIEntity entity){
        return UIEntity.toUI(actividadTarifaService.insertTarifa(entity, tarifaTipoId));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTarifa(UIEntity entity, @PathParam("tarifatipoId") Long tarifatipoId) {
        return UIEntity.toUI(actividadTarifaService.updateTarifa(entity, tarifatipoId));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTarifaTipo(@PathParam("id") Long tarifaId) {
        actividadTarifaService.deleteTarifa(tarifaId);
    }
}
