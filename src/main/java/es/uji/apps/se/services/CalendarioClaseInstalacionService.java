package es.uji.apps.se.services;

import es.uji.apps.se.dao.CalendarioClaseInstalacionDAO;
import es.uji.apps.se.dto.CalendarioClaseDTO;
import es.uji.apps.se.dto.CalendarioClaseInstalacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalendarioClaseInstalacionService {

    private CalendarioClaseInstalacionDAO calendarioClaseInstalacionDAO;

    @Autowired
    public CalendarioClaseInstalacionService(CalendarioClaseInstalacionDAO calendarioClaseInstalacionDAO) {
        this.calendarioClaseInstalacionDAO = calendarioClaseInstalacionDAO;
    }

    public CalendarioClaseInstalacionDTO insertCalendarioClaseInstalacion(CalendarioClaseInstalacionDTO calendarioClaseInstalacionDTO) {
        return calendarioClaseInstalacionDAO.insert(calendarioClaseInstalacionDTO);
    }

    public void deleteCalendarioClaseInstalacionByCalendarioClase(CalendarioClaseDTO calendarioClaseDTO) {
        List<CalendarioClaseInstalacionDTO> calendarioClaseInstalacionDTOS = getCalendarioClaseInstalacionesByCalendarioClaseId(calendarioClaseDTO.getId());
        calendarioClaseInstalacionDTOS.forEach(
                calendarioClaseInstalacionDTO -> {
                    calendarioClaseInstalacionDAO.delete(CalendarioClaseInstalacionDTO.class, calendarioClaseInstalacionDTO.getId());
                }
        );
    }

    public List<CalendarioClaseInstalacionDTO> getCalendarioClaseInstalacionesByCalendarioClaseId(Long calendarioClaseId) {
        return calendarioClaseInstalacionDAO.getCalendarioClaseInstalacionesByCalendarioClaseId(calendarioClaseId);
    }
}
