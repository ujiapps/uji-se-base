package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.TipoReciboService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("tiporecibo")
public class TipoReciboResource extends CoreBaseService {

    @InjectParam
    TipoReciboService tipoReciboService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposRecibo() {
        return UIEntity.toUI(tipoReciboService.getTiposRecibo());
    }
}
