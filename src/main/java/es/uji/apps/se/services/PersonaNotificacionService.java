package es.uji.apps.se.services;

import es.uji.apps.se.dao.*;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.NotificacionPersona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaNotificacionService {

    private final PersonaNotificacionDAO personaNotificacionDAO;

    @Autowired
    public PersonaNotificacionService(PersonaNotificacionDAO personaNotificacionDAO) {
        this.personaNotificacionDAO = personaNotificacionDAO;
    }

    public List<NotificacionPersona> getNotificacionesPersona(Long personaId, Paginacion paginacion) {
        return personaNotificacionDAO.getNotificacionesPersona(personaId, paginacion);
    }
}
