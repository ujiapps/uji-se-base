package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.MiembrosPuntosDAO;
import org.springframework.stereotype.Service;

@Service
public class MiembrosPuntosService {

    @InjectParam
    private MiembrosPuntosDAO miembrosPuntosDAO;

    public Long getMiembrosPuntos(Long userId, Long partidoId) {
        return miembrosPuntosDAO.getMiembrosPuntos(userId, partidoId);
    }

    public Long getMiembrosTotal(Long equipoId, Long partidoId){

        return miembrosPuntosDAO.getMiembrosTotal(equipoId, partidoId);
    }

    public Long getMiembrosFantasmaPuntos(Long equipoId, Long partidoId){

        return miembrosPuntosDAO.getMiembrosFantasmaPuntos(equipoId, partidoId);
    }

    public void updateMiembrosPuntos(Long inscripcionId, Long partidoId, Long goles){

        miembrosPuntosDAO.updateMiembrosPuntos(inscripcionId, partidoId, goles);

    }

    public void updateMiembrosFantasmaPuntos(Long equipo, Long partidoId, Long goles){

        miembrosPuntosDAO.updateMiembrosFantasmaPuntos(equipo, partidoId, goles);

    }

    public Long updateGolesTotales(Long equipo, Long partidoId, String tipo){

        return miembrosPuntosDAO.updateGolesTotales(equipo, partidoId, tipo);

    }

}