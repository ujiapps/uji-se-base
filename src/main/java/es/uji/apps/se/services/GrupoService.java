package es.uji.apps.se.services;

import es.uji.apps.se.dao.GrupoDAO;
import es.uji.apps.se.dto.GrupoDTO;
import es.uji.apps.se.model.domains.TipoTaxonomia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GrupoService {
    private GrupoDAO grupoDAO;

    @Autowired
    public GrupoService(GrupoDAO grupoDAO) {

        this.grupoDAO = grupoDAO;
    }

    public GrupoDTO getGrupoByTaxonomia(TipoTaxonomia tipoTaxonomia) {
        return grupoDAO.getGrupoByTaxonomia(tipoTaxonomia);
    }

    public GrupoDTO getGrupoByGrupoId(Long id) {
        return grupoDAO.getGrupoByGrupoId(id);
    }
}
