package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

public class PersonaRenovacionResource extends CoreBaseService {
    @InjectParam
    PersonaRenovacionService personaRenovacionService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRenovacionesTodas(@PathParam("personaId") Long personaId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<CarnetBonoTipo> renovacionesZona = personaRenovacionService.getRenovacionesZona(personaId, true);
            responseMessage.setData(UIEntity.toUI(renovacionesZona));
            responseMessage.setTotalCount(renovacionesZona.size());
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("zona")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRenovacionesZona(@PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<CarnetBonoTipo> renovacionesZona = personaRenovacionService.getRenovacionesZona(personaId, false);
            responseMessage.setData(UIEntity.toUI(renovacionesZona));
            responseMessage.setTotalCount(renovacionesZona.size());
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }



    @GET
    @Path("TD")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRenovacionesTD(@PathParam("personaId") Long personaId) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<CarnetBonoTipo> renovacionesTD = personaRenovacionService.getRenovacionesTD(personaId);
            responseMessage.setData(UIEntity.toUI(renovacionesTD));
            responseMessage.setTotalCount(renovacionesTD.size());
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
