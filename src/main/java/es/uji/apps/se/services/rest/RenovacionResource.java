package es.uji.apps.se.services.rest;

import java.text.ParseException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.se.services.InscripcionService;
import es.uji.apps.se.services.OfertaService;
import es.uji.apps.se.services.SesionService;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

@Path("renovacion")
public class RenovacionResource extends CoreBaseService {

    @InjectParam
    OfertaService ofertaService;
    @InjectParam
    SesionService sesionService;
    @InjectParam
    InscripcionService inscripcionService;

    @GET
    @Path("{hash}")
    @Produces(MediaType.TEXT_HTML)
    public Template getPantallaRenovacionPorHash(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                                 @PathParam("hash") String hash)
            throws ParseException {
        Long personaId = sesionService.getPersonaByHash(hash);
        if (!ParamUtils.isNotNull(personaId)) {
            personaId = AccessManager.getConnectedUserId(request);
        }
        return listadoRenovaciones(idioma, personaId, hash);
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getActividadesRenovacion(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return listadoRenovaciones(idioma, connectedUserId, "");
    }

    @POST
    @Path("{hash}/devolucion")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public void tramitarDevolucionPorHash(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @PathParam("hash") String hash, @FormParam("ofertaId") Long ofertaId,
                                          @FormParam("iban") String iban) {
        Long personaId = sesionService.getPersonaByHash(hash);
        if (!ParamUtils.isNotNull(personaId)) {
            personaId = AccessManager.getConnectedUserId(request);
        }
        tramitarDevolucion(personaId, ofertaId, iban);
    }

    @POST
    @Path("devolucion")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void tramitarDevolucionPorLogin(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @FormParam("ofertaId") Long ofertaId,
                                           @FormParam("iban") String iban) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        tramitarDevolucion(connectedUserId, ofertaId, iban);
    }

    @POST
    @Path("{hash}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Template renovarActividadPorHash(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("hash") String hash, @FormParam("ofertaId") Long ofertaId) throws ParseException {
        Long personaId = sesionService.getPersonaByHash(hash);
        if (!ParamUtils.isNotNull(personaId)) {
            personaId = AccessManager.getConnectedUserId(request);
        }
        return renovarActividad(idioma, personaId, ofertaId);

    }

    @POST
    public Template renovarActividadPorLogin(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @FormParam("ofertaId") Long ofertaId) throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return renovarActividad(idioma, connectedUserId, ofertaId);
    }

    private Template renovarActividad(String idioma, Long connectedUserId, Long ofertaId) throws ParseException {

        if (ParamUtils.isNotNull(connectedUserId)) {
            Template template = AppInfo.buildPaginaSinMenuLateral("se/actividades/procesook", idioma);
            try {
                inscripcionService.altaInscripcionUsuarioCovid(connectedUserId, ofertaId);
            } catch (Exception e) {
                return AppInfo.buildPaginaSinMenuLateral("se/actividades/errorinscripcion", idioma);
            }

            return template;

        }
        return AppInfo.buildPaginaSinMenuLateral("se/actividades/errorautenticacion", idioma);
    }

    private Template listadoRenovaciones(String idioma, Long connectedUserId, String hash) throws ParseException {
        Template template = AppInfo.buildPaginaSinMenuLateral("se/base", idioma);

        if (ParamUtils.isNotNull(connectedUserId)) {
            template.put("contenido", "se/actividades/renovacion");
            template.put("hash", hash);
            template.put("listadoRenovacionesPendientes", ofertaService.getOfertasPendientesRenovacionByUsuario(connectedUserId));
            template.put("numRenovaciones", ofertaService.getOfertasPendientesRenovacionByUsuario(connectedUserId).size());
        } else {
            template.put("contenido", "se/actividades/errorautenticacion");
        }
        return template;
    }

    private void tramitarDevolucion(Long connectedUserId, Long ofertaId, String IBAN) {
        if (ParamUtils.isNotNull(connectedUserId)) {
            inscripcionService.altaDevolucionUsuarioCovid(connectedUserId, ofertaId, IBAN);
        }
    }
}
