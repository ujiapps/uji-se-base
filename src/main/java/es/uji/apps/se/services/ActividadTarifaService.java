package es.uji.apps.se.services;

import es.uji.apps.se.dao.ActividadTarifaDAO;
import es.uji.apps.se.dto.ActividadTarifaDTO;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActividadTarifaService {

    private final ActividadTarifaDAO actividadTarifaDAO;


    @Autowired
    public ActividadTarifaService(ActividadTarifaDAO actividadTarifaDAO) {
        this.actividadTarifaDAO = actividadTarifaDAO;
    }

    public List<ActividadTarifaDTO> getTarifasByTarifaTipo(Long tarifaTipoId, Paginacion paginacion) {
        return actividadTarifaDAO.getTarifasByTarifaTipo(tarifaTipoId, paginacion);
    }

    public ActividadTarifaDTO insertTarifa(UIEntity entity, Long tarifaTipoId) {

        return actividadTarifaDAO.insert(uiEntityToModel(entity, tarifaTipoId));
    }

    public ActividadTarifaDTO updateTarifa(UIEntity entity, Long tarifatipoId) {
        return actividadTarifaDAO.update(uiEntityToModel(entity, tarifatipoId));
    }


    public void deleteTarifa(Long tarifaId) {
        actividadTarifaDAO.delete(ActividadTarifaDTO.class, tarifaId);
    }

    private ActividadTarifaDTO uiEntityToModel(UIEntity entity, Long tarifaTipoId) {
        ActividadTarifaDTO actividadTarifaDTO = entity.toModel(ActividadTarifaDTO.class);

        TipoDTO tarifaTipoDTO = new TipoDTO();
        tarifaTipoDTO.setId(tarifaTipoId);
        actividadTarifaDTO.setTipo(tarifaTipoDTO);

        TipoDTO vinculo = new TipoDTO();
        vinculo.setId(ParamUtils.parseLong(entity.get("vinculoId")));
        actividadTarifaDTO.setVinculo(vinculo);

        actividadTarifaDTO.setFechaInicio(entity.getDate("fechaInicio"));

        if (ParamUtils.isNotNull(entity.get("fechaFin"))) {
            actividadTarifaDTO.setFechaFin(entity.getDate("fechaFin"));
        }

        return actividadTarifaDTO;
    }
}

