package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.EquipacionGenero;
import es.uji.apps.se.services.EquipacionGeneroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class EquipacionGeneroResource extends CoreBaseService {

    public final static Logger LOG = LoggerFactory.getLogger(EquipacionGeneroResource.class);

    @InjectParam
    EquipacionGeneroService equipacionGeneroService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getGenerosEquipacion() {
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(null);

        try {
            List<EquipacionGenero> generosEquipacion = equipacionGeneroService.getGenerosEquipacion();
            responseMessage.setData(UIEntity.toUI(generosEquipacion));
            responseMessage.setTotalCount(generosEquipacion.size());
        } catch (Exception e) {
            LOG.error("Error en l'obtenció dels generes d'equipacions", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
