package es.uji.apps.se.services;

import es.uji.apps.se.dao.PersonaFicheroDAO;
import es.uji.apps.se.dto.PersonaFicheroDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaFicheroService {

    private PersonaFicheroDAO personaFicheroDAO;

    @Autowired
    public PersonaFicheroService(PersonaFicheroDAO personaFicheroDAO) {

        this.personaFicheroDAO = personaFicheroDAO;
    }

    public List<PersonaFicheroDTO> getDocumentosElite(Long personaId) {
        return personaFicheroDAO.getDocumentosElite(personaId);
    }

    public List<PersonaFicheroDTO> getDocumentosPersona(Long personaId) {
        return personaFicheroDAO.getDocumentosPersona(personaId);
    }

    public PersonaFicheroDTO addDocumentoPersona(PersonaFicheroDTO personaFicheroDTO) {
        return personaFicheroDAO.insert(personaFicheroDTO);
    }

    public void deleteDocumentoPersona(Long ficheroId) {
        personaFicheroDAO.deleteDocumentoPersona(ficheroId);
    }
}
