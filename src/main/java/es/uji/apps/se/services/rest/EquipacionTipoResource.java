package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tipoEquipacion.TipoEquipacionDeleteException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tipoEquipacion.TipoEquipacionInsertException;
import es.uji.apps.se.exceptions.maestrosEquipaciones.tipoEquipacion.TipoEquipacionUpdateException;
import es.uji.apps.se.model.EquipacionTipo;
import es.uji.apps.se.services.EquipacionTipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class EquipacionTipoResource extends CoreBaseService {

    public final static Logger LOG = LoggerFactory.getLogger(EquipacionTipoResource.class);

    @InjectParam
    EquipacionTipoService equipacionTipoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTiposEquipacion() {
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(null);

        try {
            List<EquipacionTipo> tiposEquipacion = equipacionTipoService.getTiposEquipacion();
            responseMessage.setData(UIEntity.toUI(tiposEquipacion));
            responseMessage.setTotalCount(tiposEquipacion.size());
        } catch (Exception e) {
            LOG.error("Error en l'obtenció dels tipus d'equipacions", e);
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @POST
    public UIEntity addTipoEquipacion(UIEntity entity) throws TipoEquipacionInsertException {
        try {
            EquipacionTipo tipo = entity.toModel(EquipacionTipo.class);
            return UIEntity.toUI(equipacionTipoService.addTipoEquipacion(tipo));
        }
        catch (Exception e) {
            throw new TipoEquipacionInsertException("Ha hagut un problema al inserir un tipus d'equipació");
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteTipoEquipacion(@PathParam("id") Long id) throws TipoEquipacionDeleteException {
        try {
            equipacionTipoService.deleteTipoEquipacion(id);
            return Response.ok().build();
        }
        catch (Exception e) {
            throw new TipoEquipacionDeleteException("Ha hagut un problema al esborrar un tipus d'equipació");
        }
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoEquipacion(UIEntity entity) throws TipoEquipacionUpdateException {
        try {
            EquipacionTipo tipo = entity.toModel(EquipacionTipo.class);
            equipacionTipoService.updateTipoEquipacion(tipo);
            return UIEntity.toUI(tipo);
        }
        catch (Exception e) {
            throw new TipoEquipacionUpdateException("Ha hagut un problema al actualitzar un tipus d'equipació");
        }
    }
}
