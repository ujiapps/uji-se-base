package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.ValidacionException;
import es.uji.apps.se.model.*;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.apps.se.services.IndicadorInstalacionesService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class IndicadorInstalacionesResource extends CoreBaseService {

    @InjectParam
    IndicadorInstalacionesService indicadorInstalacionesService;

    @GET
    @Path("accesosporperfil")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAccesosPorPerfil(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                              @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorInstalacionesService.getAccesosPorPerfil(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("accesosporperfil/export")
    @Produces("application/pdf")
    public Template generarPDFAccesosPorPerfil(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                               @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<AccesosPorPerfil> accesos = indicadorInstalacionesService.getAccesosPorPerfilExport(fechaDesde, fechaHasta, horaDesde, horaHasta);

        Template template = new PDFTemplate("se/pdf/accesosporperfil", new Locale(cookieIdioma), "se");
        template.put("fechaDesde", fechaDesde);
        template.put("fechaHasta", fechaHasta);
        template.put("horaDesde", horaDesde);
        template.put("horaHasta", horaHasta);
        template.put("accesos", accesos);

        return template;
    }

    @GET
    @Path("accesosportipologia")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAccesosPorTipologia(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                                 @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorInstalacionesService.getAccesosPorTipologia(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("accesosportipologia/export")
    @Produces("application/pdf")
    public Template generarPDFAccesosPorTipologia(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                                  @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<AccesosPorTipologia> accesos = indicadorInstalacionesService.getAccesosPorTipologiaExport(fechaDesde, fechaHasta, horaDesde, horaHasta);

        Template template = new PDFTemplate("se/pdf/accesosportipologia", new Locale(cookieIdioma), "se");
        template.put("fechaDesde", fechaDesde);
        template.put("fechaHasta", fechaHasta);
        template.put("horaDesde", horaDesde);
        template.put("horaHasta", horaHasta);
        template.put("accesos", accesos);

        return template;
    }

    @GET
    @Path("horasportipologia")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHorasReservadasPorTipologia(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                                         @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorInstalacionesService.getHorasReservadasPorTipologia(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("horasportipologia/export")
    @Produces("application/pdf")
    public Template generarPDFHorasPorTipologia(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                                @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<HorasReservadasPorTipologia> horas = indicadorInstalacionesService.getHorasReservadasPorTipologiaExport(fechaDesde, fechaHasta, horaDesde, horaHasta);

        Template template = new PDFTemplate("se/pdf/horasportipologia", new Locale(cookieIdioma), "se");
        template.put("fechaDesde", fechaDesde);
        template.put("fechaHasta", fechaHasta);
        template.put("horaDesde", horaDesde);
        template.put("horaHasta", horaHasta);
        template.put("horas", horas);

        return template;
    }


    @GET
    @Path("franjahoraria")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAccesosPorFranjaHoraria(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                                     @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorInstalacionesService.getAccesosPorFranjaHoraria(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("franjahoraria/export")
    @Produces("application/pdf")
    public Template generarPDFAccesosPorFranjaHoraria(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                                      @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<AccesosFranjaHoraria> accesos = indicadorInstalacionesService.getAccesosPorFranjaHorariaExport(fechaDesde, fechaHasta, horaDesde, horaHasta);

        Template template = new PDFTemplate("se/pdf/accesosporfranjahoraria", new Locale(cookieIdioma), "se");
        template.put("fechaDesde", fechaDesde);
        template.put("fechaHasta", fechaHasta);
        template.put("horaDesde", horaDesde);
        template.put("horaHasta", horaHasta);
        template.put("accesos", accesos);

        return template;
    }

    @GET
    @Path("material")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getUsosMaterial(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                          @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorInstalacionesService.getUsosMaterial(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("material/export")
    @Produces("application/pdf")
    public Template generarPDFUsosMaterial(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                                      @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<UsosMaterial> usos = indicadorInstalacionesService.getUsosMaterialExport(fechaDesde, fechaHasta, horaDesde, horaHasta);

        Template template = new PDFTemplate("se/pdf/usosmaterial", new Locale(cookieIdioma), "se");
        template.put("fechaDesde", fechaDesde);
        template.put("fechaHasta", fechaHasta);
        template.put("horaDesde", horaDesde);
        template.put("horaHasta", horaHasta);
        template.put("usos", usos);

        return template;
    }

    @GET
    @Path("ocupacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOcupacionEspacios(@QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                               @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException, ValidacionException {

        Date fechaDesde = new UIDia(sFechaDesde, "yyyy-MM-dd'T'HH:mm:ss").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "yyyy-MM-dd'T'HH:mm:ss").getFecha();

        return UIEntity.toUI(indicadorInstalacionesService.getOcupacionEspacios(fechaDesde, fechaHasta, horaDesde, horaHasta));
    }

    @GET
    @Path("ocupacion/export")
    @Produces("application/pdf")
    public Template generarPDFOcupacion(@CookieParam("uji-lang") @DefaultValue("ca") String cookieIdioma, @QueryParam("fechaDesde") String sFechaDesde, @QueryParam("fechaHasta") String sFechaHasta,
                                           @QueryParam("horaDesde") String horaDesde, @QueryParam("horaHasta") String horaHasta) throws ParseException, ValidacionException {

        Date fechaDesde = new UIDia(sFechaDesde, "dd/MM/yyyy").getFecha();
        Date fechaHasta = new UIDia(sFechaHasta, "dd/MM/yyyy").getFecha();

        List<OcupacionEspacio> ocupaciones = indicadorInstalacionesService.getOcupacionEspaciosExport(fechaDesde, fechaHasta, horaDesde, horaHasta);

        Template template = new PDFTemplate("se/pdf/ocupacion", new Locale(cookieIdioma), "se");
        template.put("fechaDesde", fechaDesde);
        template.put("fechaHasta", fechaHasta);
        template.put("horaDesde", horaDesde);
        template.put("horaHasta", horaHasta);
        template.put("ocupaciones", ocupaciones);

        return template;
    }


}
