package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.model.AsistenciasClasesDirigidas;
import es.uji.apps.se.model.IndicadorSatisfaccion;
import es.uji.apps.se.model.TarjetaUsos;
import es.uji.apps.se.model.UsoDeportesPersona;
import es.uji.apps.se.utils.Csv;
import es.uji.commons.rest.UIEntity;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class IndicadorActividadCSVService {

    @InjectParam
    Csv csv;

    public ByteArrayOutputStream generateAsistenciasExcel(List<AsistenciasClasesDirigidas> asistencias) throws IOException {

        WriterExcelService excelService = new WriterExcelService();

        if (asistencias != null && asistencias.size() > 0) {
            int rownum = 0;
            excelService.addFulla("Asistencias");
            String[] columnas = new String[]{"Activitat", "Horari", "Places", "Reserves", "%Reserves", "Assistència", "%Assistència/reserves", "%Assistència/ofertades"};
            excelService.generaCeldes(excelService.getEstilNegreta(), rownum, columnas);

            for (AsistenciasClasesDirigidas asistenciasClasesDirigidas : asistencias) {

                int cellnum = 0;
                Row row = excelService.getNewRow(++rownum);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getActividad(), null, row);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getHorario(), null, row);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getPlazas(), row);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getReservas(), row);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getPorcentajeReservas(), row);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getAsistencias(), row);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getPorcentajeAsistenciaReservas(), row);
                excelService.addCell(cellnum++, asistenciasClasesDirigidas.getPorcentajeAsistenciaOferta(), row);
            }
            excelService.autoSizeColumns(columnas.length); //numero de columnas
        }
        return excelService.getExcel();
    }


    public String entityPersonasUJIToCSV(List<UIEntity> personasUJI) {

        ArrayList<String> cabeceras = new ArrayList<String>(Arrays.asList("Vincle", "Num Persones"));

        List<List<String>> records = new ArrayList<List<String>>();
        for (UIEntity personaUJI : personasUJI) {
            List<String> recordMov = new ArrayList<String>(creaListaPersonasUJIString(personaUJI));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaPersonasUJIString(UIEntity personaUJI) {
        return Arrays.asList(
                personaUJI.get("vinculo"),
                personaUJI.get("personas")
        );
    }

    public String entityUsosDeportesPersonaToCSV(List<UsoDeportesPersona> usos) {
        ArrayList<String> cabeceras = new ArrayList<String>(Arrays.asList("Uso", "Vincle", "Sexe", "Num Persones", "Num Inscripcions"));

        List<List<String>> records = new ArrayList<List<String>>();
        for (UsoDeportesPersona uso : usos) {
            List<String> recordMov = new ArrayList<String>(creaListaUsoDeportesPersonaString(uso));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaUsoDeportesPersonaString(UsoDeportesPersona usoDeportesPersona) {
        return Arrays.asList(
                usoDeportesPersona.getUso(),
                usoDeportesPersona.getVinculo(),
                usoDeportesPersona.getSexo(),
                usoDeportesPersona.getPersonas().toString(),
                usoDeportesPersona.getInscripciones().toString()
        );
    }

    public ByteArrayOutputStream generateTarjetasUsosExcel(List<TarjetaUsos> tarjetaUsos) throws IOException {
        WriterExcelService excelService = new WriterExcelService();

        if (tarjetaUsos != null && tarjetaUsos.size() > 0) {
            int rownum = 0;
            excelService.addFulla("TarjetasUsos");
            String[] columnas = new String[]{"Targeta", "Actives", "Reservas", "Usos", "%Reserves", "%Utilizació"};
            excelService.generaCeldes(excelService.getEstilNegreta(), rownum, columnas);

            for (TarjetaUsos tarjetaUso : tarjetaUsos) {

                int cellnum = 0;
                Row row = excelService.getNewRow(++rownum);
                excelService.addCell(cellnum++, tarjetaUso.getTarjeta(), null, row);
                excelService.addCell(cellnum++, tarjetaUso.getActivas(), row);
                excelService.addCell(cellnum++, tarjetaUso.getReservas(), row);
                excelService.addCell(cellnum++, tarjetaUso.getUsos(), row);
                excelService.addCell(cellnum++, tarjetaUso.getPorcentajeReservas(), row);
                excelService.addCell(cellnum++, tarjetaUso.getPorcentajeUtilizacion(), row);
            }
            excelService.autoSizeColumns(columnas.length); //numero de columnas
        }
        return excelService.getExcel();
    }

    public ByteArrayOutputStream generateIndicadoresSatisfaccionExcel(List<IndicadorSatisfaccion> indicadoresSatisfaccion, Long clasesDirigidasActivas, Long clasesDirigidasImpartidas) throws IOException {

        WriterExcelService excelService = new WriterExcelService();

        if (indicadoresSatisfaccion != null && indicadoresSatisfaccion.size() > 0) {
            int rownum = 0;
            excelService.addFulla("Asistencias");
            String[] columnas = new String[]{"Període", "Tipus Indicador", "Temporalitat", "Valor", "Enquestes", "Població", "", "Classes dirigides actives", "Classes dirigides impartides durant el curs"};
            excelService.generaCeldes(excelService.getEstilNegreta(), rownum, columnas);

            for (IndicadorSatisfaccion indicadorSatisfaccion : indicadoresSatisfaccion) {

                int cellnum = 0;
                Row row = excelService.getNewRow(++rownum);
                excelService.addCell(cellnum++, indicadorSatisfaccion.getPeriodoNombre(), null, row);
                excelService.addCell(cellnum++, indicadorSatisfaccion.getIndicadorTipoNombre(), null, row);
                excelService.addCell(cellnum++, indicadorSatisfaccion.getTipoTempoNombre(), null, row);
                excelService.addCell(cellnum++, indicadorSatisfaccion.getValor(), row);
                excelService.addCell(cellnum++, indicadorSatisfaccion.getEncuestas(), row);
                excelService.addCell(cellnum++, indicadorSatisfaccion.getPoblacion(), row);
                if (rownum == 1) {
                    cellnum++;
                    excelService.addCell(cellnum++, clasesDirigidasActivas, row);
                    excelService.addCell(cellnum++, clasesDirigidasImpartidas, row);
                }
            }
            excelService.autoSizeColumns(columnas.length); //numero de columnas
            excelService.setColumnWidth(6, 10 * 256);

        }
        return excelService.getExcel();
    }
}
