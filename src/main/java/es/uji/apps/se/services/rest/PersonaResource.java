package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.FotoDTO;
import es.uji.apps.se.exceptions.CommonException;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("persona")
public class PersonaResource extends CoreBaseService {

    @Path("{personaId}/documento")
    public PersonaFicheroResource getPlatformItem(
            @InjectParam PersonaFicheroResource personaFicheroResource) {
        return personaFicheroResource;
    }

    @Path("{personaId}/bonificacion")
    public PersonaBonificacionResource getPlatformItem(
            @InjectParam PersonaBonificacionResource personaBonificacionResource) {
        return personaBonificacionResource;
    }

    @Path("{personaId}/sanciones")
    public PersonaSancionResource getPlatformItem(
            @InjectParam PersonaSancionResource personaSancionResource) {
        return personaSancionResource;
    }

    @Path("{personaId}/recibos")
    public PersonaReciboResource getPlatformItem(
            @InjectParam PersonaReciboResource personaReciboResource) {
        return personaReciboResource;
    }

    @Path("{personaId}/renovacion")
    public PersonaRenovacionResource getPlatformItem(
            @InjectParam PersonaRenovacionResource personaRenovacionResource
    ) {
        return personaRenovacionResource;
    }

    @Path("{personaId}/carnetbono")
    public PersonaCarnetBonoResource getPlatformItem(
            @InjectParam PersonaCarnetBonoResource personaCarnetBonoResource
    ){
        return personaCarnetBonoResource;
    }

    @Path("{personaId}/notificaciones")
    public PersonaNotificacionResource getPlatformItem(
            @InjectParam PersonaNotificacionResource personaNotificacionResource
    ){
        return personaNotificacionResource;
    }

    @Path("{personaId}/accesoshoy")
    public PersonaAutorizacionResource getPlatformItem(
            @InjectParam PersonaAutorizacionResource personaAutorizacionResource
    ){
        return personaAutorizacionResource;
    }

    @Path("{personaId}/accesostorno")
    public PersonaAccesoTornoResource getPlatformItem(
            @InjectParam PersonaAccesoTornoResource personaAccesosTornoResource
    ){
        return personaAccesosTornoResource;
    }

    @Path("{personaId}/preinscripciones")
    public PersonaPreinscripcionResource getPlatformItem(
            @InjectParam PersonaPreinscripcionResource preinscripcionResource
    ){
        return preinscripcionResource;
    }

    @Path("{personaId}/tarjetadeportiva")
    public PersonaTarjetaDeportivaResource getPlatformItem(
            @InjectParam PersonaTarjetaDeportivaResource personaTarjetaDeportivaResource
    ){
        return personaTarjetaDeportivaResource;
    }

    @Path("{personaId}/inscripcion")
    public InscripcionUsuarioResource getPlatformItem(
            @InjectParam InscripcionUsuarioResource inscripcionUsuarioResource
    ){
        return inscripcionUsuarioResource;
    }

    @InjectParam
    PersonaService personaService;
    @InjectParam
    PersonaUjiService personaUjiService;
    @InjectParam
    PersonaFotoService personaFotoService;
    @InjectParam
    PersonaRenovacionInscripcionService personaRenovacionInscripcionService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getPersonaById(@PathParam("id") Long id) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            responseMessage.setData(UIEntity.toUI(Collections.singletonList(personaUjiService.getPersonaById(id))));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage(e.getMessage());
        }
        return responseMessage;
    }

    @GET
    @Path("{id}/foto")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getFotoPersona(@PathParam("id") Long personaId) {
        UIEntity ui = new UIEntity();
        FotoDTO personaFoto = personaFotoService.getPersonaFotoByClienteId(personaId);
        if (personaFoto.getFoto() == null || personaFoto.getMimeType() == null)
            ui.put("binarioPersona", "");
        else {
            String foto2 = "data:" + personaFoto.getMimeType().toLowerCase() + ";base64," + new String(Base64.getEncoder().encode(personaFoto.getFoto()));
            ui.put("binarioPersona", foto2);
        }
        return ui;
    }

    @PUT
    @Path("{personaId}/compruebavinculodeportes")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response compruebaVinculoDeportes(@PathParam("personaId") Long personaId) throws CommonException {
        personaService.compruebaSiExisteUsuarioEnDeportes(personaId);
        return Response.ok().build();
    }

    @GET
    @Path("{personaId}/{periodoId}/{tipoId}/actividades")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getActividades(@PathParam("personaId") Long personaId,
                                          @PathParam("periodoId") Long periodoId,
                                          @PathParam("tipoId") Long tipoId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<ActividadPersona> actividades = personaService.getActividades(personaId, periodoId, tipoId);
            responseMessage.setTotalCount(actividades.size());
            responseMessage.setData(UIEntity.toUI(actividades));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @PUT
    @Path("{personaId}/{grupoId}/renovacion")
    public Response fichaRenovacion(@PathParam("personaId") Long personaId,
                                    @PathParam("grupoId") Long grupoId) throws CommonException {
        personaRenovacionInscripcionService.fichaRenovacion(personaId, grupoId);
        return Response.ok().build();
    }

    @GET
    @Path("esadmin")
    @Produces(MediaType.APPLICATION_JSON)
    public Response esAdmin() {
        Long personaId = AccessManager.getConnectedUserId(request);
        Boolean esAdmin = personaService.esAdmin(personaId);
        return Response.ok(esAdmin).build();
    }

    @GET
    @Path("isBecario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response isBecario() {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        final Long perfilBecario = 4L;
        Boolean isBecario = personaService.tienePerfil(conectedUserId, perfilBecario);
        return Response.ok(isBecario).build();
    }

    @GET
    @Path("{personaId}/moroso")
    @Produces(MediaType.APPLICATION_JSON)
    public Response esMoroso(@PathParam("personaId") Long personaId) {
        Boolean esMoroso = personaService.esMoroso(personaId);
        return Response.ok(esMoroso).build();
    }



}
