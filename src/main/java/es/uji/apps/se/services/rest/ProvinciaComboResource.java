package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.ProvinciaComboService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("provinciacomboresource")
public class ProvinciaComboResource extends CoreBaseService {

    @InjectParam
    ProvinciaComboService provinciaComboService;

    @GET
    public ResponseMessage getProvinciaCombo() {
        ResponseMessage response = new ResponseMessage();
        try {
            response.setData(UIEntity.toUI(provinciaComboService.getProvinciaCombo()));
            response.setSuccess(true);

        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }
}