package es.uji.apps.se.services;

import es.uji.apps.se.dao.IncidenciasEliteUsuarioDAO;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.ui.IncidenciaEliteUI;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class IncidenciaEliteUsuarioService {

    private ParametroService parametroService;
    private IncidenciasEliteUsuarioDAO incidenciasEliteUsuarioDAO;

    @Autowired
    public IncidenciaEliteUsuarioService(ParametroService parametroService, IncidenciasEliteUsuarioDAO incidenciasEliteUsuarioDAO) {
        this.parametroService = parametroService;
        this.incidenciasEliteUsuarioDAO = incidenciasEliteUsuarioDAO;
    }

    public List<IncidenciaEliteDTO> getIncidenciasByPersonaId(Long connectedUserId) {
        CursoAcademico cursoAcademico = parametroService.getParametroCursoAcademico(connectedUserId);
        return incidenciasEliteUsuarioDAO.getIncidenciasByPersonaId(connectedUserId, cursoAcademico);
    }

    //    public void insertarIncidenciaElite(Long tipoIncidenciaId, Long curso, Long personaId, String motivoIncidencia, String detalleIncidencia) {
    public void insertarIncidenciaElite(IncidenciaEliteUI incidenciaEliteUI) throws ParseException {

        IncidenciaEliteDTO incidenciaEliteDTO = new IncidenciaEliteDTO();

        if (ParamUtils.isNotNull(incidenciaEliteUI.getCursoAcademico())) {
            CursoAcademicoDTO cursoAcademicoDTO = new CursoAcademicoDTO();
            cursoAcademicoDTO.setId(incidenciaEliteUI.getCursoAcademico());
            incidenciaEliteDTO.setCursoAcademico(cursoAcademicoDTO);
        }
        incidenciaEliteDTO.setDetalle(incidenciaEliteUI.getDetalle());
        incidenciaEliteDTO.setMotivo(incidenciaEliteUI.getMotivo());

        EliteIncidenciaTipoDTO eliteIncidenciaTipoDTO = new EliteIncidenciaTipoDTO();
        eliteIncidenciaTipoDTO.setId(incidenciaEliteUI.getTipoIncidencia());

        incidenciaEliteDTO.setTipoIncidencia(eliteIncidenciaTipoDTO);

        PersonaUjiDTO persona = new PersonaUjiDTO();
        persona.setId(incidenciaEliteUI.getPersona());
        incidenciaEliteDTO.setPersona(persona);

        incidenciaEliteDTO.setAsignatura(incidenciaEliteUI.getAsignatura());
        incidenciaEliteDTO.setFechaCambio(incidenciaEliteUI.getFechaCambio());
        incidenciaEliteDTO.setFechaCreacion(new Date());
        if (ParamUtils.isNotNull(incidenciaEliteUI.getFecha())) {
            SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyy-MM-dd");
            incidenciaEliteDTO.setFecha(formateadorFecha.parse(incidenciaEliteUI.getFecha()));
        }

        if (ParamUtils.isNotNull(incidenciaEliteUI.getProfesor())) {
            PersonaUjiDTO profesor = new PersonaUjiDTO();
            profesor.setId(incidenciaEliteUI.getProfesor());
            incidenciaEliteDTO.setProfesor(profesor);
        }

        incidenciaEliteDTO.setAsignaturasMatricula(incidenciaEliteUI.getAsignaturasMatricula());
        incidenciaEliteDTO.setAsignaturasAnula(incidenciaEliteUI.getAsignaturasAnula());

        incidenciasEliteUsuarioDAO.insert(incidenciaEliteDTO);
    }
}
