package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.AccesoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.apps.se.services.AccesoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.List;

@Path("acceso")
public class AccesoResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(AccesoResource.class);

    @InjectParam
    AccesoService accesoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getAccesos(@QueryParam("start") @DefaultValue("0") Long start,
                                      @QueryParam("limit") @DefaultValue("25") Long limit,
                                      @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                      @QueryParam("busqueda") String busqueda, @QueryParam("zonaId") Long zonaId,
                                      @QueryParam("fechaDesde") String fechaDesdeParam, @QueryParam("fechaHasta") String fechaHastaParam) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<AccesoDTO> accesosDTO = accesoService.getAccesos(paginacion, busqueda, zonaId, new UIDia(fechaDesdeParam).getFecha(), new UIDia(fechaHastaParam).getFecha());
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData((UIEntity.toUI(accesosDTO)));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getAccesos", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSVAccesosByBusqueda(@QueryParam("searchTexto") String searchTexto,
                                            @QueryParam("fechaDesde") String fechaDesdeParam,
                                            @QueryParam("fechaHasta") String fechaHastaParam,
                                            @QueryParam("searchZona") Long searchZona
    ) throws ParseException {
       return accesoService.getCSVAccesosByBusqueda(searchTexto, searchZona, new UIDia(fechaDesdeParam).getFecha(), new UIDia(fechaHastaParam).getFecha());
    }
}
