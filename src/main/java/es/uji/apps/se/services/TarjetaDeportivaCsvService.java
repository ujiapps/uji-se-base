package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.TarjetaDeportivaCsvDAO;
import es.uji.apps.se.model.filtros.FiltroTarjetaDeportiva;
import es.uji.apps.se.dto.views.TarjetaDeportivaCSV;
import es.uji.apps.se.utils.Csv;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TarjetaDeportivaCsvService {

    private final TarjetaDeportivaCsvDAO tarjetaDeportivaCsvDAO;

    @InjectParam
    Csv csv;

    @Autowired
    public TarjetaDeportivaCsvService(TarjetaDeportivaCsvDAO tarjetaDeportivaCsvDAO) {

        this.tarjetaDeportivaCsvDAO = tarjetaDeportivaCsvDAO;
    }

    public List<TarjetaDeportivaCSV> getCsvBySearch(FiltroTarjetaDeportiva filtro) {
        return tarjetaDeportivaCsvDAO.getCsvBySearch(filtro);
    }

    public List<TarjetaDeportivaCSV> getCsvByPersonaId(Long personaId) {
        return tarjetaDeportivaCsvDAO.getCsvByPersonaId(personaId);
    }


    public String entityTarjetaDeportivaToCSV(List<TarjetaDeportivaCSV> tarjetasDeportivasCSV) {

        ArrayList<String> cabeceras = new ArrayList<>(Arrays.asList("Tipus Targeta", "Nom", "Identificació", "Email", "Movil",
                "Vinculació", "Génere", "Data Naixement", "Data Alta", "Data Baixa", "Data Pagament", "Import",
                "Sancionat", "Tipus Sanció"));

        List<List<String>> records = new ArrayList<>();
        for (TarjetaDeportivaCSV tarjetaDeportivaCSV : tarjetasDeportivasCSV) {
            List<String> recordMov = new ArrayList<>(creaListaString(tarjetaDeportivaCSV));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(TarjetaDeportivaCSV tarjetaDeportivaCSV) {
        return Arrays.asList(tarjetaDeportivaCSV.getTipoTarjeta(),
                tarjetaDeportivaCSV.getApellidosNombre(),
                tarjetaDeportivaCSV.getIdentificacion(),
                tarjetaDeportivaCSV.getMail(),
                tarjetaDeportivaCSV.getMovil(),
                tarjetaDeportivaCSV.getVinculoActual(),
                tarjetaDeportivaCSV.getSexo(),
                ParamUtils.isNotNull(tarjetaDeportivaCSV.getFechaNacimiento()) ? tarjetaDeportivaCSV.getFechaNacimiento().toString() : "",
                ParamUtils.isNotNull(tarjetaDeportivaCSV.getFechaAlta()) ? tarjetaDeportivaCSV.getFechaAlta().toString() : "",
                ParamUtils.isNotNull(tarjetaDeportivaCSV.getFechaBaja()) ? tarjetaDeportivaCSV.getFechaBaja().toString() : "",
                ParamUtils.isNotNull(tarjetaDeportivaCSV.getFechaPago()) ? tarjetaDeportivaCSV.getFechaPago().toString() : "",
                ParamUtils.isNotNull(tarjetaDeportivaCSV.getImporte()) ? tarjetaDeportivaCSV.getImporte().toString() : "",
                tarjetaDeportivaCSV.getSancionado(),
                tarjetaDeportivaCSV.getTipoSancion());
    }
}

