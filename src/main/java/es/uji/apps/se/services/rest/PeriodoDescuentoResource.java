package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.PeriodoDTO;
import es.uji.apps.se.dto.PeriodoDescuentoDTO;
import es.uji.apps.se.services.PeriodoDescuentoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

public class PeriodoDescuentoResource extends CoreBaseService {

    @PathParam("periodoId")
    private Long periodoId;

    @InjectParam
    PeriodoDescuentoService periodoDescuentoService;


    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDescuentosByPeriodo() {
        return UIEntity.toUI(periodoDescuentoService.getDescuentosByPeriodo(this.periodoId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addDescuentoByPeriodo(UIEntity entity) throws ParseException {

        PeriodoDescuentoDTO periodoDescuentoDTO = UIToModel(entity, this.periodoId);
        return UIEntity.toUI(periodoDescuentoService.addDescuento(periodoDescuentoDTO));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public UIEntity updateDescuento(@PathParam("id") Long descuentoId, UIEntity entity) throws ParseException {
        PeriodoDescuentoDTO periodoDescuentoDTO = UIToModel(entity);
        return UIEntity.toUI(periodoDescuentoService.updateDescuento(periodoDescuentoDTO));
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public void deleteDescuento(@PathParam("id") Long descuentoId) {
        periodoDescuentoService.deleteDescuento(descuentoId);
    }

    private PeriodoDescuentoDTO UIToModel(UIEntity entity) throws ParseException {

        PeriodoDescuentoDTO periodoDescuentoDTO = entity.toModel(PeriodoDescuentoDTO.class);

        PeriodoDTO periodoDTO = new PeriodoDTO();
        periodoDTO.setId(ParamUtils.parseLong(entity.get("periodoId")));
        periodoDescuentoDTO.setPeriodo(periodoDTO);

        periodoDescuentoDTO.setFecha(entity.getDate("fecha"));

        return periodoDescuentoDTO;
    }

    private PeriodoDescuentoDTO UIToModel(UIEntity entity, Long periodoId) throws ParseException {

        PeriodoDescuentoDTO periodoDescuentoDTO = entity.toModel(PeriodoDescuentoDTO.class);

        PeriodoDTO periodoDTO = new PeriodoDTO();
        periodoDTO.setId(periodoId);
        periodoDescuentoDTO.setPeriodo(periodoDTO);

        periodoDescuentoDTO.setFecha(entity.getDate("fecha"));

        return periodoDescuentoDTO;
    }
}
