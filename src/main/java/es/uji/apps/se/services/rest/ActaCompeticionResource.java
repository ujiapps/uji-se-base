package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;

@Path("actacompeticion")
public class ActaCompeticionResource extends CoreBaseService {

    @InjectParam
    ActaCompeticionService actaCompeticionService;

    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template getActa(@PathParam("id") Long competicionId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        return actaCompeticionService.getActa(competicionId, idioma);
    }

    @POST
    @Path("asistencia")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage insertaAsistencia (UIEntity entity){

        return actaCompeticionService.insertaAsistenciaByInscripcionYPartido(ParamUtils.parseLong(entity.get("inscripcionId")), ParamUtils.parseLong(entity.get("partidoId")), "ACTA");
    }


    @POST
    @Path("goles")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertaGoles (UIEntity entity){

        return actaCompeticionService.updateMiembrosPuntos(ParamUtils.parseLong(entity.get("inscripcionId")),
                ParamUtils.parseLong(entity.get("partidoId")), ParamUtils.parseLong(entity.get("goles")));
    }

    @POST
    @Path("fantasma")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertaGolesFantasma (UIEntity entity){

        return actaCompeticionService.updateMiembrosFantasmaPuntos(ParamUtils.parseLong(entity.get("equipo")), 
                ParamUtils.parseLong(entity.get("partidoId")), ParamUtils.parseLong(entity.get("goles")));
    }

    @PUT
    @Path("golesTotales")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertaGolesTotales (UIEntity entity) throws JSONException {
        return actaCompeticionService.insertaGolesTotales(ParamUtils.parseLong(entity.get("equipo")),
                ParamUtils.parseLong(entity.get("partidoId")), entity.get("tipo"));
    }



    @POST
    @Path("sancion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertaSancion (UIEntity entity){

        return actaCompeticionService.insertaSancion(ParamUtils.parseLong(entity.get("inscripcionId")),
                ParamUtils.parseLong(entity.get("partidoId")), entity.get("sancion"));
    }

    @POST
    @Path("actualizar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response actualizaDatos (UIEntity entity){

        return actaCompeticionService.actualizaDatos(ParamUtils.parseLong(entity.get("partId")), entity.get("observaciones"));
    }
}
