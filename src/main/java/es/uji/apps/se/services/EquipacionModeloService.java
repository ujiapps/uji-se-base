package es.uji.apps.se.services;

import es.uji.apps.se.dao.EquipacionModeloDAO;
import es.uji.apps.se.dto.EquipacionModeloDTO;
import es.uji.apps.se.model.EquipacionesModelo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipacionModeloService {

    EquipacionModeloDAO equipacionModelDAO;

    @Autowired
    public EquipacionModeloService(EquipacionModeloDAO equipacionModelDAO) {
        this.equipacionModelDAO = equipacionModelDAO;
    }

    public List<EquipacionModeloDTO> getEquipacionModelos() {
        return equipacionModelDAO.get(EquipacionModeloDTO.class);
    }

    public EquipacionesModelo addEquipacionModelo(EquipacionesModelo equipacionesModelo){
        EquipacionModeloDTO equipacionModeloDTO = new EquipacionModeloDTO(equipacionesModelo);
        return new EquipacionesModelo(equipacionModelDAO.insert(equipacionModeloDTO));
    }

    public EquipacionesModelo updateEquipacionModelo(EquipacionesModelo equipacionesModelo){
        EquipacionModeloDTO equipacionModeloDTO = new EquipacionModeloDTO(equipacionesModelo);
        return new EquipacionesModelo(equipacionModelDAO.update(equipacionModeloDTO));
    }

    public void deleteEquipacionModelo(Long modeloId) {
        equipacionModelDAO.delete(EquipacionModeloDTO.class, modeloId);
    }
}
