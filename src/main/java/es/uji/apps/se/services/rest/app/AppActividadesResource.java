package es.uji.apps.se.services.rest.app;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.NoAutorizadoException;
import es.uji.apps.se.exceptions.VinculoNoContempladoException;
import es.uji.apps.se.model.Sancion;
import es.uji.apps.se.model.ui.UICalendario;
import es.uji.apps.se.model.ui.UIEvento;
import es.uji.apps.se.model.ui.UIFranjaHoraria;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.services.*;
import es.uji.apps.se.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class AppActividadesResource extends CoreBaseService {

    @InjectParam
    ReservasService reservasService;

    @InjectParam
    CalendarioService calendarioService;

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;

    @InjectParam
    PersonaSancionService personaSancionService;


    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template actividades(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException, VinculoNoContempladoException, NoAutorizadoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Template template = AppInfo.buildPaginaAplicacionClases("se/app/actividades", idioma);

        Sancion sancion = personaSancionService.getSancionBloqueaTodoOrClasesDirigidas(connectedUserId);
        anyadirSancionReservaAlTemplate(sancion, template);

        TarjetaDeportiva tarjetaDeportiva = tarjetaDeportivaService.getTarjetaDeportivaActivaByPersonaId(connectedUserId);

        template.put("tarjetaDeportiva", tarjetaDeportiva);
        UICalendario calendario = new UICalendario(LocalDate.now(), idioma);
        template.put("calendario", calendario);
        template.put("festivos", calendarioService.getDiasFestivos(calendario.getAnyo(), calendario.getMes()));
        template.put("dias", reservasService.getDiasPuedeReservarUsuario());

        Date fechaActividad = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());

        template.put("listadoActividades", "se/app/listadoActividades");
        rellenaTemplateListadoActividades(template, connectedUserId, fechaActividad);

        return template;
    }

    @GET
    @Path("{fecha}")
    @Produces(MediaType.TEXT_HTML)
    public Template listadoActividades(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("fecha") String fecha) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Template template = AppInfo.buildPaginaAplicacionClases("se/app/listadoActividades", idioma);

        DateFormat yyyymmddFormatter = new SimpleDateFormat("yyyy-MM-dd");

        Date fechaActividad = yyyymmddFormatter.parse(fecha);

        rellenaTemplateListadoActividades(template, connectedUserId, fechaActividad);
        return template;
    }

    @GET
    @Path("calendario/{mes}")
    @Produces(MediaType.TEXT_HTML)
    public Template calendario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("mes") Integer mes) throws ParseException, VinculoNoContempladoException {

        Template template = AppInfo.buildPaginaAplicacionClases("se/app/calendarioDias", idioma);

        mes = (mes == 0L) ? 12 : mes;

        UICalendario calendario = new UICalendario(mes, idioma);
        template.put("calendario", calendario);
        template.put("dias", reservasService.getDiasPuedeReservarUsuario());
        template.put("festivos", calendarioService.getDiasFestivos(calendario.getAnyo(), mes));

        return template;
    }

    private void rellenaTemplateListadoActividades(Template template, Long personaId, Date fechaActividad) {

        List<UIEvento> actividades = reservasService.getActividadesCalendario(personaId, fechaActividad, fechaActividad);
        List<UIFranjaHoraria> franjaHorarias = reservasService.getFranjasHorarias();
        for (UIFranjaHoraria uiFranjaHoraria : franjaHorarias) {
            uiFranjaHoraria.setEventos(getActividadesByFranjaHoraria(actividades, uiFranjaHoraria));
        }
        template.put("franjas", reservasService.getFranjasHorarias());
        template.put("fecha", fechaActividad);
        template.put("hayActividades", !actividades.isEmpty());

    }

    private List<UIEvento> getActividadesByFranjaHoraria(List<UIEvento> actividades, UIFranjaHoraria uiFranjaHoraria) {
        return actividades.stream().filter(a -> uiFranjaHoraria.solapa(a.getHoraInicio())).collect(
                Collectors.toList());
    }

    private void anyadirSancionReservaAlTemplate(Sancion sancion, Template template) {
        if (ParamUtils.isNotNull(sancion)) {
            if (sancion.getTipo().equals("CLA") || sancion.getTipo().equals("ALL")) {
                String motivo = ParamUtils.isNotNull(sancion.getMotivo())? sancion.getMotivo() : "No s'ha especificat el motiu";
                if (ParamUtils.isNotNull(sancion.getFechaFin())) {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins al dia {0}. Motiu: {1}", DateExtensions.getDateAsString(sancion.getFechaFin()), motivo));
                } else {
                    template.put("mensajeInfo", MessageFormat.format("No pots reservar fins que resolgues la incidència: {0}", motivo));
                }
            } else
                template.put("mensajeInfo", null);
        } else {
            template.put("mensajeInfo", null);
        }

        if (ParamUtils.isNotNull(sancion) && (sancion.getTipo().equals("CLA") || sancion.getTipo().equals("ALL"))) {
            template.put("classSancion", "disabled");
        } else {
            template.put("classSancion", "");
        }
    }
}
