package es.uji.apps.se.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dao.ProvinciaComboDAO;
import es.uji.apps.se.model.ProvinciaCombo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvinciaComboService {

    @InjectParam
    private ProvinciaComboDAO provinciaComboDAO;

    public List<ProvinciaCombo> getProvinciaCombo() {
        return provinciaComboDAO.getProvinciaCombo();
    }

}