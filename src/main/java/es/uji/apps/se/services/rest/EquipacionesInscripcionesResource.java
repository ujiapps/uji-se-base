package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.exceptions.EquipacionInscripcionException;
import es.uji.apps.se.services.EquipacionInscripcionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("equipacion-inscripcion")
public class EquipacionesInscripcionesResource extends CoreBaseService {
    @InjectParam
    EquipacionInscripcionService equipacionInscripcionService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEquipacionesInscripciones(UIEntity entity) throws EquipacionInscripcionException {
        equipacionInscripcionService.addEquipacionesInscripciones(entity);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEquipacionesInscripciones(UIEntity entity) throws EquipacionInscripcionException {
        equipacionInscripcionService.updateEquipacionesInscripciones(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEquipacionesInscripciones(@PathParam("id") Long id) throws EquipacionInscripcionException {
        equipacionInscripcionService.deleteEquipacionesInscripciones(id);
        return Response.ok().build();
    }
}
