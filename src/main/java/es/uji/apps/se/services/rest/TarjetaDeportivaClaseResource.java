package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TarjetaDeportivaClaseDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.TarjetaDeportivaDTO;
import es.uji.apps.se.exceptions.ReservaException;
import es.uji.apps.se.exceptions.VinculoNoContempladoException;
import es.uji.apps.se.model.ui.UITarjetaDeportivaClase;
import es.uji.apps.se.services.ReservasService;
import es.uji.apps.se.services.TarjetaDeportivaClaseService;
import es.uji.apps.se.services.TarjetaDeportivaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("tarjetadeportivaclase")
public class TarjetaDeportivaClaseResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(TarjetaDeportivaClaseResource.class);

    @InjectParam
    TarjetaDeportivaClaseService tarjetaDeportivaClaseService;

    @InjectParam
    TarjetaDeportivaService tarjetaDeportivaService;

    @InjectParam
    ReservasService reservasService;

    @PathParam("tarjetaDeportivaId")
    Long tarjetaDeportivaId;

    @GET
    @Path("calendario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getReservastarjetas(@PathParam("id") Long calendarioId, @QueryParam("start") @DefaultValue("0") Long start,
                                               @QueryParam("limit") @DefaultValue("25") Long limit,
                                               @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();

        try {
            Paginacion paginacion = new Paginacion(start, limit, sortJson);

            List<UIEntity> list = UIEntity.toUI(tarjetaDeportivaClaseService.getClasesByCalendarioId(calendarioId, paginacion).stream().map(UITarjetaDeportivaClase::new).collect(
                    Collectors.toList()));
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(list);
            responseMessage.setSuccess(true);


        } catch (Exception e) {
            log.error("getReservastarjetas", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("calendario/export")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReservastarjetas(@PathParam("id") Long calendarioId) {
        return tarjetaDeportivaClaseService.getReservasTarjetasCSV(calendarioId);
    }


    @GET
    public List<UIEntity> getClasesByTarjetaDeportivaId() {
        return modelToUI(tarjetaDeportivaClaseService.getClasesByTarjetaDeportivaId(tarjetaDeportivaId));
    }

    @DELETE
    @Path("calendario/{tarjetaDeportivaClaseId}")
    public void delete(@PathParam("tarjetaDeportivaClaseId") Long tarjetaDeportivaClaseId) {
        tarjetaDeportivaClaseService.delete(tarjetaDeportivaClaseId);
    }

    @DELETE
    @Path("{tarjetaDeportivaClaseId}")
    public void deleteReserva(@PathParam("tarjetaDeportivaClaseId") Long tarjetaDeportivaClaseId) {
        tarjetaDeportivaClaseService.delete(tarjetaDeportivaClaseId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insert(@FormParam("calendarioClaseId") Long calendarioClaseId) throws ReservaException, VinculoNoContempladoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        TarjetaDeportivaDTO tarjetaDeportivaDTO = tarjetaDeportivaService.getTarjetaDeportivaById(tarjetaDeportivaId);
        TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO = reservasService.addReservaClaseDirigida(calendarioClaseId, tarjetaDeportivaDTO.getPersonaUji().getId(), connectedUserId);
        return UIEntity.toUI(tarjetaDeportivaClaseDTO);
    }

    @PUT
    @Path("{tarjetaDeportivaClaseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("tarjetaDeportivaClaseId") Long tarjetaDeportivaClaseId)
            throws ReservaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(tarjetaDeportivaClaseService.updateAsistencia(tarjetaDeportivaClaseId, connectedUserId));
    }

    @PUT
    @Path("calendario/{tarjetaDeportivaClaseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateAsistencia(@PathParam("tarjetaDeportivaClaseId") Long tarjetaDeportivaClaseId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(tarjetaDeportivaClaseService.updateAsistencia(tarjetaDeportivaClaseId, connectedUserId));
    }



    private UIEntity modelToUI(TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO) {
        UIEntity entity = UIEntity.toUI(tarjetaDeportivaClaseDTO);

        entity.put("instalacionNombre", !tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendarioClaseInstalacionesDTO().isEmpty() ? tarjetaDeportivaClaseDTO.getCalendarioClase().getInstalacionesNombres() : "");
        entity.put("claseNombre", (ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getCalendarioClase().getClase())) ? tarjetaDeportivaClaseDTO.getCalendarioClase().getClase().getNombre() : "");
        entity.put("fecha", (ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendario())) ? tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendario().getDia() : "");
        return entity;
    }

    private List<UIEntity> modelToUI(List<TarjetaDeportivaClaseDTO> tarjetaDeportivaClasesDTO) {
        List<UIEntity> lista = new ArrayList<>();
        for (TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO : tarjetaDeportivaClasesDTO) {
            lista.add(modelToUI(tarjetaDeportivaClaseDTO));
        }
        return lista;
    }
}
