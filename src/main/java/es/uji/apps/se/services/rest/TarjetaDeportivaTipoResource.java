package es.uji.apps.se.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.apps.se.services.TarjetaDeportivaTipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

@Path("tarjetadeportivatipo")
public class TarjetaDeportivaTipoResource extends CoreBaseService {
    @InjectParam
    TarjetaDeportivaTipoService tarjetaDeportivaTipoService;

    @Path("{tarjetaDeportivaTipoId}/superior")
    public TarjetaDeportivaSuperiorResource getPlatformItem(
            @InjectParam TarjetaDeportivaSuperiorResource tarjetaDeportivaSuperiorResource) {
        return tarjetaDeportivaSuperiorResource;
    }

    @Path("{tarjetaDeportivaTipoId}/tarifa")
    public TarjetaDeportivaTarifaResource getPlatformItem(
            @InjectParam TarjetaDeportivaTarifaResource tarjetaDeportivaTarifaResource) {
        return tarjetaDeportivaTarifaResource;
    }

    @GET
    public List<UIEntity> getTarjetasDeportivasTipos() {
        return UIEntity.toUI(tarjetaDeportivaTipoService.getTarjetasDeportivasTiposActivas());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insert(UIEntity entity) throws ParseException {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(tarjetaDeportivaTipoService.insert(entityToModel(entity), conectedUserId));
    }

    @PUT
    @Path("{tarjetaDeportivaTipoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTipoId, UIEntity entity) throws ParseException {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(tarjetaDeportivaTipoService.update(entityToModel(entity), conectedUserId));
    }

    @DELETE
    @Path("{tarjetaDeportivaTipoId}")
    public void delete(@PathParam("tarjetaDeportivaTipoId") Long tarjetaDeportivaTipoId) {
        Long conectedUserId = AccessManager.getConnectedUserId(request);
        tarjetaDeportivaTipoService.delete(tarjetaDeportivaTipoId, conectedUserId);
    }

    @PUT
    @Path("{tarjetaDeportivaId}/reordenar-items")
    public void reordenarItems(@PathParam("tarjetaDeportivaId") Long tarjetaDeportivaId,
                               @FormParam("targetItem") Long tarjetaDeportivaDestinoId) {
        ParamUtils.checkNotNull(tarjetaDeportivaId, tarjetaDeportivaDestinoId);
        tarjetaDeportivaTipoService.reordenarItems(tarjetaDeportivaId,
                tarjetaDeportivaDestinoId);
    }


    private TarjetaDeportivaTipoDTO entityToModel(UIEntity entity) throws ParseException {
        TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO;

        if (ParamUtils.isNotNull(entity.get("id"))) {
            tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoService.getTarjetaDeportivaTipoById(ParamUtils.parseLong(entity.get("id")));
            TarjetaDeportivaTipoDTO aux = getTarjetaDeportivaTipoDTO(entity);

            tarjetaDeportivaTipoDTO.setHoraInicioPermiteReserva(aux.getHoraInicioPermiteReserva());
            tarjetaDeportivaTipoDTO.setHoraFinPermiteReserva(aux.getHoraFinPermiteReserva());
            tarjetaDeportivaTipoDTO.setNombre(aux.getNombre());
            tarjetaDeportivaTipoDTO.setActiva(aux.isActiva());
            tarjetaDeportivaTipoDTO.setMostrarUsuario(aux.isMostrarUsuario());
            tarjetaDeportivaTipoDTO.setPermiteClasesDirigidas(aux.isPermiteClasesDirigidas());
            tarjetaDeportivaTipoDTO.setPermiteInstalaciones(aux.isPermiteInstalaciones());
            tarjetaDeportivaTipoDTO.setPermiteMusculacion(aux.isPermiteMusculacion());
            tarjetaDeportivaTipoDTO.setDias(aux.getDias());
            tarjetaDeportivaTipoDTO.setMeses(aux.getMeses());
            tarjetaDeportivaTipoDTO.setAnyos(aux.getAnyos());
            tarjetaDeportivaTipoDTO.setFechaCaducidad(aux.getFechaCaducidad());
            tarjetaDeportivaTipoDTO.setTaxonomia(aux.getTaxonomia());

        } else
            tarjetaDeportivaTipoDTO =  getTarjetaDeportivaTipoDTO(entity);
        return tarjetaDeportivaTipoDTO;
    }

    private TarjetaDeportivaTipoDTO getTarjetaDeportivaTipoDTO(UIEntity entity) throws ParseException {
        return new TarjetaDeportivaTipoDTO(entity.get("id"),
                entity.get("nombre"),
                entity.get("permiteInstalaciones"),
                entity.get("permiteMusculacion"),
                entity.get("permiteClasesDirigidas"),
                entity.get("horaInicioPermiteReserva"),
                entity.get("horaFinPermiteReserva"),
                entity.get("tipoPlantilla"),
                entity.get("imagenTarjeta"),
                entity.get("activa"),
                entity.get("mostrarUsuario"),
                entity.get("dias"),
                entity.get("meses"),
                entity.get("anyos"),
                entity.get("fechaCaducidad"),
                entity.get("taxonomiaId"));
    }
}
