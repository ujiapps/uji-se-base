package es.uji.apps.se.model;

import java.math.BigDecimal;
import java.util.Date;

public class Bono {
    private Long tipoId;
    private Long personaId;
    private Long dias;
    private Long importe;
    private Long tipoInstalacionId;
    private String nombre;
    private Date fechaInicio;
    private Date fechaFin;
    private Boolean tieneTarjetaTZ;
    private Boolean tieneTarjetaBO;

    public Bono() {}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long id) {this.tipoId = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getDias() {return dias;}
    public void setDias(Long dias) {this.dias = dias;}

    public Long getImporte() {return importe;}
    public void setImporte(Long importe) {this.importe = importe;}

    public Long getTipoInstalacionId() {return tipoInstalacionId;}
    public void setTipoInstalacionId(Long tipoInstalacionId) {this.tipoInstalacionId = tipoInstalacionId;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Boolean isTieneTarjetaTZ() {return tieneTarjetaTZ;}
    public void setTieneTarjetaTZ(Boolean tieneTarjetaTZ) {this.tieneTarjetaTZ = tieneTarjetaTZ;}

    public Boolean isTieneTarjetaBO() {return tieneTarjetaBO;}
    public void setTieneTarjetaBO(Boolean tieneTarjetaBO) {this.tieneTarjetaBO = tieneTarjetaBO;}
}
