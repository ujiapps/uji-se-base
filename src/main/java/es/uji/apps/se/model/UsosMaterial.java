package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class UsosMaterial implements Serializable {

    private String nombre;
    private Long count;

    public UsosMaterial() {
    }

    @QueryProjection
    public UsosMaterial(String nombre, Long count) {
        this.nombre = nombre;
        this.count = count;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
