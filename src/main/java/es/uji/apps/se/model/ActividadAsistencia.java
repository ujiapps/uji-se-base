package es.uji.apps.se.model;

public class ActividadAsistencia {
    private Long calendarioOfertaId;
    private Long inscripcionId;
    private Long asistenciaId;

    public ActividadAsistencia() {}

    public Long getCalendarioOfertaId() {return calendarioOfertaId;}
    public void setCalendarioOfertaId(Long calendarioOfertaId) {this.calendarioOfertaId = calendarioOfertaId;}

    public Long getInscripcionId() {return inscripcionId;}
    public void setInscripcionId(Long inscripcionId) {this.inscripcionId = inscripcionId;}

    public Long getAsistenciaId() {return asistenciaId;}
    public void setAsistenciaId(Long asistenciaId) {this.asistenciaId = asistenciaId;}
}
