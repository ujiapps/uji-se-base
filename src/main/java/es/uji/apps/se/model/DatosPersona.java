package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class DatosPersona {

    private String nombre;

    private String apellido1;

    private String apellido2;

    private Long tipoIdentificacion;

    private String identificacion;

    private Date fechaNacimiento;

    private String sexo;

    private Long libroFamilia;

    private String mail;

    private String movil;

    private String movilPublico;

    private String iban;

    private String viaCombo;

    private String calle;

    private String numero;

    private String escalera;

    private String piso;

    private String puerta;

    private String codPostal;

    private String provinciaCombo;

    private Long poblacioCombo;

    private String paisCombo;

    private Boolean consentimiento;

    private String otros;

    private Boolean arbitro;

    private Boolean deportistaElite;

    private String movilUji;

    private String mailUji;

    private String telefonoUji;

    private String direccionUji;
    private String nombrePueblo;
    private String nombrePais;

    public DatosPersona() {
    }

    @QueryProjection
    public DatosPersona(String nombre, String apellido1, String apellido2, Long tipoIdentificacion, String identificacion, Date fechaNacimiento, String sexo, Long libroFamilia, String mail, String movil, String movilPublico, String iban, String viaCombo, String calle, String numero, String escalera, String piso, String puerta, String codPostal, String provinciaCombo, Long poblacioCombo, String paisCombo, Boolean consentimiento, String otros, Boolean arbitro, Boolean deportistaElite, String movilUji, String mailUji, String telefonoUji, String direccionUji) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.tipoIdentificacion = tipoIdentificacion;
        this.identificacion = identificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.libroFamilia = libroFamilia;
        this.mail = mail;
        this.movil = movil;
        this.movilPublico = movilPublico;
        this.iban = iban;
        this.viaCombo = viaCombo;
        this.calle = calle;
        this.numero = numero;
        this.escalera = escalera;
        this.piso = piso;
        this.puerta = puerta;
        this.codPostal = codPostal;
        this.provinciaCombo = provinciaCombo;
        this.poblacioCombo = poblacioCombo;
        this.paisCombo = paisCombo;
        this.consentimiento = consentimiento;
        this.otros = otros;
        this.arbitro = arbitro;
        this.deportistaElite = deportistaElite;
        this.movilUji = movilUji;
        this.mailUji = mailUji;
        this.telefonoUji = telefonoUji;
        this.direccionUji = direccionUji;
    }

    @QueryProjection
    public DatosPersona(String nombre, String apellido1, String apellido2, Long tipoIdentificacion, String identificacion, Date fechaNacimiento, String sexo, Long libroFamilia, String mail, String movil, String movilPublico, String iban, String viaCombo, String calle, String numero, String escalera, String piso, String puerta, String codPostal, String provinciaCombo, Long poblacioCombo, String paisCombo, Boolean consentimiento, String otros, Boolean arbitro, Boolean deportistaElite, String movilUji, String mailUji, String telefonoUji, String direccionUji, String nombrePueblo, String nombrePais) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.tipoIdentificacion = tipoIdentificacion;
        this.identificacion = identificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.libroFamilia = libroFamilia;
        this.mail = mail;
        this.movil = movil;
        this.movilPublico = movilPublico;
        this.iban = iban;
        this.viaCombo = viaCombo;
        this.calle = calle;
        this.numero = numero;
        this.escalera = escalera;
        this.piso = piso;
        this.puerta = puerta;
        this.codPostal = codPostal;
        this.provinciaCombo = provinciaCombo;
        this.poblacioCombo = poblacioCombo;
        this.paisCombo = paisCombo;
        this.consentimiento = consentimiento;
        this.otros = otros;
        this.arbitro = arbitro;
        this.deportistaElite = deportistaElite;
        this.movilUji = movilUji;
        this.mailUji = mailUji;
        this.telefonoUji = telefonoUji;
        this.direccionUji = direccionUji;
        this.nombrePueblo = nombrePueblo;
        this.nombrePais = nombrePais;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public Long getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Long tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Long getLibroFamilia() {
        return libroFamilia;
    }

    public void setLibroFamilia(Long libroFamilia) {
        this.libroFamilia = libroFamilia;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getMovilPublico() {
        return movilPublico;
    }

    public void setMovilPublico(String movilPublico) {
        this.movilPublico = movilPublico;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getViaCombo() {
        return viaCombo;
    }

    public void setViaCombo(String viaCombo) {
        this.viaCombo = viaCombo;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEscalera() {
        return escalera;
    }

    public void setEscalera(String escalera) {
        this.escalera = escalera;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getPuerta() {
        return puerta;
    }

    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getProvinciaCombo() {
        return provinciaCombo;
    }

    public void setProvinciaCombo(String provinciaCombo) {
        this.provinciaCombo = provinciaCombo;
    }

    public Long getPoblacioCombo() {
        return poblacioCombo;
    }

    public void setPoblacioCombo(Long poblacioCombo) {
        this.poblacioCombo = poblacioCombo;
    }

    public String getPaisCombo() {
        return paisCombo;
    }

    public void setPaisCombo(String paisCombo) {
        this.paisCombo = paisCombo;
    }

    public Boolean isConsentimiento() {
        return consentimiento;
    }

    public void setConsentimiento(Boolean consentimiento) {
        this.consentimiento = consentimiento;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public Boolean isArbitro() {
        return arbitro;
    }

    public void setArbitro(Boolean arbitro) {
        this.arbitro = arbitro;
    }

    public String getMovilUji() {
        return movilUji;
    }

    public void setMovilUji(String movilUji) {
        this.movilUji = movilUji;
    }

    public String getMailUji() {
        return mailUji;
    }

    public void setMailUji(String mailUji) {
        this.mailUji = mailUji;
    }

    public String getTelefonoUji() {
        return telefonoUji;
    }

    public void setTelefonoUji(String telefonoUji) {
        this.telefonoUji = telefonoUji;
    }

    public String getDireccionUji() {
        return direccionUji;
    }

    public void setDireccionUji(String direccionUji) {
        this.direccionUji = direccionUji;
    }

    public String getNombrePueblo() {
        return nombrePueblo;
    }

    public void setNombrePueblo(String nombrePueblo) {
        this.nombrePueblo = nombrePueblo;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    public Boolean isDeportistaElite() {
        if (this.deportistaElite == null) {
            return false;
        }
        return this.deportistaElite;
    }

    public void setDeportistaElite(Boolean deportistaElite) {
        this.deportistaElite = deportistaElite;
    }
}
