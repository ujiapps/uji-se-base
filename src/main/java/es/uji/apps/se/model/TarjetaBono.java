package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class TarjetaBono {
    private Long id;
    private Long tipoId;
    private Long tipoInstalacionId;
    private Long personaId;
    private Long usos;
    private Long totalUsos;
    private Long activo;
    private Long dias;
    private Long neto;
    private Long plazos;
    private String estado;
    private String nombre;
    private String observaciones;
    private String origen;
    private Date fechaInicio;
    private Date fechaFin;
    private Date fechaBaja;

    public TarjetaBono() {}

    @QueryProjection
    public TarjetaBono(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    @QueryProjection
    public TarjetaBono(Long id,
                       Long tipoId,
                       Long tipoInstalacionId,
                       Long personaId,
                       Long usos,
                       Long totalUsos,
                       Long activo,
                       Long dias,
                       Long neto,
                       Long plazos,
                       String estado,
                       String nombre,
                       String observaciones,
                       String origen,
                       Date fechaInicio,
                       Date fechaFin,
                       Date fechaBaja) {
        this.id = id;
        this.tipoId = tipoId;
        this.tipoInstalacionId = tipoInstalacionId;
        this.personaId = personaId;
        this.usos = usos;
        this.totalUsos = totalUsos;
        this.activo = activo;
        this.dias = dias;
        this.neto = neto;
        this.plazos = plazos;
        this.estado = estado;
        this.nombre = nombre;
        this.observaciones = observaciones;
        this.origen = origen;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.fechaBaja = fechaBaja;
    }



    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public Long getTipoInstalacionId() {return tipoInstalacionId;}
    public void setTipoInstalacionId(Long tipoInstalacionId) {this.tipoInstalacionId = tipoInstalacionId;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getUsos() {return usos;}
    public void setUsos(Long usos) {this.usos = usos;}

    public Long getTotalUsos() {return totalUsos;}
    public void setTotalUsos(Long totalUsos) {this.totalUsos = totalUsos;}

    public Long getActivo() {return activo;}
    public void setActivo(Long activo) {this.activo = activo;}

    public Long getDias() {return dias;}
    public void setDias(Long dias) {this.dias = dias;}

    public Long getNeto() {return neto;}
    public void setNeto(Long neto) {this.neto = neto;}

    public Long getPlazos() {return plazos;}
    public void setPlazos(Long plazos) {this.plazos = plazos;}

    public String getEstado() {return estado;}
    public void setEstado(String estado) {this.estado = estado;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observaciones) {this.observaciones = observaciones;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}
}
