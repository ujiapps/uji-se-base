package es.uji.apps.se.model;

import java.util.Date;

public class Nacimientos {
    public Nacimientos() {
    }

    private Long perId;
    private Date fecha;
    private Long sexo;

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }

}
