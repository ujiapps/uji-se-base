package es.uji.apps.se.model.domains;

public enum TipoEstadoInscripcion {
    DEFINITIVO("DEFINITIVO", 291230L),
    PENDIENTE("PENDIENTE", 291212L),
    ESPERA("ESPERA", 351482L),
    INSCRITO_MANUAL("INSCRITO MANUAL", 352029L),
    INSCRIPCION_MANUAL("INSCRIPCION MANUAL", 291270L);

    private final String nombre;
    private final Long id;

    TipoEstadoInscripcion(String nombre, Long id) {
        this.nombre = nombre;
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public Long getId() {
        return id;
    }
}
