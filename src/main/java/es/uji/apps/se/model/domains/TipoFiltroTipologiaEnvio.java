package es.uji.apps.se.model.domains;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum TipoFiltroTipologiaEnvio
{
    VINCULOS(6L),
    USUARIOS(7L),
    MONITORES(8L),
    ELITE(12L),
    MOROSO(14L);

    private final Long id;

    TipoFiltroTipologiaEnvio(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public static List<Long> getAll(){
        return Arrays.stream(TipoFiltroTipologiaEnvio.values()).map(z-> z.getId()).collect(Collectors.toList());
    }

}
