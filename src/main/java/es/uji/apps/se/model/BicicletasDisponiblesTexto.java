package es.uji.apps.se.model;

public class BicicletasDisponiblesTexto {
    private Long numBicis;
    private String texto;

    public BicicletasDisponiblesTexto() {
    }

    public Long getNumBicis() {
        return numBicis;
    }

    public void setNumBicis(Long numBicis) {
        this.numBicis = numBicis;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
