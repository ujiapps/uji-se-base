package es.uji.apps.se.model;

import java.util.Date;

public class BicicletaListaEspera {
    private Long esperaId;
    private Date fecha;
    private String observaciones;
    private Long tipoUso;
    private Long posicionLista;
    private String accion;
    private String comentario;
    private Long numBicis;
    private Long numReservasActivas;

    public BicicletaListaEspera() {
    }

    public BicicletaListaEspera(Long esperaId, Date fecha, String observaciones, Long tipoUso, Long posicionLista) {
        this.esperaId = esperaId;
        this.fecha = fecha;
        this.observaciones = observaciones;
        this.tipoUso = tipoUso;
        this.posicionLista = posicionLista;
    }

    public Long getEsperaId() {
        return esperaId;
    }

    public void setEsperaId(Long esperaId) {
        this.esperaId = esperaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getTipoUso() {
        return tipoUso;
    }

    public void setTipoUso(Long tipoUso) {
        this.tipoUso = tipoUso;
    }

    public Long getPosicionLista() {
        return posicionLista;
    }

    public void setPosicionLista(Long posicionLista) {
        this.posicionLista = posicionLista;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Long getNumBicis() {
        return numBicis;
    }

    public void setNumBicis(Long numBicis) {
        this.numBicis = numBicis;
    }

    public Long getNumReservasActivas() {
        return numReservasActivas;
    }

    public void setNumReservasActivas(Long numReservasActivas) {
        this.numReservasActivas = numReservasActivas;
    }
}
