package es.uji.apps.se.model;

import java.util.Date;

public class ReciboPendienteActividad {
    private Long id;

    private Long personaId;

    private Long ofertaId;

    private Long importe;

    private Date fecha;

    private String nombre;

    private String dto;

    private String importeTexto;

    public ReciboPendienteActividad() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getOfertaId() {return ofertaId;}
    public void setOfertaId(Long ofertaId) {this.ofertaId = ofertaId;}

    public Long getImporte() {return importe;}
    public void setImporte(Long importe) {this.importe = importe;}

    public Date getFecha() {return fecha;}
    public void setFecha(Date fecha) {this.fecha = fecha;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getDto() {return dto;}
    public void setDto(String dto) {this.dto = dto;}

    public String getImporteTexto() {return importeTexto;}
    public void setImporteTexto(String importeTexto) {this.importeTexto = importeTexto;}
}
