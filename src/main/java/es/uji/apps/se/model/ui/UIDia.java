package es.uji.apps.se.model.ui;

import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.rest.ParamUtils;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UIDia {
    private Date fecha;

    public UIDia(Date fecha) {
        this.fecha = fecha;
    }

    public UIDia(String cadena) throws ParseException {
        if (ParamUtils.isNotNull(cadena)) {
            SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");
            this.fecha = formateadorFecha.parse(cadena);
        } else {
            this.fecha = null;
        }
    }

    public UIDia(String cadena, String formato) throws ParseException {

        if (!ParamUtils.isNotNull(formato))
            formato = "dd/MM/yyyy";

        if (ParamUtils.isNotNull(cadena)) {
            SimpleDateFormat formateadorFecha = new SimpleDateFormat(formato);
            this.fecha = formateadorFecha.parse(cadena);
        } else {
            this.fecha = null;
        }
    }

    public String toYYYYMMDD() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        return dateFormat.format(fecha);
    }

    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return MessageFormat.format("{0} {1}", DateExtensions.getDayNameOfWeek(fecha), dateFormat.format(fecha));
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
