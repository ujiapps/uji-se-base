package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class AccesosPorPerfil implements Serializable {

    private Long vinculoId;
    private String vinculo;
    private Long countZonaAireLibre;
    private Long countZonaPabellon;
    private Long countZonaPiscina;
    private Long countZonaRaquetas;
    private Long countTotal;


    public AccesosPorPerfil() {
    }

    @QueryProjection
    public AccesosPorPerfil(Long vinculoId, String vinculo) {
        this.vinculoId = vinculoId;
        this.vinculo = vinculo;
        this.countZonaAireLibre = 0L;
        this.countZonaPabellon = 0L;
        this.countZonaPiscina = 0L;
        this.countZonaRaquetas = 0L;
        this.countTotal = 0L;
    }

    public Long getVinculoId() {
        return vinculoId;
    }

    public void setVinculoId(Long vinculoId) {
        this.vinculoId = vinculoId;
    }

    public String getVinculo() {
        return vinculo;
    }

    public void setVinculo(String vinculo) {
        this.vinculo = vinculo;
    }

    public Long getCountZonaAireLibre() {
        return countZonaAireLibre;
    }

    public void setCountZonaAireLibre(Long countZonaAireLibre) {
        this.countZonaAireLibre = countZonaAireLibre;
    }

    public Long getCountZonaPabellon() {
        return countZonaPabellon;
    }

    public void setCountZonaPabellon(Long countZonaPabellon) {
        this.countZonaPabellon = countZonaPabellon;
    }

    public Long getCountZonaPiscina() {
        return countZonaPiscina;
    }

    public void setCountZonaPiscina(Long countZonaPiscina) {
        this.countZonaPiscina = countZonaPiscina;
    }

    public Long getCountZonaRaquetas() {
        return countZonaRaquetas;
    }

    public void setCountZonaRaquetas(Long countZonaRaquetas) {
        this.countZonaRaquetas = countZonaRaquetas;
    }

    public Long getCountTotal() {
        return countTotal;
    }

    public void setCountTotal(Long countTotal) {
        this.countTotal = countTotal;
    }
}
