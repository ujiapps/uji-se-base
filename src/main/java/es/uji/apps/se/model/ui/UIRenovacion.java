package es.uji.apps.se.model.ui;

import es.uji.apps.se.dto.OfertaDTO;

public class UIRenovacion
{
    private Long id;
    private String actividadNombre;
    private String grupoNombre;
    private Boolean tieneRenovacion;
    private Boolean inscrito;
    private Boolean devolucion;

    public UIRenovacion(OfertaDTO ofertaDTO) {
        this.id = ofertaDTO.getId();
        this.actividadNombre = ofertaDTO.getActividad().getNombre();
        this.grupoNombre = ofertaDTO.getNombre();
        this.tieneRenovacion = Boolean.FALSE;
        this.inscrito = Boolean.FALSE;
        this.devolucion = Boolean.FALSE;
    }
    public Boolean getTieneRenovacion()
    {
        return tieneRenovacion;
    }

    public Boolean isTieneRenovacion(){
        return tieneRenovacion;
    }

    public void setTieneRenovacion(Boolean tieneRenovacion)
    {
        this.tieneRenovacion = tieneRenovacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividadNombre() {
        return actividadNombre;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }

    public String getGrupoNombre() {
        return grupoNombre;
    }

    public void setGrupoNombre(String grupoNombre) {
        this.grupoNombre = grupoNombre;
    }

    public Boolean getInscrito() {
        return inscrito;
    }

    public void setInscrito(Boolean inscrito) {
        this.inscrito = inscrito;
    }

    public Boolean isInscrito(){
        return inscrito;
    }

    public Boolean getDevolucion() {
        return devolucion;
    }

    public Boolean isDevolucion() {
        return devolucion;
    }

    public void setDevolucion(Boolean devolucion) {
        this.devolucion = devolucion;
    }
}
