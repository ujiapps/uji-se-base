package es.uji.apps.se.model.ui;


import es.uji.apps.se.dto.TarjetaDeportivaClaseDTO;

public class UITarjetaDeportivaClase {
    private Long id;
    private String nombre;
    private String nombrePersona;
    private Boolean asiste;
    private Long calendarioClaseId;

    public UITarjetaDeportivaClase(TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO) {
        this.id = tarjetaDeportivaClaseDTO.getId();
        this.nombre = tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getTarjetaDeportivaTipo().getNombre();
        this.nombrePersona = tarjetaDeportivaClaseDTO.getTarjetaDeportiva().getPersonaUji().getApellidosNombre();
        this.asiste = tarjetaDeportivaClaseDTO.isAsiste();
        this.calendarioClaseId = tarjetaDeportivaClaseDTO.getCalendarioClase().getId();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public Boolean getAsiste() {
        return asiste;
    }

    public void setAsiste(Boolean asiste) {
        this.asiste = asiste;
    }

    public Boolean isAsiste() {
        return asiste;
    }

    public Long getCalendarioClaseId() {
        return calendarioClaseId;
    }

    public void setCalendarioClaseId(Long calendarioClaseId) {
        this.calendarioClaseId = calendarioClaseId;
    }
}
