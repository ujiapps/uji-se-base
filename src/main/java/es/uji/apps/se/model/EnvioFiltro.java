package es.uji.apps.se.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.model.domains.TipoFiltroEnvio;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnvioFiltro implements Serializable {
    private Long id;
    private Long envio;
    private Boolean todos;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Long> tarjetas;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Long> bonos;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/Madrid")
    private Date fechaInicioClases;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/Madrid")
    private Date fechaFinClases;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = "Europe/Madrid")
    private Date horaInicioClases;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = "Europe/Madrid")
    private Date horaFinClases;

    private Boolean asisten;
    private Boolean noAsisten;

    private List<FiltroClasesInstalacion> filtroClasesInstalaciones;

    private List<FiltroClasesClases> filtroClasesClases;

    private List<FiltroPersonas> filtroPersonas;

    private List<FiltroEnvioActividades> filtroActividades;

    private List<FiltroAlquileres> filtroAlquileres;

    private FiltroEnvioBicicletas filtroBicicletas;

    public EnvioFiltro() {
    }

    public EnvioFiltro(Long envioId, List<EnvioFiltroDTO> envioFiltros) throws JSONException {

        this.envio = envioId;
        this.tarjetas = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.TARJETA.getId())).map(EnvioFiltroDTO::getKey).collect(Collectors.toList());
        this.bonos = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.BONOS.getId())).map(EnvioFiltroDTO::getKey).collect(Collectors.toList());

        List<Date> inicios = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.CLASES.getId())).map(EnvioFiltroDTO::getFechaInicio).collect(Collectors.toList());
        if (!inicios.isEmpty()) {
            this.fechaInicioClases = inicios.get(0);
        }
        List<Date> fin = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.CLASES.getId())).map(EnvioFiltroDTO::getFechaFin).collect(Collectors.toList());
        if (!fin.isEmpty()) {
            this.fechaFinClases = fin.get(0);
        }

        List<Date> horaInicios = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.CLASES.getId())).map(EnvioFiltroDTO::getHoraInicio).collect(Collectors.toList());
        if (!horaInicios.isEmpty()) {
            this.horaInicioClases = horaInicios.get(0);
        }

        List<Date> horaFin = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.CLASES.getId())).map(EnvioFiltroDTO::getHoraFin).collect(Collectors.toList());
        if (!horaFin.isEmpty()) {
            this.horaFinClases = horaFin.get(0);
        }

        List<String> value = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.CLASES.getId())).map(EnvioFiltroDTO::getValue).collect(Collectors.toList());
        if (!value.isEmpty()) {
            JSONArray array = new JSONArray(value.get(0));

            if (ParamUtils.isNotNull(array)) {
                JSONObject obj = array.getJSONObject(0);
                this.asisten = ((Boolean) obj.get("asisten"));
                this.noAsisten = ((Boolean) obj.get("noAsisten"));
            }
        }

        this.filtroClasesInstalaciones = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.CLASESINSTALACIONES.getId())).map(filtro -> {
            FiltroClasesInstalacion filtroClasesInstalacion = new FiltroClasesInstalacion();
            filtroClasesInstalacion.setInstalacion(filtro.getKey());
            filtroClasesInstalacion.setFechaInicioInstalacion(filtro.getFechaInicio());
            filtroClasesInstalacion.setFechaFinInstalacion(filtro.getFechaFin());
            filtroClasesInstalacion.setHoraInicioInstalacion(filtro.getHoraInicio());
            filtroClasesInstalacion.setHoraFinInstalacion(filtro.getHoraFin());

            if (ParamUtils.isNotNull(filtro.getValue())) {
                JSONArray array = null;
                try {
                    array = new JSONArray(filtro.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (ParamUtils.isNotNull(array)) {
                        JSONObject obj = array.getJSONObject(0);
                        filtroClasesInstalacion.setAsisten((Boolean) obj.get("asisten"));
                        filtroClasesInstalacion.setNoAsisten((Boolean) obj.get("noAsisten"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            return filtroClasesInstalacion;

        }).collect(Collectors.toList());

        this.filtroClasesClases = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.CLASESCLASES.getId())).map(filtro -> {
            FiltroClasesClases filtroClasesClases = new FiltroClasesClases();
            filtroClasesClases.setClase(filtro.getKey());
            filtroClasesClases.setFechaInicioFiltroClases(filtro.getFechaInicio());
            filtroClasesClases.setFechaFinFiltroClases(filtro.getFechaFin());
            filtroClasesClases.setHoraInicioFiltroClases(filtro.getHoraInicio());
            filtroClasesClases.setHoraFinFiltroClases(filtro.getHoraFin());

            if (ParamUtils.isNotNull(filtro.getValue())) {
                JSONArray array = null;
                try {
                    array = new JSONArray(filtro.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (ParamUtils.isNotNull(array)) {
                        JSONObject obj = array.getJSONObject(0);
                        filtroClasesClases.setAsisten((Boolean) obj.get("asisten"));
                        filtroClasesClases.setNoAsisten((Boolean) obj.get("noAsisten"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return filtroClasesClases;

        }).collect(Collectors.toList());

        this.filtroPersonas = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.PERSONAS.getId())).map(filtro -> {
            FiltroPersonas filtroPersonas = new FiltroPersonas();
            filtroPersonas.setEmail(filtro.getValue());
            return filtroPersonas;
        }).collect(Collectors.toList());

        this.filtroActividades = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.ACTIVIDADES.getId())).map(filtro -> {
            FiltroEnvioActividades filtroEnvioActividades = new FiltroEnvioActividades();
            filtroEnvioActividades.setActividad(filtro.getKey());

            if (ParamUtils.isNotNull(filtro.getValue())) {
                JSONArray array = null;
                try {
                    array = new JSONArray(filtro.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (ParamUtils.isNotNull(array)) {
                        JSONObject obj = array.getJSONObject(0);
                        filtroEnvioActividades.setGrupo(ParamUtils.parseLong(String.valueOf((Integer) obj.get("grupo"))));
                        filtroEnvioActividades.setInscritos((Boolean) obj.get("inscritos"));
                        filtroEnvioActividades.setApto((Boolean) obj.get("apto"));
                        filtroEnvioActividades.setNoApto((Boolean) obj.get("noApto"));
                        filtroEnvioActividades.setEspera((Boolean) obj.get("espera"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return filtroEnvioActividades;
        }).collect(Collectors.toList());

        this.filtroAlquileres = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.ALQUILER.getId())).map(filtro -> {
            FiltroAlquileres filtroAlquileres = new FiltroAlquileres();
            filtroAlquileres.setAlquiler(filtro.getKey());
            return filtroAlquileres;
        }).collect(Collectors.toList());

        List<FiltroEnvioBicicletas> listaFiltroEnvioBicicletas;
        listaFiltroEnvioBicicletas = envioFiltros.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroEnvio.BICICLETA.getId())).map(filtro -> {
            FiltroEnvioBicicletas filtroEnvioBicicletas = new FiltroEnvioBicicletas();

            if (ParamUtils.isNotNull(filtro.getValue())) {
                JSONArray array = null;
                try {
                    array = new JSONArray(filtro.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (ParamUtils.isNotNull(array)) {
                        JSONObject obj = array.getJSONObject(0);
                        filtroEnvioBicicletas.setReserva((Boolean) obj.get("reserva"));
                        filtroEnvioBicicletas.setEspera((Boolean) obj.get("espera"));
                        filtroEnvioBicicletas.setDevolucion((Boolean) obj.get("devolucion"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return filtroEnvioBicicletas;
        }).collect(Collectors.toList());

        if (listaFiltroEnvioBicicletas.isEmpty()) {
            FiltroEnvioBicicletas filtroEnvioBicicletas = new FiltroEnvioBicicletas();
            filtroEnvioBicicletas.setEspera(Boolean.FALSE);
            filtroEnvioBicicletas.setReserva(Boolean.FALSE);
            filtroEnvioBicicletas.setDevolucion(Boolean.FALSE);
            this.filtroBicicletas = filtroEnvioBicicletas;
        } else {
            this.filtroBicicletas = listaFiltroEnvioBicicletas.get(0);
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEnvio() {
        return envio;
    }

    public void setEnvio(Long envio) {
        this.envio = envio;
    }

    public List<Long> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(List<Long> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public List<Long> getBonos() {
        return bonos;
    }

    public void setBonos(List<Long> bonos) {
        this.bonos = bonos;
    }

    public Date getFechaInicioClases() {
        return fechaInicioClases;
    }

    public void setFechaInicioClases(Date fechaInicioClases) {
        this.fechaInicioClases = fechaInicioClases;
    }

    public Date getFechaFinClases() {
        return fechaFinClases;
    }

    public void setFechaFinClases(Date fechaFinClases) {
        this.fechaFinClases = fechaFinClases;
    }

    public List<FiltroClasesInstalacion> getFiltroClasesInstalaciones() {
        return filtroClasesInstalaciones;
    }

    public void setFiltroClasesInstalaciones(List<FiltroClasesInstalacion> filtroClasesInstalaciones) {
        this.filtroClasesInstalaciones = filtroClasesInstalaciones;
    }

    public List<FiltroClasesClases> getFiltroClasesClases() {
        return filtroClasesClases;
    }

    public void setFiltroClasesClases(List<FiltroClasesClases> filtroClasesClases) {
        this.filtroClasesClases = filtroClasesClases;
    }

    public Date getHoraInicioClases() {
        return horaInicioClases;
    }

    public void setHoraInicioClases(Date horaInicioClases) {
        this.horaInicioClases = horaInicioClases;
    }

    public Date getHoraFinClases() {
        return horaFinClases;
    }

    public void setHoraFinClases(Date horaFinClases) {
        this.horaFinClases = horaFinClases;
    }

    public List<FiltroPersonas> getFiltroPersonas() {
        return filtroPersonas;
    }

    public void setFiltroPersonas(List<FiltroPersonas> filtroPersonas) {
        this.filtroPersonas = filtroPersonas;
    }

    public List<FiltroEnvioActividades> getFiltroActividades() {
        return filtroActividades;
    }

    public void setFiltroActividades(List<FiltroEnvioActividades> filtroActividades) {
        this.filtroActividades = filtroActividades;
    }

    public List<FiltroAlquileres> getFiltroAlquileres() {
        return filtroAlquileres;
    }

    public void setFiltroAlquileres(List<FiltroAlquileres> filtroAlquileres) {
        this.filtroAlquileres = filtroAlquileres;
    }

    public Boolean isTodos() {
        return todos;
    }

    public void setTodos(Boolean todos) {
        this.todos = todos;
    }

    public Boolean isAsisten() {
        return asisten;
    }

    public void setAsisten(Boolean asisten) {
        this.asisten = asisten;
    }

    public Boolean isNoAsisten() {
        return noAsisten;
    }

    public void setNoAsisten(Boolean noAsisten) {
        this.noAsisten = noAsisten;
    }

    public FiltroEnvioBicicletas getFiltroBicicletas() {
        return filtroBicicletas;
    }

    public void setFiltroBicicletas(FiltroEnvioBicicletas filtroBicicletas) {
        this.filtroBicicletas = filtroBicicletas;
    }
}
