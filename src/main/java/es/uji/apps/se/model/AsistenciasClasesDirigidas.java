package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;

import javax.ws.rs.DefaultValue;
import java.io.Serializable;

public class AsistenciasClasesDirigidas implements Serializable {

    private Long actividadId;
    private String actividad;
    private String horario;
    private int plazas;
    private Long plazasTotales;
    private int reservas;
    private int asistencias;
    private Float porcentajeReservas;
    private Float porcentajeAsistenciaReservas;
    private Float porcentajeAsistenciaOferta;

    public AsistenciasClasesDirigidas() {
    }

    @QueryProjection
    public AsistenciasClasesDirigidas(Long actividadId, String actividad, String horario,  int plazas,Long plazasTotales, Long reservas, Long asistencias) {
        this.actividadId = actividadId;
        this.actividad = actividad;
        this.horario = horario;
        this.plazas = (ParamUtils.isNotNull(plazas)) ? plazas : 0;
        this.plazasTotales = plazasTotales;
        this.reservas = Math.toIntExact((ParamUtils.isNotNull(reservas)) ? reservas : 0);
        this.asistencias = Math.toIntExact((ParamUtils.isNotNull(asistencias)) ? asistencias : 0);
        this.porcentajeReservas = (this.plazas != 0) ? (Float.valueOf(this.reservas) * 100) / Float.valueOf(this.plazas) : 0;
        this.porcentajeAsistenciaReservas = (this.reservas != 0) ? (Float.valueOf(this.asistencias) * 100) / Float.valueOf(this.reservas) : 0;
        this.porcentajeAsistenciaOferta = (this.plazas != 0) ? (Float.valueOf(this.asistencias) * 100) / Float.valueOf(this.plazas) : 0;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public int getPlazas() {
        return plazas;
    }

    public void setPlazas(int plazas) {
        this.plazas = plazas;
    }

    public Long getPlazasTotales() {
        return plazasTotales;
    }

    public void setPlazasTotales(Long plazasTotales) {
        this.plazasTotales = plazasTotales;
    }

    public int getReservas() {
        return reservas;
    }

    public void setReservas(int reservas) {
        this.reservas = reservas;
    }

    public int getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(int asistencias) {
        this.asistencias = asistencias;
    }

    public Float getPorcentajeReservas() {
        return porcentajeReservas;
    }

    public void setPorcentajeReservas(Float porcentajeReservas) {
        this.porcentajeReservas = porcentajeReservas;
    }

    public Float getPorcentajeAsistenciaReservas() {
        return porcentajeAsistenciaReservas;
    }

    public void setPorcentajeAsistenciaReservas(Float porcentajeAsistenciaReservas) {
        this.porcentajeAsistenciaReservas = porcentajeAsistenciaReservas;
    }

    public Float getPorcentajeAsistenciaOferta() {
        return porcentajeAsistenciaOferta;
    }

    public void setPorcentajeAsistenciaOferta(Float porcentajeAsistenciaOferta) {
        this.porcentajeAsistenciaOferta = porcentajeAsistenciaOferta;
    }
}
