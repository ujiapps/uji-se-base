package es.uji.apps.se.model;

import java.util.Date;

public class HistoricoInscripcion {
    private String periodo;
    private String actividadNombre;
    private String grupo;
    private String tipo;
    private Date fechaInscripcion;
    private Long estadoId;
    private String estadoNombre;
    private String observaciones;
    private String origen;
    private Long numeroVeces;
    private Date fechaBaja;
    private Date fechaAccion;
    private String usuarioAccion;
    private Long apto;

    public HistoricoInscripcion() {}

    public String getPeriodo() {return periodo;}
    public void setPeriodo(String periodo) {this.periodo = periodo;}

    public String getActividadNombre() {return actividadNombre;}
    public void setActividadNombre(String actividadNombre) {this.actividadNombre = actividadNombre;}

    public String getGrupo() {return grupo;}
    public void setGrupo(String grupo) {this.grupo = grupo;}

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public Date getFechaInscripcion() {return fechaInscripcion;}
    public void setFechaInscripcion(Date fechaInscripcion) {this.fechaInscripcion = fechaInscripcion;}

    public Long getEstadoId() {return estadoId;}
    public void setEstadoId(Long estadoId) {this.estadoId = estadoId;}

    public String getEstadoNombre() {return estadoNombre;}
    public void setEstadoNombre(String estadoNombre) {this.estadoNombre = estadoNombre;}

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observaciones) {this.observaciones = observaciones;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public Long getNumeroVeces() {return numeroVeces;}
    public void setNumeroVeces(Long numeroVeces) {this.numeroVeces = numeroVeces;}

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}

    public Date getFechaAccion() {return fechaAccion;}
    public void setFechaAccion(Date fechaAccion) {this.fechaAccion = fechaAccion;}

    public String getUsuarioAccion() {return usuarioAccion;}
    public void setUsuarioAccion(String usuarioAccion) {this.usuarioAccion = usuarioAccion;}

    public Long getApto() {return apto;}
    public void setApto(Long apto) {this.apto = apto;}
}
