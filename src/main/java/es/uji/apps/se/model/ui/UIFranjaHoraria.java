package es.uji.apps.se.model.ui;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class UIFranjaHoraria
{
    private Long id;
    private String horaInicial;
    private String horaFinal;
    private List<UIEvento> eventos;

    public UIFranjaHoraria(Long id, String horaInicial, String horaFinal)
    {
        this.id = id;
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
        this.eventos = new ArrayList<>();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getHoraInicial()
    {
        return horaInicial;
    }

    public void setHoraInicial(String horaInicial)
    {
        this.horaInicial = horaInicial;
    }

    public String getHoraFinal()
    {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal)
    {
        this.horaFinal = horaFinal;
    }

    public List<UIEvento> getEventos()
    {
        return eventos;
    }

    public void setEventos(List<UIEvento> eventos)
    {
        this.eventos = eventos;
    }

    @Override
    public String toString()
    {
        return MessageFormat.format("{0} a {1} hores", horaInicial, horaFinal);
    }

    public Boolean solapa(String horaInicioEvento)
    {
        Long horaInicioFranja = getHoraAsLong(horaInicial);
        Long horaFinFranja = getHoraAsLong(horaFinal);
        return horaInicioFranja <= getHoraAsLong(horaInicioEvento) && horaFinFranja > getHoraAsLong(horaInicioEvento);
    }

    protected Long getHoraAsLong(String hora)
    {
        try
        {
            String[] parsed = hora.split(":");
            return new Long(parsed[0]) * 100 + new Long(parsed[1]);
        }
        catch (Exception e)
        {
            return 0L;
        }
    }



}
