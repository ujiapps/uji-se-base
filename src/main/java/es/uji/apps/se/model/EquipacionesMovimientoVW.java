package es.uji.apps.se.model;

import java.util.Date;

public class EquipacionesMovimientoVW {
    private Long id;
    private Long equipacionId;
    private Long cantidad;
    private Long contabiliza;
    private Long equipacionInscripcionId;
    private Long personaId;
    private Long movimientoId;
    private String movimiento;
    private String equipacionTipoNombre;
    private String equipacionModeloNombre;
    private String equipacionGeneroNombre;
    private String identificacion;
    private String observaciones;
    private String talla;
    private Long dorsal;
    private String nombre;

    private String personaNombre;
    private Date fecha;

    public EquipacionesMovimientoVW() {}

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getEquipacionId() { return equipacionId; }
    public void setEquipacionId(Long equipacionId) { this.equipacionId = equipacionId; }

    public Long getCantidad() { return cantidad; }
    public void setCantidad(Long cantidad) { this.cantidad = cantidad; }

    public Long getContabiliza() { return contabiliza; }
    public void setContabiliza(Long contabiliza) { this.contabiliza = contabiliza; }

    public Long getEquipacionInscripcionId() { return equipacionInscripcionId; }
    public void setEquipacionInscripcionId(Long equipacionInscripcionId) { this.equipacionInscripcionId = equipacionInscripcionId; }

    public Long getPersonaId() { return personaId; }
    public void setPersonaId(Long personaId) { this.personaId = personaId; }

    public Long getMovimientoId() { return movimientoId; }
    public void setMovimientoId(Long movimientoId) { this.movimientoId = movimientoId; }

    public String getMovimiento() { return movimiento; }
    public void setMovimiento(String movimiento) { this.movimiento = movimiento; }

    public String getIdentificacion() { return identificacion; }
    public void setIdentificacion(String identificacion) { this.identificacion = identificacion; }

    public String getObservaciones() { return observaciones; }
    public void setObservaciones(String observaciones) { this.observaciones = observaciones; }

    public String getTalla() { return talla; }
    public void setTalla(String talla) { this.talla = talla; }

    public Long getDorsal() { return dorsal; }
    public void setDorsal(Long dorsal) { this.dorsal = dorsal; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getEquipacionTipoNombre() { return equipacionTipoNombre; }
    public void setEquipacionTipoNombre(String equipacionTipoNombre) { this.equipacionTipoNombre = equipacionTipoNombre; }

    public String getEquipacionModeloNombre() { return equipacionModeloNombre; }
    public void setEquipacionModeloNombre(String equipacionModeloNombre) { this.equipacionModeloNombre = equipacionModeloNombre; }

    public String getEquipacionGeneroNombre() { return equipacionGeneroNombre; }
    public void setEquipacionGeneroNombre(String equipacionGeneroNombre) { this.equipacionGeneroNombre = equipacionGeneroNombre; }

    public String getPersonaNombre() { return personaNombre; }

    public void setPersonaNombre(String personaNombre) { this.personaNombre = personaNombre; }

    public Date getFecha() { return fecha; }
    public void setFecha(Date fecha) { this.fecha = fecha; }
}
