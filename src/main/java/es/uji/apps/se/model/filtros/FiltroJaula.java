package es.uji.apps.se.model.filtros;

import es.uji.commons.rest.ParamUtils;

import java.util.Calendar;
import java.util.Date;

public class FiltroJaula {

    private Date fechaInicioDesde;
    private Date fechaInicioHasta;
    private Date fechaFinDesde;
    private Date fechaFinHasta;
    private Date fechaLlaveroDesde;
    private Date fechaLlaveroHasta;
    private String busqueda;

    public FiltroJaula(String busqueda, Date fechaInicioDesde, Date fechaInicioHasta, Date fechaFinDesde, Date fechaFinHasta,
                       Date fechaLlaveroDesde, Date fechaLlaveroHasta) {
        this.fechaInicioDesde = fechaInicioDesde;
        if (ParamUtils.isNotNull(fechaInicioHasta)) {
            Calendar calendarFechaInicioHasta = Calendar.getInstance();
            calendarFechaInicioHasta.setTime(fechaInicioHasta);
            calendarFechaInicioHasta.add(Calendar.DATE, 1);
            this.fechaInicioHasta = calendarFechaInicioHasta.getTime();
        }
        this.fechaFinDesde = fechaFinDesde;
        if (ParamUtils.isNotNull(fechaFinHasta)) {
            Calendar calendarFechaFinHasta = Calendar.getInstance();
            calendarFechaFinHasta.setTime(fechaFinHasta);
            calendarFechaFinHasta.add(Calendar.DATE, 1);
            this.fechaFinHasta = calendarFechaFinHasta.getTime();
        }
        this.fechaLlaveroDesde = fechaLlaveroDesde;
        if (ParamUtils.isNotNull(fechaLlaveroHasta)) {
            Calendar calendarFechaLlaveroHasta = Calendar.getInstance();
            calendarFechaLlaveroHasta.setTime(fechaLlaveroHasta);
            calendarFechaLlaveroHasta.add(Calendar.DATE, 1);
            this.fechaLlaveroHasta = calendarFechaLlaveroHasta.getTime();
        }

        this.busqueda = busqueda;
    }

    public Date getFechaInicioDesde() {
        return fechaInicioDesde;
    }

    public void setFechaInicioDesde(Date fechaInicioDesde) {
        this.fechaInicioDesde = fechaInicioDesde;
    }

    public Date getFechaInicioHasta() {
        return fechaInicioHasta;
    }

    public void setFechaInicioHasta(Date fechaInicioHasta) {
        this.fechaInicioHasta = fechaInicioHasta;
    }

    public Date getFechaFinDesde() {
        return fechaFinDesde;
    }

    public void setFechaFinDesde(Date fechaFinDesde) {
        this.fechaFinDesde = fechaFinDesde;
    }

    public Date getFechaFinHasta() {
        return fechaFinHasta;
    }

    public void setFechaFinHasta(Date fechaFinHasta) {
        this.fechaFinHasta = fechaFinHasta;
    }

    public Date getFechaLlaveroDesde() {
        return fechaLlaveroDesde;
    }

    public void setFechaLlaveroDesde(Date fechaLlaveroDesde) {
        this.fechaLlaveroDesde = fechaLlaveroDesde;
    }

    public Date getFechaLlaveroHasta() {
        return fechaLlaveroHasta;
    }

    public void setFechaLlaveroHasta(Date fechaLlaveroHasta) {
        this.fechaLlaveroHasta = fechaLlaveroHasta;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }
}
