package es.uji.apps.se.model;

import java.util.Date;

public class DetalleHistoricoTarjetaBono {
    private Long carnetId;
    private String operacion;
    private Date fechaInicio;
    private Date fechaFin;
    private Date fechaBaja;
    private Date fecha;
    private String nombre;
    private String observaciones;

    public DetalleHistoricoTarjetaBono() {}

    public DetalleHistoricoTarjetaBono(Long carnetId, String operacion, Date fechaInicio, Date fechaFin, Date fechaBaja, Date fecha, String nombre, String observaciones) {
        this.carnetId = carnetId;
        this.operacion = operacion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.fechaBaja = fechaBaja;
        this.fecha = fecha;
        this.nombre = nombre;
        this.observaciones = observaciones;
    }

    public Long getCarnetId() {return carnetId;}
    public void setCarnetId(Long carnetId) {this.carnetId = carnetId;}

    public String getOperacion() {return operacion;}
    public void setOperacion(String operacion) {this.operacion = operacion;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}

    public Date getFecha() {return fecha;}
    public void setFecha(Date fecha) {this.fecha = fecha;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observaciones) {this.observaciones = observaciones;}
}
