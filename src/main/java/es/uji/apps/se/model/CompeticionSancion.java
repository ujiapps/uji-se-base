package es.uji.apps.se.model;

public class CompeticionSancion {
    private Long id;
    private Long miembro;
    private Long partido;
    private String tarjetas;

    public CompeticionSancion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMiembro() {
        return miembro;
    }

    public void setMiembro(Long miembro) {
        this.miembro = miembro;
    }

    public Long getPartido() {
        return partido;
    }

    public void setPartido(Long partido) {
        this.partido = partido;
    }

    public String getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(String tarjetas) {
        this.tarjetas = tarjetas;
    }
}
