package es.uji.apps.se.model.ui;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

public class UICalendario {
    private LocalDate hoy;
    private List<String> diasSemana;
    private String nameMesActual;
    private int mesActual;
    private int mes;
    private int numDias;
    private int anyo;
    private int inicioMes;

    static final int ANYO_GENERICO = 2021;

    private String getMesNombre (Integer mes, String idioma){

        Map<String, String[]> nombreMeses = new HashMap<>();
        nombreMeses.put("es", new String[]{"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"});
        nombreMeses.put("ca", new String[] {"Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"});
        nombreMeses.put("en", new String[]  {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"});

        return nombreMeses.get(idioma)[mes];
    }

    public UICalendario(LocalDate fecha, String idioma) {

        List<Integer> dias = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        this.diasSemana = dias.stream().map(l -> {
            LocalDate dia = LocalDate.of(ANYO_GENERICO, 2, l);
            return dia.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.forLanguageTag(idioma));
        }).collect(Collectors.toList());

        this.hoy = fecha;

        this.nameMesActual = getMesNombre(this.hoy.getMonth().getValue()-1, idioma);
        this.mesActual = this.hoy.getMonthValue();
        this.mes = this.hoy.getMonthValue();
        this.numDias = this.hoy.lengthOfMonth();
        this.anyo = this.hoy.getYear();
        this.inicioMes = this.hoy.withDayOfMonth(1).getDayOfWeek().getValue();
    }

    public UICalendario(Integer mes, String idioma) {

        List<Integer> dias = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        this.diasSemana = dias.stream().map(l -> {
            LocalDate dia = LocalDate.of(ANYO_GENERICO, 2, l);
            return dia.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.forLanguageTag(idioma));
        }).collect(Collectors.toList());

        this.hoy = LocalDate.now();
        this.nameMesActual = getMesNombre(mes-1, idioma);
        this.mesActual = this.hoy.getMonthValue();
        this.mes = mes;
        this.numDias = LocalDate.of(ANYO_GENERICO, mes, 1).lengthOfMonth();
        this.anyo = (mes==1 && mes != this.mesActual)?this.hoy.getYear()+1:this.hoy.getYear();
        this.inicioMes = LocalDate.of(this.anyo, mes, 1).getDayOfWeek().getValue();
    }

    public LocalDate getHoy() {
        return hoy;
    }

    public void setHoy(LocalDate hoy) {
        this.hoy = hoy;
    }

    public String getNameMesActual() {
        return nameMesActual;
    }

    public void setNameMesActual(String nameMesActual) {
        this.nameMesActual = nameMesActual;
    }

    public int getMesActual() {
        return mesActual;
    }

    public void setMesActual(int mesActual) {
        this.mesActual = mesActual;
    }

    public List<String> getDiasSemana() {
        return diasSemana;
    }

    public void setDiasSemana(List<String> diasSemana) {
        this.diasSemana = diasSemana;
    }

    public int getNumDias() {
        return numDias;
    }

    public void setNumDias(int numDias) {
        this.numDias = numDias;
    }

    public int getInicioMes() {
        return inicioMes;
    }

    public void setInicioMes(int inicioMes) {
        this.inicioMes = inicioMes;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }
}
