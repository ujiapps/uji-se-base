package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;
import java.util.Date;

public class AsistenciaCursos implements Serializable {
    private String nombre;
    private Long id;
    private Date fechaIni;

    public AsistenciaCursos() {
    }

    @QueryProjection
    public AsistenciaCursos(String nombre, Long id, Date fechaIni) {
        this.nombre = nombre;
        this.id = id;
        this.fechaIni = fechaIni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }
}
