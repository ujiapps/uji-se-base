package es.uji.apps.se.model.domains;

public enum TipoTemporalidad {
    ANUAL(1L, "Anual"),
    SEMESTRAL(2L, "Semestral");

    private Long id;
    private String tipo;

    TipoTemporalidad(Long id, String tipo) {
        this.id = id;
        this.tipo = tipo;
    }
    
    public String getTipo() { return this.tipo; }
    public Long getId() { return this.id; }
    
    public static String getTipoById(Long id) {
        if (TipoTemporalidad.ANUAL.getId().equals(id)) return ANUAL.getTipo(); 
        if (TipoTemporalidad.SEMESTRAL.getId().equals(id)) return SEMESTRAL.getTipo();
        return "";
    }
}