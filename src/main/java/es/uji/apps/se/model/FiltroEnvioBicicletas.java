package es.uji.apps.se.model;

public class FiltroEnvioBicicletas {
    private Boolean reserva;
    private Boolean espera;
    private Boolean devolucion;

    public FiltroEnvioBicicletas() {
    }

    public Boolean isReserva() {
        return reserva;
    }

    public void setReserva(Boolean reserva) {
        this.reserva = reserva;
    }

    public Boolean isEspera() {
        return espera;
    }

    public void setEspera(Boolean espera) {
        this.espera = espera;
    }

    public Boolean isDevolucion() {
        return devolucion;
    }

    public void setDevolucion(Boolean devolucion) {
        this.devolucion = devolucion;
    }
}
