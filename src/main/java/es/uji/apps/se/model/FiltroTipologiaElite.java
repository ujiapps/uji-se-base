package es.uji.apps.se.model;

public class FiltroTipologiaElite {
    private Boolean elite;
    private Integer cursoAcademico;

    public FiltroTipologiaElite() {
    }

//    public Boolean getElite() {
//        return elite;
//    }

    public Boolean isElite() {
        return elite;
    }

    public void setElite(Boolean elite) {
        this.elite = elite;
    }

    public Integer getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Integer cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }
}
