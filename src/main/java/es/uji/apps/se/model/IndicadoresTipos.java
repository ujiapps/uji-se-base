package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

public class IndicadoresTipos {

    private Long id;
    private String nombre;

    public IndicadoresTipos() { }

    @QueryProjection
    public IndicadoresTipos(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
}