package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class OcupacionEspacio implements Serializable {

    private Long instalacionGrupoId;
    private String instalacionGrupoNombre;
    private Float horas;
    private Float horasLibres;
    private Float porcentajeHoras;
    private Float porcentajeHorasLibres;

    public OcupacionEspacio() {
    }

    @QueryProjection
    public OcupacionEspacio(Long instalacionGrupoId, String instalacionGrupoNombre) {
        this.instalacionGrupoId = instalacionGrupoId;
        this.instalacionGrupoNombre = instalacionGrupoNombre;
        this.horas = 0F;
        this.horasLibres = 0F;
        this.porcentajeHoras = 0F;
        this.porcentajeHorasLibres = 0F;
    }

    @QueryProjection
    public OcupacionEspacio(String instalacionGrupoNombre, Float horas, Float horasLibres, Float porcentajeHoras, Float porcentajeHorasLibres) {
        this.instalacionGrupoNombre = instalacionGrupoNombre;
        this.horas = horas;
        this.horasLibres = horasLibres;
        this.porcentajeHoras = porcentajeHoras;
        this.porcentajeHorasLibres = porcentajeHorasLibres;
    }

    public String getInstalacionGrupoNombre() {
        return instalacionGrupoNombre;
    }

    public void setInstalacionGrupoNombre(String instalacionGrupoNombre) {
        this.instalacionGrupoNombre = instalacionGrupoNombre;
    }

    public Float getHoras() {
        return horas;
    }

    public void setHoras(Float horas) {
        this.horas = horas;
    }

    public Float getHorasLibres() {
        return horasLibres;
    }

    public void setHorasLibres(Float horasLibres) {
        this.horasLibres = horasLibres;
    }

    public Float getPorcentajeHoras() {
        return porcentajeHoras;
    }

    public void setPorcentajeHoras(Float porcentajeHoras) {
        this.porcentajeHoras = porcentajeHoras;
    }

    public Float getPorcentajeHorasLibres() {
        return porcentajeHorasLibres;
    }

    public void setPorcentajeHorasLibres(Float porcentajeHorasLibres) {
        this.porcentajeHorasLibres = porcentajeHorasLibres;
    }

    public Long getInstalacionGrupoId() {
        return instalacionGrupoId;
    }

    public void setInstalacionGrupoId(Long instalacionGrupoId) {
        this.instalacionGrupoId = instalacionGrupoId;
    }
}
