package es.uji.apps.se.model;

public class ModalidadDeportiva {
    private Long id;
    private Long personaId;
    private Long cursoAca;
    private Long modalidadId;
    private String modalidadNombre;

    public ModalidadDeportiva() {
    }

    public ModalidadDeportiva(Long id, Long personaId, Long cursoAca, Long modalidadId, String modalidadNombre) {
        this.id = id;
        this.personaId = personaId;
        this.cursoAca = cursoAca;
        this.modalidadId = modalidadId;
        this.modalidadNombre = modalidadNombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getModalidadId() {
        return modalidadId;
    }

    public void setModalidadId(Long modalidadId) {
        this.modalidadId = modalidadId;
    }

    public String getModalidadNombre() {
        return modalidadNombre;
    }

    public void setModalidadNombre(String modalidadNombre) {
        this.modalidadNombre = modalidadNombre;
    }
}
