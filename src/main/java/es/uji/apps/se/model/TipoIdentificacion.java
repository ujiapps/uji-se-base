package es.uji.apps.se.model;

import java.io.Serializable;

public class TipoIdentificacion implements Serializable {
    private Long id;
    private String nombre;

    public TipoIdentificacion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
