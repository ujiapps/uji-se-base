package es.uji.apps.se.model.domains;

public enum TipoOrigenReservaMateriales
{
    CLASEGENERICO("CLASE-GEN"), ACTIVIDADGENERICO("ACT-GEN"), OFERTAGENERICO("OFE-GEN");

    private final String value;

    TipoOrigenReservaMateriales(String value)
    {
        this.value = value;
    }


    public String getValue()
    {
        return value;
    }

}
