package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

public class Talla {
    private String nombre;

    public Talla() {
    }

    @QueryProjection
    public Talla(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
