package es.uji.apps.se.model.domains;

public enum TipoTaxonomia
{
    CLASE("CLASES"), ACTIVIDAD("ACTIVIDADES"), INSTALACION("INSTALACIONES"), PERSONA("PERSONAS");

    private final String nombre;

    TipoTaxonomia(String nombre)
    {
        this.nombre = nombre;
    }


    public String getNombre()
    {
        return nombre;
    }

}
