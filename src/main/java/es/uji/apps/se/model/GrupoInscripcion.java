package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

public class GrupoInscripcion {

    private Long id;

    private String grupo;

    private String horario;

    private Long vacantes;

    public GrupoInscripcion() {}

    @QueryProjection
    public GrupoInscripcion(Long id, String grupo, String horario, Long vacantes) {
        this.id = id;
        this.grupo = grupo;
        this.horario = horario;
        this.vacantes = vacantes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getVacantes() {
        return vacantes;
    }

    public void setVacantes(Long vacantes) {
        this.vacantes = vacantes;
    }
}
