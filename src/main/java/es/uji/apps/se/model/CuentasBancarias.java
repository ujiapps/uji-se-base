package es.uji.apps.se.model;

public class CuentasBancarias {

    private Long perId;

    private Long tctId;

    private String iban;

    public CuentasBancarias() {
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getTctId() {
        return tctId;
    }

    public void setTctId(Long tctId) {
        this.tctId = tctId;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
}
