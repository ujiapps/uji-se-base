package es.uji.apps.se.model;

public class FiltroEnvioActividades {
    private Long actividad;
    private Long grupo;
    private Boolean apto;
    private Boolean noApto;
    private Boolean espera;
    private Boolean inscritos;

    public FiltroEnvioActividades() {
    }

    public Long getActividad() {
        return actividad;
    }

    public void setActividad(Long actividad) {
        this.actividad = actividad;
    }

    public Long getGrupo() {
        return grupo;
    }

    public void setGrupo(Long grupo) {
        this.grupo = grupo;
    }

    public Boolean isApto() {
        return apto;
    }

    public void setApto(Boolean apto) {
        this.apto = apto;
    }

    public Boolean isNoApto() {
        return noApto;
    }

    public void setNoApto(Boolean noApto) {
        this.noApto = noApto;
    }

    public Boolean isEspera() {
        return espera;
    }

    public void setEspera(Boolean espera) {
        this.espera = espera;
    }

    public Boolean isInscritos() {
        return inscritos;
    }

    public void setInscritos(Boolean inscritos) {
        this.inscritos = inscritos;
    }
}
