package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class EquipacionesUsuario {
    private String actividadNombre;
    private Long actividadId;
    private Long inscripcionId;
    private Date equipacionFechaFin;
    private Long equipacionId;
    private String equipacionNombre;
    private String grupoNombre;
    private Long equipacionDev; /*hayQueDevolver*/
    private Long equipacionStock; /*stockMin*/
    private Date fechaMaxDevolucion;
    private Long tieneVinculadas;
    private String dorsal;
    private String talla;
    private Long equipacionInscripcion;
    private Date fechaDevolucion;
    private String observacion;
    private Long tipoDev;
    private Long stock;
    private Boolean tieneEquipacion;

    public EquipacionesUsuario() {}

    @QueryProjection
    public EquipacionesUsuario(String actividadNombre, Long actividadId, Long inscripcionId, Date equipacionFechaFin, Long equipacionId, String equipacionNombre, String grupoNombre, Long equipacionDev, Long equipacionStock, Date fechaMaxDevolucion, Long tieneVinculadas, String dorsal, String talla, Long equipacionInscripcion, Date fechaDevolucion, String observacion, Long tipoDev, Long stock) {
        this.actividadNombre = actividadNombre;
        this.actividadId = actividadId;
        this.inscripcionId = inscripcionId;
        this.equipacionFechaFin = equipacionFechaFin;
        this.equipacionId = equipacionId;
        this.equipacionNombre = equipacionNombre;
        this.grupoNombre = grupoNombre;
        this.equipacionDev = equipacionDev;
        this.equipacionStock = equipacionStock;
        this.fechaMaxDevolucion = fechaMaxDevolucion;
        this.tieneVinculadas = tieneVinculadas;
        this.dorsal = dorsal;
        this.talla = talla;
        this.equipacionInscripcion = equipacionInscripcion;
        this.fechaDevolucion = fechaDevolucion;
        this.observacion = observacion;
        this.tipoDev = tipoDev;
        this.stock = stock;
        this.tieneEquipacion = null;
    }

    public String getActividadNombre() {
        return actividadNombre;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public Long getInscripcionId() {
        return inscripcionId;
    }

    public void setInscripcionId(Long inscripcionId) {
        this.inscripcionId = inscripcionId;
    }

    public Date getEquipacionFechaFin() {
        return equipacionFechaFin;
    }

    public void setEquipacionFechaFin(Date equipacionFechaFin) {
        this.equipacionFechaFin = equipacionFechaFin;
    }

    public Long getEquipacionId() {
        return equipacionId;
    }

    public void setEquipacionId(Long equipacionId) {
        this.equipacionId = equipacionId;
    }

    public String getEquipacionNombre() {
        return equipacionNombre;
    }

    public void setEquipacionNombre(String equipacionNombre) {
        this.equipacionNombre = equipacionNombre;
    }

    public String getGrupoNombre() {
        return grupoNombre;
    }

    public void setGrupoNombre(String grupoNombre) {
        this.grupoNombre = grupoNombre;
    }

    public Long getEquipacionDev() {
        return equipacionDev;
    }

    public void setEquipacionDev(Long equipacionDev) {
        this.equipacionDev = equipacionDev;
    }

    public Long getEquipacionStock() {
        return equipacionStock;
    }

    public void setEquipacionStock(Long equipacionStock) {
        this.equipacionStock = equipacionStock;
    }

    public Date getFechaMaxDevolucion() {
        return fechaMaxDevolucion;
    }

    public void setFechaMaxDevolucion(Date fechaMaxDevolucion) {
        this.fechaMaxDevolucion = fechaMaxDevolucion;
    }

    public Long getTieneVinculadas() {
        return tieneVinculadas;
    }

    public void setTieneVinculadas(Long tieneVinculadas) {
        this.tieneVinculadas = tieneVinculadas;
    }

    public String getDorsal() {
        return dorsal;
    }

    public void setDorsal(String dorsal) {
        this.dorsal = dorsal;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public Long getEquipacionInscripcion() {
        return equipacionInscripcion;
    }

    public void setEquipacionInscripcion(Long equipacionInscripcion) {
        this.equipacionInscripcion = equipacionInscripcion;
    }

    public Date getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Long getTipoDev() {
        return tipoDev;
    }

    public void setTipoDev(Long tipoDev) {
        this.tipoDev = tipoDev;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Boolean isTieneEquipacion() {
        return tieneEquipacion;
    }

    public void setTieneEquipacion(Boolean tieneEquipacion) {
        this.tieneEquipacion = tieneEquipacion;
    }
}
