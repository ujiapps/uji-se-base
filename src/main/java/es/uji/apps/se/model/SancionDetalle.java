package es.uji.apps.se.model;

public class SancionDetalle {
    private Long id;
    private Long sancionId;
    private String motivo;

    public SancionDetalle() {}

    public SancionDetalle(Long id, Long sancionId, String motivo) {
        this.id = id;
        this.sancionId = sancionId;
        this.motivo = motivo;
    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getSancionId() {return sancionId;}
    public void setSancionId(Long sancionId) {this.sancionId = sancionId;}

    public String getMotivo() {return motivo;}
    public void setMotivo(String motivo) {this.motivo = motivo;}
}
