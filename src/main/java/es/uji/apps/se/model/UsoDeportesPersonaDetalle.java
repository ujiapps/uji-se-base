package es.uji.apps.se.model;

public class UsoDeportesPersonaDetalle {
    private Long perId;
    private String nombreCompleto;
    private String identificacion;

    public UsoDeportesPersonaDetalle() {
    }

    public UsoDeportesPersonaDetalle(Long perId, String nombreCompleto, String identificacion) {
        this.perId = perId;
        this.nombreCompleto = nombreCompleto;
        this.identificacion = identificacion;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
}
