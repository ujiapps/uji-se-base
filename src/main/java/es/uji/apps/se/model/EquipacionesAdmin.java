package es.uji.apps.se.model;

import java.util.Date;

import com.mysema.query.annotations.QueryProjection;

public class EquipacionesAdmin
{
    private Long id;
    private Long equipacionId;
    private Long personaId;
    private String equipacionNombre;
    private Date fecha;
    private String talla;

    public EquipacionesAdmin() {}
    @QueryProjection
    public EquipacionesAdmin(Long id, Date fecha, Long equipacionId, String equipacionTipoNombre, String equipacionModeloNombre,
                             String equipacionGeneroNombre, String talla, Long personaId) {

        this.id = id;
        this.equipacionId = equipacionId;
        this.equipacionNombre = equipacionTipoNombre + " - " + equipacionModeloNombre + " - " + equipacionGeneroNombre;
        this.personaId = personaId;
        this.fecha = fecha;
        this.talla = talla;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEquipacionId()
    {
        return equipacionId;
    }

    public void setEquipacionId(Long equipacionId)
    {
        this.equipacionId = equipacionId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getEquipacionNombre()
    {
        return equipacionNombre;
    }

    public void setEquipacionNombre(String equipacionNombre)
    {
        this.equipacionNombre = equipacionNombre;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getTalla()
    {
        return talla;
    }

    public void setTalla(String talla)
    {
        this.talla = talla;
    }
}
