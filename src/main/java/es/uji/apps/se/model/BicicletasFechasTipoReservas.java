package es.uji.apps.se.model;

import java.util.Date;

public class BicicletasFechasTipoReservas {
    private Date fechaIni;
    private Date fechaFin;

    public BicicletasFechasTipoReservas() {
    }

    public BicicletasFechasTipoReservas(Date fechaIni, Date fechaFin) {
        this.fechaIni = fechaIni;
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
}
