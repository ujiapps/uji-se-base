package es.uji.apps.se.model;

import java.util.Date;

public class FiltroClasesClases {
    private Long clase;
    private Date fechaInicioFiltroClases;
    private Date fechaFinFiltroClases;
    private Date horaInicioFiltroClases;
    private Date horaFinFiltroClases;
    private Boolean asisten;
    private Boolean noAsisten;

    public FiltroClasesClases() {
    }

    public Long getClase() {
        return clase;
    }

    public void setClase(Long clase) {
        this.clase = clase;
    }

    public Date getFechaInicioFiltroClases() {
        return fechaInicioFiltroClases;
    }

    public void setFechaInicioFiltroClases(Date fechaInicioFiltroClases) {
        this.fechaInicioFiltroClases = fechaInicioFiltroClases;
    }

    public Date getFechaFinFiltroClases() {
        return fechaFinFiltroClases;
    }

    public void setFechaFinFiltroClases(Date fechaFinFiltroClases) {
        this.fechaFinFiltroClases = fechaFinFiltroClases;
    }

    public Date getHoraInicioFiltroClases() {
        return horaInicioFiltroClases;
    }

    public void setHoraInicioFiltroClases(Date horaInicioFiltroClases) {
        this.horaInicioFiltroClases = horaInicioFiltroClases;
    }

    public Date getHoraFinFiltroClases() {
        return horaFinFiltroClases;
    }

    public void setHoraFinFiltroClases(Date horaFinFiltroClases) {
        this.horaFinFiltroClases = horaFinFiltroClases;
    }

    public Boolean isAsisten() {
        return asisten;
    }

    public void setAsisten(Boolean asisten) {
        this.asisten = asisten;
    }

    public Boolean isNoAsisten() {
        return noAsisten;
    }

    public void setNoAsisten(Boolean noAsisten) {
        this.noAsisten = noAsisten;
    }
}
