package es.uji.apps.se.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReservaBicicletas {
    private Long id;
    private Date fecha;
    private Long numBici;
    private String tipo;
    private String tipoUso;
    private Long tipoReservaId;
    private String nombreTipoReserva;
    private Date fechaIni;
    private Date fechaFin;
    private String horaFin;
    private Date fechaPrevDevol;
    private String observaciones;
    private Long bicicletaId;
    private Long bicisDisponibles;

    public ReservaBicicletas() {
    }

    public ReservaBicicletas(Long id, Date fecha, Long numBici, String tipoUso, Date fechaIni, Date fechaFin, Date fechaPrevDevol, String observaciones, Long bicicletaId) {
        this.id = id;
        this.fecha = fecha;
        this.numBici = numBici;
        this.tipoUso = tipoUso;
        this.fechaIni = fechaIni;
        this.fechaFin = fechaFin;

        if(this.fechaFin != null)
            this.horaFin = new SimpleDateFormat("HH:mm").format(this.fechaFin);

        this.fechaPrevDevol = fechaPrevDevol;
        this.observaciones = observaciones;
        this.bicicletaId = bicicletaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getNumBici() {
        return numBici;
    }

    public void setNumBici(Long numBici) {
        this.numBici = numBici;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoUso() {
        return tipoUso;
    }

    public void setTipoUso(String tipoUso) {
        this.tipoUso = tipoUso;
    }

    public Long getTipoReservaId() {
        return tipoReservaId;
    }

    public void setTipoReservaId(Long tipoReservaId) {
        this.tipoReservaId = tipoReservaId;
    }

    public String getNombreTipoReserva() {
        return nombreTipoReserva;
    }

    public void setNombreTipoReserva(String nombreTipoReserva) {
        this.nombreTipoReserva = nombreTipoReserva;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHoraFin() {return horaFin;}
    public void setHoraFin(String horaFin) {this.horaFin = horaFin;}

    public Date getFechaPrevDevol() {
        return fechaPrevDevol;
    }

    public void setFechaPrevDevol(Date fechaPrevDevol) {
        this.fechaPrevDevol = fechaPrevDevol;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getBicicletaId() {
        return bicicletaId;
    }

    public void setBicicletaId(Long bicicletaId) {
        this.bicicletaId = bicicletaId;
    }

    public Long getBicisDisponibles() {
        return bicisDisponibles;
    }

    public void setBicisDisponibles(Long bicisDisponibles) {
        this.bicisDisponibles = bicisDisponibles;
    }
}
