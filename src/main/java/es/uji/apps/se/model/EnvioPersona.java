package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.se.dto.EnvioDTO;

import java.io.Serializable;
import java.util.Date;


public class EnvioPersona implements Serializable {

    private Long id;
    private Date fecha;
    private Long envio;
    private String nombre;
    private String identificacion;
    private String correo;
    private Boolean recibirCorreo;

    public EnvioPersona() {
    }

    @QueryProjection
    public EnvioPersona(Long id, Long envio){
        this.id = id;
        this.envio = envio;
    }

    @QueryProjection
    public EnvioPersona(Long id, Long envio, String nombre, String identificacion, String correo,
                        Boolean recibirCorreo){
        this.id = id;
        this.envio = envio;
        this.nombre = nombre;
        this.correo = correo;
        this.identificacion = identificacion;
        this.recibirCorreo = recibirCorreo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getEnvio() {
        return envio;
    }

    public void setEnvio(Long envio) {
        this.envio = envio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Boolean getRecibirCorreo() {
        return recibirCorreo;
    }

    public Boolean isRecibirCorreo() {
        return recibirCorreo;
    }

    public void setRecibirCorreo(Boolean recibirCorreo) {
        this.recibirCorreo = recibirCorreo;
    }

}
