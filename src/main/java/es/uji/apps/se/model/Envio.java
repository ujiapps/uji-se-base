package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;
import java.util.Date;


public class Envio implements Serializable {

    private Long id;
    private String nombre;
    private Date fecha;
    private Date fechaEnvio;

    public Envio() {
    }

    @QueryProjection
    public Envio(Long id, String nombre, Date fecha, Date fechaEnvio){
        this.id = id;
        this.nombre = nombre;
        this.fecha = fecha;
        this.fechaEnvio = fechaEnvio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
}
