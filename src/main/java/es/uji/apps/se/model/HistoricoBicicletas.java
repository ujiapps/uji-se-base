package es.uji.apps.se.model;

import java.util.Date;

public class HistoricoBicicletas {
    private Long id;
    private Date fecha;
    private Long numBici;
    private Long tipoId;
    private Date fechaIni;
    private Date fechaFin;
    private String observaciones;
    private Long bicicletaId;
    private Date xfecha;
    private String apellidosNombre;
    private String tipoReserva;
    private String tipoUso;
    private Date fechaPrevDevol;
    private String accion;

    public HistoricoBicicletas() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getNumBici() {
        return numBici;
    }

    public void setNumBici(Long numBici) {
        this.numBici = numBici;
    }

    public Long getTipoId() {
        return tipoId;
    }

    public void setTipoId(Long tipoId) {
        this.tipoId = tipoId;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getBicicletaId() {
        return bicicletaId;
    }

    public void setBicicletaId(Long bicicletaId) {
        this.bicicletaId = bicicletaId;
    }

    public Date getXfecha() {
        return xfecha;
    }

    public void setXfecha(Date xfecha) {
        this.xfecha = xfecha;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public String getTipoReserva() {
        return tipoReserva;
    }

    public void setTipoReserva(String tipoReserva) {
        this.tipoReserva = tipoReserva;
    }

    public String getTipoUso() {
        return tipoUso;
    }

    public void setTipoUso(String tipoUso) {
        this.tipoUso = tipoUso;
    }

    public Date getFechaPrevDevol() {
        return fechaPrevDevol;
    }

    public void setFechaPrevDevol(Date fechaPrevDevol) {
        this.fechaPrevDevol = fechaPrevDevol;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }
}
