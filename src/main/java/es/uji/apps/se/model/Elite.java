package es.uji.apps.se.model;

public class Elite {

    private Long id;
    private Long personaId;
    private Long cursoId;

    public Elite(Long personaId, Long cursoId) {
        this.personaId = personaId;
        this.cursoId = cursoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getCursoId() {
        return cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }
}
