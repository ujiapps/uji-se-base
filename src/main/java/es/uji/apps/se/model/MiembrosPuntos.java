package es.uji.apps.se.model;

public class MiembrosPuntos {
    public MiembrosPuntos() {
    }

    private Long id;

    private Long miembro;

    private Long partido;

    private Long puntos;

    private Long equipo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMiembro() {
        return miembro;
    }

    public void setMiembro(Long miembro) {
        this.miembro = miembro;
    }

    public Long getPartido() {
        return partido;
    }

    public void setPartido(Long partido) {
        this.partido = partido;
    }

    public Long getPuntos() {
        return puntos;
    }

    public void setPuntos(Long puntos) {
        this.puntos = puntos;
    }

    public Long getEquipo() {
        return equipo;
    }

    public void setEquipo(Long equipo) {
        this.equipo = equipo;
    }
}
