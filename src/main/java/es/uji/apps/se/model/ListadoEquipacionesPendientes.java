package es.uji.apps.se.model;

import java.util.Date;

public class ListadoEquipacionesPendientes {
    private String apellidosNombre;
    private String identificacion;
    private Long cursoAca;
    private Date fechaDev;
    private Long telefono;
    private String nombre;

    public ListadoEquipacionesPendientes() {
    }

    public ListadoEquipacionesPendientes(String apellidosNombre, String identificacion, Long cursoAca, Date fechaDev, Long telefono, String nombre) {
        this.apellidosNombre = apellidosNombre;
        this.identificacion = identificacion;
        this.cursoAca = cursoAca;
        this.fechaDev = fechaDev;
        this.telefono = telefono;
        this.nombre = nombre;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Date getFechaDev() {
        return fechaDev;
    }

    public void setFechaDev(Date fechaDev) {
        this.fechaDev = fechaDev;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
