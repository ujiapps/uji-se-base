package es.uji.apps.se.model;

public class CarnetBono {
    private Long id;
    private Long personaId;
    private String tipo;
    private String concepto;
    private Long importe;
    private Long tipoCobro;
    private String correo;
    private Long descuento;
    private String tipoDescuento;

    public CarnetBono() {}

    public CarnetBono(Long id, Long personaId, String tipo, String concepto, Long importe, Long tipoCobro, String correo, Long descuento, String tipoDescuento) {
        this.id = id;
        this.personaId = personaId;
        this.tipo = tipo;
        this.concepto = concepto;
        this.importe = importe;
        this.tipoCobro = tipoCobro;
        this.correo = correo;
        this.descuento = descuento;
        this.tipoDescuento = tipoDescuento;
    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public String getConcepto() {return concepto;}
    public void setConcepto(String concepto) {this.concepto = concepto;}

    public Long getImporte() {return importe;}
    public void setImporte(Long importe) {this.importe = importe;}

    public Long getTipoCobro() {return tipoCobro;}
    public void setTipoCobro(Long tipoCobro) {this.tipoCobro = tipoCobro;}

    public String getCorreo() {return correo;}
    public void setCorreo(String correo) {this.correo = correo;}

    public Long getDescuento() {return descuento;}
    public void setDescuento(Long descuento) {this.descuento = descuento;}

    public String getTipoDescuento() {return tipoDescuento;}
    public void setTipoDescuento(String tipoDescuento) {this.tipoDescuento = tipoDescuento;}
}
