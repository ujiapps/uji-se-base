package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class Alquiler implements Serializable {

    private Long id;
    private String nombre;
    private String comentarioAgenda;
    private String codigoLlavero;
    private String mail;
    private String telefono;
    private String contacto;

    public Alquiler() {
    }

    @QueryProjection
    public Alquiler(Long id, String nombre, String comentarioAgenda, String codigoLlavero, String mail, String telefono, String contacto) {
        this.id = id;
        this.nombre = nombre;
        this.comentarioAgenda = comentarioAgenda;
        this.codigoLlavero = codigoLlavero;
        this.mail = mail;
        this.telefono = telefono;
        this.contacto = contacto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getComentarioAgenda() {
        return comentarioAgenda;
    }

    public void setComentarioAgenda(String comentarioAgenda) {
        this.comentarioAgenda = comentarioAgenda;
    }

    public String getCodigoLlavero() {
        return codigoLlavero;
    }

    public void setCodigoLlavero(String codigoLlavero) {
        this.codigoLlavero = codigoLlavero;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }
}
