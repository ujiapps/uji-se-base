package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class NotificacionPersona {

    private Long id;
    private Long personaId;
    private Date fecha;
    private String para;
    private String asunto;
    private String cuerpo;

    public NotificacionPersona() {
    }

    @QueryProjection
    public NotificacionPersona(Long id, Long personaId, Date fecha, String para, String asunto, String cuerpo) {
        this.id = id;
        this.personaId = personaId;
        this.fecha = fecha;
        this.para = para;
        this.asunto = asunto;
        this.cuerpo = cuerpo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }
}
