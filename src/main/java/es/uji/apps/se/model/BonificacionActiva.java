package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;

import java.text.DecimalFormat;
import java.util.Date;

public class BonificacionActiva {
    private Long id;
    private Long usos;
    private String texto;
    private String tipoDescuento;
    private String descuento;
    private Long valor;
    private Date fechaFin;

    public BonificacionActiva() {
    }

    @QueryProjection
    public BonificacionActiva(Long id, Date fechaFin, Long usos, String importe, Long porcentaje) {
        this.id = id;
        this.fechaFin = fechaFin;

        if (ParamUtils.isNotNull(porcentaje)) {
            this.descuento = porcentaje + "%";
            this.tipoDescuento = "PORCENTAJE";
            this.valor = porcentaje;
        } else {
            if (ParamUtils.isNotNull(importe)) {
                DecimalFormat df = new DecimalFormat("0.00");
                this.descuento = df.format(ParamUtils.parseLong(importe)) + " €";
                this.tipoDescuento = "EUROS";
                this.valor = ParamUtils.parseLong(importe);
            } else {
                this.descuento = usos + " USOS";
                this.tipoDescuento = "USOS";
                this.valor = usos;
            }
        }
    }

    @QueryProjection
    public BonificacionActiva(Long id, Date fechaFin, Long usos, String importe, Long porcentaje, Long referencia,
                              String origen) {
        this.id = id;
        this.fechaFin = fechaFin;

        if (ParamUtils.isNotNull(porcentaje)) {
            this.descuento = porcentaje + "%";
            this.tipoDescuento = "PORCENTAJE";
            this.valor = porcentaje;
        } else {
            if (ParamUtils.isNotNull(importe)) {
                DecimalFormat df = new DecimalFormat("0.00");
                this.descuento = df.format(ParamUtils.parseLong(importe)) + " €";
                this.tipoDescuento = "EUROS";
                this.valor = ParamUtils.parseLong(importe);
            } else {
                this.descuento = usos + " USOS";
                this.tipoDescuento = "USOS";
                this.valor = usos;
            }
        }

        if (!ParamUtils.isNotNull(referencia)) {
            this.texto = "Bonificació oberta a cualsevol rebut";
        } else {
            if (ParamUtils.isNotNull(origen)) {
                switch (origen) {
                    case "ACT":
                        this.texto = "Bonificació asignada a l'activitat";
                        break;
                    case "INST":
                        this.texto = "Bonificació asignada per a usos de installacions";
                        break;
                    case "TAR":
                        this.texto = "Bonificació asignada a Carnets o bonos";
                        break;
                    default:
                        this.texto = "";
                        break;
                }

            }
        }
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUsos() {
        return usos;
    }

    public void setUsos(Long usos) {
        this.usos = usos;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(String tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Long getValor() {
        return valor;
    }
}
