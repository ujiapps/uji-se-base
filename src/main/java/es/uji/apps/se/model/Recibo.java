package es.uji.apps.se.model;

import java.util.Date;

public class Recibo {
    private Long id;
    private String tipo;
    private Date fechaCreacion;
    private Date fechaPago;
    private Long importe;
    private String tipoCobro;
    private Date fechaPagoLimite;

    public Recibo() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Long getImporte() {
        return importe;
    }

    public void setImporte(Long importe) {
        this.importe = importe;
    }

    public String getTipoCobro() {
        return tipoCobro;
    }

    public void setTipoCobro(String tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    public Date getFechaPagoLimite() {
        return fechaPagoLimite;
    }

    public void setFechaPagoLimite(Date fechaPagoLimite) {
        this.fechaPagoLimite = fechaPagoLimite;
    }
}
