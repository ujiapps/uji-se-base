package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class CarnetBonoTipo {
    private Long id;
    private Long tipoInstalacionId;
    private String nombre;
    private Long dias;
    private Long usos;
    private Long importe;
    private Date fechaInicio;
    private Date fechaInicioNuevaTarjeta;
    private Date fechaFin;
    private Date fechaInicioValidez;
    private Date fechaFinValidez;

    public CarnetBonoTipo() {}

    @QueryProjection
    public CarnetBonoTipo(Long id, Long tipoInstalacionId, String nombre, Long dias, Long usos, Date fechaInicio, Date fechaFin) {
        this.id = id;
        this.tipoInstalacionId = tipoInstalacionId;
        this.nombre = nombre;
        this.dias = dias;
        this.usos = usos;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    private Long calculaImportePorPersona (Long personaId){
        return 1L;
    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getTipoInstalacionId() {return tipoInstalacionId;}
    public void setTipoInstalacionId(Long tipoInstalacionId) {this.tipoInstalacionId = tipoInstalacionId;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public Long getDias() {return dias;}
    public void setDias(Long dias) {this.dias = dias;}

    public Long getImporte() {return importe;}
    public void setImporte(Long importe) {this.importe = importe;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}

    public Date getFechaInicioNuevaTarjeta() {return fechaInicioNuevaTarjeta;}
    public void setFechaInicioNuevaTarjeta(Date fechaInicioNuevaTarjeta) {this.fechaInicioNuevaTarjeta = fechaInicioNuevaTarjeta;}

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Date getFechaInicioValidez() {return fechaInicioValidez;}
    public void setFechaInicioValidez(Date fechaInicioValidez) {this.fechaInicioValidez = fechaInicioValidez;}

    public Date getFechaFinValidez() {return fechaFinValidez;}
    public void setFechaFinValidez(Date fechaFinValidez) {this.fechaFinValidez = fechaFinValidez;}

    public Long getUsos() {
        return usos;
    }

    public void setUsos(Long usos) {
        this.usos = usos;
    }
}
