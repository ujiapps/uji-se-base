package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

public class Dorsal {
    private Long id;
    private Long nombre;

    public Dorsal() {
    }

    @QueryProjection
    public Dorsal(Long id, Long nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNombre() {
        return nombre;
    }

    public void setNombre(Long nombre) {
        this.nombre = nombre;
    }
}
