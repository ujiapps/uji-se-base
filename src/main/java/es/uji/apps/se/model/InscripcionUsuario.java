package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class InscripcionUsuario {

    private Long id;

    private Long personaId;

    private Long muestraComentario;

    private Long cursoAcademico;

    private String nombre;

    private String grupo;

    private Date fecha;

    private Date fechaBaja;

    private Long periodoId;

    private String periodoNombre;

    private Long posEspera;

    private Long estadoId;

    private Long ofertaId;

    private int numeroVeces;

    private String observaciones;

    private Long apto;

    private Date fechaInicioActividad;

    private Date fechaFinActividad;

    private Date fechaRecogidaCertificado;

    public InscripcionUsuario() {
    }

    @QueryProjection
    public InscripcionUsuario(Long id, Long personaId, Long muestraComentario, Long cursoAcademico, String nombre, String grupo, Date fecha, Date fechaBaja, Long periodoId, String periodoNombre, Long posEspera, Long estadoId, Long ofertaId, int numeroVeces, String observaciones, Long apto, Date fechaInicioActividad, Date fechaFinActividad, Date fechaRecogidaCertificado) {
        this.id = id;
        this.personaId = personaId;
        this.muestraComentario = muestraComentario;
        this.cursoAcademico = cursoAcademico;
        this.nombre = nombre;
        this.grupo = grupo;
        this.fecha = fecha;
        this.fechaBaja = fechaBaja;
        this.periodoId = periodoId;
        this.periodoNombre = periodoNombre;
        this.posEspera = posEspera;
        this.estadoId = estadoId;
        this.ofertaId = ofertaId;
        this.numeroVeces = numeroVeces;
        this.observaciones = observaciones;
        this.apto = apto;
        this.fechaInicioActividad = fechaInicioActividad;
        this.fechaFinActividad = fechaFinActividad;
        this.fechaRecogidaCertificado = fechaRecogidaCertificado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getMuestraComentario() {return muestraComentario;}
    public void setMuestraComentario(Long muestraComentario) {this.muestraComentario = muestraComentario;}

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}

    public Long getPeriodoId() {
        return periodoId;
    }

    public void setPeriodoId(Long periodoId) {
        this.periodoId = periodoId;
    }

    public String getPeriodoNombre() {
        return periodoNombre;
    }

    public void setPeriodoNombre(String periodoNombre) {
        this.periodoNombre = periodoNombre;
    }

    public Long getPosEspera() {
        return posEspera;
    }

    public void setPosEspera(Long posEspera) {
        this.posEspera = posEspera;
    }

    public Long getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Long estadoId) {
        this.estadoId = estadoId;
    }

    public Long getOfertaId() {
        return ofertaId;
    }

    public void setOfertaId(Long ofertaId) {
        this.ofertaId = ofertaId;
    }

    public int getNumeroVeces() {
        return numeroVeces;
    }

    public void setNumeroVeces(int numeroVeces) {
        this.numeroVeces = numeroVeces;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getApto() {
        return apto;
    }

    public void setApto(Long apto) {
        this.apto = apto;
    }

    public Date getFechaInicioActividad() {
        return fechaInicioActividad;
    }

    public void setFechaInicioActividad(Date fechaInicioActividad) {
        this.fechaInicioActividad = fechaInicioActividad;
    }

    public Date getFechaFinActividad() {
        return fechaFinActividad;
    }

    public void setFechaFinActividad(Date fechaFinActividad) {
        this.fechaFinActividad = fechaFinActividad;
    }

    public Date getFechaRecogidaCertificado() {return fechaRecogidaCertificado;}
    public void setFechaRecogidaCertificado(Date fechaRecogidaCertificado) {this.fechaRecogidaCertificado = fechaRecogidaCertificado;}
}
