package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class Bonificacion {

    private Long id;
    private Long campanyaId;
    private Long referencia;
    private String origen;
    private Long usos;
    private Float importe;
    private Float porcentaje;
    private Date fechaIni;
    private Date fechaFin;
    private Long mostrar;
    private String comentarios;
    private Long personaId;

    private Long numUsos;
    private Long usado;
    private String asignacion;

    public Bonificacion() {
    }

    @QueryProjection
    public Bonificacion(Long id, Long campanyaId, Long referencia, String origen, Long usos, Float importe, Float porcentaje, Date fechaIni, Date fechaFin, Long mostrar, String comentarios, Long personaId) {
        this.id = id;
        this.campanyaId = campanyaId;
        this.referencia = referencia;
        this.origen = origen;
        this.usos = usos;
        this.importe = importe;
        this.porcentaje = porcentaje;
        this.fechaIni = fechaIni;
        this.fechaFin = fechaFin;
        this.mostrar = mostrar;
        this.comentarios = comentarios;
        this.personaId = personaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCampanyaId() {return campanyaId;}
    public void setCampanyaId(Long campanyaId) {this.campanyaId = campanyaId;}

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getUsos() {
        return usos;
    }

    public void setUsos(Long usos) {
        this.usos = usos;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public Float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Float porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Long getMostrar() {
        return mostrar;
    }

    public void setMostrar(Long mostrar) {
        this.mostrar = mostrar;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getNumUsos() {
        return numUsos;
    }

    public void setNumUsos(Long numUsos) {
        this.numUsos = numUsos;
    }

    public Long getUsado() {
        return usado;
    }

    public void setUsado(Long usado) {
        this.usado = usado;
    }

    public String getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(String asignacion) {
        this.asignacion = asignacion;
    }
}
