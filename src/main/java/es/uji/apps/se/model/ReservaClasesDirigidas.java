package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.se.dto.TarjetaDeportivaClaseDTO;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.rest.ParamUtils;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

public class ReservaClasesDirigidas {
    private Long id;
    private String actividadNombre;
    private String instalacion;
    private Date fecha;
    private String diaSemana;
    private String horaInicio;
    private String horaFin;
    private Boolean asiste;


    public ReservaClasesDirigidas() {
    }

    @QueryProjection
    public ReservaClasesDirigidas(TarjetaDeportivaClaseDTO tarjetaDeportivaClaseDTO) {
        this.id = tarjetaDeportivaClaseDTO.getId();
        this.actividadNombre = (ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getCalendarioClase().getClase())) ? tarjetaDeportivaClaseDTO.getCalendarioClase().getClase().getNombre() : "";
        this.fecha = (ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendario())) ? tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendario().getDia() : null;
        this.diaSemana = (ParamUtils.isNotNull(tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendario())) ? DateExtensions.getDayNameOfWeek(tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendario().getDia()) : "";
        this.horaInicio = tarjetaDeportivaClaseDTO.getCalendarioClase().getHoraInicio();
        this.horaFin = tarjetaDeportivaClaseDTO.getCalendarioClase().getHoraFin();
        this.asiste = tarjetaDeportivaClaseDTO.isAsiste();
        this.instalacion = !tarjetaDeportivaClaseDTO.getCalendarioClase().getCalendarioClaseInstalacionesDTO().isEmpty() ? tarjetaDeportivaClaseDTO.getCalendarioClase().getInstalacionesNombres() : "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividadNombre() {
        return actividadNombre;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean isAsiste() {
        return asiste;
    }

    public void setAsiste(Boolean asiste) {
        this.asiste = asiste;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} {1} - {2} {3}", fecha, horaInicio, horaFin, actividadNombre);
    }

    public Date getFechaHoraInicio() {
        try {
            String[] hora = this.horaInicio.split(":");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(this.fecha);
            calendar.set(Calendar.HOUR, Integer.parseInt(hora[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(hora[1]));
            return calendar.getTime();
        } catch (Exception e) {
            return null;
        }
    }
}
