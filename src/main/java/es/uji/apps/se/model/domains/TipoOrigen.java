package es.uji.apps.se.model.domains;

public enum TipoOrigen
{
    CLASE("CLASE"), OFERTA("ACT");

    private final String sufijo;

    TipoOrigen(String sufijo)
    {
        this.sufijo = sufijo;
    }


    public String getSufijo()
    {
        return sufijo;
    }

}
