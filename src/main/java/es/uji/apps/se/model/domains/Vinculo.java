package es.uji.apps.se.model.domains;

import es.uji.apps.se.exceptions.VinculoNoContempladoException;

import java.text.MessageFormat;

public enum Vinculo
{
    PDI(6322L,"PDI"),
    PAS(6343L,"PAS"),
    ESTUDIANTE(6344L,"Estudiantat"),
    FAMILIAR(292449L,"Familiars (primer grau)"),
    CLUB_DEPORTIVO(8249127L,"Club esportiu per a ús lliure d'instal·ació"),
    EMPRESAS_SERVICIOS(292469L,"Personal d'empreses de serveis"),
    SAUJI(292470L,"SAUJI"),
    SIN_VINCULO(292629L,"Altres (sense cap vincle amb l'UJI)"),
    PERSONAL_ENTIDADES(292314L,"Personal d'entitats vinculades");

    private final Long id;
    private final String descripcion;

    Vinculo(Long id, String descripcion)
    {
        this.id = id;
        this.descripcion = descripcion;
    }

    public static Vinculo getVinculoById(Long vinculoId) throws VinculoNoContempladoException
    {
        if (PDI.getId().equals(vinculoId)) return PDI;
        if (PAS.getId().equals(vinculoId)) return PAS;
        if (ESTUDIANTE.getId().equals(vinculoId)) return ESTUDIANTE;
        if (FAMILIAR.getId().equals(vinculoId)) return FAMILIAR;
        if (CLUB_DEPORTIVO.getId().equals(vinculoId)) return CLUB_DEPORTIVO;
        if (EMPRESAS_SERVICIOS.getId().equals(vinculoId)) return EMPRESAS_SERVICIOS;
        if (SAUJI.getId().equals(vinculoId)) return SAUJI;
        if (SIN_VINCULO.getId().equals(vinculoId)) return SIN_VINCULO;
        if (PERSONAL_ENTIDADES.getId().equals(vinculoId)) return PERSONAL_ENTIDADES;
        throw new VinculoNoContempladoException(MessageFormat.format("Vinculo {0} no contemplado", vinculoId));
    }

    public Long getId()
    {
        return id;
    }

    public String getDescripcion()
    {
        return descripcion;
    }
}
