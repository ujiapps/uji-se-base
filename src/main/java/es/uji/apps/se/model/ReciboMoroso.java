package es.uji.apps.se.model;

import java.util.Date;

public class ReciboMoroso {
    private Date fechaPago;
    private Date fechaPagoLimite;
    private Long tipoCobro;
    private Long reclamacionAuto;
    private Long importeNeto;

    public ReciboMoroso() {}

    public ReciboMoroso(Date fechaPago, Date fechaPagoLimite, Long tipoCobro, Long reclamacionAuto, Long importeNeto) {
        this.fechaPago = fechaPago;
        this.fechaPagoLimite = fechaPagoLimite;
        this.tipoCobro = tipoCobro;
        this.reclamacionAuto = reclamacionAuto;
        this.importeNeto = importeNeto;
    }

    public Date getFechaPago() {return fechaPago;}
    public void setFechaPago(Date fechaPago) {this.fechaPago = fechaPago;}

    public Date getFechaPagoLimite() {return fechaPagoLimite;}
    public void setFechaPagoLimite(Date fechaPagoLimite) {this.fechaPagoLimite = fechaPagoLimite;}

    public Long getTipoCobro() {return tipoCobro;}
    public void setTipoCobro(Long tipoCobro) {this.tipoCobro = tipoCobro;}

    public Long getReclamacionAuto() {return reclamacionAuto;}
    public void setReclamacionAuto(Long reclamacionAuto) {this.reclamacionAuto = reclamacionAuto;}

    public Long getImporteNeto() {return importeNeto;}
    public void setImporteNeto(Long importeNeto) {this.importeNeto = importeNeto;}
}
