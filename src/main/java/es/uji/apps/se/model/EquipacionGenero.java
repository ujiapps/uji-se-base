package es.uji.apps.se.model;

import es.uji.apps.se.dto.EquipacionGeneroDTO;

public class EquipacionGenero {
    private Long id;
    private String nombre;

    public EquipacionGenero() {}

    public EquipacionGenero(EquipacionGeneroDTO genero) {
        this.id = genero.getId();
        this.nombre = genero.getNombre();
    }

    public EquipacionGenero(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
