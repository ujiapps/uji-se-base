package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class HorasReservadasPorTipologia implements Serializable {

    private String tipologia;
    private String tipologiaNombre;
    private Long tipoTipologiaId;
    private Float countZonaAireLibre;
    private Float countZonaPabellon;
    private Float countZonaPiscina;
    private Float countZonaRaquetas;
    private Float countTotal;


    public HorasReservadasPorTipologia() {
    }

    @QueryProjection
    public HorasReservadasPorTipologia(String tipologia, Long tipoTipologiaId, String tipologiaNombre) {
        this.tipologia = tipologia;
        this.tipoTipologiaId = tipoTipologiaId;
        this.tipologiaNombre = tipologiaNombre;
        this.countZonaAireLibre = 0F;
        this.countZonaPabellon = 0F;
        this.countZonaPiscina = 0F;
        this.countZonaRaquetas = 0F;
        this.countTotal = 0F;
    }

    public String getTipologia() {
        return tipologia;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    public Long getTipoTipologiaId() {
        return tipoTipologiaId;
    }

    public void setTipoTipologiaId(Long tipoTipologiaId) {
        this.tipoTipologiaId = tipoTipologiaId;
    }

    public String getTipologiaNombre() {
        return tipologiaNombre;
    }

    public void setTipologiaNombre(String tipologiaNombre) {
        this.tipologiaNombre = tipologiaNombre;
    }

    public Float getCountZonaAireLibre() {
        return countZonaAireLibre;
    }

    public void setCountZonaAireLibre(Float countZonaAireLibre) {
        this.countZonaAireLibre = countZonaAireLibre;
    }

    public Float getCountZonaPabellon() {
        return countZonaPabellon;
    }

    public void setCountZonaPabellon(Float countZonaPabellon) {
        this.countZonaPabellon = countZonaPabellon;
    }

    public Float getCountZonaPiscina() {
        return countZonaPiscina;
    }

    public void setCountZonaPiscina(Float countZonaPiscina) {
        this.countZonaPiscina = countZonaPiscina;
    }

    public Float getCountZonaRaquetas() {
        return countZonaRaquetas;
    }

    public void setCountZonaRaquetas(Float countZonaRaquetas) {
        this.countZonaRaquetas = countZonaRaquetas;
    }

    public Float getCountTotal() {
        return countTotal;
    }

    public void setCountTotal(Float countTotal) {
        this.countTotal = countTotal;
    }


}
