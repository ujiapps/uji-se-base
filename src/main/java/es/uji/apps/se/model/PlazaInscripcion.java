package es.uji.apps.se.model;

import java.util.Date;

public class PlazaInscripcion {
    private String incrito;
    private String descripcion;
    private String grupo;
    private String horario;
    private String puedeInscripcion;
    private String cuentaAjena;
    private Long preinscripcionId;
    private Long admitePreinscripcion;
    private Long plazas;
    private Long plazasOcupadas;
    private Long plazasEspera;
    private Long maxPlazas;
    private Long inscripcionNueva;
    private Long nuevaOferta;
    private Long grupoId;
    private Long inscripcionWeb;
    private Long competicionExterna;
    private Long numVeces;
    private Long posicionEspera;
    private Long tieneBono;
    private Date fechaInicioPreinscripcion;
    private Date fechaFinPreinscripcion;
    private Date fechaInicioRenovacion;
    private Date fechaFinRenovacion;
    private Date fechaInicioValida;
    private Date fechaFinValida;
    private Float importe;
    private Boolean tieneSancion;

    public PlazaInscripcion() {}

    public String getIncrito() {return incrito;}
    public void setIncrito(String incrito) {this.incrito = incrito;}

    public String getDescripcion() {return descripcion;}
    public void setDescripcion(String descripcion) {this.descripcion = descripcion;}

    public String getGrupo() {return grupo;}
    public void setGrupo(String grupo) {this.grupo = grupo;}

    public String getHorario() {return horario;}
    public void setHorario(String horario) {this.horario = horario;}

    public Long getPreinscripcionId() {return preinscripcionId;}
    public void setPreinscripcionId(Long preinscripcionId) {this.preinscripcionId = preinscripcionId;}

    public String getPuedeInscripcion() {return puedeInscripcion;}
    public void setPuedeInscripcion(String puedeInscripcion) {this.puedeInscripcion = puedeInscripcion;}

    public String getCuentaAjena() {return cuentaAjena;}
    public void setCuentaAjena(String cuentaAjena) {this.cuentaAjena = cuentaAjena;}

    public Long getAdmitePreinscripcion() {return admitePreinscripcion;}
    public void setAdmitePreinscripcion(Long admitePreinscripcion) {this.admitePreinscripcion = admitePreinscripcion;}

    public Long getPlazas() {return plazas;}
    public void setPlazas(Long plazas) {this.plazas = plazas;}

    public Long getPlazasOcupadas() {return plazasOcupadas;}
    public void setPlazasOcupadas(Long plazasOcupadas) {this.plazasOcupadas = plazasOcupadas;}

    public Long getPlazasEspera() {return plazasEspera;}
    public void setPlazasEspera(Long plazasEspera) {this.plazasEspera = plazasEspera;}

    public Long getMaxPlazas() {return maxPlazas;}
    public void setMaxPlazas(Long maxPlazas) {this.maxPlazas = maxPlazas;}

    public Long getInscripcionNueva() {return inscripcionNueva;}
    public void setInscripcionNueva(Long inscripcionNueva) {this.inscripcionNueva = inscripcionNueva;}

    public Long getNuevaOferta() {return nuevaOferta;}
    public void setNuevaOferta(Long nuevaOferta) {this.nuevaOferta = nuevaOferta;}

    public Long getGrupoId() {return grupoId;}
    public void setGrupoId(Long grupoId) {this.grupoId = grupoId;}

    public Long getInscripcionWeb() {return inscripcionWeb;}
    public void setInscripcionWeb(Long inscripcionWeb) {this.inscripcionWeb = inscripcionWeb;}

    public Long getCompeticionExterna() {return competicionExterna;}
    public void setCompeticionExterna(Long competicionExterna) {this.competicionExterna = competicionExterna;}

    public Long getNumVeces() {return numVeces;}
    public void setNumVeces(Long numVeces) {this.numVeces = numVeces;}

    public Long getPosicionEspera() {return posicionEspera;}
    public void setPosicionEspera(Long posicionEspera) {this.posicionEspera = posicionEspera;}

    public Long getTieneBono() {return tieneBono;}
    public void setTieneBono(Long tieneBono) {this.tieneBono = tieneBono;}

    public Date getFechaInicioPreinscripcion() {return fechaInicioPreinscripcion;}
    public void setFechaInicioPreinscripcion(Date fechaInicioPreinscripcion) {this.fechaInicioPreinscripcion = fechaInicioPreinscripcion;}

    public Date getFechaFinPreinscripcion() {return fechaFinPreinscripcion;}
    public void setFechaFinPreinscripcion(Date fechaFinPreinscripcion) {this.fechaFinPreinscripcion = fechaFinPreinscripcion;}

    public Date getFechaInicioRenovacion() {return fechaInicioRenovacion;}
    public void setFechaInicioRenovacion(Date fechaInicioRenovacion) {this.fechaInicioRenovacion = fechaInicioRenovacion;}

    public Date getFechaFinRenovacion() {return fechaFinRenovacion;}
    public void setFechaFinRenovacion(Date fechaFinRenovacion) {this.fechaFinRenovacion = fechaFinRenovacion;}

    public Date getFechaInicioValida() {return fechaInicioValida;}
    public void setFechaInicioValida(Date fechaInicioValida) {this.fechaInicioValida = fechaInicioValida;}

    public Date getFechaFinValida() {return fechaFinValida;}
    public void setFechaFinValida(Date fechaFinValida) {this.fechaFinValida = fechaFinValida;}

    public Float getImporte() {return importe;}
    public void setImporte(Float importe) {this.importe = importe;}

    public Boolean isTieneSancion() {return tieneSancion;}
    public void setTieneSancion(Boolean tieneSancion) {this.tieneSancion = tieneSancion;}
}
