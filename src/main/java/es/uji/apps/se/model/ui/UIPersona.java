package es.uji.apps.se.model.ui;

import es.uji.apps.se.dto.PersonaDTO;
import es.uji.commons.rest.ParamUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UIPersona {
    private Long id;
    private String nombre;
    private String fechaNacimiento;
    private String mail;
    private String movil;
    private Long sexo;


    public UIPersona(PersonaDTO personaDTO) {
        this.id = personaDTO.getId();
        this.nombre = (ParamUtils.isNotNull(personaDTO.getApellidosNombre())) ? personaDTO.getApellidosNombre() : personaDTO.getPersonaUji().getApellidosNombre();

        Date fechaNacim = null;
        if (ParamUtils.isNotNull(personaDTO.getFechaNacimiento())) {
            fechaNacim = personaDTO.getFechaNacimiento();
        } else {
            fechaNacim = personaDTO.getPersonaUji().getFechaNacimiento();
        }
        if (ParamUtils.isNotNull(fechaNacim)) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            this.fechaNacimiento = dateFormat.format(fechaNacim);
        }
        this.mail = (ParamUtils.isNotNull(personaDTO.getMail())) ? personaDTO.getMail() : personaDTO.getPersonaUji().getMail();
        this.movil = (ParamUtils.isNotNull(personaDTO.getMovil())) ? personaDTO.getMovil() : personaDTO.getPersonaUji().getMovil();
        if (ParamUtils.isNotNull(personaDTO.getSexo()))
            this.sexo = personaDTO.getSexo();
        else {
            if (ParamUtils.isNotNull(personaDTO.getPersonaUji())) {
                if (ParamUtils.isNotNull(personaDTO.getPersonaUji().getSexoLong()))
                    this.sexo = personaDTO.getPersonaUji().getSexoLong();
            }
        }
//        this.sexo = (ParamUtils.isNotNull(persona.getSexo())) ? persona.getSexo() : (ParamUtils.isNotNull(persona.getPersonaUji().getSexoLong()))?persona.getPersonaUji().getSexoLong();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }

    public Boolean completarDatos() {
        return !ParamUtils.isNotNull(this.movil) || !ParamUtils.isNotNull(this.sexo) || !ParamUtils.isNotNull(this.fechaNacimiento) || !ParamUtils.isNotNull(this.mail);
    }
}
