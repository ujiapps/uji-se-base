package es.uji.apps.se.model;

import java.util.Date;

public class EquipacionInscripcion {
    private Long equipacionInscripcion;
    private Long inscripcionId;
    private Long equipacionId;
    private String talla;
    private String dorsal;
    private Date fecha;
    private String observacion;
    private Date fechaDev;
    private Long tipoDev;

    public EquipacionInscripcion() {}

    public Long getEquipacionInscripcion() {
        return equipacionInscripcion;
    }

    public void setEquipacionInscripcion(Long equipacionInscripcion) {
        this.equipacionInscripcion = equipacionInscripcion;
    }

    public Long getInscripcionId() {
        return inscripcionId;
    }

    public void setInscripcionId(Long inscripcionId) {
        this.inscripcionId = inscripcionId;
    }

    public Long getEquipacionId() {
        return equipacionId;
    }

    public void setEquipacionId(Long equipacionId) {
        this.equipacionId = equipacionId;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getDorsal() {
        return dorsal;
    }

    public void setDorsal(String dorsal) {
        this.dorsal = dorsal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaDev() {
        return fechaDev;
    }

    public void setFechaDev(Date fechaDev) {
        this.fechaDev = fechaDev;
    }

    public Long getTipoDev() {
        return tipoDev;
    }

    public void setTipoDev(Long tipoDev) {
        this.tipoDev = tipoDev;
    }
}
