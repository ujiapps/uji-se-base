package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class BonificacionDetalle {
    private Long id;
    private Long valor;
    private String origen;
    private String tipoEntrada;
    private Long referencia;
    private Date fecha;
    private Long bonificacionId;

    public BonificacionDetalle() {
    }

    @QueryProjection
    public BonificacionDetalle(Long id, Long valor, String origen, String tipoEntrada, Long referencia, Date fecha, Long bonificacionId) {
        this.id = id;
        this.valor = valor;
        this.origen = origen;
        this.tipoEntrada = tipoEntrada;
        this.referencia = referencia;
        this.fecha = fecha;
        this.bonificacionId = bonificacionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getBonificacionId() {
        return bonificacionId;
    }

    public void setBonificacionId(Long bonificacionId) {
        this.bonificacionId = bonificacionId;
    }
}
