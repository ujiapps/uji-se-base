package es.uji.apps.se.model;

public class FiltroPersonas {
    private String email;

    public FiltroPersonas() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
