package es.uji.apps.se.model;

import es.uji.apps.se.dto.EquipacionTipoMovimientoDTO;

public class EquipacionTipoMovimiento {

    private Long id;
    private String nombre;
    private Long tipoId;
    private String etiqueta;
    private String tipoNombre;

    public EquipacionTipoMovimiento() {}

    public EquipacionTipoMovimiento(EquipacionTipoMovimientoDTO tipoMovimientoDTO) {
        this.id = tipoMovimientoDTO.getId();
        this.nombre = tipoMovimientoDTO.getNombre();
        this.tipoId = tipoMovimientoDTO.getTipoId();
        this.etiqueta = tipoMovimientoDTO.getEtiqueta();
    }

    public EquipacionTipoMovimiento(Long id, String nombre, Long tipoId, String etiqueta, String tipoNombre) {
        this.id = id;
        this.nombre = nombre;
        this.tipoId = tipoId;
        this.etiqueta = etiqueta;
        this.tipoNombre = tipoNombre;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public Long getTipoId() { return tipoId; }
    public void setTipoId(Long tipoId) { this.tipoId = tipoId; }

    public String getEtiqueta() { return etiqueta; }
    public void setEtiqueta(String etiqueta) { this.etiqueta = etiqueta; }

    public String getTipoNombre() { return tipoNombre; }
    public void setTipoNombre(String tipoNombre) { this.tipoNombre = tipoNombre; }
}