package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;

import java.io.Serializable;

public class UsoDeportesPersona implements Serializable {

    private String uso;
    private String vinculo;
    private String sexo;
    private Long personas;
    private Long inscripciones;
    private Boolean isTotal;

    public UsoDeportesPersona() {
    }

    @QueryProjection
    public UsoDeportesPersona(String uso, String vinculo, Long sexo, Long personas, Long inscripciones) {
        this.uso = uso;
        this.vinculo = vinculo;
        this.sexo = (ParamUtils.isNotNull(sexo)) ? (sexo.equals(1L)) ? "Home" : "Dona" : "Sense sexe";
        this.personas = (ParamUtils.isNotNull(personas)) ? personas : 0L;
        this.inscripciones = (ParamUtils.isNotNull(inscripciones)) ? inscripciones : 0L;
        this.isTotal = Boolean.FALSE;
    }

    @QueryProjection
    public UsoDeportesPersona(String vinculo, Long sexo, Long personas, Long inscripciones) {
        this.vinculo = vinculo;
        this.sexo = (ParamUtils.isNotNull(sexo)) ? (sexo.equals(1L)) ? "Home" : "Dona" : "Sense sexe";
        this.personas = (ParamUtils.isNotNull(personas)) ? personas : 0L;
        this.inscripciones = (ParamUtils.isNotNull(inscripciones)) ? inscripciones : 0L;
        this.isTotal = Boolean.FALSE;
    }

    @QueryProjection
    public UsoDeportesPersona(String vinculo, Long personas, Long inscripciones) {
        this.vinculo = vinculo;
        this.personas = (ParamUtils.isNotNull(personas)) ? personas : 0L;
        this.inscripciones = (ParamUtils.isNotNull(inscripciones)) ? inscripciones : 0L;
        this.isTotal = Boolean.TRUE;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public String getVinculo() {
        return vinculo;
    }

    public void setVinculo(String vinculo) {
        this.vinculo = vinculo;
    }

    public String getSexo() {
        return sexo;
    }


    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Long getPersonas() {
        return personas;
    }

    public void setPersonas(Long personas) {
        this.personas = personas;
    }

    public Long getInscripciones() {
        return inscripciones;
    }

    public void setInscripciones(Long inscripciones) {
        this.inscripciones = inscripciones;
    }

    public Boolean isIsTotal() {
        return isTotal;
    }

    public Boolean getIsTotal() {
        return isTotal;
    }

    public void setIsTotal(Boolean isTotal) {
        isTotal = isTotal;
    }
}
