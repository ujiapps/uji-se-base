package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;

import java.io.Serializable;

public class TarjetaUsos implements Serializable {

    private Long tarjetaTipoId;
    private String tarjeta;
    private int activas;
    private int usos;
    private int reservas;
    private Float porcentajeReservas;
    private Float porcentajeUtilizacion;

    public TarjetaUsos() {
    }

    @QueryProjection
    public TarjetaUsos(Long tarjetaTipoId, String tarjeta, Long activas) {
        this.tarjetaTipoId = tarjetaTipoId;
        this.tarjeta = tarjeta;
        this.activas = Math.toIntExact(activas);
        this.usos = 0;
        this.reservas = 0;
        this.porcentajeUtilizacion = 0F;
        this.porcentajeReservas = 0F;
    }

    public Long getTarjetaTipoId() {
        return tarjetaTipoId;
    }

    public void setTarjetaTipoId(Long tarjetaTipoId) {
        this.tarjetaTipoId = tarjetaTipoId;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public int getUsos() {
        return usos;
    }

    public void setUsos(Long usos) {
        if (usos != null) {
            this.usos = Math.toIntExact(usos);
            this.setPorcentajeUtilizacion((this.activas!=0)?(Float.valueOf(this.usos) * 100) / Float.valueOf(this.activas):0);
        }
    }

    public int getReservas() {
        return reservas;
    }

    public void setReservas(Long reservas) {
        this.reservas = Math.toIntExact(reservas);
        this.setPorcentajeReservas((this.activas!=0)?(Float.valueOf(this.reservas) * 100)/Float.valueOf(this.activas):0);
    }

    public int getActivas() {
        return activas;
    }

    public void setActivas(int activas) {
        this.activas = activas;
    }

    public Float getPorcentajeReservas() {
        return porcentajeReservas;
    }

    public void setPorcentajeReservas(Float porcentajeReservas) {
        this.porcentajeReservas = porcentajeReservas;
    }

    public Float getPorcentajeUtilizacion() {
        return porcentajeUtilizacion;
    }

    public void setPorcentajeUtilizacion(Float porcentajeUtilizacion) {
        this.porcentajeUtilizacion = porcentajeUtilizacion;
    }
}
