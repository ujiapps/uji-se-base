package es.uji.apps.se.model;

import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EquipacionesMovimiento {
    private Long id;
    private Long equipacionId;
    private String equipacionTipoNombre;
    private String equipacionModeloNombre;
    private String equipacionGeneroNombre;
    private Long movimientoId;
    private String movimiento;
    private String talla;
    private Long dorsal;
    private Date fecha;
    private Long cantidad;
    private String observaciones;
    private Long personaId;

    public EquipacionesMovimiento() {
    }

    public EquipacionesMovimiento(UIEntity entity) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        this.id = ParamUtils.parseLong(entity.get("id"));

        Date fecha = sdf.parse(entity.get("fecha"));
        this.setFecha(fecha);

        this.equipacionId = ParamUtils.parseLong(entity.get("equipacionId"));
        this.movimientoId = ParamUtils.parseLong(entity.get("movimientoId"));
        this.talla = ParamUtils.isNotNull(entity.get("talla")) ? entity.get("talla").toUpperCase().replaceAll("\\s+", "") : null;
        if (ParamUtils.isNotNull(this.talla))
            if (this.talla.equalsIgnoreCase("TALLAÚNICA")) this.talla = null;

        this.dorsal = ParamUtils.isNotNull(entity.get("dorsal")) ? ParamUtils.parseLong(entity.get("dorsal").toUpperCase().replaceAll("\\s+", "")) : null;
        this.cantidad = ParamUtils.isNotNull(entity.get("cantidad")) ? ParamUtils.parseLong(entity.get("cantidad")) : 0L;
        this.observaciones = entity.get("observaciones");
        this.personaId = ParamUtils.isNotNull(entity.get("personaId"))?ParamUtils.parseLong(entity.get("personaId")):null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEquipacionId() {
        return equipacionId;
    }

    public void setEquipacionId(Long equipacionId) {
        this.equipacionId = equipacionId;
    }

    public String getEquipacionTipoNombre() {
        return equipacionTipoNombre;
    }

    public void setEquipacionTipoNombre(String equipacionTipoNombre) {
        this.equipacionTipoNombre = equipacionTipoNombre;
    }

    public String getEquipacionModeloNombre() {
        return equipacionModeloNombre;
    }

    public void setEquipacionModeloNombre(String equipacionModeloNombre) {
        this.equipacionModeloNombre = equipacionModeloNombre;
    }

    public String getEquipacionGeneroNombre() {
        return equipacionGeneroNombre;
    }

    public void setEquipacionGeneroNombre(String equipacionGeneroNombre) {
        this.equipacionGeneroNombre = equipacionGeneroNombre;
    }

    public Long getMovimientoId() {
        return movimientoId;
    }

    public void setMovimientoId(Long movimientoId) {
        this.movimientoId = movimientoId;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public Long getDorsal() {
        return dorsal;
    }

    public void setDorsal(Long dorsal) {
        this.dorsal = dorsal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }
}