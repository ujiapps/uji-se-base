package es.uji.apps.se.model;

public class ReciboPendienteCapitanActividad {
    private Long inscripcionId;

    private Long ofertaId;

    private Long equipoId;

    private Long cuenta;

    private String equipoNombre;

    private String actividadNombre;

    public ReciboPendienteCapitanActividad() {}

    public Long getInscripcionId() {return inscripcionId;}
    public void setInscripcionId(Long inscripcionId) {this.inscripcionId = inscripcionId;}

    public Long getOfertaId() {return ofertaId;}
    public void setOfertaId(Long ofertaId) {this.ofertaId = ofertaId;}

    public Long getEquipoId() {return equipoId;}
    public void setEquipoId(Long equipoId) {this.equipoId = equipoId;}

    public String getEquipoNombre() {return equipoNombre;}
    public void setEquipoNombre(String equipoNombre) {this.equipoNombre = equipoNombre;}

    public String getActividadNombre() {return actividadNombre;}
    public void setActividadNombre(String actividadNombre) {this.actividadNombre = actividadNombre;}

    public Long getCuenta() {return cuenta;}
    public void setCuenta(Long cuenta) {this.cuenta = cuenta;}
}
