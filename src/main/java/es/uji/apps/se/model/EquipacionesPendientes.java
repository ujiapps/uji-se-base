package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class EquipacionesPendientes {

    private Long personaId;
    private Long cursoAcademico;
    private String nombreCompleto;
    private String identificacion;
    private Date fechaMax;

    public EquipacionesPendientes() {
    }

    @QueryProjection
    public EquipacionesPendientes(Long personaId, Long cursoAcademico, String nombreCompleto, String identificacion) {
        this.personaId = personaId;
        this.cursoAcademico = cursoAcademico;
        this.nombreCompleto = nombreCompleto;
        this.identificacion = identificacion;
    }

    @QueryProjection
    public EquipacionesPendientes(Long personaId, Long cursoAcademico, String nombreCompleto, String identificacion, Date fechaMax) {
        this.personaId = personaId;
        this.cursoAcademico = cursoAcademico;
        this.nombreCompleto = nombreCompleto;
        this.identificacion = identificacion;
        this.fechaMax = fechaMax;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public Date getFechaMax() {
        return fechaMax;
    }

    public void setFechaMax(Date fechaMax) {
        this.fechaMax = fechaMax;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;


    }

}
