package es.uji.apps.se.model.ui;


import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTarifaDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;

import java.util.Date;

public class UIRecibosPendientes
{
    private Long tarjetaDeportiva;
    private Float importe;
    private Date fechaCobro;

    public UIRecibosPendientes(){

    }

    public UIRecibosPendientes(Long tarjetaDeportiva, TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO, PersonaUjiDTO personaUjiDTO)
    {
        this.tarjetaDeportiva = tarjetaDeportiva;
        TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO = tarjetaDeportivaTipoDTO.getTarifaByVinculo(personaUjiDTO.getVinculoId());
        if (tarjetaDeportivaTarifaDTO != null)
        {
            this.importe = tarjetaDeportivaTarifaDTO.getImporte();
        }
        this.fechaCobro = new Date();

    }

    public Long getTarjetaDeportiva() {
        return tarjetaDeportiva;
    }

    public void setTarjetaDeportiva(Long tarjetaDeportiva) {
        this.tarjetaDeportiva = tarjetaDeportiva;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public Date getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(Date fechaCobro) {
        this.fechaCobro = fechaCobro;
    }
}
