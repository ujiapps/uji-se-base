package es.uji.apps.se.model;

import java.util.Date;

public class DetalleTarjetaBono {
    private Long id;
    private Date fechaEntrada;
    private String horaEntrada;
    private Date fechaSalida;
    private String horaSalida;
    private Long tipoId;
    private String tipoNombre;

    public DetalleTarjetaBono() {}

    public DetalleTarjetaBono(Long id, Date fechaEntrada, String horaEntrada, Date fechaSalida, String horaSalida, Long tipoId, String tipoNombre) {
        this.id = id;
        this.fechaEntrada = fechaEntrada;
        this.horaEntrada = horaEntrada;
        this.fechaSalida = fechaSalida;
        this.horaSalida = horaSalida;
        this.tipoId = tipoId;
        this.tipoNombre = tipoNombre;
    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Date getFechaEntrada() {return fechaEntrada;}
    public void setFechaEntrada(Date fechaEntrada) {this.fechaEntrada = fechaEntrada;}

    public String getHoraEntrada() {return horaEntrada;}
    public void setHoraEntrada(String horaEntrada) {this.horaEntrada = horaEntrada;}

    public Date getFechaSalida() {return fechaSalida;}
    public void setFechaSalida(Date fechaSalida) {this.fechaSalida = fechaSalida;}

    public String getHoraSalida() {return horaSalida;}
    public void setHoraSalida(String horaSalida) {this.horaSalida = horaSalida;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public String getTipoNombre() {return tipoNombre;}
    public void setTipoNombre(String tipoNombre) {this.tipoNombre = tipoNombre;}
}
