package es.uji.apps.se.model;

public class Domicilios {

    private Long perId;

    private Long ordenPreferencia;

    private String viaId;

    private String nombre;

    private String numero;

    private String escalera;

    private String piso;

    private String puerta;

    private String codPostal;

    private String provinciaId;

    private Long puebloId;

    private String paisId;

    public Domicilios() {
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getOrdenPreferencia() {
        return ordenPreferencia;
    }

    public void setOrdenPreferencia(Long ordenPreferencia) {
        this.ordenPreferencia = ordenPreferencia;
    }

    public String getViaId() {
        return viaId;
    }

    public void setViaId(String viaId) {
        this.viaId = viaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEscalera() {
        return escalera;
    }

    public void setEscalera(String escalera) {
        this.escalera = escalera;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getPuerta() {
        return puerta;
    }

    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getProvinciaId() {
        return provinciaId;
    }

    public void setProvinciaId(String provinciaId) {
        this.provinciaId = provinciaId;
    }

    public Long getPuebloId() {
        return puebloId;
    }

    public void setPuebloId(Long puebloId) {
        this.puebloId = puebloId;
    }

    public String getPaisId() {
        return paisId;
    }

    public void setPaisId(String paisId) {
        this.paisId = paisId;
    }
}
