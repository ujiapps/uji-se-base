package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class Curso {
    private String nombre;
    private Date fechaIni;
    private Date fechaFin;
    private Long anulado;
    private Long id;
    private Long confirmacion;
    private String comentario;

    public Curso() {
    }

    @QueryProjection
    public Curso(String nombre, Date fechaIni, Date fechaFin, Long anulado, Long id, Long confirmacion) {
        this.nombre = nombre;
        this.fechaIni = fechaIni;
        this.fechaFin = fechaFin;
        this.anulado = anulado;
        this.id = id;
        this.confirmacion = confirmacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Long getAnulado() {
        return anulado;
    }

    public void setAnulado(Long anulado) {
        this.anulado = anulado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(Long confirmacion) {
        this.confirmacion = confirmacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}
