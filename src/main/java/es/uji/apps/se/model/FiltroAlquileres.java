package es.uji.apps.se.model;

public class FiltroAlquileres {
    private Long alquiler;

    public FiltroAlquileres() {
    }

    public Long getAlquiler() {
        return alquiler;
    }

    public void setAlquiler(Long alquiler) {
        this.alquiler = alquiler;
    }
}
