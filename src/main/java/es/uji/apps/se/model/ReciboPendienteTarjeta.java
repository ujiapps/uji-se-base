package es.uji.apps.se.model;

import java.util.Date;

public class ReciboPendienteTarjeta {
    private Long id;

    private Long personaId;

    private Long tipoId;

    private Long importe;

    private Long plazos;

    private String nombre;

    private String origen;

    private Date fechaInicio;

    public ReciboPendienteTarjeta() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public Long getImporte() {return importe;}
    public void setImporte(Long importe) {this.importe = importe;}

    public Long getPlazos() {return plazos;}
    public void setPlazos(Long plazos) {this.plazos = plazos;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}
}
