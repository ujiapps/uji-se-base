package es.uji.apps.se.model;

public class EquipacionGrupos {

    private Long id;
    private String nombre;
    private Long tipoId;
    private String tipoNombre;
    private Long hayQueDevolver;

    public EquipacionGrupos() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public Long getTipoId() { return tipoId; }
    public void setTipoId(Long tipoId) { this.tipoId = tipoId; }

    public String getTipoNombre() { return tipoNombre; }
    public void setTipoNombre(String tipoNombre) { this.tipoNombre = tipoNombre; }

    public Long getHayQueDevolver() { return hayQueDevolver; }
    public void setHayQueDevolver(Long hayQueDevolver) { this.hayQueDevolver = hayQueDevolver; }
}