package es.uji.apps.se.model;

import java.util.Date;

public class TipoTarjeta {
    private Long dias;
    private Long usos;
    private Date fechaInicioValidez;
    private Date fechaFinValidez;

    public TipoTarjeta() {}

    public TipoTarjeta(Long dias, Long usos, Date fechaInicioValidez, Date fechaFinValidez) {
        this.dias = dias;
        this.usos = usos;
        this.fechaInicioValidez = fechaInicioValidez;
        this.fechaFinValidez = fechaFinValidez;
    }

    public Long getDias() {return dias;}
    public void setDias(Long dias) {this.dias = dias;}

    public Long getUsos() {return usos;}
    public void setUsos(Long usos) {this.usos = usos;}

    public Date getFechaInicioValidez() {return fechaInicioValidez;}
    public void setFechaInicioValidez(Date fechaInicioValidez) {this.fechaInicioValidez = fechaInicioValidez;}

    public Date getFechaFinValidez() {return fechaFinValidez;}
    public void setFechaFinValidez(Date fechaFinValidez) {this.fechaFinValidez = fechaFinValidez;}
}
