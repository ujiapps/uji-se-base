package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;

import java.io.Serializable;
import java.util.Base64;

public class CompeticionMiembro implements Serializable {

    private Long id;
    private Long inscripcionId;
    private String nombre;
    private String equipo;
    private String foto;
    private String asiste;

    public CompeticionMiembro() {
    }

    @QueryProjection
    public CompeticionMiembro(Long id, Long inscripcionId, String nombre, String apellido1, String apellido2,
                              String equipo, byte[] foto,
                              String mimeType, Long asistenciaId) {
        this.id = id;
        this.inscripcionId = inscripcionId;
        this.nombre = apellido1 + " " + apellido2 + ", " + nombre;
        this.equipo = equipo;
        if (ParamUtils.isNotNull(mimeType)) {
            this.foto = convertirADataURL(foto, mimeType);
        }
        this.asiste = ParamUtils.isNotNull(asistenciaId) ? "asiste" : "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getInscripcionId() {
        return inscripcionId;
    }

    public void setInscripcionId(Long inscripcionId) {
        this.inscripcionId = inscripcionId;
    }

    public String getAsiste() {
        return asiste;
    }

    public void setAsiste(String asiste) {
        this.asiste = asiste;
    }

    private String convertirADataURL(byte[] imageBytes, String contentType) {
        String base64Image = Base64.getEncoder().encodeToString(imageBytes);
        return "data:" + contentType + ";base64," + base64Image;
    }
}
