package es.uji.apps.se.model;

import java.util.List;

public class Vinculaciones {
    private List<String> vinculacionesActivas;
    private List<String> vinculacionesUji;
    private List<String> titulacionesActivas;

    public Vinculaciones() {
    }

    public List<String> getVinculacionesActivas() {
        return vinculacionesActivas;
    }

    public void setVinculacionesActivas(List<String> vinculacionesActivas) {
        this.vinculacionesActivas = vinculacionesActivas;
    }

    public List<String> getVinculacionesUji() {
        return vinculacionesUji;
    }

    public void setVinculacionesUji(List<String> vinculacionesUji) {
        this.vinculacionesUji = vinculacionesUji;
    }

    public List<String> getTitulacionesActivas() {return titulacionesActivas;}
    public void setTitulacionesActivas(List<String> titulacionesActivas) {this.titulacionesActivas = titulacionesActivas;}
}
