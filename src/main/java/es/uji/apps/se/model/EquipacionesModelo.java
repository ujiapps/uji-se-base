package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.se.dto.EquipacionModeloDTO;

import java.io.Serializable;

public class EquipacionesModelo implements Serializable {

    private Long id;
    private String nombre;

    public EquipacionesModelo() {}

    public EquipacionesModelo(EquipacionModeloDTO equipacionModeloDTO){
        setId(equipacionModeloDTO.getId());
        setNombre(equipacionModeloDTO.getNombre());
    }

    @QueryProjection
    public EquipacionesModelo(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
