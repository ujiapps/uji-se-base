package es.uji.apps.se.model;

import java.util.Date;

public class FiltroClasesInstalacion
{
    private Long instalacion;
    private Date fechaInicioInstalacion;
    private Date fechaFinInstalacion;
    private Date horaInicioInstalacion;
    private Date horaFinInstalacion;
    private Boolean asisten;
    private Boolean noAsisten;

    public FiltroClasesInstalacion() {
    }

    public Long getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(Long instalacion) {
        this.instalacion = instalacion;
    }

    public Date getFechaInicioInstalacion() {
        return fechaInicioInstalacion;
    }

    public void setFechaInicioInstalacion(Date fechaInicioInstalacion) {
        this.fechaInicioInstalacion = fechaInicioInstalacion;
    }

    public Date getFechaFinInstalacion() {
        return fechaFinInstalacion;
    }

    public void setFechaFinInstalacion(Date fechaFinInstalacion) {
        this.fechaFinInstalacion = fechaFinInstalacion;
    }

    public Date getHoraInicioInstalacion() {
        return horaInicioInstalacion;
    }

    public void setHoraInicioInstalacion(Date horaInicioInstalacion) {
        this.horaInicioInstalacion = horaInicioInstalacion;
    }

    public Date getHoraFinInstalacion() {
        return horaFinInstalacion;
    }

    public void setHoraFinInstalacion(Date horaFinInstalacion) {
        this.horaFinInstalacion = horaFinInstalacion;
    }

    public Boolean isAsisten() {
        return asisten;
    }

    public void setAsisten(Boolean asisten) {
        this.asisten = asisten;
    }

    public Boolean isNoAsisten() {
        return noAsisten;
    }

    public void setNoAsisten(Boolean noAsisten) {
        this.noAsisten = noAsisten;
    }
}
