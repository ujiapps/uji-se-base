package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;
import java.util.Date;

public class ListadoFaltasCursos implements Serializable {
    private Long id;
    private Long perId;
    private Long cursoId;
    private Date dia;
    private String horaIni;
    private String horaFin;
    private String asisto;

    public ListadoFaltasCursos() {
    }

    @QueryProjection
    public ListadoFaltasCursos(Long id, Long perId, Long cursoId, Date dia, String horaIni, String horaFin, String asisto) {
        this.id = id;
        this.perId = perId;
        this.cursoId = cursoId;
        this.dia = dia;
        this.horaIni = horaIni;
        this.horaFin = horaFin;
        this.asisto = asisto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getCursoId() {
        return cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(String horaIni) {
        this.horaIni = horaIni;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getAsisto() {
        return asisto;
    }

    public void setAsisto(String asisto) {
        this.asisto = asisto;
    }
}
