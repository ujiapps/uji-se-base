package es.uji.apps.se.model;

import java.util.Date;

public class Autorizacion {
    private Long id;
    private Long zonaId;
    private String zonaNombre;
    private Date horaEntrada;
    private Date horaSalida;
    private Date horaInicioReserva;
    private Date horaFinReserva;
    private String actividadId;
    private String actividadNombre;
    private Long origenId;
    private String origenNombre;

    public Autorizacion() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getZonaId() {return zonaId;}
    public void setZonaId(Long zonaId) {this.zonaId = zonaId;}

    public String getZonaNombre() {return zonaNombre;}
    public void setZonaNombre(String zonaNombre) {this.zonaNombre = zonaNombre;}

    public Date getHoraEntrada() {return horaEntrada;}
    public void setHoraEntrada(Date horaEntrada) {this.horaEntrada = horaEntrada;}

    public Date getHoraSalida() {return horaSalida;}
    public void setHoraSalida(Date horaSalida) {this.horaSalida = horaSalida;}

    public Date getHoraInicioReserva() {return horaInicioReserva;}
    public void setHoraInicioReserva(Date horaInicioReserva) {this.horaInicioReserva = horaInicioReserva;}

    public Date getHoraFinReserva() {return horaFinReserva;}
    public void setHoraFinReserva(Date horaFinReserva) {this.horaFinReserva = horaFinReserva;}

    public String getActividadId() {return actividadId;}
    public void setActividadId(String actividadId) {this.actividadId = actividadId;}

    public String getActividadNombre() {return actividadNombre;}
    public void setActividadNombre(String actividadNombre) {this.actividadNombre = actividadNombre;}

    public Long getOrigenId() {return origenId;}
    public void setOrigenId(Long origenId) {this.origenId = origenId;}

    public String getOrigenNombre() {return origenNombre;}
    public void setOrigenNombre(String origenNombre) {this.origenNombre = origenNombre;}
}
