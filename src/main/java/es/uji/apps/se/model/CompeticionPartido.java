package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class CompeticionPartido implements Serializable {

    private Long id;
    private Long equipoCasa;
    private Long equipoFuera;
    private Long resultadoCasa;
    private Long resultadoFuera;
    private String comentarios;

    public CompeticionPartido() {
    }

    @QueryProjection
    public CompeticionPartido(Long id, Long equipoCasa, Long equipoFuera) {
        this.id = id;
        this.equipoCasa = equipoCasa;
        this.equipoFuera = equipoFuera;
    }

    @QueryProjection
    public CompeticionPartido(Long id, Long equipoCasa, Long equipoFuera, Long resultadoCasa, Long resultadoFuera, String comentarios) {
        this.id = id;
        this.equipoCasa = equipoCasa;
        this.equipoFuera = equipoFuera;
        this.resultadoCasa = resultadoCasa;
        this.resultadoFuera = resultadoFuera;
        this.comentarios = comentarios;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEquipoCasa() {
        return equipoCasa;
    }

    public void setEquipoCasa(Long equipoCasa) {
        this.equipoCasa = equipoCasa;
    }

    public Long getEquipoFuera() {
        return equipoFuera;
    }

    public void setEquipoFuera(Long equipoFuera) {
        this.equipoFuera = equipoFuera;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Long getResultadoCasa() {
        return resultadoCasa;
    }

    public void setResultadoCasa(Long resultadoCasa) {this.resultadoCasa = resultadoCasa; }

    public Long getResultadoFuera() {
        return resultadoFuera;
    }

    public void setResultadoFuera(Long resultadoFuera) {this.resultadoFuera = resultadoFuera; }

}
