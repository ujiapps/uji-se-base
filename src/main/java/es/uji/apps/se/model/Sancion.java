package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.se.dto.SancionDTO;

import java.util.Date;

public class Sancion {
    private Long id;
    private Long modificable;
    private Date fecha;
    private Date fechaFin;
    private Long nivelId;
    private String nivelNombre;
    private String tipo;
    private String motivo;
    private String origen;

    public Sancion() {}

    @QueryProjection
    public Sancion(Long id, Date fecha, Date fechaFin, Long nivelId, String nombre, String motivo, String tipo, Long modificable, String origen) {
        this.id = id;
        this.fecha = fecha;
        this.fechaFin = fechaFin;
        this.nivelId = nivelId;
        this.nivelNombre = nombre;
        this.motivo = motivo;
        this.tipo = tipo;
        this.modificable = modificable;
        this.origen = origen;
    }

    public Sancion(SancionDTO sancion) {
        this.id = sancion.getId();
        this.fecha = sancion.getFecha();
        this.fechaFin = sancion.getFechaFin();
        this.nivelId = sancion.getNivel().getId();
        this.nivelNombre = sancion.getNivel().getNombre();
        this.motivo = sancion.getMotivo();
        this.tipo = sancion.getTipo();
    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getModificable() {return modificable;}
    public void setModificable(Long modificable) {this.modificable = modificable;}

    public Date getFecha() {return fecha;}
    public void setFecha(Date fecha) {this.fecha = fecha;}

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Long getNivelId() {return nivelId;}
    public void setNivelId(Long nivelId) {this.nivelId = nivelId;}

    public String getNivelNombre() {return nivelNombre;}
    public void setNivelNombre(String nivelNombre) {this.nivelNombre = nivelNombre;}

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public String getMotivo() {return motivo;}
    public void setMotivo(String motivo) {this.motivo = motivo;}

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
}
