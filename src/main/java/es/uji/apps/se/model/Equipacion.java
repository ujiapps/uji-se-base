package es.uji.apps.se.model;

import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import java.time.Instant;
import java.util.Date;

public class Equipacion {

    private Long id;
    private Long modeloId;
    private String modeloNombre;
    private Long grupoId;
    private String grupoNombre;
    private Long generoId;
    private String generoNombre;
    private Long tipoId;
    private String tipoNombre;
    private Long stockMin;
    private Date fechaInicio;
    private Date fechaFin;

    public Equipacion() { }

    public Equipacion(UIEntity entity) {
        Equipacion equipacion = entity.toModel(Equipacion.class);

        String fechaIni = entity.get("fechaInicio");
        String fechaFin = entity.get("fechaFin");

        equipacion.setFechaInicio(Date.from(Instant.parse(fechaIni)));
        if (ParamUtils.isNotNull(fechaFin)) {
            equipacion.setFechaFin(Date.from(Instant.parse(fechaFin)));
        }
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getModeloId() { return modeloId; }
    public void setModeloId(Long modeloId) { this.modeloId = modeloId; }

    public String getModeloNombre() { return modeloNombre; }
    public void setModeloNombre(String modeloNombre) { this.modeloNombre = modeloNombre; }

    public Long getGrupoId() { return grupoId; }
    public void setGrupoId(Long grupoId) { this.grupoId = grupoId; }

    public String getGrupoNombre() { return grupoNombre; }
    public void setGrupoNombre(String grupoNombre) { this.grupoNombre = grupoNombre; }

    public Long getGeneroId() { return generoId; }
    public void setGeneroId(Long generoId) { this.generoId = generoId; }

    public String getGeneroNombre() { return generoNombre; }
    public void setGeneroNombre(String generoNombre) { this.generoNombre = generoNombre; }

    public Long getTipoId() { return tipoId; }
    public void setTipoId(Long tipoId) { this.tipoId = tipoId; }

    public String getTipoNombre() { return tipoNombre; }
    public void setTipoNombre(String tipoNombre) { this.tipoNombre = tipoNombre; }

    public Long getStockMin() { return stockMin; }
    public void setStockMin(Long stockMin) { this.stockMin = stockMin; }

    public Date getFechaInicio() { return fechaInicio; }
    public void setFechaInicio(Date fechaInicio) { this.fechaInicio = fechaInicio; }

    public Date getFechaFin() { return fechaFin; }
    public void setFechaFin(Date fechaFin) { this.fechaFin = fechaFin; }
}
