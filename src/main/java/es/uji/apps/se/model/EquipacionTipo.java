package es.uji.apps.se.model;

import es.uji.apps.se.dto.EquipacionTipoDTO;

public class EquipacionTipo {
    private Long id;
    private String nombre;

    public EquipacionTipo() {}

    public EquipacionTipo(EquipacionTipoDTO tipo) {
        this.id = tipo.getId();
        this.nombre = tipo.getNombre();
    }

    public EquipacionTipo(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
}
