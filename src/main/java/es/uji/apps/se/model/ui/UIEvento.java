package es.uji.apps.se.model.ui;

import es.uji.apps.se.dto.CalendarioClaseVistaDTO;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.rest.ParamUtils;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UIEvento {
    static final String COLOR_RESERVADO = "fondo-reservado";
    static final String COLOR_RESERVABLE = "fondo-reservable";
    static final String COLOR_NO_RESERVABLE = "fondo-no-reservable";
    static final String COLOR_RESERVA_PASADA = "fondo-reserva-pasada";
    static final String COLOR_NO_ASISTE = "fondo-no-asiste";

    private Long id;
    private String title;
    private String start;
    private String end;
    private String background;
    private Boolean reservable;
    private Boolean lleno;
    private Boolean anulable;
    private Boolean asiste;
    private Long plazasTotales;
    private Long plazasOcupadas;
    private Long reservaId;
    private String horaInicio;
    private String horaFin;
    private String icono;

    public UIEvento(CalendarioClaseVistaDTO calendarioClase, Date primeraFechaLimiteReserva, int minutosLimiteReserva, int minutosLimiteAnulacion) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        this.id = calendarioClase.getId();
        this.start = dateFormat.format(calendarioClase.getFechaHoraInicio());
        this.end = dateFormat.format(calendarioClase.getFechaHoraFin());
        this.plazasTotales = calendarioClase.getPlazas();
        this.plazasOcupadas = (calendarioClase.getPlazas() < calendarioClase.getOcupadas())?calendarioClase.getPlazas():calendarioClase.getOcupadas();
        this.title = MessageFormat.format("{0} ({1}/{2})", calendarioClase.getClase(), plazasOcupadas, plazasTotales);
        this.reservable = primeraFechaLimiteReserva.after(calendarioClase.getFechaHoraInicio()) && new Date().before(calendarioClase.getUltimaFechaLimiteReserva(minutosLimiteReserva)) && (plazasOcupadas < plazasTotales);
        this.lleno = plazasOcupadas >= plazasTotales;
        this.anulable = new Date().before(calendarioClase.getUltimaFechaLimiteAnulacion(minutosLimiteAnulacion)) && ParamUtils.isNotNull(calendarioClase.getReserva());
        this.reservaId = calendarioClase.getReserva();
        this.asiste = (ParamUtils.isNotNull(calendarioClase.getReserva())) ? calendarioClase.isAsiste() : null;
        this.background = getColorEvent(calendarioClase.getFechaHoraInicio());
        this.horaInicio = calendarioClase.getHoraInicio();
        this.horaFin = calendarioClase.getHoraFin();
        this.icono = calendarioClase.getIcono();
    }

    private String getColorEvent(Date fechaInicioClase) {
        if (reservaId == null) {
            return reservable ? COLOR_RESERVABLE : COLOR_NO_RESERVABLE;
        }

        if (anulable) {
            return COLOR_RESERVADO;
        }
        Date fechaClaseMas24h = DateExtensions.addDays(fechaInicioClase, 1);
        return (!asiste && new Date().after(fechaClaseMas24h)) ? COLOR_NO_ASISTE : COLOR_RESERVA_PASADA;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isReservable() {
        return reservable;
    }

    public Boolean isLleno() { return lleno; }

    public void setReservable(Boolean reservable) {
        this.reservable = reservable;
    }

    public Boolean isAnulable() {
        return anulable;
    }

    public void setAnulable(Boolean anulable) {
        this.anulable = anulable;
    }

    public Long getPlazasTotales() {
        return plazasTotales;
    }

    public void setPlazasTotales(Long plazasTotales) {
        this.plazasTotales = plazasTotales;
    }

    public Long getPlazasOcupadas() {
        return plazasOcupadas;
    }

    public void setPlazasOcupadas(Long plazasOcupadas) {
        this.plazasOcupadas = plazasOcupadas;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Long getReservaId() {
        return reservaId;
    }

    public void setReservaId(Long reservaId) {
        this.reservaId = reservaId;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0}", title);
    }


}