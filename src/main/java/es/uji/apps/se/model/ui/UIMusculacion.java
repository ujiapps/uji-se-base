package es.uji.apps.se.model.ui;

public class UIMusculacion
{
    private Long aforo;
    private Long instalacion;
    private String horaInicio;
    private String horaFin;

    public UIMusculacion()
    {
    }

    public Long getAforo() {
        return aforo;
    }

    public void setAforo(Long aforo) {
        this.aforo = aforo;
    }

    public Long getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(Long instalacion) {
        this.instalacion = instalacion;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }
}
