package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class AsistenciasPorActividad implements Serializable {

    private Long actividadId;
    private String actividad;
    private String horario;
    private Long plazas;
    private Long reservas;
    private Long asistencia;
    private Long porcentajeReservas;
    private Long porcentajeAsistenciaReservas;
    private Long porcentajeAsistenciaOferta;


    public AsistenciasPorActividad() {
    }

    @QueryProjection
    public AsistenciasPorActividad(Long actividadId, String actividad, String horario) {
        this.actividadId = actividadId;
        this.actividad = actividad;
        this.horario = horario;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getPlazas() {
        return plazas;
    }

    public void setPlazas(Long plazas) {
        this.plazas = plazas;
    }

    public Long getReservas() {
        return reservas;
    }

    public void setReservas(Long reservas) {
        this.reservas = reservas;
    }

    public Long getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(Long asistencia) {
        this.asistencia = asistencia;
    }

    public Long getPorcentajeReservas() {
        return porcentajeReservas;
    }

    public void setPorcentajeReservas(Long porcentajeReservas) {
        this.porcentajeReservas = porcentajeReservas;
    }

    public Long getPorcentajeAsistenciaReservas() {
        return porcentajeAsistenciaReservas;
    }

    public void setPorcentajeAsistenciaReservas(Long porcentajeAsistenciaReservas) {
        this.porcentajeAsistenciaReservas = porcentajeAsistenciaReservas;
    }

    public Long getPorcentajeAsistenciaOferta() {
        return porcentajeAsistenciaOferta;
    }

    public void setPorcentajeAsistenciaOferta(Long porcentajeAsistenciaOferta) {
        this.porcentajeAsistenciaOferta = porcentajeAsistenciaOferta;
    }
}
