package es.uji.apps.se.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.uji.commons.rest.ParamUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Paginacion {
    private Long start;
    private Long limit;
    private String ordenarPor;
    private String direccion;
    private Long totalCount;

    public Paginacion(Long start, Long limit) throws IOException {
        this(start, limit, null);
    }

    public Paginacion(Long start, Long limit, String orden) throws IOException {
        this.start = start;
        this.limit = limit;
        if (ParamUtils.isNotNull(orden)) {
            if (orden.length() > 2) {
                List<Map<String, String>> sortList = new ObjectMapper().readValue(orden, new TypeReference<List<Map<String, String>>>() {
                });
                this.setOrdenarPor(sortList.get(0).get("property"));
                this.setDireccion(sortList.get(0).get("direction"));
            }
        }
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public String getOrdenarPor() {
        return ordenarPor;
    }

    public void setOrdenarPor(String ordenarPor) {
        this.ordenarPor = ordenarPor;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
