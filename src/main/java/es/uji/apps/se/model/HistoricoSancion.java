package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class HistoricoSancion {
    private Long id;
    private Date fecha;
    private Date fechaFin;
    private Date fechaAccion;
    private String nivel;
    private String motivo;
    private String tipo;
    private String tipoDml;
    private String persona;

    public HistoricoSancion() {}

    @QueryProjection
    public HistoricoSancion(Long id, Date fecha, Date fechaFin, Date fechaAccion, String nivel, String motivo, String tipo, String tipoDml, String persona) {
        this.id = id;
        this.fecha = fecha;
        this.fechaFin = fechaFin;
        this.fechaAccion = fechaAccion;
        this.nivel = nivel;
        this.motivo = motivo;
        this.tipo = tipo;
        if (tipoDml.equals("I")) {
            this.tipoDml = "Insercció";
        } else {
            if (tipoDml.equals("D")) {
                this.tipoDml = "Borrado";
            } else {
                this.tipoDml = "Actualizació";
            }
        }
        this.persona = (persona.equals(" , ")) ? "" : persona;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaAccion() {
        return fechaAccion;
    }

    public void setFechaAccion(Date xFecha) {
        this.fechaAccion = xFecha;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoDml() {
        return tipoDml;
    }

    public void setTipoDml(String tipoDml) {
        this.tipoDml = tipoDml;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }
}
