package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class Adjunto implements Serializable {

    private Long id;
    private String nombre;
    private String referencia;


    public Adjunto() {
    }

    @QueryProjection
    public Adjunto(Long id, String nombre, String referencia) {
        this.id = id;
        this.nombre = nombre;
        this.referencia = referencia;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
