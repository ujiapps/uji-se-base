package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;
import java.util.Date;

public class MaterialReserva implements Serializable {

    private Long id;
    private String origen;
    private Long cantidad;
    private String comentarios;
    private Date fecha;
    private Boolean visualizarEnAgenda;
    private String referencia;
    private String referencia2;
    private Long personaUji;
    private Long admin;
    private Long material;

    public MaterialReserva() {}

    @QueryProjection
    public MaterialReserva(Long id, String origen, Long cantidad, String comentarios, Date fecha, Boolean visualizarEnAgenda,
                           String referencia, String referencia2, Long materialId) {
        this.id = id;
        this.origen = origen;
        this.cantidad = cantidad;
        this.comentarios = comentarios;
        this.fecha = fecha;
        this.visualizarEnAgenda = visualizarEnAgenda;
        this.referencia = referencia;
        this.referencia2 = referencia2;
        this.material = materialId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getVisualizarEnAgenda() {
        return visualizarEnAgenda;
    }

    public void setVisualizarEnAgenda(Boolean visualizarEnAgenda) {
        this.visualizarEnAgenda = visualizarEnAgenda;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getReferencia2() {
        return referencia2;
    }

    public void setReferencia2(String referencia2) {
        this.referencia2 = referencia2;
    }

    public Long getPersonaUji() {
        return personaUji;
    }

    public void setPersonaUji(Long personaUji) {
        this.personaUji = personaUji;
    }

    public Long getAdmin() {
        return admin;
    }

    public void setAdmin(Long admin) {
        this.admin = admin;
    }

    public Long getMaterial() {
        return material;
    }

    public void setMaterial(Long material) {
        this.material = material;
    }
}
