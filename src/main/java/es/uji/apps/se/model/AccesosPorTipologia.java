package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class AccesosPorTipologia implements Serializable {

    private Long tipologiaId;
    private String tipologia;
    private Long countZonaAireLibre;
    private Long countZonaPabellon;
    private Long countZonaPiscina;
    private Long countZonaRaquetas;
    private Long countTotal;


    public AccesosPorTipologia() {
    }

    @QueryProjection
    public AccesosPorTipologia(Long tipologiaId, String tipologia) {
        this.tipologiaId = tipologiaId;
        this.tipologia = tipologia;
        this.countZonaAireLibre = 0L;
        this.countZonaPabellon = 0L;
        this.countZonaPiscina = 0L;
        this.countZonaRaquetas = 0L;
        this.countTotal = 0L;
    }

    public Long getTipologiaId() {
        return tipologiaId;
    }

    public void setTipologiaId(Long tipologiaId) {
        this.tipologiaId = tipologiaId;
    }

    public String getTipologia() {
        return tipologia;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    public Long getCountZonaAireLibre() {
        return countZonaAireLibre;
    }

    public void setCountZonaAireLibre(Long countZonaAireLibre) {
        this.countZonaAireLibre = countZonaAireLibre;
    }

    public Long getCountZonaPabellon() {
        return countZonaPabellon;
    }

    public void setCountZonaPabellon(Long countZonaPabellon) {
        this.countZonaPabellon = countZonaPabellon;
    }

    public Long getCountZonaPiscina() {
        return countZonaPiscina;
    }

    public void setCountZonaPiscina(Long countZonaPiscina) {
        this.countZonaPiscina = countZonaPiscina;
    }

    public Long getCountZonaRaquetas() {
        return countZonaRaquetas;
    }

    public void setCountZonaRaquetas(Long countZonaRaquetas) {
        this.countZonaRaquetas = countZonaRaquetas;
    }

    public Long getCountTotal() {
        return countTotal;
    }

    public void setCountTotal(Long countTotal) {
        this.countTotal = countTotal;
    }
}
