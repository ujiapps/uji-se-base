package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class Oferta implements Serializable {

    private Long id;
    private String nombre;
    private Long actividad;

    public Oferta() {
    }

    @QueryProjection
    public Oferta(Long id, Long actividad) {
        this.id = id;
        this.actividad = actividad;
    }

    @QueryProjection
    public Oferta(Long id, Long actividad, String nombre) {
        this.id = id;
        this.actividad = actividad;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getActividad() {
        return actividad;
    }

    public void setActividad(Long actividad) {
        this.actividad = actividad;
    }
}
