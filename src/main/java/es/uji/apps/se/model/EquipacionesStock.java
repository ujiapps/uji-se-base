package es.uji.apps.se.model;

public class EquipacionesStock {

    private Long equipacionId;
    private Long stock;
    private Long dorsal;
    private String talla;
    private String equipacionTipoNombre;
    private String equipacionModeloNombre;
    private String equipacionGeneroNombre;

    public EquipacionesStock() { }

    public EquipacionesStock(Long equipacionId, Long stock, Long dorsal, String talla, String equipacionTipoNombre, String equipacionModeloNombre, String equipacionGeneroNombre) {
        this.equipacionId = equipacionId;
        this.stock = stock;
        this.dorsal = dorsal;
        this.talla = talla;
        this.equipacionTipoNombre = equipacionTipoNombre;
        this.equipacionModeloNombre = equipacionModeloNombre;
        this.equipacionGeneroNombre = equipacionGeneroNombre;
    }

    public Long getEquipacionId() { return equipacionId; }
    public void setEquipacionId(Long equipacionId) { this.equipacionId = equipacionId; }

    public Long getStock() { return stock; }
    public void setStock(Long stock) { this.stock = stock; }

    public Long getDorsal() { return dorsal; }
    public void setDorsal(Long dorsal) { this.dorsal = dorsal; }

    public String getTalla() { return talla; }
    public void setTalla(String talla) { this.talla = talla; }

    public String getEquipacionTipoNombre() { return equipacionTipoNombre; }
    public void setEquipacionTipoNombre(String equipacionTipoNombre) { this.equipacionTipoNombre = equipacionTipoNombre; }

    public String getEquipacionModeloNombre() { return equipacionModeloNombre; }
    public void setEquipacionModeloNombre(String equipacionModeloNombre) { this.equipacionModeloNombre = equipacionModeloNombre; }

    public String getEquipacionGeneroNombre() { return equipacionGeneroNombre; }
    public void setEquipacionGeneroNombre(String equipacionGeneroNombre) { this.equipacionGeneroNombre = equipacionGeneroNombre; }
}