package es.uji.apps.se.model;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class AccesosFranjaHoraria implements Serializable {

    private String franjaHoraria;
    private Long countZonaAireLibre;
    private Long countZonaPabellon;
    private Long countZonaPiscina;
    private Long countZonaRaquetas;
    private Long countTotal;


    public AccesosFranjaHoraria() {
    }

    @QueryProjection
    public AccesosFranjaHoraria(String franjaHoraria) {
        this.franjaHoraria = franjaHoraria;
        this.countZonaAireLibre = 0L;
        this.countZonaPabellon = 0L;
        this.countZonaPiscina = 0L;
        this.countZonaRaquetas = 0L;
        this.countTotal = 0L;
    }

    public String getFranjaHoraria() {
        return franjaHoraria;
    }

    public void setFranjaHoraria(String franjaHoraria) {
        this.franjaHoraria = franjaHoraria;
    }

    public Long getCountZonaAireLibre() {
        return countZonaAireLibre;
    }

    public void setCountZonaAireLibre(Long countZonaAireLibre) {
        this.countZonaAireLibre = countZonaAireLibre;
    }

    public Long getCountZonaPabellon() {
        return countZonaPabellon;
    }

    public void setCountZonaPabellon(Long countZonaPabellon) {
        this.countZonaPabellon = countZonaPabellon;
    }

    public Long getCountZonaPiscina() {
        return countZonaPiscina;
    }

    public void setCountZonaPiscina(Long countZonaPiscina) {
        this.countZonaPiscina = countZonaPiscina;
    }

    public Long getCountZonaRaquetas() {
        return countZonaRaquetas;
    }

    public void setCountZonaRaquetas(Long countZonaRaquetas) {
        this.countZonaRaquetas = countZonaRaquetas;
    }

    public Long getCountTotal() {
        return countTotal;
    }

    public void setCountTotal(Long countTotal) {
        this.countTotal = countTotal;
    }
}
