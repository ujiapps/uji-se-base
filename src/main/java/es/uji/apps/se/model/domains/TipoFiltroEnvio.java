package es.uji.apps.se.model.domains;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum TipoFiltroEnvio
{
    TARJETA(1L),
    BONOS(2L),
    CLASES(3L),
    CLASESINSTALACIONES(4L),
    CLASESCLASES(5L),
    PERSONAS(9L),
    ACTIVIDADES(10L),
//    SE(11L),
    ALQUILER(13L),
    BICICLETA(15L);

    private final Long id;

    TipoFiltroEnvio(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public static List<Long> getAll(){
        return Arrays.stream(TipoFiltroEnvio.values()).map(z-> z.getId()).collect(Collectors.toList());
    }

}
