package es.uji.apps.se.model.filtros;

public class FiltroActividades {

    private Long periodoId;
    private Long clasificacionId;
    private Long actividadId;
    private Long grupoId;
    private String busquedaNombre;

    public FiltroActividades(Long periodoId, Long clasificacionId, Long actividadId, Long grupoId) {
        this.periodoId = periodoId;
        this.clasificacionId = clasificacionId;
        this.actividadId = actividadId;
        this.grupoId = grupoId;
    }

    public FiltroActividades(Long periodoId, Long clasificacionId, String busquedaNombre) {
        this.periodoId = periodoId;
        this.clasificacionId = clasificacionId;
        this.busquedaNombre = busquedaNombre;
    }


    public Long getPeriodoId() {
        return periodoId;
    }

    public void setPeriodoId(Long periodoId) {
        this.periodoId = periodoId;
    }

    public Long getClasificacionId() {
        return clasificacionId;
    }

    public void setClasificacionId(Long clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }

    public String getBusquedaNombre() {
        return busquedaNombre;
    }

    public void setBusquedaNombre(String busquedaNombre) {
        this.busquedaNombre = busquedaNombre;
    }
}
