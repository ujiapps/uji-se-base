package es.uji.apps.se.model;

public class EquipacionActividad {
    private Long id;
    private Long equipacionId;
    private Long actividadId;
    private Long cursoAcademico;
    private Long vez;
    private Long stockMin;
    private String equipacionNombre;
    private String actividadNombre;

    public EquipacionActividad() {}

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getEquipacionId() { return equipacionId; }
    public void setEquipacionId(Long equipacionId) { this.equipacionId = equipacionId; }

    public Long getActividadId() { return actividadId; }
    public void setActividadId(Long actividadId) { this.actividadId = actividadId; }

    public Long getCursoAcademico() { return cursoAcademico; }
    public void setCursoAcademico(Long cursoAcademico) { this.cursoAcademico = cursoAcademico; }

    public Long getVez() { return vez; }
    public void setVez(Long vez) { this.vez = vez; }

    public Long getStockMin() { return stockMin; }
    public void setStockMin(Long stockMin) { this.stockMin = stockMin; }

    public String getEquipacionNombre() { return equipacionNombre; }
    public void setEquipacionNombre(String equipacionNombre) { this.equipacionNombre = equipacionNombre; }

    public String getActividadNombre() { return actividadNombre; }
    public void setActividadNombre(String actividadNombre) { this.actividadNombre = actividadNombre; }
}
