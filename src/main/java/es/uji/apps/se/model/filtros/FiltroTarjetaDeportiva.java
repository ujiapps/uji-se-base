package es.uji.apps.se.model.filtros;

import es.uji.commons.rest.ParamUtils;

import java.util.Calendar;
import java.util.Date;

public class FiltroTarjetaDeportiva {

    private Date fechaAltaDesde;
    private Date fechaAltaHasta;
    private Date fechaBajaDesde;
    private Date fechaBajaHasta;
    private String busqueda;
    private Long tarjetaDeportivaTipoId;
    private Long searchSancionados;

    public FiltroTarjetaDeportiva(String busqueda, Long tarjetaDeportivaTipoId, Date fechaAltaDesde, Date fechaAltaHasta, Date fechaBajaDesde, Date fechaBajaHasta, Long searchSancionados) {
        this.fechaAltaDesde = fechaAltaDesde;
        if (ParamUtils.isNotNull(fechaAltaHasta)) {
            Calendar calendarFechaAltaHasta = Calendar.getInstance();
            calendarFechaAltaHasta.setTime(fechaAltaHasta);
            calendarFechaAltaHasta.add(Calendar.DATE, 1);
            this.fechaAltaHasta = calendarFechaAltaHasta.getTime();
        }
        this.fechaBajaDesde = fechaBajaDesde;
        if (ParamUtils.isNotNull(fechaBajaHasta)) {
            Calendar calendarFechaBajaHasta = Calendar.getInstance();
            calendarFechaBajaHasta.setTime(fechaBajaHasta);
            calendarFechaBajaHasta.add(Calendar.DATE, 1);
            this.fechaBajaHasta = calendarFechaBajaHasta.getTime();
        }
        this.busqueda = busqueda;
        this.tarjetaDeportivaTipoId = tarjetaDeportivaTipoId;
        this.searchSancionados = searchSancionados;
    }

    public Date getFechaAltaDesde() {
        return fechaAltaDesde;
    }

    public void setFechaAltaDesde(Date fechaAltaDesde) {
        this.fechaAltaDesde = fechaAltaDesde;
    }

    public Date getFechaAltaHasta() {
        return fechaAltaHasta;
    }

    public void setFechaAltaHasta(Date fechaAltaHasta) {
        this.fechaAltaHasta = fechaAltaHasta;
    }

    public Date getFechaBajaDesde() {
        return fechaBajaDesde;
    }

    public void setFechaBajaDesde(Date fechaBajaDesde) {
        this.fechaBajaDesde = fechaBajaDesde;
    }

    public Date getFechaBajaHasta() {
        return fechaBajaHasta;
    }

    public void setFechaBajaHasta(Date fechaBajaHasta) {
        this.fechaBajaHasta = fechaBajaHasta;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public Long getTarjetaDeportivaTipoId() {
        return tarjetaDeportivaTipoId;
    }

    public void setTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId) {
        this.tarjetaDeportivaTipoId = tarjetaDeportivaTipoId;
    }

    public Long getSearchSancionados() {
        return searchSancionados;
    }

    public void setSearchSancionados(Long searchSancionados) {
        this.searchSancionados = searchSancionados;
    }
}
