package es.uji.apps.se.model;

public class IndicadorSatisfaccion {
    private Long id;
    private Long periodoId;
    private String periodoNombre;
    private Long actividadId;
    private Long ofertaId;
    private Float valor;
    private Long cursoId;
    private Long indicadorTipoId;
    private String indicadorTipoNombre;
    private Long tipoTempo;
    private String tipoTempoNombre;
    private Long encuestas;
    private Long poblacion;

    public IndicadorSatisfaccion () { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getPeriodoId() { return periodoId; }
    public void setPeriodoId(Long periodoId) { this.periodoId = periodoId; }

    public String getPeriodoNombre() { return periodoNombre; }
    public void setPeriodoNombre(String periodoNombre) { this.periodoNombre = periodoNombre; }

    public Long getActividadId() { return actividadId; }
    public void setActividadId(Long actividadId) { this.actividadId = actividadId; }

    public Long getOfertaId() { return ofertaId; }
    public void setOfertaId(Long ofertaId) { this.ofertaId = ofertaId; }

    public Float getValor() { return valor; }
    public void setValor(Float valor) { this.valor = valor; }

    public void setCursoId(Long cursoId) { this.cursoId = cursoId; }
    public Long getCursoId() { return cursoId; }

    public Long getIndicadorTipoId() { return indicadorTipoId; }
    public void setIndicadorTipoId(Long indicadorTipoId) { this.indicadorTipoId = indicadorTipoId; }

    public String getIndicadorTipoNombre() { return indicadorTipoNombre; }
    public void setIndicadorTipoNombre(String indicadorTipoNombre) { this.indicadorTipoNombre = indicadorTipoNombre; }

    public Long getTipoTempo() { return tipoTempo; }
    public void setTipoTempo(Long tipoTempo) { this.tipoTempo = tipoTempo; }

    public String getTipoTempoNombre() { return tipoTempoNombre; }
    public void setTipoTempoNombre(String tipoTempoNombre) { this.tipoTempoNombre = tipoTempoNombre; }

    public Long getEncuestas() { return encuestas; }
    public void setEncuestas(Long encuestas) { this.encuestas = encuestas; }

    public Long getPoblacion() { return poblacion; }
    public void setPoblacion(Long poblacion) { this.poblacion = poblacion; }
}
