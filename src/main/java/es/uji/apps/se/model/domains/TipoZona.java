package es.uji.apps.se.model.domains;

public enum TipoZona
{
    RAQUETAS(1L),
    PABELLON(2L),
    PISCINA(3L),
    AIRELIBRE(4L),
    MUSCULACION(5L);

    private final Long id;

    TipoZona(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
}
