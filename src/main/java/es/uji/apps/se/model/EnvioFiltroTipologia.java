package es.uji.apps.se.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.model.domains.TipoFiltroTipologiaEnvio;
import es.uji.commons.rest.ParamUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnvioFiltroTipologia implements Serializable {
    private Long id;
    private Long envio;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Long> vinculos;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Boolean usuarios;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Boolean monitores;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Boolean morosos;

    private FiltroTipologiaElite elites;


    public EnvioFiltroTipologia() {
    }

    public EnvioFiltroTipologia(Long envioId, List<EnvioFiltroDTO> envioFiltroTipologias) {

        this.envio = envioId;
        this.vinculos = envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.VINCULOS.getId())).map(filtro -> filtro.getKey()).collect(Collectors.toList());

        if (envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.USUARIOS.getId())).map(filtro -> filtro.getKey()).collect(Collectors.toList()).size() > 0) {
            if (envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.USUARIOS.getId())).map(filtro -> filtro.getKey()).collect(Collectors.toList()).get(0).equals(1L))
                this.usuarios = Boolean.TRUE;
            else
                this.usuarios = Boolean.FALSE;

        }
        if (envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.MONITORES.getId())).map(filtro -> filtro.getKey()).collect(Collectors.toList()).size() > 0) {
            if (envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.MONITORES.getId())).map(filtro -> filtro.getKey()).collect(Collectors.toList()).get(0).equals(1L))
                this.monitores = Boolean.TRUE;
            else
                this.monitores = Boolean.FALSE;
        }


        List<FiltroTipologiaElite> filtroTipologiaElites = envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.ELITE.getId())).map(filtro -> {

            FiltroTipologiaElite filtroTipologiaElite = new FiltroTipologiaElite();
            filtroTipologiaElite.setElite(Boolean.FALSE);
            filtroTipologiaElite.setCursoAcademico(null);

            if (filtro.getKey().equals(1L)) {
                filtroTipologiaElite.setElite(Boolean.TRUE);
                if (ParamUtils.isNotNull(filtro.getValue())) {
                    JSONArray array = null;
                    try {
                        array = new JSONArray(filtro.getValue());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        if (ParamUtils.isNotNull(array)) {
                            JSONObject obj = array.getJSONObject(0);
                            if (ParamUtils.isNotNull(obj.get("cursoAcademico"))) {
                                filtroTipologiaElite.setCursoAcademico((Integer) obj.get("cursoAcademico"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            return filtroTipologiaElite;
        }).collect(Collectors.toList());

        if (!filtroTipologiaElites.isEmpty())
            this.elites = filtroTipologiaElites.get(0);

        if (envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.MOROSO.getId())).map(filtro -> filtro.getKey()).collect(Collectors.toList()).size() > 0) {
            if (envioFiltroTipologias.stream().filter(filtro -> filtro.getTipoFiltro().equals(TipoFiltroTipologiaEnvio.MOROSO.getId())).map(filtro -> filtro.getKey()).collect(Collectors.toList()).get(0).equals(1L))
                this.morosos = Boolean.TRUE;
            else
                this.morosos = Boolean.FALSE;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEnvio() {
        return envio;
    }

    public void setEnvio(Long envio) {
        this.envio = envio;
    }

    public List<Long> getVinculos() {
        return vinculos;
    }

    public void setVinculos(List<Long> vinculos) {
        this.vinculos = vinculos;
    }

    public Boolean isUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Boolean usuarios) {
        this.usuarios = usuarios;
    }

    public Boolean isMonitores() {
        return monitores;
    }

    public void setMonitores(Boolean monitores) {
        this.monitores = monitores;
    }

    public FiltroTipologiaElite getElites() {
        return elites;
    }

    public void setElites(FiltroTipologiaElite elites) {
        this.elites = elites;
    }

    public Boolean isMorosos() {
        return morosos;
    }

    public void setMorosos(Boolean morosos) {
        this.morosos = morosos;
    }
}
