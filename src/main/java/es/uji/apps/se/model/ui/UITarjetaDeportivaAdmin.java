package es.uji.apps.se.model.ui;

import es.uji.apps.se.dto.TarjetaDeportivaDTO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import java.util.Date;

public class UITarjetaDeportivaAdmin {
    private Long id;
    private Long tarjetaDeportivaTipoId;
    private Long personaId;
    private String movil;
    private String email;
    private String nombre;
    private String nombrePersona;
    private String vinculoNombre;
    private Date fechaAlta;
    private Date fechaBaja;
    private Long recibo;

    public UITarjetaDeportivaAdmin(TarjetaDeportivaDTO tarjetaDeportivaDTO, UIEntity recibo) {
        this.id = tarjetaDeportivaDTO.getId();
        this.tarjetaDeportivaTipoId = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getId();
        this.fechaAlta = tarjetaDeportivaDTO.getFechaAlta();
        this.fechaBaja = tarjetaDeportivaDTO.getFechaBaja();
        this.nombre = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getNombre();
        this.nombrePersona = tarjetaDeportivaDTO.getPersonaUji().getApellidosNombre();
        this.personaId = tarjetaDeportivaDTO.getPersonaUji().getId();
        this.movil = tarjetaDeportivaDTO.getPersonaUji().getMovil();
        this.email = tarjetaDeportivaDTO.getPersonaUji().getMail();
        this.vinculoNombre = tarjetaDeportivaDTO.getVinculoNombre();
        if (ParamUtils.isNotNull(recibo))
            this.recibo = ParamUtils.parseLong(recibo.get("id"));
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public Long getRecibo() {
        return recibo;
    }

    public void setRecibo(Long recibo) {
        this.recibo = recibo;
    }

    public Long getTarjetaDeportivaTipoId() {
        return tarjetaDeportivaTipoId;
    }

    public void setTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId) {
        this.tarjetaDeportivaTipoId = tarjetaDeportivaTipoId;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVinculoNombre() {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre) {
        this.vinculoNombre = vinculoNombre;
    }
}
