package es.uji.apps.se.model;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.Date;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.dto.TarjetaDeportivaDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTarifaDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.rest.ParamUtils;

public class TarjetaDeportiva {
    private Long id;
    private String nombre;
    private String nombrePersona;
    private String nombreInformal;
    private Boolean permiteInstalaciones;
    private Boolean permiteMusculacion;
    private Boolean permiteClasesDirigidas;
    private Date fechaAlta;
    private Date fechaBaja;
    private String vinculo;
    private String importe;
    private Boolean periodoRenovacion;
    private Long tarjetaDeportivaTipoId;
    private String tipoPlantilla;
    private Boolean activa;
    private TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO;
    private String imagenTarjeta;

    private static DecimalFormat df = new DecimalFormat("0.00");

    private static final int DIAS_RENOVACION = 15;

    public TarjetaDeportiva() {
    }

    @QueryProjection
    public TarjetaDeportiva(Long id,
                            String tipoTarjetaNombre,
                            String nombrePersona,
                            String apellido1,
                            String apellido2,
                            Boolean permiteInstalaciones,
                            Boolean permiteMusculacion,
                            Boolean permiteClasesDirigidas,
                            Date fechaAlta,
                            Date fechaBaja,
                            Long tarjetaDeportivaTipoId,
                            String tipoPlantilla,
                            String imagenTarjeta) {
        this.id = id;
        this.nombre = tipoTarjetaNombre;
        this.nombrePersona = this.getApellidosNombre(nombrePersona, apellido1, apellido2);
        this.nombreInformal = this.getNombreApellidos(nombrePersona, apellido1, apellido2);
        this.permiteInstalaciones = permiteInstalaciones;
        this.permiteMusculacion = permiteMusculacion;
        this.permiteClasesDirigidas = permiteClasesDirigidas;
        this.fechaAlta = fechaAlta;
        this.fechaBaja = fechaBaja;
        this.periodoRenovacion = this.isPeriodoRenovacionTarjeta();
        this.tarjetaDeportivaTipoId = tarjetaDeportivaTipoId;
        this.tipoPlantilla = (ParamUtils.isNotNull(tipoPlantilla)) ? tipoPlantilla.toLowerCase() : "generica";
        this.activa = Boolean.TRUE;
        this.imagenTarjeta = (ParamUtils.isNotNull(imagenTarjeta)) ? imagenTarjeta.toLowerCase() : "";
    }

    public TarjetaDeportiva(TarjetaDeportivaDTO tarjetaDeportivaDTO) {
        this.id = tarjetaDeportivaDTO.getId();
        this.activa = Boolean.TRUE;
        this.fechaAlta = tarjetaDeportivaDTO.getFechaAlta();
        this.fechaBaja = tarjetaDeportivaDTO.getFechaBaja();
        this.nombre = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getNombre();
        this.tarjetaDeportivaTipoId = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getId();
        this.tarjetaDeportivaTipoDTO = tarjetaDeportivaDTO.getTarjetaDeportivaTipo();
        this.permiteInstalaciones = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().isPermiteInstalaciones();
        this.permiteMusculacion = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().isPermiteMusculacion();
        this.permiteClasesDirigidas = tarjetaDeportivaDTO.getTarjetaDeportivaTipo().isPermiteClasesDirigidas();
        this.nombrePersona = tarjetaDeportivaDTO.getPersonaUji().getApellidosNombre();
        this.nombreInformal = tarjetaDeportivaDTO.getPersonaUji().getNombreApellidos();
        this.periodoRenovacion = tarjetaDeportivaDTO.isPeriodoRenovacion();
        this.tipoPlantilla = (ParamUtils.isNotNull(tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getTipoPlantilla())) ?
                tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getTipoPlantilla().toLowerCase() : "generica";
        this.imagenTarjeta = (ParamUtils.isNotNull(tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getImagenTarjeta())) ?
                tarjetaDeportivaDTO.getTarjetaDeportivaTipo().getImagenTarjeta().toLowerCase() : "";
    }

    public TarjetaDeportiva(TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO, PersonaUjiDTO personaUjiDTO, TarjetaDeportiva tarjetaDeportivaActiva) {
        this.id = tarjetaDeportivaTipoDTO.getId();
        this.nombre = tarjetaDeportivaTipoDTO.getNombre();
        this.permiteInstalaciones = tarjetaDeportivaTipoDTO.isPermiteInstalaciones();
        this.permiteMusculacion = tarjetaDeportivaTipoDTO.isPermiteMusculacion();
        this.permiteClasesDirigidas = tarjetaDeportivaTipoDTO.isPermiteClasesDirigidas();
        this.tipoPlantilla = (ParamUtils.isNotNull(tarjetaDeportivaTipoDTO.getTipoPlantilla())) ? tarjetaDeportivaTipoDTO.getTipoPlantilla().toLowerCase() : "generica";
        this.imagenTarjeta = (ParamUtils.isNotNull(tarjetaDeportivaTipoDTO.getImagenTarjeta())) ? tarjetaDeportivaTipoDTO.getImagenTarjeta().toLowerCase() : "";
        this.activa = Boolean.TRUE;
        this.nombrePersona = personaUjiDTO.getApellidosNombre();
        this.nombreInformal = personaUjiDTO.getNombreApellidos();

        if (ParamUtils.isNotNull(tarjetaDeportivaActiva) && !tarjetaDeportivaActiva.getPeriodoRenovacion()) {
            this.activa = Boolean.FALSE;

            //TODO: Revisar código, ver si se puede cambiar la forma de recoger esta información para no tener que crear la TarjetaDeportiva con todo el DTO de tarjetaDeportivaTipo.
//            tarjetaDeportivaActiva.getTarjetaDeportivaTipo().getTarjetaDeportivaSuperiores().stream().map(t -> {
//                if (t.getTarjetaDeportivaTipoSuperior().getId().equals(tarjetaDeportivaTipoDTO.getId())) {
//                    this.activa = Boolean.TRUE;
//                }
//                return null;
//            }).collect(Collectors.toList());
        }

        TarjetaDeportivaTarifaDTO tarjetaDeportivaTarifaDTO = tarjetaDeportivaTipoDTO.getTarifaByVinculo(personaUjiDTO.getVinculoId());

        if (tarjetaDeportivaTarifaDTO != null) {
            this.vinculo = tarjetaDeportivaTarifaDTO.getVinculo().getNombre();
            this.importe = df.format(tarjetaDeportivaTarifaDTO.getImporte());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getPermiteInstalaciones() {
        return permiteInstalaciones;
    }

    public void setPermiteInstalaciones(Boolean permiteInstalaciones) {
        this.permiteInstalaciones = permiteInstalaciones;
    }

    public Boolean getPermiteMusculacion() {
        return permiteMusculacion;
    }

    public void setPermiteMusculacion(Boolean permiteMusculacion) {
        this.permiteMusculacion = permiteMusculacion;
    }

    public Boolean getPermiteClasesDirigidas() {
        return permiteClasesDirigidas;
    }

    public void setPermiteClasesDirigidas(Boolean permiteClasesDirigidas) {
        this.permiteClasesDirigidas = permiteClasesDirigidas;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getVinculo() {
        return vinculo;
    }

    public void setVinculo(String vinculo) {
        this.vinculo = vinculo;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public Boolean getPeriodoRenovacion() {
        return periodoRenovacion;
    }

    public void setPeriodoRenovacion(Boolean periodoRenovacion) {
        this.periodoRenovacion = periodoRenovacion;
    }

    public String toStringShort() {
        return nombrePersona;
    }

    public Long getTarjetaDeportivaTipoId() {
        return tarjetaDeportivaTipoId;
    }

    public void setTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId) {
        this.tarjetaDeportivaTipoId = tarjetaDeportivaTipoId;
    }

    public String getNombreInformal() {
        return nombreInformal;
    }

    public void setNombreInformal(String nombreInformal) {
        this.nombreInformal = nombreInformal;
    }

    public String getTipoPlantilla() {
        return tipoPlantilla;
    }

    public void setTipoPlantilla(String tipoPlantilla) {
        this.tipoPlantilla = tipoPlantilla;
    }

    public Boolean getActiva() {
        return activa;
    }

    public Boolean isActiva() {
        return activa;
    }

    public void setActiva(Boolean activa) {
        this.activa = activa;
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportivaTipo() {
        return tarjetaDeportivaTipoDTO;
    }

    public void setTarjetaDeportivaTipo(TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO) {
        this.tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoDTO;
    }

    public String getImagenTarjeta() {
        return imagenTarjeta;
    }

    public void setImagenTarjeta(String imagenTarjeta) {
        this.imagenTarjeta = imagenTarjeta;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} - {1} fins {2}", nombrePersona, nombre, DateExtensions.getDateAsString(fechaBaja));
    }

    private boolean isPeriodoRenovacionTarjeta()
    {
        Date fechaActual = DateExtensions.addDays(DateExtensions.getCurrentDate(), -1);
        return (fechaActual.before(this.fechaBaja) && fechaActual.after(DateExtensions.addDays(fechaBaja, -DIAS_RENOVACION)));
    }

    private String getApellidosNombre(String nombre, String apellido1, String apellido2) {
        return String.format("%s %s, %s", apellido1, apellido2, nombre);
    }

    private String getNombreApellidos(String nombre, String apellido1, String apellido2) {
        return String.format("%s %s %s", nombre, apellido1, apellido2);
    }
}
