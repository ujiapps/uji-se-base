package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QAccesosPorPerfilDTO;
import es.uji.apps.se.dto.QTipoDTO;
import es.uji.apps.se.model.AccesosPorPerfil;
import es.uji.apps.se.model.QAccesosPorPerfil;
import es.uji.apps.se.model.domains.TipoZona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AccesosPorPerfilDAO extends BaseDAODatabaseImpl {
    public List<AccesosPorPerfil> getAccesosPorPerfil(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {

        QTipoDTO qVinculos = QTipoDTO.tipoDTO;
        JPAQuery queryVinculos = new JPAQuery(entityManager);
        queryVinculos.from(qVinculos).where(qVinculos.tipoPadreDTO.id.eq(6229L));
        List<AccesosPorPerfil> accesos = queryVinculos.list(new QAccesosPorPerfil(qVinculos.id, qVinculos.nombre));

        QAccesosPorPerfilDTO qAccesosPorPerfilDTO = QAccesosPorPerfilDTO.accesosPorPerfilDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAccesosPorPerfilDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qAccesosPorPerfilDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qAccesosPorPerfilDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query.where(qAccesosPorPerfilDTO.horaEntrada.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query.where(qAccesosPorPerfilDTO.horaEntrada.loe(horaHasta));
        }

        List<Tuple> lista = query.groupBy(qAccesosPorPerfilDTO.vinculo, qAccesosPorPerfilDTO.zonaId)
                .list(qAccesosPorPerfilDTO.vinculo, qAccesosPorPerfilDTO.zonaId, qAccesosPorPerfilDTO.count());


        lista.stream().forEach(acceso -> {
            accesos.forEach(accesoPerfil -> {
                if (accesoPerfil.getVinculoId().equals(acceso.get(qAccesosPorPerfilDTO.vinculo))) {
                    if (TipoZona.RAQUETAS.getId().equals(acceso.get(qAccesosPorPerfilDTO.zonaId))) {
                        accesoPerfil.setCountZonaRaquetas(acceso.get(qAccesosPorPerfilDTO.count()));
                    }
                    if (TipoZona.PABELLON.getId().equals(acceso.get(qAccesosPorPerfilDTO.zonaId))) {
                        accesoPerfil.setCountZonaPabellon(acceso.get(qAccesosPorPerfilDTO.count()));
                    }
                    if (TipoZona.PISCINA.getId().equals(acceso.get(qAccesosPorPerfilDTO.zonaId))) {
                        accesoPerfil.setCountZonaPiscina(acceso.get(qAccesosPorPerfilDTO.count()));
                    }
                    if (TipoZona.AIRELIBRE.getId().equals(acceso.get(qAccesosPorPerfilDTO.zonaId))) {
                        accesoPerfil.setCountZonaAireLibre(acceso.get(qAccesosPorPerfilDTO.count()));
                    }
                    accesoPerfil.setCountTotal(accesoPerfil.getCountTotal()+acceso.get(qAccesosPorPerfilDTO.count()));
                }
            });
        });
        return accesos.stream().sorted(Comparator.comparing(AccesosPorPerfil::getVinculo)).collect(Collectors.toList());
    }
}
