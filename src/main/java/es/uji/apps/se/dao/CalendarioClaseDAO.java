package es.uji.apps.se.dao;

import java.util.Date;
import java.util.List;

import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CalendarioClaseDAO extends BaseDAODatabaseImpl {

    private final QCalendarioClaseDTO qCalendarioClase = QCalendarioClaseDTO.calendarioClaseDTO;


    public List<CalendarioClaseDTO> getCalendariosClase(CursoAcademico cursoAcademico, Long clase, Paginacion paginacion, String columna, String orden) {
        JPAQuery query = new JPAQuery(entityManager);

        QCalendarioDTO calendario = QCalendarioDTO.calendarioDTO;

        query.from(qCalendarioClase).join(qCalendarioClase.calendarioDTO, calendario)
                .where(qCalendarioClase.claseDTO.id.eq(clase)
                        .and(calendario.dia.between(cursoAcademico.getFechaInicio(), cursoAcademico.getFechaFin())));
        if (ParamUtils.isNotNull(columna)) {
            if (columna.equalsIgnoreCase("FECHA")) {
                if (orden.equalsIgnoreCase("ASC")) {
                    query.orderBy(calendario.dia.asc());
                } else {
                    query.orderBy(calendario.dia.desc());
                }
            }
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());

            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.list(qCalendarioClase);
    }

    public List<CalendarioClaseVistaDTO> getCalendariosClaseByRangoFechas(Date fechaInicio, Date fechaFin, Long connectedUserId){
        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioClaseVistaDTO qCalendarioClaseVista = QCalendarioClaseVistaDTO.calendarioClaseVistaDTO;

        query.from(qCalendarioClaseVista)
                .where(qCalendarioClaseVista.dia.between(fechaInicio, fechaFin)
                .and(qCalendarioClaseVista.personaId.eq(connectedUserId)));
        return query.list(qCalendarioClaseVista);
    }

    public CalendarioClaseDTO getCalendariosClaseById(Long actividadId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCalendarioDTO calendario = QCalendarioDTO.calendarioDTO;
        QClaseDTO qClase = QClaseDTO.claseDTO;
        QTarjetaDeportivaClaseDTO qTarjetaDeportivaClase = QTarjetaDeportivaClaseDTO.tarjetaDeportivaClaseDTO;

        query.from(qCalendarioClase)
                .join(qCalendarioClase.calendarioDTO, calendario).fetch()
                .join(qCalendarioClase.claseDTO, qClase).fetch()
                .leftJoin(qCalendarioClase.tarjetaDeportivaClasesDTO, qTarjetaDeportivaClase).fetch()
                .where(qCalendarioClase.id.eq(actividadId));

        List<CalendarioClaseDTO> calendarioClaseDTO = query.distinct().list(qCalendarioClase);
        if (calendarioClaseDTO.isEmpty()) return null;
        return calendarioClaseDTO.get(0);
    }

    public List<CalendarioClaseDTO> getCalendarioClaseByClaseIdAndFecha(Long claseId, Date fecha) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCalendarioClase).where(qCalendarioClase.calendarioDTO.dia.eq(fecha).and(qCalendarioClase.claseDTO.id.eq(claseId)));
        return query.list(qCalendarioClase);
    }

    public List<CalendarioClaseDTO> getCalendariosClase(Long clase) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCalendarioClase).where(qCalendarioClase.claseDTO.id.eq(clase));
        return query.list(qCalendarioClase);
    }

    public CalendarioClaseDTO getCalendarioClaseById(Long calendarioClaseId) {
        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioDTO qCalendario = QCalendarioDTO.calendarioDTO;

        query.from(qCalendarioClase).join(qCalendarioClase.calendarioDTO, qCalendario).fetch()
                .where(qCalendarioClase.id.eq(calendarioClaseId));
        List<CalendarioClaseDTO> res = query.list(qCalendarioClase);

        if (!res.isEmpty()) return res.get(0);
        return null;
    }

    public List<CalendarioClaseDTO> getCalendarioClasesByFechas(Date start, Date end, Long instalacionId) {

        JPAQuery query = new JPAQuery(entityManager);

        QCalendarioDTO qCalendario = QCalendarioDTO.calendarioDTO;
        QInstalacionDTO qInstalacion = QInstalacionDTO.instalacionDTO;
        QClaseDTO qClase = QClaseDTO.claseDTO;
        QCalendarioClaseInstalacionDTO qCalendarioClaseInstalacionDTO   = QCalendarioClaseInstalacionDTO.calendarioClaseInstalacionDTO;

        query.from(qCalendarioClase)
                .join(qCalendarioClase.calendarioClaseInstalacionesDTO, qCalendarioClaseInstalacionDTO).fetch()
                .join(qCalendarioClase.claseDTO, qClase).fetch()
                .join(qCalendarioClase.calendarioDTO, qCalendario).fetch()
                .join(qCalendarioClaseInstalacionDTO.instalacionDTO, qInstalacion).fetch()
                .where(qCalendario.dia.between(start, end)).distinct();

        if (ParamUtils.isNotNull(instalacionId)){
            query.where(qInstalacion.id.eq(instalacionId));
        }
        return query.list(qCalendarioClase);
    }

    public List<CalendarioClaseVistaDTO> getCalendariosClaseByFecha(Date fecha, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioClaseVistaDTO qCalendarioClaseVista = QCalendarioClaseVistaDTO.calendarioClaseVistaDTO;

        query.from(qCalendarioClaseVista)
                .where(qCalendarioClaseVista.dia.eq(fecha)
                        .and(qCalendarioClaseVista.personaId.eq(personaId)));
        return query.list(qCalendarioClaseVista);
    }
}
