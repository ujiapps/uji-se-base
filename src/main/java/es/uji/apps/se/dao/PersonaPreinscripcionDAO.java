package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.PreinscripcionDTO;
import es.uji.apps.se.dto.QPreinscripcionDTO;
import es.uji.apps.se.model.PlazaInscripcion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PersonaPreinscripcionDAO extends BaseDAODatabaseImpl {
    private final QPreinscripcionDTO qPreinscripcion = QPreinscripcionDTO.preinscripcionDTO;

    @Transactional
    public void deletePreinscripcion(Long preinscripcionId) {
        new JPADeleteClause(entityManager, qPreinscripcion)
                .where(qPreinscripcion.id.eq(preinscripcionId))
                .execute();
    }

    public Long compruebaPreinscripcion(Long personaId, Long grupoId) {
        return new JPAQuery(entityManager)
                .from(qPreinscripcion)
                .where(qPreinscripcion.personaId.eq(personaId)
                        .and(qPreinscripcion.ofertaId.eq(grupoId)))
                .count();

    }

    @Transactional
    public void addPreinscripcion(PlazaInscripcion plazaInscripcion, Long personaId, Long grupoId) {
        PreinscripcionDTO preinscripcionDTO = new PreinscripcionDTO();
        preinscripcionDTO.setOrigen("USUARIO");
        preinscripcionDTO.setTipo("NI");
        preinscripcionDTO.setPersonaId(personaId);
        preinscripcionDTO.setOfertaId(grupoId);
        preinscripcionDTO.setFechaInicioValida(plazaInscripcion.getFechaInicioValida());
        preinscripcionDTO.setFechaFinValida(plazaInscripcion.getFechaFinValida());
        preinscripcionDTO.setImporte(plazaInscripcion.getImporte());
        preinscripcionDTO.setEstadoId(7495381L);

        this.insert(preinscripcionDTO);
    }
}
