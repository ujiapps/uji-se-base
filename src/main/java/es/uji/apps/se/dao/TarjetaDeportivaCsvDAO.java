package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.model.filtros.FiltroTarjetaDeportiva;
import es.uji.apps.se.dto.views.QTarjetaDeportivaCSV;
import es.uji.apps.se.dto.views.TarjetaDeportivaCSV;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetaDeportivaCsvDAO extends BaseDAODatabaseImpl {

    private QTarjetaDeportivaCSV qTarjetaDeportivaCSV = QTarjetaDeportivaCSV.tarjetaDeportivaCSV;

    public String limpiaAcentos(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim().toUpperCase();
    }

    public List<TarjetaDeportivaCSV> getCsvBySearch(FiltroTarjetaDeportiva filtro) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTarjetaDeportivaCSV);

        if (ParamUtils.isNotNull(filtro.getBusqueda())) {
            query.where(qTarjetaDeportivaCSV.busqueda.like("%" + limpiaAcentos(filtro.getBusqueda()) + "%"));
        }

        if (ParamUtils.isNotNull(filtro.getTarjetaDeportivaTipoId())) {
            query.where(qTarjetaDeportivaCSV.tipoTarjetaId.eq(filtro.getTarjetaDeportivaTipoId()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaAltaDesde())) {
            query.where(qTarjetaDeportivaCSV.fechaAlta.goe(filtro.getFechaAltaDesde()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaAltaHasta())) {
            query.where(qTarjetaDeportivaCSV.fechaAlta.loe(filtro.getFechaAltaHasta()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaBajaDesde())) {
            query.where(qTarjetaDeportivaCSV.fechaBaja.goe(filtro.getFechaBajaDesde()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaBajaHasta())) {
            query.where(qTarjetaDeportivaCSV.fechaBaja.loe(filtro.getFechaBajaHasta()));
        }

        if (ParamUtils.isNotNull(filtro.getSearchSancionados())) {
            if (filtro.getSearchSancionados().equals(1L)) {
                query.where(qTarjetaDeportivaCSV.sancionado.eq("S"));
            } else {
                query.where(qTarjetaDeportivaCSV.sancionado.eq("N"));
            }
        }
        return query.distinct().list(qTarjetaDeportivaCSV);
    }

    public List<TarjetaDeportivaCSV> getCsvByPersonaId(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTarjetaDeportivaCSV).where(qTarjetaDeportivaCSV.personaId.eq(personaId));
        return query.distinct().list(qTarjetaDeportivaCSV);
    }
}
