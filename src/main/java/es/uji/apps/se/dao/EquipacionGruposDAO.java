package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QEquipacionGruposDTO;
import es.uji.apps.se.model.EquipacionGrupos;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.xml.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository("equipacionGruposDAO")
public class EquipacionGruposDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    QEquipacionGruposDTO qEquipacionGruposDTO = QEquipacionGruposDTO.equipacionGruposDTO;

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qEquipacionGruposDTO)
                            .orderBy(qEquipacionGruposDTO.id.asc());

        if(!s.equals("")) {
            try {
                query.where(qEquipacionGruposDTO.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qEquipacionGruposDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query
                .list(qEquipacionGruposDTO.id,
                      qEquipacionGruposDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qEquipacionGruposDTO.id).toString());
                    item.setNombre(tuple.get(qEquipacionGruposDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }

    public List<EquipacionGrupos> getEquipacionGrupos() {
        return new JPAQuery(entityManager)
            .from(qEquipacionGruposDTO)
            .list(qEquipacionGruposDTO).stream().map(tuple -> {
                EquipacionGrupos equipacionGrupos = new EquipacionGrupos();
                equipacionGrupos.setId(tuple.getId());
                equipacionGrupos.setNombre(tuple.getNombre());
                equipacionGrupos.setTipoId(tuple.getTipo().getId());
                equipacionGrupos.setTipoNombre(tuple.getTipo().getNombre());
                equipacionGrupos.setHayQueDevolver(tuple.getHayQueDevolver());
                return equipacionGrupos;
            }).collect(Collectors.toList());
    }

    public void deleteEquipacionGrupos(Long id) {
        new JPADeleteClause(entityManager, qEquipacionGruposDTO).where(qEquipacionGruposDTO.id.eq(id)).execute();
    }
}