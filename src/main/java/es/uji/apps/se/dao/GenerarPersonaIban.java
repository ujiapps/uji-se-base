package es.uji.apps.se.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class GenerarPersonaIban
{
    private GenerarIban generarPersonaIban;

    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        GenerarPersonaIban.dataSource = dataSource;
    }

    @PostConstruct
    public void init()
    {

        this.generarPersonaIban = new GenerarIban(dataSource);
    }

    public void generarIban(Long personaId, String iban)
    {
        generarPersonaIban.generarIban(personaId, iban);
    }

    private class GenerarIban extends StoredProcedure
    {

        private static final String SQL = "uji_sports.inserta_cuenta_bancaria";
        private static final String PPERSONA = "p_perid";
        private static final String PCUENTA = "p_cuenta";

        public GenerarIban(DataSource dataSource)
        {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PCUENTA, Types.VARCHAR));
            compile();
        }

        public void generarIban(Long personaId, String iban)
        {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PPERSONA, personaId);
            inParams.put(PCUENTA, iban);

            Map<String, Object> results = execute(inParams);
        }
    }

}
