package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.CursoAcademicoDTO;
import es.uji.apps.se.dto.QCursoAcademicoDTO;
import es.uji.apps.se.dto.QEliteDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CursoAcademicoDAO extends BaseDAODatabaseImpl {

    public List<CursoAcademicoDTO> getCursosAcademicosUsuario(Long connectedUserId) {
        QEliteDTO qElite = QEliteDTO.eliteDTO;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qElite).where(qElite.personaUjiDTO.id.eq(connectedUserId)).distinct();

        return query.list(qElite.cursoAcademicoDTO);
    }

    public CursoAcademicoDTO getCursoAcademico(Long cursoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoAcademicoDTO qCursoAcademico = QCursoAcademicoDTO.cursoAcademicoDTO;

        List<CursoAcademicoDTO> res = query.from(qCursoAcademico).where(qCursoAcademico.id.eq(cursoId)).list(qCursoAcademico);
        if (res.size()>0) {
            return res.get(0);
        }
        else return new CursoAcademicoDTO();
    }
}
