package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QAsistenciasClasesDirigidasDTO;
import es.uji.apps.se.dto.QCalendarioClaseDTO;
import es.uji.apps.se.dto.QCalendarioDTO;
import es.uji.apps.se.dto.QClaseDTO;
import es.uji.apps.se.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.beans.Expression;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AsistenciasClasesDirigidasDAO extends BaseDAODatabaseImpl {
    public List<AsistenciasClasesDirigidas> getAsistencias(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        QAsistenciasClasesDirigidasDTO qAsistenciasClasesDirigidasDTO = QAsistenciasClasesDirigidasDTO.asistenciasClasesDirigidasDTO;
        QClaseDTO qClaseDTO = QClaseDTO.claseDTO;
        QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsistenciasClasesDirigidasDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qAsistenciasClasesDirigidasDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qAsistenciasClasesDirigidasDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query.where(qAsistenciasClasesDirigidasDTO.horaInicio.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query.where(qAsistenciasClasesDirigidasDTO.horaFin.loe(horaHasta));
        }

        query.groupBy(qAsistenciasClasesDirigidasDTO.nombre,
                qAsistenciasClasesDirigidasDTO.actividadId,
                qAsistenciasClasesDirigidasDTO.horario,
                qAsistenciasClasesDirigidasDTO.horaInicio,
                qAsistenciasClasesDirigidasDTO.horaFin,
                qAsistenciasClasesDirigidasDTO.plazas);
        return query.list(qAsistenciasClasesDirigidasDTO.actividadId,
                        qAsistenciasClasesDirigidasDTO.nombre,
                        qAsistenciasClasesDirigidasDTO.horario,
                        qAsistenciasClasesDirigidasDTO.plazas,
                        qAsistenciasClasesDirigidasDTO.tarjetaDeportiva.count(),
                        qAsistenciasClasesDirigidasDTO.asiste.sum(),
                        qAsistenciasClasesDirigidasDTO.horaInicio,
                        qAsistenciasClasesDirigidasDTO.horaFin)
                .stream()
                .map(tuple -> {
                    AsistenciasClasesDirigidas asistenciasClasesDirigidas = new AsistenciasClasesDirigidas();
                    asistenciasClasesDirigidas.setActividadId(tuple.get(qAsistenciasClasesDirigidasDTO.actividadId));
                    asistenciasClasesDirigidas.setActividad(tuple.get(qAsistenciasClasesDirigidasDTO.nombre));
                    asistenciasClasesDirigidas.setHorario(tuple.get(qAsistenciasClasesDirigidasDTO.horario));
                    int plazas = (ParamUtils.isNotNull(tuple.get(qAsistenciasClasesDirigidasDTO.plazas))) ? tuple.get(qAsistenciasClasesDirigidasDTO.plazas) : 0;
                    asistenciasClasesDirigidas.setPlazas(plazas);
                    QCalendarioClaseDTO qCalendarioClaseDTO = QCalendarioClaseDTO.calendarioClaseDTO;
                    JPAQuery query2 = new JPAQuery(entityManager)
                            .from(qCalendarioClaseDTO);
                    if (ParamUtils.isNotNull(fechaDesde)) {
                        query2.where(qCalendarioClaseDTO.calendarioDTO.dia.goe(fechaDesde));
                    }
                    if (ParamUtils.isNotNull(fechaHasta)) {
                        query2.where(qCalendarioClaseDTO.calendarioDTO.dia.loe(fechaHasta));
                    }

                    if (ParamUtils.isNotNull(horaDesde)) {
                        query2.where(qCalendarioClaseDTO.horaInicio.goe(horaDesde));
                    }

                    if (ParamUtils.isNotNull(horaHasta)) {
                        query2.where(qCalendarioClaseDTO.horaFin.loe(horaHasta));
                    }

                    Long plazasTotales = query2.where(qCalendarioClaseDTO.claseDTO.id.eq(tuple.get(qAsistenciasClasesDirigidasDTO.actividadId))
                            .and(qCalendarioClaseDTO.claseDTO.nombre.eq(tuple.get(qAsistenciasClasesDirigidasDTO.nombre)))
                            .and(qCalendarioClaseDTO.horaInicio.eq(tuple.get(qAsistenciasClasesDirigidasDTO.horaInicio)))
                            .and(qCalendarioClaseDTO.horaFin.eq(tuple.get(qAsistenciasClasesDirigidasDTO.horaFin))))
                    .groupBy(qCalendarioClaseDTO.claseDTO.id, qCalendarioClaseDTO.claseDTO.nombre, qCalendarioClaseDTO.horaInicio,qCalendarioClaseDTO.horaFin)
                    .singleResult(qCalendarioClaseDTO.claseDTO.plazas.sum());

                    asistenciasClasesDirigidas.setPlazasTotales(plazasTotales);
                    int reservas = Math.toIntExact((ParamUtils.isNotNull(tuple.get(qAsistenciasClasesDirigidasDTO.tarjetaDeportiva.count()))) ? tuple.get(qAsistenciasClasesDirigidasDTO.tarjetaDeportiva.count()) : 0);
                    asistenciasClasesDirigidas.setReservas(reservas);
                    int asistencias = Math.toIntExact((ParamUtils.isNotNull(tuple.get(qAsistenciasClasesDirigidasDTO.asiste.sum()))) ? tuple.get(qAsistenciasClasesDirigidasDTO.asiste.sum()) : 0);
                    asistenciasClasesDirigidas.setAsistencias(asistencias);
                    asistenciasClasesDirigidas.setPorcentajeReservas((asistenciasClasesDirigidas.getPlazas() != 0) ? (Float.valueOf(asistenciasClasesDirigidas.getReservas()) * 100) / Float.valueOf(asistenciasClasesDirigidas.getPlazasTotales()) : 0);
                    asistenciasClasesDirigidas.setPorcentajeAsistenciaReservas((asistenciasClasesDirigidas.getReservas() != 0) ? (Float.valueOf(asistenciasClasesDirigidas.getAsistencias()) * 100) / Float.valueOf(asistenciasClasesDirigidas.getReservas()) : 0);
                    asistenciasClasesDirigidas.setPorcentajeAsistenciaOferta((asistenciasClasesDirigidas.getPlazas() != 0) ? (Float.valueOf(asistenciasClasesDirigidas.getAsistencias()) * 100) / Float.valueOf(asistenciasClasesDirigidas.getPlazasTotales()) : 0);
                    return asistenciasClasesDirigidas;
                }).collect(Collectors.toList());
    }
}
