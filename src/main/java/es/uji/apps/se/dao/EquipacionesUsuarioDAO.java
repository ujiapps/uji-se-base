package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EquipacionesUsuarioDAO extends BaseDAODatabaseImpl {
    private QEquipacionesUsuarioDTO qEquipacionesUsuarioDTO = QEquipacionesUsuarioDTO.equipacionesUsuarioDTO;
    private QEquipacionesStockDTO qEquipacionesStockDTO = QEquipacionesStockDTO.equipacionesStockDTO;

    public List<EquipacionesUsuario> getEquipacionesUsuario(Long cursoAca, Long persona, Boolean general) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qEquipacionesUsuarioDTO);

        if (general)
            query.where(qEquipacionesUsuarioDTO.personaId.eq(persona));
        else query.where(qEquipacionesUsuarioDTO.cursoAca.eq(cursoAca).and(qEquipacionesUsuarioDTO.personaId.eq(persona)));

        return query.list(new QEquipacionesUsuario(qEquipacionesUsuarioDTO.actividadNombre,
                qEquipacionesUsuarioDTO.actividadId,
                qEquipacionesUsuarioDTO.inscripcionId,
                qEquipacionesUsuarioDTO.equipacionFechaFin,
                qEquipacionesUsuarioDTO.equipacionId,
                qEquipacionesUsuarioDTO.equipacionNombre,
                qEquipacionesUsuarioDTO.equipacionGrupo,
                qEquipacionesUsuarioDTO.equipacionDev,
                qEquipacionesUsuarioDTO.equipacionStock,
                qEquipacionesUsuarioDTO.fechaMaxDev,
                qEquipacionesUsuarioDTO.tieneVinculadas,
                qEquipacionesUsuarioDTO.equipacionDorsal,
                qEquipacionesUsuarioDTO.equipacionTalla,
                qEquipacionesUsuarioDTO.equipacionInscripcion,
                qEquipacionesUsuarioDTO.fechaDev,
                qEquipacionesUsuarioDTO.observaciones,
                qEquipacionesUsuarioDTO.tipoDev,
                qEquipacionesUsuarioDTO.stock));
    }

    public List<Talla> getTallas(Long equipId) {
        return new JPAQuery(entityManager)
                .from(qEquipacionesStockDTO)
                .where(qEquipacionesStockDTO.equipacionId.eq(equipId))
                .distinct()
                .orderBy(qEquipacionesStockDTO.talla.asc())
                .list(new QTalla(qEquipacionesStockDTO.talla)).stream().map(talla -> {
                    if (talla == null)
                        return new Talla("Talla única");
                    else
                        return talla;
                }).collect(Collectors.toList());
    }

    public List<Talla> getTallasDisponibles(Long equipId) {
        return new JPAQuery(entityManager)
                .from(qEquipacionesStockDTO)
                .where(qEquipacionesStockDTO.equipacionId.eq(equipId)
                        .and(qEquipacionesStockDTO.stock.gt(0)))
                .distinct()
                .orderBy(qEquipacionesStockDTO.talla.asc())
                .list(new QTalla(qEquipacionesStockDTO.talla)).stream().map(talla -> {
                    if (talla == null)
                        return new Talla("Talla única");
                    else
                        return talla;
                }).collect(Collectors.toList());
    }

    public List<DorsalDisponible> getDorsales(Long equipId, String talla) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qEquipacionesStockDTO)
                .where(qEquipacionesStockDTO.equipacionId.eq(equipId)
                        .and(qEquipacionesStockDTO.stock.gt(0)));

        if (talla == null) {
            query.where(qEquipacionesStockDTO.talla.isNull());
        }
        else {
            query.where(qEquipacionesStockDTO.talla.eq(talla));
        }
        query.orderBy(qEquipacionesStockDTO.talla.asc(), qEquipacionesStockDTO.dorsal.asc());
        List<DorsalDisponible> result = query.list(qEquipacionesStockDTO.dorsal).stream().map(tuple -> {
            if (tuple == null) return new DorsalDisponible(null, "Sense numeració");
            else return new DorsalDisponible(tuple, tuple.toString());
        }).collect(Collectors.toList());
        if (result.isEmpty()) {
            result.add(new DorsalDisponible(null, "Sense numeració"));
        }
        return result;
    }

    public List<DorsalDisponible> getAllDorsales(Long equipId, String talla) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qEquipacionesStockDTO)
                .where(qEquipacionesStockDTO.equipacionId.eq(equipId));

        if (talla == null)
            query.where(qEquipacionesStockDTO.talla.isNull());
        else
            query.where(qEquipacionesStockDTO.talla.eq(talla));
        query.orderBy(qEquipacionesStockDTO.talla.asc(), qEquipacionesStockDTO.dorsal.asc());
        return query.list(qEquipacionesStockDTO.dorsal).stream().map(aLong -> {
            if (aLong == null) return new DorsalDisponible(null, "Sense numeració");
            else return new DorsalDisponible(aLong, aLong.toString());
        }).collect(Collectors.toList());
    }

    public Boolean getTieneEquipacion(Long equiId, Long persona, Long cursoAca) {
        StringBuilder sql = new StringBuilder();
        sql.append("select count(*)" +
                " from se_equipaciones_inscripciones ei, se_inscripciones i, se_actividades_oferta o, se_equipaciones_actividades a, se_periodos p, se_actividades_oferta o2, se_inscripciones i2" +
                " where ei.equipacion_id = " + equiId +
                " and ei.inscripcion_id = i.id" +
                " and i.oferta_id = o.id" +
                " and o.periodo_id = p.id" +
                " and p.curso_aca = " + cursoAca +
                " and i.persona_id = " + persona +
                " and o.actividad_id <> a.actividad_id" +
                " and ei.equipacion_id = a.equipacion_id" +
                " and a.curso_aca = " + cursoAca +
                " and a.actividad_id = o2.actividad_id" +
                " and o2.id = i2.oferta_id" +
                " and i2.persona_id = i.persona_id" +
                " and nvl(ei.fecha_devolucion, trunc(sysdate+1)) > trunc(sysdate)");
        Query q = entityManager.createNativeQuery(sql.toString());
        BigDecimal t = (BigDecimal) q.getSingleResult();
        return t.compareTo(new BigDecimal(0)) != 0;
    }
}
