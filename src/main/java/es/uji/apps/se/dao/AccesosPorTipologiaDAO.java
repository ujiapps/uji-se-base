package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QAccesosPorPerfilDTO;
import es.uji.apps.se.dto.QAccesosPorTipologiaDTO;
import es.uji.apps.se.dto.QTipoDTO;
import es.uji.apps.se.model.AccesosPorPerfil;
import es.uji.apps.se.model.AccesosPorTipologia;
import es.uji.apps.se.model.QAccesosPorPerfil;
import es.uji.apps.se.model.domains.TipoZona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AccesosPorTipologiaDAO extends BaseDAODatabaseImpl {
    public List<AccesosPorTipologia> getAccesosPorTipologia(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {

        List<AccesosPorTipologia> accesos = new ArrayList<>();
        accesos.add( new AccesosPorTipologia(1L, "Lloger"));
        accesos.add( new AccesosPorTipologia(2L, "Docencia"));
        accesos.add( new AccesosPorTipologia(3L, "TA"));
        accesos.add( new AccesosPorTipologia(4L, "TEUJI"));
        accesos.add( new AccesosPorTipologia(5L, "TEUJI+"));
        accesos.add( new AccesosPorTipologia(6L, "Campus Saludable"));
        accesos.add( new AccesosPorTipologia(7L, "Bonos"));
        accesos.add( new AccesosPorTipologia(8L, "Altres"));

        QAccesosPorTipologiaDTO qAccesosPorTipologiaDTO = QAccesosPorTipologiaDTO.accesosPorTipologiaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAccesosPorTipologiaDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qAccesosPorTipologiaDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qAccesosPorTipologiaDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query.where(qAccesosPorTipologiaDTO.horaEntrada.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query.where(qAccesosPorTipologiaDTO.horaEntrada.loe(horaHasta));
        }

        List<Tuple> lista = query.groupBy(qAccesosPorTipologiaDTO.tipologiaId, qAccesosPorTipologiaDTO.zonaId)
                .list(qAccesosPorTipologiaDTO.tipologiaId, qAccesosPorTipologiaDTO.zonaId, qAccesosPorTipologiaDTO.count());


        lista.stream().forEach(acceso -> {
            accesos.forEach(accesoTipologia -> {
                if (accesoTipologia.getTipologiaId().equals(acceso.get(qAccesosPorTipologiaDTO.tipologiaId))) {
                    if (TipoZona.RAQUETAS.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                        accesoTipologia.setCountZonaRaquetas(acceso.get(qAccesosPorTipologiaDTO.count()));
                    }
                    if (TipoZona.PABELLON.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                        accesoTipologia.setCountZonaPabellon(acceso.get(qAccesosPorTipologiaDTO.count()));
                    }
                    if (TipoZona.PISCINA.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                        accesoTipologia.setCountZonaPiscina(acceso.get(qAccesosPorTipologiaDTO.count()));
                    }
                    if (TipoZona.AIRELIBRE.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                        accesoTipologia.setCountZonaAireLibre(acceso.get(qAccesosPorTipologiaDTO.count()));
                    }
                    accesoTipologia.setCountTotal(accesoTipologia.getCountTotal()+acceso.get(qAccesosPorTipologiaDTO.count()));
                }
            });
        });

        return accesos.stream().sorted(Comparator.comparing(AccesosPorTipologia::getTipologia)).collect(Collectors.toList());
    }
}
