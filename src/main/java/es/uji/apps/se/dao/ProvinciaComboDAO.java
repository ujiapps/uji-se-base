package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QProvinciaComboDTO;
import es.uji.apps.se.model.ProvinciaCombo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository("provinciaComboDAO")
public class ProvinciaComboDAO extends BaseDAODatabaseImpl {

    private QProvinciaComboDTO qProvinciaComboDTO = QProvinciaComboDTO.provinciaComboDTO;

    public List<ProvinciaCombo> getProvinciaCombo() {

        return new JPAQuery(entityManager).from(qProvinciaComboDTO).list(qProvinciaComboDTO.id,
                        qProvinciaComboDTO.nombre)
                .stream()
                .map(tuple -> {
                    ProvinciaCombo item = new ProvinciaCombo();
                    item.setId(tuple.get(qProvinciaComboDTO.id));
                    item.setNombre(tuple.get(qProvinciaComboDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }
}