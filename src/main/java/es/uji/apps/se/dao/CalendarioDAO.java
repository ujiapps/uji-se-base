package es.uji.apps.se.dao;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.se.dto.CalendarioDTO;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.se.dto.QCalendarioDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CalendarioDAO extends BaseDAODatabaseImpl {
    QCalendarioDTO calendario = QCalendarioDTO.calendarioDTO;

    public List<CalendarioDTO> getDiasSemanaByFechaInicioAndFechaFin(Date fechaInicio, Date fechaFin, Integer diaSemana) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(calendario).where(calendario.dia.between(fechaInicio, fechaFin));

        if (ParamUtils.isNotNull(diaSemana)) {
            return query.list(calendario)
                    .stream().filter(a -> {
                        LocalDate localDate = new java.sql.Date(a.getDia().getTime()).toLocalDate();
                        Integer day = localDate.getDayOfWeek().getValue();
                        return day.equals(diaSemana);
                    }).collect(Collectors.toList());
        }
        else
            return query.list(calendario);
    }

    public CalendarioDTO getCalendarioByFecha(Date fecha) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(calendario).where(calendario.dia.eq(fecha));
        List<CalendarioDTO> lista = query.list(calendario);
        if (lista.size() > 0) {
            return lista.get(0);
        }
        return null;
    }

    public List<CalendarioDTO> getDiasNoFestivos(Date desdeLaFecha, Long numero) {
        JPAQuery query = new JPAQuery(entityManager);
        Date dia = DateExtensions.truncDate(desdeLaFecha);
        query.from(calendario)
                .where(calendario.dia.gt(dia).or(calendario.dia.eq(dia)))
                .orderBy(calendario.dia.asc())
                .limit(numero + 1);
        return query.distinct().list(calendario);
    }

    public Date getPrimeraFechaLimiteReserva(Long offset) {

        Calendar dia = Calendar.getInstance();
        dia.setTime(new Date());

        CalendarioDTO diaCalendarioDTO = getDiasNoFestivos(dia.getTime(), offset).stream().reduce((first, second) -> second)
                .orElse(null);

        Calendar calendario = Calendar.getInstance();
        calendario.setTime(diaCalendarioDTO.getDia());
        calendario.set(Calendar.HOUR_OF_DAY, dia.get(Calendar.HOUR_OF_DAY));
        calendario.set(Calendar.MINUTE, dia.get(Calendar.MINUTE));
        return calendario.getTime();
    }

    public List<CalendarioDTO> getDiasFestivos(int anyo, int mes) {

        JPAQuery query = new JPAQuery(entityManager);

        QCalendarioDTO qCalendario = QCalendarioDTO.calendarioDTO;

        query.from(qCalendario).where(qCalendario.dia.year().eq(anyo).and(qCalendario.dia.month().eq(mes)).and(qCalendario.estado.in("F", "E", "S")));
        return query.list(qCalendario);
    }
}
