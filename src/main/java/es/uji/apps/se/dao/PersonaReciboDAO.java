package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.apps.se.model.Recibo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PersonaReciboDAO extends BaseDAODatabaseImpl{
    private QReciboVWDTO qReciboVWDTO = QReciboVWDTO.reciboVWDTO;
    private QReciboPendienteActividadVWDTO qReciboPendienteActividadVW = QReciboPendienteActividadVWDTO.reciboPendienteActividadVWDTO;
    private QReciboPendienteTarjetaVWDTO qReciboPendienteTarjetaVW = QReciboPendienteTarjetaVWDTO.reciboPendienteTarjetaVWDTO;
    private QReciboPendienteCapitanActividadVWDTO qReciboPendienteCapitanActividadVW = QReciboPendienteCapitanActividadVWDTO.reciboPendienteCapitanActividadVWDTO;

    public PersonaReciboDAO() {}

    public List<es.uji.apps.se.model.Recibo> getRecibos(Paginacion paginacion, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qReciboVWDTO)
                .where(qReciboVWDTO.personaId.eq(personaId)
                        .and(qReciboVWDTO.emisoraId.eq(760L)));

        if (paginacion != null) {
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if(atributoOrdenacion != null) {
                query.orderBy(ordenaRecibos(orden, atributoOrdenacion));
            }
            else
                query.orderBy(qReciboVWDTO.id.desc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.
                list(qReciboVWDTO.id,
                        qReciboVWDTO.tipoNombre,
                        qReciboVWDTO.observaciones,
                        qReciboVWDTO.fechaCreacion,
                        qReciboVWDTO.fechaPago,
                        qReciboVWDTO.importeNeto,
                        qReciboVWDTO.tipoCobroNombre,
                        qReciboVWDTO.fechaPagoLimite)
                .stream()
                .map(tuple -> {
                    es.uji.apps.se.model.Recibo recibo = new Recibo();
                    recibo.setId(tuple.get(qReciboVWDTO.id));
                    String observaciones = tuple.get(qReciboVWDTO.observaciones) != null ? " (".concat(tuple.get(qReciboVWDTO.observaciones)).concat(")") : "";
                    recibo.setTipo(tuple.get(qReciboVWDTO.tipoNombre) + observaciones);
                    recibo.setFechaCreacion(tuple.get(qReciboVWDTO.fechaCreacion));
                    recibo.setFechaPago(tuple.get(qReciboVWDTO.fechaPago));
                    recibo.setImporte(tuple.get(qReciboVWDTO.importeNeto));
                    recibo.setTipoCobro(tuple.get(qReciboVWDTO.tipoCobroNombre));
                    recibo.setFechaPagoLimite(tuple.get(qReciboVWDTO.fechaPagoLimite));
                    return recibo;
                }).collect(Collectors.toList());
    }


    public List<ReciboPendienteActividad> getRecibosPendientesActividades(Paginacion paginacion, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qReciboPendienteActividadVW)
                .where(qReciboPendienteActividadVW.personaId.eq(personaId));

        if (paginacion != null) {
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if(atributoOrdenacion != null) {
                query.orderBy(ordenaRecibosPendientesAct(orden, atributoOrdenacion));
            }
            else
                query.orderBy(qReciboPendienteActividadVW.id.desc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query
                .list(qReciboPendienteActividadVW.id,
                        qReciboPendienteActividadVW.personaId,
                        qReciboPendienteActividadVW.ofertaId,
                        qReciboPendienteActividadVW.nombre,
                        qReciboPendienteActividadVW.fecha)
                .stream()
                .map(tuple -> {
                    ReciboPendienteActividad reciboPendiente = new ReciboPendienteActividad();
                    reciboPendiente.setId(tuple.get(qReciboPendienteActividadVW.id));
                    reciboPendiente.setPersonaId(tuple.get(qReciboPendienteActividadVW.personaId));
                    reciboPendiente.setOfertaId(tuple.get(qReciboPendienteActividadVW.ofertaId));
                    reciboPendiente.setNombre(tuple.get(qReciboPendienteActividadVW.nombre));
                    reciboPendiente.setFecha(tuple.get(qReciboPendienteActividadVW.fecha));
                    return reciboPendiente;
                }).collect(Collectors.toList());
    }

    public List<ReciboPendienteTarjeta> getRecibosPendientesTarjetas(Paginacion paginacion, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qReciboPendienteTarjetaVW)
                .where(qReciboPendienteTarjetaVW.personaId.eq(personaId));

        if (paginacion != null) {
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if(atributoOrdenacion != null) {
                query.orderBy(ordenaRecibosPendientesTar(orden, atributoOrdenacion));
            }
            else
                query.orderBy(qReciboPendienteTarjetaVW.id.desc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query
                .list(qReciboPendienteTarjetaVW.id,
                        qReciboPendienteTarjetaVW.personaId,
                        qReciboPendienteTarjetaVW.tipoId,
                        qReciboPendienteTarjetaVW.fechaInicio,
                        qReciboPendienteTarjetaVW.nombre,
                        qReciboPendienteTarjetaVW.origen)
                .stream()
                .map(tuple -> {
                    ReciboPendienteTarjeta reciboPendiente = new ReciboPendienteTarjeta();
                    reciboPendiente.setId(tuple.get(qReciboPendienteTarjetaVW.id));
                    reciboPendiente.setPersonaId(tuple.get(qReciboPendienteTarjetaVW.personaId));
                    reciboPendiente.setTipoId(tuple.get(qReciboPendienteTarjetaVW.tipoId));
                    reciboPendiente.setOrigen(tuple.get(qReciboPendienteTarjetaVW.origen));
                    reciboPendiente.setNombre(tuple.get(qReciboPendienteTarjetaVW.nombre));
                    reciboPendiente.setFechaInicio(tuple.get(qReciboPendienteTarjetaVW.fechaInicio));
                    return reciboPendiente;
                }).collect(Collectors.toList());
    }


    public List<ReciboPendienteCapitanActividad> getRecibosPendientesCapitanActividades(Paginacion paginacion, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qReciboPendienteCapitanActividadVW)
                .where(qReciboPendienteCapitanActividadVW.personaId.eq(personaId));

        if (paginacion != null) {
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if(atributoOrdenacion != null) {
                query.orderBy(ordenaRecibosPendientesCapAct(orden, atributoOrdenacion));
            }
            else
                query.orderBy(qReciboPendienteCapitanActividadVW.inscripcionId.desc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query
                .list(qReciboPendienteCapitanActividadVW.inscripcionId,
                        qReciboPendienteCapitanActividadVW.ofertaId,
                        qReciboPendienteCapitanActividadVW.actividadNombre,
                        qReciboPendienteCapitanActividadVW.equipoId,
                        qReciboPendienteCapitanActividadVW.equipoNombre)
                .stream()
                .map(tuple -> {
                    ReciboPendienteCapitanActividad reciboPendiente = new ReciboPendienteCapitanActividad();
                    reciboPendiente.setInscripcionId(tuple.get(qReciboPendienteCapitanActividadVW.inscripcionId));
                    reciboPendiente.setOfertaId(tuple.get(qReciboPendienteCapitanActividadVW.ofertaId));
                    reciboPendiente.setActividadNombre(tuple.get(qReciboPendienteCapitanActividadVW.actividadNombre));
                    reciboPendiente.setEquipoId(tuple.get(qReciboPendienteCapitanActividadVW.equipoId));
                    reciboPendiente.setEquipoNombre(tuple.get(qReciboPendienteCapitanActividadVW.equipoNombre));
                    return reciboPendiente;
                })
                .collect(Collectors.toList());
    }


    private OrderSpecifier ordenaRecibos(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "id":
                return orden.equals("ASC") ? qReciboVWDTO.id.asc() : qReciboVWDTO.id.desc();

            case "tipo":
                return orden.equals("ASC") ? qReciboVWDTO.tipoNombre.asc() : qReciboVWDTO.tipoNombre.desc();

            case "fechaCreacion":
                return orden.equals("ASC") ? qReciboVWDTO.fechaCreacion.asc() : qReciboVWDTO.fechaCreacion.desc();

            case "fechaPago":
                return orden.equals("ASC") ? qReciboVWDTO.fechaPago.asc() : qReciboVWDTO.fechaPago.desc();

            case "importeNeto":
                return orden.equals("ASC") ? qReciboVWDTO.importeNeto.asc() : qReciboVWDTO.importeNeto.desc();

            case "tipoCobro":
                return orden.equals("ASC") ? qReciboVWDTO.tipoCobroNombre.asc() : qReciboVWDTO.tipoCobroNombre.desc();

            default:
                return qReciboVWDTO.id.desc();
        }
    }

    private OrderSpecifier ordenaRecibosPendientesAct(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "id":
                return orden.equals("ASC") ? qReciboPendienteActividadVW.id.asc() : qReciboPendienteActividadVW.id.desc();

            case "ofertaId":
                return orden.equals("ASC") ? qReciboPendienteActividadVW.ofertaId.asc() : qReciboPendienteActividadVW.ofertaId.desc();

            case "fecha":
                return orden.equals("ASC") ? qReciboPendienteActividadVW.fecha.asc() : qReciboPendienteActividadVW.fecha.desc();

            case "nombre":
                return orden.equals("ASC") ? qReciboPendienteActividadVW.nombre.asc() : qReciboPendienteActividadVW.nombre.desc();

            case "personaId":
                return orden.equals("ASC") ? qReciboPendienteActividadVW.personaId.asc() : qReciboPendienteActividadVW.personaId.desc();

            default:
                return qReciboPendienteActividadVW.id.desc();
        }
    }

    private OrderSpecifier ordenaRecibosPendientesTar(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "id":
                return orden.equals("ASC") ? qReciboPendienteTarjetaVW.id.asc() : qReciboPendienteTarjetaVW.id.desc();

            case "personaId":
                return orden.equals("ASC") ? qReciboPendienteTarjetaVW.personaId.asc() : qReciboPendienteTarjetaVW.personaId.desc();

            case "tipoId":
                return orden.equals("ASC") ? qReciboPendienteTarjetaVW.tipoId.asc() : qReciboPendienteTarjetaVW.tipoId.desc();

            case "fechaInicio":
                return orden.equals("ASC") ? qReciboPendienteTarjetaVW.fechaInicio.asc() : qReciboPendienteTarjetaVW.fechaInicio.desc();

            case "nombre":
                return orden.equals("ASC") ? qReciboPendienteTarjetaVW.nombre.asc() : qReciboPendienteTarjetaVW.nombre.desc();

            case "origen":
                return orden.equals("ASC") ? qReciboPendienteTarjetaVW.origen.asc() : qReciboPendienteTarjetaVW.origen.desc();

            default:
                return qReciboPendienteTarjetaVW.id.desc();
        }
    }

    private OrderSpecifier ordenaRecibosPendientesCapAct(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "inscripcionId":
                return orden.equals("ASC") ? qReciboPendienteCapitanActividadVW.inscripcionId.asc() : qReciboPendienteCapitanActividadVW.inscripcionId.desc();

            case "ofertaId":
                return orden.equals("ASC") ? qReciboPendienteCapitanActividadVW.ofertaId.asc() : qReciboPendienteCapitanActividadVW.ofertaId.desc();

            case "personaId":
                return orden.equals("ASC") ? qReciboPendienteCapitanActividadVW.personaId.asc() : qReciboPendienteCapitanActividadVW.personaId.desc();

            case "actividadNombre":
                return orden.equals("ASC") ? qReciboPendienteCapitanActividadVW.actividadNombre.asc() : qReciboPendienteCapitanActividadVW.actividadNombre.desc();

            case "equipoId":
                return orden.equals("ASC") ? qReciboPendienteCapitanActividadVW.equipoId.asc() : qReciboPendienteCapitanActividadVW.equipoId.desc();

            case "equipoNombre":
                return orden.equals("ASC") ? qReciboPendienteCapitanActividadVW.equipoNombre.asc() : qReciboPendienteCapitanActividadVW.equipoNombre.desc();

            default:
                return qReciboPendienteCapitanActividadVW.inscripcionId.desc();
        }
    }
}
