package es.uji.apps.se.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.UIEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PersonasUJIVinculoDAO extends BaseDAODatabaseImpl {
    public List<UIEntity> getPersonasUJI() {
        QPersonaUJIVinculoDTO qPersonaUJIVinculoDTO = QPersonaUJIVinculoDTO.personaUJIVinculoDTO;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPersonaUJIVinculoDTO)
                .groupBy(qPersonaUJIVinculoDTO.vinculo).list(qPersonaUJIVinculoDTO.vinculo, qPersonaUJIVinculoDTO.id.count()).stream().map(p -> {
                    UIEntity e = new UIEntity();
                    e.put("vinculo", p.get(qPersonaUJIVinculoDTO.vinculo));
                    e.put("personas", p.get(qPersonaUJIVinculoDTO.id.count()));
                    return e;
                }).collect(Collectors.toList());
    }

    public PersonasSubVinculosDTO getPersonaVinculoUJIByVinculoYSubvinculo(Long personaId, Long vinculo, Long subvinculo) {

        QPersonasSubVinculosDTO qPersonasSubVinculos = QPersonasSubVinculosDTO.personasSubVinculosDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPersonasSubVinculos)
                .where(qPersonasSubVinculos.id.personaDTO.id.eq(personaId)
                        .and(qPersonasSubVinculos.id.sviId.eq(subvinculo)
                                .and(qPersonasSubVinculos.id.sviVINid.eq(vinculo))))
                .singleResult(qPersonasSubVinculos);

    }

    public TipoDTO getVinculoActualPersona(Long personaId, String valorParametroGlobal) {
        QTipoDTO qTipo = QTipoDTO.tipoDTO;
        QTipoDTO qTipo2 = QTipoDTO.tipoDTO;
        QVinculoExtDTO qVinculoExt = QVinculoExtDTO.vinculoExtDTO;
        QSubvinculoPersonaVWDTO qSubvinculoPersonaVW = QSubvinculoPersonaVWDTO.subvinculoPersonaVWDTO;

        List<TipoDTO> vinculos = new JPAQuery(entityManager)
                .from(qTipo, qVinculoExt, qSubvinculoPersonaVW)
                .where(qVinculoExt.vinculoExtId.eq(qSubvinculoPersonaVW.subvinculoId)
                        .and(qVinculoExt.vinculoId.eq(qTipo.id))
                        .and(qTipo.tipoPadreDTO.id.in(new JPASubQuery().from(qTipo2).where(qTipo2.grupoDTO.id.eq(7330L).and(qTipo2.uso.eq(valorParametroGlobal))).list(qTipo2.id)))
                        .and(qSubvinculoPersonaVW.personaId.eq(personaId)))
                .orderBy(qTipo.orden.asc())
                .list(qTipo);

        if (!vinculos.isEmpty()) {
            if (vinculos.stream().findFirst().get().getId() != -1L) {
                return vinculos.stream().findFirst().get();
            }
        }
        return new TipoDTO(292629L, "Altres (sense cap vincle amb l'UJI)");
    }
}
