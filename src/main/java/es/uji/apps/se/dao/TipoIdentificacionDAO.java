package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QTipoIdentificacionDTO;
import es.uji.apps.se.model.TipoIdentificacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class TipoIdentificacionDAO extends BaseDAODatabaseImpl {

    private QTipoIdentificacionDTO qTipoIdentificacionDTO = QTipoIdentificacionDTO.tipoIdentificacionDTO;

    public List<TipoIdentificacion> getTipoIdentificacion() {

        return new JPAQuery(entityManager).from(qTipoIdentificacionDTO).list(qTipoIdentificacionDTO.id,
                        qTipoIdentificacionDTO.nombre)
                .stream()
                .map(tuple -> {
                    TipoIdentificacion item = new TipoIdentificacion();
                    item.setId(tuple.get(qTipoIdentificacionDTO.id));
                    item.setNombre(tuple.get(qTipoIdentificacionDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }
}
