package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.ReservaClasesDirigidas;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class TarjetaDeportivaClaseDAO extends BaseDAODatabaseImpl {


    QTarjetaDeportivaClaseDTO qTarjetaDeportivaClase = QTarjetaDeportivaClaseDTO.tarjetaDeportivaClaseDTO;
    QCalendarioDTO qCalendario = QCalendarioDTO.calendarioDTO;
    QInstalacionDTO qInstalacion = QInstalacionDTO.instalacionDTO;
    QCalendarioClaseDTO qCalendarioClase = QCalendarioClaseDTO.calendarioClaseDTO;
    QCalendarioClaseInstalacionDTO qCalendarioClaseInstalacion = QCalendarioClaseInstalacionDTO.calendarioClaseInstalacionDTO;
    QClaseDTO qClaseDTO = QClaseDTO.claseDTO;
    QTarjetaDeportivaDTO qTarjetaDeportiva = QTarjetaDeportivaDTO.tarjetaDeportivaDTO;
    QPersonaUjiDTO qPersonaUjiDTO = QPersonaUjiDTO.personaUjiDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public TarjetaDeportivaClaseDAO() {
        relations.put("nombre", qTarjetaDeportivaClase.tarjetaDeportivaDTO.tarjetaDeportivaTipo.nombre);
        relations.put("nombrePersona", qTarjetaDeportivaClase.tarjetaDeportivaDTO.personaUji.apellido1);
        relations.put("asiste", qTarjetaDeportivaClase.asiste);
    }

    public List<TarjetaDeportivaClaseDTO> getClasesByTarjetaDeportivaId(Long tarjetaDeportivaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioClaseInstalacionDTO qCalendarioClaseInstalacion = QCalendarioClaseInstalacionDTO.calendarioClaseInstalacionDTO;

        query.from(qTarjetaDeportivaClase)
                .leftJoin(qTarjetaDeportivaClase.calendarioClase, qCalendarioClase).fetch()
                .leftJoin(qCalendarioClase.calendarioClaseInstalacionesDTO, qCalendarioClaseInstalacion).fetch()
                .leftJoin(qCalendarioClaseInstalacion.instalacionDTO, qInstalacion).fetch()
                .leftJoin(qCalendarioClase.calendarioDTO, qCalendario).fetch()
                .where(qTarjetaDeportivaClase.tarjetaDeportivaDTO.id.eq(tarjetaDeportivaId));
        return query.distinct().list(qTarjetaDeportivaClase);
    }

    public List<ReservaClasesDirigidas> getReservaClasesDirigidasFuturasByPersonaId(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaDeportivaClase)
                .join(qTarjetaDeportivaClase.tarjetaDeportivaDTO, qTarjetaDeportiva).fetch()
                .join(qTarjetaDeportivaClase.calendarioClase, qCalendarioClase).fetch()
                .join(qCalendarioClase.calendarioClaseInstalacionesDTO, qCalendarioClaseInstalacion).fetch()
                .join(qCalendarioClase.claseDTO, qClaseDTO).fetch()
                .join(qCalendarioClase.calendarioDTO, qCalendario).fetch()
                .join(qCalendarioClaseInstalacion.instalacionDTO, qInstalacion).fetch()
                .join(qTarjetaDeportiva.personaUji, qPersonaUjiDTO).fetch()
                .where(qPersonaUjiDTO.id.eq(personaId)
                        .and(qCalendario.dia.goe(DateExtensions.getCurrentDate())))
                .orderBy(qCalendario.dia.asc());

        List<TarjetaDeportivaClaseDTO> list = query.distinct().list(qTarjetaDeportivaClase);
        return list.stream().map(ReservaClasesDirigidas::new).collect(Collectors.toList());
    }

    public List<ReservaClasesDirigidas> getReservaClasesDirigidasPasadasByPersonaId(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaDeportivaClase)
                .join(qTarjetaDeportivaClase.tarjetaDeportivaDTO, qTarjetaDeportiva).fetch()
                .join(qTarjetaDeportivaClase.calendarioClase, qCalendarioClase).fetch()
                .join(qCalendarioClase.calendarioClaseInstalacionesDTO, qCalendarioClaseInstalacion).fetch()
                .join(qCalendarioClase.claseDTO, qClaseDTO).fetch()
                .join(qCalendarioClase.calendarioDTO, qCalendario).fetch()
                .join(qCalendarioClaseInstalacion.instalacionDTO, qInstalacion).fetch()
                .join(qTarjetaDeportiva.personaUji, qPersonaUjiDTO).fetch()
                .where(qPersonaUjiDTO.id.eq(personaId)
                        .and(qCalendario.dia.loe(DateExtensions.getCurrentDate())))
                .orderBy(qCalendario.dia.asc());

        List<TarjetaDeportivaClaseDTO> list = query.distinct().list(qTarjetaDeportivaClase);
        return list.stream().map(ReservaClasesDirigidas::new).collect(Collectors.toList());
    }

    public TarjetaDeportivaClaseDTO getClaseById(Long reservaId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaDeportivaClase)
                .leftJoin(qTarjetaDeportivaClase.calendarioClase, qCalendarioClase).fetch()
                .leftJoin(qCalendarioClase.calendarioDTO, qCalendario).fetch()
                .where(qTarjetaDeportivaClase.id.eq(reservaId));
        List<TarjetaDeportivaClaseDTO> clases = query.distinct().list(qTarjetaDeportivaClase);

        if (clases.isEmpty()) return null;
        return clases.get(0);
    }

    public List<TarjetaDeportivaClaseDTO> getTarjetaDeportivaClasesByCalendarioClaseId(Long calendarioClaseId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaDeportivaClase).where(qTarjetaDeportivaClase.calendarioClase.id.eq(calendarioClaseId));
        return query.list(qTarjetaDeportivaClase);
    }

    public List<TarjetaDeportivaClaseDTO> getClasesByCalendarioId(Long calendarioId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaDeportivaClase).where(qTarjetaDeportivaClase.calendarioClase.id.eq(calendarioId));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                query.orderBy(qTarjetaDeportivaClase.tarjetaDeportivaDTO.personaUji.apellido1.asc());
            }
        }

        return query.list(qTarjetaDeportivaClase);
    }

    public List<TarjetaDeportivaClaseDTO> getClasesByCalendarioId(Long calendarioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QTarjetaDeportivaDTO qTarjetaDeportiva = QTarjetaDeportivaDTO.tarjetaDeportivaDTO;
        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;

        query.from(qTarjetaDeportivaClase)
                .join(qTarjetaDeportivaClase.tarjetaDeportivaDTO, qTarjetaDeportiva).fetch()
                .join(qTarjetaDeportiva.personaUji, qPersonaUji).fetch()
                .where(qTarjetaDeportivaClase.calendarioClase.id.eq(calendarioId));

        return query.list(qTarjetaDeportivaClase);
    }
}
