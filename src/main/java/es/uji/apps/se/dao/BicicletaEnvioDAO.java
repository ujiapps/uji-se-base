package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.BicicletaEnvioDTO;
import es.uji.apps.se.dto.QBicicletaEnvioDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BicicletaEnvioDAO extends BaseDAODatabaseImpl {
    public BicicletaEnvioDTO getEnvioById(Long envioId) {
        QBicicletaEnvioDTO qBicicletaEnvioDTO = QBicicletaEnvioDTO.bicicletaEnvioDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qBicicletaEnvioDTO).where(qBicicletaEnvioDTO.id.eq(envioId));
        return query.uniqueResult(qBicicletaEnvioDTO);
    }

    public List<BicicletaEnvioDTO> getEnvios() {

        QBicicletaEnvioDTO qBicicletaEnvioDTO = QBicicletaEnvioDTO.bicicletaEnvioDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qBicicletaEnvioDTO);
        return query.list(qBicicletaEnvioDTO);
    }
}
