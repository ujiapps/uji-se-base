package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.MonitorMusculacionFichajeDTO;
import es.uji.apps.se.dto.QMonitorMusculacionFichajeDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MonitorMusculacionFichajeDAO extends BaseDAODatabaseImpl
{
    public List<MonitorMusculacionFichajeDTO> getFichajesByMonitorId(Long monitorId) {

        JPAQuery query = new JPAQuery(entityManager);

        QMonitorMusculacionFichajeDTO qMonitorMusculacionFichaje = QMonitorMusculacionFichajeDTO.monitorMusculacionFichajeDTO;

        query.from(qMonitorMusculacionFichaje).where(qMonitorMusculacionFichaje.monitorMusculacionDTO.id.eq(monitorId));

        return query.list(qMonitorMusculacionFichaje);
    }
}
