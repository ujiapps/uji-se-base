package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.dto.QPersonaUjiDTO;
import es.uji.apps.se.dto.QPersonaVinculoDTO;
import es.uji.apps.se.dto.QSancionVistaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository("personaUjiDAO")
public class PersonaUjiDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;

    public PersonaUjiDTO getPersonaById(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersonaVinculoDTO qPersonaVinculo = QPersonaVinculoDTO.personaVinculoDTO;
        QSancionVistaDTO qSancion = QSancionVistaDTO.sancionVistaDTO;

        query.from(qPersonaUji)
                .leftJoin(qPersonaUji.personaVinculoDTO, qPersonaVinculo).fetch()
                .leftJoin(qPersonaUji.sanciones, qSancion).fetch()
                .where(qPersonaUji.id.eq(connectedUserId));

        List<PersonaUjiDTO> personas = query.distinct().list(qPersonaUji);
        if (personas.isEmpty()) return null;
        return personas.get(0);
    }

    @Override
    public List<LookupItem> search(String s) {

        JPAQuery query = new JPAQuery(entityManager)
                            .from(qPersonaUji);

        if(!s.equals("")) {
            try {
                query.where(qPersonaUji.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where((Utils.limpia(qPersonaUji.nombre).concat(" ")
                     .concat(Utils.limpia(qPersonaUji.apellido1)).concat(" ")
                     .concat(Utils.limpia(qPersonaUji.apellido2)).concat(" ").containsIgnoreCase(StringUtils.limpiaAcentos(s)))
                     .or(qPersonaUji.identificacion.containsIgnoreCase(s)));
            }
        }

        return query
                .list(qPersonaUji.id,
                      qPersonaUji.nombre,
                      qPersonaUji.apellido1,
                      qPersonaUji.apellido2)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(String.valueOf(tuple.get(qPersonaUji.id)));
                    String nombre = "";
                    if (ParamUtils.isNotNull(tuple.get(qPersonaUji.nombre))) {
                        nombre = nombre.concat(tuple.get(qPersonaUji.nombre));
                    }
                    if (ParamUtils.isNotNull(tuple.get(qPersonaUji.apellido1))){
                        nombre = nombre.concat(" ").concat(tuple.get(qPersonaUji.apellido1));
                    }
                    if (ParamUtils.isNotNull(tuple.get(qPersonaUji.apellido2))){
                        nombre = nombre.concat(" ").concat(tuple.get(qPersonaUji.apellido2));
                    }

                    item.setNombre(nombre);
                    return item;
                }).collect(Collectors.toList());
    }
}
