package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QAccesosPorTipologiaDTO;
import es.uji.apps.se.dto.QUsosMaterialDTO;
import es.uji.apps.se.model.AccesosFranjaHoraria;
import es.uji.apps.se.model.QUsosMaterial;
import es.uji.apps.se.model.UsosMaterial;
import es.uji.apps.se.model.domains.TipoZona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UsosMaterialDAO extends BaseDAODatabaseImpl {
    public List<UsosMaterial> getUsosMaterial(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {

        QUsosMaterialDTO qUsosMaterialDTO = QUsosMaterialDTO.usosMaterialDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qUsosMaterialDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qUsosMaterialDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qUsosMaterialDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query.where(qUsosMaterialDTO.horaInicio.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query.where(qUsosMaterialDTO.horaInicio.loe(horaHasta));
        }

        query.groupBy(qUsosMaterialDTO.nombre);

        return query.list(new QUsosMaterial(qUsosMaterialDTO.nombre, qUsosMaterialDTO.cantidad.sum())).stream().sorted(Comparator.comparing(UsosMaterial::getNombre)).collect(Collectors.toList());
    }
}
