package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.QSancionDetalleDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.SancionDetalle;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PersonaSancionDetalleDAO extends BaseDAODatabaseImpl  {

    QSancionDetalleDTO qSancionDetalleDTO = QSancionDetalleDTO.sancionDetalleDTO;

    public List<SancionDetalle> getDetallesSancion(Long sancionId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qSancionDetalleDTO)
                .where(qSancionDetalleDTO.sancionId.eq(sancionId));

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());

            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if(atributoOrdenacion != null) {
                query.orderBy(ordenaDetallesSancion(orden, atributoOrdenacion));
            }
            else
                query.orderBy(qSancionDetalleDTO.id.asc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.
                list(qSancionDetalleDTO.id,
                        qSancionDetalleDTO.sancionId,
                        qSancionDetalleDTO.motivo)
                .stream()
                .map(tuple -> new SancionDetalle(tuple.get(qSancionDetalleDTO.id),
                        tuple.get(qSancionDetalleDTO.sancionId),
                        tuple.get(qSancionDetalleDTO.motivo)))
                .collect(Collectors.toList());
    }

    private OrderSpecifier ordenaDetallesSancion(String orden, String atributoOrdenacion) {

        switch (atributoOrdenacion) {
            case "id":
                return orden.equals("ASC") ? qSancionDetalleDTO.id.asc() : qSancionDetalleDTO.id.desc();

            case "sancionId":
                return orden.equals("ASC") ? qSancionDetalleDTO.sancionId.asc() : qSancionDetalleDTO.sancionId.desc();

            case "motivo":
                return orden.equals("ASC") ? qSancionDetalleDTO.motivo.asc() : qSancionDetalleDTO.motivo.desc();

            default:
                return qSancionDetalleDTO.id.asc();
        }
    }

    public void deleteDetalleSancion(Long detalleSancionId) {
        new JPADeleteClause(entityManager, QSancionDetalleDTO.sancionDetalleDTO)
                .where(QSancionDetalleDTO.sancionDetalleDTO.id.eq(detalleSancionId))
                .execute();
    }
}
