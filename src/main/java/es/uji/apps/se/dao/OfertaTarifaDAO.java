package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.OfertaTarifaDTO;
import es.uji.apps.se.dto.QOfertaTarifaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OfertaTarifaDAO extends BaseDAODatabaseImpl {

    private QOfertaTarifaDTO qOfertaTarifa = QOfertaTarifaDTO.ofertaTarifaDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public OfertaTarifaDAO() {
        relations.put("id", qOfertaTarifa.id);
        relations.put("diasRetrasoInscripcion", qOfertaTarifa.diasRetrasoInscripcion);
        relations.put("diasRetrasoValidacion", qOfertaTarifa.diasRetrasoValidacion);
        relations.put("fechaInicio", qOfertaTarifa.fechaInicio);
        relations.put("fechaFin", qOfertaTarifa.fechaFin);
        relations.put("importe", qOfertaTarifa.importe);
        relations.put("vinculoId", qOfertaTarifa.vinculo.nombre);
    }

    public List<OfertaTarifaDTO> getOfertasTarifasByOferta(Long ofertaId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qOfertaTarifa).where(qOfertaTarifa.ofertaDTO.id.eq(ofertaId));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qOfertaTarifa);
    }
}
