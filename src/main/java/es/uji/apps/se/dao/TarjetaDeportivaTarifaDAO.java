package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QTarjetaDeportivaTarifaDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTarifaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetaDeportivaTarifaDAO extends BaseDAODatabaseImpl {
    public List<TarjetaDeportivaTarifaDTO> getAllByTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId)
    {
        QTarjetaDeportivaTarifaDTO qTarjetaDeportivaTarifa = QTarjetaDeportivaTarifaDTO.tarjetaDeportivaTarifaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaDeportivaTarifa).where(qTarjetaDeportivaTarifa.tarjetaDeportivaTipoDTO.id.eq(tarjetaDeportivaTipoId));
        return query.distinct().list(qTarjetaDeportivaTarifa);
    }
}
