package es.uji.apps.se.dao;

import es.uji.commons.rest.ParamUtils;
import org.bouncycastle.util.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class Recibo {
    private GenerarRecibo recibo;
    private GetReciboByReferencia getReciboByReferencia;
    private GetUrlPagoDirecto getUrlPagoDirecto;
    private ReciboBloqueado reciboBloqueado;
    private BorraRecibo borraRecibo;
    private CreaReciboEquipo creaReciboEquipo;
    private CreaReciboNew creaReciboNew;

    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        Recibo.dataSource = dataSource;
    }

    @PostConstruct
    public void init() {
        this.recibo = new GenerarRecibo(dataSource);
        this.getReciboByReferencia = new GetReciboByReferencia(dataSource);
        this.getUrlPagoDirecto = new GetUrlPagoDirecto(dataSource);
        this.reciboBloqueado = new ReciboBloqueado(dataSource);
        this.borraRecibo = new BorraRecibo(dataSource);
        this.creaReciboEquipo = new CreaReciboEquipo(dataSource);
        this.creaReciboNew = new CreaReciboNew(dataSource);
    }

    public void generarRecibo(Long tipoCobro, String origen, Long id, Long plazos, Long importe, Long personaId, Long vinculoId, String concepto, String idioma, String cuentaBancaria, Long dto, String tipoDto,
                              String cuentaAbono, String etiqueta, Date fechaLimite, String correo, Long linea) {
        recibo.generarRecibo(tipoCobro, origen, id, plazos, importe, personaId, vinculoId, concepto, idioma, cuentaBancaria, dto, tipoDto, cuentaAbono, etiqueta, fechaLimite, correo, linea);
    }

    public String getReciboByReferenciaId(Long referenciaId) {
        return getReciboByReferencia.getReciboByReferencia(referenciaId);
    }

    public String getUrlPagoDirecto(Long reciboId) {
        return getUrlPagoDirecto.getUrlPagoDirecto(reciboId);
    }

    public void creaReciboEquipo(Long equipoId, Long personaId, Long ofertaId, String actividadNombre) {
        creaReciboEquipo.creaReciboEquipo(equipoId, personaId, ofertaId, actividadNombre);
    }

    public void creaReciboNew(Long personaId) {
        creaReciboNew.creaReciboNew(personaId);
    }

    public String reciboBloqueado(Long reciboId) {
        return reciboBloqueado.reciboBloqueado(reciboId);
    }

    public void borraRecibo(Long reciboId) {
        borraRecibo.borraRecibo(reciboId);
    }

    private class GenerarRecibo extends StoredProcedure {

        private static final String SQL = "uji_recibos.crea_recibo.se_recibo";
        private static final String PTIPOCOBRO = "p_tipo_cobro";
        private static final String PORIGEN = "p_origen";
        private static final String PID = "p_id";
        private static final String PPLAZOS = "p_plazos";
        private static final String PIMPORTE = "p_importe";
        private static final String PPERSONA = "p_persona";
        private static final String PVINCULO = "p_vinculo";
        private static final String PCONCEPTO = "p_concepto";
        private static final String PIDIOMA = "p_idioma";
        private static final String PCUENTABANCARIA = "p_cuenta_bancaria";
        private static final String PDTO = "p_dto";
        private static final String PTIPODTO = "p_tipo_dto";
        private static final String PCUENTAABONO = "p_cuenta_abono";
        private static final String PETIQUETA = "p_etiqueta";
        private static final String PFECHALIMITE = "p_fecha_limite";
        private static final String PCORREO = "p_correo";
        private static final String PLINEA = "p_linea";

        public GenerarRecibo(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PTIPOCOBRO, Types.BIGINT));
            declareParameter(new SqlParameter(PORIGEN, Types.VARCHAR));
            declareParameter(new SqlParameter(PID, Types.BIGINT));
            declareParameter(new SqlParameter(PPLAZOS, Types.BIGINT));
            declareParameter(new SqlParameter(PIMPORTE, Types.BIGINT));
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PVINCULO, Types.BIGINT));
            declareParameter(new SqlParameter(PCONCEPTO, Types.VARCHAR));
            declareParameter(new SqlParameter(PIDIOMA, Types.VARCHAR));
            declareParameter(new SqlParameter(PCUENTABANCARIA, Types.VARCHAR));
            declareParameter(new SqlParameter(PDTO, Types.BIGINT));
            declareParameter(new SqlParameter(PTIPODTO, Types.VARCHAR));
            declareParameter(new SqlParameter(PCUENTAABONO, Types.VARCHAR));
            declareParameter(new SqlParameter(PETIQUETA, Types.VARCHAR));
            declareParameter(new SqlParameter(PFECHALIMITE, Types.DATE));
            declareParameter(new SqlParameter(PCORREO, Types.VARCHAR));
            declareParameter(new SqlParameter(PLINEA, Types.BIGINT));

            compile();
        }

        public void generarRecibo(Long tipoCobro, String origen, Long id, Long plazos, Long importe, Long personaId, Long vinculoId, String concepto, String idioma, String cuentaBancaria, Long dto, String tipoDto,
                                  String cuentaAbono, String etiqueta, Date fechaLimite, String correo, Long linea) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PTIPOCOBRO, tipoCobro);
            inParams.put(PORIGEN, origen);
            inParams.put(PID, id);
            inParams.put(PPLAZOS, plazos);
            inParams.put(PIMPORTE, importe);
            inParams.put(PPERSONA, personaId);
            inParams.put(PVINCULO, vinculoId);
            inParams.put(PCONCEPTO, concepto);
            inParams.put(PIDIOMA, idioma);
            inParams.put(PCUENTABANCARIA, cuentaBancaria);
            inParams.put(PDTO, dto);
            inParams.put(PTIPODTO, tipoDto);
            inParams.put(PCUENTAABONO, cuentaAbono);
            inParams.put(PETIQUETA, etiqueta);
            inParams.put(PFECHALIMITE, fechaLimite);
            inParams.put(PCORREO, correo);
            inParams.put(PLINEA, linea);

            Map<String, Object> results = execute(inParams);
        }
    }

    private class GetReciboByReferencia extends StoredProcedure {

        private static final String SQL = "uji_sports.get_recibo_by_referencia";
        private static final String PREFERENCIA = "p_referencia";

        public GetReciboByReferencia(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(PREFERENCIA, Types.BIGINT));
            compile();
        }

        public String getReciboByReferencia(Long referenciaId) {
            Map<String, Object> inParams = new HashMap<String, Object>();

            inParams.put(PREFERENCIA, referenciaId);

            Map<String, Object> results = execute(inParams);
            if (ParamUtils.isNotNull(results.get("response"))){
                return results.get("response").toString();
            }
            return "";
        }
    }

    private class GetUrlPagoDirecto extends StoredProcedure {

        private static final String SQL = "uji_recibos.url_pago_directo";
        private static final String PRECIBOID = "p_id";
        private static final String POK = "pOK";
        private static final String PKO = "pKO";


        public GetUrlPagoDirecto(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(PRECIBOID, Types.BIGINT));
            declareParameter(new SqlParameter(POK, Types.VARCHAR));
            declareParameter(new SqlParameter(PKO, Types.VARCHAR));

            compile();
        }

        public String getUrlPagoDirecto(Long reciboId) {
            Map<String, Object> inParams = new HashMap<String, Object>();

            inParams.put(PRECIBOID, reciboId);
            inParams.put(POK, "https://ujiapps.uji.es/se/rest/app/pagotarjeta");
            inParams.put(PKO, "https://ujiapps.uji.es/se/rest/app/pagotarjeta");
            Map<String, Object> results = execute(inParams);
            return results.get("response").toString();
        }
    }

    private class ReciboBloqueado extends StoredProcedure {
        private static final String SQL = "uji_recibos.recibo_bloqueado";
        private static final String P_RECIBO = "p_recibo";

        public ReciboBloqueado(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.BIGINT));
            declareParameter(new SqlParameter(P_RECIBO, Types.BIGINT));

            compile();
        }

        public String reciboBloqueado(Long reciboId) {
            Map<String, Object> inParams = new HashMap<>();

            inParams.put(P_RECIBO, reciboId);
            Map<String, Object> results = execute(inParams);
            return results.get("response").toString();
        }
    }

    private class BorraRecibo extends StoredProcedure {
        private static final String SQL = "uji_recibos.borra_recibo";
        private static final String P_RECIBO = "p_recibo";

        public BorraRecibo(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(P_RECIBO, Types.BIGINT));
            compile();
        }

        public void borraRecibo(Long reciboId) {
            Map<String, Object> inParams = new HashMap<>();
            inParams.put(P_RECIBO, reciboId);
            execute(inParams);
        }
    }

    private class CreaReciboEquipo extends StoredProcedure {
        private static final String SQL = "util_ficha_actividad.crea_recibo_equipo";
        private static final String P_EQUIPO = "p_equipo";
        private static final String P_PERSONA = "p_persona";
        private static final String P_OFERTA = "p_oferta";
        private static final String P_CONCEPTO = "p_concepto";

        public CreaReciboEquipo(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(P_EQUIPO, Types.BIGINT));
            declareParameter(new SqlParameter(P_PERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(P_OFERTA, Types.BIGINT));
            declareParameter(new SqlParameter(P_CONCEPTO, Types.VARCHAR));
            compile();
        }

        public void creaReciboEquipo(Long equipoId, Long personaId, Long ofertaId, String actividadNombre) {
            Map<String, Object> inParams = new HashMap<>();
            inParams.put(P_EQUIPO, equipoId);
            inParams.put(P_PERSONA, personaId);
            inParams.put(P_OFERTA, ofertaId);
            inParams.put(P_CONCEPTO, actividadNombre);
            execute(inParams);
        }
    }

    private class CreaReciboNew extends StoredProcedure {
        private static final String SQL = "uji_sports.crear_recibo_new_version";
        private static final String P_PERSONA = "p_persona";
        private static final String P_DIV = "p_div";
        private static final String P_URL = "p_url";
        private static final String P_PARAM = "p_param";

        public CreaReciboNew(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(P_PERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(P_DIV, Types.VARCHAR));
            declareParameter(new SqlParameter(P_URL, Types.VARCHAR));
            declareParameter(new SqlParameter(P_PARAM, Types.VARCHAR));
            compile();
        }

        public void creaReciboNew(Long personaId) {
            Map<String, Object> inParams = new HashMap<>();
            inParams.put(P_PERSONA, personaId);
            inParams.put(P_DIV, null);
            inParams.put(P_URL, null);
            inParams.put(P_PARAM, null);
            execute(inParams);
        }
    }
}
