package es.uji.apps.se.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.StringExpression;
import es.uji.apps.se.dto.QCarnetBonoDTO;
import es.uji.apps.se.dto.QCarnetBonoTipoDTO;
import es.uji.apps.se.model.CarnetBonoTipo;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;


import java.time.Year;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository("personaRenovacionDAO")
public class PersonaRenovacionDAO extends BaseDAODatabaseImpl {
    private final PersonaDAO personaDAO;
    private final CarnetBonoTipoDAO carnetBonoTiposDAO;
    private final QCarnetBonoTipoDTO qCarnetBonoTipoDTO = QCarnetBonoTipoDTO.carnetBonoTipoDTO;
    private final QCarnetBonoDTO qCarnetBono = QCarnetBonoDTO.carnetBonoDTO;

    public PersonaRenovacionDAO(PersonaDAO personaDAO, CarnetBonoTipoDAO carnetBonoTiposDAO) {
        this.personaDAO = personaDAO;
        this.carnetBonoTiposDAO = carnetBonoTiposDAO;
    }

    public List<CarnetBonoTipo> getRenovacionesZona(Long personaId, Long anyoNacimiento, Boolean isTodasZonas, String uso) {
        Date hoy = DateExtensions.getCurrentDate();
        long anyoActual = Year.now().getValue();

        StringExpression estado = Expressions.stringTemplate("estado", carnetBonoTiposDAO.estadoTdyb2(qCarnetBono.id.longValue()).substring(0,2));
        StringExpression dameImpTdybPer = Expressions.stringTemplate("dame_imp_tdyb_per({0}, {1}, {2})", personaId, qCarnetBonoTipoDTO.id, "IMPORTE");

        QCarnetBonoDTO qCarnetBono2 = new QCarnetBonoDTO("cb");
        return new JPAQuery(entityManager)
                .from(qCarnetBonoTipoDTO, qCarnetBono)
                .where(qCarnetBonoTipoDTO.id.eq(qCarnetBono.tipo.id)
                        .and(qCarnetBonoTipoDTO.fechaInicio.coalesce(hoy).asDate().loe(new Date()))
                        .and(qCarnetBonoTipoDTO.fechaFin.coalesce(DateExtensions.addDays(hoy, 1)).asDate().goe(hoy))
                        .and(isTodasZonas ? qCarnetBonoTipoDTO.tipoInstalacionId.isNull() : qCarnetBonoTipoDTO.tipoInstalacionId.isNotNull())
                        .and(qCarnetBonoTipoDTO.dias.coalesce(0L).asNumber().gt(0L))
                        .and(qCarnetBono.personaId.eq(personaId)
                                .and(dameImpTdybPer.goe(String.valueOf(0L)))
                                .and(qCarnetBono.fechaFin.between(hoy, DateExtensions.addDays(hoy, 15))))
                        .and(qCarnetBonoTipoDTO.activo.eq(true))
                        .and(qCarnetBonoTipoDTO.mostrarUsuario.eq(Boolean.TRUE))
                        .and(qCarnetBonoTipoDTO.fechaFinValidez.coalesce(DateExtensions.addDays(hoy, 1)).asDate().gt(hoy))
                        .and(qCarnetBonoTipoDTO.edadMin.coalesce(anyoActual).asNumber().loe(anyoActual - (anyoNacimiento != null ? anyoNacimiento : 1900)))
                        .and(qCarnetBonoTipoDTO.edadMax.coalesce(anyoActual).asNumber().goe(anyoActual - (anyoNacimiento != null ? anyoNacimiento : 3000)))
                        .and(new JPASubQuery()
                                .from(qCarnetBono2)
                                .where(qCarnetBono2.tipo.id.eq(qCarnetBonoTipoDTO.id)
                                        .and(estado.notIn("BA", "CA", "SA", "AG"))
                                        .and(qCarnetBono2.fechaInicio.gt(hoy))
                                        .and(qCarnetBono2.personaId.eq(personaId)))
                                .notExists()))
                .orderBy(qCarnetBonoTipoDTO.nombre.asc())
                .list(qCarnetBonoTipoDTO.id,
                        qCarnetBonoTipoDTO.nombre,
                        qCarnetBonoTipoDTO.dias,
                        qCarnetBonoTipoDTO.tipoInstalacionId,
                        qCarnetBonoTipoDTO.fechaInicio,
                        qCarnetBonoTipoDTO.fechaFin)
                .stream()
                .map(tuple -> {
                    CarnetBonoTipo carnetBonoTipo = new CarnetBonoTipo();
                    carnetBonoTipo.setId(tuple.get(qCarnetBonoTipoDTO.id));
                    carnetBonoTipo.setNombre(tuple.get(qCarnetBonoTipoDTO.nombre));
                    carnetBonoTipo.setDias(tuple.get(qCarnetBonoTipoDTO.dias));
                    carnetBonoTipo.setTipoInstalacionId(tuple.get(qCarnetBonoTipoDTO.tipoInstalacionId));
                    carnetBonoTipo.setFechaInicio(tuple.get(qCarnetBonoTipoDTO.fechaInicio));
                    carnetBonoTipo.setFechaFin(tuple.get(qCarnetBonoTipoDTO.fechaFin));

                    if(carnetBonoTipo.getFechaFin() != null)
                        carnetBonoTipo.setFechaInicioNuevaTarjeta(DateExtensions.addDays(tuple.get(qCarnetBonoTipoDTO.fechaFin), 1));

                    carnetBonoTipo.setImporte(personaDAO.dameImpTdybPer(personaId, carnetBonoTipo.getId(), "IMPORTE", uso));
                    return carnetBonoTipo;
                })
                .collect(Collectors.toList());
    }

    public List<CarnetBonoTipo> getRenovacionesTD(Long personaId, Long anyoNacimiento, String uso) {
        long anyoActual = Year.now().getValue();
        Date hoy = DateExtensions.getCurrentDate();
        StringExpression estado = Expressions.stringTemplate("estado", carnetBonoTiposDAO.estadoTdyb2(qCarnetBono.id.longValue()).substring(0,2));
        StringExpression dameImpTdybPer = Expressions.stringTemplate("dame_imp_tdyb_per({0}, {1}, {2})", personaId, qCarnetBonoTipoDTO.id, "IMPORTE");
        QCarnetBonoTipoDTO qCarnetBonoTipo2 = new QCarnetBonoTipoDTO("cbt");

        return new JPAQuery(entityManager)
                .from(qCarnetBonoTipoDTO)
                .where(qCarnetBonoTipoDTO.dias.coalesce(0L).asNumber().gt(0L)
                        .and(dameImpTdybPer.goe(String.valueOf(0L)))
                        .and(qCarnetBonoTipoDTO.tipoInstalacionId.isNotNull())
                        .and(qCarnetBonoTipoDTO.edadMin.coalesce(anyoActual).asNumber().loe(anyoActual - (anyoNacimiento != null ? anyoNacimiento : 1900)))
                        .and(qCarnetBonoTipoDTO.edadMax.coalesce(anyoActual).asNumber().goe(anyoActual - (anyoNacimiento != null ? anyoNacimiento : 3000)))
                        .and(qCarnetBonoTipoDTO.fechaInicio.coalesce(hoy).asDate().loe(new Date()))
                        .and(qCarnetBonoTipoDTO.fechaFin.coalesce(DateExtensions.addDays(hoy, 1)).asDate().goe(hoy))
                        .and(qCarnetBonoTipoDTO.fechaFinValidez.coalesce(DateExtensions.addDays(hoy, 1)).asDate().gt(hoy))
                        .and(qCarnetBonoTipoDTO.activo.eq(true))
                        .and(qCarnetBonoTipoDTO.mostrarUsuario.eq(Boolean.TRUE))
                        .and(new JPASubQuery()
                                .from(qCarnetBono)
                                .where(qCarnetBono.tipo.id.eq(qCarnetBonoTipoDTO.id)
                                        .and(estado.notIn("BA", "CA", "SA", "AG"))
                                        .and(qCarnetBono.fechaInicio.gt(hoy))
                                        .and(qCarnetBono.personaId.eq(personaId)))
                                .notExists())
                        .and(new JPASubQuery()
                                .from(qCarnetBono, qCarnetBonoTipo2)
                                .where(qCarnetBono.tipo.id.eq(qCarnetBonoTipo2.id)
                                        .and(qCarnetBono.personaId.eq(personaId))
                                        .and(estado.notIn("BA", "CA", "SA", "AG"))
                                        .and(qCarnetBono.fechaFin.between(hoy, DateExtensions.addDays(hoy, 15)))
                                        .and(qCarnetBonoTipo2.dias.coalesce(0L).asNumber().gt(0L))
                                        .and(qCarnetBonoTipo2.tipoInstalacionId.isNull()))
                                .exists())
                ).orderBy(qCarnetBonoTipoDTO.nombre.asc())
                .list(qCarnetBonoTipoDTO.id,
                        qCarnetBonoTipoDTO.nombre,
                        qCarnetBonoTipoDTO.dias,
                        qCarnetBonoTipoDTO.tipoInstalacionId,
                        qCarnetBonoTipoDTO.fechaInicio,
                        qCarnetBonoTipoDTO.fechaFin
                ).stream().map(tuple -> {
                    CarnetBonoTipo carnetBonoTipo = new CarnetBonoTipo();
                    carnetBonoTipo.setId(tuple.get(qCarnetBonoTipoDTO.id));
                    carnetBonoTipo.setNombre(tuple.get(qCarnetBonoTipoDTO.nombre));
                    carnetBonoTipo.setDias(tuple.get(qCarnetBonoTipoDTO.dias));
                    carnetBonoTipo.setTipoInstalacionId(tuple.get(qCarnetBonoTipoDTO.tipoInstalacionId));
                    carnetBonoTipo.setFechaInicio(tuple.get(qCarnetBonoTipoDTO.fechaInicio));
                    carnetBonoTipo.setFechaFin(tuple.get(qCarnetBonoTipoDTO.fechaFin));
                    carnetBonoTipo.setImporte(personaDAO.dameImpTdybPer(personaId, carnetBonoTipo.getId(), "IMPORTE", uso));

                    Date fechaInicioNuevaTarjeta = new JPAQuery(entityManager)
                            .from(qCarnetBono, qCarnetBonoTipo2)
                            .where(qCarnetBono.tipo.id.eq(qCarnetBonoTipo2.id)
                                    .and(qCarnetBono.personaId.eq(personaId))
                                    .and(estado.notIn("BA", "CA", "SA", "AG"))
                                    .and(qCarnetBono.fechaFin.between(hoy, DateExtensions.addDays(hoy, 15)))
                                    .and(qCarnetBonoTipo2.dias.coalesce(0L).asNumber().gt(0L))
                                    .and(qCarnetBonoTipo2.tipoInstalacionId.isNotNull()))
                            .singleResult(qCarnetBono.fechaFin);
                    if (fechaInicioNuevaTarjeta != null)
                        carnetBonoTipo.setFechaInicioNuevaTarjeta(DateExtensions.addDays(fechaInicioNuevaTarjeta, 1));

                    return carnetBonoTipo;
                }).collect(Collectors.toList());
    }
}
