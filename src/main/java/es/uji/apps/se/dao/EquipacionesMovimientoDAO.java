package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.exceptions.equipaciones.movimientos.EquipacionesMovimientosGetException;
import es.uji.apps.se.model.EquipacionesMovimientoVW;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.ui.UIDia;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Repository("equipacionMovimientoDAO")
public class EquipacionesMovimientoDAO extends BaseDAODatabaseImpl {
    private QEquipacionesMovimientoVWDTO qEquipacionesMovimientoVWDTO = QEquipacionesMovimientoVWDTO.equipacionesMovimientoVWDTO;
    private QEquipacionDTO qEquipacionDTO = QEquipacionDTO.equipacionDTO;
    private QEquipacionTipoDTO qEquipacionTipoDTO = QEquipacionTipoDTO.equipacionTipoDTO;
    private QEquipacionModeloDTO qEquipacionModeloDTO = QEquipacionModeloDTO.equipacionModeloDTO;
    private QEquipacionGeneroDTO qEquipacionGeneroDTO = QEquipacionGeneroDTO.equipacionGeneroDTO;
    private QPersonaUjiDTO qPersonaUjiDTO = QPersonaUjiDTO.personaUjiDTO;
    private QEquipacionesMovimientoDTO qEquipacionesMovimientoDTO = QEquipacionesMovimientoDTO.equipacionesMovimientoDTO;

    private Map<String, Path> relations = new HashMap<>();

    public EquipacionesMovimientoDAO() {
        relations.put("equipacionTipoNombre", qEquipacionTipoDTO.nombre);
        relations.put("equipacionModeloNombre", qEquipacionModeloDTO.nombre);
        relations.put("equipacionGeneroNombre", qEquipacionGeneroDTO.nombre);
        relations.put("talla", qEquipacionesMovimientoVWDTO.talla);
        relations.put("dorsal", qEquipacionesMovimientoVWDTO.dorsal);
        relations.put("movimiento", qEquipacionesMovimientoVWDTO.movimiento);
        relations.put("identificacion", qEquipacionesMovimientoVWDTO.identificacion);
        relations.put("cantidad", qEquipacionesMovimientoVWDTO.cantidad);
        relations.put("fecha", qEquipacionesMovimientoVWDTO.fecha);
        relations.put("personaId", qEquipacionesMovimientoVWDTO.personaId);
        relations.put("personaNombre", qPersonaUjiDTO.nombre);
        relations.put("observaciones", qEquipacionesMovimientoVWDTO.observaciones);
    }

    public List<EquipacionesMovimientoVW> getEquipacionesMovimientos(Paginacion paginacion, List<Map<String, String>> filtros) throws EquipacionesMovimientosGetException{

        JPAQuery query = new JPAQuery(entityManager)
                .from(qEquipacionesMovimientoVWDTO)
                .join(qEquipacionDTO).on(qEquipacionDTO.id.eq(qEquipacionesMovimientoVWDTO.equipacionId))
                .join(qEquipacionTipoDTO).on(qEquipacionTipoDTO.id.eq(qEquipacionDTO.tipoDTO.id))
                .join(qEquipacionModeloDTO).on(qEquipacionModeloDTO.id.eq(qEquipacionDTO.modeloDTO.id))
                .join(qEquipacionGeneroDTO).on(qEquipacionGeneroDTO.id.eq(qEquipacionDTO.generoDTO.id))
                .leftJoin(qPersonaUjiDTO).on(qPersonaUjiDTO.id.eq(qEquipacionesMovimientoVWDTO.personaId));

        if (filtros != null) {
            for (Map<String, String> item : filtros) {
                try {
                    String property = item.get("property");
                    String value = item.get("value");

                    if (property.equals("equipacion")) {
                        query.where(qEquipacionesMovimientoVWDTO.equipacionId.eq(Long.parseLong(value)));
                    }

                    if (property.equals("tipo")) {
                        query.where(qEquipacionesMovimientoVWDTO.movimientoId.eq(Long.parseLong(value)));
                    }

                    if (property.equals("talla")) {
                        query.where(qEquipacionesMovimientoVWDTO.talla.eq(value));
                    }

                    if (property.equals("generoNombre")) {
                        query.where(qEquipacionGeneroDTO.id.eq(Long.parseLong(value)));
                    }

                    if (property.equals("modeloNombre")) {
                        query.where(qEquipacionModeloDTO.id.eq(Long.parseLong(value)));
                    }

                    if (property.equals("desde")) {
                        Date desde = new UIDia(value, "dd/MM/yyyy").getFecha();
                        query.where(qEquipacionesMovimientoVWDTO.fecha.goe(desde));
                    }

                    if (property.equals("hasta")) {
                        Date hasta = new UIDia(value, "dd/MM/yyyy").getFecha();
                        query.where(qEquipacionesMovimientoVWDTO.fecha.loe(hasta));
                    }
                } catch (ParseException e) {
                    throw new EquipacionesMovimientosGetException("Ha hagut un problema amb les dates indicades");
                } catch (Exception e) {
                    throw new EquipacionesMovimientosGetException();
                }
            }
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }
            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                }
            }
            else{
                query.orderBy(qEquipacionesMovimientoVWDTO.id.desc());
            }
        }

        return query.list(
                    qEquipacionesMovimientoVWDTO.id,
                    qEquipacionesMovimientoVWDTO.personaId,
                    qEquipacionesMovimientoVWDTO.talla,
                    qEquipacionesMovimientoVWDTO.movimientoId,
                    qEquipacionesMovimientoVWDTO.movimiento,
                    qEquipacionesMovimientoVWDTO.observaciones,
                    qEquipacionesMovimientoVWDTO.equipacionId,
                    qEquipacionesMovimientoVWDTO.identificacion,
                    qEquipacionesMovimientoVWDTO.fecha,
                    qEquipacionesMovimientoVWDTO.dorsal,
                    qEquipacionesMovimientoVWDTO.contabiliza,
                    qEquipacionesMovimientoVWDTO.cantidad,
                    qEquipacionTipoDTO.nombre,
                    qEquipacionModeloDTO.nombre,
                    qEquipacionGeneroDTO.nombre,
                    qPersonaUjiDTO.nombre,
                    qPersonaUjiDTO.apellido1,
                    qPersonaUjiDTO.apellido2)
                .stream()
                .map(tuple -> {
                    EquipacionesMovimientoVW equipacionesMovimientoVW = new EquipacionesMovimientoVW();
                    equipacionesMovimientoVW.setId(tuple.get(qEquipacionesMovimientoVWDTO.id));
                    equipacionesMovimientoVW.setPersonaId(tuple.get(qEquipacionesMovimientoVWDTO.personaId));

                    if(tuple.get(qEquipacionesMovimientoVWDTO.personaId) != null)
                        equipacionesMovimientoVW.setPersonaNombre(tuple.get(qPersonaUjiDTO.nombre).concat(" ").concat(tuple.get(qPersonaUjiDTO.apellido1).concat(" ").concat(tuple.get(qPersonaUjiDTO.apellido2))));

                    equipacionesMovimientoVW.setTalla(tuple.get(qEquipacionesMovimientoVWDTO.talla));
                    equipacionesMovimientoVW.setMovimientoId(tuple.get(qEquipacionesMovimientoVWDTO.movimientoId));
                    equipacionesMovimientoVW.setMovimiento(tuple.get(qEquipacionesMovimientoVWDTO.movimiento));
                    equipacionesMovimientoVW.setObservaciones(tuple.get(qEquipacionesMovimientoVWDTO.observaciones));
                    equipacionesMovimientoVW.setEquipacionId(tuple.get(qEquipacionesMovimientoVWDTO.equipacionId));
                    equipacionesMovimientoVW.setEquipacionTipoNombre(tuple.get(qEquipacionTipoDTO.nombre));
                    equipacionesMovimientoVW.setEquipacionModeloNombre(tuple.get(qEquipacionModeloDTO.nombre));
                    equipacionesMovimientoVW.setEquipacionGeneroNombre(tuple.get(qEquipacionGeneroDTO.nombre));
                    equipacionesMovimientoVW.setIdentificacion(tuple.get(qEquipacionesMovimientoVWDTO.identificacion));
                    equipacionesMovimientoVW.setFecha(tuple.get(qEquipacionesMovimientoVWDTO.fecha));
                    equipacionesMovimientoVW.setDorsal(tuple.get(qEquipacionesMovimientoVWDTO.dorsal));
                    equipacionesMovimientoVW.setContabiliza(tuple.get(qEquipacionesMovimientoVWDTO.contabiliza));
                    equipacionesMovimientoVW.setCantidad(tuple.get(qEquipacionesMovimientoVWDTO.cantidad));
                    return equipacionesMovimientoVW;
            }
        ).collect(Collectors.toList());
    }

    public EquipacionesMovimientoDTO getEquipacionMovimiento(Long id) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEquipacionesMovimientoDTO).where(qEquipacionesMovimientoDTO.id.eq(id));
        ;
        if (!query.list(qEquipacionesMovimientoDTO).isEmpty()) {
            return query.list(qEquipacionesMovimientoDTO).get(0);
        }
        return null;
    }
}