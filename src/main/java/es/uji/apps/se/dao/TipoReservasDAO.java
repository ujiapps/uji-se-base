package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QBicicletaTiposReservasDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.xml.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository("tiporeservaDAO")
public class TipoReservasDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {
    private QBicicletaTiposReservasDTO qBicicletaTiposReservasDTO = QBicicletaTiposReservasDTO.bicicletaTiposReservasDTO;

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qBicicletaTiposReservasDTO);
        if (!s.equals("")) {
            try {
                query.where(qBicicletaTiposReservasDTO.cursoAca.eq(Long.parseLong(s)));
            } catch (NumberFormatException e) {
                query.where(Utils.limpia(qBicicletaTiposReservasDTO.nombre).like("%"+ s +"%"));
            }
        }
        return query.list(qBicicletaTiposReservasDTO)
                .stream()
                .map(qBicicletaTiposReservasDTO -> {
                    LookupItem item = new LookupItem();
                    item.setId(qBicicletaTiposReservasDTO.getId().toString());
                    item.setNombre(qBicicletaTiposReservasDTO.getNombre());
                    return item;
                }).collect(Collectors.toList());
    }
}
