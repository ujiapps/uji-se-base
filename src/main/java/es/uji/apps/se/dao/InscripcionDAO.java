package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.expr.StringExpression;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.dto.views.QVWInscripcionUsuarioDTO;
import es.uji.apps.se.dto.views.VWActividadesAsistenciaDTO;
import es.uji.apps.se.model.*;
import es.uji.apps.se.model.domains.TipoEstadoInscripcion;
import es.uji.apps.se.services.DateExtensions;
import es.uji.apps.se.utils.Utils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class InscripcionDAO extends BaseDAODatabaseImpl {
    private final QInscripcionDTO qInscripcion = QInscripcionDTO.inscripcionDTO;
    private final QOfertaDTO qOfertaDTO = QOfertaDTO.ofertaDTO;
    private final QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;
    private final QOfertaDTO qOfertaDTO2 = new QOfertaDTO("oferta2");
    private final QActividadDTO qActividadDTO = QActividadDTO.actividadDTO;
    private final QPeriodoDTO qPeriodoDTO = QPeriodoDTO.periodoDTO;
    private final QVWInscripcionUsuarioDTO qvwInscripcionUsuarioDTO = QVWInscripcionUsuarioDTO.vWInscripcionUsuarioDTO;
    private final QCalendarioOfertaDTO qCalendarioOfertaDTO = QCalendarioOfertaDTO.calendarioOfertaDTO;
    private final QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;
    private final QTipoDTO qTipoDTO = QTipoDTO.tipoDTO;
    private final QTipoDTO qTipo2DTO = new QTipoDTO("qTipo2DTO");
    private final QDiasCaduActividadDTO qDiasCaduActividadDTO = QDiasCaduActividadDTO.diasCaduActividadDTO;
    private final QActividadAsistenciaDTO qActividadAsistenciaDTO = QActividadAsistenciaDTO.actividadAsistenciaDTO;
    private final QCompeticionMiembroDTO qCompeticionMiembroDTO = QCompeticionMiembroDTO.competicionMiembroDTO;
    private final QLineaReciboVWDTO qLineaReciboVWDTO = QLineaReciboVWDTO.lineaReciboVWDTO;
    private final QInscripcionEsperaDTO qInscripcionEsperaDTO = QInscripcionEsperaDTO.inscripcionEsperaDTO;
    private final QRecRecibosDTO qRecRecibosDTO = QRecRecibosDTO.recRecibosDTO;
    private final QMovimientoReciboDTO qMovimientoReciboDTO = QMovimientoReciboDTO.movimientoReciboDTO;
    private final QTipoMovimientoReciboDTO qTipoMovimientoReciboDTO = QTipoMovimientoReciboDTO.tipoMovimientoReciboDTO;
    private final QActividadesOfertasParametrosDTO qActividadesOfertasParametrosDTO = QActividadesOfertasParametrosDTO.actividadesOfertasParametrosDTO;
    private final QInscripcionParametrosDTO qInscripcionParametrosDTO = QInscripcionParametrosDTO.inscripcionParametrosDTO;
    private final QRecLineasDTO qRecLineasDTO = QRecLineasDTO.recLineasDTO;
    private final QBonificacioAutoDTO qBonificacioAutoDTO = QBonificacioAutoDTO.bonificacioAutoDTO;
    private final QActividadTipoDTO qActividadTipo = QActividadTipoDTO.actividadTipoDTO;
    private final QOfertaDTO qOfertaNueva = new QOfertaDTO("ofertaNueva");
    private final QPreinscripcionDTO qPreinscripcion = QPreinscripcionDTO.preinscripcionDTO;
    private final QBonificacionActivaDTO qBonificacionActiva = QBonificacionActivaDTO.bonificacionActivaDTO;
    private final QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
    private final QAptoActividadDTO qAptoActividad = QAptoActividadDTO.aptoActividadDTO;
    private final QHistoricoInscripcionVWDTO qHistoricoInscripcionVW = QHistoricoInscripcionVWDTO.historicoInscripcionVWDTO;

    public InscripcionDAO() {
    }


    public List<InscripcionDTO> getInscripcionesUsuario(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qInscripcion).where(qInscripcion.personaUjiDTO.id.eq(personaId));
        return query.list(qInscripcion);
    }

    public List<InscripcionDTO> getInscripcionesUsuarioPorPeriodo(Long persona, Long periodo) {

        QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qInscripcion).join(qInscripcion.ofertaDTO, qOferta)
                .where(qInscripcion.personaUjiDTO.id.eq(persona)
                        .and(qOferta.periodoDTO.id.eq(periodo)
                                .and(qInscripcion.estado.id.eq(TipoEstadoInscripcion.DEFINITIVO.getId()))));
        return query.list(qInscripcion);
    }

    public Boolean getInscripcionUsuario(Long connectedUserId, Long ofertaId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qInscripcion).where(qInscripcion.ofertaDTO.id.eq(ofertaId).and(qInscripcion.personaUjiDTO.id.eq(connectedUserId)));
        if (!query.list(qInscripcion).isEmpty()) return Boolean.TRUE;
        return Boolean.FALSE;
    }

    public List<InscripcionCombo> getInscripcionesUsuarioCurso(Long persona, Long cursoAca) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActividadDTO)
                .join(qOfertaDTO).on(qActividadDTO.id.eq(qOfertaDTO.actividadDTO.id))
                .join(qInscripcion).on(qInscripcion.ofertaDTO.id.eq(qOfertaDTO.id))
                .join(qPeriodoDTO).on(qOfertaDTO.periodoDTO.id.eq(qPeriodoDTO.id))
                .where(qPeriodoDTO.cursoAcademicoDTO.id.eq(cursoAca)
                        .and(qInscripcion.personaUjiDTO.id.eq(persona))
                        .and(qInscripcion.estado.id.eq(TipoEstadoInscripcion.DEFINITIVO.getId()))
                        .and(qOfertaDTO.daEquipacion.isTrue()));
        return query.list(qActividadDTO.nombre,
                qOfertaDTO.nombre,
                qInscripcion.id).stream().map(tuple -> {
                    String inscripcionNombre = tuple.get(qActividadDTO.nombre) + "- Grup:" + tuple.get(qOfertaDTO.nombre);
                    return new InscripcionCombo(tuple.get(qInscripcion.id), inscripcionNombre);
        }).collect(Collectors.toList());
    }

    public List<InscripcionUsuario> getInscripcionesUsuarioByCurso(Long persona, Long cursoAca) {
        return new JPAQuery(entityManager)
                   .from(qvwInscripcionUsuarioDTO)
                   .where(qvwInscripcionUsuarioDTO.personaId.eq(persona)
                   .and(qvwInscripcionUsuarioDTO.cursoAcademico.eq(cursoAca)))
                   .list(new QInscripcionUsuario(qvwInscripcionUsuarioDTO.id,
                                                 qvwInscripcionUsuarioDTO.personaId,
                                                 qvwInscripcionUsuarioDTO.muestraComentario,
                                                 qvwInscripcionUsuarioDTO.cursoAcademico,
                                                 qvwInscripcionUsuarioDTO.nombre,
                                                 qvwInscripcionUsuarioDTO.grupo,
                                                 qvwInscripcionUsuarioDTO.fecha,
                                                 qvwInscripcionUsuarioDTO.fechaBaja,
                                                 qvwInscripcionUsuarioDTO.periodoId,
                                                 qvwInscripcionUsuarioDTO.periodoNombre,
                                                 qvwInscripcionUsuarioDTO.posEspera,
                                                 qvwInscripcionUsuarioDTO.estadoId,
                                                 qvwInscripcionUsuarioDTO.ofertaId,
                                                 qvwInscripcionUsuarioDTO.numeroVeces,
                                                 qvwInscripcionUsuarioDTO.observaciones,
                                                 qvwInscripcionUsuarioDTO.apto,
                                                 qvwInscripcionUsuarioDTO.fechaInicioActividad,
                                                 qvwInscripcionUsuarioDTO.fechaFinActividad,
                                                 qvwInscripcionUsuarioDTO.fechaRecogidaCertificado));
    }

    public Long getCuenta(Long inscripcionId, Long personaId) {
        return new JPAQuery(entityManager)
                .from(qLineaReciboVWDTO)
                .where(qLineaReciboVWDTO.personaId.eq(personaId)
                .and(qLineaReciboVWDTO.tarjetaBonoId.eq(inscripcionId)))
                .count();
    }

    public List<GrupoInscripcion> getGruposDisponibles(Long inscripcionId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qOfertaDTO)
                .join(qOfertaDTO2).on(qOfertaDTO.actividadDTO.id.eq(qOfertaDTO2.actividadDTO.id))
                .join(qInscripcion).on(qOfertaDTO2.id.eq(qInscripcion.ofertaDTO.id))
                .where(qOfertaDTO.periodoDTO.id.eq(qOfertaDTO2.periodoDTO.id)
                        .and(qOfertaDTO.nombre.ne(qOfertaDTO2.nombre))
                        .and(qInscripcion.id.eq(inscripcionId)));

        return query.list(new QGrupoInscripcion(qOfertaDTO.id,
                qOfertaDTO.nombre,
                qOfertaDTO.horario,
                qOfertaDTO.plazas));
    }

    @Transactional
    public void modificaGrupoInscripcion(Long inscripcionId, Long ofertaId) {
        new JPAUpdateClause(entityManager, qInscripcion)
                .set(qInscripcion.ofertaDTO.id, ofertaId)
                .where(qInscripcion.id.eq(inscripcionId))
                .execute();
    }

    public List<VWActividadesAsistenciaDTO> getActividadesAsistencia(Long inscripcionId, Long ofertaId, String fecha, String fechaBaja) {
        String sql = "select coa.id, coa.hora_ini, coa.hora_fin, c.dia, 'SI' asisto, aa.id asistencia_id " +
                     "from se_calendario_ofertas_act coa, se_inscripciones i, se_calendario c, se_actividades_asistencias aa " +
                     "where coa.oferta_id = i.oferta_id " +
                     "and coa.calendario_id = c.id " +
                     "and aa.calendario_oferta_id = coa.id " +
                     "and aa.inscripcion_id = i.id " +
                     "and coa.obligatoria = 1 " +
                     "and i.id = " + inscripcionId + " " +
                     "union " +
                     "( " +
                     "  select coa.id, coa.hora_ini, coa.hora_fin, c.dia, 'NO' asisto, null " +
                     "  from se_calendario_ofertas_act coa, se_calendario c " +
                     "  where coa.oferta_id = " + ofertaId + " " +
                     "  and coa.calendario_id = c.id " +
                     "  and c.dia between trunc(to_date('" + fecha + "', 'dd/mm/yyyy')) and nvl(trunc(to_date(" + (fechaBaja != null ? "'" + fechaBaja + "'" : null)  + ", 'dd/mm/yyyy')), trunc(sysdate))" +
                     "  and coa.obligatoria = 1 " +
                     "  minus " +
                     "  select coa.id, coa.hora_ini, coa.hora_fin, c.dia, 'NO' asisto, null " +
                     "  from se_calendario_ofertas_act coa, se_inscripciones i, se_calendario c, se_actividades_asistencias aa " +
                     "  where coa.oferta_id = i.oferta_id " +
                     "  and coa.calendario_id = c.id " +
                     "  and aa.calendario_oferta_id = coa.id " +
                     "  and aa.inscripcion_id = i.id " +
                     "  and coa.obligatoria = 1 " +
                     "  and i.id = " + inscripcionId + " " +
                     ") " +
                     "order by 3, dia";
        List<Object[]> resultado = entityManager.createNativeQuery(sql).getResultList();

        if(resultado != null) {
            List<VWActividadesAsistenciaDTO> asistencias = new ArrayList<>();

            for(Object[] a : resultado) {
                asistencias.add(new VWActividadesAsistenciaDTO(Utils.convierteBigDecimalEnLong((BigDecimal) a[0]),
                                                               String.valueOf(a[1]),
                                                               String.valueOf(a[2]),
                                                               (Date) a[3],
                                                               String.valueOf(a[4]),
                                                               Utils.convierteBigDecimalEnLong((BigDecimal) a[5])));
            }

            return asistencias;
        }

        return null;
    }

    @Transactional
    public void addActividadAsistencia(ActividadAsistencia actividadAsistencia) {
        ActividadAsistenciaDTO actividadAsistenciaDTO = new ActividadAsistenciaDTO();
        InscripcionDTO inscripcionDTO = new InscripcionDTO();
        inscripcionDTO.setId(actividadAsistencia.getInscripcionId());
        actividadAsistenciaDTO.setCalendarioOfertaId(actividadAsistencia.getCalendarioOfertaId());
        actividadAsistenciaDTO.setInscripcion(inscripcionDTO);
        actividadAsistenciaDTO.setTipoEntrada("FICHAUSUARIO");
        this.insert(actividadAsistenciaDTO);
    }

    @Transactional
    public void deleteActividadAsistencia(Long asistenciaId) {
        new JPADeleteClause(entityManager, qActividadAsistenciaDTO)
            .where(qActividadAsistenciaDTO.id.eq(asistenciaId))
            .execute();
    }

    public Date getFechaBaja(Long inscripcionId) {
        String sql = "select max(xfecha) " +
                     "from uji_zetadolar.z$_se_inscripciones " +
                     "where id = " + inscripcionId;
        Object resultado = entityManager.createNativeQuery(sql).getSingleResult();
        return (Date) resultado;
    }

    public Long getCadu(Long inscripcionId, Long personaId) {
        return new JPAQuery(entityManager)
                    .from(qOfertaDTO, qInscripcion, qCalendarioOfertaDTO)
                    .where(qOfertaDTO.id.eq(qCalendarioOfertaDTO.ofertaDTO.id)
                    .and(qOfertaDTO.id.eq(qInscripcion.ofertaDTO.id)
                    .and(qInscripcion.personaUjiDTO.id.eq(personaId)
                    .and(qInscripcion.id.eq(inscripcionId)
                    .and(qInscripcion.estado.id.eq(TipoEstadoInscripcion.DEFINITIVO.getId())
                    .and(qCalendarioOfertaDTO.tipoReservaDTO.id.in(new JPAQuery(entityManager)
                                                                        .from(qTipoDTO, qTipo2DTO)
                                                                        .where(qTipoDTO.tipoPadreDTO.id.eq(qTipo2DTO.id)
                                                                        .and(qTipo2DTO.uso.eq("TIPORESERVASACT")))
                                                                        .list(qTipoDTO.id))))))))
                    .count();
    }

    public Long getDiasAsis(Long inscripcionId) {
        return new JPAQuery(entityManager)
                    .from(qActividadAsistenciaDTO)
                    .where(qActividadAsistenciaDTO.inscripcion.id.eq(inscripcionId))
                    .count();
    }

    public Long getCaduDiasTotal(Long ofertaId) {
        Long caduDias = new JPAQuery(entityManager)
                            .from(qDiasCaduActividadDTO)
                            .where(qDiasCaduActividadDTO.ofertaId.eq(ofertaId)).singleResult(qDiasCaduActividadDTO.dias);

            return caduDias != null ? caduDias : new JPAQuery(entityManager)
                                                     .from(qCalendarioOfertaDTO)
                                                     .where(qCalendarioOfertaDTO.ofertaDTO.id.eq(ofertaId)).count();
    }

    public Long getDiasTotal(Long ofertaId) {
        return new JPAQuery(entityManager)
                    .from(qCalendarioOfertaDTO, qCalendarioDTO)
                    .where(qCalendarioOfertaDTO.ofertaDTO.id.eq(ofertaId)
                    .and(qCalendarioOfertaDTO.calendarioDTO.id.eq(qCalendarioDTO.id)
                    .and(qCalendarioOfertaDTO.obligatoria.eq(true))))
                    .count();
    }

    public Long getDiasActiv(Long ofertaId, String fecha, String fechaBaja) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        return new JPAQuery(entityManager)
                    .from(qCalendarioOfertaDTO, qCalendarioDTO)
                    .where(qCalendarioOfertaDTO.ofertaDTO.id.eq(ofertaId)
                    .and(qCalendarioOfertaDTO.calendarioDTO.id.eq(qCalendarioDTO.id)
                    .and(qCalendarioDTO.dia.goe(formato.parse(fecha))
                    .and(qCalendarioDTO.dia.loe((fechaBaja == null) ? new Date() : formato.parse(fechaBaja))))
                    .and(qCalendarioOfertaDTO.obligatoria.eq(true))))
                    .count();
    }

    public Long[] getInscripcionesYPlazas(Long ofertaId) {
        NumberExpression<Long> decode = new CaseBuilder()
                .when(qOfertaDTO.plazas.eq(0L)).then(qActividadDTO.plazas)
                .when(qOfertaDTO.plazas.isNull()).then(qActividadDTO.plazas)
                .otherwise(qOfertaDTO.plazas);

        Object[] inscripcionesYPlazas = new JPAQuery(entityManager)
                .from(qInscripcion, qActividadDTO, qOfertaDTO)
                .where(qInscripcion.estado.id.in(TipoEstadoInscripcion.PENDIENTE.getId(), TipoEstadoInscripcion.DEFINITIVO.getId())
                        .and(qOfertaDTO.id.eq(qInscripcion.ofertaDTO.id))
                        .and(qActividadDTO.id.eq(qOfertaDTO.actividadDTO.id))
                        .and(qInscripcion.ofertaDTO.id.eq(ofertaId)))
                .groupBy(qInscripcion.ofertaDTO.id, qOfertaDTO.plazas)
                .singleResult(qInscripcion.count(), decode.sum()).toArray();
        return new Long[] { (Long) inscripcionesYPlazas[0], (Long) inscripcionesYPlazas[1] };
    }

    public Long getAsistenciasByInscripcionId(Long inscripcionId) {
        return new JPAQuery(entityManager)
                .from(qActividadAsistenciaDTO)
                .where(qActividadAsistenciaDTO.inscripcion.id.eq(inscripcionId)).count();
    }

    public Long[] getReciboYLinea(Long personaId, Long inscripcionId) {
        Object[] reciboYLinea = new JPAQuery(entityManager)
                .from(qLineaReciboVWDTO)
                .where(qLineaReciboVWDTO.personaId.eq(personaId)
                        .and(qLineaReciboVWDTO.tarjetaBonoId.eq(inscripcionId))
                        .and(qLineaReciboVWDTO.origen.eq("ACT")))
                .singleResult(qLineaReciboVWDTO.reciboId.min(), qLineaReciboVWDTO.id.min()).toArray();
        return new Long[] {(Long) reciboYLinea[0], (Long) reciboYLinea[1]};
    }

    public RecLineasDTO getRecibo(Long lineaId) {
        return new JPAQuery(entityManager)
                .from(qRecLineasDTO)
                .where(qRecLineasDTO.id.eq(lineaId))
                .singleResult(qRecLineasDTO);
    }

    public Long getCapitanByInscripcionId(Long inscripcionId) {
        return new JPAQuery(entityManager)
                .from(qCompeticionMiembroDTO)
                .where(qCompeticionMiembroDTO.inscripcion.id.eq(inscripcionId))
                .singleResult(qCompeticionMiembroDTO.capitan.min());
    }

    public Boolean isReciboBloqueado(Long reciboId) {
        try {
            Long hayRecibo = new JPAQuery(entityManager)
                    .from(qRecRecibosDTO)
                    .where(qRecRecibosDTO.id.eq(reciboId)
                            .and(qRecRecibosDTO.fechaCobro.isNotNull()))
                    .count();
            return hayRecibo > 0 || new JPAQuery(entityManager)
                    .from(qMovimientoReciboDTO, qTipoMovimientoReciboDTO)
                    .where(qMovimientoReciboDTO.id.eq(qTipoMovimientoReciboDTO.id)
                            .and(qMovimientoReciboDTO.reciboId.eq(reciboId))
                            .and(qTipoMovimientoReciboDTO.tipoConciliaBanco.isNotNull()))
                    .count() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Long> getInscripcionesOfertasExtra(Long ofertaId) {
        return new JPAQuery(entityManager)
                .from(qActividadesOfertasParametrosDTO, qInscripcionParametrosDTO)
                .where(qActividadesOfertasParametrosDTO.oferta.id.eq(ofertaId)
                        .and(qInscripcionParametrosDTO.parametroId.eq(qActividadesOfertasParametrosDTO.id)))
                .list(qActividadesOfertasParametrosDTO.id);
    }

    public Long getPlazaEspera(Long ofertaId) {
        return new JPAQuery(entityManager)
                .from(qInscripcionEsperaDTO)
                .where(qInscripcionEsperaDTO.ofertaId.eq(ofertaId)
                        .and(qInscripcionEsperaDTO.estadoId.eq(TipoEstadoInscripcion.ESPERA.getId())))
                .singleResult(qInscripcionEsperaDTO.posEspera.max());
    }

    @Transactional
    public void deleteInscripcion(Long inscripcionId) {
        new JPADeleteClause(entityManager, qInscripcion).where(qInscripcion.id.eq(inscripcionId)).execute();
    }

    @Transactional
    public void deleteCompeticionesMiembrosFromInscripcion(Long inscripcionId) {
        new JPADeleteClause(entityManager, qCompeticionMiembroDTO).where(qCompeticionMiembroDTO.inscripcion.id.eq(inscripcionId)).execute();
    }

    @Transactional
    public void deleteLineasRecibo(Long lineaId) {
        if (lineaId != null) {
            RecLineasDTO recibo = getRecibo(lineaId);
            if (!isReciboBloqueado(recibo.getRecibo().getId()) || recibo.getImporte() < 0)
                new JPADeleteClause(entityManager, qRecLineasDTO).where(qRecLineasDTO.id.eq(lineaId)).execute();
        }
    }

    @Transactional
    public void deleteRecibo(Long reciboId) {
        new JPADeleteClause(entityManager, qRecRecibosDTO).where(qRecRecibosDTO.id.eq(reciboId)).execute();
    }

    @Transactional
    public void deleteInscripcionesExtras(List<Long> inscripcionesExtrasIds, Long inscripcionId) {
        new JPADeleteClause(entityManager, qInscripcionParametrosDTO)
                .where(qInscripcionParametrosDTO.parametroId.in(inscripcionesExtrasIds).
                        and(qInscripcionParametrosDTO.inscripcionId.eq(inscripcionId))).execute();
    }

    public String[] getNombreYGrupoByOfertaId(Long ofertaId) {
        Object[] nombreYGrupo = new JPAQuery(entityManager)
                .from(qActividadDTO, qOfertaDTO)
                .where(qActividadDTO.id.eq(qOfertaDTO.actividadDTO.id)
                        .and(qOfertaDTO.id.eq(ofertaId)))
                .singleResult(qActividadDTO.nombre, qOfertaDTO.nombre).toArray();
        return new String[] {(String) nombreYGrupo[0], (String) nombreYGrupo[1]};
    }

    public InscripcionEsperaDTO getSiguienteEnEspera(Long ofertaId) {
        return new JPAQuery(entityManager)
                .from(qInscripcionEsperaDTO)
                .where(qInscripcionEsperaDTO.ofertaId.eq(ofertaId)
                        .and(qInscripcionEsperaDTO.estadoId.eq(TipoEstadoInscripcion.ESPERA.getId()))
                        .and(qInscripcionEsperaDTO.posEspera.eq(
                                new JPASubQuery()
                                        .from(qInscripcionEsperaDTO)
                                        .where(qInscripcionEsperaDTO.ofertaId.eq(ofertaId)
                                                .and(qInscripcionEsperaDTO.estadoId.eq(TipoEstadoInscripcion.ESPERA.getId())))
                                        .unique(qInscripcionEsperaDTO.posEspera.min())
                                )
                        )
                ).singleResult(qInscripcionEsperaDTO);
    }

    public Boolean permiteDuplicados(Long ofertaId) {
        return new JPAQuery(entityManager)
                .from(qActividadDTO, qOfertaDTO)
                .where(qActividadDTO.id.eq(qOfertaDTO.actividadDTO.id)
                        .and(qOfertaDTO.id.eq(ofertaId)))
                .singleResult(qActividadDTO.permiteDuplicidadInscripcion);
    }

    @Transactional
    public void actualizaEstadoInscripcionAManual(InscripcionEsperaDTO inscripcionEsperaDTO) {
        new JPAUpdateClause(entityManager, qInscripcionEsperaDTO)
                .set(qInscripcionEsperaDTO.estadoId, TipoEstadoInscripcion.INSCRITO_MANUAL.getId())
                .where(qInscripcionEsperaDTO.id.eq(inscripcionEsperaDTO.getId())).execute();
    }

    public List<BonificacioAutoDTO> getBonificacionesAuto(Long ofertaId) {
        return new JPAQuery(entityManager)
                .from(qBonificacioAutoDTO)
                .where(qBonificacioAutoDTO.referencia1Id.eq(ofertaId))
                .list(qBonificacioAutoDTO);
    }

    public List<PlazaInscripcion> getPlazasInscripciones(Long personaId, Long periodoId, Long actividadId, Long tipoCompeticionExterna, Boolean tieneSancion) throws ParseException {
        StringExpression ordenaCharToNumber = Expressions.stringTemplate("ordena_char_to_number({0})", qOfertaDTO.nombre);
        Date hoy = DateExtensions.getCurrentDate();


        return new JPAQuery(entityManager)
                .from(qOfertaDTO)
                .where(qOfertaDTO.actividadDTO.id.eq(actividadId)
                        .and(qOfertaDTO.periodoDTO.id.eq(periodoId))
                        .and(qOfertaDTO.fechaInicioWeb.coalesce(DateExtensions.addDays(hoy, - 1)).asDate().loe(hoy)
                                .and(qOfertaDTO.fechaFinWeb.coalesce(DateExtensions.addDays(hoy, + 1)).asDate().goe(hoy))))
                .orderBy(ordenaCharToNumber.asc())
                .list(qOfertaDTO)
                .stream()
                .map(tuple -> {
                    Long grupoId = tuple.getId();
                    String importe = dameImpInscPer(grupoId, personaId, "TEXTO", null, null, new Date());
                    if(!"N".equals(importe)) {
                        try {
                            return getPlazaInscripcion(grupoId, personaId, importe, tipoCompeticionExterna, null, tieneSancion);
                        }
                        catch (ParseException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    public PlazaInscripcion getPlazaInscripcion(Long grupoId, Long personaId, String importe, Long tipoCompeticionExterna, Long admin, Boolean tieneSancion) throws ParseException {
        Tuple tupla = new JPAQuery(entityManager)
                .from(qOferta, qActividadDTO)
                .where(qOferta.id.eq(grupoId)
                        .and(qActividadDTO.id.eq(qOferta.actividadDTO.id)))
                .singleResult(qOferta.descripcion,
                        qOferta.nombre,
                        qOferta.plazas,
                        qActividadDTO.plazas,
                        qOferta.horario,
                        qOferta.ibanAbono,
                        qOferta.plazasMaximo,
                        qOferta.permiteInscripcionWeb,
                        qOferta.premitePreinscripcion,
                        qOferta.ibanAbono);

        String descripcion = tupla.get(qOferta.descripcion);
        String grupo = tupla.get(qOferta.nombre);
        Long plazas = tupla.get(tupla.get(qOferta.plazas) == null || tupla.get(qOferta.plazas) == 0 ? qActividadDTO.plazas : qOferta.plazas);

        Long maxPlazas = tupla.get(qOferta.plazasMaximo) == null ? tupla.get(qOferta.plazas): tupla.get(qOferta.plazasMaximo);
        if (maxPlazas == null || maxPlazas == 0) {
            maxPlazas = tupla.get(qActividadDTO.plazas);
        }

        String horario = tupla.get(qOferta.horario);
        Long inscripcionWeb = Boolean.TRUE.equals(tupla.get(qOferta.permiteInscripcionWeb)) ? 1L : 0L;
        Long admitePreinscripcion = Boolean.TRUE.equals(tupla.get(qOferta.premitePreinscripcion)) ? 1L : 0L;
        String cuentaAjena = tupla.get(qOferta.ibanAbono);

        Long plazasOcupadas = new JPAQuery(entityManager)
                .from(qInscripcion)
                .where(qInscripcion.ofertaDTO.id.eq(grupoId)
                        .and(qInscripcion.estado.id.in(TipoEstadoInscripcion.DEFINITIVO.getId(), TipoEstadoInscripcion.PENDIENTE.getId())))
                .count();

        Long plazasEspera = new JPAQuery(entityManager)
                .from(qInscripcionEsperaDTO)
                .where(qInscripcionEsperaDTO.ofertaId.eq(grupoId)
                        .and(qInscripcionEsperaDTO.estadoId.eq(TipoEstadoInscripcion.ESPERA.getId())))
                .count();

        Long competicionExterna = new JPAQuery(entityManager)
                .from(qActividadDTO, qOferta, qActividadTipo, qTipoDTO)
                .where(qActividadDTO.id.eq(qOferta.actividadDTO.id)
                        .and(qActividadDTO.id.eq(qActividadTipo.actividadDTO.id)
                                .and(qOferta.id.eq(grupoId))
                                .and(qTipoDTO.id.eq(qActividadTipo.tipoDTO.id)
                                        .and(qTipoDTO.tipoPadreDTO.id.eq(tipoCompeticionExterna)))))
                .singleResult(qOferta.id.min());

        String puedeIncripcion = puedeInscripcionOfertaAdmin(personaId, grupoId);
        Date fechaInicioValida = dameFechaValidacion(personaId, grupoId, "INI");
        Date fechaFinValida = dameFechaValidacion(personaId, grupoId, "FIN");

        Date fechaInicioPreinscripcion = null;
        Date fechaFinPreinscripcion = null;
        Date fechaInicioRenovacion = null;
        Date fechaFinRenovacion = null;

        Long nuevaOferta = null;

        if(puedeIncripcion.charAt(0) != 'S' && admitePreinscripcion == 1) {
            nuevaOferta = new JPAQuery(entityManager)
                    .from(qOferta, qOfertaNueva, qInscripcion, qPeriodoDTO)
                    .where(qOfertaNueva.id.eq(grupoId)
                            .and(qOferta.nombre.eq(qOfertaNueva.nombre))
                            .and(qOferta.periodoDTO.id.ne(qOfertaNueva.periodoDTO.id))
                            .and(qOferta.actividadDTO.id.eq(qOfertaNueva.actividadDTO.id)
                                    .and(qInscripcion.ofertaDTO.id.eq(qOferta.id)
                                            .and(qInscripcion.estado.id.eq(TipoEstadoInscripcion.DEFINITIVO.getId()))
                                            .and(qInscripcion.personaUjiDTO.id.eq(personaId))
                                            .and(qPeriodoDTO.id.eq(qOfertaNueva.periodoDTO.id))
                                            .and(qPeriodoDTO.periodoReferenciaDTO.id.eq(qOferta.periodoDTO.id)))))
                    .groupBy(qOfertaNueva.id, qOfertaNueva.fechaFinPreinscripcion, qPeriodoDTO.fechaFinPreinscripcion)
                    .singleResult(qOfertaNueva.id.min());

            Tuple tupla2 = new JPAQuery(entityManager)
                    .from(qOferta, qPeriodoDTO)
                    .where(qOferta.id.eq(grupoId)
                            .and(qOferta.periodoDTO.id.eq(qPeriodoDTO.id)))
                    .singleResult(qOferta.fechaInicioPreinscripcion,
                            qPeriodoDTO.fechaInicioPreinscripcion,
                            qOferta.fechaFinPreinscripcion,
                            qPeriodoDTO.fechaFinPreinscripcion,
                            qOferta.fechaInicioRenovacion,
                            qPeriodoDTO.fechaInicioRenovacion,
                            qOferta.fechaFinRenovacion,
                            qPeriodoDTO.fechaFinRenovacion);

            fechaInicioPreinscripcion = tupla2.get(qOferta.fechaInicioPreinscripcion) != null ? tupla2.get(qOferta.fechaInicioPreinscripcion) : tupla2.get(qPeriodoDTO.fechaInicioPreinscripcion);
            fechaFinPreinscripcion = tupla2.get(qOferta.fechaFinPreinscripcion) != null ? tupla2.get(qOferta.fechaFinPreinscripcion) : tupla2.get(qPeriodoDTO.fechaFinPreinscripcion);
            fechaInicioRenovacion = tupla2.get(qOferta.fechaInicioRenovacion) != null ? tupla2.get(qOferta.fechaInicioRenovacion) : tupla2.get(qPeriodoDTO.fechaInicioRenovacion);
            fechaFinRenovacion = tupla2.get(qOferta.fechaFinRenovacion) != null ? tupla2.get(qOferta.fechaFinRenovacion) : tupla2.get(qPeriodoDTO.fechaFinRenovacion);
        }

        Long preinscripcionId = new JPAQuery(entityManager)
                .from(qPreinscripcion)
                .where(qPreinscripcion.ofertaId.eq(grupoId)
                        .and(qPreinscripcion.personaId.eq(personaId)))
                .singleResult(qPreinscripcion.id.min());

        PlazaInscripcion plazaInscripcion = new PlazaInscripcion();
        plazaInscripcion.setDescripcion(descripcion);
        plazaInscripcion.setNuevaOferta(nuevaOferta);
        plazaInscripcion.setGrupoId(grupoId);
        plazaInscripcion.setGrupo(grupo);
        plazaInscripcion.setPlazas(plazas);
        plazaInscripcion.setPlazasEspera(plazasEspera);
        plazaInscripcion.setPlazasOcupadas(plazasOcupadas);
        plazaInscripcion.setHorario(horario);
        plazaInscripcion.setPuedeInscripcion(puedeIncripcion);
        plazaInscripcion.setAdmitePreinscripcion(admitePreinscripcion);
        plazaInscripcion.setFechaInicioPreinscripcion(fechaInicioPreinscripcion);
        plazaInscripcion.setFechaFinPreinscripcion(fechaFinPreinscripcion);
        plazaInscripcion.setFechaInicioRenovacion(fechaInicioRenovacion);
        plazaInscripcion.setFechaFinRenovacion(fechaFinRenovacion);
        plazaInscripcion.setFechaInicioValida(fechaInicioValida);
        plazaInscripcion.setFechaFinValida(fechaFinValida);
        plazaInscripcion.setInscripcionNueva(getNuevaInscripcion(personaId, grupoId));
        plazaInscripcion.setPreinscripcionId(preinscripcionId);
        plazaInscripcion.setIncrito(estaIncrito(personaId, grupoId));
        plazaInscripcion.setTieneSancion(tieneSancion);
        plazaInscripcion.setInscripcionWeb(inscripcionWeb);
        plazaInscripcion.setCompeticionExterna(competicionExterna);
        plazaInscripcion.setCuentaAjena(cuentaAjena);
        plazaInscripcion.setMaxPlazas(maxPlazas);

        Long tieneBono = new JPAQuery(entityManager)
                .from(qBonificacionActiva)
                .where(qBonificacionActiva.personaUjiDTO.id.eq(personaId)
                        .and(qBonificacionActiva.referencia.isNull()
                                .or(qBonificacionActiva.referencia.eq(grupoId))))
                .count();

        plazaInscripcion.setTieneBono(tieneBono);

        String inscrito = plazaInscripcion.getIncrito();

        if(inscrito.charAt(0) == 'I') {
            String veces = inscrito.substring(1);

            Long numVeces = new JPAQuery(entityManager)
                    .from(qInscripcion)
                    .where(qInscripcion.ofertaDTO.id.eq(grupoId)
                            .and(qInscripcion.personaUjiDTO.id.eq(personaId))
                            .and(qInscripcion.numeroInscripcion.eq(Long.valueOf(veces))))
                    .singleResult(qInscripcion.id.min());

            plazaInscripcion.setNumVeces(numVeces);
        }
        else if(!"N".equals(inscrito)) {
            Long posicionEspera = new JPAQuery(entityManager)
                    .from(qInscripcionEsperaDTO)
                    .where(qInscripcionEsperaDTO.ofertaId.eq(grupoId)
                            .and(qInscripcionEsperaDTO.posEspera.loe(Long.valueOf(inscrito)))
                            .and(qInscripcionEsperaDTO.estadoId.eq(351482L)))
                    .count();

            plazaInscripcion.setPosicionEspera(posicionEspera);
        }

        return plazaInscripcion;
    }

    public String estaIncrito(Long personaId, Long grupoId) {
        Long inscrito = new JPAQuery(entityManager)
                .from(qInscripcion)
                .where(qInscripcion.personaUjiDTO.id.eq(personaId)
                        .and(qInscripcion.ofertaDTO.id.eq(grupoId))
                        .and(qInscripcion.estado.id.in(291212L, 291230L)))
                .singleResult(qInscripcion.numeroInscripcion.max());

        Long espera = new JPAQuery(entityManager)
                .from(qInscripcionEsperaDTO)
                .where(qInscripcionEsperaDTO.personaId.eq(personaId)
                        .and(qInscripcionEsperaDTO.ofertaId.eq(grupoId))
                        .and(qInscripcionEsperaDTO.estadoId.eq(351482L)))
                .singleResult(qInscripcionEsperaDTO.posEspera.max());

        if(inscrito != null && inscrito > 0) {
            return "I".concat(String.valueOf(inscrito));
        }
        else if(espera != null) {
            return String.valueOf(espera);
        }
        else
            return "N";
    }

    public Long getNuevaInscripcion(Long personaId, Long ofertaId) {
        return new JPAQuery(entityManager)
                .from(qPreinscripcion)
                .where(qPreinscripcion.ofertaId.eq(ofertaId)
                        .and(qPreinscripcion.personaId.eq(personaId)))
                .singleResult(qPreinscripcion.id.min());
    }

    public String puedeInscripcionOfertaAdmin(Long personaId, Long ofertaId) throws ParseException {
        Long edadMin = new JPAQuery(entityManager)
                .from(qActividadDTO, qOferta)
                .where(qActividadDTO.id.eq(qOferta.actividadDTO.id)
                        .and(qOferta.id.eq(ofertaId)))
                .singleResult(qActividadDTO.edadMinimaInscripcion.min());

        Date fechaNacimiento = new JPAQuery(entityManager)
                .from(qPersonaUji)
                .where(qPersonaUji.id.eq(personaId))
                .singleResult(qPersonaUji.fechaNacimiento);

        Tuple tupla = new JPAQuery(entityManager)
                .from(qOferta, qPeriodoDTO)
                .where(qOferta.periodoDTO.id.eq(qPeriodoDTO.id)
                        .and(qOferta.id.eq(ofertaId)))
                .singleResult(qOferta.fechaInicioInscripcion,
                        qPeriodoDTO.fechaInicioInscripcion,
                        qOferta.fechaFinInscripcion,
                        qPeriodoDTO.fechaFinInscripcion,
                        qOferta.fechaInicioNacimiento,
                        qOferta.fechaFinNacimiento);

        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaInicioInscripcion = tupla.get(qOferta.fechaInicioInscripcion) != null ? tupla.get(qOferta.fechaInicioInscripcion) : tupla.get(qPeriodoDTO.fechaInicioInscripcion);
        Date fechaFinInscripcion = tupla.get(qOferta.fechaFinInscripcion) != null ? tupla.get(qOferta.fechaFinInscripcion) : tupla.get(qPeriodoDTO.fechaFinInscripcion);
        Date fechaNacimientoInicial = tupla.get(qOferta.fechaInicioNacimiento) != null ? tupla.get(qOferta.fechaInicioNacimiento): formato.parse("1900-01-01");
        Date fechaNacimientoFinal = tupla.get(qOferta.fechaFinNacimiento) != null ? tupla.get(qOferta.fechaFinNacimiento): formato.parse("2100-12-31");

        Long aptos = new JPAQuery(entityManager)
                .from(qAptoActividad, qOferta)
                .where(qAptoActividad.actividadId.eq(qOferta.actividadDTO.id)
                        .and(qOferta.id.eq(ofertaId)))
                .count();
        Long numAptos;

        if(aptos > 0) {
            String sql = "select count(*) " +
                    "from( select i.id " +
                    "      from se_actividades_oferta o, se_actividades_aptos aa, se_inscripciones i, se_actividades_oferta o_ins " +
                    "      where o.actividad_id = aa.actividad_id " +
                    "      and aa.actividad_id_acceso = o_ins.actividad_id " +
                    "      and o_ins.id = i.oferta_id " +
                    "      and o.id =  " + ofertaId + " " +
                    "      and i.persona_id = :personaId " +
                    "      and i.estado_id = 291230 " +
                    "      and decode(aa.necesita_apto, 0, 0, 1, nvl(i.apto, 0)) = 0 " +
                    "      union all " +
                    "      select i.id " +
                    "      from se_actividades_oferta o, se_actividades_aptos aa, se_vw_inscripciones_ant_aptos i " +
                    "      where o.actividad_id = aa.actividad_id " +
                    "      and aa.actividad_id_acceso = i.id " +
                    "      and o.id = " + ofertaId + " " +
                    "      and i.persona_id = :personaId " +
                    "      and decode(aa.necesita_apto, 0, 0, 1, decode(i.apto, 'N', 1, 0)) = 0 )";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("personaId", personaId);
            numAptos = Utils.convierteBigDecimalEnLong((BigDecimal) query.getSingleResult());
        }
        else
            numAptos = 0L;

        fechaInicioInscripcion = DateExtensions.addDays(fechaInicioInscripcion, Integer.valueOf(dameImpInscPer(ofertaId, personaId, "RETRASO", null, null, new Date())));

        if(fechaNacimiento != null && (formato.format(fechaNacimiento).compareTo(formato.format(fechaNacimientoInicial)) < 0 ||
                formato.format(fechaNacimiento).compareTo(formato.format(fechaNacimientoFinal)) > 0)) {
            return "N-No tens la edad apropiada per a inscriure't en aquest grup";
        }
        else if(numAptos == 0 && aptos > 0) {
            return "A-Cal estar inscrit en altres activitats abans d'aquesta";
        }
        else if(fechaNacimiento != null && ((new Date().getTime() - fechaNacimiento.getTime()) / 86400000 / 365 < edadMin)) {
            return "S-Inscriure's a l'activitat (L'usuari no te l'edat apropiada per inscriure en aquest grup)";
        }
        else if(formato.format(new Date()).compareTo(formato.format(fechaInicioInscripcion)) >= 0 &&
                formato.format(new Date()).compareTo(formato.format(fechaFinInscripcion)) <= 0) {
            return "S-Inscriure's a l'activitat";
        }
        else {
            return "N-Inscripcions des de: ".concat(formato.format(fechaInicioInscripcion)).concat(" fins a ").concat(formato.format(fechaFinInscripcion));
        }
    }

    public Date dameFechaValidacion(Long personaId, Long ofertaId, String tipo) {
        Tuple tupla = new JPAQuery(entityManager)
                .from(qOferta, qPeriodoDTO)
                .where(qOferta.id.eq(ofertaId)
                        .and(qOferta.periodoDTO.id.eq(qPeriodoDTO.id)))
                .singleResult(qOferta.fechaInicioValidacion, qPeriodoDTO.fechaInicioValidacion,
                        qOferta.fechaFinValidacion, qPeriodoDTO.fechaFinValidacion);

        Date fechaInicioValidacion = tupla.get(qOferta.fechaInicioValidacion) != null ? tupla.get(qOferta.fechaInicioValidacion) : tupla.get(qPeriodoDTO.fechaInicioValidacion);
        Date fechaFinValidacion = tupla.get(qOferta.fechaFinValidacion) != null ? tupla.get(qOferta.fechaFinValidacion) : tupla.get(qPeriodoDTO.fechaFinValidacion);

        String retardo = dameImpInscPer(ofertaId, personaId, "RETRASOVALIDA", null, null, new Date());

        if(fechaInicioValidacion != null && ("INI".equals(tipo) || "VIN".equals(tipo))) {
            return DateExtensions.addDays(fechaInicioValidacion, Integer.parseInt(retardo));
        }
        else if(fechaFinValidacion != null && ("FIN".equals(tipo) || "VIN-FIN".equals(tipo))) {
            return DateExtensions.addDays(fechaFinValidacion, Integer.parseInt(retardo));
        }
        else
            return null;
    }

    public String dameImpInscPer(Long ofertaId, Long personaId, String tipo, Long tarjeta, String uso, Date fecha) {
        String sql = "select min(tarjeta_deportiva_tipo_id) " +
                "from se_tarjetas_deportivas td " +
                "where td.persona_id = ?1 " +
                "and trunc(sysdate) between trunc(fecha_alta) and trunc(nvl(fecha_baja, sysdate + 1))";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, personaId);
        Long tarjetaDeportivaId = Utils.convierteBigDecimalEnLong((BigDecimal) query.getSingleResult());

        sql = "select nvl(max(descuento), 0), nvl(max(tipo), 'EUROS') " +
                "from (select d.descuento, d.tipo " +
                "      from se_actividades_oferta o join se_periodos_descuentos d on d.periodo_id = o.periodo_id " +
                "      and o.id = ?1 " +
                "      and trunc(to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(DateExtensions.truncDate(fecha)) + "', 'dd/mm/yyyy')) >= trunc(d.fecha) " +
                "      order by d.fecha desc) " +
                "where rownum = 1";

        Query query2 = entityManager.createNativeQuery(sql);
        query2.setParameter(1, ofertaId);

        List<Object[]> resultado = query2.getResultList();
        Long descuento = null;
        String tipoDescuento = null;

        for(Object[] b : resultado) {
            descuento = Utils.convierteBigDecimalEnLong((BigDecimal) b[0]);
            tipoDescuento = (String) b[1];
        }

        Float importe = null;
        Long diasRetrasoInscripcion = null;
        Long diasRetrasoValidacion = null;
        String vinculoActual = null;
        Long tarifaId = null;

        sql = "select o.importe, dias_retraso_ins, dias_retraso_valida, tv.nombre vinculo, o.id tarifa_id " +
                "from se_ofertas_tarifas o join se_tipos tv on tv.id = o.vinculo_id " +
                "where o.vinculo_id = dame_vinculo_persona(?1, 'TARIFAS-ACTIVIDADES') " +
                "and o.oferta_id = " + ofertaId + " and trunc(sysdate) between trunc(fecha_inicio) and trunc(nvl(fecha_fin, sysdate + 1))";

        try {
            Query query3 = entityManager.createNativeQuery(sql);
            query3.setParameter(1, personaId);
            resultado = query3.getResultList();

            if(resultado.isEmpty()) { throw new Exception(); }

            for(Object[] b : resultado) {
                importe = ((BigDecimal) b[0]).floatValue();
                diasRetrasoInscripcion = Utils.convierteBigDecimalEnLong((BigDecimal) b[1]);
                diasRetrasoValidacion = Utils.convierteBigDecimalEnLong((BigDecimal) b[2]);
                vinculoActual = (String) b[3];
                tarifaId = Utils.convierteBigDecimalEnLong((BigDecimal) b[4]);
            }

            if(tarjetaDeportivaId != null) {
                sql = "select min(importe) " +
                        "from se_ofertas_tarifas_dto " +
                        "where oferta_tarifa_id = ?1 " +
                        "and tarjeta_deportiva_id = ?2 " + tarjetaDeportivaId + " " +
                        "and trunc(sysdate) between trunc(fecha_inicio) and trunc(nvl(fecha_fin, sysdate + 1))";

                Query query4 = entityManager.createNativeQuery(sql);
                query4.setParameter(1, tarifaId);
                Float importeConDescuento = (Float) query4.getSingleResult();

                if(importeConDescuento != null)
                    importe = importeConDescuento;
                else {
                    if(descuento > 0) {
                        if(tipoDescuento.equals("EUROS"))
                            importe = importe - descuento;
                        else
                            importe = importe * (1 - (descuento / 100.0f));
                    }
                }
            }
            else if(descuento > 0) {
                if(tipoDescuento.equals("EUROS"))
                    importe = importe - descuento;
                else
                    importe = importe * (1 - (descuento / 100.0f));
            }
        }
        catch (Exception e) {
            try {
                sql = "select t.importe, t.dias_retraso_ins, t.dias_retraso_valida, tv.nombre vinculo, t.id tarifa_id " +
                        "from se_tipos at join se_actividades a on a.tipo_tarifa_id = at.id " +
                        "join se_actividades_tarifas t on t.tipo_id = at.id and t.periodo_id is null " +
                        "join se_tipos tp on tp.id = at.tipo_id and tp.uso = 'TARIFA1' " +
                        "join se_actividades_oferta o on o.actividad_id = a.id and o.id = :ofertaId " +
                        "join se_tipos tv on tv.id = dame_vinculo_persona(:personaId, 'TARIFAS-ACTIVIDADES') and tv.id = t.vinculo_id";

                Query query5 = entityManager.createNativeQuery(sql);
                query5.setParameter("ofertaId", ofertaId);
                query5.setParameter("personaId", personaId);
                resultado = query5.getResultList();

                if(resultado.isEmpty()) { throw new Exception(); }

                for(Object[] b : resultado) {
                    importe = ((BigDecimal) b[0]).floatValue();
                    diasRetrasoInscripcion = Utils.convierteBigDecimalEnLong((BigDecimal) b[1]);
                    diasRetrasoValidacion = Utils.convierteBigDecimalEnLong((BigDecimal) b[2]);
                    vinculoActual = (String) b[3];
                    tarifaId = Utils.convierteBigDecimalEnLong((BigDecimal) b[4]);
                }

                if(tarjetaDeportivaId != null) {
                    sql = "select min(importe) " +
                            "from se_ofertas_tarifas_dto " +
                            "where oferta_tarifa_id = :tarifaId " +
                            "and tarjeta_deportiva_id = :tarjetaDeportivaId " +
                            "and trunc(sysdate) between trunc(fecha_inicio) and trunc(nvl(fecha_fin, sysdate + 1))";

                    Query query6 = entityManager.createNativeQuery(sql);
                    query6.setParameter("tarifaId", tarifaId);
                    query6.setParameter("tarjetaDeportivaId", tarjetaDeportivaId);
                    Float importeConDescuento = (Float) query6.getSingleResult();

                    if(importeConDescuento != null)
                        importe = importeConDescuento;
                    else {
                        if(descuento > 0) {
                            if(tipoDescuento.equals("EUROS"))
                                importe = importe - descuento;
                            else
                                importe = importe * (1 - (descuento / 100.0f));
                        }
                    }
                }
                else if(descuento > 0) {
                    if(tipoDescuento.equals("EUROS"))
                        importe = importe - descuento;
                    else
                        importe = importe * (1 - (descuento / 100.0f));
                }
            }
            catch (Exception ex) {
                switch (tipo) {
                    case "SN":
                    case "TEXTO":
                        return "N";

                    case "IMPORTE":
                        return "-1";

                    case "RETRASO":
                    case "RETRASOVALIDA":
                        return "0";

                    case "VINCULO":
                        return "NSNC";

                    case "ALL":
                        return "<registro><sn>N</sn><importe>-1</importe><retraso>0</retraso><retrasovalida>0</retrasovalida><texto>N</texto><vinculo>NSNC</vinculo></registro>";
                }
            }
        }

        switch (tipo) {
            case "SN":
                return "S";

            case "IMPORTE":
                return String.valueOf(importe);

            case "RETRASO":
                return String.valueOf(diasRetrasoInscripcion);

            case "RETRASOVALIDA":
                return String.valueOf(diasRetrasoValidacion);

            case "TEXTO":
                return String.valueOf(importe).concat(" euros (tarifa: ").concat(vinculoActual).concat(")");

            case "VINCULO":
                return vinculoActual;

            case "ALL":
                return "<registro><sn>S</sn><importe>".concat(String.valueOf(importe)).concat("</importe><retraso>")
                        .concat(String.valueOf(diasRetrasoInscripcion)).concat("</retraso><retrasovalida>")
                        .concat(String.valueOf(diasRetrasoValidacion)).concat("</retrasovalida><texto>")
                        .concat(String.valueOf(importe)).concat("€ (tarifa: ").concat(vinculoActual)
                        .concat(")</texto><vinculo>").concat(vinculoActual)
                        .concat("</vinculo></registro>");
        }

        return null;
    }

    public List<HistoricoInscripcion> getHistoricoInscripciones(Paginacion paginacion, Long personaId, Long curso) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qHistoricoInscripcionVW)
                .where(qHistoricoInscripcionVW.personaId.eq(personaId)
                        .and(qHistoricoInscripcionVW.curso.eq(curso)));

        if (paginacion != null) {
            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if(atributoOrdenacion != null) {
                query.orderBy(ordenaHistoricoIncripciones(orden, atributoOrdenacion));
            }
            else
                query.orderBy(qHistoricoInscripcionVW.fechaAccion.asc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.
                list(qHistoricoInscripcionVW.periodo,
                        qHistoricoInscripcionVW.actividadNombre,
                        qHistoricoInscripcionVW.grupo,
                        qHistoricoInscripcionVW.operacion,
                        qHistoricoInscripcionVW.diaInscripcion,
                        qHistoricoInscripcionVW.estadoId,
                        qHistoricoInscripcionVW.estadoNombre,
                        qHistoricoInscripcionVW.observaciones,
                        qHistoricoInscripcionVW.origen,
                        qHistoricoInscripcionVW.numeroVeces,
                        qHistoricoInscripcionVW.fechaBaja,
                        qHistoricoInscripcionVW.fechaAccion,
                        qHistoricoInscripcionVW.usuarioAccion,
                        qHistoricoInscripcionVW.apto)
                .stream()
                .map(tuple -> {
                    HistoricoInscripcion historicoInscripcion = new HistoricoInscripcion();
                    historicoInscripcion.setPeriodo(tuple.get(qHistoricoInscripcionVW.periodo));
                    historicoInscripcion.setActividadNombre(tuple.get(qHistoricoInscripcionVW.actividadNombre));
                    historicoInscripcion.setGrupo(tuple.get(qHistoricoInscripcionVW.grupo));
                    historicoInscripcion.setTipo(tuple.get(qHistoricoInscripcionVW.operacion));
                    historicoInscripcion.setFechaInscripcion(tuple.get(qHistoricoInscripcionVW.diaInscripcion));
                    historicoInscripcion.setEstadoId(tuple.get(qHistoricoInscripcionVW.estadoId));
                    historicoInscripcion.setEstadoNombre(tuple.get(qHistoricoInscripcionVW.estadoNombre));
                    historicoInscripcion.setObservaciones(tuple.get(qHistoricoInscripcionVW.observaciones));
                    historicoInscripcion.setOrigen(tuple.get(qHistoricoInscripcionVW.origen));
                    historicoInscripcion.setNumeroVeces(tuple.get(qHistoricoInscripcionVW.numeroVeces));
                    historicoInscripcion.setFechaBaja(tuple.get(qHistoricoInscripcionVW.fechaBaja));
                    historicoInscripcion.setFechaAccion(tuple.get(qHistoricoInscripcionVW.fechaAccion));
                    historicoInscripcion.setUsuarioAccion(tuple.get(qHistoricoInscripcionVW.usuarioAccion));
                    historicoInscripcion.setApto(tuple.get(qHistoricoInscripcionVW.apto));
                    return historicoInscripcion;
                }).collect(Collectors.toList());
    }

    private OrderSpecifier<?> ordenaHistoricoIncripciones(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "periodo":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.periodo.asc() : qHistoricoInscripcionVW.periodo.desc();

            case "grupo":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.grupo.asc() : qHistoricoInscripcionVW.grupo.desc();

            case "actividadNombre":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.actividadNombre.asc() : qHistoricoInscripcionVW.actividadNombre.desc();

            case "tipo":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.operacion.asc() : qHistoricoInscripcionVW.operacion.desc();

            case "fechaInscripcion":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.diaInscripcion.asc() : qHistoricoInscripcionVW.diaInscripcion.desc();

            case "estadoId":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.estadoId.asc() : qHistoricoInscripcionVW.estadoId.desc();

            case "origen":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.origen.asc() : qHistoricoInscripcionVW.origen.desc();

            case "numeroVeces":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.numeroVeces.asc() : qHistoricoInscripcionVW.numeroVeces.desc();

            case "fechaBaja":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.fechaBaja.asc() : qHistoricoInscripcionVW.fechaBaja.desc();

            case "fechaAccion":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.fechaAccion.asc() : qHistoricoInscripcionVW.fechaAccion.desc();

            case "usuarioAccion":
                return orden.equals("ASC") ? qHistoricoInscripcionVW.usuarioAccion.asc() : qHistoricoInscripcionVW.usuarioAccion.desc();

            default:
                return qHistoricoInscripcionVW.fechaAccion.asc();
        }
    }

    public String dameImpInscPer(Long ofertaId, Long personaId, String tipo, Date fecha) {
        String sql = "select min(tarjeta_deportiva_tipo_id) " +
                "from se_tarjetas_deportivas td " +
                "where td.persona_id = ?1 " +
                "and trunc(sysdate) between trunc(fecha_alta) and trunc(nvl(fecha_baja, sysdate + 1))";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, personaId);
        Long tarjetaDeportivaId = Utils.convierteBigDecimalEnLong((BigDecimal) query.getSingleResult());

        sql = "select nvl(max(descuento), 0), nvl(max(tipo), 'EUROS') " +
                "from (select d.descuento, d.tipo " +
                "      from se_actividades_oferta o join se_periodos_descuentos d on d.periodo_id = o.periodo_id " +
                "      and o.id = ?1 " +
                "      and trunc(to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(DateExtensions.truncDate(fecha)) + "', 'dd/mm/yyyy')) >= trunc(d.fecha) " +
                "      order by d.fecha desc) " +
                "where rownum = 1";

        Query query2 = entityManager.createNativeQuery(sql);
        query2.setParameter(1, ofertaId);

        List<Object[]> resultado = query2.getResultList();
        Long descuento = null;
        String tipoDescuento = null;

        for (Object[] b : resultado) {
            descuento = Utils.convierteBigDecimalEnLong((BigDecimal) b[0]);
            tipoDescuento = (String) b[1];
        }

        Float importe = null;
        Long diasRetrasoInscripcion = null;
        Long diasRetrasoValidacion = null;
        String vinculoActual = null;
        Long tarifaId = null;

        sql = "select o.importe, dias_retraso_ins, dias_retraso_valida, tv.nombre vinculo, o.id tarifa_id " +
                "from se_ofertas_tarifas o join se_tipos tv on tv.id = o.vinculo_id " +
                "where o.vinculo_id = dame_vinculo_persona(?1, 'TARIFAS-ACTIVIDADES') " +
                "and o.oferta_id = " + ofertaId + " and trunc(sysdate) between trunc(fecha_inicio) and trunc(nvl(fecha_fin, sysdate + 1))";

        try {
            Query query3 = entityManager.createNativeQuery(sql);
            query3.setParameter(1, personaId);
            resultado = query3.getResultList();

            if (resultado.isEmpty()) {
                throw new Exception();
            }

            for (Object[] b : resultado) {
                importe = ((BigDecimal) b[0]).floatValue();
                diasRetrasoInscripcion = Utils.convierteBigDecimalEnLong((BigDecimal) b[1]);
                diasRetrasoValidacion = Utils.convierteBigDecimalEnLong((BigDecimal) b[2]);
                vinculoActual = (String) b[3];
                tarifaId = Utils.convierteBigDecimalEnLong((BigDecimal) b[4]);
            }

            if (tarjetaDeportivaId != null) {
                sql = "select min(importe) " +
                        "from se_ofertas_tarifas_dto " +
                        "where oferta_tarifa_id = ?1 " +
                        "and tarjeta_deportiva_id = ?2 " + tarjetaDeportivaId + " " +
                        "and trunc(sysdate) between trunc(fecha_inicio) and trunc(nvl(fecha_fin, sysdate + 1))";

                Query query4 = entityManager.createNativeQuery(sql);
                query4.setParameter(1, tarifaId);
                Float importeConDescuento = (Float) query4.getSingleResult();

                if (importeConDescuento != null)
                    importe = importeConDescuento;
                else {
                    if (descuento > 0) {
                        if (tipoDescuento.equals("EUROS"))
                            importe = importe - descuento;
                        else
                            importe = importe * (1 - (descuento / 100.0f));
                    }
                }
            } else if (descuento > 0) {
                if (tipoDescuento.equals("EUROS"))
                    importe = importe - descuento;
                else
                    importe = importe * (1 - (descuento / 100.0f));
            }
        } catch (Exception e) {
            try {
                sql = "select t.importe, t.dias_retraso_ins, t.dias_retraso_valida, tv.nombre vinculo, t.id tarifa_id " +
                        "from se_tipos at join se_actividades a on a.tipo_tarifa_id = at.id " +
                        "join se_actividades_tarifas t on t.tipo_id = at.id and t.periodo_id is null " +
                        "join se_tipos tp on tp.id = at.tipo_id and tp.uso = 'TARIFA1' " +
                        "join se_actividades_oferta o on o.actividad_id = a.id and o.id = :ofertaId " +
                        "join se_tipos tv on tv.id = dame_vinculo_persona(:personaId, 'TARIFAS-ACTIVIDADES') and tv.id = t.vinculo_id";

                Query query5 = entityManager.createNativeQuery(sql);
                query5.setParameter("ofertaId", ofertaId);
                query5.setParameter("personaId", personaId);
                resultado = query5.getResultList();

                if (resultado.isEmpty()) {
                    throw new Exception();
                }

                for (Object[] b : resultado) {
                    importe = ((BigDecimal) b[0]).floatValue();
                    diasRetrasoInscripcion = Utils.convierteBigDecimalEnLong((BigDecimal) b[1]);
                    diasRetrasoValidacion = Utils.convierteBigDecimalEnLong((BigDecimal) b[2]);
                    vinculoActual = (String) b[3];
                    tarifaId = Utils.convierteBigDecimalEnLong((BigDecimal) b[4]);
                }

                if (tarjetaDeportivaId != null) {
                    sql = "select min(importe) " +
                            "from se_ofertas_tarifas_dto " +
                            "where oferta_tarifa_id = :tarifaId " +
                            "and tarjeta_deportiva_id = :tarjetaDeportivaId " +
                            "and trunc(sysdate) between trunc(fecha_inicio) and trunc(nvl(fecha_fin, sysdate + 1))";

                    Query query6 = entityManager.createNativeQuery(sql);
                    query6.setParameter("tarifaId", tarifaId);
                    query6.setParameter("tarjetaDeportivaId", tarjetaDeportivaId);
                    Float importeConDescuento = (Float) query6.getSingleResult();

                    if (importeConDescuento != null)
                        importe = importeConDescuento;
                    else {
                        if (descuento > 0) {
                            if (tipoDescuento.equals("EUROS"))
                                importe = importe - descuento;
                            else
                                importe = importe * (1 - (descuento / 100.0f));
                        }
                    }
                } else if (descuento > 0) {
                    if (tipoDescuento.equals("EUROS"))
                        importe = importe - descuento;
                    else
                        importe = importe * (1 - (descuento / 100.0f));
                }
            } catch (Exception ex) {
                switch (tipo) {
                    case "SN":
                    case "TEXTO":
                        return "N";

                    case "IMPORTE":
                        return "-1";

                    case "RETRASO":
                    case "RETRASOVALIDA":
                        return "0";

                    case "VINCULO":
                        return "NSNC";

                    case "ALL":
                        return "<registro><sn>N</sn><importe>-1</importe><retraso>0</retraso><retrasovalida>0</retrasovalida><texto>N</texto><vinculo>NSNC</vinculo></registro>";
                }
            }
        }

        switch (tipo) {
            case "SN":
                return "S";

            case "IMPORTE":
                return String.valueOf(importe);

            case "RETRASO":
                return String.valueOf(diasRetrasoInscripcion);

            case "RETRASOVALIDA":
                return String.valueOf(diasRetrasoValidacion);

            case "TEXTO":
                return String.valueOf(importe).concat(" euros (tarifa: ").concat(vinculoActual).concat(")");

            case "VINCULO":
                return vinculoActual;

            case "ALL":
                return "<registro><sn>S</sn><importe>".concat(String.valueOf(importe)).concat("</importe><retraso>")
                        .concat(String.valueOf(diasRetrasoInscripcion)).concat("</retraso><retrasovalida>")
                        .concat(String.valueOf(diasRetrasoValidacion)).concat("</retrasovalida><texto>")
                        .concat(String.valueOf(importe)).concat("€ (tarifa: ").concat(vinculoActual)
                        .concat(")</texto><vinculo>").concat(vinculoActual)
                        .concat("</vinculo></registro>");
        }

        return null;
    }

}
