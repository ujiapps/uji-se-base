package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.QEquipacionDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("equipacionActivaDAO")
public class EquipacionActivaDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    QEquipacionDTO qEquipacionDTO = QEquipacionDTO.equipacionDTO;

    private Map<String, Path> relations = new HashMap<>();

    public EquipacionActivaDAO() {
        relations.put("tipoNombre", qEquipacionDTO.tipoDTO.nombre);
        relations.put("modeloNombre", qEquipacionDTO.modeloDTO.nombre);
        relations.put("grupoNombre", qEquipacionDTO.gruposDTO.nombre);
        relations.put("generoNombre", qEquipacionDTO.generoDTO.nombre);
        relations.put("fechaInicio", qEquipacionDTO.fechaInicio);
        relations.put("fechaFin", qEquipacionDTO.fechaFin);
    }

    @Override
    public List<LookupItem> search(String s) {

        Date hoy = new Date();

        JPAQuery query = new JPAQuery(entityManager)
                            .from(qEquipacionDTO)
                            .where(qEquipacionDTO.fechaInicio.loe(hoy).and(qEquipacionDTO.fechaFin.isNull().or(qEquipacionDTO.fechaFin.gt(hoy))))
                            .orderBy(qEquipacionDTO.id.asc());

        if(!s.isEmpty()) {
            try {
                query.where(qEquipacionDTO.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qEquipacionDTO.tipoDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s))
                        .or(Utils.limpia(qEquipacionDTO.modeloDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s))
                        .or(Utils.limpia(qEquipacionDTO.generoDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s))))
                );
            }
        }

        return query
                .list(qEquipacionDTO.id,
                        qEquipacionDTO.tipoDTO.nombre,
                        qEquipacionDTO.modeloDTO.nombre,
                        qEquipacionDTO.generoDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qEquipacionDTO.id).toString());
                    item.setNombre(tuple.get(qEquipacionDTO.tipoDTO.nombre) + " - " + tuple.get(qEquipacionDTO.modeloDTO.nombre) + " - " + tuple.get(qEquipacionDTO.generoDTO.nombre));
                    item.addExtraParam("tipoNombre", tuple.get(qEquipacionDTO.tipoDTO.nombre));
                    item.addExtraParam("modeloNombre", tuple.get(qEquipacionDTO.modeloDTO.nombre));
                    item.addExtraParam("generoNombre", tuple.get(qEquipacionDTO.generoDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }
}
