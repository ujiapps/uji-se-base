package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.OrderSpecifier;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.ParametroService;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Repository
public class PersonaSancionDAO extends BaseDAODatabaseImpl {
    @InjectParam
    ParametroService parametroService;

    private final QSancionVistaDTO qSancionVista = QSancionVistaDTO.sancionVistaDTO;
    private final QTipoDTO qTipo = QTipoDTO.tipoDTO;
    private final QSancionDTO qSancion = QSancionDTO.sancionDTO;


    public List<Sancion> getSanciones(Long personaId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qSancionVista)
                .leftJoin(qTipo).on(qSancionVista.nivel.id.eq(qTipo.id))
                .where(qSancionVista.personaUjiDTO.id.eq(personaId));

        if (paginacion != null) {
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if (atributoOrdenacion != null) {
                query.orderBy(ordenaSanciones(orden, atributoOrdenacion));
            } else
                query.orderBy(qSancionVista.fecha.desc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.
                list(new QSancion(qSancionVista.id,
                        qSancionVista.fecha,
                        qSancionVista.fechaFin,
                        qTipo.id,
                        qTipo.nombre,
                        qSancionVista.motivo,
                        qSancionVista.tipo,
                        qSancionVista.modificable,
                        qSancionVista.origen));
    }

    @Transactional
    public void addSancion(Sancion sancion, Long personaId, Long connectedUserId) {
        SancionDTO sancionDTO = new SancionDTO();
        sancionDTO.setPersonaId(personaId);
        sancionDTO.setFecha(sancion.getFecha());
        sancionDTO.setFechaFin(sancion.getFechaFin());
        sancionDTO.setMotivo(sancion.getMotivo());
        sancionDTO.setTipo(sancion.getTipo());
        sancionDTO.setPersonaAccion(connectedUserId);
        sancionDTO.setNivel(sancion.getNivelId() != null ? new JPAQuery(entityManager)
                .from(qTipo)
                .where(qTipo.id.eq(sancion.getNivelId()))
                .singleResult(qTipo) : null);
        this.insert(sancionDTO);
    }

    @Transactional
    public void insertSancionValor(Long perId, Long valor, String tipo, String comentario) {
        SancionDTO sancionDTO = new SancionDTO();

        Date fecha = Date.from(LocalDate.now().plusDays(7 * valor).atStartOfDay(ZoneId.systemDefault()).toInstant());

        sancionDTO.setPersonaId(perId);
        sancionDTO.setFecha(new Date());
        sancionDTO.setFechaFin(fecha);
        sancionDTO.setMotivo(comentario);
        sancionDTO.setTipo(tipo);

        this.insert(sancionDTO);
    }

    @Transactional
    public void updateSancion(Sancion sancion, Long sancionId, TipoDTO tipoDTO, Long connectedUserId) {
        new JPAUpdateClause(entityManager, qSancion)
                .set(qSancion.fecha, sancion.getFecha())
                .set(qSancion.fechaFin, sancion.getFechaFin())
                .set(qSancion.motivo, sancion.getMotivo())
                .set(qSancion.tipo, sancion.getTipo())
                .set(qSancion.personaAccion, connectedUserId)
                .set(qSancion.nivel, tipoDTO.getId() != null ? tipoDTO : null)
                .where(qSancion.id.eq(sancionId))
                .execute();
    }

    @Transactional
    public void deleteSancion(Long sancionId) {
        new JPADeleteClause(entityManager, qSancion)
                .where(qSancion.id.eq(sancionId))
                .execute();
    }

    private OrderSpecifier<?> ordenaSanciones(String orden, String atributoOrdenacion) {

        if (atributoOrdenacion.equals("id")) {
            return orden.equals("ASC") ? qSancionVista.id.asc() : qSancionVista.id.desc();
        }
        if (atributoOrdenacion.equals("fecha")) {
            return orden.equals("ASC") ? qSancionVista.fecha.asc() : qSancionVista.fecha.desc();
        }
        if (atributoOrdenacion.equals("fechaFin")) {
            return orden.equals("ASC") ? qSancionVista.fechaFin.asc() : qSancionVista.fechaFin.desc();
        }
        if (atributoOrdenacion.equals("nivelId")) {
            return orden.equals("ASC") ? qTipo.id.asc() : qTipo.id.desc();
        }
        if (atributoOrdenacion.equals("nivelNombre")) {
            return orden.equals("ASC") ? qTipo.nombre.asc() : qTipo.nombre.desc();
        }
        if (atributoOrdenacion.equals("motivo")) {
            return orden.equals("ASC") ? qSancionVista.motivo.asc() : qSancionVista.motivo.desc();
        }
        if (atributoOrdenacion.equals("tipo")) {
            return orden.equals("ASC") ? qSancionVista.tipo.asc() : qSancionVista.tipo.desc();
        }
        if (atributoOrdenacion.equals("modificable")) {
            return orden.equals("ASC") ? qSancionVista.modificable.asc() : qSancionVista.modificable.desc();
        }
        return qSancionVista.fecha.desc();
    }

    public List<HistoricoSancion> getHistoricoSanciones(Long personaId, Long conectedUserId) {

        QSancionHistoricoDTO qSancionHistoricoDTO = QSancionHistoricoDTO.sancionHistoricoDTO;
        QPersonaUjiDTO qPersonaUjiDTO = QPersonaUjiDTO.personaUjiDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSancionHistoricoDTO)
                .leftJoin(qSancionHistoricoDTO.nivel, qTipo)
                .leftJoin(qSancionHistoricoDTO.xPersonaId, qPersonaUjiDTO)
                .where(qSancionHistoricoDTO.personaId.eq(personaId)
                        .and(qSancionHistoricoDTO.xFecha.between(parametroService.getParametroCursoAcademico(conectedUserId).getFechaInicio(), parametroService.getParametroCursoAcademico(conectedUserId).getFechaFin())))
                .orderBy(qSancionHistoricoDTO.id.asc()).orderBy(qSancionHistoricoDTO.xFecha.asc());

        return query.list(new QHistoricoSancion(qSancionHistoricoDTO.id,
                qSancionHistoricoDTO.fecha,
                qSancionHistoricoDTO.fechaFin,
                qSancionHistoricoDTO.xFecha,
                qSancionHistoricoDTO.nivel.nombre,
                qSancionHistoricoDTO.motivo,
                qSancionHistoricoDTO.tipo,
                qSancionHistoricoDTO.xTipoDml,
                qSancionHistoricoDTO.xPersonaId.apellido1.append(" ").append(qSancionHistoricoDTO.xPersonaId.apellido2).append(", ").append(qSancionHistoricoDTO.xPersonaId.nombre)));
    }

    public SancionDTO getSancionById(Long sancionId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qSancion).where(qSancion.id.eq(sancionId));
        return query.uniqueResult(qSancion);

    }
}
