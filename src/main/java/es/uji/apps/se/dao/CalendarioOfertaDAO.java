package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.CalendarioOfertaDTO;
import es.uji.apps.se.dto.OfertaDTO;
import es.uji.apps.se.dto.QCalendarioDTO;
import es.uji.apps.se.dto.QCalendarioOfertaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class CalendarioOfertaDAO extends BaseDAODatabaseImpl {

    private QCalendarioOfertaDTO qCalendarioOferta = QCalendarioOfertaDTO.calendarioOfertaDTO;

    public List<CalendarioOfertaDTO> getCalendariosOfertaByOferta(OfertaDTO ofertaDTO) {


        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCalendarioOferta).where(qCalendarioOferta.ofertaDTO.id.eq(ofertaDTO.getId()));

        return query.list(qCalendarioOferta);

    }

    public List<Map<String,String>> getDiasYHorasCalendariosOfertaByOferta(OfertaDTO ofertaDTO) {
        QCalendarioDTO calendario = QCalendarioDTO.calendarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCalendarioOferta).join(qCalendarioOferta.calendarioDTO, calendario).where(qCalendarioOferta.ofertaDTO.id.eq(ofertaDTO.getId()));

        return query.distinct().list(qCalendarioOferta.horaInicio,qCalendarioOferta.horaFin,qCalendarioOferta.calendarioDTO.dia)
                .stream().map(a -> {
                    Map<String,String> ofertas = new HashMap<>();
                    ofertas.put("horaInicio", a.get(qCalendarioOferta.horaInicio));
                    ofertas.put("horaFin", a.get(qCalendarioOferta.horaFin));
                    LocalDate localDate = new java.sql.Date(a.get(qCalendarioOferta.calendarioDTO.dia).getTime()).toLocalDate();
                    ofertas.put("diaSemana", String.valueOf(localDate.getDayOfWeek().getValue()));
                    return ofertas;
                }).distinct().collect(Collectors.toList());
    }
}
