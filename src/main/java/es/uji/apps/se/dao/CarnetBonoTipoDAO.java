package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.NumberExpression;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Bono;
import es.uji.apps.se.model.TarjetaBono;
import es.uji.apps.se.services.PersonaSancionService;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository("carnetBonoTiposDAO")
public class CarnetBonoTipoDAO extends BaseDAODatabaseImpl {
    private final PersonaSancionService personaSancionService;
    private final QCarnetBonoDTO qCarnetBono = QCarnetBonoDTO.carnetBonoDTO;
    private final QCarnetBonoTipoDTO qCarnetBonoTipoDTO = QCarnetBonoTipoDTO.carnetBonoTipoDTO;
    private final QCarnetBonoDetalleDTO qCarnetBonoDetalle = QCarnetBonoDetalleDTO.carnetBonoDetalleDTO;

    public CarnetBonoTipoDAO(PersonaSancionService personaSancionService) {
        this.personaSancionService = personaSancionService;
    }

    public List<CarnetBonoTipoDTO> getCarnetBonoTipos() {
        QCarnetBonoTipoDTO qCarnetBonoTipoDTO = QCarnetBonoTipoDTO.carnetBonoTipoDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCarnetBonoTipoDTO).where(qCarnetBonoTipoDTO.activo.isTrue())
                .orderBy(qCarnetBonoTipoDTO.ordenUso.asc())
                .list(qCarnetBonoTipoDTO);
    }

    public String getCarnetByReferenciaId(Long referenciaId) {
        return new JPAQuery(entityManager)
                .from(qCarnetBonoTipoDTO)
                .where(qCarnetBonoTipoDTO.id.eq(referenciaId))
                .singleResult(qCarnetBonoTipoDTO.nombre);
    }

    public List<Bono> getUsuarioTarjetas() throws ParseException {

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date today = formatter.parse(formatter.format(new Date()));

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCarnetBonoTipoDTO)
                .where(qCarnetBonoTipoDTO.activo.isTrue()
                        .and(
                                qCarnetBonoTipoDTO.fechaInicio.isNull().or(
                                        qCarnetBonoTipoDTO.fechaInicio.before(today)
                                                .or(qCarnetBonoTipoDTO.fechaInicio.eq(today))
                                )
                        ).and(
                                qCarnetBonoTipoDTO.fechaFin.isNull().or(
                                        qCarnetBonoTipoDTO.fechaFin.after(today)
                                                .or(qCarnetBonoTipoDTO.fechaFin.eq(today)))
                        ))
                .orderBy(qCarnetBonoTipoDTO.ordenUso.asc())
                .list(qCarnetBonoTipoDTO.id,
                        qCarnetBonoTipoDTO.nombre)
                .stream().map(tuple -> {
                    Bono bono = new Bono();
                    bono.setTipoId((tuple.get(qCarnetBonoTipoDTO.id)));
                    bono.setNombre(tuple.get(qCarnetBonoTipoDTO.nombre));
                    return bono;
                }).collect(Collectors.toList());
    }

    public Long dameUsos(Long tarjetaBonoId) {
        return new JPAQuery(entityManager)
                .from(qCarnetBonoDetalle)
                .where(qCarnetBonoDetalle.carnetBonoId.eq(tarjetaBonoId))
                .groupBy(qCarnetBonoDetalle.fechaEntrada, qCarnetBonoDetalle.tipoId)
                .singleResult(qCarnetBonoDetalle.count().sum());
    }

    public String estadoTdyb2(NumberExpression<Long> tarjetaBonoId) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        String hoy = formato.format(new Date());

        Tuple tupla = new JPAQuery(entityManager)
                .from(qCarnetBono, qCarnetBonoTipoDTO)
                .where(qCarnetBono.tipo.id.eq(qCarnetBonoTipoDTO.id)
                        .and(qCarnetBono.id.eq(tarjetaBonoId)))
                .singleResult(qCarnetBono.fechaInicio,
                        qCarnetBono.fechaFin,
                        qCarnetBono.fechaBaja,
                        qCarnetBono.id,
                        qCarnetBonoTipoDTO.usos,
                        qCarnetBono.personaId);
        if (tupla != null) {
            TarjetaBono tarjetaBono = new TarjetaBono();
            tarjetaBono.setFechaInicio(tupla.get(qCarnetBono.fechaInicio));
            tarjetaBono.setFechaFin(tupla.get(qCarnetBono.fechaFin));
            tarjetaBono.setFechaBaja(tupla.get(qCarnetBono.fechaBaja));
            tarjetaBono.setUsos(dameUsos(tupla.get(qCarnetBono.id)));
            tarjetaBono.setTotalUsos(tupla.get(qCarnetBonoTipoDTO.usos) != null ? tupla.get(qCarnetBonoTipoDTO.usos) : 9999L);
            tarjetaBono.setPersonaId(tupla.get(qCarnetBono.personaId));

            if (tarjetaBono.getFechaFin() != null) {
                if (hoy.compareTo(formato.format(tarjetaBono.getFechaFin())) > 0)
                    return "CA-Caducada";
            } else if (hoy.compareTo(formato.format(tarjetaBono.getFechaInicio())) < 0)
                return "NA-No activa";
            else if (personaSancionService.tieneSancion(tarjetaBono.getPersonaId()))
                return "SA-Usuari-Sancionat";
            else if (tarjetaBono.getUsos() >= tarjetaBono.getTotalUsos())
                return "AG-Esgotada";
            return "OK";

        }

        return null;
    }

    public CarnetBonoTipoDTO getCarnetBonoTipoById(Long tipoBonoId) {
        return new JPAQuery(entityManager)
                .from(qCarnetBonoTipoDTO)
                .where(qCarnetBonoTipoDTO.id.eq(tipoBonoId))
                .singleResult(qCarnetBonoTipoDTO);
    }
}
