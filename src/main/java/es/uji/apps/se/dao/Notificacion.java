package es.uji.apps.se.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class Notificacion {
    private static DataSource dataSource;

    private NotificaRenovacion notificaRenovacion;
    private NotificaAulaRenovacion notificaAulaRenovacion;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        Notificacion.dataSource = dataSource;
    }

    public void notificaRenovacion(Long ofertaId, Long personaId, String cuenta) {
        notificaRenovacion.notificaRenovacion(ofertaId, personaId, cuenta);
    }

    public void notificaAulaRenovacion(Long ofertaId, Long personaId) {
        notificaAulaRenovacion.notificaAulaRenovacion(ofertaId, personaId);
    }

    @PostConstruct
    public void init() {
        this.notificaRenovacion = new NotificaRenovacion(dataSource);
        this.notificaAulaRenovacion = new NotificaAulaRenovacion(dataSource);
    }

    private class NotificaRenovacion extends StoredProcedure {
        private static final String SQL = "notificaciones.notificacion_renovacion";
        private static final String P_OFERTA = "p_oferta";
        private static final String P_PERSONA = "p_persona";
        private static final String P_CUENTA = "p_cuenta";

        public NotificaRenovacion(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(P_OFERTA, Types.NUMERIC));
            declareParameter(new SqlParameter(P_PERSONA, Types.NUMERIC));
            declareParameter(new SqlParameter(P_CUENTA, Types.VARCHAR));

            compile();
        }

        public void notificaRenovacion(Long ofertaId, Long personaId, String cuenta) {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put(P_OFERTA, ofertaId);
            parametros.put(P_PERSONA, personaId);
            parametros.put(P_CUENTA, cuenta);

            execute(parametros);
        }
    }

    private class NotificaAulaRenovacion extends StoredProcedure {
        private static final String SQL = "notificaciones.notificacion_anula_renovacion";
        private static final String P_OFERTA = "p_oferta";
        private static final String P_PERSONA = "p_persona";

        public NotificaAulaRenovacion(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(P_OFERTA, Types.NUMERIC));
            declareParameter(new SqlParameter(P_PERSONA, Types.NUMERIC));

            compile();
        }

        public void notificaAulaRenovacion(Long ofertaId, Long personaId) {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put(P_OFERTA, ofertaId);
            parametros.put(P_PERSONA, personaId);

            execute(parametros);
        }
    }
}
