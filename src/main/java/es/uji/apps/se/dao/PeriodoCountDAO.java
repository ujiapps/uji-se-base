package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.PeriodoCountActividadYGrupoDTO;
import es.uji.apps.se.dto.QPeriodoCountActividadYGrupoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class PeriodoCountDAO extends BaseDAODatabaseImpl {
    public PeriodoCountActividadYGrupoDTO getTotalesByPeriodo(Long periodoId) {
        QPeriodoCountActividadYGrupoDTO qPeriodoCount = QPeriodoCountActividadYGrupoDTO.periodoCountActividadYGrupoDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPeriodoCount).where(qPeriodoCount.periodo.eq(periodoId)).uniqueResult(qPeriodoCount);

    }
}
