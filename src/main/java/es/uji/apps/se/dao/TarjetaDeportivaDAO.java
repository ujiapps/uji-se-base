package es.uji.apps.se.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.QTarjetaDeportiva;
import es.uji.apps.se.model.TarjetaDeportiva;
import es.uji.apps.se.model.filtros.FiltroTarjetaDeportiva;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class TarjetaDeportivaDAO extends BaseDAODatabaseImpl {

    private QTarjetaDeportivaDTO qTarjetaDeportiva = QTarjetaDeportivaDTO.tarjetaDeportivaDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public TarjetaDeportivaDAO() {
        relations.put("id", qTarjetaDeportiva.id);
        relations.put("tarjetaDeportivaTipoId", qTarjetaDeportiva.tarjetaDeportivaTipo.nombre);
        relations.put("personaUjiId", qTarjetaDeportiva.personaUji.busqueda);
        relations.put("mail", qTarjetaDeportiva.personaUji.mail);
        relations.put("movil", qTarjetaDeportiva.personaUji.movil);
        relations.put("fechaAlta", qTarjetaDeportiva.fechaAlta);
        relations.put("fechaBaja", qTarjetaDeportiva.fechaBaja);

    }


    public String limpiaAcentos(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim();
    }

    public List<TarjetaDeportivaDTO> getBySearch(FiltroTarjetaDeportiva filtro, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
        QPersonaVinculoDTO qPersonaVinculoDTO = QPersonaVinculoDTO.personaVinculoDTO;

        query.from(qTarjetaDeportiva)
                .join(qTarjetaDeportiva.personaUji, qPersonaUji).fetch()
                .join(qPersonaUji.personaVinculoDTO, qPersonaVinculoDTO).fetch();

        if (ParamUtils.isNotNull(filtro.getBusqueda())) {
            query.where(qTarjetaDeportiva.personaUji.busqueda.containsIgnoreCase(limpiaAcentos(filtro.getBusqueda())));
        }

        if (ParamUtils.isNotNull(filtro.getTarjetaDeportivaTipoId())) {
            query.where(qTarjetaDeportiva.tarjetaDeportivaTipo.id.eq(filtro.getTarjetaDeportivaTipoId()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaAltaDesde())) {
            query.where(qTarjetaDeportiva.fechaAlta.goe(filtro.getFechaAltaDesde()));
        }


        if (ParamUtils.isNotNull(filtro.getFechaAltaHasta())) {
            query.where(qTarjetaDeportiva.fechaAlta.lt(filtro.getFechaAltaHasta()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaBajaDesde())) {
            query.where(qTarjetaDeportiva.fechaBaja.goe(filtro.getFechaBajaDesde()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaBajaHasta())) {
            query.where(qTarjetaDeportiva.fechaBaja.lt(filtro.getFechaBajaHasta()));
        }

        Date fechaActual = new Date();

        if (ParamUtils.isNotNull(filtro.getSearchSancionados())) {
            QSancionVistaDTO qSancionVista = QSancionVistaDTO.sancionVistaDTO;
            if (filtro.getSearchSancionados().equals(1L)) {

                query.join(qPersonaUji.sanciones, qSancionVista).where(qSancionVista.fecha.before(fechaActual)
                        .and((qSancionVista.fechaFin.isNull()).or(qSancionVista.fechaFin.after(fechaActual))
                                .and(qSancionVista.tipo.substring(0, 3).ne("NNN"))));
            } else {
                query.where(qPersonaUji.id.notIn(new JPASubQuery().from(qSancionVista)
                        .where(qSancionVista.fecha.before(fechaActual)
                                .and((qSancionVista.fechaFin.isNull()).or(qSancionVista.fechaFin.after(fechaActual))
                                        .and(qSancionVista.tipo.substring(0, 3).ne("NNN")))).distinct()
                        .list(qSancionVista.personaUjiDTO.id)));

            }
        }

        if (ParamUtils.isNotNull(paginacion)) {

            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        List<TarjetaDeportivaDTO> tarjetas = query.list(qTarjetaDeportiva);
        return anyadeCamposExtraTarjetaDeportiva(tarjetas);
    }

    public TarjetaDeportiva getTarjetaDeportivaActivaByPersonaId(Long personaId) {

        Date fecha = DateExtensions.getCurrentDate();

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTarjetaDeportiva)
                .where(qTarjetaDeportiva.personaUji.id.eq(personaId)
                        .and(qTarjetaDeportiva.fechaAlta.loe(fecha)
                                .and(qTarjetaDeportiva.fechaBaja.goe(fecha))))
                .orderBy(qTarjetaDeportiva.fechaAlta.desc());

        return query.distinct().singleResult(new QTarjetaDeportiva(qTarjetaDeportiva.id,
                qTarjetaDeportiva.tarjetaDeportivaTipo.nombre,
                qTarjetaDeportiva.personaUji.nombre,
                qTarjetaDeportiva.personaUji.apellido1,
                qTarjetaDeportiva.personaUji.apellido2,
                qTarjetaDeportiva.tarjetaDeportivaTipo.permiteInstalaciones,
                qTarjetaDeportiva.tarjetaDeportivaTipo.permiteMusculacion,
                qTarjetaDeportiva.tarjetaDeportivaTipo.permiteClasesDirigidas,
                qTarjetaDeportiva.fechaAlta,
                qTarjetaDeportiva.fechaBaja,
                qTarjetaDeportiva.tarjetaDeportivaTipo.id,
                qTarjetaDeportiva.tarjetaDeportivaTipo.tipoPlantilla,
                qTarjetaDeportiva.tarjetaDeportivaTipo.imagenTarjeta
        ));

    }

    public TarjetaDeportivaDTO getTarjetaDeportivaDTOActivaByPersonaId(Long personaId) {
        Date fechaActual = DateExtensions.getCurrentDate();
        return getTarjetaDeportivaActivaByPersonaIdAndFecha(personaId, fechaActual);
    }

    public TarjetaDeportivaDTO getTarjetaDeportivaActivaByPersonaIdAndFecha(Long personaId, Date fecha) {
        JPAQuery query = new JPAQuery(entityManager);
        QTarjetaDeportivaClaseDTO qTarjetaDeportivaClase = QTarjetaDeportivaClaseDTO.tarjetaDeportivaClaseDTO;
        QCalendarioClaseDTO calendarioClase = QCalendarioClaseDTO.calendarioClaseDTO;
        QCalendarioDTO calendario = QCalendarioDTO.calendarioDTO;
        QTarjetaDeportivaSuperiorDTO qTarjetaDeportivaSuperior = QTarjetaDeportivaSuperiorDTO.tarjetaDeportivaSuperiorDTO;
        QTarjetaDeportivaTipoDTO qTarjetaDeportivaTipo = QTarjetaDeportivaTipoDTO.tarjetaDeportivaTipoDTO;

        query.from(qTarjetaDeportiva)
                .leftJoin(qTarjetaDeportiva.tarjetaDeportivaClasesDTO, qTarjetaDeportivaClase).fetch()
                .leftJoin(qTarjetaDeportivaClase.calendarioClase, calendarioClase).fetch()
                .leftJoin(calendarioClase.calendarioDTO, calendario).fetch()
                .leftJoin(qTarjetaDeportiva.tarjetaDeportivaTipo, qTarjetaDeportivaTipo).fetch()
                .leftJoin(qTarjetaDeportivaTipo.tarjetaDeportivaSuperioresDTO, qTarjetaDeportivaSuperior).fetch()
                .where(qTarjetaDeportiva.personaUji.id.eq(personaId)
                        .and(qTarjetaDeportiva.fechaAlta.loe(fecha)
                                .and(qTarjetaDeportiva.fechaBaja.goe(fecha))))
                .orderBy(qTarjetaDeportiva.fechaAlta.desc());

        List<TarjetaDeportivaDTO> tajetasDeportivas = query.distinct().list(qTarjetaDeportiva);
        if (tajetasDeportivas.isEmpty()) return null;
        return tajetasDeportivas.get(0);
    }

    public TarjetaDeportivaDTO getTarjetaDeportivaById(Long tarjetaDeportivaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaDeportiva)
                .where(qTarjetaDeportiva.id.eq(tarjetaDeportivaId));

        List<TarjetaDeportivaDTO> tajetasDeportivas = query.distinct().list(qTarjetaDeportiva);
        if (tajetasDeportivas.isEmpty()) return null;
        return tajetasDeportivas.get(0);
    }

    public TarjetaDeportivaDTO getTarjetaDeportivaFuturaByPersonaId(Long personaId) {
        Date fechaActual = DateExtensions.getCurrentDate();
        JPAQuery query = new JPAQuery(entityManager);
        QTarjetaDeportivaClaseDTO qTarjetaDeportivaClase = QTarjetaDeportivaClaseDTO.tarjetaDeportivaClaseDTO;
        QCalendarioClaseDTO calendarioClase = QCalendarioClaseDTO.calendarioClaseDTO;
        QCalendarioDTO calendario = QCalendarioDTO.calendarioDTO;

        query.from(qTarjetaDeportiva)
                .leftJoin(qTarjetaDeportiva.tarjetaDeportivaClasesDTO, qTarjetaDeportivaClase).fetch()
                .leftJoin(qTarjetaDeportivaClase.calendarioClase, calendarioClase).fetch()
                .leftJoin(calendarioClase.calendarioDTO, calendario).fetch()
                .where(qTarjetaDeportiva.personaUji.id.eq(personaId)
                        .and(qTarjetaDeportiva.fechaAlta.gt(fechaActual))
                        .and(calendario.dia.isNull().or(calendario.dia.goe(fechaActual))))
                .orderBy(qTarjetaDeportiva.fechaAlta.asc());

        List<TarjetaDeportivaDTO> tajetasDeportivas = query.distinct().list(qTarjetaDeportiva);
        if (tajetasDeportivas.isEmpty()) return null;
        return tajetasDeportivas.get(0);
    }

    public List<TarjetaDeportivaDTO> getTarjetaDeportivaByPersonaId(Long personaId, Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager);

        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
        QPersonaVinculoDTO qPersonaVinculoDTO = QPersonaVinculoDTO.personaVinculoDTO;

        query.from(qTarjetaDeportiva)
                .join(qTarjetaDeportiva.personaUji, qPersonaUji).fetch()
                .join(qPersonaUji.personaVinculoDTO, qPersonaVinculoDTO).fetch()
                .where(qTarjetaDeportiva.personaUji.id.eq(personaId));


        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return anyadeCamposExtraTarjetaDeportiva(query.list(qTarjetaDeportiva));
    }

    private List<TarjetaDeportivaDTO> anyadeCamposExtraTarjetaDeportiva(List<TarjetaDeportivaDTO> tarjetas) {
        Date fechaActual = new Date();

        for (TarjetaDeportivaDTO tarjeta : tarjetas) {
            if (tarjeta.getPersonaUji().getPersonaVinculo() != null && tarjeta.getPersonaUji().getPersonaVinculo().getVinculo() != null) {
                tarjeta.setVinculoNombre(tarjeta.getPersonaUji().getPersonaVinculo().getVinculo().getNombre());
            }

            JPAQuery query2 = new JPAQuery(entityManager);
            QSancionVistaDTO qSancionVista = QSancionVistaDTO.sancionVistaDTO;

            query2.from(qSancionVista).where(qSancionVista.personaUjiDTO.id.eq(tarjeta.getPersonaUji().getId()));

            List<SancionVistaDTO> sanciones =
                    query2.list(qSancionVista).stream()
                            .filter(s -> s.getFecha().before(fechaActual) && (!ParamUtils.isNotNull(s.getFechaFin()) || s.getFechaFin().after(fechaActual)) && !s.getTipo().startsWith("NNN"))
                            .collect(Collectors.toList());

            tarjeta.setSancion(!sanciones.isEmpty());
        }
        return tarjetas;
    }
}
