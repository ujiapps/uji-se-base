package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QIndicadorSatisfaccionDTO;
import es.uji.apps.se.model.IndicadorSatisfaccion;
import es.uji.apps.se.model.domains.TipoTemporalidad;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class IndicadorSatisfaccionDAO extends BaseDAODatabaseImpl {

    QIndicadorSatisfaccionDTO qIndicadorSatisfaccionDTO = QIndicadorSatisfaccionDTO.indicadorSatisfaccionDTO;

    public List<IndicadorSatisfaccion> getIndicadoresSatisfaccion(Long cursoId) {
        return new JPAQuery(entityManager).from(qIndicadorSatisfaccionDTO)
                .where(qIndicadorSatisfaccionDTO.cursoId.eq(cursoId))
                .list(qIndicadorSatisfaccionDTO).stream().map(tuple -> {
                    IndicadorSatisfaccion indicadorSatisfaccion = new IndicadorSatisfaccion();
                    indicadorSatisfaccion.setId(tuple.getId());
                    indicadorSatisfaccion.setPeriodoId(tuple.getPeriodoDTO() != null ? tuple.getPeriodoDTO().getId(): null);
                    indicadorSatisfaccion.setPeriodoNombre(tuple.getPeriodoDTO() != null ? tuple.getPeriodoDTO().getNombre(): null);
                    indicadorSatisfaccion.setActividadId(tuple.getActividadDTO() != null ? tuple.getActividadDTO().getId(): null);
                    indicadorSatisfaccion.setOfertaId(tuple.getOfertaDTO() != null ? tuple.getOfertaDTO().getId() : null);
                    indicadorSatisfaccion.setIndicadorTipoId(tuple.getIndicadorTipoId() != null ? tuple.getIndicadorTipoId().getId() : null);
                    indicadorSatisfaccion.setIndicadorTipoNombre(tuple.getIndicadorTipoId() != null ? tuple.getIndicadorTipoId().getNombre() : null);
                    indicadorSatisfaccion.setValor(tuple.getValor());
                    indicadorSatisfaccion.setCursoId(tuple.getCursoId());
                    indicadorSatisfaccion.setTipoTempo(tuple.getTipoTempo());
                    indicadorSatisfaccion.setTipoTempoNombre(TipoTemporalidad.getTipoById(tuple.getTipoTempo()));
                    indicadorSatisfaccion.setEncuestas(tuple.getEncuestas());
                    indicadorSatisfaccion.setPoblacion(tuple.getPoblacion());
                    return indicadorSatisfaccion;
        }).collect(Collectors.toList());
    }

    public void deleteIndicadorSatisfaccion(Long id) {
        new JPADeleteClause(entityManager, qIndicadorSatisfaccionDTO).where(qIndicadorSatisfaccionDTO.id.eq(id)).execute();
    }
}
