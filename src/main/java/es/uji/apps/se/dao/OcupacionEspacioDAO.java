package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QHorasPorInstalacionDTO;
import es.uji.apps.se.dto.QReservasPorInstalacionDTO;
import es.uji.apps.se.exceptions.ValidacionException;
import es.uji.apps.se.model.OcupacionEspacio;
import es.uji.apps.se.model.QOcupacionEspacio;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.lang.Math.min;

@Repository
public class OcupacionEspacioDAO extends BaseDAODatabaseImpl {
    public List<OcupacionEspacio> getOcupacionEspacios(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) throws ValidacionException {

        QHorasPorInstalacionDTO qHorasPorInstalacionDTO = QHorasPorInstalacionDTO.horasPorInstalacionDTO;

        JPAQuery queryCabeceras = new JPAQuery(entityManager);
        queryCabeceras.from(qHorasPorInstalacionDTO);

        List<OcupacionEspacio> ocupacionEspacios = queryCabeceras/*.where(qHorasPorInstalacionDTO.nombre.notLike("%COVID%"))*/.groupBy(qHorasPorInstalacionDTO.id, qHorasPorInstalacionDTO.nombre).list(new
                QOcupacionEspacio(qHorasPorInstalacionDTO.id, qHorasPorInstalacionDTO.nombre));

        JPAQuery queryHoras = new JPAQuery(entityManager);
        queryHoras.from(qHorasPorInstalacionDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            queryHoras.where(qHorasPorInstalacionDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            queryHoras.where(qHorasPorInstalacionDTO.dia.loe(fechaHasta));
        }

        queryHoras.groupBy(qHorasPorInstalacionDTO.id);

        Map<Long, Float> horasTotal = queryHoras.map(qHorasPorInstalacionDTO.id, qHorasPorInstalacionDTO.horas.sum());

        QReservasPorInstalacionDTO qReservasPorInstalacionDTO = QReservasPorInstalacionDTO.reservasPorInstalacionDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qReservasPorInstalacionDTO)
                .where(qReservasPorInstalacionDTO.grupoInstalaciones.isNotNull());

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qReservasPorInstalacionDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qReservasPorInstalacionDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query.where(qReservasPorInstalacionDTO.horaInicio.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query.where(qReservasPorInstalacionDTO.horaInicio.loe(horaHasta));
        }

        query.groupBy(qReservasPorInstalacionDTO.grupoInstalacionesNombre, qReservasPorInstalacionDTO.grupoInstalaciones);

        Map<Long, Float> horasPorInstalacion = query.map(qReservasPorInstalacionDTO.grupoInstalaciones, qReservasPorInstalacionDTO.horasReserva.sum());

        ocupacionEspacios.forEach(ocupacionEspacio -> {
            Float horas = horasPorInstalacion.get(ocupacionEspacio.getInstalacionGrupoId());
            if (ParamUtils.isNotNull(horas)) {
                Float subTotal = 0F;

                if (ParamUtils.isNotNull(horaDesde)) {
                    Date hIni = null;
                    try {
                        hIni = DateExtensions.getFechaConHora(new Date(), horaDesde);
                    } catch (ValidacionException e) {
                        e.printStackTrace();
                    }

                    Date hF = null;
                    if (ParamUtils.isNotNull(horaHasta)) {

                        try {
                            hF = DateExtensions.getFechaConHora(new Date(), horaHasta);
                        } catch (ValidacionException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        try {
                            hF = DateExtensions.getFechaConHora(new Date(), "23:59");
                        } catch (ValidacionException e) {
                            e.printStackTrace();
                        }
                    }

                    Long diff = hF.getTime() - hIni.getTime();
                    TimeUnit time = TimeUnit.HOURS;

                    Date dIni = fechaDesde;
                    Date dFin = fechaHasta;
                    Long diffDias = dFin.getTime() - dIni.getTime() + 1L;
                    TimeUnit timeDias = TimeUnit.DAYS;
                    Long dias = (timeDias.convert(diffDias, TimeUnit.MILLISECONDS) == 0) ? 1L : timeDias.convert(diffDias, TimeUnit.MILLISECONDS) + 1L;

                    subTotal = Float.valueOf(time.convert(diff, TimeUnit.MILLISECONDS) * dias);
                } else {
                    subTotal = horasTotal.get(ocupacionEspacio.getInstalacionGrupoId());
                }

                horas = min(horas, subTotal);

                Float horasLibres = subTotal - horas;
                ocupacionEspacio.setHoras(horas);
                ocupacionEspacio.setHorasLibres(horasLibres);
                ocupacionEspacio.setPorcentajeHoras((horas * 100) / subTotal);
                ocupacionEspacio.setPorcentajeHorasLibres((horasLibres * 100) / subTotal);
            }
        });

        return ocupacionEspacios.stream()
                .sorted(Comparator.comparing(OcupacionEspacio::getInstalacionGrupoNombre))
                .collect(Collectors.toList())
                .stream()
                .filter(ocupacionEspacio -> !(ocupacionEspacio.getHoras() == 0 && ocupacionEspacio.getHorasLibres() == 0 && ocupacionEspacio.getInstalacionGrupoNombre().contains(String.valueOf("COVID"))))
                .collect(Collectors.toList());
//        return ocupacionEspacios.stream().sorted(Comparator.comparing(OcupacionEspacio::getInstalacionGrupoNombre)).collect(Collectors.toList());
    }
}
