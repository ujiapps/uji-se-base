package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.EquipacionActividad;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("equipacionActividadDAO")
public class EquipacionActividadDAO extends BaseDAODatabaseImpl {

    private QEquipacionActividadDTO qEquipacionActividadDTO = QEquipacionActividadDTO.equipacionActividadDTO;

    private QEquipacionModeloDTO qEquipacionModeloDTO = QEquipacionModeloDTO.equipacionModeloDTO;

    private QEquipacionGeneroDTO qEquipacionGeneroDTO = QEquipacionGeneroDTO.equipacionGeneroDTO;

    private QEquipacionDTO qEquipacionDTO = QEquipacionDTO.equipacionDTO;
    private QEquipacionTipoDTO qEquipacionTipoDTO = QEquipacionTipoDTO.equipacionTipoDTO;
    private QActividadDTO qActividadDTO = QActividadDTO.actividadDTO;

    private Map<String, Path> columnas = new HashMap<>();

    public EquipacionActividadDAO() {
        columnas.put("id", qEquipacionActividadDTO.id);
        columnas.put("actividadId", qEquipacionActividadDTO.actividadId);
        columnas.put("actividadNombre", qActividadDTO.nombre);
        columnas.put("equipacionId", qEquipacionActividadDTO.equipacionId);
        columnas.put("equipacionNombre", qEquipacionTipoDTO.nombre);
        columnas.put("cursoAcademico", qEquipacionActividadDTO.cursoAcademico);
        columnas.put("vez", qEquipacionActividadDTO.vez);
        columnas.put("stockMin", qEquipacionActividadDTO.stockMin);
    }

    public List<EquipacionActividad> getEquipacionesActividadesGenericas(Paginacion paginacion, List<Map<String, String>> filtros) {
        JPAQuery query = new JPAQuery(entityManager)
                             .from(qEquipacionActividadDTO)
                             .join(qEquipacionDTO).on(qEquipacionDTO.id.eq(qEquipacionActividadDTO.equipacionId))
                             .join(qEquipacionTipoDTO).on(qEquipacionTipoDTO.id.eq(qEquipacionDTO.tipoDTO.id))
                             .join(qEquipacionModeloDTO).on(qEquipacionModeloDTO.id.eq(qEquipacionDTO.modeloDTO.id))
                             .join(qEquipacionGeneroDTO).on(qEquipacionGeneroDTO.id.eq(qEquipacionDTO.generoDTO.id))
                             .join(qActividadDTO).on(qActividadDTO.id.eq(qEquipacionActividadDTO.actividadId))
                             .where(qEquipacionActividadDTO.cursoAcademico.isNull());

        if (filtros != null) {
            filtros.forEach(item -> {
                String property = item.get("property");
                String value = item.get("value");

                if (property.equals("equipacionId")) {
                    query.where(qEquipacionActividadDTO.equipacionId.eq(Long.parseLong(value)));
                }

                if (property.equals("actividadId")) {
                    query.where(qEquipacionActividadDTO.actividadId.eq(Long.parseLong(value)));
                }
            });
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());

            if (paginacion.getLimit() > 0)
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());

            Path path = columnas.get(paginacion.getOrdenarPor());

            if (path != null) {
                try { query.orderBy((OrderSpecifier<?>) path.getClass().getMethod(paginacion.getDireccion().toLowerCase()).invoke(path)); }
                catch (Exception e) { }
            }
            else
                query.orderBy(qEquipacionActividadDTO.id.desc());
        }

        return query
               .list(qEquipacionActividadDTO.id,
                     qEquipacionActividadDTO.actividadId,
                     qActividadDTO.nombre,
                     qEquipacionActividadDTO.equipacionId,
                     qEquipacionTipoDTO.nombre,
                     qEquipacionModeloDTO.nombre,
                     qEquipacionGeneroDTO.nombre,
                     qEquipacionActividadDTO.cursoAcademico,
                     qEquipacionActividadDTO.vez,
                     qEquipacionActividadDTO.stockMin)
                .stream()
                .map(tuple -> {
                    EquipacionActividad equipacionActividad = new EquipacionActividad();
                    equipacionActividad.setId(tuple.get(qEquipacionActividadDTO.id));
                    equipacionActividad.setActividadId(tuple.get(qEquipacionActividadDTO.actividadId));
                    equipacionActividad.setActividadNombre(tuple.get(qActividadDTO.nombre));
                    equipacionActividad.setEquipacionId(tuple.get(qEquipacionActividadDTO.equipacionId));
                    equipacionActividad.setEquipacionNombre(tuple.get(qEquipacionTipoDTO.nombre) + " - " + tuple.get(qEquipacionModeloDTO.nombre) + " - " + tuple.get(qEquipacionGeneroDTO.nombre));
                    equipacionActividad.setCursoAcademico(tuple.get(qEquipacionActividadDTO.cursoAcademico));
                    equipacionActividad.setVez(tuple.get(qEquipacionActividadDTO.vez));
                    equipacionActividad.setStockMin(tuple.get(qEquipacionActividadDTO.stockMin));
                    return equipacionActividad;
                }).collect(Collectors.toList());
    }

    public long updateEquipacionActividad(EquipacionActividad equipacionActividad) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qEquipacionActividadDTO);
        updateClause.set(qEquipacionActividadDTO.stockMin, equipacionActividad.getStockMin())
                .set(qEquipacionActividadDTO.vez, equipacionActividad.getVez());
        updateClause.where(qEquipacionActividadDTO.id.eq(equipacionActividad.getId()));
        return updateClause.execute();
    }

    public List<EquipacionActividad> getEquipacionesActividadesEspecificas(Long cursoAcademico, List<Map<String, String>> filtros, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qEquipacionActividadDTO)
                .join(qEquipacionDTO).on(qEquipacionDTO.id.eq(qEquipacionActividadDTO.equipacionId))
                .join(qEquipacionTipoDTO).on(qEquipacionTipoDTO.id.eq(qEquipacionDTO.tipoDTO.id))
                .join(qEquipacionModeloDTO).on(qEquipacionModeloDTO.id.eq(qEquipacionDTO.modeloDTO.id))
                .join(qEquipacionGeneroDTO).on(qEquipacionGeneroDTO.id.eq(qEquipacionDTO.generoDTO.id))
                .join(qActividadDTO).on(qActividadDTO.id.eq(qEquipacionActividadDTO.actividadId))
                .where(qEquipacionActividadDTO.cursoAcademico.eq(cursoAcademico)
                        .or(qEquipacionActividadDTO.cursoAcademico.isNull()))
                .orderBy(qEquipacionActividadDTO.cursoAcademico.asc());

        if (filtros != null) {
            filtros.forEach(item -> {
                String property = item.get("property");
                String value = item.get("value");

                if (property.equals("equipacionId")) {
                    query.where(qEquipacionActividadDTO.equipacionId.eq(Long.parseLong(value)));
                }

                if (property.equals("actividadId")) {
                    query.where(qEquipacionActividadDTO.actividadId.eq(Long.parseLong(value)));
                }
            });
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            Path path = columnas.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy((OrderSpecifier<?>) path.getClass()
                        .getMethod(paginacion.getDireccion().toLowerCase())
                        .invoke(path));
                } catch (Exception e) {}
            }
            else query.orderBy(qEquipacionActividadDTO.id.desc());
        }

        return query
                .list(qEquipacionActividadDTO.id,
                        qEquipacionActividadDTO.actividadId,
                        qActividadDTO.nombre,
                        qEquipacionActividadDTO.equipacionId,
                        qEquipacionTipoDTO.nombre,
                        qEquipacionModeloDTO.nombre,
                        qEquipacionGeneroDTO.nombre,
                        qEquipacionActividadDTO.cursoAcademico,
                        qEquipacionActividadDTO.vez,
                        qEquipacionActividadDTO.stockMin)
                .stream()
                .map(tuple -> {
                    EquipacionActividad equipacionActividad = new EquipacionActividad();
                    equipacionActividad.setId(tuple.get(qEquipacionActividadDTO.id));
                    equipacionActividad.setActividadId(tuple.get(qEquipacionActividadDTO.actividadId));
                    equipacionActividad.setActividadNombre(tuple.get(qActividadDTO.nombre));
                    equipacionActividad.setEquipacionId(tuple.get(qEquipacionActividadDTO.equipacionId));
                    equipacionActividad.setEquipacionNombre(tuple.get(qEquipacionTipoDTO.nombre) + " - " + tuple.get(qEquipacionModeloDTO.nombre) + " - " + tuple.get(qEquipacionGeneroDTO.nombre));
                    equipacionActividad.setCursoAcademico(tuple.get(qEquipacionActividadDTO.cursoAcademico));
                    equipacionActividad.setVez(tuple.get(qEquipacionActividadDTO.vez));
                    equipacionActividad.setStockMin(tuple.get(qEquipacionActividadDTO.stockMin));
                    return equipacionActividad;
                }).collect(Collectors.toList());
    }

    public EquipacionActividadDTO getEquipacionActividadDuplicado(EquipacionActividad ea) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEquipacionActividadDTO)
            .where(qEquipacionActividadDTO.actividadId.eq(ea.getActividadId())
                .and(qEquipacionActividadDTO.equipacionId.eq(ea.getEquipacionId()))
                .and(qEquipacionActividadDTO.cursoAcademico.eq(ea.getCursoAcademico()))
                .and( ea.getVez() != null ? qEquipacionActividadDTO.vez.eq(ea.getVez()) : qEquipacionActividadDTO.vez.isNull() ));

        if (ea.getId() != null)
            query.where(qEquipacionActividadDTO.id.ne(ea.getId()));

        return query.singleResult(qEquipacionActividadDTO);

    }
}
