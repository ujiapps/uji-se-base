package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.QLineaReciboVWDTO;
import es.uji.apps.se.dto.QReciboVWDTO;
import es.uji.apps.se.utils.Utils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public class ReciboDAO extends BaseDAODatabaseImpl {

    private QLineaReciboVWDTO qLineaReciboVWDTO = QLineaReciboVWDTO.lineaReciboVWDTO;
    private QReciboVWDTO qReciboVWDTO = QReciboVWDTO.reciboVWDTO;

    public ReciboDAO() {}

    public Long getImporte(Long reciboId) {
        String sql = "select l.neto " +
                     "from rec_vw_lineas l " +
                     "where l.referencia1_id = " + reciboId + " " +
                     "and not exists (select * from rec_vw_recibos r where r.id = l.recibo_id and r.tipo_cobro_id = 4)";
        try {
            return Utils.convierteBigDecimalEnLong((BigDecimal) entityManager.createNativeQuery(sql).getSingleResult());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public Long getImporteTarjeta() {
        return new JPAQuery(entityManager)
                .from(qLineaReciboVWDTO, qReciboVWDTO)
                .where(qLineaReciboVWDTO.tarjetaBonoId.eq(qReciboVWDTO.id)
                .and(qReciboVWDTO.id.eq(qLineaReciboVWDTO.reciboId))
                .and(qReciboVWDTO.tipoCobroId.notIn(2, 4)))
                .singleResult(qLineaReciboVWDTO.neto);
    }
}
