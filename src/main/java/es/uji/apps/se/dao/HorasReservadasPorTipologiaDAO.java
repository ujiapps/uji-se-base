package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QReservasPorInstalacionDTO;
import es.uji.apps.se.model.HorasReservadasPorTipologia;
import es.uji.apps.se.model.domains.TipoZona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class HorasReservadasPorTipologiaDAO extends BaseDAODatabaseImpl {
    public List<HorasReservadasPorTipologia> getHorasReservadasPorTipologia(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {

        List<HorasReservadasPorTipologia> accesoTipologias = new ArrayList<>();
        accesoTipologias.add(new HorasReservadasPorTipologia("ALQ", -3L, "Llogers"));
        accesoTipologias.add(new HorasReservadasPorTipologia("DOC", -3L, "Docencia"));
        accesoTipologias.add(new HorasReservadasPorTipologia("DOC-USER", -3L, "Docencia Usuarios"));
        accesoTipologias.add(new HorasReservadasPorTipologia("TAR", 4L, "TA"));
        accesoTipologias.add(new HorasReservadasPorTipologia("TAR", 2L, "TEUJI"));
        accesoTipologias.add(new HorasReservadasPorTipologia("TAR", 3L, "TEUJI+"));
        accesoTipologias.add(new HorasReservadasPorTipologia("TAR", -1L, "Campus saludable"));
        accesoTipologias.add(new HorasReservadasPorTipologia("TAR", -2L, "Bonos"));
        accesoTipologias.add(new HorasReservadasPorTipologia("ACT", -3L, "Activitats amb profesorat"));
        accesoTipologias.add(new HorasReservadasPorTipologia("COMP", 8088077L, "Competició interna"));
        accesoTipologias.add(new HorasReservadasPorTipologia("COMP", 8088169L, "Competició externa"));
        accesoTipologias.add(new HorasReservadasPorTipologia("COMP", -3L, "Altres Competicions"));
        accesoTipologias.add(new HorasReservadasPorTipologia("ACT", 8088077L, "Competició interna (Reserva Act)"));
        accesoTipologias.add(new HorasReservadasPorTipologia("ACT", 8088169L, "Competició externa (Reserva Act)"));
        accesoTipologias.add(new HorasReservadasPorTipologia("APLAZ", -3L, "Aplazamientos partidos"));
        accesoTipologias.add(new HorasReservadasPorTipologia("CLA", -3L, "Clases dirigides"));
        accesoTipologias.add(new HorasReservadasPorTipologia("ADMIN", -3L, "Administració"));
        accesoTipologias.add(new HorasReservadasPorTipologia("CUR", -3L, "Cursos"));

        QReservasPorInstalacionDTO qReservasPorInstalacionDTO = QReservasPorInstalacionDTO.reservasPorInstalacionDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qReservasPorInstalacionDTO).where(qReservasPorInstalacionDTO.grupoInstalaciones.isNotNull());

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qReservasPorInstalacionDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qReservasPorInstalacionDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query.where(qReservasPorInstalacionDTO.horaInicio.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query.where(qReservasPorInstalacionDTO.horaInicio.loe(horaHasta));
        }

        List<Tuple> lista = query.groupBy(qReservasPorInstalacionDTO.origen, qReservasPorInstalacionDTO.zonaId, qReservasPorInstalacionDTO.origenTipo)
                .list(qReservasPorInstalacionDTO.origen, qReservasPorInstalacionDTO.zonaId, qReservasPorInstalacionDTO.origenTipo, qReservasPorInstalacionDTO.horasReserva.sum());

        lista.forEach(acceso -> {
            accesoTipologias.forEach(accesoTipologia -> {
                if (accesoTipologia.getTipologia().equals(acceso.get(qReservasPorInstalacionDTO.origen)) && accesoTipologia.getTipoTipologiaId().equals(acceso.get(qReservasPorInstalacionDTO.origenTipo))) {
                    Long zonaId = acceso.get(qReservasPorInstalacionDTO.zonaId);
                    if (zonaId.equals(TipoZona.RAQUETAS.getId())) {
                        accesoTipologia.setCountZonaRaquetas(acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                        accesoTipologia.setCountTotal(accesoTipologia.getCountTotal() + acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                    }
                    if (zonaId.equals(TipoZona.PABELLON.getId()) || zonaId.equals(TipoZona.MUSCULACION.getId())) {
                        accesoTipologia.setCountZonaPabellon(accesoTipologia.getCountZonaPabellon() + acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                        accesoTipologia.setCountTotal(accesoTipologia.getCountTotal() + acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                    }
                    if (zonaId.equals(TipoZona.PISCINA.getId())) {
                        accesoTipologia.setCountZonaPiscina(acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                        accesoTipologia.setCountTotal(accesoTipologia.getCountTotal() + acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                    }
                    if (zonaId.equals(TipoZona.AIRELIBRE.getId())) {
                        accesoTipologia.setCountZonaAireLibre(acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                        accesoTipologia.setCountTotal(accesoTipologia.getCountTotal() + acceso.get(qReservasPorInstalacionDTO.horasReserva.sum()));
                    }
                }
            });
        });
        return accesoTipologias.stream().sorted(Comparator.comparing(HorasReservadasPorTipologia::getTipologiaNombre)).collect(Collectors.toList());
    }
}
