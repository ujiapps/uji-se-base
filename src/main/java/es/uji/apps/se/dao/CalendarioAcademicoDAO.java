package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.CalendarioAcademicoDTO;
import es.uji.apps.se.dto.QCalendarioAcademicoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CalendarioAcademicoDAO extends BaseDAODatabaseImpl {
    public List<CalendarioAcademicoDTO> getCalendariosAcademicosByAnyo(Long cursoAcademicoId) {

        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioAcademicoDTO calendarioAcademico = QCalendarioAcademicoDTO.calendarioAcademicoDTO;

        query.from(calendarioAcademico).where(calendarioAcademico.anyo.eq(String.valueOf(cursoAcademicoId)));

        return query.list(calendarioAcademico);
    }
}
