package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.AlumnoAsignaturaDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.dto.QAlumnoAsignaturaDTO;
import es.uji.apps.se.dto.QEliteDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AlumnoAsignaturaDAO extends BaseDAODatabaseImpl {
    public List<AlumnoAsignaturaDTO> getAsignaturasByCursoAndPersonaId(Long curso, Long connectedUserId) {

        QAlumnoAsignaturaDTO alumnoAsignatura = QAlumnoAsignaturaDTO.alumnoAsignaturaDTO;
        QEliteDTO qElite = QEliteDTO.eliteDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(alumnoAsignatura).join(alumnoAsignatura.eliteDTO, qElite)
                .where(alumnoAsignatura.cursoAcademico.eq(curso).and(qElite.personaUjiDTO.id.eq(connectedUserId)));

        return query.list(alumnoAsignatura);
    }

    public List<PersonaUjiDTO> getProfesorAsignaturaByAsignatura(String asignatura, Long curso) {

        QAlumnoAsignaturaDTO alumnoAsignatura = QAlumnoAsignaturaDTO.alumnoAsignaturaDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(alumnoAsignatura).where(alumnoAsignatura.cursoAcademico.eq(curso).and(alumnoAsignatura.asignatura.eq(asignatura))).distinct();

        return query.list(alumnoAsignatura.personaUjiDTO);
    }
}
