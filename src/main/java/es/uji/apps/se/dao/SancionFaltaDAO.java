package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.SancionFaltaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.QSancionFaltaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SancionFaltaDAO extends BaseDAODatabaseImpl {

    private QSancionFaltaDTO qSancionFalta = QSancionFaltaDTO.sancionFaltaDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public SancionFaltaDAO() {
        relations.put("id", qSancionFalta.id);
        relations.put("orden", qSancionFalta.orden);
        relations.put("asunto", qSancionFalta.asunto);
    }


    public List<SancionFaltaDTO> getFaltas(Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSancionFalta);

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qSancionFalta);
    }

    public SancionFaltaDTO getSancionFaltaById(Long sancionFaltaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSancionFalta).where(qSancionFalta.id.eq(sancionFaltaId));
        return query.uniqueResult(qSancionFalta);
    }
}
