package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QPerPersonasDTO;
import es.uji.apps.se.dto.QUsoDeportesPersonaDTO;
import es.uji.apps.se.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UsoDeportesPersonaDAO extends BaseDAODatabaseImpl {
    public List<UsoDeportesPersona> getUsosDeportesPersona(Date fechaDesde, Date fechaHasta) {
        QUsoDeportesPersonaDTO qUsoDeportesPersonaDTO = QUsoDeportesPersonaDTO.usoDeportesPersonaDTO;
        QUsoDeportesPersonaDTO qUsoDeportesPersonaDTOTotales = QUsoDeportesPersonaDTO.usoDeportesPersonaDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qUsoDeportesPersonaDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qUsoDeportesPersonaDTO.fechaBaja.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qUsoDeportesPersonaDTO.fechaAlta.loe(fechaHasta));
        }

        JPAQuery queryTotales = new JPAQuery(entityManager);
        queryTotales.from(qUsoDeportesPersonaDTOTotales);
        if (ParamUtils.isNotNull(fechaDesde)) {
            queryTotales.where(qUsoDeportesPersonaDTOTotales.fechaBaja.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            queryTotales.where(qUsoDeportesPersonaDTOTotales.fechaAlta.loe(fechaHasta));
        }

        List<UsoDeportesPersona> usos = query.groupBy(qUsoDeportesPersonaDTO.uso, qUsoDeportesPersonaDTO.sexo, qUsoDeportesPersonaDTO.vinculoNombre)
                .list(new QUsoDeportesPersona(qUsoDeportesPersonaDTO.uso, qUsoDeportesPersonaDTO.vinculoNombre, qUsoDeportesPersonaDTO.sexo,
                        qUsoDeportesPersonaDTO.persona.countDistinct(), qUsoDeportesPersonaDTO.id.countDistinct()));

        usos.addAll(queryTotales.groupBy(qUsoDeportesPersonaDTO.vinculoNombre).list(new QUsoDeportesPersona(qUsoDeportesPersonaDTOTotales.vinculoNombre,
                qUsoDeportesPersonaDTOTotales.persona.countDistinct(), qUsoDeportesPersonaDTOTotales.id.countDistinct())));

        return usos;

    }

    public List<UsoDeportesPersonaDetalle> getInscripcionesyPersonasDetalle(String uso, String vinculoNombre, String sexo, Date fechaDesde, Date fechaHasta, Paginacion paginacion) {
        QUsoDeportesPersonaDTO qUsoDeportesPersonaDTO = QUsoDeportesPersonaDTO.usoDeportesPersonaDTO;
        QPerPersonasDTO qPersonaDTO = QPerPersonasDTO.perPersonasDTO;

        JPAQuery query = new JPAQuery(entityManager)
                .from(qUsoDeportesPersonaDTO)
                .leftJoin(qPersonaDTO).on(qPersonaDTO.id.eq(qUsoDeportesPersonaDTO.persona))
                .where(qUsoDeportesPersonaDTO.fechaBaja.goe(fechaDesde)
                        .and(qUsoDeportesPersonaDTO.fechaAlta.loe(fechaHasta))
                        .and(qUsoDeportesPersonaDTO.vinculoNombre.eq(vinculoNombre)));

        if (ParamUtils.isNotNull(sexo) && !sexo.isEmpty()) {
            if (sexo.equals("Home"))
                query.where(qUsoDeportesPersonaDTO.sexo.eq(1L));
            if (sexo.equals("Dona"))
                query.where(qUsoDeportesPersonaDTO.sexo.eq(2L));
            if (sexo.equals("Sense sexe"))
                query.where(qUsoDeportesPersonaDTO.sexo.isNull());
        }

        if (ParamUtils.isNotNull(uso) && !uso.isEmpty()) {
            query.where(qUsoDeportesPersonaDTO.uso.eq(uso));
            query.groupBy(qUsoDeportesPersonaDTO.uso);
        }

        query.groupBy(qUsoDeportesPersonaDTO.persona, qUsoDeportesPersonaDTO.sexo, qUsoDeportesPersonaDTO.vinculoNombre,
                qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qPersonaDTO.identificacion);

        JPAQuery count = query;
        int totalCount = count.list(qUsoDeportesPersonaDTO.persona, qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qPersonaDTO.identificacion).size();

        if (paginacion != null) {
//            paginacion.setTotalCount((long) totalCount);
//            if (paginacion.getLimit() > 0)
//                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            query.orderBy(qPersonaDTO.nombre.asc());
            paginacion.setTotalCount((long) totalCount);
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.list(qUsoDeportesPersonaDTO.persona, qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qPersonaDTO.identificacion)
                .stream().map(tuple -> {
                    UsoDeportesPersonaDetalle personaDetalle = new UsoDeportesPersonaDetalle();
                    personaDetalle.setPerId(tuple.get(qUsoDeportesPersonaDTO.persona));
                    String nombreCompleto = tuple.get(qPersonaDTO.apellido1) + " " + tuple.get(qPersonaDTO.apellido2) + ", " + tuple.get(qPersonaDTO.nombre);

                    if (nombreCompleto.contains("null null, "))
                        nombreCompleto = nombreCompleto.replace("null null, ", "");
                    else if (nombreCompleto.contains("null")) nombreCompleto = nombreCompleto.replace("null", "");

                    personaDetalle.setNombreCompleto(nombreCompleto);
                    personaDetalle.setIdentificacion(tuple.get(qPersonaDTO.identificacion));
                    return personaDetalle;
                }).collect(Collectors.toList());
    }

    public List<UsoDeportesPersona> getInscripcionesyPersonasSinUso(Date fechaDesde, Date fechaHasta) {
        QUsoDeportesPersonaDTO qUsoDeportesPersonaDTO = QUsoDeportesPersonaDTO.usoDeportesPersonaDTO;
        QUsoDeportesPersonaDTO qUsoDeportesPersonaDTOTotales = QUsoDeportesPersonaDTO.usoDeportesPersonaDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qUsoDeportesPersonaDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qUsoDeportesPersonaDTO.fechaBaja.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qUsoDeportesPersonaDTO.fechaAlta.loe(fechaHasta));
        }

        JPAQuery queryTotales = new JPAQuery(entityManager);
        queryTotales.from(qUsoDeportesPersonaDTOTotales);
        if (ParamUtils.isNotNull(fechaDesde)) {
            queryTotales.where(qUsoDeportesPersonaDTOTotales.fechaBaja.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            queryTotales.where(qUsoDeportesPersonaDTOTotales.fechaAlta.loe(fechaHasta));
        }

        List<UsoDeportesPersona> usos = query.groupBy(qUsoDeportesPersonaDTO.sexo, qUsoDeportesPersonaDTO.vinculoNombre)
                .list(new QUsoDeportesPersona(qUsoDeportesPersonaDTO.vinculoNombre, qUsoDeportesPersonaDTO.sexo,
                        qUsoDeportesPersonaDTO.persona.countDistinct(), qUsoDeportesPersonaDTO.id.countDistinct()));

        usos.addAll(queryTotales.groupBy(qUsoDeportesPersonaDTO.vinculoNombre).list(new QUsoDeportesPersona(qUsoDeportesPersonaDTOTotales.vinculoNombre,
                qUsoDeportesPersonaDTOTotales.persona.countDistinct(), qUsoDeportesPersonaDTOTotales.id.countDistinct())));

        return usos;
    }
}
