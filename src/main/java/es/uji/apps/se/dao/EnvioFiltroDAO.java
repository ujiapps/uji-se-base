package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.EnvioFiltroDTO;
import es.uji.apps.se.dto.QEnvioFiltroDTO;
import es.uji.apps.se.model.domains.TipoFiltroEnvio;
import es.uji.apps.se.model.domains.TipoFiltroTipologiaEnvio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class EnvioFiltroDAO extends BaseDAODatabaseImpl {

    private QEnvioFiltroDTO qEnvioFiltroDTO = QEnvioFiltroDTO.envioFiltroDTO;

    public List<EnvioFiltroDTO> getEnvioFiltrosByEnvioId(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvioFiltroDTO).where(qEnvioFiltroDTO.envio.id.eq(envioId)
                .and(qEnvioFiltroDTO.tipoFiltro.in(TipoFiltroEnvio.getAll())));

        return query.list(qEnvioFiltroDTO);
    }

    public List<EnvioFiltroDTO> getEnvioFiltrosTipologiaByEnvioId(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvioFiltroDTO).where(qEnvioFiltroDTO.envio.id.eq(envioId)
                .and(qEnvioFiltroDTO.tipoFiltro.in(TipoFiltroTipologiaEnvio.getAll())));

        return query.list(qEnvioFiltroDTO);
    }

    @Transactional
    public void deleteEnvioFiltroByEnvioId(Long envioId) {
        JPADeleteClause query = new JPADeleteClause(entityManager, qEnvioFiltroDTO);

        query.where(qEnvioFiltroDTO.envio.id.eq(envioId)
                .and(qEnvioFiltroDTO.tipoFiltro.in(TipoFiltroEnvio.getAll()))).execute();
    }


    @Transactional
    public void deleteEnvioFiltroTipologiaByEnvioId(Long envioId) {
        JPADeleteClause query = new JPADeleteClause(entityManager, qEnvioFiltroDTO);

        query.where(qEnvioFiltroDTO.envio.id.eq(envioId)
                .and(qEnvioFiltroDTO.tipoFiltro.in(TipoFiltroTipologiaEnvio.getAll()))).execute();
    }

    @Transactional
    public void deleteEnvioFiltroTodos(Long envioId) {
        JPADeleteClause query = new JPADeleteClause(entityManager, qEnvioFiltroDTO);

        query.where(qEnvioFiltroDTO.envio.id.eq(envioId)
                .and(qEnvioFiltroDTO.tipoFiltro.in(11L))).execute();
    }

    @Transactional
    public void deleteEnvioFiltroAlquilerByEnvioId(Long envioId) {

        JPADeleteClause query = new JPADeleteClause(entityManager, qEnvioFiltroDTO);
        query.where(qEnvioFiltroDTO.envio.id.eq(envioId)
                .and(qEnvioFiltroDTO.tipoFiltro.in(TipoFiltroEnvio.ALQUILER.getId()))).execute();
    }
}
