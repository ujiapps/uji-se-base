package es.uji.apps.se.dao;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.QEquipacionDTO;
import es.uji.apps.se.model.Equipacion;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("equipacionDAO")
public class EquipacionDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    QEquipacionDTO qEquipacionDTO = QEquipacionDTO.equipacionDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public EquipacionDAO() {
        relations.put("tipoNombre", qEquipacionDTO.tipoDTO.nombre);
        relations.put("modeloNombre", qEquipacionDTO.modeloDTO.nombre);
        relations.put("grupoNombre", qEquipacionDTO.gruposDTO.nombre);
        relations.put("generoNombre", qEquipacionDTO.generoDTO.nombre);
        relations.put("fechaInicio", qEquipacionDTO.fechaInicio);
        relations.put("fechaFin", qEquipacionDTO.fechaFin);
    }

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qEquipacionDTO)
                            .orderBy(qEquipacionDTO.id.asc());

        if(!s.isEmpty()) {
            try {
                query.where(qEquipacionDTO.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qEquipacionDTO.tipoDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s))
                        .or(Utils.limpia(qEquipacionDTO.modeloDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s))
                        .or(Utils.limpia(qEquipacionDTO.generoDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s))))
                );
            }
        }

        return query
                .list(qEquipacionDTO.id,
                        qEquipacionDTO.tipoDTO.nombre,
                        qEquipacionDTO.modeloDTO.nombre,
                        qEquipacionDTO.generoDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qEquipacionDTO.id).toString());
                    item.setNombre(tuple.get(qEquipacionDTO.tipoDTO.nombre) + " - " + tuple.get(qEquipacionDTO.modeloDTO.nombre) + " - " + tuple.get(qEquipacionDTO.generoDTO.nombre));
                    item.addExtraParam("tipoNombre", tuple.get(qEquipacionDTO.tipoDTO.nombre));
                    item.addExtraParam("modeloNombre", tuple.get(qEquipacionDTO.modeloDTO.nombre));
                    item.addExtraParam("generoNombre", tuple.get(qEquipacionDTO.generoDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }

    public List<Equipacion> getEquipaciones(Paginacion paginacion, List<Map<String, String>> filtros) {

        JPAQuery query = new JPAQuery(entityManager).from(qEquipacionDTO);

        if (filtros != null) {
            filtros.forEach(item -> {
                String property = item.get("property");
                String value = item.get("value");

                if (property.equals("tipoNombre")) {
                    query.where(qEquipacionDTO.tipoDTO.id.eq(Long.parseLong(value)));
                }

                if (property.equals("grupoNombre")) {
                    query.where(qEquipacionDTO.gruposDTO.id.eq(Long.parseLong(value)));
                }

                if (property.equals("modeloNombre")) {
                    query.where(qEquipacionDTO.modeloDTO.id.eq(Long.parseLong(value)));
                }

                if (property.equals("generoNombre")) {
                    query.where(qEquipacionDTO.generoDTO.id.eq(Long.parseLong(value)));
                }

                if (property.equals("activa")) {
                    query.where(qEquipacionDTO.fechaInicio.loe(new Date()).and((qEquipacionDTO.fechaFin.isNull().or(qEquipacionDTO.fechaFin.gt(new Date())))));
                }
            });
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }
            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                query.orderBy(qEquipacionDTO.tipoDTO.nombre.asc());
            }
        }

        return query.list(qEquipacionDTO).stream().map(tuple -> {
            Equipacion equipacion = new Equipacion();
            equipacion.setId(tuple.getId());
            equipacion.setModeloId(tuple.getModeloDTO().getId());
            equipacion.setModeloNombre(tuple.getModeloDTO().getNombre());
            equipacion.setGrupoId(tuple.getGruposDTO().getId());
            equipacion.setGrupoNombre(tuple.getGruposDTO().getNombre());
            equipacion.setGeneroId(tuple.getGeneroDTO().getId());
            equipacion.setGeneroNombre(tuple.getGeneroDTO().getNombre());
            equipacion.setTipoId(tuple.getTipoDTO().getId());
            equipacion.setTipoNombre(tuple.getTipoDTO().getNombre());
            equipacion.setStockMin(tuple.getStockMin());
            equipacion.setFechaInicio(tuple.getFechaInicio());
            equipacion.setFechaFin(tuple.getFechaFin());
            return equipacion;
        }).collect(Collectors.toList());
    }
}