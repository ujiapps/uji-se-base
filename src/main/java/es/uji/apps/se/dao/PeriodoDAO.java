package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.xml.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository("periodosDAO")
public class PeriodoDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private QPeriodoDTO qPeriodo = QPeriodoDTO.periodoDTO;

    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager).from(qPeriodo);

        if(!s.equals("")) {
            Long id;
            try {
                id = Long.parseLong(s);
                query.where(qPeriodo.id.eq(id));
            } catch (NumberFormatException e){
                query.where(Utils.limpia(qPeriodo.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }
        return query
        .list(qPeriodo)
        .stream()
        .map(periodoDTO -> {
            LookupItem item = new LookupItem();
            item.setId(periodoDTO.getId().toString());
            item.setNombre(periodoDTO.getNombre());
            return item;
        }).collect(Collectors.toList());
    }

    public PeriodoDTO getPeriodoById(Long id) {
        if (id != null)
            return new JPAQuery(entityManager)
                    .from(qPeriodo)
                    .where(qPeriodo.id.eq(id))
                    .singleResult(qPeriodo);
        return null;
    }

    public List<PeriodoDTO> getPeriodosByCurso(Long curso) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPeriodo).where(qPeriodo.cursoAcademicoDTO.id.eq(curso)).orderBy(qPeriodo.nombre.asc());
        return query.list(qPeriodo);
    }

    public List<PeriodoDTO> getPeriodosConInscripcionUsuario(Long persona) {
        JPAQuery query = new JPAQuery(entityManager);

        QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;
        QInscripcionDTO qInscripcion = QInscripcionDTO.inscripcionDTO;

        query.from(qPeriodo).join(qPeriodo.ofertasPeriodo, qOferta).join(qOferta.inscripciones, qInscripcion)
                .where(qInscripcion.personaUjiDTO.id.eq(persona))
                .orderBy(qPeriodo.nombre.desc());

        return query.list(qPeriodo);
    }

    public List<PeriodoDTO> getPeriodosActivoRenovaciones()
    {
        Date hoy = new Date();
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPeriodo)
                .where(qPeriodo.fechaInicioRenovacion.before(hoy)
                .and(qPeriodo.fechaFinRenovacion.after(hoy)));

        return query.list(qPeriodo);
    }
}
