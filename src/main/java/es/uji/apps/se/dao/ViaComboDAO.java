package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QViaComboDTO;
import es.uji.apps.se.model.ProvinciaCombo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository("viaComboDAO")
public class ViaComboDAO extends BaseDAODatabaseImpl {

    private QViaComboDTO qViaComboDTO = QViaComboDTO.viaComboDTO.viaComboDTO;

    public List<ProvinciaCombo> getViaCombo() {

        return new JPAQuery(entityManager).from(qViaComboDTO).list(qViaComboDTO.id,
                        qViaComboDTO.nombre)
                .stream()
                .map(tuple -> {
                    ProvinciaCombo item = new ProvinciaCombo();
                    item.setId(tuple.get(qViaComboDTO.id));
                    item.setNombre(tuple.get(qViaComboDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }
}