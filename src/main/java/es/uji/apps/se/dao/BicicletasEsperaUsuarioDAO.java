package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.BicicletaListaEspera;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

@Repository
public class BicicletasEsperaUsuarioDAO extends BaseDAODatabaseImpl {
    private QBicicletasDTO qBicicletasDTO = QBicicletasDTO.bicicletasDTO;
    private QBicicletasReservasDTO qBicicletasReservasDTO = QBicicletasReservasDTO.bicicletasReservasDTO;
    private QBicicletasListaEsperaDTO qBicicletasListaEsperaDTO = QBicicletasListaEsperaDTO.bicicletasListaEsperaDTO;

    public BicicletaListaEspera getBicicletasListaEspera(Long perId) throws ParseException {
        String nativeQuery = "select e.id, e.fecha, e.observaciones, e.tipo_uso_id, (" +
                                "select num from (" +
                                    "select persona_id, rownum num from (" +
                                        "select b.id, b.persona_id from se_bic_lista_espera b order by fecha asc)" +
                                    ") where persona_id = e.persona_id" +
                                ") posicionLista " +
                             "from se_bic_lista_espera e " +
                             "where e.persona_id = " + perId;
        Query query = entityManager.createNativeQuery(nativeQuery);
        Object[] a = (Object[]) query.getSingleResult();
        return new BicicletaListaEspera(((BigDecimal) a[0]).longValue(),
                new Date(((Timestamp) (a[1])).getTime()),
                a[2] == null ? null : a[2].toString(),
                ((BigDecimal) a[3]).longValue(),
                ((BigDecimal) a[4]).longValue()
        );
    }

    @Transactional
    public void asignarBicicletasEspera() {
        new JPAQuery(entityManager)
                .from(qBicicletasDTO)
                .where(qBicicletasDTO.id.eq(9560263L)
                        .and(qBicicletasDTO.id.notIn(new JPAQuery(entityManager)
                                .from(qBicicletasReservasDTO)
                                .where(qBicicletasReservasDTO.bicicletaId.eq(qBicicletasDTO.id)
                                        .and(qBicicletasReservasDTO.fechaFin.isNull()))
                                .list(qBicicletasReservasDTO.bicicletaId))))
                .list(qBicicletasDTO.id).forEach(bicicleta -> {
                    Long esperaId = new JPAQuery(entityManager)
                            .from(qBicicletasListaEsperaDTO)
                            .where(qBicicletasListaEsperaDTO.fecha.eq(
                                    new JPAQuery(entityManager)
                                            .from(qBicicletasListaEsperaDTO)
                                            .singleResult(qBicicletasListaEsperaDTO.fecha.min())
                            )).singleResult(qBicicletasListaEsperaDTO.esperaId);
                    executeAsignarBicicletasEspera(esperaId, bicicleta);
                });
    }

    @Transactional
    public void asignarBicicletasEsperaIfNotNull(Long reservaEspera) {
        Long bicicleta = new JPAQuery(entityManager)
                .from(qBicicletasDTO)
                .where(qBicicletasDTO.estadoId.eq(9560263L)
                        .and(qBicicletasDTO.id.notIn(
                                new JPAQuery(entityManager)
                                        .from(qBicicletasReservasDTO)
                                        .where(qBicicletasReservasDTO.bicicletaId.eq(qBicicletasDTO.id)
                                                .and(qBicicletasReservasDTO.fechaFin.isNull()))
                                        .list(qBicicletasReservasDTO.id)))
                ).singleResult(qBicicletasDTO.id.max());
        executeAsignarBicicletasEspera(reservaEspera, bicicleta);
    }

    @Transactional
    private void executeAsignarBicicletasEspera(Long espera, Long bicicleta) {
        Tuple tuple = new JPAQuery(entityManager)
                .from(qBicicletasListaEsperaDTO)
                .where(qBicicletasListaEsperaDTO.esperaId.eq(espera))
                .singleResult(qBicicletasListaEsperaDTO.perId, qBicicletasListaEsperaDTO.tipoUso);
        Long persona = tuple.get(qBicicletasListaEsperaDTO.perId);
        Long tipoUso = tuple.get(qBicicletasListaEsperaDTO.tipoUso);

        BicicletasReservasDTO reservasDTO = new BicicletasReservasDTO();
        reservasDTO.setFecha(new Date());
        reservasDTO.setBicicletaId(bicicleta);
        reservasDTO.setPerId(persona);
        reservasDTO.setTipoUsoId(tipoUso);
        this.insert(reservasDTO);

        new JPADeleteClause(entityManager, qBicicletasListaEsperaDTO)
                .where(qBicicletasListaEsperaDTO.esperaId.eq(espera))
                .execute();
    }
}
