package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.views.MonitorCSV;
import es.uji.apps.se.dto.views.QMonitorCSV;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class MonitorCSVDAO extends BaseDAODatabaseImpl {
    public List<MonitorCSV> getCsvBySearch(List<Long> clases, List<Long> instalaciones, Date fechaInicio, Date fechaFin) {
        QMonitorCSV qMonitorCSV = QMonitorCSV.monitorCSV;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMonitorCSV);

        if (ParamUtils.isNotNull(clases)){
            query.where(qMonitorCSV.claseId.in(clases));
        }

        if (ParamUtils.isNotNull(instalaciones))
            query.where(qMonitorCSV.instalacionId.in(instalaciones));

        if (ParamUtils.isNotNull(fechaInicio))
            query.where(qMonitorCSV.dia.goe(fechaInicio));

        if (ParamUtils.isNotNull(fechaFin))
            query.where(qMonitorCSV.dia.loe(fechaFin));

        return query.list(qMonitorCSV);
    }
}
