package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.views.ClaseCSV;
import es.uji.apps.se.dto.views.QClaseCSV;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class ClaseCSVDAO extends BaseDAODatabaseImpl {
    public List<ClaseCSV> getCsvBySearch(List<Long> clases, List<Long> instalaciones, Date fechaInicio, Date fechaFin) {
        QClaseCSV qClaseCSV = QClaseCSV.claseCSV;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qClaseCSV);

        if (ParamUtils.isNotNull(clases)){
            query.where(qClaseCSV.claseId.in(clases));
        }

        if (ParamUtils.isNotNull(instalaciones))
            query.where(qClaseCSV.instalacionId.in(instalaciones));

        if (ParamUtils.isNotNull(fechaInicio))
            query.where(qClaseCSV.dia.goe(fechaInicio));

        if (ParamUtils.isNotNull(fechaFin))
            query.where(qClaseCSV.dia.loe(fechaFin));

        return query.list(qClaseCSV);
    }
}
