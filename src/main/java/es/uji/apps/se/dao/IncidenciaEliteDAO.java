package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.IncidenciaEliteDTO;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.dto.QIncidenciaEliteDTO;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IncidenciaEliteDAO extends BaseDAODatabaseImpl {
    public List<IncidenciaEliteDTO> getIncidenciasElite(CursoAcademico cursoAcademico) {
        QIncidenciaEliteDTO qIncidenciaElite = QIncidenciaEliteDTO.incidenciaEliteDTO;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIncidenciaElite).where(qIncidenciaElite.cursoAcademicoDTO.id.eq(cursoAcademico.getId()));

        return query.list(qIncidenciaElite);
    }
}
