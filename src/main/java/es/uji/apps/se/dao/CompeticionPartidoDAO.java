package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.QCompeticionPartidoDTO;
import es.uji.apps.se.model.CompeticionPartido;
import es.uji.apps.se.model.QCompeticionPartido;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CompeticionPartidoDAO extends BaseDAODatabaseImpl {
    public CompeticionPartido getCompeticionPartidoById(Long competicionId) {
        QCompeticionPartidoDTO qCompeticionPartidoDTO = QCompeticionPartidoDTO.competicionPartidoDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCompeticionPartidoDTO).where(qCompeticionPartidoDTO.id.eq(competicionId));

        if (!query.list(qCompeticionPartidoDTO).isEmpty())
            return query.list(new QCompeticionPartido(qCompeticionPartidoDTO.id, qCompeticionPartidoDTO.equipoCasa.id, qCompeticionPartidoDTO.equipoFuera.id)).get(0);
        return null;
    }

    @Transactional
    public void updateObservacionesPartido(Long partId, String observaciones){

        QCompeticionPartidoDTO qCompeticionPartidoDTO = QCompeticionPartidoDTO.competicionPartidoDTO;

        new JPAUpdateClause(entityManager, qCompeticionPartidoDTO)
                .set(qCompeticionPartidoDTO.comentarios, observaciones)
                .where(qCompeticionPartidoDTO.id.eq(partId))
                .execute();

    }

    public CompeticionPartido getCompeticionPartidoByIdEntero(Long competicionId) {

        QCompeticionPartidoDTO qCompeticionPartidoDTO = QCompeticionPartidoDTO.competicionPartidoDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCompeticionPartidoDTO)
                .where(qCompeticionPartidoDTO.id.eq(competicionId))
                .singleResult(new QCompeticionPartido(qCompeticionPartidoDTO.id, qCompeticionPartidoDTO.equipoCasa.id, qCompeticionPartidoDTO.equipoFuera.id
                , qCompeticionPartidoDTO.resultadoCasa, qCompeticionPartidoDTO.resultadoFuera, qCompeticionPartidoDTO.comentarios));
    }
}
