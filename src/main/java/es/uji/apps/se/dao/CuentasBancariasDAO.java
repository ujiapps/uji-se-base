package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QCuentasBancariasDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CuentasBancariasDAO extends BaseDAODatabaseImpl {
    private QCuentasBancariasDTO qCuentasBancariasDTO = QCuentasBancariasDTO.cuentasBancariasDTO;

    public List<String> getCuentasBancariasByPersonaId(Long personaId) {
        return new JPAQuery(entityManager)
                .from(qCuentasBancariasDTO)
                .where(qCuentasBancariasDTO.perId.eq(personaId))
                .list(qCuentasBancariasDTO).stream().map(tuple -> {
                    return tuple.getIban() != null ? tuple.getIban() :
                            tuple.getEntidad() +
                            tuple.getSucursal() +
                            tuple.getDigitos() +
                            tuple.getCuenta();
                }).distinct().collect(Collectors.toList());
    }
}
