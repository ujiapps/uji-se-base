package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.PersonaFicheroDTO;
import es.uji.apps.se.dto.QPersonaFicheroDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class PersonaFicheroDAO extends BaseDAODatabaseImpl {

    QPersonaFicheroDTO qPersonaFichero = QPersonaFicheroDTO.personaFicheroDTO;

    public List<PersonaFicheroDTO> getDocumentosElite(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qPersonaFichero).where(qPersonaFichero.personaUjiDTO.id.eq(personaId));
        return query.list(qPersonaFichero);

    }

    public List<PersonaFicheroDTO> getDocumentosPersona(Long personaId) {
        return new JPAQuery(entityManager)
                .from(qPersonaFichero)
                .where(qPersonaFichero.personaUjiDTO.id.eq(personaId))
                .list(qPersonaFichero);
    }

    @Transactional
    public void deleteDocumentoPersona(Long docuId) {
        new JPADeleteClause(entityManager, qPersonaFichero)
                .where(qPersonaFichero.id.eq(docuId))
                .execute();
    }
}
