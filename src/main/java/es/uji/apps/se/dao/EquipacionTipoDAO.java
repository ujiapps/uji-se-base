package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.EquipacionTipoDTO;
import es.uji.apps.se.dto.QEquipacionTipoDTO;
import es.uji.apps.se.model.EquipacionTipo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository("tipoEquipacionDAO")
public class EquipacionTipoDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private final QEquipacionTipoDTO qEquipacionTipoDTO = QEquipacionTipoDTO.equipacionTipoDTO;

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qEquipacionTipoDTO)
                            .orderBy(qEquipacionTipoDTO.id.asc());

        if(!s.equals("")) {
            try {
                query.where(qEquipacionTipoDTO.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qEquipacionTipoDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query
                .list(qEquipacionTipoDTO.id,
                      qEquipacionTipoDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qEquipacionTipoDTO.id).toString());
                    item.setNombre(tuple.get(qEquipacionTipoDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }

    public List<EquipacionTipo> getTiposEquipacion() {
        return new JPAQuery(entityManager)
                    .from(qEquipacionTipoDTO)
                    .list(qEquipacionTipoDTO.id,
                          qEquipacionTipoDTO.nombre)
                    .stream()
                    .map(tuple -> new EquipacionTipo(tuple.get(qEquipacionTipoDTO.id), tuple.get(qEquipacionTipoDTO.nombre)))
                    .collect(Collectors.toList());
    }

    @Transactional
    public void deleteTipoEquipacion(Long id) {
        new JPADeleteClause(entityManager, qEquipacionTipoDTO)
            .where(qEquipacionTipoDTO.id.eq(id))
            .execute();
    }

    public void updateTipoEquipacion(EquipacionTipo tipo) {
        this.update(new EquipacionTipoDTO(tipo));
    }
}
