package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.JaulasPermisosDTO;
import es.uji.apps.se.dto.QJaulasPermisosDTO;
import es.uji.apps.se.exceptions.VinculoNoContempladoException;
import es.uji.apps.se.exceptions.jaulasPermisos.JaulasPermisosDeleteException;
import es.uji.apps.se.exceptions.jaulasPermisos.JaulasPermisosInsertException;
import es.uji.apps.se.model.domains.Vinculo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Repository("jaulasPermisosDAO")
public class JaulasPermisosDAO extends BaseDAODatabaseImpl {

    QJaulasPermisosDTO qJaulasPermisosDTO = QJaulasPermisosDTO.jaulasPermisosDTO;

    public List<JaulasPermisosDTO> getJaulasPermisos() {
        return new JPAQuery(entityManager)
                .from(qJaulasPermisosDTO).list(qJaulasPermisosDTO).stream().map(tuple -> {
                    JaulasPermisosDTO jaulasPermisosDTO = new JaulasPermisosDTO();
                    jaulasPermisosDTO.setId(tuple.getId());
                    jaulasPermisosDTO.setVinculoId(tuple.getVinculoId());
                    try {
                        jaulasPermisosDTO.setVinculoNombre(Vinculo.getVinculoById(tuple.getVinculoId()).getDescripcion());
                    } catch (VinculoNoContempladoException e) {
                        jaulasPermisosDTO.setVinculoNombre("Vincle no contemplat");
                    }
                    return jaulasPermisosDTO;
                }).collect(Collectors.toList());
    }

    public JaulasPermisosDTO addJaulasPermisos(JaulasPermisosDTO jaulasPermisosDTO) throws JaulasPermisosInsertException {
        if (tieneAccesoJaulas(jaulasPermisosDTO.getVinculoId()) != null)
            throw new JaulasPermisosInsertException("Aquest vincle ja existeix");
        return this.insert(jaulasPermisosDTO);
    }

    @Transactional
    public void deleteJaulasPermisos(Long id) throws JaulasPermisosDeleteException {
        try {
            new JPADeleteClause(entityManager, qJaulasPermisosDTO).where(qJaulasPermisosDTO.id.eq(id)).execute();
        } catch (Exception e) {
            throw new JaulasPermisosDeleteException();
        }
    }

    public JaulasPermisosDTO tieneAccesoJaulas(Long vinculoId) {
        return new JPAQuery(entityManager)
                .from(qJaulasPermisosDTO)
                .where(qJaulasPermisosDTO.vinculoId.eq(vinculoId))
                .singleResult(qJaulasPermisosDTO);
    }
}