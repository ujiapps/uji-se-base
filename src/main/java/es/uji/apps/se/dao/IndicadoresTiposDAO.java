package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.IndicadoresTiposDTO;
import es.uji.apps.se.dto.QIndicadoresTiposDTO;
import es.uji.apps.se.model.IndicadoresTipos;
import es.uji.apps.se.model.QIndicadoresTipos;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("indicadorTipoDAO")
public class IndicadoresTiposDAO extends BaseDAODatabaseImpl {

    QIndicadoresTiposDTO qIndicadoresTiposDTO = QIndicadoresTiposDTO.indicadoresTiposDTO;

    public IndicadoresTiposDTO getIndicadorTipoById(Long id) {
        if (id != null)
            return new JPAQuery(entityManager)
                    .from(qIndicadoresTiposDTO)
                    .where(qIndicadoresTiposDTO.id.eq(id))
                    .singleResult(qIndicadoresTiposDTO);
        return null;
    }

    public List<IndicadoresTipos> getIndicadoresTipo() {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qIndicadoresTiposDTO);
        return query.list(new QIndicadoresTipos(qIndicadoresTiposDTO.id, qIndicadoresTiposDTO.nombre));
    }
}