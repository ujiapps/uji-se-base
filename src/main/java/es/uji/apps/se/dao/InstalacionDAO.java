package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.InstalacionDTO;
import es.uji.apps.se.dto.QCompeticionPartidoDTO;
import es.uji.apps.se.dto.QInstalacionDTO;
import es.uji.apps.se.dto.QTipoDTO;
import es.uji.apps.se.model.Tipo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class InstalacionDAO extends BaseDAODatabaseImpl {
    public List<InstalacionDTO> getInstalaciones() {

        QInstalacionDTO qInstalacion = QInstalacionDTO.instalacionDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qInstalacion).orderBy(qInstalacion.nombre.asc()).list(qInstalacion);
    }

    public List<Tipo> getUsuarioInstalaciones() {
        QTipoDTO qTipoDTO = QTipoDTO.tipoDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTipoDTO)
                .where(qTipoDTO.uso.containsIgnoreCase("TORNO"))
                .orderBy(qTipoDTO.orden.asc());

        List<Tipo> ubicaciones = query.list(
                qTipoDTO.id,
                qTipoDTO.nombre,
                qTipoDTO.orden
        ).stream().map(tuple -> {
            Tipo tipo = new Tipo();
            tipo.setId(tuple.get(qTipoDTO.id));
            tipo.setNombre(tuple.get(qTipoDTO.nombre));
            tipo.setOrden(tuple.get(qTipoDTO.orden));
            return tipo;
        }).collect(Collectors.toList());

        Tipo todasUbis = new Tipo();
        todasUbis.setId(0L);
        todasUbis.setNombre("Totes les instal·lacions");
        todasUbis.setOrden(-1L);
        ubicaciones.add(0, todasUbis);
        return ubicaciones;
    }

    public String getInstalacionPartido(Long partidoId){

        QCompeticionPartidoDTO qCompeticionPartidoDTO = QCompeticionPartidoDTO.competicionPartidoDTO;
        QInstalacionDTO qInstalacionDTO = QInstalacionDTO.instalacionDTO;

        Long instalacionId = new JPAQuery(entityManager)
                .from(qCompeticionPartidoDTO)
                .where(qCompeticionPartidoDTO.id.eq(partidoId))
                .singleResult(qCompeticionPartidoDTO.instalacionId);

        JPAQuery query = new JPAQuery(entityManager)
                .from(qInstalacionDTO)
                .where(qInstalacionDTO.id.eq(instalacionId));

        String nombre = query.singleResult(qInstalacionDTO.nombre);
        String codigo = query.singleResult(qInstalacionDTO.codigo);

        return nombre +" (" + codigo +")";

    }
}
