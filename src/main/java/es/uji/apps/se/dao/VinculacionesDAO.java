package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Vinculaciones;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class VinculacionesDAO extends BaseDAODatabaseImpl {
    private QVinculacionesActivasVWDTO qVinculacionesActivasVWDTO = QVinculacionesActivasVWDTO.vinculacionesActivasVWDTO;
    private QPersonasSubVinculosDTO qPersonasSubVinculosDTO = QPersonasSubVinculosDTO.personasSubVinculosDTO;
    private QVinculosDTO qVinculosDTO = QVinculosDTO.vinculosDTO;
    private QSubVinculosDTO qSubVinculosDTO = QSubVinculosDTO.subVinculosDTO;
    private QPersonaTitulacionDTO qPersonaTitulacionDTO = QPersonaTitulacionDTO.personaTitulacionDTO;
    private QTipoDTO qTipoDTO = QTipoDTO.tipoDTO;
    private QVinculosVinextDTO qVinculosVinextDTO = QVinculosVinextDTO.vinculosVinextDTO;
    private QSubvinculosPersonasDTO qSubvinculosPersonasDTO = QSubvinculosPersonasDTO.subvinculosPersonasDTO;


    public Vinculaciones getVinculacionesPersona(Long personaId, Long curso) {
        Vinculaciones vinculaciones = new Vinculaciones();

        List<String> list = new JPAQuery(entityManager)
                .from(qVinculacionesActivasVWDTO)
                .where(qVinculacionesActivasVWDTO.perId.eq(personaId))
                .list(qVinculacionesActivasVWDTO.vinculoNombre);
        vinculaciones.setVinculacionesActivas(list);

        List<String> listUji = new JPAQuery(entityManager)
                .from(qPersonasSubVinculosDTO, qVinculosDTO, qSubVinculosDTO)
                .where(qPersonasSubVinculosDTO.id.personaDTO.id.eq(personaId)
                        .and(qPersonasSubVinculosDTO.id.sviVINid.eq(qVinculosDTO.id))
                        .and(qSubVinculosDTO.id.eq(qPersonasSubVinculosDTO.id.sviId))
                        .and(qSubVinculosDTO.vinId.eq(qVinculosDTO.id)))
                .list(qVinculosDTO.nombre, qSubVinculosDTO.nombre)
                .stream().map(s -> s.get(qVinculosDTO.nombre) + " (" + s.get(qSubVinculosDTO.nombre) + ")")
                .collect(Collectors.toList());
        vinculaciones.setVinculacionesUji(listUji);

        vinculaciones.setTitulacionesActivas(new JPAQuery(entityManager)
                                            .from(qPersonaTitulacionDTO)
                                            .where(qPersonaTitulacionDTO.cursoAcademico.eq(curso)
                                            .and(qPersonaTitulacionDTO.persona.eq(personaId)))
                                            .list(qPersonaTitulacionDTO.titulacion, qPersonaTitulacionDTO.titulacionNombre, qPersonaTitulacionDTO.oficial)
                                            .stream().map(s -> s.get(qPersonaTitulacionDTO.titulacionNombre) + " (Titulació " + (s.get(qPersonaTitulacionDTO.oficial) == 0 ? "no " : "") + " oficial)")
                                            .collect(Collectors.toList()));

        return vinculaciones;
    }

    public long dameVinculoPersona(long perId, String parametro) {
        String valor = dameParametro(perId, parametro);

        //Padre
        JPAQuery queryPadre = new JPAQuery(entityManager).from(qTipoDTO)
                .where(qTipoDTO.grupoDTO.id.eq(7330L));
        if (valor == null)
            queryPadre.where(qTipoDTO.uso.isNull());
        else
            queryPadre.where(qTipoDTO.uso.eq(valor));
        long padre = -1L;
        if (queryPadre.singleResult(qTipoDTO.id) != null)
            padre = queryPadre.singleResult(qTipoDTO.id);

        //Orden
        JPAQuery queryOrden = new JPAQuery(entityManager).from(qTipoDTO, qVinculosVinextDTO, qSubvinculosPersonasDTO)
                .where(qVinculosVinextDTO.vinextId.eq(qSubvinculosPersonasDTO.subvinculoId)
                        .and(qVinculosVinextDTO.vinculoId.eq(qTipoDTO.id))
                        .and(qTipoDTO.tipoPadreDTO.id.eq(padre))
                        .and(qSubvinculosPersonasDTO.perId.eq(perId)));
        long orden = -1L;
        if (queryOrden.singleResult(qTipoDTO.orden.min()) != null)
            orden = queryOrden.singleResult(qTipoDTO.orden.min());

        //Vinculo
        QTipoDTO qTipoDTO1 = new QTipoDTO("qTipoDTO1");
        JPAQuery queryVinculo = new JPAQuery(entityManager).from(qTipoDTO, qTipoDTO1, qVinculosVinextDTO, qSubvinculosPersonasDTO)
                .where(qTipoDTO.id.eq(qTipoDTO1.tipoPadreDTO.id)
                        .and(qVinculosVinextDTO.vinextId.eq(qSubvinculosPersonasDTO.subvinculoId))
                        .and(qVinculosVinextDTO.vinculoId.eq(qTipoDTO1.id))
                        .and(qTipoDTO.id.eq(padre))
                        .and(qSubvinculosPersonasDTO.perId.eq(perId))
                        .and(qTipoDTO1.orden.eq(orden)))
                .groupBy(qTipoDTO.uso, qTipoDTO.id, qTipoDTO.nombre, qSubvinculosPersonasDTO.perId, qTipoDTO1.id, qTipoDTO1.nombre);
        long vinculo = -1L;
        if (queryVinculo.singleResult(qTipoDTO1.id) != null)
            vinculo = queryVinculo.singleResult(qTipoDTO1.id);
        if (vinculo == -1L)
            return 292629L;
        else
            return vinculo;
    }

    private String dameParametro(long perId, String parametro) {
        String nativeQuery =    "SELECT MIN(valor) " +
                                "FROM se_parametros " +
                                "WHERE nombre = '" + parametro + "'" +
                                    " AND COALESCE(persona_id, -1) = COALESCE(" + perId + ", -1)";
        Query query = entityManager.createNativeQuery(nativeQuery);
        Object result = query.getSingleResult();

        if (result == null) return null;
        else                return result.toString();
    }
}
