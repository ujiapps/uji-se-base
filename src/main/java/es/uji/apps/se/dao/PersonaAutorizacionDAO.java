package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PersonaAutorizacionDAO extends BaseDAODatabaseImpl{
    private QAutorizacionDTO qAutorizacion = QAutorizacionDTO.autorizacionDTO;

    public List<Autorizacion> getAutorizaciones(Paginacion paginacion, Long personaId) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String hoy = formato.format(DateExtensions.getCurrentDate());
        JPAQuery query = new JPAQuery(entityManager)
                .from(qAutorizacion)
                .where(qAutorizacion.personaId.eq(personaId)
                        .and(Expressions.stringTemplate("TO_CHAR({0}, {1})", qAutorizacion.horaEntrada, "DD/MM/YYYY").eq(hoy)));

        if (paginacion != null) {
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if(atributoOrdenacion != null) {
                query.orderBy(ordenaAccesos(orden, atributoOrdenacion));
            }

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.
                list(qAutorizacion.id,
                        qAutorizacion.zonaId,
                        qAutorizacion.zonaNombre,
                        qAutorizacion.horaEntrada,
                        qAutorizacion.horaSalida,
                        qAutorizacion.horaInicioReserva,
                        qAutorizacion.horaFinReserva,
                        qAutorizacion.actividadId,
                        qAutorizacion.actividadNombre,
                        qAutorizacion.origenId,
                        qAutorizacion.origenNombre)
                .stream()
                .map(tuple -> {
                    Autorizacion autorizacion = new Autorizacion();
                    autorizacion.setId(tuple.get(qAutorizacion.id));
                    autorizacion.setZonaId(tuple.get(qAutorizacion.zonaId));
                    autorizacion.setZonaNombre(tuple.get(qAutorizacion.zonaNombre));
                    autorizacion.setHoraEntrada(tuple.get(qAutorizacion.horaEntrada));
                    autorizacion.setHoraSalida(tuple.get(qAutorizacion.horaSalida));
                    autorizacion.setHoraInicioReserva(tuple.get(qAutorizacion.horaInicioReserva));
                    autorizacion.setHoraFinReserva(tuple.get(qAutorizacion.horaFinReserva));
                    autorizacion.setActividadId(tuple.get(qAutorizacion.actividadId));
                    autorizacion.setActividadNombre(tuple.get(qAutorizacion.actividadNombre));
                    autorizacion.setOrigenId(tuple.get(qAutorizacion.origenId));
                    autorizacion.setOrigenNombre(tuple.get(qAutorizacion.origenNombre));
                    return autorizacion;
                }).collect(Collectors.toList());
    }

    private OrderSpecifier ordenaAccesos(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "zonaId":
                return orden.equals("ASC") ? qAutorizacion.zonaId.asc() : qAutorizacion.zonaId.desc();

            case "zonaNombre":
                return orden.equals("ASC") ? qAutorizacion.zonaNombre.asc() : qAutorizacion.zonaNombre.desc();

            case "actividadNombre":
                return orden.equals("ASC") ? qAutorizacion.actividadNombre.asc() : qAutorizacion.actividadNombre.desc();

            case "origenNombre":
                return orden.equals("ASC") ? qAutorizacion.origenNombre.asc() : qAutorizacion.origenNombre.desc();

            default:
                return qAutorizacion.zonaId.asc();
        }
    }

}
