package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.EquipacionEnvioDTO;
import es.uji.apps.se.dto.QEquipacionEnvioDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EquipacionEnvioDAO extends BaseDAODatabaseImpl {
    public EquipacionEnvioDTO getEnvioById(Long envioId) {
        QEquipacionEnvioDTO qEquipacionEnvioDTO = QEquipacionEnvioDTO.equipacionEnvioDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEquipacionEnvioDTO).where(qEquipacionEnvioDTO.id.eq(envioId));
        return query.uniqueResult(qEquipacionEnvioDTO);
    }

    public List<EquipacionEnvioDTO> getEnvios() {

        QEquipacionEnvioDTO qEquipacionEnvioDTO = QEquipacionEnvioDTO.equipacionEnvioDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEquipacionEnvioDTO);
        return query.list(qEquipacionEnvioDTO);
    }
}
