package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.OfertaTarifaDescuentoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.QOfertaTarifaDescuentoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OfertaTarifaDescuentoDAO extends BaseDAODatabaseImpl {


    private QOfertaTarifaDescuentoDTO qOfertaTarifaDescuento = QOfertaTarifaDescuentoDTO.ofertaTarifaDescuentoDTO;
    private Map<String, Path> relations = new HashMap<String, Path>();

    public OfertaTarifaDescuentoDAO() {
        relations.put("id", qOfertaTarifaDescuento.id);
        relations.put("tarjetaDeportivaId", qOfertaTarifaDescuento.tarjetaDeportiva.nombre);
        relations.put("importe", qOfertaTarifaDescuento.importe);
        relations.put("fechaInicio", qOfertaTarifaDescuento.fechaInicio);
        relations.put("fechaFin", qOfertaTarifaDescuento.fechaFin);
    }

    public List<OfertaTarifaDescuentoDTO> getDescuentosTarifas(Long ofertaTarifaId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qOfertaTarifaDescuento).where(qOfertaTarifaDescuento.ofertaTarifaDTO.id.eq(ofertaTarifaId));

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qOfertaTarifaDescuento);
    }
}
