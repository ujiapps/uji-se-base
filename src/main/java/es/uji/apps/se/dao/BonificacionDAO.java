package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class BonificacionDAO extends BaseDAODatabaseImpl {

    QBonificacionDTO bonificacionDTO = QBonificacionDTO.bonificacionDTO;
    QBonificacionUsoDTO bonificacionUsoDTO = QBonificacionUsoDTO.bonificacionUsoDTO;
    QBonificacionActivaDTO qBonificacionActiva = QBonificacionActivaDTO.bonificacionActivaDTO;

    public List<BonificacionActivaDTO> getBonificacionesActivasGenericasByPersonaId(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qBonificacionActiva)
                .where(qBonificacionActiva.origen.isNull()
                        .and(qBonificacionActiva.usos.isNull())
                        .and(qBonificacionActiva.mostrar.isTrue())
                        .and(qBonificacionActiva.personaUjiDTO.id.eq(personaId)));
        return query.list(qBonificacionActiva);
    }

    public List<BonificacionActiva> getBonificacionesAsociadasAActividadesDePersonaByAdmin(Long personaId) {
        Date ahora = new Date();

        return new JPAQuery(entityManager)
                .from(qBonificacionActiva)
                .where(qBonificacionActiva.personaUjiDTO.id.eq(personaId)
                        .and((qBonificacionActiva.origen.eq("ACT").or(qBonificacionActiva.origen.isNull()))))
                .list(new QBonificacionActiva(qBonificacionActiva.id,
                        qBonificacionActiva.fechaFin,
                        qBonificacionActiva.usos,
                        qBonificacionActiva.importe,
                        qBonificacionActiva.porcentaje,
                        qBonificacionActiva.referencia,
                        qBonificacionActiva.origen));
    }

    public BonificacionActivaDTO getBonificacionById(Long bonificacionId) {
        QBonificacionActivaDTO qBonificacionActiva = QBonificacionActivaDTO.bonificacionActivaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qBonificacionActiva).where(qBonificacionActiva.id.eq(bonificacionId));

        return query.uniqueResult(qBonificacionActiva);
    }

    public List<BonificacionActivaDTO> getBonificacionesActivasByTipoTarjetaAndPersonaId(Long personaId, TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO) {
        QBonificacionActivaDTO qBonificacionActiva = QBonificacionActivaDTO.bonificacionActivaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qBonificacionActiva)
                .where(qBonificacionActiva.origen.eq("TAR")
                        .and(qBonificacionActiva.usos.isNull())
                        .and(qBonificacionActiva.referencia.eq(tarjetaDeportivaTipoDTO.getId()))
                        .and(qBonificacionActiva.mostrar.isTrue())
                        .and(qBonificacionActiva.personaUjiDTO.id.eq(personaId)));
        return query.list(qBonificacionActiva);

    }

    public List<Bonificacion> getBonificaciones(Long personaId) {
        List<Bonificacion> bonificaciones = new JPAQuery(entityManager)
                .from(bonificacionDTO)
                .where(bonificacionDTO.personaUjiDTO.id.eq(personaId))
                .list(new QBonificacion(bonificacionDTO.id,
                        bonificacionDTO.campanyaId,
                        bonificacionDTO.referencia,
                        bonificacionDTO.origen,
                        bonificacionDTO.usos,
                        bonificacionDTO.importe,
                        bonificacionDTO.porcentaje,
                        bonificacionDTO.fechaIni,
                        bonificacionDTO.fechaFin,
                        bonificacionDTO.mostrar,
                        bonificacionDTO.comentarios,
                        bonificacionDTO.personaUjiDTO.id));

        return bonificaciones.stream().map(bonificacion -> {
            JPAQuery q = new JPAQuery(entityManager)
                    .from(bonificacionUsoDTO)
                    .where(bonificacionUsoDTO.bonificacionId.eq(bonificacion.getId()));
            Long numUsos = q.count();

            Long total = q.singleResult(bonificacionUsoDTO.valor.sum());

            bonificacion.setNumUsos(numUsos);
            bonificacion.setUsado(total);

            return bonificacion;
        }).collect(Collectors.toList());
    }

    public List<BonificacionDetalle> getBonificacionDetalles(Long bonificacionId) {
        return new JPAQuery(entityManager)
                .from(bonificacionUsoDTO)
                .where(bonificacionUsoDTO.bonificacionId.eq(bonificacionId))
                .list(new QBonificacionDetalle(
                        bonificacionUsoDTO.id,
                        bonificacionUsoDTO.valor,
                        bonificacionUsoDTO.origen,
                        bonificacionUsoDTO.tipoEntrada,
                        bonificacionUsoDTO.referencia,
                        bonificacionUsoDTO.fecha,
                        bonificacionUsoDTO.bonificacionId
                ));
    }

    @Transactional
    public void addBonificaciones(List<BonificacioAutoDTO> bonificaciones, Long personaId) {
        for (BonificacioAutoDTO bono : bonificaciones) {
            BonificacionDTO bonificacionDTO = new BonificacionDTO();
            bonificacionDTO.setId(bono.getId());
            bonificacionDTO.setPersonaUji(new PersonaUjiDTO(personaId));
            bonificacionDTO.setReferencia(bono.getReferencia2Id());
            bonificacionDTO.setOrigen(bono.getOrigen2());
            bonificacionDTO.setUsos(bono.getUsos());
            bonificacionDTO.setImporte(bono.getImporte());
            bonificacionDTO.setPorcentaje(bono.getPorcentaje());
            bonificacionDTO.setCampanyaId(bono.getCampanya().getId());
            bonificacionDTO.setFechaIni(bono.getFechaIni());
            bonificacionDTO.setFechaFin(bono.getFechaFin());
            bonificacionDTO.setMostrar(bono.getMostrar());
            this.insert(bonificacionDTO);
        }
    }

    @Transactional
    public void asignarBonificacion(Long referenciaId, Long bonificacionId, String origen) {
        new JPAUpdateClause(entityManager, bonificacionDTO)
                .set(bonificacionDTO.referencia, referenciaId)
                .set(bonificacionDTO.origen, origen)
                .where(bonificacionDTO.id.eq(bonificacionId))
                .execute();
    }

    @Transactional
    public void deleteBonificacion(Long bonificacionId) {
        new JPADeleteClause(entityManager, bonificacionDTO)
                .where(bonificacionDTO.id.eq(bonificacionId))
                .execute();
    }

    @Transactional
    public void updateBonificacion(Bonificacion bonificacion) {
        new JPAUpdateClause(entityManager, bonificacionDTO)
                .set(bonificacionDTO.usos, bonificacion.getUsos())
                .set(bonificacionDTO.importe, bonificacion.getImporte())
                .set(bonificacionDTO.porcentaje, bonificacion.getPorcentaje())
                .set(bonificacionDTO.fechaIni, bonificacion.getFechaIni())
                .set(bonificacionDTO.fechaFin, bonificacion.getFechaFin())
                .set(bonificacionDTO.mostrar, bonificacion.getMostrar())
                .set(bonificacionDTO.comentarios, bonificacion.getComentarios())
                .where(bonificacionDTO.id.eq(bonificacion.getId()))
                .execute();
    }

    public List<BonificacionActiva> getBonificacionesAsociadasACarnetsOBonoTipoDePersonaByAdmin(Long personaId, Long tipoBonoId) {

        return new JPAQuery(entityManager)
                .from(qBonificacionActiva)
                .where(qBonificacionActiva.personaUjiDTO.id.eq(personaId)
                        .and(qBonificacionActiva.referencia.eq(tipoBonoId)))
                .list(new QBonificacionActiva(qBonificacionActiva.id,
                        qBonificacionActiva.fechaFin,
                        qBonificacionActiva.usos,
                        qBonificacionActiva.importe,
                        qBonificacionActiva.porcentaje
                ));
    }

    public List<BonificacionActiva> getBonificacionesAsociadasACarnetsOBonosDePersonaByAdmin(Long personaId) {

        return new JPAQuery(entityManager)
                .from(qBonificacionActiva)
                .where(qBonificacionActiva.personaUjiDTO.id.eq(personaId)
                        .and(qBonificacionActiva.referencia.isNull())
                        .and(qBonificacionActiva.importe.isNotNull().or(qBonificacionActiva.porcentaje.isNotNull()))
                        .and((qBonificacionActiva.origen.eq("TD").or(qBonificacionActiva.origen.isNull()))))
                .list(new QBonificacionActiva(qBonificacionActiva.id,
                        qBonificacionActiva.fechaFin,
                        qBonificacionActiva.usos,
                        qBonificacionActiva.importe,
                        qBonificacionActiva.porcentaje));

    }
}
