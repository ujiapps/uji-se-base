package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;
import es.uji.apps.se.dto.EnvioPersonaCorreoVwDTO;
import es.uji.apps.se.dto.PersonaUjiDTO;
import es.uji.apps.se.dto.QEnvioPersonaCorreoVwDTO;
import es.uji.apps.se.dto.QEnvioPersonaVwDTO;
import es.uji.apps.se.model.EnvioPersona;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.QEnvioPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EnvioPersonaCorreoDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {
    private QEnvioPersonaCorreoVwDTO qEnvioPersonaCorreoVwDTO = QEnvioPersonaCorreoVwDTO.envioPersonaCorreoVwDTO;

    private StringExpression limpiaAcentos(StringPath nombre) {
        return Expressions.stringTemplate(
                "upper(translate({0}, 'âàãáÁÂÀÃéèêÉÈÊíÍóôõòÓÔÕÒüúÜÚ', 'AAAAAAAAEEEEEEIIOOOOOOOOUUUU'))",
                nombre);
    }

    @Override
    public List search(String query) {

        JPAQuery qQueryPersonas = new JPAQuery(entityManager);

        String q = StringUtils.limpiaAcentos(query).trim().toUpperCase();

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (query != null && !query.isEmpty()) {
            qQueryPersonas.from(qEnvioPersonaCorreoVwDTO)
                    .where(limpiaAcentos(qEnvioPersonaCorreoVwDTO.identificacion).like("%" + q + "%")
                            .or(limpiaAcentos(qEnvioPersonaCorreoVwDTO.nombre).like("%" + q + "%")
                            .or(limpiaAcentos(qEnvioPersonaCorreoVwDTO.mail).like("%" + q + "%"))));

            List<EnvioPersonaCorreoVwDTO> listaPersonas = qQueryPersonas.list(qEnvioPersonaCorreoVwDTO);

            for (EnvioPersonaCorreoVwDTO envioPersonaCorreoVwDTO : listaPersonas) {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(envioPersonaCorreoVwDTO.getId()));
                lookupItem.setNombre(envioPersonaCorreoVwDTO.getNombre());
                lookupItem.addExtraParam("email", envioPersonaCorreoVwDTO.getMail());
                lookupItem.addExtraParam("identificacion", envioPersonaCorreoVwDTO.getIdentificacion());

                result.add(lookupItem);
            }
        }

        return result;
    }
}
