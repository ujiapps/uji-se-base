package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PersonaNotificacionDAO extends BaseDAODatabaseImpl {

    private QNotificacionPersonaDTO qNotificacionPersona = QNotificacionPersonaDTO.notificacionPersonaDTO;

    public List<NotificacionPersona> getNotificacionesPersona(Long personaId, Paginacion paginacion) {
        QEnvioDTO qEnvio = QEnvioDTO.envioDTO;

        JPAQuery query = new JPAQuery(entityManager)
                .from(qNotificacionPersona)
                .leftJoin(qNotificacionPersona.envioRemesa, qEnvio)
                .where(qNotificacionPersona.personaId.eq(personaId))
                .orderBy(qNotificacionPersona.fecha.desc());

        if (ParamUtils.isNotNull(paginacion)) {
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if (atributoOrdenacion != null) {
                query.orderBy(ordena(orden, atributoOrdenacion));
            } else
                query.orderBy(qNotificacionPersona.fecha.desc());

            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.list(new QNotificacionPersona(
                qNotificacionPersona.id,
                qNotificacionPersona.personaId,
                qNotificacionPersona.fecha,
                qNotificacionPersona.para,
                qNotificacionPersona.asunto,
                ParamUtils.isNotNull(qNotificacionPersona.cuerpo)?qNotificacionPersona.cuerpo:qEnvio.cuerpo
        ));
    }

    private OrderSpecifier<?> ordena(String orden, String atributoOrdenacion) {

        if (atributoOrdenacion.equals("id")) {
            return orden.equals("ASC") ? qNotificacionPersona.id.asc() : qNotificacionPersona.id.desc();
        }
        if (atributoOrdenacion.equals("fecha")) {
            return orden.equals("ASC") ? qNotificacionPersona.fecha.asc() : qNotificacionPersona.fecha.desc();
        }
        if (atributoOrdenacion.equals("para")) {
            return orden.equals("ASC") ? qNotificacionPersona.para.asc() : qNotificacionPersona.para.desc();
        }
        if (atributoOrdenacion.equals("asunto")) {
            return orden.equals("ASC") ? qNotificacionPersona.asunto.asc() : qNotificacionPersona.asunto.desc();
        }
        if (atributoOrdenacion.equals("cuerpo")) {
            return orden.equals("ASC") ? qNotificacionPersona.cuerpo.asc() : qNotificacionPersona.cuerpo.desc();
        }

        return qNotificacionPersona.fecha.desc();
    }
}
