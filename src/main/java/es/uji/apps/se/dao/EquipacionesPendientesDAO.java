package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.ListaEquipacionesPendientesItemsVWDTO;
import es.uji.apps.se.dto.QEquipacionesPendientesVWDTO;
import es.uji.apps.se.dto.QListaEquipacionesPendientesItemsVWDTO;
import es.uji.apps.se.model.EquipacionesPendientes;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.QEquipacionesPendientes;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class EquipacionesPendientesDAO extends BaseDAODatabaseImpl {

    private final QEquipacionesPendientesVWDTO qEquipacionesPendientes = QEquipacionesPendientesVWDTO.equipacionesPendientesVWDTO;
    private final QListaEquipacionesPendientesItemsVWDTO qListaEquipacionesPendientesItemsVWDTO = QListaEquipacionesPendientesItemsVWDTO.listaEquipacionesPendientesItemsVWDTO;

    public List<EquipacionesPendientes> getPendientesDevolucionEquipacion(Date fechaHasta, Long cursoAca, Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager)
                .from(qEquipacionesPendientes)
                .where(qEquipacionesPendientes.cursoAcademico.in(cursoAca, cursoAca - 1)
                        .and(qEquipacionesPendientes.fechaMax.lt(fechaHasta))).distinct();

        if (paginacion != null) {
            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if (atributoOrdenacion != null) {
                query.orderBy(ordenaEquipacionesPendientes(orden, atributoOrdenacion));
            } else
                query.orderBy(qEquipacionesPendientes.nombreCompleto.desc());
        }


        return query.list(new QEquipacionesPendientes(qEquipacionesPendientes.personaId,
                qEquipacionesPendientes.cursoAcademico,
                qEquipacionesPendientes.nombreCompleto,
                qEquipacionesPendientes.identificacion));

    }

    private OrderSpecifier<?> ordenaEquipacionesPendientes(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "cursoAcademico":
                return orden.equals("ASC") ? qEquipacionesPendientes.cursoAcademico.asc() : qEquipacionesPendientes.cursoAcademico.desc();

            case "nombreCompleto":
                return orden.equals("ASC") ? qEquipacionesPendientes.nombreCompleto.asc() : qEquipacionesPendientes.nombreCompleto.desc();

            case "identificacion":
                return orden.equals("ASC") ? qEquipacionesPendientes.identificacion.asc() : qEquipacionesPendientes.identificacion.desc();
            default:
                return qEquipacionesPendientes.nombreCompleto.asc();
        }
    }

    public List<ListaEquipacionesPendientesItemsVWDTO> getListadoEquipacionesPendientes(Long cursoAca, Date filtroFecha) {
        return new JPAQuery(entityManager)
                .from(qListaEquipacionesPendientesItemsVWDTO)
                .where(qListaEquipacionesPendientesItemsVWDTO.curso.in(cursoAca, cursoAca - 1)
                        .and(qListaEquipacionesPendientesItemsVWDTO.fechaDevolucion.lt(filtroFecha))).list(qListaEquipacionesPendientesItemsVWDTO);
    }
}
