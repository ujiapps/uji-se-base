package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Repository
public class CompeticionSancionDAO extends BaseDAODatabaseImpl {

    QCompeticionSancionDTO qCompeticionSancionDTO = QCompeticionSancionDTO.competicionSancionDTO;

    QCompeticionMiembroDTO qCompeticionMiembroDTO = QCompeticionMiembroDTO.competicionMiembroDTO;

    QCompeticionPartidoDTO qCompeticionPartidoDTO = QCompeticionPartidoDTO.competicionPartidoDTO;

    QCompeticionGrupoDTO qCompeticionGrupoDTO = QCompeticionGrupoDTO.competicionGrupoDTO;

    QCompeticionFaseDTO qCompeticionFaseDTO = QCompeticionFaseDTO.competicionFaseDTO;

    @Transactional
    public void updateCompeticionSancion(Long inscripcionId, Long partidoId, String tarjetas){

        Long miembroId = new JPAQuery(entityManager).from(qCompeticionMiembroDTO)
                .where(qCompeticionMiembroDTO.inscripcion.id.eq(inscripcionId))
                .singleResult(qCompeticionMiembroDTO.id);

        Boolean exists = new JPAQuery(entityManager)
                .from(qCompeticionSancionDTO)
                .where(qCompeticionSancionDTO.competicionMiembro.id.eq(miembroId)
                        .and(qCompeticionSancionDTO.partido.eq(partidoId)))
                .exists();

        if (exists){
            new JPAUpdateClause(entityManager, qCompeticionSancionDTO)
                    .set(qCompeticionSancionDTO.tarjetas, tarjetas)
                    .where(qCompeticionSancionDTO.competicionMiembro.id.eq(miembroId)
                            .and(qCompeticionSancionDTO.partido.eq(partidoId)))
                    .execute();
        }

        else {

            CompeticionSancionDTO nuevaFila = new CompeticionSancionDTO();
            CompeticionMiembroDTO competicionMiembro = new CompeticionMiembroDTO();
            competicionMiembro.setId(miembroId);
            nuevaFila.setCompeticionMiembro(competicionMiembro);
            nuevaFila.setPartido(partidoId);
            nuevaFila.setTarjetas(tarjetas);
            insert(nuevaFila);
        }

    }
    @Transactional
    public void deleteCompeticionSancion(Long inscripcionId, Long partidoId){

        Long miembroId = new JPAQuery(entityManager).from(qCompeticionMiembroDTO)
                .where(qCompeticionMiembroDTO.inscripcion.id.eq(inscripcionId))
                .singleResult(qCompeticionMiembroDTO.id);

        Boolean exists = new JPAQuery(entityManager)
                .from(qCompeticionSancionDTO)
                .where(qCompeticionSancionDTO.competicionMiembro.id.eq(miembroId)
                        .and(qCompeticionSancionDTO.partido.eq(partidoId)))
                .exists();

        if (exists){
            new JPADeleteClause(entityManager, qCompeticionSancionDTO)
                    .where(qCompeticionSancionDTO.competicionMiembro.id.eq(miembroId)
                            .and(qCompeticionSancionDTO.partido.eq(partidoId)))
                    .execute();
        }

    }

    public Map<Long, String> getCompeticionSancionLista(Long competicionId) {

        List<Tuple> results = new JPAQuery(entityManager)
                .from(qCompeticionSancionDTO)
                .join(qCompeticionSancionDTO.competicionMiembro, qCompeticionMiembroDTO)
                .where(qCompeticionSancionDTO.partido.eq(competicionId))
                .list(qCompeticionMiembroDTO.inscripcion.id, qCompeticionSancionDTO.tarjetas);

        Map<Long, String> diccionario = new HashMap<>();
        for (Tuple result : results) {
            Long inscripcion = result.get(qCompeticionMiembroDTO.inscripcion.id);
            String tarjetas = result.get(qCompeticionSancionDTO.tarjetas);
            diccionario.put(inscripcion, tarjetas);
        }

        return diccionario;
    }

    public boolean getEstaSancionado(Long miembroId, Long partidoId) {

        JPAQuery datosPartido = new JPAQuery(entityManager).from(qCompeticionPartidoDTO)
                .join(qCompeticionGrupoDTO).on(qCompeticionPartidoDTO.grupoId.eq(qCompeticionGrupoDTO.id))
                .join(qCompeticionFaseDTO).on(qCompeticionGrupoDTO.faseId.eq(qCompeticionFaseDTO.id))
                .where(qCompeticionPartidoDTO.id.eq(partidoId));

        Date fecha = datosPartido.singleResult(qCompeticionPartidoDTO.fecha);
        Long competicionId = datosPartido.singleResult(qCompeticionFaseDTO.competicionId);

        if (fecha != null) {

            JPAQuery result = new JPAQuery(entityManager).from(qCompeticionSancionDTO)
                    .join(qCompeticionPartidoDTO).on(qCompeticionSancionDTO.partido.eq(qCompeticionPartidoDTO.id))
                    .join(qCompeticionGrupoDTO).on(qCompeticionPartidoDTO.grupoId.eq(qCompeticionGrupoDTO.id))
                    .join(qCompeticionFaseDTO).on(qCompeticionGrupoDTO.faseId.eq(qCompeticionFaseDTO.id))
                    .where(qCompeticionSancionDTO.competicionMiembro.id.eq(miembroId)
                            .and(qCompeticionFaseDTO.competicionId.eq(competicionId)).and(qCompeticionSancionDTO.exclusiones.gt(0)));

            Long numPartidos = result.singleResult(qCompeticionSancionDTO.exclusiones.max());
            Date fechaExclusion = result.singleResult(qCompeticionPartidoDTO.fecha.max());

            numPartidos = (numPartidos != null) ? numPartidos : 0L;

            if (numPartidos > 0) {
                Long cuantosPartidos = new JPAQuery(entityManager).from(qCompeticionMiembroDTO)
                        .join(qCompeticionPartidoDTO).on(qCompeticionMiembroDTO.competicionEquipo.id.eq(qCompeticionPartidoDTO.equipoCasa.id)
                                .or(qCompeticionMiembroDTO.competicionEquipo.id.eq(qCompeticionPartidoDTO.equipoFuera.id)))
                        .join(qCompeticionGrupoDTO).on(qCompeticionPartidoDTO.grupoId.eq(qCompeticionGrupoDTO.id))
                        .join(qCompeticionFaseDTO).on(qCompeticionGrupoDTO.faseId.eq(qCompeticionFaseDTO.id))
                        .where(qCompeticionMiembroDTO.id.eq(miembroId)
                                .and(qCompeticionPartidoDTO.fecha.isNotNull())
                                .and(qCompeticionPartidoDTO.fecha.gt(fechaExclusion))
                                .and(qCompeticionPartidoDTO.fecha.between(fechaExclusion, fecha))).count();

                if (cuantosPartidos <= numPartidos && fechaExclusion != fecha)
                    return true;

            }
        }

        return false;

    }
}
