package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.EnvioPersona;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.QEnvioPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EnvioPersonaDAO extends BaseDAODatabaseImpl {
    private QEnvioPersonaVwDTO qEnvioPersonaVwDTO = QEnvioPersonaVwDTO.envioPersonaVwDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public EnvioPersonaDAO() {
        relations.put("id", qEnvioPersonaVwDTO.persona);
        relations.put("identificacion", qEnvioPersonaVwDTO.identificacion);
        relations.put("nombre", qEnvioPersonaVwDTO.nombre);
        relations.put("correo", qEnvioPersonaVwDTO.mail);
        relations.put("recibirCorreo", qEnvioPersonaVwDTO.recibirCorreo);
    }

    public List<EnvioPersona> getEnvioPersonasByEnvioId(Long envioId, Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvioPersonaVwDTO)
                .where(qEnvioPersonaVwDTO.envio.eq(envioId));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(new QEnvioPersona(qEnvioPersonaVwDTO.persona,
                qEnvioPersonaVwDTO.envio,
                qEnvioPersonaVwDTO.nombre,
                qEnvioPersonaVwDTO.identificacion,
                qEnvioPersonaVwDTO.mail,
                qEnvioPersonaVwDTO.recibirCorreo));
    }
}
