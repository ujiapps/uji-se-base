package es.uji.apps.se.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class EncriptaClave {

    private EncriptaPassword encriptaPassword;

    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        EncriptaClave.dataSource = dataSource;
    }

    @PostConstruct
    public void init(){
        this.encriptaPassword = new EncriptaPassword(dataSource);
    }

    public void encriptarClave(String clave, Long personaId){
        encriptaPassword.encriptaClave(clave, personaId);
    }

    private class EncriptaPassword extends StoredProcedure {

        private static final String SQL = "uji_sports.encripta_clave";
        private static final String PCLAVE = "clave";
        private static final String PPERSONA = "p_persona";

        public EncriptaPassword(DataSource dataSource){
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PCLAVE, Types.VARCHAR));
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            compile();
        }

        public void encriptaClave(String clave, Long personaId) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PCLAVE, clave);
            inParams.put(PPERSONA, personaId);
            Map<String, Object> results = execute(inParams);
        }
    }
}
