package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.MonitorClaseCalendarioDTO;
import es.uji.apps.se.dto.QMonitorClaseCalendarioDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MonitorCalendarioDAO extends BaseDAODatabaseImpl {
    public List<MonitorClaseCalendarioDTO> getMonitoresCalendario(Long calendarioClaseId) {

        QMonitorClaseCalendarioDTO qMonitorClaseCalendario = QMonitorClaseCalendarioDTO.monitorClaseCalendarioDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMonitorClaseCalendario).where(qMonitorClaseCalendario.calendarioClaseDTO.id.eq(calendarioClaseId));
        return query.list(qMonitorClaseCalendario);
    }
}
