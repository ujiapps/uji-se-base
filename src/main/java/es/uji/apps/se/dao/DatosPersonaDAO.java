package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.DatosPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DatosPersonaDAO extends BaseDAODatabaseImpl {
    private final QPersonaDTO qPersonaDTO = QPersonaDTO.personaDTO;
    private final QPerPersonasDTO qPerPersonasDTO = QPerPersonasDTO.perPersonasDTO;
    private final QNacimientosDTO qNacimientosDTO = QNacimientosDTO.nacimientosDTO;
    private final QPersonaUjiDTO qPersonaUjiDTO = QPersonaUjiDTO.personaUjiDTO;
    private final QCuentasBancariasDTO qCuentasBancariasDTO = QCuentasBancariasDTO.cuentasBancariasDTO;
    private final QDomiciliosDTO qDomiciliosDTO = QDomiciliosDTO.domiciliosDTO;
    private final QPaisComboDTO qPaisComboDTO = QPaisComboDTO.paisComboDTO;
    private final QPoblacioComboDTO qPoblacioComboDTO = QPoblacioComboDTO.poblacioComboDTO;
    private final QEliteDTO qEliteDTO = QEliteDTO.eliteDTO;
    private final QRgpdDTO qRgpdDTO = QRgpdDTO.rgpdDTO;

    @InjectParam
    private EliteDAO eliteDAO;
    @InjectParam
    private NacimientosDAO nacimientosDAO;
    @InjectParam
    private CuentasBancariasDAO cuentasBancariasDAO;
    @InjectParam
    private DomiciliosDAO domiciliosDAO;

    public List<DatosPersona> getDatosPersona(Long userId, Boolean consentimiento, Boolean elite) {
        return new JPAQuery(entityManager)
                .from(qPersonaUjiDTO)
                .leftJoin(qPersonaDTO).on(qPersonaUjiDTO.id.eq(qPersonaDTO.id))
                .leftJoin(qEliteDTO).on(qPersonaUjiDTO.id.eq(qEliteDTO.personaUjiDTO.id))
                .leftJoin(qCuentasBancariasDTO).on(qPersonaUjiDTO.id.eq(qCuentasBancariasDTO.perId).and(qCuentasBancariasDTO.tctId.eq(22L)))
                .leftJoin(qPoblacioComboDTO).on(qPoblacioComboDTO.id.eq(qPersonaUjiDTO.direccionSePueblo))
                .leftJoin(qPaisComboDTO).on(qPaisComboDTO.id.eq(qPersonaUjiDTO.direccionSePais))
                .where(qPersonaUjiDTO.id.eq(userId))
                .distinct()
                .list(qPersonaUjiDTO.nombre, qPersonaUjiDTO.apellido1, qPersonaUjiDTO.apellido2, qPersonaUjiDTO.tipoIdentificacion,
                        qPersonaUjiDTO.identificacion, qPersonaUjiDTO.fechaNacimiento, qPersonaUjiDTO.sexo, qPersonaDTO.libroFamilia,
                        qPersonaDTO.mail, qPersonaDTO.movil, qPersonaDTO.movilPublico, qCuentasBancariasDTO.iban, qPersonaDTO.contactos,
                        qPersonaDTO.arbitro, qPersonaUjiDTO.movil, qPersonaUjiDTO.mail, qPersonaUjiDTO.telefono, qPersonaUjiDTO.direccionSeCodigoPostal,
                        qPersonaUjiDTO.direccionSeEscalera, qPersonaUjiDTO.direccionSeNombre, qPersonaUjiDTO.direccionSeNumero, qPersonaUjiDTO.direccionSePais,
                        qPersonaUjiDTO.direccionSeProvincia, qPersonaUjiDTO.direccionSePueblo, qPersonaUjiDTO.direccionSePuerta, qPersonaUjiDTO.direccionSeVia,
                        qPersonaUjiDTO.domicilioLinea1, qPersonaUjiDTO.domicilioLinea2, qPoblacioComboDTO.nombre, qPaisComboDTO.nombre
                ).stream().map(tuple -> new DatosPersona(
                        tuple.get(qPersonaUjiDTO.nombre),
                        tuple.get(qPersonaUjiDTO.apellido1),
                        tuple.get(qPersonaUjiDTO.apellido2),
                        tuple.get(qPersonaUjiDTO.tipoIdentificacion),
                        tuple.get(qPersonaUjiDTO.identificacion),
                        tuple.get(qPersonaUjiDTO.fechaNacimiento),
                        tuple.get(qPersonaUjiDTO.sexo),
                        tuple.get(qPersonaDTO.libroFamilia),
                        tuple.get(qPersonaDTO.mail),
                        tuple.get(qPersonaDTO.movil),
                        tuple.get(qPersonaDTO.movilPublico),
                        tuple.get(qCuentasBancariasDTO.iban),
                        tuple.get(qPersonaUjiDTO.direccionSeVia),
                        tuple.get(qPersonaUjiDTO.direccionSeNombre),
                        tuple.get(qPersonaUjiDTO.direccionSeNumero),
                        tuple.get(qPersonaUjiDTO.direccionSeEscalera),
                        tuple.get(qPersonaUjiDTO.direccionSePiso),
                        tuple.get(qPersonaUjiDTO.direccionSePuerta),
                        tuple.get(qPersonaUjiDTO.direccionSeCodigoPostal),
                        tuple.get(qPersonaUjiDTO.direccionSeProvincia) != null ? Integer.toString(Integer.parseInt(tuple.get(qPersonaUjiDTO.direccionSeProvincia))) : tuple.get(qPersonaUjiDTO.direccionSeProvincia),
                        tuple.get(qPersonaUjiDTO.direccionSePueblo),
                        tuple.get(qPersonaUjiDTO.direccionSePais),
                        consentimiento,
                        tuple.get(qPersonaDTO.contactos),
                        tuple.get(qPersonaDTO.arbitro),
                        elite,
                        tuple.get(qPersonaUjiDTO.movil),
                        tuple.get(qPersonaUjiDTO.mail),
                        tuple.get(qPersonaUjiDTO.telefono),
                        tuple.get(qPersonaUjiDTO.domicilioLinea1) + " " + tuple.get(qPersonaUjiDTO.domicilioLinea2),
                        tuple.get(qPoblacioComboDTO.nombre),
                        tuple.get(qPaisComboDTO.nombre)
                )).collect(Collectors.toList());
    }

    public Long getRgpd(Long userId, Long cursoAcademico) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qRgpdDTO).where(qRgpdDTO.id.eq(userId)
                .and(qRgpdDTO.curso.eq(cursoAcademico))
                .and(qRgpdDTO.codTratamiento.eq("U034"))).list(qRgpdDTO.id);

        return query.count();

    }

    @Transactional
    public Long getElite(Long userId, Long cursoAcademico) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEliteDTO).where(qEliteDTO.personaUjiDTO.id.eq(userId)
                .and(qEliteDTO.cursoAcademicoDTO.id.eq(cursoAcademico))).list(qEliteDTO);

        return query.count();

    }

    @Transactional
    public void updateDatosPersona(DatosPersona datosPersona, Long personaId, Long curso) {

        new JPAUpdateClause(entityManager, qPersonaDTO)
                .set(qPersonaDTO.libroFamilia, datosPersona.getLibroFamilia())
                .set(qPersonaDTO.mail, datosPersona.getMail())
                .set(qPersonaDTO.movil, datosPersona.getMovil())
                .set(qPersonaDTO.movilPublico, datosPersona.getMovilPublico())
                .set(qPersonaDTO.contactos, datosPersona.getOtros())
                .set(qPersonaDTO.arbitro, datosPersona.isArbitro())
                .where(qPersonaDTO.id.eq(personaId))
                .execute();

        new JPAUpdateClause(entityManager, qPerPersonasDTO)
                .set(qPerPersonasDTO.nombre, datosPersona.getNombre())
                .set(qPerPersonasDTO.apellido1, datosPersona.getApellido1())
                .set(qPerPersonasDTO.apellido2, datosPersona.getApellido2())
                .set(qPerPersonasDTO.identificacion, datosPersona.getIdentificacion())
                .set(qPerPersonasDTO.tipoId, datosPersona.getTipoIdentificacion())
                .where(qPerPersonasDTO.id.eq(personaId))
                .execute();

        Long sexo = 0L;

        if (datosPersona.getSexo().equals("Home")) sexo = 1L;
        else if (datosPersona.getSexo().equals("Dona")) sexo = 2L;

        Long nacim = new JPAQuery(entityManager)
                .from(qNacimientosDTO)
                .where(qNacimientosDTO.perId.eq(personaId))
                .count();
        if (nacim > 0) {
            new JPAUpdateClause(entityManager, qNacimientosDTO)
                    .set(qNacimientosDTO.fecha, datosPersona.getFechaNacimiento())
                    .set(qNacimientosDTO.sexo, sexo)
                    .where(qNacimientosDTO.perId.eq(personaId))
                    .execute();
        } else {
            nacimientosDAO.insert(new NacimientosDTO(
                    personaId,
                    datosPersona.getFechaNacimiento(),
                    datosPersona.getPaisCombo(),
                    datosPersona.getPoblacioCombo(),
                    datosPersona.getProvinciaCombo(),
                    sexo));
        }

        if (datosPersona.getIban() != null) {
            Long banco = new JPAQuery(entityManager)
                    .from(qCuentasBancariasDTO)
                    .where(qCuentasBancariasDTO.tctId.eq(22L)
                            .and(qCuentasBancariasDTO.perId.eq(personaId)))
                    .count();
            if (banco > 0) {
                new JPAUpdateClause(entityManager, qCuentasBancariasDTO)
                        .set(qCuentasBancariasDTO.iban, datosPersona.getIban())
                        .set(qCuentasBancariasDTO.tctId, 22L)
                        .where(qCuentasBancariasDTO.perId.eq(personaId))
                        .execute();
            } else {
                cuentasBancariasDAO.insert(new CuentasBancariasDTO(personaId, 22L, datosPersona.getIban()));
            }
        }

        Long domicilio = new JPAQuery(entityManager)
                .from(qDomiciliosDTO)
                .where(qDomiciliosDTO.perId.eq(personaId)
                        .and(qDomiciliosDTO.tdoId.eq(8L)))
                .count();
        if (domicilio > 0) {
            new JPAUpdateClause(entityManager, qDomiciliosDTO)
                    .set(qDomiciliosDTO.nombre, datosPersona.getCalle())
                    .set(qDomiciliosDTO.numero, datosPersona.getNumero())
                    .set(qDomiciliosDTO.escalera, datosPersona.getEscalera())
                    .set(qDomiciliosDTO.piso, datosPersona.getPiso())
                    .set(qDomiciliosDTO.puerta, datosPersona.getPuerta())
                    .set(qDomiciliosDTO.codPostal, datosPersona.getCodPostal())
                    .set(qDomiciliosDTO.provinciaId, datosPersona.getProvinciaCombo())
                    .set(qDomiciliosDTO.puebloId, datosPersona.getPoblacioCombo())
                    .set(qDomiciliosDTO.paisId, datosPersona.getPaisCombo())
                    .where(qDomiciliosDTO.perId.eq(personaId)
                        .and(qDomiciliosDTO.tdoId.eq((8L))))
                    .execute();
        } else {
            Long maximoDom = new JPAQuery(entityManager)
                    .from(qDomiciliosDTO)
                    .where(qDomiciliosDTO.perId.eq(personaId))
                    .singleResult(qDomiciliosDTO.ordenPreferencia.max());
            if (maximoDom == null) maximoDom = 0L;
            domiciliosDAO.insert(new DomiciliosDTO(personaId, maximoDom + 1, 8L, datosPersona.getViaCombo(),
                    datosPersona.getCalle(), datosPersona.getNumero(), datosPersona.getEscalera(), datosPersona.getPiso(),
                    datosPersona.getPuerta(), datosPersona.getCodPostal(), datosPersona.getProvinciaCombo(),
                    datosPersona.getPoblacioCombo(), datosPersona.getPaisCombo()
            ));
        }

        JPAQuery query = new JPAQuery(entityManager);
        Long elit = query.from(qEliteDTO)
                .where(qEliteDTO.personaUjiDTO.id.eq(personaId)
                        .and(qEliteDTO.cursoAcademicoDTO.id.eq(curso)))
                .count();

        if (datosPersona.isDeportistaElite() && elit == 0) {
            eliteDAO.insert(new EliteDTO(personaId, curso));
        } else if (!datosPersona.isDeportistaElite() && elit > 0) {
            new JPADeleteClause(entityManager, qEliteDTO)
                    .where(qEliteDTO.personaUjiDTO.id.eq(personaId)
                            .and(qEliteDTO.cursoAcademicoDTO.id.eq(curso))).execute();
        }
    }



    public Long existeIdentificacion(String identificacion) {
        return new JPAQuery(entityManager)
                .from(qPersonaUjiDTO)
                .where(qPersonaUjiDTO.identificacion.equalsIgnoreCase(identificacion))
                .count();
    }
}
