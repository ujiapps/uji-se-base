package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.ActividadTarifaDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.QActividadTarifaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ActividadTarifaDAO extends BaseDAODatabaseImpl {

    private QActividadTarifaDTO qActividadTarifa = QActividadTarifaDTO.actividadTarifaDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public ActividadTarifaDAO() {
        relations.put("id", qActividadTarifa.id);
        relations.put("vinculoId", qActividadTarifa.vinculo.nombre);
        relations.put("importe", qActividadTarifa.importe);
        relations.put("diasRetrasoInscripcion", qActividadTarifa.diasRetrasoInscripcion);
        relations.put("diasRetrasoValidacion", qActividadTarifa.diasRetrasoValidacion);
        relations.put("fechaInicio", qActividadTarifa.fechaInicio);
        relations.put("fechaFin", qActividadTarifa.fechaFin);
    }


    public List<ActividadTarifaDTO> getTarifasByTarifaTipo(Long tarifaId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qActividadTarifa).where(qActividadTarifa.tipoDTO.id.eq(tarifaId).and(qActividadTarifa.periodoDTO.isNull()));

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qActividadTarifa);
    }
}
