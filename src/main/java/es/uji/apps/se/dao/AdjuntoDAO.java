package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.QAdjuntoDTO;
import es.uji.apps.se.model.Adjunto;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.QAdjunto;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AdjuntoDAO extends BaseDAODatabaseImpl {
    private QAdjuntoDTO qAdjuntoDTO = QAdjuntoDTO.adjuntoDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public AdjuntoDAO() {
        relations.put("id", qAdjuntoDTO.id);
        relations.put("nombre", qAdjuntoDTO.nombre);
        relations.put("referencia", qAdjuntoDTO.referencia);
    }


    public List<Adjunto> getAdjuntosByEnvioId(Long envioId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qAdjuntoDTO).where(qAdjuntoDTO.envioDTO.id.eq(envioId));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(new QAdjunto(qAdjuntoDTO.id, qAdjuntoDTO.nombre, qAdjuntoDTO.referencia));
    }

    public Adjunto getAdjuntoById(Long adjuntoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAdjuntoDTO).where(qAdjuntoDTO.id.eq(adjuntoId));
        if (query.list(qAdjuntoDTO).isEmpty())
            return null;
        return query.list(new QAdjunto(qAdjuntoDTO.id, qAdjuntoDTO.nombre, qAdjuntoDTO.referencia)).get(0);
    }

    public List<Adjunto> getAdjuntosByReferencia(String referencia) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAdjuntoDTO).where(qAdjuntoDTO.referencia.eq(referencia));
        if (query.list(qAdjuntoDTO).isEmpty())
            return null;
        return query.list(new QAdjunto(qAdjuntoDTO.id, qAdjuntoDTO.nombre, qAdjuntoDTO.referencia));
    }
}
