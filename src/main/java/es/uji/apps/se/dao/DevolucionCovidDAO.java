package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QDevolucionCovidDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class DevolucionCovidDAO extends BaseDAODatabaseImpl {
    public Boolean getDevolucionUsuario(Long connectedUserId, Long ofertaId) {

        QDevolucionCovidDTO qDevolucionCovid = QDevolucionCovidDTO.devolucionCovidDTO;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qDevolucionCovid).where(qDevolucionCovid.ofertaDTO.id.eq(ofertaId).and(qDevolucionCovid.personaDTO.id.eq(connectedUserId)));
        if (query.list(qDevolucionCovid).size()>0) return Boolean.TRUE;
        return Boolean.FALSE;
    }
}
