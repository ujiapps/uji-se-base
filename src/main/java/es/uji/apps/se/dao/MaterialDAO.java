package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.MaterialDTO;
import es.uji.apps.se.dto.QMaterialDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MaterialDAO extends BaseDAODatabaseImpl {
    public List<MaterialDTO> getMateriales() {

        QMaterialDTO qMaterial = QMaterialDTO.materialDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMaterial).orderBy(qMaterial.nombre.asc());

        return query.list(qMaterial);
    }
}
