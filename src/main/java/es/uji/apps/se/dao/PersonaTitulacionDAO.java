package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.PersonaTitulacionDTO;
import es.uji.apps.se.dto.QPersonaTitulacionDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonaTitulacionDAO extends BaseDAODatabaseImpl {
    public List<PersonaTitulacionDTO> getTitulacionesElite(Long personaId, Long cursoAcademico) {

        QPersonaTitulacionDTO qPersonaTitulacion = QPersonaTitulacionDTO.personaTitulacionDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qPersonaTitulacion).where(qPersonaTitulacion.personaUjiDTO.id.eq(personaId).and(qPersonaTitulacion.cursoAcademico.eq(cursoAcademico)));

        return query.list(qPersonaTitulacion);
    }
}
