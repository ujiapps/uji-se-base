package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;
import es.uji.apps.se.dto.AccesoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.QAccesoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AccesoDAO extends BaseDAODatabaseImpl {

    private final QAccesoDTO qAcceso = QAccesoDTO.accesoDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public AccesoDAO() {
        relations.put("nombre", qAcceso.nombre);
        relations.put("zona", qAcceso.zona);
        relations.put("dia", qAcceso.dia);
        relations.put("destino", qAcceso.destino);
    }

    public String limpiaAcentos(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim().toUpperCase();
    }

    private StringExpression limpiaAcentos(StringPath nombre) {
        return Expressions.stringTemplate(
                "upper(translate({0}, 'âàãáÁÂÀÃéèêÉÈÊíÍóôõòÓÔÕÒüúÜÚ', 'AAAAAAAAEEEEEEIIOOOOOOOOUUUU'))",
                nombre);
    }

    public List<AccesoDTO> getAccesos(Paginacion paginacion, String busqueda, Long zonaId, Date fechaDesde, Date fechaHasta) {

        JPAQuery query = new JPAQuery(entityManager);
        query.join(qAcceso);

        if (ParamUtils.isNotNull(busqueda)) {
            query.where(limpiaAcentos(qAcceso.nombre).like('%' + limpiaAcentos(busqueda) + '%')
                    .or(limpiaAcentos(qAcceso.identificacion).like('%' + limpiaAcentos(busqueda) + '%')));
        }

        if (ParamUtils.isNotNull(zonaId)) {
            query.where(qAcceso.zonaId.eq(zonaId));
        }

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qAcceso.dia.goe(fechaDesde));
        }

        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qAcceso.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                query.orderBy(qAcceso.horaEntrada.desc());
            }
        }

        return query.list(qAcceso);
    }

    public List<AccesoDTO> getAccesosByPersona(Paginacion paginacion, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.join(qAcceso).where(qAcceso.personaId.eq(personaId));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                query.orderBy(qAcceso.dia.desc()).orderBy(qAcceso.horaEntrada.desc());
            }
        }

        return query.list(qAcceso);
    }

    public List<AccesoDTO> getAccesosCsvBySearch(String searchTexto, Long searchZona, Date fechaDesde, Date fechaHasta) {
        JPAQuery query = new JPAQuery(entityManager);
        query.join(qAcceso);

        if (ParamUtils.isNotNull(searchTexto)) {
            query.where(limpiaAcentos(qAcceso.nombre).like('%' + limpiaAcentos(searchTexto) + '%')
                    .or(limpiaAcentos(qAcceso.identificacion).like('%' + limpiaAcentos(searchTexto) + '%')));
        }

        if (ParamUtils.isNotNull(searchZona)) {
            query.where(qAcceso.zonaId.eq(searchZona));
        }

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qAcceso.dia.goe(fechaDesde));
        }

        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qAcceso.dia.loe(fechaHasta));
        }

        return query.list(qAcceso);
    }
}
