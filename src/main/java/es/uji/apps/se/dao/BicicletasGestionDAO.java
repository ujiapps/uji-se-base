package es.uji.apps.se.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.BicicletasDisponiblesTexto;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;

@Repository
public class BicicletasGestionDAO extends BaseDAODatabaseImpl {
    private QBicicletaTiposReservasDTO qBicicletaTiposReservasDTO = QBicicletaTiposReservasDTO.bicicletaTiposReservasDTO;
    private QBicicletasReservasDTO qBicicletasReservasDTO = QBicicletasReservasDTO.bicicletasReservasDTO;
    private QBicicletasDTO qBicicletasDTO = QBicicletasDTO.bicicletasDTO;

    public BicicletaTiposReservasDTO getBicicletaTipoReserva(Long reservaId) {
        return new JPAQuery(entityManager)
                .from(qBicicletaTiposReservasDTO)
                .where(qBicicletaTiposReservasDTO.id.eq(new JPAQuery(entityManager).from(qBicicletasReservasDTO)
                        .where(qBicicletasReservasDTO.id.eq(reservaId))
                        .singleResult(qBicicletasReservasDTO.tipoReservaId)
                )).singleResult(qBicicletaTiposReservasDTO);
    }

    @Transactional
    public void darBicicleta(Long reservaId, Date fechaPrevista) {
        new JPAUpdateClause(entityManager, qBicicletasReservasDTO)
                .set(qBicicletasReservasDTO.fechaIni, new Date())
                .set(qBicicletasReservasDTO.fechaPrevDevol, fechaPrevista)
                .where(qBicicletasReservasDTO.id.eq(reservaId))
                .execute();
    }

    @Transactional
    public void cambiarBicicleta(Long bicicletaAntigua, Long reservaId) {
        Long bicicleta;
        new JPAUpdateClause(entityManager, qBicicletasDTO)
                .set(qBicicletasDTO.estadoId, 9560264L)
                .set(qBicicletasDTO.fechaBaja, new Date())
                .where(qBicicletasDTO.id.eq(bicicletaAntigua))
                .execute();

        bicicleta = new JPAQuery(entityManager)
                        .from(qBicicletasDTO)
                        .where(qBicicletasDTO.estadoId.eq(9560263L)
                        .and(new JPASubQuery()
                                .from(qBicicletasReservasDTO)
                                .where(qBicicletasReservasDTO.bicicletaId.eq(qBicicletasDTO.id)
                                .and(qBicicletasReservasDTO.fechaFin.isNull()))
                                .notExists()))
                        .singleResult(qBicicletasDTO.id.max());

        new JPAUpdateClause(entityManager, qBicicletasReservasDTO)
                .set(qBicicletasReservasDTO.bicicletaId, bicicleta)
                .where(qBicicletasReservasDTO.id.eq(reservaId))
                .execute();
    }

    public BicicletasReservasDTO getBicicletasReservas(Long reservaId) {
        return new JPAQuery(entityManager)
                .from(qBicicletasReservasDTO)
                .where(qBicicletasReservasDTO.id.eq(reservaId))
                .singleResult(qBicicletasReservasDTO);
    }

    @Transactional
    public void devolverBicicleta(Long reservaId, Long bicicleta, Long estado) {
        new JPAUpdateClause(entityManager, qBicicletasReservasDTO).set(qBicicletasReservasDTO.fechaFin, new Date()).where(qBicicletasReservasDTO.id.eq(reservaId)).execute();
        new JPAUpdateClause(entityManager, qBicicletasDTO).set(qBicicletasDTO.estadoId, estado).where(qBicicletasDTO.id.eq(bicicleta)).execute();
    }

    public BicicletasDisponiblesTexto getBicicletasDisponiblesTexto() {
        String nativeSql = "select count(*), decode(count(*), 1, 'bicicleta disponible', 'bicicletes disponibles') " +
                                "from se_bicicletas b " +
                                "where b.estado_id = 9560263 " +
                                "and not exists (" +
                                    "select * " +
                                        "from se_bic_reservas " +
                                        "where bicicleta_id = b.id and fecha_fin is null)";
        Query query = entityManager.createNativeQuery(nativeSql);
        Object[] a = (Object[]) query.getSingleResult();

        BicicletasDisponiblesTexto disponiblesTexto = new BicicletasDisponiblesTexto();

        disponiblesTexto.setTexto(a[1].toString());
        if (a[0].toString() == null)    disponiblesTexto.setNumBicis(null);
        else                            disponiblesTexto.setNumBicis(((BigDecimal) a[0]).longValue());
        return  disponiblesTexto;
    }

    public Long getNumeroReservasActivas(Long perId) {
        return new JPAQuery(entityManager)
                .from(qBicicletasReservasDTO)
                .where(qBicicletasReservasDTO.perId.eq(perId)
                        .and(qBicicletasReservasDTO.fechaFin.isNull()))
                .count();
    }
}
