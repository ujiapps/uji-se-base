package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QPoblacioComboDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupFilterDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("poblacioComboDAO")
public class PoblacioComboDAO extends BaseDAODatabaseImpl implements LookupFilterDAO<LookupItem> {

    private QPoblacioComboDTO qPoblacioComboDTO = QPoblacioComboDTO.poblacioComboDTO;

    public List<LookupItem> search(String s, Map<String, String> map) {

        String provinciaId = map.get("provinciaId");

        JPAQuery query = new JPAQuery(entityManager).from(qPoblacioComboDTO);

        if (provinciaId != null) {
            query.where(qPoblacioComboDTO.provinciaId.equalsIgnoreCase(provinciaId));
        }

        if (!s.equals("")) {
            try {
                query.where(qPoblacioComboDTO.id.eq(Long.parseLong(s)));

            } catch (NumberFormatException e) {
                query.where((Utils.limpia(qPoblacioComboDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s))));
            }
        }

        return query.list(qPoblacioComboDTO.id,
                        qPoblacioComboDTO.nombre).stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(String.valueOf(tuple.get(qPoblacioComboDTO.id)));
                    item.setNombre(tuple.get(qPoblacioComboDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }

}
