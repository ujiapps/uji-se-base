package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.ModalidadDeportivaDTO;
import es.uji.apps.se.dto.QModalidadDeportivaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModalidadDeportivaDAO extends BaseDAODatabaseImpl {
    private QModalidadDeportivaDTO qModalidadDeportivaDTO = QModalidadDeportivaDTO.modalidadDeportivaDTO;

    public List<ModalidadDeportivaDTO> getModalidadesDeportivas() {
        return new JPAQuery(entityManager)
                .from(qModalidadDeportivaDTO)
                .list(qModalidadDeportivaDTO);
    }
}
