package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.ParametroDTO;
import es.uji.apps.se.dto.QParametroDTO;
import es.uji.apps.se.dto.QPersonaUjiDTO;
import es.uji.apps.se.dto.QPersonaVinculoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ParametroDAO extends BaseDAODatabaseImpl {

    private final QParametroDTO qParametro = QParametroDTO.parametroDTO;

    public List<ParametroDTO> getParametrosGlobales(Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qParametro).where(qParametro.persona.isNull());

        paginacion.setTotalCount(query.count());

        query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        query.orderBy(ordenaQuery(paginacion));

        return query.list(qParametro);
    }

    public List<ParametroDTO> getParametrosPersonales(Long connectedUserId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qParametro).where(qParametro.persona.id.eq(connectedUserId));

        paginacion.setTotalCount(query.count());

        query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        query.orderBy(ordenaQuery(paginacion));

        return query.list(qParametro);
    }

    private OrderSpecifier<?> ordenaQuery(Paginacion paginacion) {
        if (ParamUtils.isNotNull(paginacion.getOrdenarPor())) {
            if (paginacion.getOrdenarPor().equals("nombre")) {
                return paginacion.getDireccion().equals("DESC") ? qParametro.nombre.desc() : qParametro.nombre.asc();
            }
        }
        return qParametro.nombre.asc();
    }

    public ParametroDTO getParametroGlobal(String nombre) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qParametro).where(qParametro.nombre.upper().eq(nombre.toUpperCase()));

        List<ParametroDTO> lista = query.list(qParametro);
        if (!lista.isEmpty()) return lista.get(0);

        ParametroDTO parametroVacioDTO = new ParametroDTO();
        parametroVacioDTO.setNombre(nombre);
        return parametroVacioDTO;
    }

    public ParametroDTO getParametro(String nombre, Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);

        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
        QPersonaVinculoDTO qPersonaVinculo = QPersonaVinculoDTO.personaVinculoDTO;

        query.from(qParametro)
                .join(qParametro.persona, qPersonaUji).fetch()
                .join(qPersonaUji.personaVinculoDTO, qPersonaVinculo).fetch()
                .where(qParametro.nombre.upper().eq(nombre.toUpperCase())
                        .and(qPersonaUji.id.eq(personaId)));

        List<ParametroDTO> lista = query.list(qParametro);
        if (!lista.isEmpty()) return lista.get(0);
        return new ParametroDTO();
    }

    public List<ParametroDTO> getParametros(String nombre, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qParametro).where(qParametro.nombre.upper().eq(nombre.toUpperCase()).and(qParametro.persona.id.eq(personaId)));
        return query.list(qParametro);
    }
}
