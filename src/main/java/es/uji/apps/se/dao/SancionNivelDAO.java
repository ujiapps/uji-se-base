package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QSancionVistaDTO;
import es.uji.apps.se.dto.QTipoDTO;
import es.uji.apps.se.model.NivelSancion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository("SancionNivelDAO")
public class SancionNivelDAO extends BaseDAODatabaseImpl {

    private QTipoDTO qTipoDTO = QTipoDTO.tipoDTO;
    private QSancionVistaDTO qSancionVistaDTO = QSancionVistaDTO.sancionVistaDTO;

    public SancionNivelDAO() {}

    public List<NivelSancion> getNiveles() {
        return new JPAQuery(entityManager)
                    .from(qTipoDTO)
                    .join(qSancionVistaDTO).on(qSancionVistaDTO.nivel.id.eq(qTipoDTO.id))
                    .distinct()
                    .orderBy(qTipoDTO.nombre.asc())
                    .list(qTipoDTO.id, qTipoDTO.nombre)
                    .stream()
                    .map(tuple -> new NivelSancion(tuple.get(qTipoDTO.id), tuple.get(qTipoDTO.nombre)))
                    .collect(Collectors.toList());
    }
}
