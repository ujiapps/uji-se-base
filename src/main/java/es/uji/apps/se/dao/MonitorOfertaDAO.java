package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.MonitorOfertaDTO;
import es.uji.apps.se.dto.OfertaDTO;
import es.uji.apps.se.dto.QMonitorOfertaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MonitorOfertaDAO extends BaseDAODatabaseImpl {
    public List<MonitorOfertaDTO> getMonitoresActivosByOferta(OfertaDTO ofertaDTO) {

        QMonitorOfertaDTO qMonitorOferta = QMonitorOfertaDTO.monitorOfertaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMonitorOferta)
                .where(qMonitorOferta.oferta.id.eq(ofertaDTO.getId())
                        .and(qMonitorOferta.fechaBaja.isNull()));
        return query.list(qMonitorOferta);
    }
}
