package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.QEquipacionesInscripcionesDTO;
import es.uji.apps.se.model.EquipacionInscripcion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("equipacionesInscripcionesDAO")
public class EquipacionesInscripcionesDAO extends BaseDAODatabaseImpl {
    private QEquipacionesInscripcionesDTO qEquipacionesInscripcionesDTO = QEquipacionesInscripcionesDTO.equipacionesInscripcionesDTO;

    @Transactional
    public void updateEquipacionesInscripciones(EquipacionInscripcion equipacionInscripcion) {
        new JPAUpdateClause(entityManager, qEquipacionesInscripcionesDTO)
                .set(qEquipacionesInscripcionesDTO.equipacionId, equipacionInscripcion.getEquipacionId())
                .set(qEquipacionesInscripcionesDTO.inscripcionId, equipacionInscripcion.getInscripcionId())
                .set(qEquipacionesInscripcionesDTO.dorsal, equipacionInscripcion.getDorsal())
                .set(qEquipacionesInscripcionesDTO.fechaDev, equipacionInscripcion.getFechaDev())
                .set(qEquipacionesInscripcionesDTO.fecha, equipacionInscripcion.getFecha())
                .set(qEquipacionesInscripcionesDTO.talla, equipacionInscripcion.getTalla())
                .set(qEquipacionesInscripcionesDTO.tipoDev, equipacionInscripcion.getTipoDev())
                .set(qEquipacionesInscripcionesDTO.observacion, equipacionInscripcion.getObservacion())
                .where(qEquipacionesInscripcionesDTO.id.eq(equipacionInscripcion.getEquipacionInscripcion()))
                .execute();
    }

    @Transactional
    public void deleteEquipacionesInscripciones(Long id) {
        new JPADeleteClause(entityManager, qEquipacionesInscripcionesDTO)
                .where(qEquipacionesInscripcionesDTO.id.eq(id)).execute();
    }
}
