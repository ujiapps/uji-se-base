package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.ModalidadDeportiva;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EliteDAO extends BaseDAODatabaseImpl {

    private QEliteDTO qElite = QEliteDTO.eliteDTO;
    private QModalidadDeportivaDTO qModalidadDeportivaDTO = QModalidadDeportivaDTO.modalidadDeportivaDTO;

    public List<EliteDTO> getListadoDeportistasElite(CursoAcademico cursoAcademico) {

        JPAQuery query = new JPAQuery(entityManager);
        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
        QCriterioAdmisionDTO qCriterioAdmision = QCriterioAdmisionDTO.criterioAdmisionDTO;
        QPersonaTitulacionDTO qPersonaTitulacion = QPersonaTitulacionDTO.personaTitulacionDTO;

        query.from(qElite).join(qElite.personaUjiDTO, qPersonaUji).fetch()
                .leftJoin(qElite.criterioAdmisionDTO, qCriterioAdmision).fetch()
                .leftJoin(qPersonaUji.titulaciones, qPersonaTitulacion).fetch()
                .where(qElite.cursoAcademicoDTO.id.eq(cursoAcademico.getId())
                        .and(qPersonaTitulacion.cursoAcademico.eq(cursoAcademico.getId())))
                .distinct();
        return query.list(qElite);

    }

    public List<EliteMatriculaDTO> getListadoDeportistasEliteMatricula(CursoAcademico cursoAcademico) {
        JPAQuery query = new JPAQuery(entityManager);
        QEliteMatriculaDTO qEliteMatricula = QEliteMatriculaDTO.eliteMatriculaDTO;

        query.from(qEliteMatricula)
                .where(qEliteMatricula.cursoAca.eq(cursoAcademico.getId()));
        return query.list(qEliteMatricula);
    }

    public List<UIEntity> getListadoProfesoresConAlumnosElite(CursoAcademico cursoAcademico) {

        JPAQuery query = new JPAQuery(entityManager);

        QProfesorEliteDTO qProfesorElite = QProfesorEliteDTO.profesorEliteDTO;

        query.from(qProfesorElite).where(qProfesorElite.cursoAcademico.eq(cursoAcademico.getId()))
                .groupBy(qProfesorElite.identificacion, qProfesorElite.cursoAcademico, qProfesorElite.apellidosNombre, qProfesorElite.personaId);

        return query.list(qProfesorElite.identificacion, qProfesorElite.apellidosNombre, qProfesorElite.personaId,
                qProfesorElite.asignatura.countDistinct(), qProfesorElite.alumnos.countDistinct(), qProfesorElite.avisos.sum(), qProfesorElite.alumnos.count()).stream().map(profesor -> {
            UIEntity ui = new UIEntity();
            ui.put("id", profesor.get(qProfesorElite.personaId));
            ui.put("identificacion", profesor.get(qProfesorElite.identificacion));
            ui.put("apellidosNombre", profesor.get(qProfesorElite.apellidosNombre));
            ui.put("personaId", profesor.get(qProfesorElite.personaId));
            ui.put("numAsignaturas", profesor.get(qProfesorElite.asignatura.countDistinct()));
            ui.put("numAlumnos", profesor.get(qProfesorElite.alumnos.countDistinct()));

            if (profesor.get(qProfesorElite.avisos.sum()) > 0) {
                if (profesor.get(qProfesorElite.avisos.sum()).equals(profesor.get(qProfesorElite.alumnos.count()))) {
                    ui.put("enviado", "COMPLETO");
                } else {
                    ui.put("enviado", "FALTAN");
                }
            } else {
                ui.put("enviado", "VACIO");

            }

            return ui;
        }).collect(Collectors.toList());
    }

    public List<UIEntity> getListadoProfesoresConAlumnosEliteFaltanEnvios(ParametroDTO cursoAcademico) {
        JPAQuery query = new JPAQuery(entityManager);

        QProfesorEliteDTO qProfesorElite = QProfesorEliteDTO.profesorEliteDTO;

        query.from(qProfesorElite).where(qProfesorElite.cursoAcademico.eq(ParamUtils.parseLong(cursoAcademico.getValor())))
                .groupBy(qProfesorElite.identificacion, qProfesorElite.cursoAcademico, qProfesorElite.apellidosNombre, qProfesorElite.correo, qProfesorElite.personaId)
                .having(qProfesorElite.avisos.sum().lt(qProfesorElite.alumnos.count()));
        return query.list(qProfesorElite.identificacion, qProfesorElite.apellidosNombre, qProfesorElite.personaId, qProfesorElite.correo,
                qProfesorElite.alumnos.count(), qProfesorElite.avisos.sum()).stream().map(profesor -> {
            UIEntity ui = new UIEntity();
            ui.put("id", profesor.get(qProfesorElite.personaId));
            ui.put("correo", profesor.get(qProfesorElite.correo));
            return ui;
        })
                .distinct().collect(Collectors.toList());

    }

    public List<AlumnoAsignaturaDTO> getListadoAlumnosElitePorProfesor(CursoAcademico cursoAcademico, Long profesorId) {

        JPAQuery query = new JPAQuery(entityManager);

        QAlumnoAsignaturaDTO alumnoAsignatura = QAlumnoAsignaturaDTO.alumnoAsignaturaDTO;

        query.from(alumnoAsignatura)
                .where(alumnoAsignatura.cursoAcademico.eq(cursoAcademico.getId())
                        .and(alumnoAsignatura.personaId.eq(profesorId)));

        return query.list(alumnoAsignatura);

    }

    public List<DecanoEliteDTO> getListadoDecanosConAlumnosElite(CursoAcademico cursoAcademico) {

        JPAQuery query = new JPAQuery(entityManager);

        QDecanoEliteDTO qDecanoElite = QDecanoEliteDTO.decanoEliteDTO;

        query.from(qDecanoElite).where(qDecanoElite.cursoAcademico.eq(cursoAcademico.getId()));

        return query.list(qDecanoElite);
    }

    public List<DecanoEliteDTO> getListadoDecanosConAlumnosEliteFaltaEnvio(CursoAcademico cursoAcademico) {
        JPAQuery query = new JPAQuery(entityManager);

        QDecanoEliteDTO qDecanoElite = QDecanoEliteDTO.decanoEliteDTO;

        query.from(qDecanoElite).where(qDecanoElite.fechaEnvio.isNull().and(qDecanoElite.cursoAcademico.eq(cursoAcademico.getId()))).distinct();

        return query.list(qDecanoElite);
    }

    public EliteDTO getEliteById(Long eliteId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qElite).where(qElite.id.eq(eliteId));
        return query.uniqueResult(qElite);

    }

    public EliteDTO getDeporistaEliteByPersonaAndCurso(Long personaId, CursoAcademicoDTO cursoAcademicoDTO) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qElite).where(qElite.personaUjiDTO.id.eq(personaId).and(qElite.cursoAcademicoDTO.id.eq(cursoAcademicoDTO.getId())));
        return query.uniqueResult(qElite);
    }

    public List<ModalidadDeportiva> getEliteByPersona(Long personaId) {
        return new JPAQuery(entityManager)
                .from(qElite)
                .leftJoin(qModalidadDeportivaDTO).on(qElite.modalidadDeportivaDTO.id.eq(qModalidadDeportivaDTO.id))
                .where(qElite.personaUjiDTO.id.eq(personaId))
                .list(qElite.id, qElite.personaUjiDTO.id, qElite.cursoAcademicoDTO.id, qElite.modalidadDeportivaDTO.id, qModalidadDeportivaDTO.nombre)
                .stream().map(tuple -> new ModalidadDeportiva(
                        tuple.get(qElite.id),
                        tuple.get(qElite.personaUjiDTO.id),
                        tuple.get(qElite.cursoAcademicoDTO.id),
                        tuple.get(qElite.modalidadDeportivaDTO.id),
                        tuple.get(qModalidadDeportivaDTO.nombre)
                )).collect(Collectors.toList());
    }

    @Transactional
    public void updateElitePersona(Long id, Long cursoAca, Long modalidadNombre) {
        new JPAUpdateClause(entityManager, qElite)
                .set(qElite.cursoAcademicoDTO.id, cursoAca)
                .set(qElite.modalidadDeportivaDTO.id, modalidadNombre)
                .where(qElite.id.eq(id))
                .execute();
    }

    @Transactional
    public void deleteElitePersona(Long id) {
        new JPADeleteClause(entityManager, qElite)
                .where(qElite.id.eq(id))
                .execute();
    }
}
