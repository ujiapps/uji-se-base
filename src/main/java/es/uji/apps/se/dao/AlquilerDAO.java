package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QAlquilerDTO;
import es.uji.apps.se.dto.QAlquilerOfertaDTO;
import es.uji.apps.se.model.Alquiler;
import es.uji.apps.se.model.CursoAcademico;
import es.uji.apps.se.model.QAlquiler;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AlquilerDAO extends BaseDAODatabaseImpl {
    public List<Alquiler> getAlquileres(CursoAcademico cursoAcademico) {

        QAlquilerDTO qAlquilerDTO = QAlquilerDTO.alquilerDTO;
        QAlquilerOfertaDTO qAlquilerOfertaDTO = QAlquilerOfertaDTO.alquilerOfertaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qAlquilerDTO).join(qAlquilerDTO.ofertasDTO, qAlquilerOfertaDTO)
                .where(qAlquilerOfertaDTO.cursoAcademico.eq(cursoAcademico.getId()))
                .orderBy(qAlquilerDTO.contacto.asc()).orderBy(qAlquilerDTO.nombre.asc()).distinct().list(new QAlquiler(qAlquilerDTO.id, qAlquilerDTO.nombre, qAlquilerDTO.comentarioAgenda, qAlquilerDTO.codigoLlavero,
                qAlquilerDTO.mail, qAlquilerDTO.telefono, qAlquilerDTO.contacto));
    }
}
