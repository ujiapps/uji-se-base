package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.ActividadAsistenciaDTO;
import es.uji.apps.se.dto.QActividadAsistenciaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class ActividadAsistenciaDAO extends BaseDAODatabaseImpl {
    public ActividadAsistenciaDTO getAsistenciaByInscripcionYPartido(Long inscripcionId, Long partidoId) {
        QActividadAsistenciaDTO qActividadAsistenciaDTO = QActividadAsistenciaDTO.actividadAsistenciaDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActividadAsistenciaDTO).where(qActividadAsistenciaDTO.partido.id.eq(partidoId).and(qActividadAsistenciaDTO.inscripcion.id.eq(inscripcionId)));
        if (query.list(qActividadAsistenciaDTO).size() > 0)
            return query.list(qActividadAsistenciaDTO).get(0);
        else return null;
    }
}
