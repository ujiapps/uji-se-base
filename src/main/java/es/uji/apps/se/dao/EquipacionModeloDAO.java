package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QEquipacionModeloDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.xml.lookup.LookupItem;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.stream.Collectors;

@Repository("equipacionModeloDAO")
public class EquipacionModeloDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private QEquipacionModeloDTO qEquipacionModeloDTO = QEquipacionModeloDTO.equipacionModeloDTO;

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qEquipacionModeloDTO)
                            .orderBy(qEquipacionModeloDTO.nombre.asc());

        if(!s.equals("")) {
            try {
                query.where(qEquipacionModeloDTO.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qEquipacionModeloDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query
                .list(qEquipacionModeloDTO.id,
                      qEquipacionModeloDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qEquipacionModeloDTO.id).toString());
                    item.setNombre(tuple.get(qEquipacionModeloDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }
}
