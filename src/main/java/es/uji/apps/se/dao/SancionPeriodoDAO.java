package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.SancionPeriodoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.QSancionPeriodoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SancionPeriodoDAO extends BaseDAODatabaseImpl {

    private QSancionPeriodoDTO qSancionPeriodo = QSancionPeriodoDTO.sancionPeriodoDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public SancionPeriodoDAO() {
        relations.put("id", qSancionPeriodo.id);
        relations.put("diaInicio", qSancionPeriodo.diaInicio);
        relations.put("diaFin", qSancionPeriodo.diaFin);
        relations.put("mesInicio", qSancionPeriodo.mesInicio);
        relations.put("mesFin", qSancionPeriodo.mesFin);
    }

    public List<SancionPeriodoDTO> getPeriodos(Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSancionPeriodo);

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qSancionPeriodo);
    }

    public SancionPeriodoDTO getSancionPeriodoById(Long sancionPeriodoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSancionPeriodo).where(qSancionPeriodo.id.eq(sancionPeriodoId));

        return query.uniqueResult(qSancionPeriodo);
    }
}
