package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.PersonaIbanDTO;
import es.uji.apps.se.dto.QPersonaIbanDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonaIbanDAO extends BaseDAODatabaseImpl {
    public List<PersonaIbanDTO> getListaPersonaIbanByPersonaId(Long personaId) {

        QPersonaIbanDTO qPersonaIban = QPersonaIbanDTO.personaIbanDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPersonaIban).where(qPersonaIban.persona.eq(personaId));
        return query.list(qPersonaIban);
    }
}
