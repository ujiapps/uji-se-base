package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.AsistenciaCursos;
import es.uji.apps.se.model.ListadoFaltasCursos;
import es.uji.apps.se.model.QAsistenciaCursos;
import es.uji.apps.se.model.QListadoFaltasCursos;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class AsistenciaCursosDAO extends BaseDAODatabaseImpl {
    private QCursosInscritosDTO qCursosInscritosDTO = QCursosInscritosDTO.cursosInscritosDTO;
    private QCursosDTO qCursosDTO = QCursosDTO.cursosDTO;
    private QCursoAsistenciaDTO qCursoAsistenciaDTO = QCursoAsistenciaDTO.cursoAsistenciaDTO;
    private QCalendarioCursosDTO qCalendarioCursosDTO = QCalendarioCursosDTO.calendarioCursosDTO;
    private QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;
    private QListadoFaltasCursosDTO qListadoFaltasCursosDTO = QListadoFaltasCursosDTO.listadoFaltasCursosDTO;

    public List<AsistenciaCursos> getAsistenciaCursos(Long perId) {
        return new JPAQuery(entityManager)
                .from(qCursosInscritosDTO, qCursosDTO, qCalendarioCursosDTO)
                .where(qCursosInscritosDTO.cursoAca.eq(qCursosDTO.id)
                        .and(qCalendarioCursosDTO.cursoId.eq(qCursosDTO.id))
                        .and(qCursosInscritosDTO.perId.eq(perId)))
                .groupBy(qCursosDTO.nombre, qCursosDTO.id, qCursosDTO.fechaIni)
                .list(new QAsistenciaCursos(qCursosDTO.nombre, qCursosDTO.id, qCursosDTO.fechaIni));
    }

    public List<ListadoFaltasCursos> getListadoFaltasCursos(Long perId, Long cursoId) {
        return new JPAQuery(entityManager)
                .from(qListadoFaltasCursosDTO)
                .where(qListadoFaltasCursosDTO.perId.eq(perId)
                        .and(qListadoFaltasCursosDTO.cursoId.eq(cursoId)))
                .list(new QListadoFaltasCursos(qListadoFaltasCursosDTO.id,
                        qListadoFaltasCursosDTO.perId,
                        qListadoFaltasCursosDTO.cursoId,
                        qListadoFaltasCursosDTO.dia,
                        qListadoFaltasCursosDTO.horaIni,
                        qListadoFaltasCursosDTO.horaFin,
                        qListadoFaltasCursosDTO.asisto));
    }

    public Long getDiasActiv(Long cursoId) {
        return new JPAQuery(entityManager)
                    .from(qCalendarioCursosDTO, qCalendarioDTO, qCursosDTO)
                    .where(qCalendarioCursosDTO.cursoId.eq(qCursosDTO.id)
                    .and(qCalendarioCursosDTO.calendarioId.eq(qCalendarioDTO.id)
                    .and(qCalendarioDTO.dia.lt(new Date())))
                    .and(qCursosDTO.id.eq(cursoId)))
                    .count();
    }

    public Long getDiasAsis(Long cursoId, Long personaId) {
        return new JPAQuery(entityManager)
                    .from(qCalendarioCursosDTO, qCursosInscritosDTO, qCalendarioDTO, qCursoAsistenciaDTO, qCursosDTO)
                    .where(qCalendarioCursosDTO.cursoId.eq(qCursosDTO.id)
                    .and(qCalendarioCursosDTO.calendarioId.eq(qCalendarioDTO.id)
                    .and(qCalendarioDTO.dia.lt(new Date()))
                    .and(qCursoAsistenciaDTO.calendarioCursoId.eq(qCalendarioCursosDTO.id))
                    .and(qCursoAsistenciaDTO.personaId.eq(personaId))
                    .and(qCursosInscritosDTO.perId.eq(qCursoAsistenciaDTO.personaId)
                    .and(qCursosInscritosDTO.cursoAca.eq(qCursosDTO.id))
                    .and(qCursosDTO.id.eq(cursoId)))))
                    .count();
    }
}