package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.*;

@Repository
public class PersonaDAO extends BaseDAODatabaseImpl {

    private final PersonaSancionDAO personaSancionDAO;
    private final QPersonaDTO qPersona = QPersonaDTO.personaDTO;
    private final QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
    private final QPersonaAdminDTO qPersonaAdmin = QPersonaAdminDTO.personaAdminDTO;
    private final QCarnetBonoTarifaDTO qCarnetBonoTarifa = QCarnetBonoTarifaDTO.carnetBonoTarifaDTO;
    private final QPerfilPersonaDTO qPerfilPersona = QPerfilPersonaDTO.perfilPersonaDTO;
    private final QVinculacionActivaVWDTO qVinculacionActivaVW = QVinculacionActivaVWDTO.vinculacionActivaVWDTO;
    private final QParametroDTO qParametro = QParametroDTO.parametroDTO;

    public PersonaDAO(PersonaSancionDAO personaSancionDAO) {
        this.personaSancionDAO = personaSancionDAO;
    }

    public PersonaDTO getPersonaById(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
        QSancionVistaDTO qSancion = QSancionVistaDTO.sancionVistaDTO;

        query.from(qPersona).join(qPersona.personaUjiDTO, qPersonaUji).fetch()
                .leftJoin(qPersonaUji.sanciones, qSancion).fetch()
                .where(qPersona.personaUjiDTO.id.eq(personaId));

        List<PersonaDTO> personasDTO = query.distinct().list(qPersona);
        if (personasDTO.isEmpty()) return null;
        return personasDTO.get(0);
    }

    public Boolean tienePerfil(Long conectedUserId, Long perfilBecario) {
        return new JPAQuery(entityManager)
                .from(qPerfilPersona)
                .where(qPerfilPersona.persona.eq(conectedUserId)
                        .and(qPerfilPersona.perfilDTO.id.eq(perfilBecario)))
                .count() != 0;
    }

    public Boolean isAdmin(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager)
                .from(qPersonaAdmin)
                .where(qPersonaAdmin.personaUjiDTO.id.eq(personaId)
                        .and((qPersonaAdmin.fechaBaja.isNull().or(qPersonaAdmin.fechaBaja.gt(new Date())))));

        return query.count() != 0;
    }

    public Long getAnyoNacimiento(Long personaId) {
        Date fechaNacimiento = new JPAQuery(entityManager)
                .from(qPersonaUji)
                .where(qPersonaUji.id.eq(personaId))
                .singleResult(qPersonaUji.fechaNacimiento.min());
        if (fechaNacimiento != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaNacimiento);
            return (long) calendar.get(Calendar.YEAR);
        } else
            return null;
    }

    public Boolean esMoroso(Long personaId) {
        List<Sancion> sanciones = personaSancionDAO.getSanciones(personaId, null);
        return sanciones.stream()
                .anyMatch(s -> (s.getFechaFin().after(new Date()) || !ParamUtils.isNotNull(s.getFechaFin()))
                        && s.getOrigen().equals("RECIBO"));
    }

    public Boolean permiteDomiciliar(Long personaId) {

        return !new JPAQuery(entityManager)
                .from(qParametro)
                .where(qParametro.nombre.eq("PERMITE-DOM")
                        .and(qParametro.valor.in(String.valueOf(new JPASubQuery()
                                .from(qVinculacionActivaVW)
                                .where(qVinculacionActivaVW.personaId.eq(personaId))
                                .distinct()
                                .list(qVinculacionActivaVW.vinculoId))))).list(qParametro).isEmpty();
    }

    @Transactional
    public Long dameImpTdybPer(Long personaId, Long tipoTarjetaBonoId, String tipo, String uso) {
        Long plazos = 0L;
        Long importeActual = -1L;

        List<Tuple> tarifas = new JPAQuery(entityManager)
                .from(qVinculacionActivaVW, qCarnetBonoTarifa)
                .where(qVinculacionActivaVW.vinculoId.eq(qCarnetBonoTarifa.vinculoId)
                        .and(qVinculacionActivaVW.personaId.eq(personaId)
                                .and(qCarnetBonoTarifa.tipoId.eq(tipoTarjetaBonoId))
                                .and(qVinculacionActivaVW.uso.eq(uso))))
                .list(qCarnetBonoTarifa.importe, qCarnetBonoTarifa.plazos);

        for (Tuple tupla : tarifas) {
            if (tupla.get(qCarnetBonoTarifa.importe) != null && (tupla.get(qCarnetBonoTarifa.importe) < importeActual) || importeActual == -1.0f) {
                importeActual = tupla.get(qCarnetBonoTarifa.importe);
                plazos = tupla.get(qCarnetBonoTarifa.plazos);
            }
        }

        if (tipo.equals("IMPORTE")) {
            return (importeActual >= 0) ? importeActual : -1L;
        } else {
            return (importeActual >= 0) ? plazos : 1L;
        }
    }

    public String getImporte(String xml, String tipo) {
        xml = xml.replace("'", "''");
        String sql = "select extractValue(xmltype('" + xml + "'), '/registro/" + tipo + "') from dual";
        Query query = entityManager.createNativeQuery(sql);
        return (String) query.getSingleResult();
    }

}
