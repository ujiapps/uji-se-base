package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.CalendarioClaseInstalacionDTO;
import es.uji.apps.se.dto.QCalendarioClaseInstalacionDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CalendarioClaseInstalacionDAO extends BaseDAODatabaseImpl {
    public List<CalendarioClaseInstalacionDTO> getCalendarioClaseInstalacionesByCalendarioClaseId(Long calendarioClaseId) {
        QCalendarioClaseInstalacionDTO qCalendarioClaseInstalacionDTO  = QCalendarioClaseInstalacionDTO.calendarioClaseInstalacionDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCalendarioClaseInstalacionDTO).where(qCalendarioClaseInstalacionDTO.calendarioClaseDTO.id.eq(calendarioClaseId));
        return query.list(qCalendarioClaseInstalacionDTO);


    }
}
