package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.domains.TipoEstadoInscripcion;
import es.uji.apps.se.model.domains.TipoOrigen;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OfertaDAO extends BaseDAODatabaseImpl {

    private QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;
    private QLineaReciboVWDTO qLineaReciboVW = QLineaReciboVWDTO.lineaReciboVWDTO;
    private QInscripcionDTO qInscripcion = QInscripcionDTO.inscripcionDTO;
    private QPeriodoDTO qPeriodo = QPeriodoDTO.periodoDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public OfertaDAO() {
        relations.put("id", qOferta.id);
        relations.put("nombre", qOferta.nombre);
        relations.put("periodoId", qOferta.periodoDTO.nombre);
    }

//    public List<Oferta> getOfertasByActividadAndPeriodo(Long actividadId, Long periodoId) {
//        JPAQuery query = new JPAQuery(entityManager);
//
//        QOferta oferta = QOferta.oferta;
//
//        query.from(oferta).where(oferta.actividad.id.eq(actividadId).and(oferta.periodo.id.eq(periodoId))).distinct();
//
//        return query.list(oferta);
//    }

    public List<OfertaDTO> getOfertasByPeriodo(Long periodoId) {
        JPAQuery query = new JPAQuery(entityManager);

        QPeriodoDTO qPeriodo = QPeriodoDTO.periodoDTO;

        query.from(qOferta)
                .join(qOferta.periodoDTO, qPeriodo).fetch()
                .where(qOferta.periodoDTO.id.eq(periodoId)).distinct();

        return query.list(qOferta);
    }

    public OfertaDTO getOfertaByActividadByNombreByPeriodo(Long actividadId, String ofertaNombre, Long periodoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qOferta)
                .where(qOferta.actividadDTO.id.eq(actividadId)
                        .and(qOferta.nombre.eq(ofertaNombre))
                        .and(qOferta.periodoDTO.id.eq(periodoId)));
        List<OfertaDTO> lista = query.list(qOferta);

        if (lista.size() > 0) return lista.get(0);
        return null;
    }

    public List<OfertaDTO> getOfertasByActividad(Long actividadId, Long cursoId, Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager);
        QPeriodoDTO qPeriodo = QPeriodoDTO.periodoDTO;

        query.from(qOferta).leftJoin(qOferta.periodoDTO, qPeriodo)
                .where(qOferta.actividadDTO.id.eq(actividadId)
                        .and(qPeriodo.cursoAcademicoDTO.id.eq(cursoId)));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qOferta);
    }

    public List<OfertaDTO> getOfertas() {
        JPAQuery query = new JPAQuery(entityManager);
        QActividadDTO qActividadDTO = QActividadDTO.actividadDTO;
        QPeriodoDTO qPeriodoDTO = QPeriodoDTO.periodoDTO;

        return query.from(qOferta).join(qOferta.actividadDTO, qActividadDTO).fetch()
                .join(qOferta.periodoDTO, qPeriodoDTO).fetch().list(qOferta);
    }

    public OfertaDTO getOfertaById(Long id) {
        if (id != null)
            return new JPAQuery(entityManager)
                    .from(qOferta)
                    .where(qOferta.id.eq(id))
                    .singleResult(qOferta);
        return null;
    }

    public List<OfertaDTO> getOfertasByCursoId(Long cursoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QActividadDTO qActividadDTO = QActividadDTO.actividadDTO;
        QPeriodoDTO qPeriodoDTO = QPeriodoDTO.periodoDTO;

        return query.from(qOferta).join(qOferta.actividadDTO, qActividadDTO).fetch()
                .join(qOferta.periodoDTO, qPeriodoDTO).fetch()
                .where(qPeriodoDTO.cursoAcademicoDTO.id.eq(cursoId))
                .list(qOferta);
    }
}
