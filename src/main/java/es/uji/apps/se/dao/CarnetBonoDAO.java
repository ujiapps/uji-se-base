package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.apps.se.utils.Utils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CarnetBonoDAO extends BaseDAODatabaseImpl {
    private final QCarnetBonoDetalleDTO qCarnetBonoDetalleDTO = QCarnetBonoDetalleDTO.carnetBonoDetalleDTO;
    private final QTipoDTO qTipoDTO = QTipoDTO.tipoDTO;
    private final QCarnetBonoDTO qCarnetBonoDTO = QCarnetBonoDTO.carnetBonoDTO;
    private final QCarnetBonoTipoDTO qCarnetBonoTipoDTO = QCarnetBonoTipoDTO.carnetBonoTipoDTO;
    private final QLineaReciboVWDTO qLineaReciboVWDTO = QLineaReciboVWDTO.lineaReciboVWDTO;
    private final QTipoHorarioCarnetBonoDTO qTipoHorarioCarnetBonoDTO = QTipoHorarioCarnetBonoDTO.tipoHorarioCarnetBonoDTO;
    private final QDiaSemanaVWDTO qDiaSemanaVWDTO = QDiaSemanaVWDTO.diaSemanaVWDTO;

    public CarnetBonoDAO() {}

    public List<DetalleTarjetaBono> getDetallesTarjetaBono(Long tarjetaBonoId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qCarnetBonoDetalleDTO)
                            .join(qTipoDTO).on(qCarnetBonoDetalleDTO.tipoId.eq(qTipoDTO.id))
                            .where(qCarnetBonoDetalleDTO.carnetBonoId.eq(tarjetaBonoId));

        if (paginacion != null) {

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if (atributoOrdenacion != null) {
                query.orderBy(ordenaDetalles(orden, atributoOrdenacion));
            } else
                query.orderBy(qCarnetBonoDetalleDTO.fechaEntrada.desc());
        }

        SimpleDateFormat hora = new SimpleDateFormat("HH:mm");

        return query.
                list(qCarnetBonoDetalleDTO.id,
                     qCarnetBonoDetalleDTO.fechaEntrada,
                     qCarnetBonoDetalleDTO.fechaSalida,
                     qCarnetBonoDetalleDTO.tipoId,
                     qTipoDTO.nombre)
                .stream()
                .map(tuple -> new DetalleTarjetaBono(tuple.get(qCarnetBonoDetalleDTO.id),
                                                     tuple.get(qCarnetBonoDetalleDTO.fechaEntrada),
                                                     hora.format(tuple.get(qCarnetBonoDetalleDTO.fechaEntrada)),
                                                     tuple.get(qCarnetBonoDetalleDTO.fechaSalida),
                                                     tuple.get(qCarnetBonoDetalleDTO.fechaSalida) != null ? hora.format(tuple.get(qCarnetBonoDetalleDTO.fechaSalida)) : null,
                                                     tuple.get(qCarnetBonoDetalleDTO.tipoId),
                                                     tuple.get(qTipoDTO.nombre)))
                .collect(Collectors.toList());
    }

    private OrderSpecifier<?> ordenaDetalles(String orden, String atributoOrdenacion) {

        if (atributoOrdenacion.equals("id")) {
            return orden.equals("ASC") ? qCarnetBonoDetalleDTO.id.asc() : qCarnetBonoDetalleDTO.id.desc();
        }
        if (atributoOrdenacion.equals("fechaEntrada")) {
            return orden.equals("ASC") ? qCarnetBonoDetalleDTO.fechaEntrada.asc() : qCarnetBonoDetalleDTO.fechaEntrada.desc();
        }
        if (atributoOrdenacion.equals("horaEntrada")) {
            return orden.equals("ASC") ? qCarnetBonoDetalleDTO.fechaEntrada.asc() : qCarnetBonoDetalleDTO.fechaEntrada.desc();
        }
        if (atributoOrdenacion.equals("fechaSalida")) {
            return orden.equals("ASC") ? qCarnetBonoDetalleDTO.fechaSalida.asc() : qCarnetBonoDetalleDTO.fechaSalida.desc();
        }
        if (atributoOrdenacion.equals("horaSalida")) {
            return orden.equals("ASC") ? qCarnetBonoDetalleDTO.fechaSalida.asc() : qCarnetBonoDetalleDTO.fechaSalida.desc();
        }
        if (atributoOrdenacion.equals("tipoNombre")) {
            return orden.equals("ASC") ? qCarnetBonoDetalleDTO.tipoId.asc() : qCarnetBonoDetalleDTO.tipoId.desc();
        }
        return qCarnetBonoDetalleDTO.fechaEntrada.desc();
    }

    public List<DetalleHistoricoTarjetaBono> getDetallesHistoricoTarjetaBono(Long tarjetaBonoId) {
        String sql = "select decode(xtipo_dml, 'I', 'Nou','U', 'Actualització','D','Eliminació') xtipo_dml, cb.fecha_ini, cb.fecha_fin, cb.observaciones, cb.fecha_baja, cb.xfecha, p.apellidos_nombre, cb.id " +
                     "from uji_zetadolar.z$_se_carnets_bonos cb, se_ext_personas p " +
                     "where cb.id = ?1 " +
                     "and cb.xper_id = p.id (+) " +
                     "order by xfecha";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, tarjetaBonoId);
        List<Object[]> resultado = query.getResultList();
        List<DetalleHistoricoTarjetaBono> detalles = new ArrayList<>();
        resultado.forEach(r -> detalles.add(new DetalleHistoricoTarjetaBono(
                Utils.convierteBigDecimalEnLong((BigDecimal) r[7]),
                (String) r[0],
                (Date) r[1],
                (Date) r[2],
                (Date) r[4],
                (Date) r[5],
                (String) r[6],
                (String) r[3]
        )));
        return detalles;
    }

    public List<TarjetaHorario> fichaTarjetaHorario(Long tarjetaBonoId) {
        Long tieneHorarioEspecial = new JPAQuery(entityManager)
                                        .from(qTipoHorarioCarnetBonoDTO)
                                        .where(qTipoHorarioCarnetBonoDTO.carnetBonoTipoId.eq(tarjetaBonoId))
                                        .count();

        if(tieneHorarioEspecial > 0) {
            return new JPAQuery(entityManager)
                       .from(qTipoHorarioCarnetBonoDTO, qDiaSemanaVWDTO)
                       .where(qTipoHorarioCarnetBonoDTO.diaSemana.eq(qDiaSemanaVWDTO.id)
                       .and(qTipoHorarioCarnetBonoDTO.carnetBonoTipoId.eq(tarjetaBonoId)))
                       .list(qDiaSemanaVWDTO.nombre,
                             qTipoHorarioCarnetBonoDTO.horaInicio,
                             qTipoHorarioCarnetBonoDTO.horaFin)
                       .stream()
                       .map(tuple -> new TarjetaHorario(tuple.get(qDiaSemanaVWDTO.nombre),
                                                        tuple.get(qTipoHorarioCarnetBonoDTO.horaInicio),
                                                        tuple.get(qTipoHorarioCarnetBonoDTO.horaFin)))
                       .collect(Collectors.toList());
        }

        return null;
    }

    @Transactional
    public void deleteDetalleTarjetaBono(Long detalleTarjetaBonoId) {
        new JPADeleteClause(entityManager, qCarnetBonoDetalleDTO)
            .where(qCarnetBonoDetalleDTO.id.eq(detalleTarjetaBonoId))
            .execute();
    }

    public Long getZona(Long tarjetaBonoId) {
        return new JPAQuery(entityManager)
                    .from(qCarnetBonoDTO)
                    .join(qCarnetBonoTipoDTO).on(qCarnetBonoTipoDTO.id.eq(qCarnetBonoDTO.tipo.id))
                    .where(qCarnetBonoDTO.id.eq(tarjetaBonoId))
                    .singleResult(qCarnetBonoTipoDTO.tipoInstalacionId.min());
    }

    public Long getReciboId(Long tarjetaBonoId) {
        return new JPAQuery(entityManager)
                .from(qLineaReciboVWDTO)
                .where(qLineaReciboVWDTO.tarjetaBonoId.eq(tarjetaBonoId)
                .and(qLineaReciboVWDTO.origen.in(new ArrayList<>(Arrays.asList("TD", "BO", "TB")))))
                .singleResult(qLineaReciboVWDTO.reciboId);
    }

    @Transactional
    public void updateDetalleTarjetaBono(DetalleTarjetaBono detalleTarjetaBono) {
        new JPAUpdateClause(entityManager, qCarnetBonoDetalleDTO)
            .set(qCarnetBonoDetalleDTO.fechaEntrada, detalleTarjetaBono.getFechaEntrada())
            .set(qCarnetBonoDetalleDTO.fechaSalida, detalleTarjetaBono.getFechaSalida())
            .set(qCarnetBonoDetalleDTO.tipoId, detalleTarjetaBono.getTipoId())
            .where(qCarnetBonoDetalleDTO.id.eq(detalleTarjetaBono.getId()))
            .execute();
    }

    @Transactional
    public void bajaTarjetaBono(Long tarjetaBonoId) {
        new JPADeleteClause(entityManager, qCarnetBonoDTO)
            .where(qCarnetBonoDTO.id.eq(tarjetaBonoId))
            .execute();
    }

    @Transactional
    public void addUso(Long tipoInstalacionId, Long tarjetaBonoId) {
        this.insert(new CarnetBonoDetalleDTO(tarjetaBonoId, tipoInstalacionId, new Date(), new Date()));
    }
}
