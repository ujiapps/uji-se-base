package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;

import es.uji.apps.se.dto.QGrupoDTO;
import es.uji.apps.se.dto.QTipoDTO;
import es.uji.apps.se.dto.TipoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.Tipo;
import es.uji.apps.se.model.domains.TipoTaxonomia;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("tipoDAO")
public class TipoDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {


    private QTipoDTO qTipo = QTipoDTO.tipoDTO;
    private QTipoDTO qTipoPadre = new QTipoDTO("tipoPadre");
    private QGrupoDTO qGrupo = QGrupoDTO.grupoDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public TipoDAO() {
        relations.put("id", qTipo.id);
        relations.put("nombre", qTipo.nombre);
    }

    public List<TipoDTO> getTiposClasificacion() {
        Long CLASIFICACION_ACTIVIDADES = 7329L;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTipo).where(qTipo.grupoDTO.id.eq(CLASIFICACION_ACTIVIDADES));
        return query.list(qTipo);
    }

    public List<Tipo> getTipos(Long clasificacionId) {
        return new JPAQuery(entityManager)
                   .from(qTipo)
                   .where(qTipo.tipoPadreDTO.id.eq(clasificacionId))
                   .orderBy(qTipo.nombre.asc())
                   .list(qTipo.id, qTipo.nombre)
                   .stream()
                   .map(tuple -> new Tipo(tuple.get(qTipo.id), tuple.get(qTipo.nombre)))
                   .collect(Collectors.toList());
    }

    public List<TipoDTO> getVinculosTarifas() {
        Long GRUPO_TARIFA = 7330L;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTipo).join(qTipo.tipoPadreDTO, qTipoPadre).where(qTipoPadre.grupoDTO.id.eq(GRUPO_TARIFA));
        return query.distinct().orderBy(qTipo.orden.asc()).list(qTipo);
    }

    public List<TipoDTO> getTiposTaxonomias(TipoTaxonomia tipoTaxonomia) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipo).join(qTipo.grupoDTO, qGrupo).where(qGrupo.procedimiento.eq(tipoTaxonomia.getNombre()));

        return query.list(qTipo);
    }

    public List<TipoDTO> getTiposByParentId(Long node) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipo).where(qTipo.tipoPadreDTO.id.eq(node));

        return query.list(qTipo);
    }

    public Long getPadreEtiqueta(Long grupoId, String valorParametroGlobal) {
        return new JPAQuery(entityManager)
                .from(qTipo)
                .where(qTipo.grupoDTO.id.eq(grupoId)
                        .and(qTipo.uso.eq(valorParametroGlobal)))
                .singleResult(qTipo.id);
    }

    public Boolean tieneHijos(Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipo).where(qTipo.tipoPadreDTO.id.eq(tipoId));
        if (query.list(qTipo).size() > 0)
            return Boolean.TRUE;
        return Boolean.FALSE;
    }

    public TipoDTO getTipoById(Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipo).where(qTipo.id.eq(tipoId));

        List<TipoDTO> tiposDTO = query.distinct().list(qTipo);
        if (tiposDTO.isEmpty()) return null;
        return tiposDTO.get(0);
    }

    public List<TipoDTO> getTiposByUso(String uso, Long grupoId, Paginacion paginacion) {


        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipo).join(qTipo.tipoPadreDTO, qTipoPadre);
        if (ParamUtils.isNotNull(grupoId)) {
            query.where(qTipoPadre.uso.eq(uso).and(qTipoPadre.grupoDTO.id.eq(grupoId)));
        } else {
            query.where(qTipoPadre.uso.eq(uso));
        }

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qTipo);
    }

    public TipoDTO getTipoPadre(String uso, Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipo).where(qTipo.uso.eq(uso).and(qTipo.grupoDTO.id.eq(grupoId)));
        return query.list(qTipo).get(0);
    }

    public List<TipoDTO> getZonas() {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTipo).where(qTipo.uso.like("TORNO_").and(qTipo.uso.substring(5, 6).in("1", "2", "3", "4", "5")));
        return query.list(qTipo);

    }

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qTipo)
                            .orderBy(qTipo.id.desc());

        if(!s.equals("")) {
            try {
                query.where(qTipo.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qTipo.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query
                .list(qTipo.id,
                      qTipo.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qTipo.id).toString());
                    item.setNombre(tuple.get(qTipo.nombre));
                    return item;
                }).collect(Collectors.toList());
    }

    public List<TipoDTO> getTiposAgrupaciones() {
        Long GRUPO_TIPOS_GRUPO = 9266975L;

        return new JPAQuery(entityManager)
                .from(qTipo)
                .where(qTipo.grupoDTO.id.eq(GRUPO_TIPOS_GRUPO))
                .orderBy(qTipo.nombre.asc())
                .list(qTipo.nombre, qTipo.id, qTipo.uso)
                .stream().map(tuple -> {
                    TipoDTO tipoAgrupacion = new TipoDTO();
                    tipoAgrupacion.setNombre(tuple.get(qTipo.nombre));
                    tipoAgrupacion.setId(tuple.get(qTipo.id));
                    tipoAgrupacion.setUso(tuple.get(qTipo.uso));
                    return tipoAgrupacion;
                }).collect(Collectors.toList());
    }

    public String getUbicacionByReferenciaId(Long referenciaId) {
        return new JPAQuery(entityManager)
                .from(qTipo)
                .where(qTipo.id.eq(referenciaId))
                .singleResult(qTipo.nombre);
    }
}
