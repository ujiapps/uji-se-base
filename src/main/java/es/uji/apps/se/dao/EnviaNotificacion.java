package es.uji.apps.se.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class EnviaNotificacion {

    private EnvioNotificacion envioNotificacion;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource){
        EnviaNotificacion.dataSource = dataSource;
    }

    @PostConstruct
    public void init() {
        this.envioNotificacion = new EnvioNotificacion(dataSource);
    }

    public void enviaNotificacion(Long personaId, String asunto, String cuerpo,
                                  String reply, Long remesa, Date fechaEnvio,
                                  String permiteDup) {
        envioNotificacion.enviaNotificacion(personaId, asunto, cuerpo, reply, remesa, fechaEnvio, permiteDup);
    }

    private class EnvioNotificacion extends StoredProcedure{

        private static final String SQL = "uji_sports.envia_notificacion";
        private static final String PPERSONA = "p_persona";
        private static final String PASUNTO = "p_asunto";
        private static final String PCUERPO = "p_cuerpo";
        private static final String PREPLY = "p_reply";
        private static final String PREMESA = "p_remesa";
        private static final String PFECHAENVIO = "p_fecha_envio";
        private static final String PPERMITEDUP = "p_permite_dup";

        public EnvioNotificacion(DataSource dataSource){
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PASUNTO, Types.VARCHAR));
            declareParameter(new SqlParameter(PCUERPO, Types.VARCHAR));
            declareParameter(new SqlParameter(PREPLY, Types.VARCHAR));
            declareParameter(new SqlParameter(PREMESA, Types.BIGINT));
            declareParameter(new SqlParameter(PFECHAENVIO, Types.DATE));
            declareParameter(new SqlParameter(PPERMITEDUP, Types.VARCHAR));
            compile();
        }

        public void enviaNotificacion(Long personaId, String asunto, String cuerpo, String reply, Long remesa, Date fechaEnvio, String permiteDup){
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PPERSONA, personaId);
            inParams.put(PASUNTO, asunto);
            inParams.put(PCUERPO, cuerpo);
            inParams.put(PREPLY, reply);
            inParams.put(PREMESA, remesa);
            inParams.put(PFECHAENVIO, fechaEnvio);
            inParams.put(PPERMITEDUP, permiteDup);
            Map<String, Object> results = execute(inParams);
        }
    }
}
