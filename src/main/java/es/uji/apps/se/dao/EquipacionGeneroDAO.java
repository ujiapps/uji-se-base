package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QEquipacionGeneroDTO;
import es.uji.apps.se.model.EquipacionGenero;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.xml.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository("equipacionGeneroDAO")
public class EquipacionGeneroDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private QEquipacionGeneroDTO qEquipacionGeneroDTO = QEquipacionGeneroDTO.equipacionGeneroDTO;

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qEquipacionGeneroDTO)
                            .orderBy(qEquipacionGeneroDTO.id.asc());

        if(!s.equals("")) {
            try {
                query.where(qEquipacionGeneroDTO.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qEquipacionGeneroDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query
                .list(qEquipacionGeneroDTO.id,
                      qEquipacionGeneroDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qEquipacionGeneroDTO.id).toString());
                    item.setNombre(tuple.get(qEquipacionGeneroDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }

    public List<EquipacionGenero> getGenerosEquipacion() {
        return new JPAQuery(entityManager)
                .from(qEquipacionGeneroDTO)
                .list(qEquipacionGeneroDTO.id,
                        qEquipacionGeneroDTO.nombre)
                .stream()
                .map(tuple -> new EquipacionGenero(tuple.get(qEquipacionGeneroDTO.id), tuple.get(qEquipacionGeneroDTO.nombre)))
                .collect(Collectors.toList());
    }
}
