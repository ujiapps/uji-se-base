package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.CheckDTO;
import es.uji.apps.se.dto.QCalendarioClaseDTO;
import es.uji.apps.se.dto.QCheckDTO;
import es.uji.apps.se.dto.TarjetaDeportivaClaseDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CheckDAO extends BaseDAODatabaseImpl {

    private QCheckDTO qCheck = QCheckDTO.checkDTO;


    public Boolean existeCheckTarjetaDeportivaClase(TarjetaDeportivaClaseDTO clase) {

        JPAQuery query = new JPAQuery(entityManager);

        //TODO-Comprobar que se cumple que el fichaje esta dentro de las horas en las que se realiza la actividad
//        if r.fecha_check between to_date(to_char(r.dia, 'ddmmyy')||r.hora_inicio, 'ddmmyyhh24:mi') - (1/144)
//           and to_date(to_char(r.dia, 'ddmmyy')||r.hora_inicio, 'ddmmyyhh24:mi') + (1/48)

        query.from(qCheck).where(qCheck.calendarioClaseDTO.id.eq(clase.getCalendarioClase().getId())
                .and(qCheck.persona.id.eq(clase.getTarjetaDeportiva().getPersonaUji().getId())));
        return !query.list(qCheck).isEmpty();
    }

    public List<CheckDTO> getCheckByTarjetaDeportivaId(Long tarjetaDeportivaId) {

        QCalendarioClaseDTO calendarioClase = QCalendarioClaseDTO.calendarioClaseDTO;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qCheck).join(qCheck.calendarioClaseDTO, calendarioClase).fetch().where(qCheck.tarjetaDeportiva.eq(tarjetaDeportivaId)).list(qCheck);
    }
}
