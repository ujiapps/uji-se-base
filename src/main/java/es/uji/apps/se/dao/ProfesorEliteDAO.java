package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.ProfesorEliteDTO;
import es.uji.apps.se.dto.QProfesorEliteDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class ProfesorEliteDAO extends BaseDAODatabaseImpl {
    public ProfesorEliteDTO getProfesorEliteById(Long profesorEliteId) {
        QProfesorEliteDTO qProfesorElite = QProfesorEliteDTO.profesorEliteDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qProfesorElite).where(qProfesorElite.personaId.eq(profesorEliteId));

        return query.list(qProfesorElite).get(0);

    }
}