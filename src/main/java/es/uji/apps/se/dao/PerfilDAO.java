package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QPerfilDTO;
import es.uji.apps.se.model.Perfil;
import es.uji.apps.se.model.QPerfil;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PerfilDAO extends BaseDAODatabaseImpl {
    public List<Perfil> getPerfiles() {

        QPerfilDTO qPerfilDTO = QPerfilDTO.perfilDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPerfilDTO).orderBy(qPerfilDTO.nombre.asc()).list(new QPerfil(qPerfilDTO.id, qPerfilDTO.nombre));
    }
}
