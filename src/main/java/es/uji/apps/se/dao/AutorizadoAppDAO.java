package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.AutorizadoAppDTO;
import es.uji.apps.se.dto.QAutorizadoAppDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AutorizadoAppDAO extends BaseDAODatabaseImpl
{
    public AutorizadoAppDTO getAutorizadoById(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QAutorizadoAppDTO autorizadoApp = QAutorizadoAppDTO.autorizadoAppDTO;

        List<AutorizadoAppDTO> administradores = query.from(autorizadoApp)
                .where(autorizadoApp.personaId.eq(personaId)).list(autorizadoApp);
        if (administradores.isEmpty()) return null;
        return administradores.get(0);
    }
}
