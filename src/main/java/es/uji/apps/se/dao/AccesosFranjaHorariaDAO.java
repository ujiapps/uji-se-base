package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QAccesosPorTipologiaDTO;
import es.uji.apps.se.model.AccesosFranjaHoraria;
import es.uji.apps.se.model.AccesosPorTipologia;
import es.uji.apps.se.model.domains.TipoZona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class AccesosFranjaHorariaDAO extends BaseDAODatabaseImpl {
    public List<AccesosFranjaHoraria> getAccesosPorFranjaHoraria(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {
        Map<String, AccesosFranjaHoraria> accesos = new HashMap<>();
        accesos.put("07:00-13:00", new AccesosFranjaHoraria("07:00-13:00"));
        accesos.put("13:00-17:00", new AccesosFranjaHoraria("13:00-17:00"));
        accesos.put("17:00-22:30", new AccesosFranjaHoraria("17:00-22:30"));

        QAccesosPorTipologiaDTO qAccesosPorTipologiaDTO = QAccesosPorTipologiaDTO.accesosPorTipologiaDTO;
        JPAQuery query1 = new JPAQuery(entityManager);
        query1.from(qAccesosPorTipologiaDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query1.where(qAccesosPorTipologiaDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query1.where(qAccesosPorTipologiaDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query1.where(qAccesosPorTipologiaDTO.horaEntrada.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query1.where(qAccesosPorTipologiaDTO.horaEntrada.loe(horaHasta));
        }

        query1.where(qAccesosPorTipologiaDTO.horaEntrada.goe("07:00").and(qAccesosPorTipologiaDTO.horaEntrada.lt("13:00")));

        List<Tuple> lista1 = query1
                .groupBy(qAccesosPorTipologiaDTO.zonaId)
                .list(qAccesosPorTipologiaDTO.zonaId, qAccesosPorTipologiaDTO.count());


        lista1.stream().forEach(acceso -> {
            AccesosFranjaHoraria accesosFranjaHoraria = accesos.get("07:00-13:00");

            if (TipoZona.RAQUETAS.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaRaquetas(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            if (TipoZona.PABELLON.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaPabellon(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            if (TipoZona.PISCINA.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaPiscina(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            if (TipoZona.AIRELIBRE.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaAireLibre(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            accesosFranjaHoraria.setCountTotal(accesosFranjaHoraria.getCountTotal() + acceso.get(qAccesosPorTipologiaDTO.count()));
        });

        JPAQuery query2 = new JPAQuery(entityManager);
        query2.from(qAccesosPorTipologiaDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query2.where(qAccesosPorTipologiaDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query2.where(qAccesosPorTipologiaDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query2.where(qAccesosPorTipologiaDTO.horaEntrada.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query2.where(qAccesosPorTipologiaDTO.horaEntrada.loe(horaHasta));
        }

        query2.where(qAccesosPorTipologiaDTO.horaEntrada.goe("13:00").and(qAccesosPorTipologiaDTO.horaEntrada.lt("17:00")));

        List<Tuple> lista2 = query2
                .groupBy(qAccesosPorTipologiaDTO.zonaId)
                .list(qAccesosPorTipologiaDTO.zonaId, qAccesosPorTipologiaDTO.count());


        lista2.stream().forEach(acceso -> {
            AccesosFranjaHoraria accesosFranjaHoraria = accesos.get("13:00-17:00");
            if (TipoZona.RAQUETAS.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaRaquetas(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            if (TipoZona.PABELLON.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaPabellon(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            if (TipoZona.PISCINA.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaPiscina(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            if (TipoZona.AIRELIBRE.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                accesosFranjaHoraria.setCountZonaAireLibre(acceso.get(qAccesosPorTipologiaDTO.count()));
            }
            accesosFranjaHoraria.setCountTotal(accesosFranjaHoraria.getCountTotal() + acceso.get(qAccesosPorTipologiaDTO.count()));
        });

        JPAQuery query3 = new JPAQuery(entityManager);
        query3.from(qAccesosPorTipologiaDTO);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query3.where(qAccesosPorTipologiaDTO.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query3.where(qAccesosPorTipologiaDTO.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query3.where(qAccesosPorTipologiaDTO.horaEntrada.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query3.where(qAccesosPorTipologiaDTO.horaEntrada.loe(horaHasta));
        }

        query3.where(qAccesosPorTipologiaDTO.horaEntrada.goe("17:00").and(qAccesosPorTipologiaDTO.horaEntrada.lt("22:30")));

        List<Tuple> lista3 = query3
                .groupBy(qAccesosPorTipologiaDTO.zonaId)
                .list(qAccesosPorTipologiaDTO.zonaId, qAccesosPorTipologiaDTO.count());


        lista3.forEach(acceso -> {
            AccesosFranjaHoraria accesosFranjaHoraria = accesos.get("17:00-22:30");
                if (TipoZona.RAQUETAS.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                    accesosFranjaHoraria.setCountZonaRaquetas(acceso.get(qAccesosPorTipologiaDTO.count()));
                }
                if (TipoZona.PABELLON.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                    accesosFranjaHoraria.setCountZonaPabellon(acceso.get(qAccesosPorTipologiaDTO.count()));
                }
                if (TipoZona.PISCINA.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                    accesosFranjaHoraria.setCountZonaPiscina(acceso.get(qAccesosPorTipologiaDTO.count()));
                }
                if (TipoZona.AIRELIBRE.getId().equals(acceso.get(qAccesosPorTipologiaDTO.zonaId))) {
                    accesosFranjaHoraria.setCountZonaAireLibre(acceso.get(qAccesosPorTipologiaDTO.count()));
                }
            accesosFranjaHoraria.setCountTotal(accesosFranjaHoraria.getCountTotal() + acceso.get(qAccesosPorTipologiaDTO.count()));
            });

        return accesos.values().stream().sorted(Comparator.comparing(AccesosFranjaHoraria::getFranjaHoraria)).collect(Collectors.toList());
    }
}
