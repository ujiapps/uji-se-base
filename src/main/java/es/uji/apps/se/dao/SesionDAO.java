package es.uji.apps.se.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.se.dto.QSesionDTO;
import es.uji.apps.se.dto.SesionDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SesionDAO extends BaseDAODatabaseImpl
{
    public Long getPersonaByHash(String hash)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QSesionDTO qSesion = QSesionDTO.sesionDTO;

        query.from(qSesion).where(qSesion.id.eq(hash).and(qSesion.fechaCaduca.after(new Date())));

        List<SesionDTO> lista = query.list(qSesion);
        if (lista.size()>0) return lista.get(0).getPersona();
        return null;
    }
}
