package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.se.dto.MiembrosPuntosDTO;
import es.uji.apps.se.dto.QCompeticionMiembroDTO;
import es.uji.apps.se.dto.QCompeticionPartidoDTO;
import es.uji.apps.se.dto.QMiembrosPuntosDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class MiembrosPuntosDAO extends BaseDAODatabaseImpl {

    private QMiembrosPuntosDTO qMiembrosPuntosDTO = QMiembrosPuntosDTO.miembrosPuntosDTO;

    private QCompeticionMiembroDTO qCompeticionMiembroDTO = QCompeticionMiembroDTO.competicionMiembroDTO;

    private QCompeticionPartidoDTO qCompeticionPartidoDTO = QCompeticionPartidoDTO.competicionPartidoDTO;

    public Long getMiembrosPuntos(Long userId, Long partidoId) {

        return new JPAQuery(entityManager).from(qMiembrosPuntosDTO)
                .where(qMiembrosPuntosDTO.miembro.eq(userId)
                    .and(qMiembrosPuntosDTO.partido.eq(partidoId)))
                .singleResult(qMiembrosPuntosDTO.puntos);
    }

    public Long getMiembrosTotal(Long equipoId, Long partidoId) {

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qMiembrosPuntosDTO)
                .join(qCompeticionMiembroDTO).on(qMiembrosPuntosDTO.miembro.eq(qCompeticionMiembroDTO.id))
                .where(qMiembrosPuntosDTO.partido.eq(partidoId)
                    .and(qCompeticionMiembroDTO.competicionEquipo.id.eq(equipoId)))
                .list(qMiembrosPuntosDTO.puntos)
                .stream().reduce(0L, (a,b) -> a+b);
    }

    public Long getMiembrosFantasmaPuntos(Long equipoId, Long partidoId) {

        return new JPAQuery(entityManager).from(qMiembrosPuntosDTO)
                .where(qMiembrosPuntosDTO.miembro.isNull()
                        .and(qMiembrosPuntosDTO.partido.eq(partidoId)).and(qMiembrosPuntosDTO.equipo.eq(equipoId)))
                .singleResult(qMiembrosPuntosDTO.puntos);
    }

    @Transactional
    public void updateMiembrosPuntos(Long inscripcionId, Long partidoId, Long goles){
        Long userid = new JPAQuery(entityManager).from(qCompeticionMiembroDTO)
                .where(qCompeticionMiembroDTO.inscripcion.id.eq(inscripcionId))
                .singleResult(qCompeticionMiembroDTO.id);

        Boolean exists = new JPAQuery(entityManager)
                .from(qMiembrosPuntosDTO)
                .where(qMiembrosPuntosDTO.miembro.eq(userid)
                        .and(qMiembrosPuntosDTO.partido.eq(partidoId)))
                .exists();

        if (exists){
            new JPAUpdateClause(entityManager, qMiembrosPuntosDTO)
                    .set(qMiembrosPuntosDTO.puntos, goles)
                    .where(qMiembrosPuntosDTO.miembro.eq(userid)
                            .and(qMiembrosPuntosDTO.partido.eq(partidoId)))
                    .execute();
        }

        else {
            MiembrosPuntosDTO nuevaFila = new MiembrosPuntosDTO();
            nuevaFila.setMiembro(userid);
            nuevaFila.setPartido(partidoId);
            nuevaFila.setPuntos(goles);
            insert(nuevaFila);
        }

    }

    @Transactional
    public void updateMiembrosFantasmaPuntos(Long equipo, Long partidoId, Long goles){

        Boolean exists = new JPAQuery(entityManager)
                .from(qMiembrosPuntosDTO)
                .where(qMiembrosPuntosDTO.miembro.isNull()
                        .and(qMiembrosPuntosDTO.partido.eq(partidoId)).and(qMiembrosPuntosDTO.equipo.eq(equipo)))
                .exists();

        if (exists){
            new JPAUpdateClause(entityManager, qMiembrosPuntosDTO)
                    .set(qMiembrosPuntosDTO.puntos, goles)
                    .where(qMiembrosPuntosDTO.equipo.eq(equipo)
                            .and(qMiembrosPuntosDTO.partido.eq(partidoId)))
                    .execute();
        }

        else {
            MiembrosPuntosDTO nuevaFila = new MiembrosPuntosDTO();
            nuevaFila.setPartido(partidoId);
            nuevaFila.setPuntos(goles);
            nuevaFila.setEquipo(equipo);
            insert(nuevaFila);
        }

    }

    @Transactional
    public Long updateGolesTotales(Long equipo, Long partidoId, String tipo){

        Long goles;
        Long incremento = (tipo.equals("plus")) ? 1L : -1L;

        if (equipo == 1) {

            goles = new JPAQuery(entityManager)
                    .from(qCompeticionPartidoDTO)
                    .where(qCompeticionPartidoDTO.id.eq(partidoId))
                    .singleResult(qCompeticionPartidoDTO.resultadoCasa);

        } else {

                goles = new JPAQuery(entityManager)
                        .from(qCompeticionPartidoDTO)
                        .where(qCompeticionPartidoDTO.id.eq(partidoId))
                        .singleResult(qCompeticionPartidoDTO.resultadoFuera);
        }

        if (goles == null)
            goles = 0L;

        if (!(goles == 0L && incremento < 0L)) {
            if (equipo == 1)
                new JPAUpdateClause(entityManager, qCompeticionPartidoDTO)
                        .set(qCompeticionPartidoDTO.resultadoCasa, goles + incremento)
                        .where(qCompeticionPartidoDTO.id.eq(partidoId))
                        .execute();
            else
                new JPAUpdateClause(entityManager, qCompeticionPartidoDTO)
                        .set(qCompeticionPartidoDTO.resultadoFuera, goles + incremento)
                        .where(qCompeticionPartidoDTO.id.eq(partidoId))
                        .execute();
            return goles + incremento;
        }

        return goles;
    }

}
