package es.uji.apps.se.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.se.dto.EquipacionesMovimientoDTO;
import es.uji.apps.se.dto.QEquipacionDTO;
import es.uji.apps.se.dto.QEquipacionModeloDTO;
import es.uji.apps.se.dto.QEquipacionesMovimientoDTO;
import es.uji.apps.se.model.EquipacionesAdmin;
import es.uji.apps.se.model.QEquipacionesAdmin;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EquipacionesAdminDAO extends BaseDAODatabaseImpl
{
    public List<EquipacionesAdmin> getEquipacionesAdmin(Long persona)
    {
        QEquipacionesMovimientoDTO qEquipacionesMovimientoDTO = QEquipacionesMovimientoDTO.equipacionesMovimientoDTO;
        QEquipacionDTO qEquipacionDTO = QEquipacionDTO.equipacionDTO;
        QEquipacionModeloDTO qEquipacionModeloDTO = QEquipacionModeloDTO.equipacionModeloDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEquipacionesMovimientoDTO)
                .where(qEquipacionesMovimientoDTO.personaId.eq(persona));

        return query.list(new QEquipacionesAdmin(qEquipacionesMovimientoDTO.id,
                qEquipacionesMovimientoDTO.fecha,
                qEquipacionesMovimientoDTO.equipacionDTO.id,
                qEquipacionesMovimientoDTO.equipacionDTO.tipoDTO.nombre,
                qEquipacionesMovimientoDTO.equipacionDTO.modeloDTO.nombre,
                qEquipacionesMovimientoDTO.equipacionDTO.generoDTO.nombre,
                qEquipacionesMovimientoDTO.talla,
                qEquipacionesMovimientoDTO.personaId));
    }
}