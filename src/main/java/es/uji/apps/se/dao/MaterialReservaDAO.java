package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QMaterialReservaDTO;
import es.uji.apps.se.model.MaterialReserva;
import es.uji.apps.se.model.QMaterialReserva;
import es.uji.apps.se.model.domains.TipoOrigenReservaMateriales;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MaterialReservaDAO extends BaseDAODatabaseImpl {
    public List<MaterialReserva> getMaterialesReservaClase(String claseId) {
        JPAQuery query = new JPAQuery(entityManager);

        QMaterialReservaDTO qMaterialReserva = QMaterialReservaDTO.materialReservaDTO;

        query.from(qMaterialReserva).where(qMaterialReserva.origen.eq(TipoOrigenReservaMateriales.CLASEGENERICO.getValue()).and(qMaterialReserva.referencia.eq(claseId)));

        return query.list(new QMaterialReserva(qMaterialReserva.id, qMaterialReserva.origen, qMaterialReserva.cantidad,
                qMaterialReserva.comentarios, qMaterialReserva.fecha, qMaterialReserva.visualizarEnAgenda, qMaterialReserva.referencia,
                qMaterialReserva.referencia2, qMaterialReserva.materialDTO.id));

    }
}
