package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.GrupoDTO;
import es.uji.apps.se.dto.QGrupoDTO;
import es.uji.apps.se.model.domains.TipoTaxonomia;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;


@Repository
public class GrupoDAO extends BaseDAODatabaseImpl {

    private QGrupoDTO qGrupo = QGrupoDTO.grupoDTO;

    public GrupoDTO getGrupoByTaxonomia(TipoTaxonomia tipoTaxonomia) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qGrupo).where(qGrupo.procedimiento.eq(tipoTaxonomia.getNombre()));

        return query.list(qGrupo).get(0);
    }

    public GrupoDTO getGrupoByGrupoId(Long id) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qGrupo)
                .where(qGrupo.id.eq(id))
                .singleResult(qGrupo);
    }
}
