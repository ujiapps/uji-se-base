package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.EquipacionesStock;
import es.uji.apps.se.model.Paginacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("equipacionesStock")
public class EquipacionesStockDAO extends BaseDAODatabaseImpl {

    private QEquipacionesStockDTO qEquipacionesStockDTO = QEquipacionesStockDTO.equipacionesStockDTO;
    private QEquipacionDTO qEquipacionDTO = QEquipacionDTO.equipacionDTO;
    private QEquipacionTipoDTO qEquipacionTipoDTO = QEquipacionTipoDTO.equipacionTipoDTO;
    private QEquipacionModeloDTO qEquipacionModeloDTO = QEquipacionModeloDTO.equipacionModeloDTO;
    private QEquipacionGeneroDTO qEquipacionGeneroDTO = QEquipacionGeneroDTO.equipacionGeneroDTO;
    private QEquipacionesMovimientoVWDTO qEquipacionesMovimientoVWDTO = QEquipacionesMovimientoVWDTO.equipacionesMovimientoVWDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public EquipacionesStockDAO() {
        relations.put("equipacionTipoNombre", qEquipacionTipoDTO.nombre);
        relations.put("equipacionModeloNombre", qEquipacionModeloDTO.nombre);
        relations.put("equipacionGeneroNombre", qEquipacionGeneroDTO.nombre);
        relations.put("talla", qEquipacionesStockDTO.talla);
        relations.put("dorsal", qEquipacionesStockDTO.dorsal);
        relations.put("stock", qEquipacionesStockDTO.stock);
    }

    public List<EquipacionesStock> getEquipacionesStock(Paginacion paginacion, List<Map<String, String>> filtros) {
        Date hoy = new Date();

        JPAQuery query = new JPAQuery(entityManager)
            .from(qEquipacionesStockDTO)
            .join(qEquipacionDTO).on(qEquipacionDTO.id.eq(qEquipacionesStockDTO.equipacionId))
            .join(qEquipacionTipoDTO).on(qEquipacionTipoDTO.id.eq(qEquipacionDTO.tipoDTO.id))
            .join(qEquipacionModeloDTO).on(qEquipacionModeloDTO.id.eq(qEquipacionDTO.modeloDTO.id))
            .join(qEquipacionGeneroDTO).on(qEquipacionGeneroDTO.id.eq(qEquipacionDTO.generoDTO.id))
            .where(qEquipacionDTO.fechaInicio.loe(hoy).and(qEquipacionDTO.fechaFin.isNull().or(qEquipacionDTO.fechaFin.gt(hoy))));

        if (filtros != null) {
            filtros.forEach(item -> {
                String property = item.get("property");
                String value = item.get("value");

                if (property.equals("equipacion")) {
                    query.where(qEquipacionesStockDTO.equipacionId.eq(Long.parseLong(value)));
                }

                if (property.equals("generoNombre")) {
                    query.where(qEquipacionGeneroDTO.id.eq(Long.parseLong(value)));
                }

                if (property.equals("modeloNombre")) {
                    query.where(qEquipacionModeloDTO.id.eq(Long.parseLong(value)));
                }

                if (property.equals("talla")) {
                    query.where(qEquipacionesStockDTO.talla.equalsIgnoreCase(value));
                }
            });
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }
            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else{
                query.orderBy(qEquipacionesStockDTO.equipacionId.desc());
            }
        }

        return query.list(
                    qEquipacionesStockDTO.talla,
                    qEquipacionesStockDTO.dorsal,
                    qEquipacionesStockDTO.stock,
                    qEquipacionDTO.id,
                    qEquipacionTipoDTO.nombre,
                    qEquipacionModeloDTO.nombre,
                    qEquipacionGeneroDTO.nombre)
                .stream()
                .map(tuple -> {
                    EquipacionesStock equipacionesStock = new EquipacionesStock();
                    equipacionesStock.setEquipacionId(tuple.get(qEquipacionDTO.id));
                    equipacionesStock.setEquipacionTipoNombre(tuple.get(qEquipacionTipoDTO.nombre));
                    equipacionesStock.setEquipacionModeloNombre(tuple.get(qEquipacionModeloDTO.nombre));
                    equipacionesStock.setEquipacionGeneroNombre(tuple.get(qEquipacionGeneroDTO.nombre));
                    equipacionesStock.setDorsal(tuple.get(qEquipacionesStockDTO.dorsal));
                    equipacionesStock.setTalla(tuple.get(qEquipacionesStockDTO.talla));
                    equipacionesStock.setStock(tuple.get(qEquipacionesStockDTO.stock));
                    return equipacionesStock;
                }
        ).collect(Collectors.toList());
    }

    public Long getStockByEquipacionId(Long equipacionId, String talla, Long dorsal) {
        Long stock = new JPAQuery(entityManager)
                .from(qEquipacionesStockDTO)
                .where(qEquipacionesStockDTO.equipacionId.eq(equipacionId)
                        .and((talla != null) ? qEquipacionesStockDTO.talla.eq(talla) : qEquipacionesStockDTO.talla.isNull())
                        .and((dorsal != null) ? qEquipacionesStockDTO.dorsal.eq(dorsal) : qEquipacionesStockDTO.dorsal.isNull()))
                .singleResult(qEquipacionesStockDTO.stock);
        return stock == null ? 0 : stock;
    }

    public List<EquipacionesStock> getEquipacionesStockFinsA(List<Map<String, String>> filtros, Date fechaIni, Date fechaFin) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qEquipacionesMovimientoVWDTO)
                .join(qEquipacionDTO).on(qEquipacionDTO.id.eq(qEquipacionesMovimientoVWDTO.equipacionId))
                .join(qEquipacionTipoDTO).on(qEquipacionTipoDTO.id.eq(qEquipacionDTO.tipoDTO.id))
                .join(qEquipacionModeloDTO).on(qEquipacionModeloDTO.id.eq(qEquipacionDTO.modeloDTO.id))
                .join(qEquipacionGeneroDTO).on(qEquipacionGeneroDTO.id.eq(qEquipacionDTO.generoDTO.id));

        if (fechaIni != null) query.where(qEquipacionesMovimientoVWDTO.fecha.between(fechaIni, fechaFin));
        else query.where(qEquipacionesMovimientoVWDTO.fecha.loe(fechaFin));

        if (filtros != null) {
            filtros.forEach(item -> {
                String property = item.get("property");
                String value = item.get("value");

                if (property.equals("equipacion"))
                    query.where(qEquipacionesMovimientoVWDTO.equipacionId.eq(Long.parseLong(value)));
                if (property.equals("generoNombre"))
                    query.where(qEquipacionGeneroDTO.id.eq(Long.parseLong(value)));
                if (property.equals("modeloNombre"))
                    query.where(qEquipacionModeloDTO.id.eq(Long.parseLong(value)));
                if (property.equals("talla"))
                    query.where(qEquipacionesMovimientoVWDTO.talla.equalsIgnoreCase(value));
            });
        }
        return query.groupBy(qEquipacionesMovimientoVWDTO.equipacionId,
                        qEquipacionesMovimientoVWDTO.talla,
                        qEquipacionesMovimientoVWDTO.dorsal,
                        qEquipacionTipoDTO.nombre,
                        qEquipacionModeloDTO.nombre,
                        qEquipacionGeneroDTO.nombre)
                .orderBy(qEquipacionesMovimientoVWDTO.equipacionId.asc(),
                        qEquipacionesMovimientoVWDTO.talla.asc(),
                        qEquipacionesMovimientoVWDTO.dorsal.asc())
                .list(qEquipacionesMovimientoVWDTO.equipacionId,
                        qEquipacionesMovimientoVWDTO.talla,
                        qEquipacionesMovimientoVWDTO.dorsal,
                        qEquipacionTipoDTO.nombre,
                        qEquipacionModeloDTO.nombre,
                        qEquipacionGeneroDTO.nombre,
                        qEquipacionesMovimientoVWDTO.contabiliza.sum())
                .stream().map(tuple -> {
                            EquipacionesStock equipacionesStock = new EquipacionesStock();
                            equipacionesStock.setEquipacionId(tuple.get(qEquipacionesMovimientoVWDTO.equipacionId));
                            equipacionesStock.setTalla(tuple.get(qEquipacionesMovimientoVWDTO.talla));
                            equipacionesStock.setDorsal(tuple.get(qEquipacionesMovimientoVWDTO.dorsal));
                            equipacionesStock.setEquipacionTipoNombre(tuple.get(qEquipacionTipoDTO.nombre));
                            equipacionesStock.setEquipacionModeloNombre(tuple.get(qEquipacionModeloDTO.nombre));
                            equipacionesStock.setEquipacionGeneroNombre(tuple.get(qEquipacionGeneroDTO.nombre));
                            equipacionesStock.setStock(tuple.get(qEquipacionesMovimientoVWDTO.contabiliza.sum()));
                            return equipacionesStock;
                        }).collect(Collectors.toList());
    }
}