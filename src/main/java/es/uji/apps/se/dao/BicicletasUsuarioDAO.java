package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class BicicletasUsuarioDAO extends BaseDAODatabaseImpl {
    private QBicicletasDTO qBicicletasDTO = QBicicletasDTO.bicicletasDTO;
    private QBicicletasReservasDTO qBicicletasReservasDTO = QBicicletasReservasDTO.bicicletasReservasDTO;
    private QTipoDTO qTipoDTO = QTipoDTO.tipoDTO;
    private QBicicletasDisponiblesVWDTO qBicicletasDisponiblesVWDTO = QBicicletasDisponiblesVWDTO.bicicletasDisponiblesVWDTO;
    private QBicicletaTiposReservasDTO qBicicletaTiposReservasDTO = QBicicletaTiposReservasDTO.bicicletaTiposReservasDTO;
    private QBicicletasHistoricoVWDTO qBicicletasHistoricoVWDTO = QBicicletasHistoricoVWDTO.bicicletasHistoricoVWDTO;
    private QBicicletasPeriodosDTO qBicicletasPeriodosDTO = QBicicletasPeriodosDTO.bicicletasPeriodosDTO;
    private QBicicletasTiposReservasVinculosDTO qBicicletasTiposReservasVinculosDTO = QBicicletasTiposReservasVinculosDTO.bicicletasTiposReservasVinculosDTO;
    private QOfertaDTO qOfertaDTO = QOfertaDTO.ofertaDTO;
    private QPeriodoDTO qPeriodoDTO = QPeriodoDTO.periodoDTO;
    private QInscripcionDTO qInscripcionDTO = QInscripcionDTO.inscripcionDTO;

    @InjectParam
    VinculacionesDAO vinculacionesDAO;

    public List<ReservaBicicletas> getListadoReservas(Long perId, Long cursoAca) {
        long vinculaciones = vinculacionesDAO.dameVinculoPersona(perId, "TARIFAS-ACTIVIDADES");
        return new JPAQuery(entityManager)
                .from(qBicicletasReservasDTO, qBicicletasDTO, qTipoDTO, qBicicletaTiposReservasDTO)
                .where(qBicicletasReservasDTO.bicicletaId.eq(qBicicletasDTO.id)
                        .and(qBicicletasReservasDTO.tipoUsoId.eq(qTipoDTO.id))
                        .and(qBicicletaTiposReservasDTO.id.eq(qBicicletasReservasDTO.tipoReservaId))
                        .and(qBicicletasReservasDTO.perId.eq(perId)))
                .orderBy(qBicicletasReservasDTO.fecha.desc())
                .list(qBicicletasReservasDTO.id,
                        qBicicletasReservasDTO.fecha,
                        qBicicletasDTO.numBici,
                        qBicicletasReservasDTO.tipoId,
                        qBicicletasReservasDTO.tipoReservaId,
                        qBicicletaTiposReservasDTO.nombre,
                        qTipoDTO.nombre,
                        qBicicletasReservasDTO.fechaPrevDevol,
                        qBicicletasReservasDTO.fechaIni,
                        qBicicletasReservasDTO.fechaFin,
                        qBicicletasReservasDTO.observaciones,
                        qBicicletasDTO.id)
                .stream().map(tuple -> {
                    ReservaBicicletas bicicletas = new ReservaBicicletas(
                            tuple.get(qBicicletasReservasDTO.id),
                            tuple.get(qBicicletasReservasDTO.fecha),
                            tuple.get(qBicicletasDTO.numBici),
                            tuple.get(qTipoDTO.nombre),
                            tuple.get(qBicicletasReservasDTO.fechaIni),
                            tuple.get(qBicicletasReservasDTO.fechaFin),
                            tuple.get(qBicicletasReservasDTO.fechaPrevDevol),
                            tuple.get(qBicicletasReservasDTO.observaciones),
                            tuple.get(qBicicletasDTO.id));
                    bicicletas.setBicisDisponibles(new JPAQuery(entityManager).from(qBicicletasDisponiblesVWDTO).singleResult(qBicicletasDisponiblesVWDTO.numBicisDisponibles));
                    String nativeQuery = "SELECT COUNT(v.id) " +
                            "FROM se_bic_tipos_reservas r, se_bic_tipos_reservas_vinculos v " +
                            "WHERE (r.id = v.tipo_reserva_id AND r.curso_aca = " + cursoAca + " AND v.vinculacion_id = " + vinculaciones + ") " +
                            "   OR (v.tipo_reserva_id = r.id AND r.curso_aca = " + cursoAca + " AND v.carnet_bono_id IN (SELECT cbt.id FROM se_carnets_bonos cb, se_carnets_bonos_tipos cbt WHERE cb.persona_id = " + perId + " AND TRUNC(SYSDATE) <= TRUNC(cb.fecha_fin) AND TRUNC(NVL(cb.fecha_baja, SYSDATE + 1)) > TRUNC(SYSDATE) AND dame_usos(cb.id) < NVL(cbt.usos, 999999) AND cbt.id = cb.tipo_id)) " +
                            "   OR (v.tipo_reserva_id = r.id AND r.curso_aca = " + cursoAca + " AND v.actividad_id IN (SELECT actividad_id FROM se_actividades_oferta o, se_periodos p, se_inscripciones i WHERE i.persona_id = " + perId + " AND i.estado_id = 291230 AND i.oferta_id = o.id AND o.periodo_id = p.id AND p.curso_aca = " + cursoAca + "))";
                    Query query = entityManager.createNativeQuery(nativeQuery);

                    bicicletas.setTipoReservaId(tuple.get(qBicicletasReservasDTO.tipoReservaId));

                    long numTipo = ((Number) query.getSingleResult()).longValue();
                    if (tuple.get(qBicicletasReservasDTO.tipoId) == null && numTipo >= 0L) {
                        if (tuple.get(qBicicletasReservasDTO.tipoReservaId) != null) {
                            bicicletas.setTipo(tuple.get(qBicicletasReservasDTO.tipoReservaId).toString());
                            bicicletas.setNombreTipoReserva(tuple.get(qBicicletaTiposReservasDTO.nombre));
                        }
                    } else {
                        String nombre = new JPAQuery(entityManager).from(qTipoDTO).where(qTipoDTO.id.eq(tuple.get(qBicicletasReservasDTO.tipoId))).singleResult(qTipoDTO.nombre);
                        bicicletas.setTipo(nombre);
                    }
                    return bicicletas;
                }).collect(Collectors.toList());
    }

    @Transactional
    public void updateReservaBicicleta(Long reservaId, Long tipoReservaId, Date fechaFin, Date fechaPrevDevol, String observaciones) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qBicicletasReservasDTO)
                                    .set(qBicicletasReservasDTO.tipoReservaId, tipoReservaId)
                                    .set(qBicicletasReservasDTO.observaciones, observaciones);

        if(fechaFin == null)
            update = update.setNull(qBicicletasReservasDTO.fechaFin);
        else
            update = update.set(qBicicletasReservasDTO.fechaFin, fechaFin);

        if(fechaPrevDevol == null)
            update = update.setNull(qBicicletasReservasDTO.fechaPrevDevol);
        else
            update = update.set(qBicicletasReservasDTO.fechaPrevDevol, fechaPrevDevol);

        update
        .where(qBicicletasReservasDTO.id.eq(reservaId))
        .execute();
    }

    @Transactional
    public void deleteReservaBicicleta(Long reservaId) {
        new JPADeleteClause(entityManager, qBicicletasReservasDTO)
                .where(qBicicletasReservasDTO.id.eq(reservaId))
                .execute();
    }

    public List<HistoricoBicicletas> getListadoBicicletasHistorico(Long perId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qBicicletasHistoricoVWDTO)
                .where(qBicicletasHistoricoVWDTO.personaId.eq(perId));

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0)
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            query.orderBy(qBicicletasHistoricoVWDTO.fecha.desc());
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }
        return query.list(qBicicletasHistoricoVWDTO).stream().map(tuple -> {
                    HistoricoBicicletas historicoBicicletas = new HistoricoBicicletas();
                    historicoBicicletas.setId(tuple.getId());
                    historicoBicicletas.setFecha(tuple.getFecha());
                    historicoBicicletas.setNumBici(tuple.getNumBicis());
                    historicoBicicletas.setTipoId(tuple.getTipoId());
                    historicoBicicletas.setFechaIni(tuple.getFechaIni());
                    historicoBicicletas.setFechaFin(tuple.getFechaFin());
                    historicoBicicletas.setObservaciones(tuple.getObservaciones());
                    historicoBicicletas.setBicicletaId(tuple.getBicicletaId());
                    historicoBicicletas.setXfecha(tuple.getxFecha());
                    historicoBicicletas.setApellidosNombre(tuple.getApellidosNombre());
                    historicoBicicletas.setTipoReserva(tuple.getTipoReserva());
                    historicoBicicletas.setTipoUso(tuple.getTipoUso());
                    historicoBicicletas.setFechaPrevDevol(tuple.getFechaPrevDevol());
                    historicoBicicletas.setAccion(tuple.getAccion());
                    return historicoBicicletas;
                }).collect(Collectors.toList());
    }

    public List<TipoIdentificacion> getListadoTipoReservasByYear(Long cursoAca) {
        return new JPAQuery(entityManager)
                .from(qBicicletaTiposReservasDTO)
                .where(qBicicletaTiposReservasDTO.cursoAca.eq(cursoAca))
                .list(qBicicletaTiposReservasDTO.id, qBicicletaTiposReservasDTO.nombre).stream().map(tuple -> {
                    TipoIdentificacion identificacion = new TipoIdentificacion();
                    identificacion.setId(tuple.get(qBicicletaTiposReservasDTO.id));
                    identificacion.setNombre(tuple.get(qBicicletaTiposReservasDTO.nombre));
                    return identificacion;
                }).collect(Collectors.toList());
    }

    public BicicletasPeriodosDTO getBicicletaPeriodos(Long cursoAca) {
        return new JPAQuery(entityManager)
                .from(qBicicletasPeriodosDTO)
                .where(qBicicletasPeriodosDTO.cursoAca.eq(cursoAca))
                .singleResult(qBicicletasPeriodosDTO);
    }

    public Long getNumeroVinculos(Long perId, Long cursoAca, Long vinculo) {
        return new JPAQuery(entityManager)
                .from(qBicicletasTiposReservasVinculosDTO, qBicicletaTiposReservasDTO)
                .where(qBicicletasTiposReservasVinculosDTO.vinculacionId.eq(vinculo)
                        .and(qBicicletasTiposReservasVinculosDTO.tipoReservaId.eq(qBicicletaTiposReservasDTO.id))
                        .and(qBicicletaTiposReservasDTO.cursoAca.eq(cursoAca))
                        .and(qBicicletasTiposReservasVinculosDTO.vinculacionId.eq(vinculacionesDAO.dameVinculoPersona(perId, "TARIFAS-ACTIVIDADES"))))
                .count();
    }

    public Long getNumeroTarjetas(Long perId, Long cursoAca) {
        String nativeSql =  "select count(*) " +
                            "from se_bic_tipos_reservas_vinculos v, se_bic_tipos_reservas r " +
                            "where v.tipo_reserva_id = r.id " +
                                "and r.curso_aca = " + cursoAca +
                                " and v.carnet_bono_id in (" +
                                    "select cbt.id " +
                                    "from se_carnets_bonos cb, se_carnets_bonos_tipos cbt " +
                                    "where persona_id = " + perId +
                                        " and trunc(sysdate) <= trunc(cb.fecha_fin) " +
                                        "and trunc(nvl(cb.fecha_baja, sysdate+1)) > trunc(sysdate) " +
                                        "and dame_usos(cb.id) < nvl(cbt.usos, 999999) " +
                                        "and cbt.id = cb.tipo_id)";
        Query query = entityManager.createNativeQuery(nativeSql);
        Object result = query.getSingleResult();

        if (result == null) return null;
        else                return ((BigDecimal) result).longValue();
    }

    public Long getNumeroActividades(Long perId, Long cursoAca) {
        return new JPAQuery(entityManager)
                .from(qBicicletasTiposReservasVinculosDTO, qBicicletaTiposReservasDTO)
                .where(qBicicletasTiposReservasVinculosDTO.tipoReservaId.eq(qBicicletaTiposReservasDTO.id)
                        .and(qBicicletaTiposReservasDTO.cursoAca.eq(cursoAca))
                        .and(qBicicletasTiposReservasVinculosDTO.actividadId.in(
                                new JPAQuery(entityManager).from(qOfertaDTO, qPeriodoDTO, qInscripcionDTO)
                                        .where(qInscripcionDTO.personaUjiDTO.id.eq(perId)
                                                .and(qInscripcionDTO.estado.id.eq(291230L))
                                                .and(qInscripcionDTO.ofertaDTO.id.eq(qOfertaDTO.id))
                                                .and(qOfertaDTO.periodoDTO.id.eq(qPeriodoDTO.id))
                                                .and(qPeriodoDTO.cursoAcademicoDTO.id.eq(cursoAca)))
                                        .list(qOfertaDTO.actividadDTO.id)
                        ))).count();
    }

    public BicicletasFechasTipoReservas getFechasBicicletasTipoReservasVinculo(Long vinculo, Long cursoAca, Date fechaIni, Date fechaFin) throws ParseException {
        String nativeSql =  "select case when min(fecha_inicio) < CAST(TO_TIMESTAMP('" + fechaIni + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) " +
                                "then min(fecha_inicio) else CAST(TO_TIMESTAMP('" + fechaIni + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) end, " +
                                "case when max(fecha_fin) > CAST(TO_TIMESTAMP('" + fechaFin + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) " +
                                "then max(fecha_fin) else CAST(TO_TIMESTAMP('" + fechaFin + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) end " +
                            " from se_bic_tipos_reservas_vinculos v, se_bic_tipos_reservas r " +
                            " where v.vinculacion_id = " + vinculo +
                                " and v.tipo_reserva_id = r.id " +
                                "and r.curso_aca = " + cursoAca;
        Query query = entityManager.createNativeQuery(nativeSql);
        Object[] a = (Object[]) query.getSingleResult();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return new BicicletasFechasTipoReservas(sdf.parse(a[0].toString()), sdf.parse(a[1].toString()));
    }

    public BicicletasFechasTipoReservas getFechasBicicletasTipoReservasTarjeta(Long perId, Long cursoAca, Date fechaIni, Date fechaFin) throws ParseException {
        String nativeSql = "select case when min(fecha_inicio) < CAST(TO_TIMESTAMP('" + fechaIni + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) " +
                            "then min(fecha_inicio) else CAST(TO_TIMESTAMP('" + fechaIni + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) end, " +
                                "case when max(fecha_fin) > CAST(TO_TIMESTAMP('" + fechaFin + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) " +
                                "then max(fecha_fin) else CAST(TO_TIMESTAMP('" + fechaFin + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) end " +
                            "from se_bic_tipos_reservas_vinculos v, se_bic_tipos_reservas r " +
                            "where v.carnet_bono_id in (" +
                                    "select cbt.id " +
                                    "from se_carnets_bonos cb, se_carnets_bonos_tipos cbt " +
                                    "where persona_id = " + perId +
                                        " and trunc(sysdate) <= trunc(cb.fecha_fin) " +
                                        "and trunc(nvl(cb.fecha_baja, sysdate+1)) > trunc(sysdate) " +
                                        "and dame_usos (cb.id) < nvl(cbt.usos, 999999) " +
                                        "and cbt.id = cb.tipo_id) " +
                                "and v.tipo_reserva_id = r.id " +
                                "and r.curso_aca = " + cursoAca;
        Query query = entityManager.createNativeQuery(nativeSql);
        Object[] a = (Object[]) query.getSingleResult();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return new BicicletasFechasTipoReservas(sdf.parse(a[0].toString()), sdf.parse(a[1].toString()));
    }

    public BicicletasFechasTipoReservas getFechasBicicletasTipoReservasActividades(Long perId, Long cursoAca, Date fechaIni, Date fechaFin) throws ParseException {
        String nativeSql = "select case when min(fecha_inicio) < CAST(TO_TIMESTAMP('" + fechaIni + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) " +
                                " then min(fecha_inicio) else CAST(TO_TIMESTAMP('" + fechaIni + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) end, " +
                                "case when max(fecha_fin) > CAST(TO_TIMESTAMP('" + fechaFin + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) " +
                                " then max(fecha_fin) else CAST(TO_TIMESTAMP('" + fechaFin + "', 'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE) end " +
                            "from se_bic_tipos_reservas_vinculos v, se_bic_tipos_reservas r " +
                            "where v.actividad_id in (" +
                                    "select actividad_id " +
                                    "from se_actividades_oferta o, se_periodos p, se_inscripciones i " +
                                    "where i.persona_id = " + perId +
                                        " and i.estado_id = 291230 " +
                                        "and i.oferta_id = o.id " +
                                        "and o.periodo_id = p.id " +
                                        "and p.curso_aca = " + cursoAca + ") " +
                                "and v.tipo_reserva_id = r.id " +
                                "and r.curso_aca = " + cursoAca;
        Query query = entityManager.createNativeQuery(nativeSql);
        Object[] a = (Object[]) query.getSingleResult();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return new BicicletasFechasTipoReservas(sdf.parse(a[0].toString()), sdf.parse(a[1].toString()));
    }
}
