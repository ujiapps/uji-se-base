package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.filtros.FiltroJaula;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class JaulaPersonaDAO extends BaseDAODatabaseImpl {

    QJaulaPersonaVistaDTO qJaulaPersonaVista = QJaulaPersonaVistaDTO.jaulaPersonaVistaDTO;

    public String limpiaAcentos(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim().toUpperCase();
    }

//    private Map<String, Path> relations = new HashMap<>();
//
//    public JaulaPersonaDAO() {
//        relations.put("id", qJaulaPersonaVista.id);
//        relations.put("personaUjiId", qJaulaPersonaVista.personaUji.busqueda);
//        relations.put("fechaInicio", qJaulaPersonaVista.fechaInicio);
//    }


    public JaulaPersonaDTO getAccesoJaula(Long personaId) {
        QJaulaPersonaDTO qJaulaPersona = QJaulaPersonaDTO.jaulaPersonaDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qJaulaPersona).where(qJaulaPersona.personaUji.id.eq(personaId)
                        .and((qJaulaPersona.fechaFin.isNull()).or(qJaulaPersona.fechaFin.gt(new Date()))))
                .orderBy(qJaulaPersona.fechaFin.desc());
        List<JaulaPersonaDTO> list = query.list(qJaulaPersona);
        if (!list.isEmpty()) return list.get(0);
        return null;
    }

    public List<JaulaPersonaVistaDTO> getJaulasPersonas(Paginacion paginacion, FiltroJaula filtro) {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
        QPersonaVinculoDTO qPersonaVinculo = QPersonaVinculoDTO.personaVinculoDTO;
        query.from(qJaulaPersonaVista)
                .leftJoin(qJaulaPersonaVista.personaUji, qPersonaUji).fetch()
                .leftJoin(qPersonaUji.personaVinculoDTO, qPersonaVinculo).fetch();

        if (ParamUtils.isNotNull(filtro.getBusqueda())) {
            query.where(qJaulaPersonaVista.busqueda.like("%" + limpiaAcentos(filtro.getBusqueda()) + "%"));
        }

        if (ParamUtils.isNotNull(filtro.getFechaInicioDesde())) {
            query.where(qJaulaPersonaVista.fechaInicio.goe(filtro.getFechaInicioDesde()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaInicioHasta())) {
            query.where(qJaulaPersonaVista.fechaInicio.loe(filtro.getFechaInicioHasta()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaFinDesde())) {
            query.where(qJaulaPersonaVista.fechaFin.goe(filtro.getFechaFinDesde()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaFinHasta())) {
            query.where(qJaulaPersonaVista.fechaFin.loe(filtro.getFechaFinHasta()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaLlaveroDesde())) {
            query.where(qJaulaPersonaVista.fechaLlavero.goe(filtro.getFechaLlaveroDesde()));
        }

        if (ParamUtils.isNotNull(filtro.getFechaLlaveroHasta())) {
            query.where(qJaulaPersonaVista.fechaLlavero.loe(filtro.getFechaLlaveroHasta()));
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if (atributoOrdenacion != null)
                query.orderBy(ordenaJaulas(orden, atributoOrdenacion));
            else
                query.orderBy(qJaulaPersonaVista.id.asc());

            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }
        }

        return query.list(qJaulaPersonaVista);

    }

    private OrderSpecifier<?> ordenaJaulas(String orden, String atributoOrdenacion) {

        if (atributoOrdenacion.equals("id")) {
            return orden.equals("ASC") ? qJaulaPersonaVista.id.asc() : qJaulaPersonaVista.id.desc();
        }
        if (atributoOrdenacion.equals("fechaInicio")) {
            return orden.equals("ASC") ? qJaulaPersonaVista.fechaInicio.asc() : qJaulaPersonaVista.fechaInicio.desc();
        }
        if (atributoOrdenacion.equals("fechaFin")) {
            return orden.equals("ASC") ? qJaulaPersonaVista.fechaFin.asc() : qJaulaPersonaVista.fechaFin.desc();
        }
        if (atributoOrdenacion.equals("fechaLlavero")) {
            return orden.equals("ASC") ? qJaulaPersonaVista.fechaLlavero.asc() : qJaulaPersonaVista.fechaLlavero.desc();
        }
        if(atributoOrdenacion.equals("personaUjiId")){
            return orden.equals("ASC")?qJaulaPersonaVista.personaUji.apellido1.asc() : qJaulaPersonaVista.personaUji.apellido1.desc();
        }

        return qJaulaPersonaVista.id.desc();
    }
}
