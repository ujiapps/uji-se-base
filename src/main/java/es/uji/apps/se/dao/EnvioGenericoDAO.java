package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.EnvioGenericoDTO;
import es.uji.apps.se.dto.QEnvioGenericoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class EnvioGenericoDAO extends BaseDAODatabaseImpl {
    public EnvioGenericoDTO getEnvioGenerico(String tipoEnvio) {

        JPAQuery query = new JPAQuery(entityManager);
        QEnvioGenericoDTO qEnvioGenerico = QEnvioGenericoDTO.envioGenericoDTO;

        query.from(qEnvioGenerico).where(qEnvioGenerico.tipoEnvio.eq(tipoEnvio));

        return query.uniqueResult(qEnvioGenerico);
    }
}
