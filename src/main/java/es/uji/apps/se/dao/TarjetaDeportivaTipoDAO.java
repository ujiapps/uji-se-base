package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QTarjetaDeportivaTarifaDTO;
import es.uji.apps.se.dto.QTarjetaDeportivaTipoDTO;
import es.uji.apps.se.dto.QTipoDTO;
import es.uji.apps.se.dto.TarjetaDeportivaTipoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetaDeportivaTipoDAO extends BaseDAODatabaseImpl {

    private QTarjetaDeportivaTipoDTO qTarjetaDeportivaTipo = QTarjetaDeportivaTipoDTO.tarjetaDeportivaTipoDTO;

    public List<TarjetaDeportivaTipoDTO> getTarjetasDeportivasTiposActivas()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTarjetaDeportivaTarifaDTO qTarjetaDeportivaTarifa = QTarjetaDeportivaTarifaDTO.tarjetaDeportivaTarifaDTO;
        QTipoDTO qTipo = QTipoDTO.tipoDTO;

        query.from(qTarjetaDeportivaTipo)
                .leftJoin(qTarjetaDeportivaTipo.tarjetaDeportivaTarifasDTO, qTarjetaDeportivaTarifa).fetch()
                .leftJoin(qTarjetaDeportivaTarifa.vinculo, qTipo).fetch().orderBy(qTarjetaDeportivaTipo.ordenVisualizacion.asc());

        return query.distinct().list(qTarjetaDeportivaTipo);
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportivaTipoById(Long tipoTarjetaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QTarjetaDeportivaTarifaDTO qTarjetaDeportivaTarifa = QTarjetaDeportivaTarifaDTO.tarjetaDeportivaTarifaDTO;
        QTipoDTO qTipo = QTipoDTO.tipoDTO;

        query.from(qTarjetaDeportivaTipo)
                .leftJoin(qTarjetaDeportivaTipo.tarjetaDeportivaTarifasDTO, qTarjetaDeportivaTarifa).fetch()
                .leftJoin(qTarjetaDeportivaTarifa.vinculo, qTipo).fetch()
                .where(qTarjetaDeportivaTipo.id.eq(tipoTarjetaId));
        return query.uniqueResult(qTarjetaDeportivaTipo);
    }

    public String getTarjetaDeportivaTipoNombreByReferenciaId(Long referenciaId) {
        JPAQuery query = new JPAQuery(entityManager);
        return  query.from(qTarjetaDeportivaTipo)
                .where(qTarjetaDeportivaTipo.id.eq(referenciaId))
                .singleResult(qTarjetaDeportivaTipo.nombre);
    }
}
