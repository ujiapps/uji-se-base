package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.Curso;
import es.uji.apps.se.model.QCurso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class CursosDAO extends BaseDAODatabaseImpl {
    private QCursosDTO qCursosDTO = QCursosDTO.cursosDTO;
    private QCursosInscritosDTO qCursosInscritosDTO = QCursosInscritosDTO.cursosInscritosDTO;
    private QExpRecibosDTO qExpRecibosDTO = QExpRecibosDTO.expRecibosDTO;
    private QReciboVWDTO qReciboVWDTO = QReciboVWDTO.reciboVWDTO;

    public List<Curso> getCursos(Long perId) {
        return new JPAQuery(entityManager)
                .from(qCursosDTO, qCursosInscritosDTO)
                .where(qCursosInscritosDTO.cursoAca.eq(qCursosDTO.id)
                        .and(qCursosInscritosDTO.perId.eq(perId)))
                .orderBy(qCursosDTO.fechaIni.asc())
                .list(new QCurso(qCursosDTO.nombre, qCursosDTO.fechaIni, qCursosDTO.fechaFin, qCursosDTO.anulado, qCursosDTO.id, qCursosInscritosDTO.confirmacion));
    }

    public String getComentario(Curso curso, Long perId) {
        String result = "";
        Date fechaPagoMinima = new JPAQuery(entityManager)
                                    .from(qExpRecibosDTO)
                                    .where(qExpRecibosDTO.perId.eq(perId)
                                    .and(qExpRecibosDTO.titId.eq(curso.getId())))
                                    .singleResult(qExpRecibosDTO.fechaPago.min());

        if (fechaPagoMinima == null) {
            fechaPagoMinima = new JPAQuery(entityManager)
                                    .from(qReciboVWDTO)
                                    .where(qReciboVWDTO.personaId.eq(perId)
                                    .and(qReciboVWDTO.referenciaId.eq(curso.getId())))
                                    .singleResult(qReciboVWDTO.fechaPago.min());
        }

        if (curso.getConfirmacion() == 1)
            result = "Confirmació de pagament extern";
        else if (fechaPagoMinima == null)
            result = "Impagat";
        else {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            result = "data de pagament: " + dateFormat.format(fechaPagoMinima);
        }

        if (curso.getAnulado() == 1)
            result = "Curs anul.lat";

        return result;
    }
}
