
package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.*;
import es.uji.apps.se.utils.Utils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class PersonaTarjetaBonoDAO extends BaseDAODatabaseImpl {

    private final PersonaDAO personaDAO;

    public PersonaTarjetaBonoDAO(PersonaDAO personaDAO) {
        this.personaDAO = personaDAO;
    }

    private final QCarnetBonoTipoDTO qCarnetBonoTipo = QCarnetBonoTipoDTO.carnetBonoTipoDTO;
    private final QCarnetBonoDTO qCarnetBono = QCarnetBonoDTO.carnetBonoDTO;

    private final QCarnetBonoVWDTO qCarnetBonoVW = QCarnetBonoVWDTO.carnetBonoVWDTO;

    public List<TarjetaBono> getTarjetasBonos(Paginacion paginacion, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qCarnetBonoVW)
                .where(qCarnetBonoVW.personaId.eq(personaId));

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());

            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            String atributoOrdenacion = paginacion.getOrdenarPor();
            String orden = paginacion.getDireccion();

            if (atributoOrdenacion != null) {
                query.orderBy(ordenaTarjetasBonos(orden, atributoOrdenacion));
            } else
                query.orderBy(qCarnetBonoVW.fechaInicio.desc(), qCarnetBonoVW.fechaFin.desc());

            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.list(new QTarjetaBono(qCarnetBonoVW.id,
                qCarnetBonoVW.tipoId,
                qCarnetBonoVW.tipoInstalacionId,
                qCarnetBonoVW.personaId,
                qCarnetBonoVW.usos,
                qCarnetBonoVW.totalUsos,
                qCarnetBonoVW.activo,
                qCarnetBonoVW.dias,
                qCarnetBonoVW.neto,
                qCarnetBonoVW.plazos,
                qCarnetBonoVW.estado,
                qCarnetBonoVW.nombre,
                qCarnetBonoVW.observaciones,
                qCarnetBonoVW.origen,
                qCarnetBonoVW.fechaInicio,
                qCarnetBonoVW.fechaFin,
                qCarnetBonoVW.fechaBaja
        ));
    }

    public List<TarjetaBono> getHistoricoTarjetasBonos(Long personaId, Long cursoAcademico) {

        JPAQuery query = new JPAQuery(entityManager);
        QCarnetBonoHistoricoDTO qCarnetBonoHistoricoDTO = QCarnetBonoHistoricoDTO.carnetBonoHistoricoDTO;
        query.from(qCarnetBonoHistoricoDTO)
                .join(qCarnetBonoHistoricoDTO.tipoHistorico, qCarnetBonoTipo)
                .where(qCarnetBonoHistoricoDTO.cursoAcademico.eq(cursoAcademico)
                        .and(qCarnetBonoHistoricoDTO.personaId.eq(personaId)));

        return query.list(new QTarjetaBono(qCarnetBonoHistoricoDTO.id, qCarnetBonoTipo.nombre));
    }

    @Transactional
    public void updateTarjetaBono(TarjetaBono tarjetaBono, Long tarjetaBonoId) {
        new JPAUpdateClause(entityManager, qCarnetBono)
                .set(qCarnetBono.fechaInicio, tarjetaBono.getFechaInicio())
                .set(qCarnetBono.fechaFin, tarjetaBono.getFechaFin())
                .set(qCarnetBono.fechaBaja, tarjetaBono.getFechaBaja())
                .set(qCarnetBono.observaciones, tarjetaBono.getObservaciones())
                .where(qCarnetBono.id.eq(tarjetaBonoId))
                .execute();
    }

    private OrderSpecifier<?> ordenaTarjetasBonos(String orden, String atributoOrdenacion) {
        switch (atributoOrdenacion) {
            case "id":
                return orden.equals("ASC") ? qCarnetBonoVW.id.asc() : qCarnetBonoVW.id.desc();

            case "nombre":
                return orden.equals("ASC") ? qCarnetBonoVW.nombre.asc() : qCarnetBonoVW.nombre.desc();

            case "tipoId":
                return orden.equals("ASC") ? qCarnetBonoVW.tipoId.asc() : qCarnetBonoVW.tipoId.desc();

            case "usos":
                return orden.equals("ASC") ? qCarnetBonoVW.usos.asc() : qCarnetBonoVW.usos.desc();

            case "totalUsos":
                return orden.equals("ASC") ? qCarnetBonoVW.totalUsos.asc() : qCarnetBonoVW.totalUsos.desc();

            case "activo":
                return orden.equals("ASC") ? qCarnetBonoVW.activo.asc() : qCarnetBonoVW.activo.desc();

            case "dias":
                return orden.equals("ASC") ? qCarnetBonoVW.dias.asc() : qCarnetBonoVW.dias.desc();

            case "neto":
                return orden.equals("ASC") ? qCarnetBonoVW.neto.asc() : qCarnetBonoVW.neto.desc();

            case "estado":
                return orden.equals("ASC") ? qCarnetBonoVW.estado.asc() : qCarnetBonoVW.estado.desc();

            case "observaciones":
                return orden.equals("ASC") ? qCarnetBonoVW.observaciones.asc() : qCarnetBonoVW.observaciones.desc();

            case "origen":
                return orden.equals("ASC") ? qCarnetBonoVW.origen.asc() : qCarnetBonoVW.origen.desc();

            case "fechaInicio":
                return orden.equals("ASC") ? qCarnetBonoVW.fechaInicio.asc() : qCarnetBonoVW.fechaInicio.desc();

            case "fechaFin":
                return orden.equals("ASC") ? qCarnetBonoVW.fechaFin.asc() : qCarnetBonoVW.fechaFin.desc();

            case "fechaBaja":
                return orden.equals("ASC") ? qCarnetBonoVW.fechaBaja.asc() : qCarnetBonoVW.fechaBaja.desc();

            default:
                return qCarnetBonoVW.fechaInicio.desc();
        }
    }

    public Boolean tieneTarjetaTipo(Long personaId, Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCarnetBono)
                .where(qCarnetBono.personaId.eq(personaId)
                        .and(qCarnetBono.tipo.id.eq(tipoId))
                        .and(qCarnetBono.estado.eq("OK"))
                        .and(qCarnetBono.fechaFin.isNull().or(qCarnetBono.fechaFin.goe(new Date()))));
        List<CarnetBonoDTO> lista = query.list(qCarnetBono);
        if (lista.isEmpty()) return Boolean.FALSE;
        return Boolean.TRUE;
    }

    public Boolean tieneTarjeta(Long personaId, String tipoId, Long id) {
        String sql;
        Long bono;
        Long tipoInstalacionId = 0L;

        if (id != null) {
            tipoInstalacionId = new JPAQuery(entityManager)
                    .from(qCarnetBonoTipo)
                    .where(qCarnetBonoTipo.id.eq(id))
                    .singleResult(qCarnetBonoTipo.tipoInstalacionId);
        }

        if (tipoId == null) {
            sql = "select count(*) " +
                    "from se_carnets_bonos cb, se_carnets_bonos_tipos cbt " +
                    "where cb.persona_id = :personaId " +
                    "and cb.tipo_id = cbt.id " +
                    "and trunc(sysdate) <= trunc(cb.fecha_fin) " +
                    "and trunc(nvl(cb.fecha_baja, sysdate + 1)) > trunc(sysdate) " +
                    "and dame_usos (cb.id) < nvl(cbt.usos, 999999)";
        } else if (tipoId.equals("TZ")) {
            sql = "select count(*) " +
                    "from se_carnets_bonos cb, se_carnets_bonos_tipos cbt " +
                    "where cb.persona_id = :personaId " +
                    "and cb.tipo_id = cbt.id " +
                    "and nvl(cbt.dias, 0) <> 0 ";

            sql += id == null ? "and cbt.tipo_instalacion_id is not null " : "and cbt.tipo_instalacion_id = " + tipoInstalacionId + " ";

            sql += "and trunc(sysdate) <= trunc(cb.fecha_fin) " +
                    "and trunc(nvl(cb.fecha_baja, sysdate + 1)) > trunc(sysdate) " +
                    "and dame_usos (cb.id) < nvl(cbt.usos, 999999)";
        } else if (tipoId.equals("BO")) {
            sql = "select count(*) " +
                    "from se_carnets_bonos cb, se_carnets_bonos_tipos cbt " +
                    "where cb.persona_id = :personaId " +
                    "and cb.tipo_id = cbt.id " +
                    "and nvl(cbt.usos, 0) > 0 ";

            sql += id == null ? "and cbt.tipo_instalacion_id is not null " : "and cbt.tipo_instalacion_id = " + tipoInstalacionId + " ";

            sql += "and trunc(sysdate) <= trunc(cb.fecha_fin) " +
                    "and trunc(nvl(cb.fecha_baja, sysdate + 1)) > trunc(sysdate) " +
                    "and dame_usos (cb.id) < nvl(cbt.usos, 999999)";
        } else if (tipoId.equals("TE")) {
            sql = "select count(*) " +
                    "from se_tarjetas_deportivas td " +
                    "where td.persona_id = :personaId " +
                    "and trunc(sysdate) between td.fecha_alta and nvl(td.fecha_baja, sysdate + 1)";
        } else if (tipoId.equals("TEI")) {
            sql = "select count(*) " +
                    "from se_tarjetas_deportivas td join se_tarjetas_deportivas_tipos tdt on td.tarjeta_deportiva_tipo_id = tdt.id " +
                    "where td.persona_id = :personaId " +
                    "and trunc(sysdate) between td.fecha_alta and nvl(td.fecha_baja, sysdate + 1) " +
                    "and tdt.activa = 1 " +
                    "and tdt.permite_instalaciones = 1";
        } else {
            sql = "select count(*) " +
                    "from se_carnets_bonos cb, se_carnets_bonos_tipos cbt " +
                    "where cb.persona_id = :personaId " +
                    "and nvl(cbt.dias, 0) > 0 " +
                    "and cb.tipo_id = cbt.id " +
                    "and cbt.tipo_instalacion_id is null " +
                    "and trunc(sysdate) <= trunc(cb.fecha_fin) " +
                    "and trunc(nvl(cb.fecha_baja, sysdate + 1)) > trunc(sysdate) " +
                    "and dame_usos(cb.id) < nvl(cbt.usos, 999999)";
        }

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("personaId", personaId);
        bono = Utils.convierteBigDecimalEnLong((BigDecimal) query.getSingleResult());
        return bono > 0L;
    }

    public Boolean tieneTarjetaDeportiva(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCarnetBono).join(qCarnetBono.tipo, qCarnetBonoTipo)
                .where(qCarnetBono.personaId.eq(personaId)
                        .and(qCarnetBonoTipo.dias.gt(0)
                                .and(qCarnetBonoTipo.tipoInstalacionId.isNull()
                                        .and(qCarnetBonoTipo.activo))));
        return query.list(qCarnetBono).stream()
                .filter(carnet -> {
                    Date fechaFinCarnetBono = carnet.getFechaFin();
                    if (fechaFinCarnetBono == null) {
                        fechaFinCarnetBono = new Date();
                    }
                    Calendar calendarFechaFinCarnetBono = Calendar.getInstance();
                    calendarFechaFinCarnetBono.setTime(fechaFinCarnetBono);
                    calendarFechaFinCarnetBono.add(Calendar.DAY_OF_YEAR, 16);
                    calendarFechaFinCarnetBono.getTime();
                    Date fechaFinHoy = new Date();
                    Calendar calendarFechaFinHoy = Calendar.getInstance();
                    calendarFechaFinHoy.setTime(fechaFinHoy);
                    calendarFechaFinHoy.add(Calendar.DAY_OF_YEAR, 15);
                    return calendarFechaFinHoy.before(calendarFechaFinCarnetBono);

                })
                .anyMatch(carnet -> carnet.estadoTarjetaBono().equals("OK"));
    }

    public List<CarnetBonoTipo> getTarjetasBonosTiposPuedeDarseDeAlta(Long personaId) {
        QCarnetBonoTipoDTO qCarnetBonoTipoDTO = QCarnetBonoTipoDTO.carnetBonoTipoDTO;
        Date fechaNacimiento = personaDAO.getPersonaById(personaId).getFechaNacimiento();
        Date hoy = new Date();
        Calendar nacimiento = Calendar.getInstance();
        nacimiento.setTime(fechaNacimiento);
        Calendar calendarHoy = Calendar.getInstance();
        int edad = calendarHoy.get(Calendar.YEAR) - nacimiento.get(Calendar.YEAR);

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCarnetBonoTipoDTO)
                .where(qCarnetBonoTipoDTO.activo.isTrue()
                        .and((qCarnetBonoTipo.fechaInicio.before(hoy)).or(qCarnetBonoTipo.fechaInicio.isNull()))
                        .and((qCarnetBonoTipo.fechaFin.after(hoy)).or(qCarnetBonoTipo.fechaFin.isNull()))
                        .and((qCarnetBonoTipo.edadMin.loe(edad)).or(qCarnetBonoTipo.edadMin.isNull()))
                        .and((qCarnetBonoTipo.edadMax.goe(edad)).or(qCarnetBonoTipo.edadMax.isNull())));
        return query.list(new QCarnetBonoTipo(qCarnetBonoTipo.id,
                qCarnetBonoTipo.tipoInstalacionId,
                qCarnetBonoTipo.nombre,
                qCarnetBonoTipo.dias,
                qCarnetBonoTipo.usos,
                qCarnetBonoTipo.fechaInicioValidez,
                qCarnetBonoTipo.fechaFinValidez));
    }

    public TarjetaBono getTarjetaBonoById(Long id) {
        return new JPAQuery(entityManager).from(qCarnetBonoVW).where(qCarnetBonoVW.id.eq(id))
                .singleResult(new QTarjetaBono(qCarnetBonoVW.id,
                        qCarnetBonoVW.tipoId,
                        qCarnetBonoVW.tipoInstalacionId,
                        qCarnetBonoVW.personaId,
                        qCarnetBonoVW.usos,
                        qCarnetBonoVW.totalUsos,
                        qCarnetBonoVW.activo,
                        qCarnetBonoVW.dias,
                        qCarnetBonoVW.neto,
                        qCarnetBonoVW.plazos,
                        qCarnetBonoVW.estado,
                        qCarnetBonoVW.nombre,
                        qCarnetBonoVW.observaciones,
                        qCarnetBonoVW.origen,
                        qCarnetBonoVW.fechaInicio,
                        qCarnetBonoVW.fechaFin,
                        qCarnetBonoVW.fechaBaja
                ));
    }
}
