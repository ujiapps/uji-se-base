package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QTarjetaDeportivaSuperiorDTO;
import es.uji.apps.se.dto.TarjetaDeportivaSuperiorDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetaDeportivaSuperiorDAO extends BaseDAODatabaseImpl {
    public List<TarjetaDeportivaSuperiorDTO> getAllByTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId) {

        JPAQuery query = new JPAQuery(entityManager);
        QTarjetaDeportivaSuperiorDTO qTarjetaDeportivaSuperior = QTarjetaDeportivaSuperiorDTO.tarjetaDeportivaSuperiorDTO;
        query.from(qTarjetaDeportivaSuperior).where(qTarjetaDeportivaSuperior.tarjetaDeportivaTipoDTO.id.eq(tarjetaDeportivaTipoId));
        return query.list(qTarjetaDeportivaSuperior);

    }
}
