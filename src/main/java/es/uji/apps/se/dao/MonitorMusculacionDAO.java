package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.MonitorMusculacionDTO;
import es.uji.apps.se.dto.QMonitorMusculacionDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MonitorMusculacionDAO extends BaseDAODatabaseImpl {
    public List<MonitorMusculacionDTO> getMonitores(Long cursoAcademico) {

        JPAQuery query = new JPAQuery(entityManager);

        QMonitorMusculacionDTO qMonitorMusculacion = QMonitorMusculacionDTO.monitorMusculacionDTO;

        query.from(qMonitorMusculacion).where(qMonitorMusculacion.monitorCursoAcademico.cursoAcademicoDTO.id.eq(cursoAcademico));

        return query.list(qMonitorMusculacion);
    }
}
