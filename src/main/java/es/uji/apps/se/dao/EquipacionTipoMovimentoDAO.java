package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.EquipacionTipoMovimientoDTO;
import es.uji.apps.se.dto.QEquipacionTipoMovimientoDTO;
import es.uji.apps.se.dto.QTipoDTO;
import es.uji.apps.se.model.EquipacionTipoMovimiento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository("tipoMovimientoDAO")
public class EquipacionTipoMovimentoDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private QEquipacionTipoMovimientoDTO qEquipacionTipoMovimientoDTO = QEquipacionTipoMovimientoDTO.equipacionTipoMovimientoDTO;
    private QTipoDTO qTipoDTO = QTipoDTO.tipoDTO;

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                            .from(qEquipacionTipoMovimientoDTO)
                            .orderBy(qEquipacionTipoMovimientoDTO.id.asc());

        if(!s.equals("")) {
            try {
                query.where(qEquipacionTipoMovimientoDTO.id.eq(Long.parseLong(s)));
            }
            catch (NumberFormatException e){
                query.where(Utils.limpia(qEquipacionTipoMovimientoDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query
                .list(qEquipacionTipoMovimientoDTO.id,
                      qEquipacionTipoMovimientoDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qEquipacionTipoMovimientoDTO.id).toString());
                    item.setNombre(tuple.get(qEquipacionTipoMovimientoDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }

    public List<EquipacionTipoMovimiento> getTiposMovimiento() {
        return new JPAQuery(entityManager)
                .from(qEquipacionTipoMovimientoDTO)
                .leftJoin(qTipoDTO).on(qTipoDTO.id.eq(qEquipacionTipoMovimientoDTO.tipoId))
                .list(qEquipacionTipoMovimientoDTO.id,
                      qEquipacionTipoMovimientoDTO.nombre,
                      qEquipacionTipoMovimientoDTO.tipoId,
                      qEquipacionTipoMovimientoDTO.etiqueta,
                      qTipoDTO.nombre)
                .stream()
                .map(tuple -> new EquipacionTipoMovimiento(tuple.get(qEquipacionTipoMovimientoDTO.id),
                                                           tuple.get(qEquipacionTipoMovimientoDTO.nombre),
                                                           tuple.get(qEquipacionTipoMovimientoDTO.tipoId),
                                                           tuple.get(qEquipacionTipoMovimientoDTO.etiqueta),
                                                           tuple.get(qTipoDTO.nombre)))
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteTipoMovimiento(Long id) {
        new JPADeleteClause(entityManager, qEquipacionTipoMovimientoDTO)
            .where(qEquipacionTipoMovimientoDTO.id.eq(id))
            .execute();
    }

    public void updateTipoMovimiento(EquipacionTipoMovimiento tipo) {
        this.update(new EquipacionTipoMovimientoDTO(tipo));
    }

    public Boolean isAltaOrDevolucion(Long equipacionTipoMovimientoId) {
        final Long TIPO_ID_ALTA_DEVOLUCION = 9267722L;
        return new JPAQuery(entityManager).from(qEquipacionTipoMovimientoDTO)
                .where(qEquipacionTipoMovimientoDTO.id.eq(equipacionTipoMovimientoId)
                        .and(qEquipacionTipoMovimientoDTO.tipoId.eq(TIPO_ID_ALTA_DEVOLUCION)))
                .singleResult(qEquipacionTipoMovimientoDTO.id) != null;
    }

}
