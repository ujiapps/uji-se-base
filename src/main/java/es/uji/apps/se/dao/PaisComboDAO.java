package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.QPaisComboDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository("paisComboDAO")
public class PaisComboDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private QPaisComboDTO qPaisComboDTO = QPaisComboDTO.paisComboDTO;

    public List<LookupItem> search(String s) {

        JPAQuery query = new JPAQuery(entityManager).from(qPaisComboDTO);

        if (!s.equals("")) {
            query.where(qPaisComboDTO.id.equalsIgnoreCase((s)).or((Utils.limpia(qPaisComboDTO.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)))));
        }

        /**
         * Ordena de forma ascendente y posiciona como primer elemento el paise seleccionado, en este caso España
         * **/
        query.orderBy(
                new OrderSpecifier<>(Order.ASC,
                        Expressions.cases()
                                .when(qPaisComboDTO.id.equalsIgnoreCase("E")).then(0)
                                .otherwise(1)), qPaisComboDTO.nombre.asc()
        );

        return query.list(qPaisComboDTO.id,
                        qPaisComboDTO.nombre)
                .stream()
                .map(tuple -> {
                    LookupItem item = new LookupItem();
                    item.setId(tuple.get(qPaisComboDTO.id));
                    item.setNombre(tuple.get(qPaisComboDTO.nombre));
                    return item;
                }).collect(Collectors.toList());
    }
}
