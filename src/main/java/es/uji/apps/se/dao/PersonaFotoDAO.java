package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.FotoDTO;
import es.uji.apps.se.dto.QFotoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.springframework.stereotype.Repository;

@Repository
public class PersonaFotoDAO extends BaseDAODatabaseImpl  {

    public FotoDTO getPersonaFotoByClienteId(Long personaId) {
        QFotoDTO qFotoDTO = QFotoDTO.fotoDTO;
        return new JPAQuery(entityManager)
                .from(qFotoDTO)
                .where(qFotoDTO.persona.id.eq(personaId))
                .singleResult(qFotoDTO);
    }

}
