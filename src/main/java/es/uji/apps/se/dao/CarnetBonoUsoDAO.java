package es.uji.apps.se.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QTarjetaDeportivaDTO;
import es.uji.apps.se.dto.QTarjetaUsosDTO;
import es.uji.apps.se.model.QTarjetaUsos;
import es.uji.apps.se.model.TarjetaUsos;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class CarnetBonoUsoDAO extends BaseDAODatabaseImpl {
    public List<TarjetaUsos> getTarjetasUsos(Date fechaDesde, Date fechaHasta, String horaDesde, String horaHasta) {

        QTarjetaDeportivaDTO qTarjetaDeportivaDTO = QTarjetaDeportivaDTO.tarjetaDeportivaDTO;
        JPAQuery queryTarjetas = new JPAQuery(entityManager);
        queryTarjetas.from(qTarjetaDeportivaDTO)
                //.where(qTarjetaDeportivaDTO.fechaBaja.goe(fechaDesde).and(qTarjetaDeportivaDTO.fechaAlta.loe(fechaHasta))
                .where(qTarjetaDeportivaDTO.tarjetaDeportivaTipo.permiteClasesDirigidas.isTrue()); //);

        if (ParamUtils.isNotNull(fechaDesde)) {
            queryTarjetas.where(qTarjetaDeportivaDTO.fechaBaja.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            queryTarjetas.where(qTarjetaDeportivaDTO.fechaAlta.loe(fechaHasta));
        }

        List<TarjetaUsos> tarjetaUsos = queryTarjetas.groupBy(qTarjetaDeportivaDTO.tarjetaDeportivaTipo.id, qTarjetaDeportivaDTO.tarjetaDeportivaTipo.nombre)
                .list( new QTarjetaUsos (qTarjetaDeportivaDTO.tarjetaDeportivaTipo.id, qTarjetaDeportivaDTO.tarjetaDeportivaTipo.nombre, qTarjetaDeportivaDTO.id.count()));

        QTarjetaUsosDTO qTarjetaUsos = QTarjetaUsosDTO.tarjetaUsosDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTarjetaUsos);

        if (ParamUtils.isNotNull(fechaDesde)) {
            query.where(qTarjetaUsos.dia.goe(fechaDesde));
        }
        if (ParamUtils.isNotNull(fechaHasta)) {
            query.where(qTarjetaUsos.dia.loe(fechaHasta));
        }

        if (ParamUtils.isNotNull(horaDesde)) {
            query.where(qTarjetaUsos.horaInicio.goe(horaDesde));
        }

        if (ParamUtils.isNotNull(horaHasta)) {
            query.where(qTarjetaUsos.horaFin.loe(horaHasta));
        }

        List<Tuple> tarjetaUsosDto = query.groupBy(qTarjetaUsos.tarjetaDeportivaNombre, qTarjetaUsos.tarjetaDeportivaTipo)
                .list(qTarjetaUsos.tarjetaDeportivaTipo, qTarjetaUsos.id.count(), qTarjetaUsos.asiste.sum());

        tarjetaUsosDto.forEach(tarjetaUsoDto -> {
            tarjetaUsos.forEach(tarjetaUso -> {
                if (tarjetaUso.getTarjetaTipoId().equals(tarjetaUsoDto.get(qTarjetaUsos.tarjetaDeportivaTipo))){
                    tarjetaUso.setReservas(tarjetaUsoDto.get(qTarjetaUsos.id.count()));
                    tarjetaUso.setUsos(tarjetaUsoDto.get(qTarjetaUsos.asiste.sum()));
                }
            });
        });

        return tarjetaUsos;
    }
}
