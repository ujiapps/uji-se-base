package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.ClaseDirigidaTipoDTO;
import es.uji.apps.se.dto.QClaseDirigidaTipoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ClaseDirigidaTiposDAO extends BaseDAODatabaseImpl {

    private QClaseDirigidaTipoDTO qClaseDirigidaTipo = QClaseDirigidaTipoDTO.claseDirigidaTipoDTO;

    public List<ClaseDirigidaTipoDTO> getClasesDirigidasTipoByTipo(Long tipoId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qClaseDirigidaTipo).where(qClaseDirigidaTipo.tipo.id.eq(tipoId));

        return query.list(qClaseDirigidaTipo);
    }

    @Transactional
    public void deleteClaseDirigidaTipoByClaseId(Long claseId) {

        JPADeleteClause query = new JPADeleteClause(entityManager, qClaseDirigidaTipo);

        query.where(qClaseDirigidaTipo.clase.id.eq(claseId)).execute();
    }
}
