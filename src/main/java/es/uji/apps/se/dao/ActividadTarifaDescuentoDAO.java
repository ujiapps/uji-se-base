package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.se.dto.ActividadTarifaDescuentoDTO;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.QActividadTarifaDescuentoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ActividadTarifaDescuentoDAO extends BaseDAODatabaseImpl {

    private QActividadTarifaDescuentoDTO qActividadTarifaDescuento = QActividadTarifaDescuentoDTO.actividadTarifaDescuentoDTO;
    private Map<String, Path> relations = new HashMap<String, Path>();

    public ActividadTarifaDescuentoDAO() {
        relations.put("id", qActividadTarifaDescuento.id);
        relations.put("tarjetaDeportivaId", qActividadTarifaDescuento.tarjetaDeportiva.nombre);
        relations.put("importe", qActividadTarifaDescuento.importe);
        relations.put("fechaInicio", qActividadTarifaDescuento.fechaInicio);
        relations.put("fechaFin", qActividadTarifaDescuento.fechaFin);
    }

    public List<ActividadTarifaDescuentoDTO> getDescuentosTarifas(Long actividadTarifaId, Paginacion paginacion) {


        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActividadTarifaDescuento).where(qActividadTarifaDescuento.actividadTarifaDTO.id.eq(actividadTarifaId));

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qActividadTarifaDescuento);
    }
}
