package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.CompeticionMiembro;
import es.uji.apps.se.model.QCompeticionMiembro;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompeticionMiembroDAO extends BaseDAODatabaseImpl {
    public List<CompeticionMiembro> getCompeticionMiembrosByEquipoId(Long equipoId, Long partidoId) {
        QCompeticionMiembroDTO qCompeticionMiembroDTO = QCompeticionMiembroDTO.competicionMiembroDTO;
        QInscripcionDTO qInscripcionDTO = QInscripcionDTO.inscripcionDTO;
        QPersonaUjiDTO qPersonaUjiDTO = QPersonaUjiDTO.personaUjiDTO;
        QFotoDTO qFotoDTO = QFotoDTO.fotoDTO;
        QCompeticionEquipoDTO qCompeticionEquipoDTO = QCompeticionEquipoDTO.competicionEquipoDTO;
        QActividadAsistenciaDTO qActividadAsistenciaDTO = QActividadAsistenciaDTO.actividadAsistenciaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCompeticionMiembroDTO)
                .join(qCompeticionMiembroDTO.inscripcion, qInscripcionDTO)
                .join(qInscripcionDTO.personaUjiDTO, qPersonaUjiDTO)
                .join(qCompeticionMiembroDTO.competicionEquipo, qCompeticionEquipoDTO)
                .leftJoin(qPersonaUjiDTO.fotosDTO, qFotoDTO)
                .leftJoin(qInscripcionDTO.actividadAsistencias, qActividadAsistenciaDTO).on(qActividadAsistenciaDTO.partido.id.eq(partidoId))
                .where(qCompeticionMiembroDTO.competicionEquipo.id.eq(equipoId));

        return query.list(new QCompeticionMiembro(qCompeticionMiembroDTO.id, qInscripcionDTO.id, qPersonaUjiDTO.nombre, qPersonaUjiDTO.apellido1, qPersonaUjiDTO.apellido2, qCompeticionEquipoDTO.nombre,
                qFotoDTO.foto, qFotoDTO.mimeType, qActividadAsistenciaDTO.id));
    }
}
