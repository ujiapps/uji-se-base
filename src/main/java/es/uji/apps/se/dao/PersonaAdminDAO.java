package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;
import es.uji.apps.se.dto.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonaAdminDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private QPersonaAdminDTO qPersonaAdminDTO = QPersonaAdminDTO.personaAdminDTO;

    private StringExpression limpiaAcentos(StringPath nombre) {
        return Expressions.stringTemplate(
                "upper(translate({0}, 'âàãáÁÂÀÃéèêÉÈÊíÍóôõòÓÔÕÒüúÜÚ', 'AAAAAAAAEEEEEEIIOOOOOOOOUUUU'))",
                nombre);
    }

    @Override
    public List<LookupItem> search(String query) {
        JPAQuery qQueryPersonas = new JPAQuery(entityManager);

        QPersonaUjiDTO qPersonaUjiDTO = QPersonaUjiDTO.personaUjiDTO;

        String q = StringUtils.limpiaAcentos(query).trim().toUpperCase();

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (q != null && !q.isEmpty()) {
            qQueryPersonas.from(qPersonaAdminDTO).join(qPersonaAdminDTO.personaUjiDTO, qPersonaUjiDTO)
                    .where(limpiaAcentos(qPersonaUjiDTO.identificacion).containsIgnoreCase(q)
                            .or(limpiaAcentos(qPersonaUjiDTO.nombre).concat(" ").concat(limpiaAcentos(qPersonaUjiDTO.apellido1).concat(" ").concat(limpiaAcentos(qPersonaUjiDTO.apellido2))).containsIgnoreCase(q))
                            .or(limpiaAcentos(qPersonaUjiDTO.mail).substring(0, qPersonaUjiDTO.mail.indexOf("@")).containsIgnoreCase(q)));

            List<PersonaAdminDTO> listaPersonasUji = qQueryPersonas.list(qPersonaAdminDTO);

            for (PersonaAdminDTO personaAdmin : listaPersonasUji) {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(personaAdmin.getPersonaUjiDTO().getId()));
                lookupItem.setNombre(personaAdmin.getPersonaUjiDTO().getApellidosNombre());

                result.add(lookupItem);
            }
        }

        return result;
    }
}
