package es.uji.apps.se.dao;

import java.util.List;

import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.se.dto.*;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.se.model.Paginacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClaseDAO extends BaseDAODatabaseImpl {

    private final QClaseDTO qClase = QClaseDTO.claseDTO;
    private final QCalendarioClaseDTO qCalendarioClaseDTO = QCalendarioClaseDTO.calendarioClaseDTO;
    private final QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;
    private final QCursoAcademicoDTO qCursoAcademicoDTO = QCursoAcademicoDTO.cursoAcademicoDTO;

    public List<ClaseDTO> getClases(Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qClase);

        paginacion.setTotalCount(query.count());

        if (ParamUtils.isNotNull(paginacion.getLimit())) {
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        query.orderBy(ordenaQuery(paginacion));
        return query.list(qClase);
    }

    public List<ClaseDTO> getClasesParaExport() {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qClase);
        return query.list(qClase);
    }

    private OrderSpecifier<?> ordenaQuery(Paginacion paginacion) {
        if (ParamUtils.isNotNull(paginacion.getOrdenarPor())) {
            if (paginacion.getOrdenarPor().equals("nombre")) {
                return paginacion.getDireccion().equals("DESC") ? qClase.nombre.desc() : qClase.nombre.asc();
            }

            if (paginacion.getOrdenarPor().equals("descripcion")) {
                return paginacion.getDireccion().equals("DESC") ? qClase.descripcion.desc() : qClase.descripcion.asc();
            }

            if (paginacion.getOrdenarPor().equals("activa")) {
                return paginacion.getDireccion().equals("DESC") ? qClase.activa.desc() : qClase.activa.asc();
            }

            if (paginacion.getOrdenarPor().equals("plazas")) {
                return paginacion.getDireccion().equals("DESC") ? qClase.plazas.desc() : qClase.plazas.asc();
            }
        }
        return qClase.nombre.desc();
    }

    public List<ClaseDTO> getClasesActivas() {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qClase).where(qClase.activa.isTrue())
                .orderBy(qClase.nombre.asc());

        return query.list(qClase);

    }

    public Long getRecuentoClasesDirigidas(Long cursoAcademico) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qClase)
                .join(qCalendarioClaseDTO).on(qClase.id.eq(qCalendarioClaseDTO.claseDTO.id))
                .join(qCalendarioDTO).on(qCalendarioClaseDTO.calendarioDTO.id.eq(qCalendarioDTO.id))
                .join(qCursoAcademicoDTO).on(qCalendarioDTO.dia.between(qCursoAcademicoDTO.fechaInicio, qCursoAcademicoDTO.fechaFin))
                .where(qCursoAcademicoDTO.id.eq(cursoAcademico));
        return query.singleResult(qClase.id.countDistinct());
    }

    public Long getRecuentoClasesDirigidasActivas() {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qClase).where(qClase.activa.isTrue());
        return query.singleResult(qClase.id.countDistinct());
    }
}
