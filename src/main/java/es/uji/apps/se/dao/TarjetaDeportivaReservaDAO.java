package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QTarjetaDeportivaReservaDTO;
import es.uji.apps.se.dto.TarjetaDeportivaReservaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetaDeportivaReservaDAO extends BaseDAODatabaseImpl
{
    public List<TarjetaDeportivaReservaDTO> getReservasByTarjetaDeportivaId(Long tarjetaDeportivaId)
    {
            QTarjetaDeportivaReservaDTO qTarjetaDeportivaReserva = QTarjetaDeportivaReservaDTO.tarjetaDeportivaReservaDTO;
            JPAQuery query = new JPAQuery(entityManager);
            query.from(qTarjetaDeportivaReserva).where(qTarjetaDeportivaReserva.tarjetaDeportivaDTO.id.eq(tarjetaDeportivaId));
            return query.distinct().list(qTarjetaDeportivaReserva);
    }
}
