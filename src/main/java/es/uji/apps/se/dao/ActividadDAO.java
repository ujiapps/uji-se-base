package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;
import es.uji.apps.se.dto.*;
import es.uji.apps.se.model.ActividadPersona;
import es.uji.apps.se.model.Oferta;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.model.QOferta;
import es.uji.apps.se.model.filtros.FiltroActividades;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("actividadDAO")
public class ActividadDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    private final QActividadDTO qActividad = QActividadDTO.actividadDTO;
    private final QOfertaDTO qOfertaDTO = QOfertaDTO.ofertaDTO;
    private final QPeriodoDTO qPeriodoDTO = QPeriodoDTO.periodoDTO;
    private final QCalendarioOfertaDTO qCalendarioOfertaDTO = QCalendarioOfertaDTO.calendarioOfertaDTO;
    private final QActividadTipoDTO qActividadTipo = QActividadTipoDTO.actividadTipoDTO;
    private final QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public ActividadDAO() {
        relations.put("id", qActividad.id);
        relations.put("nombre", qActividad.nombre);
        relations.put("descripcion", qActividad.descripcion);
        relations.put("plazas", qActividad.plazas);
        relations.put("activa", qActividad.activa);
        relations.put("permiteDuplicidadInscripcion", qActividad.permiteDuplicidadInscripcion);
        relations.put("edadMinimaInscripcion", qActividad.edadMinimaInscripcion);
        relations.put("mostrarComentarioAsistencias", qActividad.mostrarComentarioAsistencias);
    }

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qActividad);

        if (!s.isEmpty()) {
            try {
                query.where(qActividad.id.eq(Long.parseLong(s)));
            } catch (NumberFormatException e) {
                query.where(Utils.limpia(qActividad.nombre).containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query
                .list(qActividad)
                .stream()
                .map(qActividad -> {
                    LookupItem item = new LookupItem();
                    item.setId(qActividad.getId().toString());
                    item.setNombre(qActividad.getNombre());
                    return item;
                }).collect(Collectors.toList());
    }

    public String limpiaAcentos(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim().toUpperCase();
    }

    private StringExpression limpiaAcentos(StringPath nombre) {
        return Expressions.stringTemplate(
                "upper(translate({0}, 'âàãáÁÂÀÃéèêÉÈÊíÍóôõòÓÔÕÒüúÜÚ', 'AAAAAAAAEEEEEEIIOOOOOOOOUUUU'))",
                nombre);
    }

    public List<ActividadDTO> getActividades(FiltroActividades filtroActividades, Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActividad);

        if (ParamUtils.isNotNull(filtroActividades)) {
            if (ParamUtils.isNotNull(filtroActividades.getPeriodoId())) {
                QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;
                query.join(qActividad.ofertasDTO, qOferta).where(qOferta.periodoDTO.id.eq(filtroActividades.getPeriodoId())).distinct();
            }

            if (ParamUtils.isNotNull(filtroActividades.getClasificacionId())) {
                QActividadTipoDTO qActividadTipo = QActividadTipoDTO.actividadTipoDTO;
                query.join(qActividad.actividadTiposDTO, qActividadTipo).where(qActividadTipo.tipoDTO.id.eq(filtroActividades.getClasificacionId())).distinct();
            }

            if (ParamUtils.isNotNull(filtroActividades.getBusquedaNombre())) {
                query.where(limpiaAcentos(qActividad.nombre).upper().like('%' + filtroActividades.getBusquedaNombre().toUpperCase() + '%'));
            }

        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            query.orderBy(qActividad.nombre.asc());
        }

        return query.list(qActividad);
    }

    public ActividadDTO getActividadById(Long id) {
        if (id != null)
            return new JPAQuery(entityManager)
                    .from(qActividad)
                    .where(qActividad.id.eq(id))
                    .singleResult(qActividad);
        return null;
    }

    public List<Oferta> getActividadesEnvio(FiltroActividades filtroActividades) {

        JPAQuery query = new JPAQuery(entityManager);

        QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;

        query.from(qActividad).join(qActividad.ofertasDTO, qOferta);

        if (ParamUtils.isNotNull(filtroActividades.getPeriodoId())) {
            query.where(qOferta.periodoDTO.id.eq(filtroActividades.getPeriodoId()));
        }

        if (ParamUtils.isNotNull(filtroActividades.getClasificacionId())) {
            QActividadTipoDTO qActividadTipo = QActividadTipoDTO.actividadTipoDTO;
            query.join(qActividad.actividadTiposDTO, qActividadTipo).where(qActividadTipo.tipoDTO.id.eq(filtroActividades.getClasificacionId()));
        }

        if (ParamUtils.isNotNull(filtroActividades.getActividadId())) {
            query.where(qActividad.id.eq(filtroActividades.getActividadId()));
        }

        if (ParamUtils.isNotNull(filtroActividades.getGrupoId())) {
            query.where(qOferta.id.eq(filtroActividades.getGrupoId()));
        }

        if (ParamUtils.isNotNull(filtroActividades.getBusquedaNombre())) {
            query.where(qActividad.nombre.upper().containsIgnoreCase(limpiaAcentos(filtroActividades.getBusquedaNombre())));
        }

        return query.orderBy(qActividad.nombre.asc()).orderBy(qOferta.nombre.asc()).list(new QOferta(qOferta.id, qOferta.actividadDTO.id));
    }

    public List<Oferta> getGruposActividad(FiltroActividades filtroActividades) {
        JPAQuery query = new JPAQuery(entityManager);

        QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;

        query.from(qActividad).join(qActividad.ofertasDTO, qOferta);

        if (ParamUtils.isNotNull(filtroActividades.getPeriodoId())) {
            query.where(qOferta.periodoDTO.id.eq(filtroActividades.getPeriodoId()));
        }

        if (ParamUtils.isNotNull(filtroActividades.getClasificacionId())) {
            QActividadTipoDTO qActividadTipo = QActividadTipoDTO.actividadTipoDTO;
            query.join(qActividad.actividadTiposDTO, qActividadTipo).where(qActividadTipo.tipoDTO.id.eq(filtroActividades.getClasificacionId()));
        }

        if (ParamUtils.isNotNull(filtroActividades.getActividadId())) {
            query.where(qActividad.id.eq(filtroActividades.getActividadId()));
        }

        return query.orderBy(qActividad.nombre.asc()).orderBy(qOferta.nombre.asc()).list(new QOferta(qOferta.id, qOferta.actividadDTO.id, qOferta.nombre));
    }

    public Long getActividadesConProfesorado(Long cursoAcademico) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActividad)
                .join(qOfertaDTO).on(qActividad.id.eq(qOfertaDTO.actividadDTO.id))
                .join(qPeriodoDTO).on(qOfertaDTO.periodoDTO.id.eq(qPeriodoDTO.id))
                .join(qCalendarioOfertaDTO).on(qCalendarioOfertaDTO.ofertaDTO.id.eq(qOfertaDTO.id))
                .where(qPeriodoDTO.cursoAcademicoDTO.id.eq(cursoAcademico));
        return query.singleResult(qActividad.id.countDistinct());
    }

    public String getActividadByReferenciaId(Long id) {
        if (id != null)
            return new JPAQuery(entityManager)
                    .from(qOfertaDTO)
                    .join(qActividad).on(qActividad.id.eq(qOfertaDTO.actividadDTO.id))
                    .where(qOfertaDTO.id.eq(id))
                    .singleResult(qActividad.nombre, qOfertaDTO.nombre).toString();
        return null;
    }

    public List<ActividadDTO> getActividadesCurso(Long cursoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActividad)
                .join(qOfertaDTO).on(qActividad.id.eq(qOfertaDTO.actividadDTO.id))
                .join(qPeriodoDTO).on(qOfertaDTO.periodoDTO.id.eq(qPeriodoDTO.id))
                .where(qActividad.activa.eq(true)
                        .and(qPeriodoDTO.cursoAcademicoDTO.id.eq(cursoId)))
                .orderBy(qActividad.nombre.asc())
                .distinct();

        return query.list(qActividad);
    }

    public List<Oferta> getGruposActividadCurso(Long actividadId, Long cursoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qOfertaDTO)
                .join(qPeriodoDTO).on(qOfertaDTO.periodoDTO.id.eq(qPeriodoDTO.id))
                .where(qPeriodoDTO.cursoAcademicoDTO.id.eq(cursoId)
                        .and(qOfertaDTO.actividadDTO.id.eq(actividadId)))
                .orderBy(qPeriodoDTO.nombre.asc(), qOfertaDTO.nombre.asc());

        return query.list(qOfertaDTO.id,
                        qOfertaDTO.nombre,
                        qOfertaDTO.actividadDTO.id,
                        qPeriodoDTO.nombre)
                .stream().map(tuple ->
                        new Oferta(
                                tuple.get(qOfertaDTO.id),
                                tuple.get(qOfertaDTO.actividadDTO.id),
                                tuple.get(qPeriodoDTO.nombre) + " - Grup " + tuple.get(qOfertaDTO.nombre)
                        )
                ).collect(Collectors.toList());
    }

    public List<ActividadPersona> getActividades(Long personaId, Long periodoId, Long tipoId) {
        StringExpression puedeInscripcionActRap = Expressions.stringTemplate("puede_inscripcion_act_rap({0}, {1}, {2})", personaId, periodoId, qActividadTipo.actividadDTO.id);

        return new JPAQuery(entityManager)
                .from(qOferta, qActividad, qActividadTipo)
                .where(qOferta.periodoDTO.id.eq(periodoId)
                        .and(qOferta.actividadDTO.id.eq(qActividad.id))
                        .and(qActividad.id.eq(qActividadTipo.actividadDTO.id))
                        .and(qActividad.activa.eq(true))
                        .and(qActividadTipo.tipoDTO.id.eq(tipoId))
                        .and(puedeInscripcionActRap.eq("S")))
                .orderBy(qActividad.nombre.asc())
                .distinct()
                .list(qActividad.nombre, qActividad.id)
                .stream()
                .map(tuple -> new ActividadPersona(tuple.get(qActividad.id), tuple.get(qActividad.nombre)))
                .collect(Collectors.toList());
    }

    public List<ActividadDTO> getActividadesParaCsv(Long periodoId, Long clasificacionId, String busquedaNombre) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActividad);
        if (ParamUtils.isNotNull(periodoId)) {
            QOfertaDTO qOferta = QOfertaDTO.ofertaDTO;
            query.join(qActividad.ofertasDTO, qOferta).where(qOferta.periodoDTO.id.eq(periodoId)).distinct();
        }

        if (ParamUtils.isNotNull(clasificacionId)) {
            QActividadTipoDTO qActividadTipo = QActividadTipoDTO.actividadTipoDTO;
            query.join(qActividad.actividadTiposDTO, qActividadTipo).where(qActividadTipo.tipoDTO.id.eq(clasificacionId)).distinct();
        }

        if (ParamUtils.isNotNull(busquedaNombre)) {
            query.where(limpiaAcentos(qActividad.nombre).upper().like('%' + busquedaNombre.toUpperCase() + '%'));
        }
        return query.list(qActividad);
    }
}
