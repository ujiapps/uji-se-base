package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.se.dto.PreinscripcionDTO;
import es.uji.apps.se.dto.QPreinscripcionDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class PersonaRenovacionInscripcionDAO extends BaseDAODatabaseImpl {

    @InjectParam
    InscripcionDAO inscripcionDAO;

    private final QPreinscripcionDTO qPreinscripcion = QPreinscripcionDTO.preinscripcionDTO;
    private final Notificacion notificacion = new Notificacion();

    public void fichaRenovacion(Long personaId, Long ofertaId, String actividad) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaWeb = null;

        String sql = "select nvl(o.fecha_ini_web, p.fecha_ini_web), nvl(o.fecha_fin_renovacion, p.fecha_fin_renovacion) " +
                "from se_actividades_oferta o, se_periodos p " +
                "where o.id = ?1 " +
                "and o.periodo_id = p.id " +
                "group by nvl(o.fecha_ini_web, p.fecha_ini_web), p.fecha_ini_ins, nvl(o.fecha_fin_renovacion, p.fecha_fin_renovacion)";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, ofertaId);
        List<Object[]> resultado = query.getResultList();

        for (Object[] f : resultado) {
            fechaWeb = (Date) f[0];
        }

        if (fechaWeb != null && formato.format(fechaWeb).compareTo(formato.format(new Date())) <= 0) {
            preincripcionPersona(personaId, ofertaId, actividad == null ? "USUARIO" : "EXTERNA");
        }
    }

    @Transactional
    public void preincripcionPersona(Long personaId, Long ofertaId, String origen) {
        String sql = "select nvl(o.fecha_fin_renovacion, p.fecha_fin_renovacion) " +
                "from se_actividades_oferta o, se_periodos p " +
                "where o.id = ?1 " +
                "and o.periodo_id = p.id " +
                "group by p.fecha_ini_ins, nvl(o.fecha_fin_renovacion, p.fecha_fin_renovacion)";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, ofertaId);
        Date fecha = (Date) query.getSingleResult();

        Long numPreinscripciones = new JPAQuery(entityManager)
                .from(qPreinscripcion)
                .where(qPreinscripcion.ofertaId.eq(ofertaId)
                        .and(qPreinscripcion.personaId.eq(personaId)
                                .and(qPreinscripcion.tipo.eq("CA"))
                                .and(qPreinscripcion.estadoId.eq(7495381L))))
                .count();

        if (numPreinscripciones == 0) {
            String importe = inscripcionDAO.dameImpInscPer(ofertaId, personaId, "IMPORTE", new Date());
            PreinscripcionDTO preinscripcionDTO = new PreinscripcionDTO();
            preinscripcionDTO.setOfertaId(ofertaId);
            preinscripcionDTO.setPersonaId(personaId);
            preinscripcionDTO.setEstadoId(7495381L);
            preinscripcionDTO.setTipo("CA");
            preinscripcionDTO.setFechaInicioValida(fecha);
            preinscripcionDTO.setImporte(Float.valueOf(importe));
            preinscripcionDTO.setOrigen(origen);
            this.insert(preinscripcionDTO);

            notificacion.notificaRenovacion(ofertaId, 131864L, "");
        } else {
            new JPADeleteClause(entityManager, qPreinscripcion)
                    .where(qPreinscripcion.ofertaId.eq(ofertaId)
                            .and(qPreinscripcion.personaId.eq(personaId)))
                    .execute();

            notificacion.notificaAulaRenovacion(ofertaId, 131864L);
        }
    }
}
