package es.uji.apps.se.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;
import es.uji.apps.se.dto.EnvioDTO;
import es.uji.apps.se.dto.QPerfilPersonaDTO;
import es.uji.apps.se.model.Envio;
import es.uji.apps.se.model.Paginacion;
import es.uji.apps.se.dto.QEnvioDTO;
import es.uji.apps.se.model.QEnvio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EnvioDAO extends BaseDAODatabaseImpl {

    public String limpiaAcentos(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim();
    }

    private StringExpression limpiaAcentos(StringPath nombre) {
        return Expressions.stringTemplate(
                "upper(translate({0}, 'âàãáÁÂÀÃéèêÉÈÊíÍóôõòÓÔÕÒüúÜÚ', 'AAAAAAAAEEEEEEIIOOOOOOOOUUUU'))",
                nombre);
    }

    private QEnvioDTO qEnvio = QEnvioDTO.envioDTO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public EnvioDAO() {
        relations.put("id", qEnvio.id);
        relations.put("nombre", qEnvio.nombre);
        relations.put("fecha", qEnvio.fecha);
        relations.put("fechaEnvio", qEnvio.fechaEnvio);
    }


    public List<Envio> getEnvios(Long connectedUserId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvio).where(qEnvio.personaCrea.id.eq(connectedUserId));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return query.list(new QEnvio(qEnvio.id, qEnvio.nombre, qEnvio.fecha, qEnvio.fechaEnvio));
    }

    public EnvioDTO getEnvioById(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvio).where(qEnvio.id.eq(envioId));

        if (query.list(qEnvio).isEmpty())
            return null;
        else return query.list(qEnvio).get(0);
    }

    public List<Envio> getEnviosByBusqueda(Long perfil, String asunto, Long persona, Long connectedUserId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        QPerfilPersonaDTO qPerfilPersona = QPerfilPersonaDTO.perfilPersonaDTO;

        query.from(qEnvio);

        if (!ParamUtils.isNotNull(perfil) && !ParamUtils.isNotNull(asunto) && !ParamUtils.isNotNull(persona)){
            query.where(qEnvio.personaCrea.id.eq(connectedUserId));
        }

        if (ParamUtils.isNotNull(perfil)) {
            query.where(qEnvio.personaCrea.id.in(new JPASubQuery().from(qPerfilPersona).where(qPerfilPersona.perfilDTO.id.eq(perfil)).list(qPerfilPersona.persona)));
        }

        if (ParamUtils.isNotNull(asunto)) {
            query.where(limpiaAcentos(qEnvio.nombre).containsIgnoreCase(limpiaAcentos(asunto)));
        }

        if (ParamUtils.isNotNull(persona)){
            query.where(qEnvio.personaCrea.id.eq(persona));
        }

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return query.list(new QEnvio(qEnvio.id, qEnvio.nombre, qEnvio.fecha, qEnvio.fechaEnvio));
    }
}
