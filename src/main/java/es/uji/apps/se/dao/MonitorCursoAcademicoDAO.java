package es.uji.apps.se.dao;


import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MonitorCursoAcademicoDAO extends BaseDAODatabaseImpl {

    private QMonitorCursoAcademicoDTO qMonitorCursoAcademico = QMonitorCursoAcademicoDTO.monitorCursoAcademicoDTO;

    public List<MonitorCursoAcademicoDTO> getMonitoresCursoAcademicoByOferta(OfertaDTO ofertaDTO) {
        JPAQuery query = new JPAQuery(entityManager);

        QMonitorOfertaDTO qMonitorOferta = QMonitorOfertaDTO.monitorOfertaDTO;
        QCursoAcademicoDTO qCursoAcademico = QCursoAcademicoDTO.cursoAcademicoDTO;

        query.from(qMonitorCursoAcademico)
                .join(qMonitorCursoAcademico.monitoresOferta, qMonitorOferta)
                .join(qMonitorCursoAcademico.cursoAcademicoDTO, qCursoAcademico)
                .where(qMonitorOferta.oferta.id.eq(ofertaDTO.getId())
                        .and((qMonitorCursoAcademico.fechaBaja.isNull().or(qMonitorCursoAcademico.fechaBaja.after(qCursoAcademico.fechaFin)))));

        return query.list(qMonitorCursoAcademico);

    }

    public MonitorCursoAcademicoDTO getMonitorCursoByMonitorAndCurso(MonitorDTO monitorDTO, CursoAcademicoDTO cursoAcademicoDTO) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qMonitorCursoAcademico)
                .where(qMonitorCursoAcademico.cursoAcademicoDTO.id.eq(cursoAcademicoDTO.getId())
                        .and(qMonitorCursoAcademico.monitorDTO.id.eq(monitorDTO.getId())));

        return query.uniqueResult(qMonitorCursoAcademico);
    }

    public List<MonitorCursoAcademicoDTO> getMonitoresByCursoAcademico(Long cursoAcademico)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMonitorDTO qMonitor = QMonitorDTO.monitorDTO;
        QPersonaUjiDTO qPersonaUji = QPersonaUjiDTO.personaUjiDTO;
        QPersonaVinculoDTO qPersonaVinculo = QPersonaVinculoDTO.personaVinculoDTO;
        QCursoAcademicoDTO qCursoAcademico = QCursoAcademicoDTO.cursoAcademicoDTO;
        QMonitorTitulacionDTO qMonitorTitulacion = QMonitorTitulacionDTO.monitorTitulacionDTO;

        query.from(qMonitorCursoAcademico)
                .join(qMonitorCursoAcademico.monitorDTO, qMonitor).fetch()
                .join(qMonitor.personaUji, qPersonaUji).fetch()
                .leftJoin(qMonitor.monitorTitulacion, qMonitorTitulacion).fetch()
                .join(qPersonaUji.personaVinculoDTO, qPersonaVinculo).fetch()
                .join(qMonitorCursoAcademico.cursoAcademicoDTO, qCursoAcademico).fetch()
                .where(qCursoAcademico.id.eq(cursoAcademico))
                .orderBy(qPersonaUji.apellido1.asc())
                .orderBy(qPersonaUji.apellido2.asc())
                .orderBy(qPersonaUji.nombre.asc());

        return query.list(qMonitorCursoAcademico);
    }
}
