package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.QTipoReciboVWDTO;
import es.uji.apps.se.dto.TipoReciboVWDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TipoReciboDAO extends BaseDAODatabaseImpl {
    public List<TipoReciboVWDTO> getTiposRecibo() {
        QTipoReciboVWDTO qTipoReciboVWDTO = QTipoReciboVWDTO.tipoReciboVWDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qTipoReciboVWDTO).list(qTipoReciboVWDTO);
    }
}
