package es.uji.apps.se.dao;

import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class InscripcionPlazasLibresDAO {

    private static DataSource dataSource;

    private PlazasLibres plazasLibres;

    @Autowired
    public void setDataSource(DataSource dataSource){InscripcionPlazasLibresDAO.dataSource = dataSource;}

    @PostConstruct
    public void init(){
        this.plazasLibres = new PlazasLibres(dataSource);
    }

    public String plazasLibresOferta(Long ofertaId) {
        return plazasLibres.plazasLibresOferta(ofertaId);
    }

    private class PlazasLibres extends StoredProcedure {
        private static final String SQL = "uji_sports.plazas_libres";
        private static final String POFERTA = "p_oferta";

        public PlazasLibres(DataSource dataSource){
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(POFERTA, Types.VARCHAR));

            compile();
        }

        public String plazasLibresOferta(Long ofertaId) {
            Map<String, Object> inParams = new HashMap<String, Object>();

            inParams.put(POFERTA, ofertaId);

            Map<String, Object> results = execute(inParams);
            if (ParamUtils.isNotNull(results.get("response")))
            {
                return results.get("response").toString();
            }
            else
            {
                return "";
            }
        }
    }
}
