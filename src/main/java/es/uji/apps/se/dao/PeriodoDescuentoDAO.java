package es.uji.apps.se.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.se.dto.PeriodoDescuentoDTO;
import es.uji.apps.se.dto.QPeriodoDescuentoDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PeriodoDescuentoDAO extends BaseDAODatabaseImpl {
    public List<PeriodoDescuentoDTO> getDescuentosByPeriodo(Long periodo) {
        JPAQuery query = new JPAQuery(entityManager);

        QPeriodoDescuentoDTO qPeriodoDescuento = QPeriodoDescuentoDTO.periodoDescuentoDTO;
        query.from(qPeriodoDescuento).where(qPeriodoDescuento.periodoDTO.id.eq(periodo));
        return query.list(qPeriodoDescuento);
    }
}
