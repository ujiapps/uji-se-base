package es.uji.apps.se.exceptions.maestrosEquipaciones.tipoMovimiento;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoMovimientoUpdateException extends CoreBaseException {

    public TipoMovimientoUpdateException() { super("Error al actualitzar un tipus de moviment"); }

    public TipoMovimientoUpdateException(String mensaje) { super(mensaje); }
}
