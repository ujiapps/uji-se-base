package es.uji.apps.se.exceptions.equipaciones.movimientos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionesMovimientosInsertException extends CoreBaseException {

    public EquipacionesMovimientosInsertException() { super("Error al inserir un nou moviment d'equipació"); }

    public EquipacionesMovimientosInsertException(String mensaje) { super(mensaje); }
}