package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ActividadSinPlazasException extends CoreBaseException
{
    public ActividadSinPlazasException()
    {
        super("L'activitat no té places disponibles");
    }

    public ActividadSinPlazasException(String message)
    {
        super(message);
    }
}