package es.uji.apps.se.exceptions.maestrosEquipaciones.tipoEquipacion;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoEquipacionInsertException extends CoreBaseException {

    public TipoEquipacionInsertException() { super("Error al inserir un tipus d'equipació"); }

    public TipoEquipacionInsertException(String mensaje) { super(mensaje); }
}
