package es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TiposAgrupacionesInsertException extends CoreBaseException {

    public TiposAgrupacionesInsertException() { super("Error al inserir un nou tipus d'agrupació d'equipació"); }

    public TiposAgrupacionesInsertException(String mensaje) { super(mensaje); }
}