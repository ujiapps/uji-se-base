package es.uji.apps.se.exceptions.equipaciones.movimientos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionesMovimientosDeleteException extends CoreBaseException {

    public EquipacionesMovimientosDeleteException() { super("Error al esborrar el moviment d'equipació seleccionat"); }

    public EquipacionesMovimientosDeleteException(String mensaje) { super(mensaje); }
}