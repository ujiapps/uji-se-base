package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class BorradoClaseDirigidaException extends CoreBaseException
{
    public BorradoClaseDirigidaException()
    {
        super("No es pot eliminar el dia del calendari perque té asistencies");
    }

    public BorradoClaseDirigidaException(String message)
    {
        super(message);
    }
}