package es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionesActividadesUpdateException extends CoreBaseException {

    public EquipacionesActividadesUpdateException() { super("Error al actualitzar una nova relació de equipació-activitat"); }

    public EquipacionesActividadesUpdateException(String mensaje) { super(mensaje); }
}
