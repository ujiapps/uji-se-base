package es.uji.apps.se.exceptions.maestrosEquipaciones.modelos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ModelosDeleteException extends CoreBaseException {

    public ModelosDeleteException() { super("Error al esborrar un model d'equipació"); }

    public ModelosDeleteException(String mensaje) { super(mensaje); }
}
