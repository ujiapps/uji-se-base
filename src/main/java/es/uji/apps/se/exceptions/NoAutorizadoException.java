package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoAutorizadoException extends CoreBaseException
{
    public NoAutorizadoException()
    {
        super("No tens permis per accedir");
    }

    public NoAutorizadoException(String message)
    {
        super(message);
    }
}