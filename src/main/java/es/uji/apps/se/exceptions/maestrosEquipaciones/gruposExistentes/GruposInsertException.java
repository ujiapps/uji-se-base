package es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class GruposInsertException extends CoreBaseException {

    public GruposInsertException() { super("Error al inserir un nou grup d'equipació"); }

    public GruposInsertException(String mensaje) { super(mensaje); }
}
