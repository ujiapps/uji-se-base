package es.uji.apps.se.exceptions.jaulasPermisos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class JaulasPermisosDeleteException extends CoreBaseException {

    public JaulasPermisosDeleteException() { super("Error al esborrar el vincle seleccionat"); }

    public JaulasPermisosDeleteException(String mensaje) { super(mensaje); }
}