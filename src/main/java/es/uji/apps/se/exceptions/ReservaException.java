package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ReservaException extends CoreBaseException
{
    public ReservaException()
    {
        super("No es pot trobar la reserva");
    }

    public ReservaException(String message)
    {
        super(message);
    }
}