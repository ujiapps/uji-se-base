package es.uji.apps.se.exceptions.maestrosEquipaciones.modelos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ModelosUpdateException extends CoreBaseException {

    public ModelosUpdateException() { super("Error al actualitzar un model d'equipació"); }

    public ModelosUpdateException(String mensaje) { super(mensaje); }
}
