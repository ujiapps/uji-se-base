package es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionInsertException extends CoreBaseException {

    public EquipacionInsertException() { super("Error al inserir una nova equipació"); }

    public EquipacionInsertException(String mensaje) { super(mensaje); }
}