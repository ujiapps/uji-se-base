package es.uji.apps.se.exceptions.maestrosEquipaciones.tipoEquipacion;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoEquipacionDeleteException extends CoreBaseException {

    public TipoEquipacionDeleteException() { super("Error al esborrar un tipus d'equipació"); }

    public TipoEquipacionDeleteException(String mensaje) { super(mensaje); }
}
