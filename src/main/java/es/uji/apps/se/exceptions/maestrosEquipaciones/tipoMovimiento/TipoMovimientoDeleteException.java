package es.uji.apps.se.exceptions.maestrosEquipaciones.tipoMovimiento;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoMovimientoDeleteException extends CoreBaseException {

    public TipoMovimientoDeleteException() { super("Error al esborrar un tipus de moviment"); }

    public TipoMovimientoDeleteException(String mensaje) { super(mensaje); }
}
