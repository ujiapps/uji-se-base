package es.uji.apps.se.exceptions.maestrosEquipaciones.tipoMovimiento;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoMovimientoInsertException extends CoreBaseException {

    public TipoMovimientoInsertException() { super("Error al inserir un tipus de moviment"); }

    public TipoMovimientoInsertException(String mensaje) { super(mensaje); }
}
