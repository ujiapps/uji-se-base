package es.uji.apps.se.exceptions.jaulasPermisos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class JaulasPermisosInsertException extends CoreBaseException {

    public JaulasPermisosInsertException() { super("Error al inserir un nou vincle"); }

    public JaulasPermisosInsertException(String mensaje) { super(mensaje); }
}