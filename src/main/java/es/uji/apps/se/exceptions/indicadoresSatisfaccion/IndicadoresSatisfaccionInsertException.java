package es.uji.apps.se.exceptions.indicadoresSatisfaccion;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class IndicadoresSatisfaccionInsertException extends CoreBaseException {

    public IndicadoresSatisfaccionInsertException() { super("Error al inserir un nou indicador de satisfacció"); }

    public IndicadoresSatisfaccionInsertException(String mensaje) { super(mensaje); }
}