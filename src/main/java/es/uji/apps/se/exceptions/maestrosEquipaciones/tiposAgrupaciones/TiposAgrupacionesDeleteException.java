package es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TiposAgrupacionesDeleteException extends CoreBaseException {

    public TiposAgrupacionesDeleteException() { super("Error al esborrar el tipus d'agrupació d'equipació seleccionat"); }

    public TiposAgrupacionesDeleteException(String mensaje) { super(mensaje); }
}