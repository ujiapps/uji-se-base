package es.uji.apps.se.exceptions.maestrosEquipaciones.tipoEquipacion;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoEquipacionUpdateException extends CoreBaseException {

    public TipoEquipacionUpdateException() { super("Error al actualitzar un tipus d'equipació"); }

    public TipoEquipacionUpdateException(String mensaje) { super(mensaje); }
}
