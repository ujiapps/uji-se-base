package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class BusquedaException extends CoreBaseException
{
    public BusquedaException()
    {
        super("No s'ha pogut fer la cerca");
    }

    public BusquedaException(String message)
    {
        super(message);
    }
}