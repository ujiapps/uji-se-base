package es.uji.apps.se.exceptions.indicadoresSatisfaccion;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class IndicadoresSatisfaccionUpdateException extends CoreBaseException {

    public IndicadoresSatisfaccionUpdateException() { super("Error actualitzant aquest indicador de satisfacció"); }

    public IndicadoresSatisfaccionUpdateException(String mensaje) { super(mensaje); }
}