package es.uji.apps.se.exceptions.indicadoresSatisfaccion;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class IndicadoresSatisfaccionDeleteException extends CoreBaseException {

    public IndicadoresSatisfaccionDeleteException() { super("Error al eliminar el indicador de satisfacció seleccionat"); }

    public IndicadoresSatisfaccionDeleteException(String mensaje) { super(mensaje); }
}