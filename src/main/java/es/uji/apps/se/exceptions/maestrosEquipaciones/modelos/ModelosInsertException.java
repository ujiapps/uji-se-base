package es.uji.apps.se.exceptions.maestrosEquipaciones.modelos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ModelosInsertException extends CoreBaseException {

    public ModelosInsertException() { super("Error al inserir un nou model d'equipació"); }

    public ModelosInsertException(String mensaje) { super(mensaje); }
}
