package es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionDeleteException extends CoreBaseException {

    public EquipacionDeleteException() { super("Error al esborrar la equipació seleccionada"); }

    public EquipacionDeleteException(String mensaje) { super(mensaje); }
}