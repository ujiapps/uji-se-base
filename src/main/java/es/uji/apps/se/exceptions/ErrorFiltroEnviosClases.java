package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class ErrorFiltroEnviosClases extends CoreDataBaseException {
    public ErrorFiltroEnviosClases(String message) {
        super(message);
    }

    public ErrorFiltroEnviosClases(String message, Throwable e) {
        super(message, e);
    }
}
