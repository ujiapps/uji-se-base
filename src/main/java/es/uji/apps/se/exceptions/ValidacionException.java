package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ValidacionException extends CoreBaseException
{
    public ValidacionException()
    {
        super("S'ha produït un error de validació");
    }

    public ValidacionException(String message)
    {
        super(message);
    }
}