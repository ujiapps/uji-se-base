package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class CommonException extends CoreBaseException {
    public CommonException(String message) {
        super(message);
    }
}
