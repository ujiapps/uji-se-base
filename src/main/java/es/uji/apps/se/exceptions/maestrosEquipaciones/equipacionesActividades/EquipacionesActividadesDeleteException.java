package es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionesActividadesDeleteException extends CoreBaseException {

    public EquipacionesActividadesDeleteException() { super("Error al esborrar una nova relació de equipació-activitat"); }

    public EquipacionesActividadesDeleteException(String mensaje) { super(mensaje); }
}

