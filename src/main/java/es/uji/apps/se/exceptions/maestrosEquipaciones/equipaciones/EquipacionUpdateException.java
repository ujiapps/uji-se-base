package es.uji.apps.se.exceptions.maestrosEquipaciones.equipaciones;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionUpdateException extends CoreBaseException {

    public EquipacionUpdateException() { super("Error al actualitzar la equipació seleccionada"); }

    public EquipacionUpdateException(String mensaje) { super(mensaje); }
}