package es.uji.apps.se.exceptions.equipaciones.movimientos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionesMovimientosGetException extends CoreBaseException {

    public EquipacionesMovimientosGetException() { super("Error al obtenir els moviments d'equipacions"); }

    public EquipacionesMovimientosGetException(String mensaje) { super(mensaje); }
}
