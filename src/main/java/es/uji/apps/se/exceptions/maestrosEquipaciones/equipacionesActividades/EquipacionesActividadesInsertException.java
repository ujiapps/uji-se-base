package es.uji.apps.se.exceptions.maestrosEquipaciones.equipacionesActividades;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionesActividadesInsertException extends CoreBaseException {

    public EquipacionesActividadesInsertException() { super("Error al inserir una nova relació de equipació-activitat"); }

    public EquipacionesActividadesInsertException(String mensaje) { super(mensaje); }
}
