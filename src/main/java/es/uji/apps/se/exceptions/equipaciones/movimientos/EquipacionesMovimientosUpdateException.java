package es.uji.apps.se.exceptions.equipaciones.movimientos;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionesMovimientosUpdateException extends CoreBaseException {

    public EquipacionesMovimientosUpdateException() { super("Error al actualitzar el moviment d'equipació seleccionat"); }

    public EquipacionesMovimientosUpdateException(String mensaje) { super(mensaje); }
}