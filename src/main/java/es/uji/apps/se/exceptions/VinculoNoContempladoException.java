package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class VinculoNoContempladoException extends CoreBaseException
{
    public VinculoNoContempladoException()
    {
        super("Vincle no contemplat");
    }

    public VinculoNoContempladoException(String message)
    {
        super(message);
    }
}