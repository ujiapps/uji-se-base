package es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class GruposDeleteException extends CoreBaseException {

    public GruposDeleteException() { super("Error al esborrar el grup d'equipació seleccionat"); }

    public GruposDeleteException(String mensaje) { super(mensaje); }
}
