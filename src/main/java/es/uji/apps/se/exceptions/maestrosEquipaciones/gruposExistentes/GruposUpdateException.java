package es.uji.apps.se.exceptions.maestrosEquipaciones.gruposExistentes;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class GruposUpdateException extends CoreBaseException {

    public GruposUpdateException() { super("Error al actualitzar el grup d'equipació seleccionat"); }

    public GruposUpdateException(String mensaje) { super(mensaje); }
}
