package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ParametroException extends CoreBaseException
{
    public ParametroException()
    {
        super("Paràmetres incorrectes");
    }

    public ParametroException(String message)
    {
        super(message);
    }
}