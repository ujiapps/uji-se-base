package es.uji.apps.se.exceptions.maestrosEquipaciones.tiposAgrupaciones;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TiposAgrupacionesUpdateException extends CoreBaseException {

    public TiposAgrupacionesUpdateException() { super("Error al actualitzar el tipus d'agrupació d'equipació seleccionat"); }

    public TiposAgrupacionesUpdateException(String mensaje) { super(mensaje); }
}