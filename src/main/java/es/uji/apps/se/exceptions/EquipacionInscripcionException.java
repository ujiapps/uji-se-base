package es.uji.apps.se.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EquipacionInscripcionException extends CoreBaseException {
    public EquipacionInscripcionException(String mensaje) { super(mensaje); }
}
