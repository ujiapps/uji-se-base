package es.uji.apps.se.ui;

public class IncidenciaEliteUI {

    private Long persona;
    private Long cursoAcademico;
    private Long tipoIncidencia;
    private String motivo;
    private String detalle;
    private String asignatura;
    private String fecha;
    private Long profesor;
    private String fechaCambio;
    private String asignaturasMatricula;
    private String asignaturasAnula;

    public IncidenciaEliteUI() {
    }

    public Long getPersona() {
        return persona;
    }

    public void setPersona(Long persona) {
        this.persona = persona;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public Long getTipoIncidencia() {
        return tipoIncidencia;
    }

    public void setTipoIncidencia(Long tipoIncidencia) {
        this.tipoIncidencia = tipoIncidencia;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Long getProfesor() {
        return profesor;
    }

    public void setProfesor(Long profesor) {
        this.profesor = profesor;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public String getAsignaturasMatricula() {
        return asignaturasMatricula;
    }

    public void setAsignaturasMatricula(String asignaturasMatricula) {
        this.asignaturasMatricula = asignaturasMatricula;
    }

    public String getAsignaturasAnula() {
        return asignaturasAnula;
    }

    public void setAsignaturasAnula(String asignaturasAnula) {
        this.asignaturasAnula = asignaturasAnula;
    }
}
