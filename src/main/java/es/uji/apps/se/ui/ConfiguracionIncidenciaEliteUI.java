package es.uji.apps.se.ui;

public class ConfiguracionIncidenciaEliteUI {

    private String correoAdministracionIncidenciasElite;

    public ConfiguracionIncidenciaEliteUI() {
    }

    public String getCorreoAdministracionIncidenciasElite() {
       return this.correoAdministracionIncidenciasElite;
    }

    public void setCorreoAdministracionIncidenciasElite(String correoAdministracionIncidenciasElite) {
        this.correoAdministracionIncidenciasElite = correoAdministracionIncidenciasElite;
    }
}
