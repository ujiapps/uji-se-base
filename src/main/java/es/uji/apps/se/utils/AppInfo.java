package es.uji.apps.se.utils;

import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.ItemMenu;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class AppInfo {
    private static String urlBase;
    private static String host;

    @Value("${uji.webapp.urlBase}")
    public void setUrlBase(String urlBase) {
        this.urlBase = urlBase;
    }

    @Value("${uji.es.host}")
    public void setHost(String host) {
        this.host = host;
    }

    public static String getHost() {
        return host;
    }

    public static Template buildPagina(String plantilla, String idioma) throws ParseException {


        Template template = new HTMLTemplate(plantilla, new Locale(idioma), "se");
        Pagina pagina = new Pagina(urlBase, urlBase, idioma, "SE");
        pagina.setTitulo("SE");
        ItemMenu itemincidencia = new ItemMenu("Creació de incidencies", urlBase + "incidenciaeliteusuario");
        ItemMenu itemlistado = new ItemMenu("Consulta les teues incidencies", urlBase + "incidenciaeliteusuario/listado");
        GrupoMenu grupoMenu = new GrupoMenu("Incidencias");
        grupoMenu.addItem(itemincidencia);
        grupoMenu.addItem(itemlistado);
        Menu menu = new Menu();
        menu.addGrupo(grupoMenu);
        pagina.setMenu(menu);


        template.put("urlBase", urlBase + "incidenciaeliteusuario");
        template.put("host", host);
        template.put("server", host);
        template.put("page_title", "SE");
        template.put("idioma", idioma);
        template.put("pagina", pagina);
        template.put("footer", "se/footer");

        return template;
    }

    public static Template buildPaginaAplicacionClases(String plantilla, String idioma) throws ParseException {


        Template template = new HTMLTemplate(plantilla, new Locale(idioma), "se");
        Pagina pagina = new Pagina(urlBase, urlBase, idioma, "SE");
        pagina.setTitulo("SE");

        template.put("urlBase", urlBase + "app");
        template.put("host", host);
        template.put("server", host);
        template.put("page_title", "SE");
        template.put("idioma", idioma);
        template.put("pagina", pagina);

        return template;
    }

    public static Template buildPaginaActa(String plantilla, String idioma) throws ParseException {

        Template template = new HTMLTemplate(plantilla, new Locale(idioma), "se");
        Pagina pagina = new Pagina(urlBase, urlBase, idioma, "SE");
        pagina.setTitulo("Acta");
        Menu menu = new Menu();
        pagina.setMenu(menu);


        template.put("urlBase", urlBase);
        template.put("host", host);
        template.put("page_title", "SE");
        template.put("idioma", idioma);
        template.put("pagina", pagina);

        return template;
    }


    public static Template buildPaginaSinMenuLateral(String plantilla, String idioma) throws ParseException {

        Template template = new HTMLTemplate(plantilla, new Locale(idioma), "se");
        Pagina pagina = new Pagina(urlBase, urlBase, idioma, "SE");
        pagina.setTitulo("SE");
        Menu menu = new Menu();
        pagina.setMenu(menu);


        template.put("urlBase", urlBase);
        template.put("host", host);
        template.put("page_title", "SE");
        template.put("idioma", idioma);
        template.put("pagina", pagina);
        template.put("footer", "se/footer");

        return template;
    }
}