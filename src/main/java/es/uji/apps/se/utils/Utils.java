package es.uji.apps.se.utils;

import java.math.BigDecimal;

public class Utils {
    public static Long convierteBigDecimalEnLong(BigDecimal param) {
        return param == null ? null : param.longValue();
    }
}
