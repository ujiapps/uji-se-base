package es.uji.apps.se.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "SE_CALENDARIO")
public class CalendarioDTO implements Serializable
{
    public static final String FESTIVO = "F";
    public static final String LECTIVO = "L";
    public static final String EXAMEN  = "E";
    public static final String S  = "S";

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DIA")
    private Date dia;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "HORA_INICIO")
    private String horaInicio;

    @Column(name = "HORA_FIN")
    private String horaFin;

    @OneToMany(mappedBy = "calendarioDTO")
    private Set<CalendarioOfertaDTO> calendariosOferta;

    @OneToMany(mappedBy = "calendarioDTO")
    private Set<CalendarioClaseDTO> calendariosClase;

    public CalendarioDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getDia()
    {
        return dia;
    }

    public void setDia(Date dia)
    {
        this.dia = dia;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(String horaFin)
    {
        this.horaFin = horaFin;
    }

    public Set<CalendarioOfertaDTO> getCalendariosOferta()
    {
        return calendariosOferta;
    }

    public void setCalendariosOferta(Set<CalendarioOfertaDTO> calendariosOferta)
    {
        this.calendariosOferta = calendariosOferta;
    }

    public Set<CalendarioClaseDTO> getCalendariosClase()
    {
        return calendariosClase;
    }

    public void setCalendariosClase(Set<CalendarioClaseDTO> calendariosClase)
    {
        this.calendariosClase = calendariosClase;
    }

}