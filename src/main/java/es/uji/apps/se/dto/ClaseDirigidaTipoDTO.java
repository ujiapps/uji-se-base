package es.uji.apps.se.dto;

import javax.persistence.*;

@Entity
@Table(name = "SE_CLASES_DIRIGIDAS_TIPOS")
public class ClaseDirigidaTipoDTO {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name="CLASE_DIRIGIDA_ID")
    private ClaseDTO clase;

    @ManyToOne
    @JoinColumn(name="TIPO_ID")
    private TipoDTO tipo;

    public ClaseDirigidaTipoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClaseDTO getClase() {
        return clase;
    }

    public void setClase(ClaseDTO clase) {
        this.clase = clase;
    }

    public TipoDTO getTipo() {
        return tipo;
    }

    public void setTipo(TipoDTO tipo) {
        this.tipo = tipo;
    }
}
