package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_COMPETICIONES_GRUPOS")
public class CompeticionGrupoDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "FASE_ID")
    private Long faseId;

    @Column(name = "PLAYOFF")
    private Long playoff;

    @Column(name = "SEXO")
    private String sexo;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "NUM_EQUIPOS_PASAN")
    private Long numEquiposPasan;

    public CompeticionGrupoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getFaseId() {
        return faseId;
    }

    public void setFaseId(Long faseId) {
        this.faseId = faseId;
    }

    public Long getPlayoff() {
        return playoff;
    }

    public void setPlayoff(Long playoff) {
        this.playoff = playoff;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getNumEquiposPasan() {
        return numEquiposPasan;
    }

    public void setNumEquiposPasan(Long numEquiposPasan) {
        this.numEquiposPasan = numEquiposPasan;
    }
}
