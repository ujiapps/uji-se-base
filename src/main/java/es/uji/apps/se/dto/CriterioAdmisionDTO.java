package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_EXT_CRIT_ADMISION_ELITE")
public class CriterioAdmisionDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    @OneToMany(mappedBy = "criterioAdmisionDTO")
    private Set<EliteDTO> criteriosAdmision;

    public CriterioAdmisionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<EliteDTO> getCriteriosAdmision() {
        return criteriosAdmision;
    }

    public void setCriteriosAdmision(Set<EliteDTO> criteriosAdmision) {
        this.criteriosAdmision = criteriosAdmision;
    }
}