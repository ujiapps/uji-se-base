package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_PERIODOS_DESCUENTOS")
public class PeriodoDescuentoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String tipo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA")
    private Date fecha;

    @Column(name="DESCUENTO")
    private Long descuento;

    @ManyToOne
    @JoinColumn(name = "PERIODO_ID")
    private PeriodoDTO periodoDTO;

    public PeriodoDescuentoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getDescuento() {
        return descuento;
    }

    public void setDescuento(Long descuento) {
        this.descuento = descuento;
    }

    public PeriodoDTO getPeriodo() {
        return periodoDTO;
    }

    public void setPeriodo(PeriodoDTO periodoDTO) {
        this.periodoDTO = periodoDTO;
    }
}