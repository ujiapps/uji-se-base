package es.uji.apps.se.dto.views;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VMC_TARJETAS_DEPORTIVAS_CSV")
public class TarjetaDeportivaCSV implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "MOVIL")
    private String movil;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "APELLIDOS_NOMBRE")
    private String apellidosNombre;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    @Column(name = "SEXO")
    private String sexo;

    @Column(name = "BUSQUEDA")
    private String busqueda;

    @Column(name = "VINCULO_ACTUAL")
    private String vinculoActual;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name="TIPO_TARJETA")
    private String tipoTarjeta;

    @Column(name="TIPO_TARJETA_ID")
    private Long tipoTarjetaId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_PAGO")
    private Date fechaPago;

    @Column(name="IMPORTE")
    private Float importe;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_ALTA")
    private Date fechaAlta;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @Column(name="TIPO_SANCION")
    private String tipoSancion;

    private String sancionado;

    public TarjetaDeportivaCSV() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public Long getSexoLong(){
        return (sexo.equalsIgnoreCase("dona"))?2L:1L;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public String getVinculoActual() {
        return vinculoActual;
    }

    public void setVinculoActual(String vinculoActual) {
        this.vinculoActual = vinculoActual;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getSancionado() {
        return sancionado;
    }

    public void setSancionado(String sancionado) {
        this.sancionado = sancionado;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getTipoSancion() {
        return tipoSancion;
    }
}