package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SE_EXT_PERSONAS_TITULACION")
public class PersonaTitulacionDTO implements Serializable {

    @Id
    @Column(name="PERSONA_ID")
    private Long persona;

    @Id
    @Column(name="CURSO_ACA")
    private Long cursoAcademico;

    @Id
    @Column(name="TITULACION_ID")
    private Long titulacion;

    @Column(name="TITULACION_NOMBRE")
    private String titulacionNombre;

    private String matricula;
    private String expediente;
    private Long oficial;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    public PersonaTitulacionDTO() {
    }

    public Long getPersona() {
        return persona;
    }

    public void setPersona(Long persona) {
        this.persona = persona;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public String getTitulacionNombre() {
        return titulacionNombre;
    }

    public void setTitulacionNombre(String titulacionNombre) {
        this.titulacionNombre = titulacionNombre;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public Long getTitulacion() {
        return titulacion;
    }

    public void setTitulacion(Long titulacion) {
        this.titulacion = titulacion;
    }

    public Long getOficial() {return oficial;}
    public void setOficial(Long oficial) {this.oficial = oficial;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonaTitulacionDTO that = (PersonaTitulacionDTO) o;
        return Objects.equals(persona, that.persona) && Objects.equals(cursoAcademico, that.cursoAcademico) && Objects.equals(titulacion, that.titulacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(persona, cursoAcademico, titulacion);
    }
}