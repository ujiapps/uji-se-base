package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PER_PERSONAS")
public class PerPersonasDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "APELLIDO1")
    private String apellido1;

    @Column(name = "APELLIDO2")
    private String apellido2;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "TIP_ID")
    private Long tipoId;

    @Column(name = "TIPO_PERSONA")
    private String tipoPersona;

    @Column(name = "PAI_ID")
    private String paiId;

    @Column(name = "LOR_ID")
    private Long lorId;

    public PerPersonasDTO() {
    }

    public PerPersonasDTO(String nombre, String apellido1, String apellido2, String identificacion, Long tipoId, String tipoPersona, String paiId, Long lorId) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.identificacion = identificacion;
        this.tipoId = tipoId;
        this.tipoPersona = tipoPersona;
        this.paiId = paiId;
        this.lorId = lorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Long getTipoId() {
        return tipoId;
    }

    public void setTipoId(Long tipoId) {
        this.tipoId = tipoId;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getPaiId() {
        return paiId;
    }

    public void setPaiId(String paiId) {
        this.paiId = paiId;
    }

    public Long getLorId() {
        return lorId;
    }

    public void setLorId(Long lorId) {
        this.lorId = lorId;
    }
}
