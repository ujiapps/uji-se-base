package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_OFERTAS_TARIFAS_DTO")
public class OfertaTarifaDescuentoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private Float importe;

    @ManyToOne
    @JoinColumn(name = "OFERTA_TARIFA_ID")
    private OfertaTarifaDTO ofertaTarifaDTO;

    @ManyToOne
    @JoinColumn(name = "TARJETA_DEPORTIVA_ID")
    private TarjetaDeportivaTipoDTO tarjetaDeportiva;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    public OfertaTarifaDescuentoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }


    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public OfertaTarifaDTO getOfertaTarifa() {
        return ofertaTarifaDTO;
    }

    public void setOfertaTarifa(OfertaTarifaDTO ofertaTarifaDTO) {
        this.ofertaTarifaDTO = ofertaTarifaDTO;
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportiva() {
        return tarjetaDeportiva;
    }

    public void setTarjetaDeportiva(TarjetaDeportivaTipoDTO tarjetaDeportiva) {
        this.tarjetaDeportiva = tarjetaDeportiva;
    }
}
