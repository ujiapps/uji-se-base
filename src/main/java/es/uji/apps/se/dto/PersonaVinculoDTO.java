package es.uji.apps.se.dto;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "SE_VW_PERSONAS_VINCULO_ACTUAL")
//@Table(name = "SE_VMC_PERSONAS_VINCULOS")

public class PersonaVinculoDTO implements Serializable {

    public static final Long SIN_VINCULACION_UJI = 292629L;

    @Id
    @Column(name = "id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "id")
    private PersonaUjiDTO personaUjiDTO;

    @ManyToOne
    @JoinColumn(name = "VINCULO_ACTUAL")
    private TipoDTO vinculo;

    public PersonaVinculoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public TipoDTO getVinculo()
    {
        return vinculo;
    }

    public void setVinculo(TipoDTO vinculo)
    {
        this.vinculo = vinculo;
    }
}