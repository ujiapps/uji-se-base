package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_VW_AUTORIZADOS_APP")
public class AutorizadoAppDTO implements Serializable {

    @Id
    @Column(name="PERSONA_ID")
    private Long personaId;

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }
}