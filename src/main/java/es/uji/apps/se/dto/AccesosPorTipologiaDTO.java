package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "se_vw_ind_accesos_tipologia")
public class AccesosPorTipologiaDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dia;

    @Column(name="HORA_ENTRADA")
    private String horaEntrada;

    @Column(name="HORA_SALIDA")
    private String horaSalida;

    @Column (name="ZONA_ID")
    private Long zonaId;

    private String zona;

    @Column(name="TIPOLOGIA_ID")
    private Long tipologiaId;

    public AccesosPorTipologiaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Long getZonaId() {
        return zonaId;
    }

    public void setZonaId(Long zonaId) {
        this.zonaId = zonaId;
    }

    public Long getTipologiaId() {
        return tipologiaId;
    }

    public void setTipologiaId(Long tipologiaId) {
        this.tipologiaId = tipologiaId;
    }
}
