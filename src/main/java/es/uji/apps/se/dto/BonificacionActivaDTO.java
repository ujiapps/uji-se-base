package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_BONIFICACIONES_ACTIVAS")
public class BonificacionActivaDTO implements Serializable {
    @Id
    @Column(name = "ID")
//    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "REFERENCIA_ID")
    private Long referencia;

    @Column(name = "ORIGEN")
    private String origen;

    @Column(name = "USOS")
    private Long usos;

    @Column(name = "IMPORTE")
    private Float importeFloat;

    @Column(name = "PORCENTAJE")
    private Long porcentaje;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INI")
    private Date fechaIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Column(name = "MOSTRAR")
    private Boolean mostrar;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    private String importe;

    public BonificacionActivaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getUsos() {
        return usos;
    }

    public void setUsos(Long usos) {
        this.usos = usos;
    }

    public Long getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Long porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Boolean getMostrar() {
        return mostrar;
    }

    public void setMostrar(Boolean mostrar) {
        this.mostrar = mostrar;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public Float getImporteFloat() {
        return importeFloat;
    }

    public void setImporteFloat(Float importeFloat) {
        this.importeFloat = importeFloat;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }
}