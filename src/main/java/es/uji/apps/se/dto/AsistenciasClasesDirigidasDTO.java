package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_IND_ASISTENCIAS_CD")
public class AsistenciasClasesDirigidasDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    private String horario;
    private String nombre;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dia;

    @Column (name="ACTIVIDAD_ID")
    private Long actividadId;

    private Integer plazas;

    @Column(name="TARJETA_DEPORTIVA_ID")
    private Long tarjetaDeportiva;

    private Long asiste;

    @Column(name="HORA_INICIO")
    private String horaInicio;

    @Column(name="HORA_FIN")
    private String horaFin;

    public AsistenciasClasesDirigidasDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public Integer getPlazas() {
        return plazas;
    }

    public void setPlazas(Integer plazas) {
        this.plazas = plazas;
    }

    public Long getTarjetaDeportiva() {
        return tarjetaDeportiva;
    }

    public void setTarjetaDeportiva(Long tarjetaDeportiva) {
        this.tarjetaDeportiva = tarjetaDeportiva;
    }

    public Long getAsiste() {
        return asiste;
    }

    public void setAsiste(Long asiste) {
        this.asiste = asiste;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }
}
