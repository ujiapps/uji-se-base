package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity @Table(name = "SE_VW_TARJETAS_DEPORTIVAS")
public class TarjetaDeportivaViewDTO implements Serializable
{
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaUji;

    private String identificacion;

    private String nombre;

    private String movil;

    private String mail;

    @Column(name = "TARJETA_DEPORTIVA_TIPO_ID")
    private Long tarjetaDeportivaTipoId;

    @Column(name = "FECHA_ALTA")
    private Date fechaAlta;

    @Column(name = "FECHA_BAJA")
    private Date fechaBaja;

    @Column(name="vinculo")
    private String vinculoNombre;

    @Column(name="sancionado")
    private Boolean sancionado;

//    @Column(name="motivo")
//    private String sancion;

    public TarjetaDeportivaViewDTO()
    {
    }

    public TarjetaDeportivaViewDTO(Long tarjetaDeportivaId)
    {
        this.id = tarjetaDeportivaId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getPersonaUji() {
        return personaUji;
    }

    public void setPersonaUji(Long personaUji) {
        this.personaUji = personaUji;
    }

    public Long getTarjetaDeportivaTipoId() {
        return tarjetaDeportivaTipoId;
    }

    public void setTarjetaDeportivaTipoId(Long tarjetaDeportivaTipoId) {
        this.tarjetaDeportivaTipoId = tarjetaDeportivaTipoId;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getVinculoNombre() {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre) {
        this.vinculoNombre = vinculoNombre;
    }

//    public String getSancion() {
//        return sancion;
//    }
//
//    public void setSancion(String sancion) {
//        this.sancion = sancion;
//    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Boolean getSancionado() {
        return sancionado;
    }

    public Boolean isSancionado() {
        return sancionado;
    }

    public void setSancionado(Boolean sancionado) {
        this.sancionado = sancionado;
    }
}