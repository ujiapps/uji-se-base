package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_ELITE_INCIDENCIAS_MAIL")
public class MailIncidenciaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name= "CORREO")
    private String correo;

    private String responder;
    private String desde;
    private String asunto;
    private String cuerpo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_ENVIO")
    private Date fechaEnvio;

    @ManyToOne
    @JoinColumn(name = "INCIDENCIA_ELITE_ID")
    private IncidenciaEliteDTO incidenciaEliteDTO;

    public MailIncidenciaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public IncidenciaEliteDTO getIncidenciaElite() {
        return incidenciaEliteDTO;
    }

    public void setIncidenciaElite(IncidenciaEliteDTO incidenciaEliteDTO) {
        this.incidenciaEliteDTO = incidenciaEliteDTO;
    }
}
