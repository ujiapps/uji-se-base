package es.uji.apps.se.dto;

import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.annotations.DataTag;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_TARJETAS_DEPORTIVAS_TIPOS")
public class TarjetaDeportivaTipoDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @DataTag
    @Column(name = "NOMBRE")
    private String nombre;

    @DataTag
    @Column(name = "PERMITE_INSTALACIONES")
    private Boolean permiteInstalaciones;

    @DataTag
    @Column(name = "PERMITE_MUSCULACION")
    private Boolean permiteMusculacion;

    @DataTag
    @Column(name = "PERMITE_CLASES_DIRIGIDAS")
    private Boolean permiteClasesDirigidas;

    @DataTag
    @Column(name = "HORA_INI_PERMITE_RESERVA")
    private String horaInicioPermiteReserva;

    @DataTag
    @Column(name = "HORA_FIN_PERMITE_RESERVA")
    private String horaFinPermiteReserva;

    @DataTag
    @Column(name = "TIPO_PLANTILLA")
    private String tipoPlantilla;

    @DataTag
    @Column(name = "IMAGEN_TARJETA")
    private String imagenTarjeta;

    @DataTag
    @Column(name = "ACTIVA")
    private Boolean activa;

    @Column(name = "ORDEN_VISUALIZACION")
    private Long ordenVisualizacion;

    @DataTag
    @Column(name = "MOSTRAR_USUARIO")
    private Boolean mostrarUsuario;

    @Column(name = "DIAS")
    private Long dias;

    @Column(name = "MESES")
    private Long meses;

    @Column(name = "ANYOS")
    private Long anyos;

    @Column(name = "FECHA_CADUCIDAD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCaducidad;

    @ManyToOne
    @JoinColumn(name = "TAXONOMIA_ID")
    private TipoDTO taxonomia;

    @OneToMany(mappedBy = "tarjetaDeportivaTipoDTO")
    private Set<TarjetaDeportivaTarifaDTO> tarjetaDeportivaTarifasDTO;

    @OneToMany(mappedBy = "tarjetaDeportivaTipo")
    private Set<TarjetaDeportivaDTO> tarjetaDeportivasDTO;

    @OneToMany(mappedBy = "tarjetaDeportivaTipoDTO")
    private Set<TarjetaDeportivaSuperiorDTO> tarjetaDeportivaSuperioresDTO;

    @OneToMany(mappedBy = "tarjetaDeportiva")
    private Set<ActividadTarifaDescuentoDTO> actividadTarifaDescuentosDTO;

    public TarjetaDeportivaTipoDTO() {
    }

    public TarjetaDeportivaTipoDTO(Long id) {
        this.id = id;
    }

    public TarjetaDeportivaTipoDTO(String id,
                                   String nombre,
                                   String permiteInstalaciones,
                                   String permiteMusculacion,
                                   String permiteClasesDirigidas,
                                   String horaInicioPermiteReserva,
                                   String horaFinPermiteReserva,
                                   String tipoPlantilla,
                                   String imagenTarjeta,
                                   String activa,
                                   String mostrarUsuario,
                                   String dias,
                                   String meses,
                                   String anyos,
                                   String fechaCaducidad,
                                   String taxonomia) throws ParseException {
        this.id = ParamUtils.isNotNull(id) ? ParamUtils.parseLong(id) : null;
        this.nombre = nombre;
        this.permiteInstalaciones = Boolean.parseBoolean(permiteInstalaciones);
        this.permiteMusculacion = Boolean.parseBoolean(permiteMusculacion);
        this.permiteClasesDirigidas = Boolean.parseBoolean(permiteClasesDirigidas);
        this.horaInicioPermiteReserva = horaInicioPermiteReserva;
        this.horaFinPermiteReserva = horaFinPermiteReserva;
        this.tipoPlantilla = tipoPlantilla;
        this.imagenTarjeta = imagenTarjeta;
        this.activa = Boolean.parseBoolean(activa);
        this.mostrarUsuario = Boolean.parseBoolean(mostrarUsuario);
        this.dias = ParamUtils.isNotNull(dias)?ParamUtils.parseLong(dias):null;
        this.meses = ParamUtils.isNotNull(meses)?ParamUtils.parseLong(meses):null;
        this.anyos = ParamUtils.isNotNull(anyos)?ParamUtils.parseLong(anyos):null;
        if (ParamUtils.isNotNull(fechaCaducidad)) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            this.fechaCaducidad = sdf.parse(fechaCaducidad);
        }
        if(ParamUtils.isNotNull(taxonomia)) {
            this.taxonomia = new TipoDTO(ParamUtils.parseLong(taxonomia));
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean isPermiteInstalaciones() {
        return permiteInstalaciones;
    }

    public void setPermiteInstalaciones(Boolean permiteInstalaciones) {
        this.permiteInstalaciones = permiteInstalaciones;
    }

    public Boolean isPermiteMusculacion() {
        return permiteMusculacion;
    }

    public void setPermiteMusculacion(Boolean permiteMusculacion) {
        this.permiteMusculacion = permiteMusculacion;
    }

    public Boolean isPermiteClasesDirigidas() {
        return permiteClasesDirigidas;
    }

    public void setPermiteClasesDirigidas(Boolean permiteClasesDirigidas) {
        this.permiteClasesDirigidas = permiteClasesDirigidas;
    }

    public TarjetaDeportivaTarifaDTO getTarifaByVinculo(Long vinculoId) {
        return tarjetaDeportivaTarifasDTO.stream().filter(t -> t.getVinculo().getId().equals(vinculoId)).findFirst().orElse(null);
    }

    public TipoDTO getTaxonomia() {
        return taxonomia;
    }

    public void setTaxonomia(TipoDTO taxonomia) {
        this.taxonomia = taxonomia;
    }

    public Set<TarjetaDeportivaTarifaDTO> getTarjetaDeportivaTarifas() {
        return tarjetaDeportivaTarifasDTO;
    }

    public void setTarjetaDeportivaTarifas(Set<TarjetaDeportivaTarifaDTO> tarjetaDeportivaTarifasDTO) {
        this.tarjetaDeportivaTarifasDTO = tarjetaDeportivaTarifasDTO;
    }

    public Set<TarjetaDeportivaDTO> getTarjetaDeportivas() {
        return tarjetaDeportivasDTO;
    }

    public void setTarjetaDeportivas(Set<TarjetaDeportivaDTO> tarjetaDeportivasDTO) {
        this.tarjetaDeportivasDTO = tarjetaDeportivasDTO;
    }

    public String getHoraInicioPermiteReserva() {
        return horaInicioPermiteReserva;
    }

    public void setHoraInicioPermiteReserva(String horaInicioPermiteReserva) {
        this.horaInicioPermiteReserva = horaInicioPermiteReserva;
    }

    public String getHoraFinPermiteReserva() {
        return horaFinPermiteReserva;
    }

    public void setHoraFinPermiteReserva(String horaFinPermiteReserva) {
        this.horaFinPermiteReserva = horaFinPermiteReserva;
    }

    public String getTipoPlantilla() {
        return tipoPlantilla;
    }

    public void setTipoPlantilla(String tipoPlantilla) {
        this.tipoPlantilla = tipoPlantilla;
    }

    public Set<TarjetaDeportivaSuperiorDTO> getTarjetaDeportivaSuperiores() {
        return tarjetaDeportivaSuperioresDTO;
    }

    public void setTarjetaDeportivaSuperiores(Set<TarjetaDeportivaSuperiorDTO> tarjetaDeportivaSuperioresDTO) {
        this.tarjetaDeportivaSuperioresDTO = tarjetaDeportivaSuperioresDTO;
    }

    public Boolean isActiva() {
        return activa;
    }

    public void setActiva(Boolean activa) {
        this.activa = activa;
    }

    public Boolean isMostrarUsuario() {
        return mostrarUsuario;
    }

    public void setMostrarUsuario(Boolean mostrarUsuario) {
        this.mostrarUsuario = mostrarUsuario;
    }

    public String getImagenTarjeta() {
        return imagenTarjeta;
    }

    public void setImagenTarjeta(String imagenTarjeta) {
        this.imagenTarjeta = imagenTarjeta;
    }

    public Long getDias() {
        return dias;
    }

    public void setDias(Long dias) {
        this.dias = dias;
    }

    public Long getMeses() {
        return meses;
    }

    public void setMeses(Long meses) {
        this.meses = meses;
    }

    public Long getAnyos() {
        return anyos;
    }

    public void setAnyos(Long anyos) {
        this.anyos = anyos;
    }

    public Date getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(Date fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public Long getOrdenVisualizacion() {
        return ordenVisualizacion;
    }

    public void setOrdenVisualizacion(Long ordenVisualizacion) {
        this.ordenVisualizacion = ordenVisualizacion;
    }

    public Set<ActividadTarifaDescuentoDTO> getActividadTarifaDescuentos() {
        return actividadTarifaDescuentosDTO;
    }

    public void setActividadTarifaDescuentos(Set<ActividadTarifaDescuentoDTO> actividadTarifaDescuentosDTO) {
        this.actividadTarifaDescuentosDTO = actividadTarifaDescuentosDTO;
    }
}