package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_PREINSCRIPCIONES")
public class PreinscripcionDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "ESTADO_ID")
    private Long estadoId;

    @Column(name = "OFERTA_ID")
    private Long ofertaId;

    @Column(name = "IMPORTE")
    private Float importe;

    @Column(name = "ORDEN_INSCRIPCION")
    private Long ordenInscripcion;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "ORIGEN")
    private String origen;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INI_VALIDA")
    private Date fechaInicioValida;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_VALIDA")
    private Date fechaFinValida;

    public PreinscripcionDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getEstadoId() {return estadoId;}
    public void setEstadoId(Long estadoId) {this.estadoId = estadoId;}

    public Long getOfertaId() {return ofertaId;}
    public void setOfertaId(Long ofertaId) {this.ofertaId = ofertaId;}

    public Float getImporte() {return importe;}
    public void setImporte(Float importe) {this.importe = importe;}

    public Long getOrdenInscripcion() {return ordenInscripcion;}
    public void setOrdenInscripcion(Long ordenInscripcion) {this.ordenInscripcion = ordenInscripcion;}

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public Date getFechaInicioValida() {return fechaInicioValida;}
    public void setFechaInicioValida(Date fechaInicioValida) {this.fechaInicioValida = fechaInicioValida;}

    public Date getFechaFinValida() {return fechaFinValida;}
    public void setFechaFinValida(Date fechaFinValida) {this.fechaFinValida = fechaFinValida;}

    /*
    @OneToMany(mappedBy = "preinscripcionDTO")
    private Set<InscripcionDTO> inscripciones;
     */
}
