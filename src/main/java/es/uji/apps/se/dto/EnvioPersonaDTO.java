package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "SE_PERSONAS_NOTIFICACIONES")
public class EnvioPersonaDTO implements Serializable {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String asunto;
    private String cuerpo;
    private String para;

    @Column(name="recibir_correo")
    private Boolean recibirCorreo;

    @Column(name="RESPONDER_A")
    private String responder;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", insertable = false, updatable = false)
    private PersonaUjiDTO persona;

    @Column(name="PERSONA_ID")
    private Long personaId;

    @ManyToOne
    @JoinColumn(name = "REMESA_ID")
    private EnvioDTO envio;

    public EnvioPersonaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public PersonaUjiDTO getPersona() {
        return persona;
    }

    public void setPersona(PersonaUjiDTO persona) {
        this.persona = persona;
    }

    public EnvioDTO getEnvio() {
        return envio;
    }

    public void setEnvio(EnvioDTO envio) {
        this.envio = envio;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Boolean getRecibirCorreo() {
        return recibirCorreo;
    }

    public Boolean isRecibirCorreo() {
        return recibirCorreo;
    }

    public void setRecibirCorreo(Boolean recibirCorreo) {
        this.recibirCorreo = recibirCorreo;
    }
}
