package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_ACTIVIDADES_ASISTENCIAS")
public class ActividadAsistenciaDTO implements Serializable {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PARTIDO_ID")
    private CompeticionPartidoDTO partido;

    @ManyToOne
    @JoinColumn(name = "INSCRIPCION_ID")
    private InscripcionDTO inscripcion;

    @Column(name="TIPO_ENTRADA")
    private String tipoEntrada;

    @Column(name="CALENDARIO_OFERTA_ID")
    private Long calendarioOfertaId;

    public ActividadAsistenciaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CompeticionPartidoDTO getPartido() {
        return partido;
    }

    public void setPartido(CompeticionPartidoDTO partido) {
        this.partido = partido;
    }

    public InscripcionDTO getInscripcion() {
        return inscripcion;
    }

    public void setInscripcion(InscripcionDTO inscripcion) { this.inscripcion = inscripcion; }

    public String getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public Long getCalendarioOfertaId() {return calendarioOfertaId;}
    public void setCalendarioOfertaId(Long calendarioOfertaId) {this.calendarioOfertaId = calendarioOfertaId;}
}
