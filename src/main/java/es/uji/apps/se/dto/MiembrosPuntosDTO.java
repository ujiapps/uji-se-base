package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_COMP_MIEMBROS_PUNTOS")
public class MiembrosPuntosDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "MIEMBRO_ID")

    private Long miembro;

    @Column(name = "PARTIDO_ID")

    private Long partido;

    @Column(name = "PUNTOS")

    private Long puntos;

    @Column(name = "EQUIPO_ID")

    private Long equipo;

    public MiembrosPuntosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMiembro() {
        return miembro;
    }

    public void setMiembro(Long miembro) {
        this.miembro = miembro;
    }

    public Long getPartido() {
        return partido;
    }

    public void setPartido(Long partido) {
        this.partido = partido;
    }

    public Long getPuntos() {
        return puntos;
    }

    public void setPuntos(Long puntos) {
        this.puntos = puntos;
    }

    public Long getEquipo() {
        return equipo;
    }

    public void setEquipo(Long equipo) {
        this.equipo = equipo;
    }
}
