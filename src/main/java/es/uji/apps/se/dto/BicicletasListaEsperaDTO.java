package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_BIC_LISTA_ESPERA")
public class BicicletasListaEsperaDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long esperaId;
    @Column(name = "FECHA")
    private Date fecha;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "PERSONA_ID")
    private Long perId;
    @Column(name = "TIPO_USO_ID")
    private Long tipoUso;

    public BicicletasListaEsperaDTO() {
    }

    public Long getEsperaId() {
        return esperaId;
    }

    public void setEsperaId(Long esperaId) {
        this.esperaId = esperaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getTipoUso() {
        return tipoUso;
    }

    public void setTipoUso(Long tipoUso) {
        this.tipoUso = tipoUso;
    }
}
