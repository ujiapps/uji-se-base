package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "UJI_ZETADOLAR.Z$_SE_SANCIONES")
public class SancionHistoricoDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Column(name = "MOTIVO")
    private String motivo;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademico;

    @Column(name="XTIPO_DML")
    private String xTipoDml;

    @Column(name="XFECHA")
    private Date xFecha;

    @ManyToOne()
    @JoinColumn(name="XPER_ID")
    private PersonaUjiDTO xPersonaId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "NIVEL_ID")
    private TipoDTO nivel;

    public Long getId()
    {
        return id;
    }


    public Long getPersonaId() {return personaId;}


    public Date getFecha()
    {
        return fecha;
    }


    public Date getFechaFin()
    {
        return fechaFin;
    }


    public String getMotivo()
    {
        return motivo;
    }


    public String getTipo()
    {
        return tipo;
    }


    public Long getCursoAcademico()
    {
        return cursoAcademico;
    }


    public TipoDTO getNivel()
    {
        return nivel;
    }

    public String getxTipoDml() {
        return xTipoDml;
    }

    public Date getxFecha() {
        return xFecha;
    }

    public PersonaUjiDTO getxPersonaId() {
        return xPersonaId;
    }
}