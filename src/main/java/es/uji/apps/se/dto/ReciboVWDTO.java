package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "REC_VW_RECIBOS")
public class ReciboVWDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "TIPO_RECIBO_ID")
    private Long tipoId;

    @Column(name = "TIPO_RECIBO_NOMBRE")
    private String tipoNombre;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;

    @Column(name = "FECHA_PAGO")
    @Temporal(TemporalType.DATE)
    private Date fechaPago;

    @Column(name = "IMPORTE_NETO")
    private Long importeNeto;

    @Column(name = "EMISORA_ID")
    private Long emisoraId;

    @Column(name = "EMISORA_NOMBRE")
    private String emisoraNombre;

    @Column(name = "TIPO_COBRO_ID")
    private Long tipoCobroId;

    @Column(name = "TIPO_COBRO_NOMBRE")
    private String tipoCobroNombre;

    @Column(name = "FECHA_PAGO_LIMITE")
    private Date fechaPagoLimite;

    @Column(name = "REFERENCIA_ID")
    private Long referenciaId;

    @Column(name = "REFERENCIA2_ID")
    private Long referencia2Id;

    public ReciboVWDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public String getTipoNombre() {return tipoNombre;}
    public void setTipoNombre(String tipoNombre) {this.tipoNombre = tipoNombre;}

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observaciones) {this.observaciones = observaciones;}

    public Date getFechaCreacion() {return fechaCreacion;}
    public void setFechaCreacion(Date fechaCreacion) {this.fechaCreacion = fechaCreacion;}

    public Date getFechaPago() {return fechaPago;}
    public void setFechaPago(Date fechaPago) {this.fechaPago = fechaPago;}

    public Long getImporteNeto() {return importeNeto;}
    public void setImporteNeto(Long importeNeto) {this.importeNeto = importeNeto;}

    public Long getEmisoraId() {return emisoraId;}
    public void setEmisoraId(Long emisoraId) {this.emisoraId = emisoraId;}

    public String getEmisoraNombre() {return emisoraNombre;}
    public void setEmisoraNombre(String emisoraNombre) {this.emisoraNombre = emisoraNombre;}

    public Long getTipoCobroId() {return tipoCobroId;}
    public void setTipoCobroId(Long tipoCobroId) {this.tipoCobroId = tipoCobroId;}

    public String getTipoCobroNombre() {return tipoCobroNombre;}
    public void setTipoCobroNombre(String tipoCobroNombre) {this.tipoCobroNombre = tipoCobroNombre;}

    public Date getFechaPagoLimite() {
        return fechaPagoLimite;
    }
    public void setFechaPagoLimite(Date fechaPagoLimite) {
        this.fechaPagoLimite = fechaPagoLimite;
    }

    public Long getReferenciaId() {
        return referenciaId;
    }
    public void setReferenciaId(Long referenciaId) {
        this.referenciaId = referenciaId;
    }

    public Long getReferencia2Id() {return referencia2Id;}
    public void setReferencia2Id(Long referencia2Id) {this.referencia2Id = referencia2Id;}
}
