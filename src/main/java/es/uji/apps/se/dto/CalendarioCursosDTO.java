package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_CALENDARIO_CURSOS")
public class CalendarioCursosDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CURSO_ID")
    private Long cursoId;

    @Column(name = "CALENDARIO_ID")
    private Long calendarioId;

    public CalendarioCursosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCursoId() {
        return cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }

    public Long getCalendarioId() {return calendarioId;}
    public void setCalendarioId(Long calendarioId) {this.calendarioId = calendarioId;}
}
