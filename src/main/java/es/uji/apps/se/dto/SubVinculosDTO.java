package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PER_SUBVINCULOS")
public class SubVinculosDTO {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "VIN_ID")
    private Long vinId;

    @Column(name = "NOMBRE")
    private String nombre;

    public SubVinculosDTO() {
    }

    public SubVinculosDTO(Long id, Long vinId, String nombre) {
        this.id = id;
        this.vinId = vinId;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVinId() {
        return vinId;
    }

    public void setVinId(Long vinId) {
        this.vinId = vinId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
