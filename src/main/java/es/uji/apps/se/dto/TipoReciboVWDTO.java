package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_EXT_TIPOS_RECIBOS")
public class TipoReciboVWDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
