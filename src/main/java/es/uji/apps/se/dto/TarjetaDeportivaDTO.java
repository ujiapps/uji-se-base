package es.uji.apps.se.dto;

import es.uji.apps.se.services.DateExtensions;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity @Table(name = "SE_TARJETAS_DEPORTIVAS")
public class TarjetaDeportivaDTO implements Serializable
{
    private static final int DIAS_RENOVACION = 15;

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", insertable=false, updatable = false)
    private PersonaUjiDTO personaUji;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaDTO personaSE;

    @ManyToOne
    @JoinColumn(name = "TARJETA_DEPORTIVA_TIPO_ID")
    private TarjetaDeportivaTipoDTO tarjetaDeportivaTipo;

    @Column(name = "FECHA_ALTA")
    private Date fechaAlta;

    @Column(name = "FECHA_BAJA")
    private Date fechaBaja;

    @Column(name = "GENERAR_RECIBO")
    private Boolean generarRecibo;

    @Column(name="PERSONA_ID_ACCION")
    private Long personaAccion;

    @Column(name="COMENTARIOS")
    private String comentarios;

    @Transient
    private String vinculoNombre;

    @Transient
    private Boolean sancion;

    @OneToMany(mappedBy = "tarjetaDeportivaDTO")
    private Set<TarjetaDeportivaReservaDTO> tarjetaDeportivaReservasDTO;

    @OneToMany(mappedBy = "tarjetaDeportivaDTO")
    private Set<TarjetaDeportivaClaseDTO> tarjetaDeportivaClasesDTO;

    public TarjetaDeportivaDTO()
    {
    }

    public TarjetaDeportivaDTO(Long tarjetaDeportivaId)
    {
        this.id = tarjetaDeportivaId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public PersonaUjiDTO getPersonaUji()
    {
        return personaUji;
    }

    public void setPersonaUji(PersonaUjiDTO persona)
    {
        this.personaUji = persona;
    }

    public PersonaDTO getPersonaSE() {
        return personaSE;
    }

    public void setPersonaSE(PersonaDTO personaSE) {
        this.personaSE = personaSE;
    }

    public Set<TarjetaDeportivaReservaDTO> getTarjetaDeportivaReservas()
    {
        return tarjetaDeportivaReservasDTO;
    }

    public void setTarjetaDeportivaReservas(Set<TarjetaDeportivaReservaDTO> tarjetaDeportivaReservasDTO)
    {
        this.tarjetaDeportivaReservasDTO = tarjetaDeportivaReservasDTO;
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportivaTipo()
    {
        return tarjetaDeportivaTipo;
    }

    public void setTarjetaDeportivaTipo(TarjetaDeportivaTipoDTO tarjetaDeportivaTipo)
    {
        this.tarjetaDeportivaTipo = tarjetaDeportivaTipo;
    }

    public Set<TarjetaDeportivaClaseDTO> getTarjetaDeportivaClases()
    {
        return tarjetaDeportivaClasesDTO;
    }

    public void setTarjetaDeportivaClases(Set<TarjetaDeportivaClaseDTO> tarjetaDeportivaClasesDTO)
    {
        this.tarjetaDeportivaClasesDTO = tarjetaDeportivaClasesDTO;
    }

    public Date getFechaAlta()
    {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta)
    {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja()
    {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja)
    {
        this.fechaBaja = fechaBaja;
    }

    public boolean isPeriodoRenovacion()
    {
        Date fechaActual = DateExtensions.addDays(DateExtensions.getCurrentDate(), -1);
        return (fechaActual.before(fechaBaja) && fechaActual.after(DateExtensions.addDays(fechaBaja, -DIAS_RENOVACION)));
    }

    public CalendarioClaseDTO getCalendarioClaseSolapado(CalendarioClaseDTO calendarioClaseDTO)
    {
        return tarjetaDeportivaClasesDTO.stream().map(tdp -> tdp.getCalendarioClase())
                        .filter(c -> c.getFechaHoraInicio().before(calendarioClaseDTO.getFechaHoraFin()) && c.getFechaHoraFin().after(calendarioClaseDTO.getFechaHoraInicio())).findFirst().orElse(null);
    }

    public String getVinculoNombre()
    {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre)
    {
        this.vinculoNombre = vinculoNombre;
    }

    public Boolean getSancion() {
        return sancion;
    }

    public void setSancion(Boolean sancion) {
        this.sancion = sancion;
    }

    public Boolean isSancion() {
        return sancion;
    }

    public Boolean getGenerarRecibo() {
        return generarRecibo;
    }

    public Boolean isGenerarRecibo() {
        return generarRecibo;
    }

    public void setGenerarRecibo(Boolean generarRecibo) {
        this.generarRecibo = generarRecibo;
    }

    public Long getPersonaAccion() {
        return personaAccion;
    }

    public void setPersonaAccion(Long personaAccion) {
        this.personaAccion = personaAccion;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
}