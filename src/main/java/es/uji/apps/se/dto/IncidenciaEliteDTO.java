package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_ELITE_INCIDENCIAS")
public class IncidenciaEliteDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO persona;

    @ManyToOne
    @JoinColumn(name = "TIPO_INCIDENCIA_ID")
    private EliteIncidenciaTipoDTO tipoIncidencia;

    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademicoDTO cursoAcademicoDTO;

    private String motivo;

    private String detalle;

    private String asignatura;

    private String comentarios;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_EXAMEN")
    private Date fecha;

    @ManyToOne
    @JoinColumn(name = "PROFESOR_ID")
    private PersonaUjiDTO profesor;

    @Column(name = "FECHA_CAMBIO")
    private String fechaCambio;

    @Column(name = "ASIGNATURAS_MATRICULA")
    private String asignaturasMatricula;

    @Column(name = "ASIGNATURAS_ANULA")
    private String asignaturasAnula;

    @ManyToOne
    @JoinColumn(name = "ESTADO_ID")
    private TipoDTO estado;

    @Column(name = "FECHA_RESOLUCION")
    private Date fechaResolucion;

    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    public IncidenciaEliteDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonaUjiDTO getPersona() {
        return persona;
    }

    public void setPersona(PersonaUjiDTO persona) {
        this.persona = persona;
    }

    public EliteIncidenciaTipoDTO getTipoIncidencia() {
        return tipoIncidencia;
    }

    public void setTipoIncidencia(EliteIncidenciaTipoDTO tipoIncidencia) {
        this.tipoIncidencia = tipoIncidencia;
    }

    public CursoAcademicoDTO getCursoAcademico() {
        return cursoAcademicoDTO;
    }

    public void setCursoAcademico(CursoAcademicoDTO cursoAcademicoDTO) {
        this.cursoAcademicoDTO = cursoAcademicoDTO;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public PersonaUjiDTO getProfesor() {
        return profesor;
    }

    public void setProfesor(PersonaUjiDTO profesor) {
        this.profesor = profesor;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public String getAsignaturasMatricula() {
        return asignaturasMatricula;
    }

    public void setAsignaturasMatricula(String asignaturasMatricula) {
        this.asignaturasMatricula = asignaturasMatricula;
    }

    public String getAsignaturasAnula() {
        return asignaturasAnula;
    }

    public void setAsignaturasAnula(String asignaturasAnula) {
        this.asignaturasAnula = asignaturasAnula;
    }

    public TipoDTO getEstado() {
        return estado;
    }

    public void setEstado(TipoDTO estado) {
        this.estado = estado;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public String getResolucion() {
        return getEstado().getNombre() + " - " + getFechaResolucion();
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
}
