package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_VW_USO_CLA_PER_VIN")
public class VinculacionActivaVWDTO implements Serializable {

    @Id
    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "VINCULO_ID")
    private Long vinculoId;

    @Column(name = "USO")
    private String uso;

    public VinculacionActivaVWDTO() {}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getVinculoId() {return vinculoId;}
    public void setVinculoId(Long vinculoId) {this.vinculoId = vinculoId;}

    public String getUso() {return uso;}
    public void setUso(String uso) {this.uso = uso;}
}
