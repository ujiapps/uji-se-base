package es.uji.apps.se.dto;

import es.uji.commons.rest.annotations.DataTag;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_CARNETS_BONOS_TIPOS")
public class CarnetBonoTipoDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @DataTag
    @Column(name = "NOMBRE")
    private String nombre;

    @DataTag
    @Column(name = "ACTIVO")
    private Boolean activo;

    @Column(name = "TIPO_INSTALACION_ID")
    private Long tipoInstalacionId;

    @Column(name = "TIPO_RESERVA_ID")
    private Long tipoReservaId;

    @Column(name = "ORDEN_USO")
    private Long ordenUso;

    @Column(name = "USOS")
    private Long usos;

    @Column(name = "DIAS")
    private Long dias;

    @Column(name = "EDAD_MIN")
    private Long edadMin;

    @Column(name = "EDAD_MAX")
    private Long edadMax;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INI")
    private Date fechaInicio;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INI_VALIDEZ")
    private Date fechaInicioValidez;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN_VALIDEZ")
    private Date fechaFinValidez;

    @Column(name = "MOSTRAR_USUARIO")
    private Boolean mostrarUsuario;

    @OneToMany(mappedBy = "tipo")
    private Set<CarnetBonoDTO> carnetBonosDTO;

    @OneToMany(mappedBy = "tipoHistorico")
    private Set<CarnetBonoHistoricoDTO> carnetBonosHistoricoDTO;

    public CarnetBonoTipoDTO() {
    }

    public CarnetBonoTipoDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public Long getTipoInstalacionId() {
        return tipoInstalacionId;
    }

    public void setTipoInstalacionId(Long tipoInstalacionId) {
        this.tipoInstalacionId = tipoInstalacionId;
    }

    public Long getTipoReservaId() {
        return tipoReservaId;
    }

    public void setTipoReservaId(Long tipoReservaId) {
        this.tipoReservaId = tipoReservaId;
    }

    public Boolean getActivo() {
        return activo;
    }

    public Boolean isActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Long getOrdenUso() {
        return ordenUso;
    }

    public void setOrdenUso(Long ordenUso) {
        this.ordenUso = ordenUso;
    }

    public Long getUsos() {
        return usos;
    }

    public void setUsos(Long usos) {
        this.usos = usos;
    }

    public Long getDias() {
        return dias;
    }

    public void setDias(Long dias) {
        this.dias = dias;
    }

    public Long getEdadMin() {
        return edadMin;
    }

    public void setEdadMin(Long edadMin) {
        this.edadMin = edadMin;
    }

    public Long getEdadMax() {
        return edadMax;
    }

    public void setEdadMax(Long edadMax) {
        this.edadMax = edadMax;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaInicioValidez() {
        return fechaInicioValidez;
    }

    public void setFechaInicioValidez(Date fechaInicioValidez) {
        this.fechaInicioValidez = fechaInicioValidez;
    }

    public Date getFechaFinValidez() {
        return fechaFinValidez;
    }

    public void setFechaFinValidez(Date fechaFinValidez) {
        this.fechaFinValidez = fechaFinValidez;
    }

    public Boolean getMostrarUsuario() {
        return mostrarUsuario;
    }

    public void setMostrarUsuario(Boolean mostrarUsuario) {
        this.mostrarUsuario = mostrarUsuario;
    }

    public Boolean isMostrarUsuario() {
        return mostrarUsuario;
    }

    public Set<CarnetBonoDTO> getCarnetBonosDTO() {
        return carnetBonosDTO;
    }

    public void setCarnetBonosDTO(Set<CarnetBonoDTO> carnetBonosDTO) {
        this.carnetBonosDTO = carnetBonosDTO;
    }

    public Set<CarnetBonoHistoricoDTO> getCarnetBonosHistoricoDTO() {
        return carnetBonosHistoricoDTO;
    }

    public void setCarnetBonosHistoricoDTO(Set<CarnetBonoHistoricoDTO> carnetBonosHistoricoDTO) {
        this.carnetBonosHistoricoDTO = carnetBonosHistoricoDTO;
    }
}