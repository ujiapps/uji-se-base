package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_BICICLETAS_HISTORICO")
public class BicicletasHistoricoVWDTO implements Serializable {
    @Id
    @Column(name = "XFECHA")
    private Date xFecha;

    @Column(name = "ID")
    private Long id;
    @Column(name = "ACCION")
    private String accion;
    @Column(name = "APELLIDOS_NOMBRE")
    private String apellidosNombre;
    @Column(name = "BICICLETAID")
    private Long bicicletaId;
    @Column(name = "FECHA")
    private Date fecha;
    @Column(name = "FECHA_FIN")
    private Date fechaFin;
    @Column(name = "FECHA_INI")
    private Date fechaIni;
    @Column(name = "FECHA_PREV_DEVOL")
    private Date fechaPrevDevol;
    @Column(name = "NUM_BICI")
    private Long numBicis;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "PERSONA_ID")
    private Long personaId;
    @Column(name = "TIPO_ID")
    private Long tipoId;
    @Column(name = "TIPO_RESERVA")
    private String tipoReserva;
    @Column(name = "TIPOUSO")
    private String tipoUso;

    public BicicletasHistoricoVWDTO() {
    }

    public Date getxFecha() {
        return xFecha;
    }

    public void setxFecha(Date xFecha) {
        this.xFecha = xFecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public Long getBicicletaId() {
        return bicicletaId;
    }

    public void setBicicletaId(Long bicicletaId) {
        this.bicicletaId = bicicletaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaPrevDevol() {
        return fechaPrevDevol;
    }

    public void setFechaPrevDevol(Date fechaPrevDevol) {
        this.fechaPrevDevol = fechaPrevDevol;
    }

    public Long getNumBicis() {
        return numBicis;
    }

    public void setNumBicis(Long numBicis) {
        this.numBicis = numBicis;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getTipoId() {
        return tipoId;
    }

    public void setTipoId(Long tipoId) {
        this.tipoId = tipoId;
    }

    public String getTipoReserva() {
        return tipoReserva;
    }

    public void setTipoReserva(String tipoReserva) {
        this.tipoReserva = tipoReserva;
    }

    public String getTipoUso() {
        return tipoUso;
    }

    public void setTipoUso(String tipoUso) {
        this.tipoUso = tipoUso;
    }
}
