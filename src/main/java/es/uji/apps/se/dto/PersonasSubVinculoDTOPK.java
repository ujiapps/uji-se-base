package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PersonasSubVinculoDTOPK implements Serializable {
    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private PersonaUjiDTO personaDTO;
    @Column(name = "SVI_VIN_ID")
    private Long sviVINid;
    @Column(name = "SVI_ID")
    private Long sviId;

    public PersonasSubVinculoDTOPK() {
    }

    public PersonasSubVinculoDTOPK(PersonaUjiDTO personaDTO, Long sviVINid, Long sviId) {
        this.personaDTO = personaDTO;
        this.sviVINid = sviVINid;
        this.sviId = sviId;
    }

    public PersonaUjiDTO getPersonaDTO() {
        return personaDTO;
    }

    public void setPersonaDTO(PersonaUjiDTO personaDTO) {
        this.personaDTO = personaDTO;
    }

    public Long getSviVINid() {
        return sviVINid;
    }

    public void setSviVINid(Long sviVINid) {
        this.sviVINid = sviVINid;
    }

    public Long getSviId() {
        return sviId;
    }

    public void setSviId(Long sviId) {
        this.sviId = sviId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonasSubVinculoDTOPK that = (PersonasSubVinculoDTOPK) o;
        return Objects.equals(personaDTO.getId(), that.personaDTO.getId()) && Objects.equals(sviVINid, that.sviVINid) && Objects.equals(sviId, that.sviId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personaDTO.getId(), sviVINid, sviId);
    }
}
