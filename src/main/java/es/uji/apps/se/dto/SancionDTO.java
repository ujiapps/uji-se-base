package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_SANCIONES")
public class SancionDTO implements Serializable {

    @Id
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Column(name = "MOTIVO")
    private String motivo;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademico;

    @Column(name = "PERSONA_ID_ACCION")
    private Long personaAccion;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "NIVEL_ID")
    private TipoDTO nivel;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getPersonaId() {return personaId;}

    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public String getMotivo()
    {
        return motivo;
    }

    public void setMotivo(String motivo)
    {
        this.motivo = motivo;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getCursoAcademico()
    {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public TipoDTO getNivel()
    {
        return nivel;
    }

    public void setNivel(TipoDTO nivel)
    {
        this.nivel = nivel;
    }

    public Long getPersonaAccion() {
        return personaAccion;
    }

    public void setPersonaAccion(Long personaAccion) {
        this.personaAccion = personaAccion;
    }
}