package es.uji.apps.se.dto;

import es.uji.apps.se.model.EquipacionTipo;

import javax.persistence.*;

@Entity
@Table(name = "SE_EQUIPACIONES_TIPOS")
public class EquipacionTipoDTO {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    public EquipacionTipoDTO() {}

    public EquipacionTipoDTO(EquipacionTipo tipo) {
        this.id = tipo.getId();
        this.nombre = tipo.getNombre();
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
}