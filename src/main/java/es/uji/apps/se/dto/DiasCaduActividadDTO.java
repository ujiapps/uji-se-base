package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_ACTIVIDADES_CADU_DIAS")
public class DiasCaduActividadDTO implements Serializable {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "OFERTA_ID")
    private Long ofertaId;

    @Column(name = "DIAS")
    private Long dias;

    public DiasCaduActividadDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getOfertaId() {return ofertaId;}
    public void setOfertaId(Long ofertaId) {this.ofertaId = ofertaId;}

    public Long getDias() {return dias;}
    public void setDias(Long dias) {this.dias = dias;}
}
