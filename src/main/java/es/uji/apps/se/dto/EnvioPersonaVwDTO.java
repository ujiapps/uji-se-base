package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "se_vw_personas_envios")
public class EnvioPersonaVwDTO implements Serializable {
    @Id
    @Column(name = "PERSONA_ID")
    private Long persona;
    @Id
    @Column(name = "ENVIO_ID")
    private Long envio;

    private String nombre;
    private String identificacion;
    private String mail;

    @Column(name = "recibir_correo")
    private Boolean recibirCorreo;

    public EnvioPersonaVwDTO() {
    }

    public Long getPersona() {
        return persona;
    }

    public Long getEnvio() {
        return envio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getMail() {
        return mail;
    }

    public Boolean getRecibirCorreo() {
        return recibirCorreo;
    }

    public Boolean isRecibirCorreo() {
        return recibirCorreo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnvioPersonaVwDTO that = (EnvioPersonaVwDTO) o;
        return Objects.equals(persona, that.persona) && Objects.equals(envio, that.envio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(persona, envio);
    }
}
