package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SE_BIC_TIPOS_RESERVAS_VINCULOS")
public class BicicletasTiposReservasVinculosDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Id
    @Column(name = "ACTIVIDAD_ID")
    private Long actividadId;
    @Id
    @Column(name = "CARNET_BONO_ID")
    private Long carnetBonoId;
    @Id
    @Column(name = "TIPO_RESERVA_ID")
    private Long tipoReservaId;
    @Id
    @Column(name = "VINCULACION_ID")
    private Long vinculacionId;

    public BicicletasTiposReservasVinculosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public Long getCarnetBonoId() {
        return carnetBonoId;
    }

    public void setCarnetBonoId(Long carnetBonoId) {
        this.carnetBonoId = carnetBonoId;
    }

    public Long getTipoReservaId() {
        return tipoReservaId;
    }

    public void setTipoReservaId(Long tipoReservaId) {
        this.tipoReservaId = tipoReservaId;
    }

    public Long getVinculacionId() {
        return vinculacionId;
    }

    public void setVinculacionId(Long vinculacionId) {
        this.vinculacionId = vinculacionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BicicletasTiposReservasVinculosDTO that = (BicicletasTiposReservasVinculosDTO) o;
        return Objects.equals(id, that.id) &&
               Objects.equals(actividadId, that.actividadId) &&
               Objects.equals(carnetBonoId, that.carnetBonoId) &&
               Objects.equals(tipoReservaId, that.tipoReservaId) &&
               Objects.equals(vinculacionId, that.vinculacionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, actividadId, carnetBonoId, tipoReservaId, vinculacionId);
    }
}
