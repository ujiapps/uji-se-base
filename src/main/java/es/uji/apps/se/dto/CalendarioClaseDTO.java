package es.uji.apps.se.dto;

import es.uji.apps.se.services.DateExtensions;
import es.uji.commons.rest.annotations.DataTag;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SE_CALENDARIO_CLASES_DIRIGIDAS")
public class CalendarioClaseDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @DataTag
    @Column(name = "HORA_INICIO")
    private String horaInicio;

    @DataTag
    @Column(name = "HORA_FIN")
    private String horaFin;

    @DataTag
    @Column(name = "COMENTARIOS")
    private String comentarios;

    @ManyToOne
    @JoinColumn(name = "CALENDARIO_ID")
    private CalendarioDTO calendarioDTO;

    @ManyToOne
    @JoinColumn(name = "CLASE_DIRIGIDA_ID")
    private ClaseDTO claseDTO;

    @OneToMany(mappedBy = "calendarioClase")
    private Set<TarjetaDeportivaClaseDTO> tarjetaDeportivaClasesDTO;

    @OneToMany(mappedBy = "calendarioClaseDTO")
    private Set<MonitorClaseCalendarioDTO> monitorClaseCalendariosDTO;

    @OneToMany(mappedBy = "calendarioClaseDTO")
    private Set<CheckDTO> checksCalendarios;

    @OneToMany(mappedBy = "calendarioClaseDTO")
    private Set<CalendarioClaseInstalacionDTO> calendarioClaseInstalacionesDTO;

    public CalendarioClaseDTO() {
    }

    public CalendarioClaseDTO(Long calendarioClaseDirigidaId) {
        this.id = calendarioClaseDirigidaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public CalendarioDTO getCalendario() {
        return calendarioDTO;
    }

    public void setCalendario(CalendarioDTO calendarioDTO) {
        this.calendarioDTO = calendarioDTO;
    }

    public ClaseDTO getClase() {
        return claseDTO;
    }

    public void setClase(ClaseDTO claseDTO) {
        this.claseDTO = claseDTO;
    }

    public Set<TarjetaDeportivaClaseDTO> getTarjetaDeportivaClases() {
        return tarjetaDeportivaClasesDTO;
    }

    public void setTarjetaDeportivaClases(Set<TarjetaDeportivaClaseDTO> tarjetaDeportivaClasesDTO) {
        this.tarjetaDeportivaClasesDTO = tarjetaDeportivaClasesDTO;
    }

    public Date getFechaHoraInicio() {
        try {
            String[] hora = horaInicio.split(":");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(calendarioDTO.getDia());
            calendar.set(Calendar.HOUR, Integer.parseInt(hora[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(hora[1]));
            return calendar.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    public Date getFechaHoraFin() {
        try {
            String[] hora = horaFin.split(":");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(calendarioDTO.getDia());
            calendar.set(Calendar.HOUR, Integer.parseInt(hora[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(hora[1]));
            return calendar.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    public Date getUltimaFechaLimiteReserva(int limiteReserva) {
        return DateExtensions.addMinutes(getFechaHoraInicio(), limiteReserva);
    }

    public Date getUltimaFechaLimiteAnulacion(int limiteAnulacion) {
        return DateExtensions.addMinutes(getFechaHoraInicio(), limiteAnulacion);
    }

    public Set<MonitorClaseCalendarioDTO> getMonitorClaseCalendarios() {
        return monitorClaseCalendariosDTO;
    }

    public void setMonitorClaseCalendarios(Set<MonitorClaseCalendarioDTO> monitorClaseCalendariosDTO) {
        this.monitorClaseCalendariosDTO = monitorClaseCalendariosDTO;
    }

    public Long getPlazasOcupadas() {
        return (long) tarjetaDeportivaClasesDTO.size();
    }

    public Long getPlazasLibres() {
        return claseDTO.getPlazas() - getPlazasOcupadas();
    }

    public Boolean quedanPlazasLibres() {
        return getPlazasOcupadas() < claseDTO.getPlazas();
    }

    public TarjetaDeportivaClaseDTO getReservaIdByPersona(Long personaId) {
        return tarjetaDeportivaClasesDTO.stream().filter(tdc -> tdc.getTarjetaDeportiva().getPersonaUji().getId().equals(personaId)).findFirst().orElse(null);
    }

    public Set<CheckDTO> getChecksCalendarios() {
        return checksCalendarios;
    }

    public void setChecksCalendarios(Set<CheckDTO> checksCalendarios) {
        this.checksCalendarios = checksCalendarios;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} - {1} {2} ({3} plaçes lliures)", DateExtensions.getDateAsString(getCalendario().getDia()), horaInicio, claseDTO.getNombre(), getPlazasLibres());
    }

    public Set<CalendarioClaseInstalacionDTO> getCalendarioClaseInstalacionesDTO() {
        return calendarioClaseInstalacionesDTO;
    }

    public void setCalendarioClaseInstalacionesDTO(Set<CalendarioClaseInstalacionDTO> calendarioClaseInstalacionesDTO) {
        this.calendarioClaseInstalacionesDTO = calendarioClaseInstalacionesDTO;
    }

    public String getInstalacionesNombres() {
        String instalaciones = this.getCalendarioClaseInstalacionesDTO().stream().map(calendarioClaseInstalacionesDTO -> calendarioClaseInstalacionesDTO.getInstalacionDTO().getNombre()).reduce("", (acumulador, instalacionNombre) -> acumulador + instalacionNombre + ", ").trim();
        instalaciones = instalaciones.substring(0, instalaciones.length() - 1);
        return instalaciones;
    }
}