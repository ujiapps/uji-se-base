package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_ADMINISTRADORES")
public class PersonaAdminDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    @Column(name = "FECHA_BAJA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;

    public PersonaAdminDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonaUjiDTO getPersonaUjiDTO() {
        return personaUjiDTO;
    }

    public void setPersonaUjiDTO(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}
}