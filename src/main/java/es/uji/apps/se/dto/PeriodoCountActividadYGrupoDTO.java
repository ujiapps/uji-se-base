package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_VW_PERIODOS_COUNT_ACT")
public class PeriodoCountActividadYGrupoDTO implements Serializable {

    @Id
    @Column(name="PERIODO_ID")
    private Long periodo;

    @Column(name="NUMERO_ACT")
    private Long numeroActividades;

    @Column(name="NUMERO_OFERTAS")
    private Long numeroOfertas;

    public Long getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Long periodo) {
        this.periodo = periodo;
    }

    public Long getNumeroActividades() {
        return numeroActividades;
    }

    public void setNumeroActividades(Long numeroActividades) {
        this.numeroActividades = numeroActividades;
    }

    public Long getNumeroOfertas() {
        return numeroOfertas;
    }

    public void setNumeroOfertas(Long numeroOfertas) {
        this.numeroOfertas = numeroOfertas;
    }
}
