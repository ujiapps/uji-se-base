package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_CALENDARIO_OFERTAS_ACT")
public class CalendarioOfertaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="COMENTARIOS")
    private String comentarios;

    @Column(name="HORA_INI")
    private String horaInicio;

    @Column(name="HORA_FIN")
    private String horaFin;

    @Column(name="ASISTENCIA_OK")
    private Boolean asistencia;

    @Column(name="OBLIGATORIA")
    private Boolean obligatoria;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID")
    private OfertaDTO ofertaDTO;

    @ManyToOne
    @JoinColumn(name="CALENDARIO_ID")
    private CalendarioDTO calendarioDTO;

    @ManyToOne
    @JoinColumn(name="TIPO_RESERVA")
    private TipoDTO tipoReservaDTO;

    public CalendarioOfertaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Boolean getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(Boolean asistencia) {
        this.asistencia = asistencia;
    }

    public Boolean getObligatoria() {
        return obligatoria;
    }

    public void setObligatoria(Boolean obligatoria) {
        this.obligatoria = obligatoria;
    }

    public OfertaDTO getOferta() {
        return ofertaDTO;
    }

    public void setOferta(OfertaDTO ofertaDTO) {
        this.ofertaDTO = ofertaDTO;
    }

    public CalendarioDTO getCalendario() {
        return calendarioDTO;
    }

    public void setCalendario(CalendarioDTO calendarioDTO) {
        this.calendarioDTO = calendarioDTO;
    }

    public TipoDTO getTipoReserva() {
        return tipoReservaDTO;
    }

    public void setTipoReserva(TipoDTO tipoReservaDTO) {
        this.tipoReservaDTO = tipoReservaDTO;
    }
}