package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_CARNETS_BONOS_DETALLES")
public class CarnetBonoDetalleDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "CARNET_BONO_ID")
    private Long carnetBonoId;

    @Column(name = "TIPO_ID")
    private Long tipoId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA")
    private Date fechaEntrada;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_SALIDA")
    private Date fechaSalida;

    public CarnetBonoDetalleDTO() {}

    public CarnetBonoDetalleDTO(Long carnetBonoId, Long tipoId, Date fechaEntrada, Date fechaSalida) {
        this.carnetBonoId = carnetBonoId;
        this.tipoId = tipoId;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getCarnetBonoId() {return carnetBonoId;}
    public void setCarnetBonoId(Long carnetBonoId) {this.carnetBonoId = carnetBonoId;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public Date getFechaEntrada() {return fechaEntrada;}
    public void setFechaEntrada(Date fechaEntrada) {this.fechaEntrada = fechaEntrada;}

    public Date getFechaSalida() {return fechaSalida;}
    public void setFechaSalida(Date fechaSalida) {this.fechaSalida = fechaSalida;}
}
