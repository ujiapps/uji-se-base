package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_EQUIPACIONES_MOROSOS")
public class EquipacionesPendientesVWDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long personaId;

    @Column(name = "APELLIDOS_NOMBRE")
    private String nombreCompleto;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademico;

    @Column(name = "FECHA_MAX")
    @Temporal(TemporalType.DATE)
    private Date fechaMax;

    public EquipacionesPendientesVWDTO() {
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public Date getFechaMax() {
        return fechaMax;
    }

    public void setFechaMax(Date fechaMax) {
        this.fechaMax = fechaMax;
    }
}
