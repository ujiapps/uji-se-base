package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_INSCRIPCIONES_ESPERA")
public class InscripcionEsperaDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="OFERTA_ID")
    private Long ofertaId;

    @Column(name="PERSONA_ID")
    private Long personaId;

    @Column(name="FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name="TELEFONO")
    private Long telefono;

    @Column(name="ESTADO_ID")
    private Long estadoId;

    @Column(name="MODALIDAD_ID")
    private Long modalidadId;

    @Column(name="OBSERVACIONES")
    private String observaciones;

    @Column(name="POS_ESPERA")
    private Long posEspera;

    @Column(name="PERSONA_ID_ADMIN")
    private Long personaIdAdmin;

    @Column(name="VINCULO_ID")
    private Long vinculoId;

    @Column(name="PREINSCRIPCION_ID")
    private Long preinscripcionId;

    @Column(name="ORIGEN")
    private String origen;

    public InscripcionEsperaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOfertaId() {
        return ofertaId;
    }

    public void setOfertaId(Long ofertaId) {
        this.ofertaId = ofertaId;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public Long getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Long estadoId) {
        this.estadoId = estadoId;
    }

    public Long getModalidadId() {
        return modalidadId;
    }

    public void setModalidadId(Long modalidadId) {
        this.modalidadId = modalidadId;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getPosEspera() {
        return posEspera;
    }

    public void setPosEspera(Long posEspera) {
        this.posEspera = posEspera;
    }

    public Long getPersonaIdAdmin() {
        return personaIdAdmin;
    }

    public void setPersonaIdAdmin(Long personaIdAdmin) {
        this.personaIdAdmin = personaIdAdmin;
    }

    public Long getVinculoId() {
        return vinculoId;
    }

    public void setVinculoId(Long vinculoId) {
        this.vinculoId = vinculoId;
    }

    public Long getPreinscripcionId() {
        return preinscripcionId;
    }

    public void setPreinscripcionId(Long preinscripcionId) {
        this.preinscripcionId = preinscripcionId;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
}
