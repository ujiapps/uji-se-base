package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_EXT_ACCESOS_PERSONA")
public class AccesoDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    private String identificacion;
    private String nombre;
    @Column (name="PERSONA_ID")
    private Long personaId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dia;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="HORA_ENTRADA")
    private Date horaEntrada;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="HORA_SALIDA")
    private Date horaSalida;

    @Column (name="ZONA_ID")
    private Long zonaId;

    private String zona;

    private String destino;

    public AccesoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public Date getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(Date horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Long getZonaId() {
        return zonaId;
    }

    public void setZonaId(Long zonaId) {
        this.zonaId = zonaId;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }
}
