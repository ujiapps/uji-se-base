package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "REC_MOVIMIENTOS")
public class MovimientoReciboDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="RECIBO_ID")
    private Long reciboId;

    @Column(name="TIPO_ID")
    private Long tipoId;

    public MovimientoReciboDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getReciboId() {return reciboId;}
    public void setReciboId(Long reciboId) {this.reciboId = reciboId;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}
}
