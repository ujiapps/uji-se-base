package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_COMPETICIONES_MIEMBROS")
public class CompeticionMiembroDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name="EQUIPO_ID")
    private CompeticionEquipoDTO competicionEquipo;

    @ManyToOne
    @JoinColumn(name="INSCRIPCION_ID")
    private InscripcionDTO inscripcion;

    @Column(name = "CAPITAN")
    private Long capitan;

    @Column(name = "ARBITRO")
    private Long arbitro;

    @OneToMany(mappedBy = "competicionMiembro")
    private Set<CompeticionSancionDTO> competicionSanciones;

    public CompeticionMiembroDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CompeticionEquipoDTO getCompeticionEquipo() {
        return competicionEquipo;
    }

    public void setCompeticionEquipo(CompeticionEquipoDTO competicionEquipo) {
        this.competicionEquipo = competicionEquipo;
    }

    public InscripcionDTO getInscripcion() {
        return inscripcion;
    }

    public void setInscripcion(InscripcionDTO inscripcion) {
        this.inscripcion = inscripcion;
    }

    public Long getCapitan() {return capitan;}
    public void setCapitan(Long capitan) {this.capitan = capitan;}

    public Long getArbitro() {return arbitro;}
    public void setArbitro(Long arbitro) {this.arbitro = arbitro;}

    public Set<CompeticionSancionDTO> getCompeticionSanciones() {
        return competicionSanciones;
    }

    public void setCompeticionSanciones(Set<CompeticionSancionDTO> competicionSanciones) {
        this.competicionSanciones = competicionSanciones;
    }
}