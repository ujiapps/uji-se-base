package es.uji.apps.se.dto;

import es.uji.apps.se.model.EquipacionTipoMovimiento;

import javax.persistence.*;

@Entity
@Table(name = "SE_EQUIPACIONES_TIPOS_MOV")
public class EquipacionTipoMovimientoDTO {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="TIPO_ID")
    private Long tipoId;

    @Column(name="ETIQUETA")
    private String etiqueta;

    public EquipacionTipoMovimientoDTO() {}

    public EquipacionTipoMovimientoDTO(EquipacionTipoMovimiento tipoMovimiento) {
        this.id = tipoMovimiento.getId();
        this.nombre = tipoMovimiento.getNombre();
        this.tipoId = tipoMovimiento.getTipoId();
        this.etiqueta = tipoMovimiento.getEtiqueta();
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public Long getTipoId() { return tipoId; }
    public void setTipoId(Long tipoId) { this.tipoId = tipoId; }

    public String getEtiqueta() { return etiqueta; }
    public void setEtiqueta(String etiqueta) { this.etiqueta = etiqueta; }
}