package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_TARJETAS_BONOS")
public class CarnetBonoVWDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "TIPO_ID")
    private Long tipoId;

    @Column(name = "TIPO_INSTALACION_ID")
    private Long tipoInstalacionId;

    @Column(name = "USOS")
    private Long usos;

    @Column(name = "TOTAL_USOS")
    private Long totalUsos;

    @Column(name = "ACTIVO")
    private Long activo;

    @Column(name = "PLAZOS")
    private Long plazos;

    @Column(name = "DIAS")
    private Long dias;

    @Column(name = "NETO")
    private Long neto;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "ESTADO2")
    private String estado2;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "ORIGEN")
    private String origen;

    @Column(name = "FECHA_INI")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;

    @Column(name = "FECHA_BAJA")
    @Temporal(TemporalType.DATE)
    private Date fechaBaja;

    public CarnetBonoVWDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public Long getTipoInstalacionId() {return tipoInstalacionId;}
    public void setTipoInstalacionId(Long tipoInstalacionId) {this.tipoInstalacionId = tipoInstalacionId;}

    public Long getUsos() {return usos;}
    public void setUsos(Long usos) {this.usos = usos;}

    public Long getTotalUsos() {return totalUsos;}
    public void setTotalUsos(Long totalUsos) {this.totalUsos = totalUsos;}

    public Long getActivo() {return activo;}
    public void setActivo(Long activo) {this.activo = activo;}

    public Long getPlazos() {return plazos;}
    public void setPlazos(Long plazos) {this.plazos = plazos;}

    public Long getDias() {return dias;}
    public void setDias(Long dias) {this.dias = dias;}

    public Long getNeto() {return neto;}
    public void setNeto(Long neto) {this.neto = neto;}

    public String getEstado() {return estado;}
    public void setEstado(String estado) {this.estado = estado;}

    public String getEstado2() {return estado2;}
    public void setEstado2(String estado2) {this.estado2 = estado2;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observaciones) {this.observaciones = observaciones;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}
}
