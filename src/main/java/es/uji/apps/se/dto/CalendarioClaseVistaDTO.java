package es.uji.apps.se.dto;

import es.uji.apps.se.services.DateExtensions;

import javax.persistence.*;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "se_vw_calendario_clases")
public class CalendarioClaseVistaDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CLASE")
    private String clase;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DIA")
    private Date dia;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DIA_HORA")
    private Date diaHora;

    @Column(name = "HORA_INICIO")
    private String horaInicio;

    @Column(name = "HORA_FIN")
    private String horaFin;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "RESERVA")
    private Long reserva;

    @Column(name="ASISTE")
    private Boolean asiste;

    @Column(name = "ICONO")
    private String icono;

    @Column(name = "PLAZAS")
    private Long plazas;

    @Column(name = "OCUPADAS")
    private Long ocupadas;

    @Column(name = "INSTALACION")
    private String instalacion;

    public CalendarioClaseVistaDTO() {
    }

    public CalendarioClaseVistaDTO(Long calendarioClaseDirigidaId) {
        this.id = calendarioClaseDirigidaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }


    public Date getFechaHoraInicio() {
        try {
            String[] hora = horaInicio.split(":");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dia);
            calendar.set(Calendar.HOUR, Integer.parseInt(hora[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(hora[1]));
            return calendar.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    public Date getFechaHoraFin() {
        try {
            String[] hora = horaFin.split(":");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dia);
            calendar.set(Calendar.HOUR, Integer.parseInt(hora[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(hora[1]));
            return calendar.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    public Date getUltimaFechaLimiteReserva(int limiteReserva) {
        return DateExtensions.addMinutes(getFechaHoraInicio(), limiteReserva);
    }

    public Date getUltimaFechaLimiteAnulacion(int limiteAnulacion) {
        return DateExtensions.addMinutes(getFechaHoraInicio(), limiteAnulacion);
    }

    public Long getPlazasLibres() {
        return plazas - ocupadas;
    }

    public Boolean quedanPlazasLibres() {
        return ocupadas < plazas;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public Long getPlazas() {
        return plazas;
    }

    public void setPlazas(Long plazas) {
        this.plazas = plazas;
    }

    public Long getOcupadas() {
        return ocupadas;
    }

    public void setOcupadas(Long ocupadas) {
        this.ocupadas = ocupadas;
    }

    public Long getReserva() {
        return reserva;
    }

    public void setReserva(Long reserva) {
        this.reserva = reserva;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    public Boolean getAsiste() {
        return asiste;
    }

    public void setAsiste(Boolean asiste) {
        this.asiste = asiste;
    }

    public Boolean isAsiste() {
        return asiste;
    }

    public Date getDiaHora() {
        return diaHora;
    }

    public void setDiaHora(Date diaHora) {
        this.diaHora = diaHora;
    }

    //    public TarjetaDeportivaClase getReservaIdByPersona(Long personaId) {
//        return null;
////        return tarjetaDeportivaClases.stream().filter(tdc -> tdc.getTarjetaDeportiva().getPersona().getId().equals(personaId)).findFirst().orElse(null);
//    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} - {1} {2} ({3} plaçes lliures)", DateExtensions.getDateAsString(dia), horaInicio, clase, getPlazasLibres());
    }
}