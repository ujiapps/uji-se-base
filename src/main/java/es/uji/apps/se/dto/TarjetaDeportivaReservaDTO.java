package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity @Table(name = "SE_TARJETA_DEPORTIVA_RESERVA")
public class TarjetaDeportivaReservaDTO implements Serializable
{
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TARJETA_DEPORTIVA_ID")
    private TarjetaDeportivaDTO tarjetaDeportivaDTO;

    @ManyToOne
    @JoinColumn(name = "INSTALACION_ID")
    private InstalacionDTO instalacionDTO;

    @Column(name = "HORA_INICIO")
    private String horaInicio;

    @Column(name = "HORA_FIN")
    private String horaFin;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "FECHA_ALTA")
    private Date fechaAlta;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID_ADMIN")
    private PersonaDTO personaAdminDTO;

    @Column(name="PERSONA_ID_ACCION")
    private Long personaAccion;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public TarjetaDeportivaDTO getTarjetaDeportiva()
    {
        return tarjetaDeportivaDTO;
    }

    public void setTarjetaDeportiva(TarjetaDeportivaDTO tarjetaDeportivaDTO)
    {
        this.tarjetaDeportivaDTO = tarjetaDeportivaDTO;
    }

    public InstalacionDTO getInstalacion()
    {
        return instalacionDTO;
    }

    public void setInstalacion(InstalacionDTO instalacionDTO)
    {
        this.instalacionDTO = instalacionDTO;
    }

    public String getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(String horaFin)
    {
        this.horaFin = horaFin;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaAlta()
    {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta)
    {
        this.fechaAlta = fechaAlta;
    }

    public String getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(String comentarios)
    {
        this.comentarios = comentarios;
    }

    public PersonaDTO getPersonaAdmin()
    {
        return personaAdminDTO;
    }

    public void setPersonaAdmin(PersonaDTO personaAdminDTO)
    {
        this.personaAdminDTO = personaAdminDTO;
    }

    public Long getPersonaAccion() {
        return personaAccion;
    }

    public void setPersonaAccion(Long personaAccion) {
        this.personaAccion = personaAccion;
    }
}