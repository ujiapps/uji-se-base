package es.uji.apps.se.dto;

import es.uji.apps.se.model.Equipacion;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SE_EQUIPACIONES")
public class EquipacionDTO {

    @Id
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "MODELO_ID")
    private EquipacionModeloDTO modeloDTO;

    @ManyToOne
    @JoinColumn(name = "GRUPO_ID")
    private EquipacionGruposDTO gruposDTO;

    @ManyToOne
    @JoinColumn(name = "GENERO_ID")
    private EquipacionGeneroDTO generoDTO;

    @ManyToOne
    @JoinColumn(name = "EQUIPACION_TIPO_ID")
    private EquipacionTipoDTO tipoDTO;

    @Column(name = "STOCK_MIN")
    private Long stockMin;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;


    public EquipacionDTO() { }

    public EquipacionDTO(Equipacion equipacion) {
        this.id = equipacion.getId();
        EquipacionModeloDTO modeloDTO = new EquipacionModeloDTO();
        modeloDTO.setId(equipacion.getModeloId());
        this.modeloDTO = modeloDTO;
        EquipacionGruposDTO gruposDTO = new EquipacionGruposDTO();
        gruposDTO.setId(equipacion.getGrupoId());
        this.gruposDTO = gruposDTO;
        EquipacionGeneroDTO generoDTO = new EquipacionGeneroDTO();
        generoDTO.setId(equipacion.getGeneroId());
        this.generoDTO = generoDTO;
        EquipacionTipoDTO tipoDTO = new EquipacionTipoDTO();
        tipoDTO.setId(equipacion.getTipoId());
        this.tipoDTO = tipoDTO;
        this.stockMin = equipacion.getStockMin();
        this.fechaInicio = equipacion.getFechaInicio();
        this.fechaFin = equipacion.getFechaFin();
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public EquipacionModeloDTO getModeloDTO() { return modeloDTO; }
    public void setModeloDTO(EquipacionModeloDTO modeloDTO) { this.modeloDTO = modeloDTO; }

    public EquipacionGruposDTO getGruposDTO() { return gruposDTO; }
    public void setGruposDTO(EquipacionGruposDTO gruposDTO) { this.gruposDTO = gruposDTO; }

    public EquipacionGeneroDTO getGeneroDTO() { return generoDTO; }
    public void setGeneroDTO(EquipacionGeneroDTO generoDTO) { this.generoDTO = generoDTO; }

    public EquipacionTipoDTO getTipoDTO() { return tipoDTO; }
    public void setTipoDTO(EquipacionTipoDTO tipoDTO) { this.tipoDTO = tipoDTO; }

    public Long getStockMin() { return stockMin; }
    public void setStockMin(Long stockMin) { this.stockMin = stockMin; }

    public Date getFechaInicio() { return fechaInicio; }
    public void setFechaInicio(Date fechaInicio) { this.fechaInicio = fechaInicio; }

    public Date getFechaFin() { return fechaFin; }
    public void setFechaFin(Date fechaFin) { this.fechaFin = fechaFin; }

}
