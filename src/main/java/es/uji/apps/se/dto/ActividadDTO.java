package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_ACTIVIDADES")
public class ActividadDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String nombre;

    @Column(name="DESCRIPCION")
    private String descripcion;

    @Column(name="ACTIVA")
    private Boolean activa;

    @Column(name="PLAZAS")
    private Long plazas;

    @Column(name="PERMITE_DUPLICIDAD_INS")
    private Boolean permiteDuplicidadInscripcion;

    @Column(name="EDAD_MINIMA")
    private Long edadMinimaInscripcion;

    @Column(name="ETIQUETA")
    private String etiquetaInscripcionExterna;

    @Column(name="COMENTARIO_AGENDA")
    private String comentarioAgenda;

    @Column(name="MOSTRAR_COMENTARIO_ASISTENCIAS")
    private Boolean mostrarComentarioAsistencias;

    @ManyToOne
    @JoinColumn(name = "TIPO_TARIFA_ID")
    private TipoDTO tipoTarifa;

    @ManyToOne
    @JoinColumn(name = "TIPO_RECIBO_ID")
    private TipoReciboVWDTO tipoRecibo;

    @OneToMany(mappedBy = "actividadDTO")
    private Set<OfertaDTO> ofertasDTO;

    @OneToMany(mappedBy = "actividadDTO")
    private Set<ActividadTipoDTO> actividadTiposDTO;

    public ActividadDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getActiva() {
        return activa;
    }

    public Boolean isActiva() {
        return activa;
    }

    public void setActiva(Boolean activa) {
        this.activa = activa;
    }

    public Long getPlazas() {
        return plazas;
    }

    public void setPlazas(Long plazas) {
        this.plazas = plazas;
    }

    public Boolean getPermiteDuplicidadInscripcion() {
        return permiteDuplicidadInscripcion;
    }

    public void setPermiteDuplicidadInscripcion(Boolean permiteDuplicidadInscripcion) {
        this.permiteDuplicidadInscripcion = permiteDuplicidadInscripcion;
    }

    public Boolean isPermiteDuplicidadInscripcion() {
        return permiteDuplicidadInscripcion;
    }

    public Long getEdadMinimaInscripcion() {
        return edadMinimaInscripcion;
    }

    public void setEdadMinimaInscripcion(Long edadMinimaInscripcion) {
        this.edadMinimaInscripcion = edadMinimaInscripcion;
    }

    public String getEtiquetaInscripcionExterna() {
        return etiquetaInscripcionExterna;
    }

    public void setEtiquetaInscripcionExterna(String etiquetaInscripcionExterna) {
        this.etiquetaInscripcionExterna = etiquetaInscripcionExterna;
    }

    public String getComentarioAgenda() {
        return comentarioAgenda;
    }

    public void setComentarioAgenda(String comentarioAgenda) {
        this.comentarioAgenda = comentarioAgenda;
    }

    public Boolean getMostrarComentarioAsistencias() {
        return mostrarComentarioAsistencias;
    }

    public void setMostrarComentarioAsistencias(Boolean mostrarComentarioAsistencias) {
        this.mostrarComentarioAsistencias = mostrarComentarioAsistencias;
    }

    public Boolean isMostrarComentarioAsistencias() {
        return mostrarComentarioAsistencias;
    }

    public TipoReciboVWDTO getTipoRecibo() {
        return tipoRecibo;
    }

    public void setTipoRecibo(TipoReciboVWDTO tipoRecibo) {
        this.tipoRecibo = tipoRecibo;
    }

    public Set<OfertaDTO> getOfertas() {
        return ofertasDTO;
    }

    public void setOfertas(Set<OfertaDTO> ofertasDTO) {
        this.ofertasDTO = ofertasDTO;
    }

    public TipoDTO getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(TipoDTO tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public Set<ActividadTipoDTO> getActividadTipos() {
        return actividadTiposDTO;
    }

    public void setActividadTipos(Set<ActividadTipoDTO> actividadTiposDTO) {
        this.actividadTiposDTO = actividadTiposDTO;
    }
}