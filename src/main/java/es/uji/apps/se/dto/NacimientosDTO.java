package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PER_NACIMIENTOS")
public class NacimientosDTO implements Serializable {

    @Id
    @Column(name = "PER_ID")
    private Long perId;

    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "PAI_ID")
    private String paiId;

    @Column(name = "PUE_ID")
    private Long pueId;

    @Column(name = "PUE_PRO_ID")
    private String pueProId;

    @Column(name = "SEXO")
    private Long sexo;

    public NacimientosDTO() {
    }

    public NacimientosDTO(Long perId, Date fecha, Long sexo) {
        this.perId = perId;
        this.fecha = fecha;
        this.sexo = sexo;
    }

    public NacimientosDTO(Long perId, Date fecha, String paiId, Long pueId, String pueProId, Long sexo) {
        this.perId = perId;
        this.fecha = fecha;
        this.paiId = paiId;
        this.pueId = pueId;
        this.pueProId = pueProId;
        this.sexo = sexo;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }

    public String getPaiId() {
        return paiId;
    }

    public void setPaiId(String paiId) {
        this.paiId = paiId;
    }

    public Long getPueId() {
        return pueId;
    }

    public void setPueId(Long pueId) {
        this.pueId = pueId;
    }

    public String getPueProId() {
        return pueProId;
    }

    public void setPueProId(String pueProId) {
        this.pueProId = pueProId;
    }
}
