package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_PERFILES_PERSONAS")
public class PerfilPersonaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERFIL_ID")
    private PerfilDTO perfilDTO;

    @Column(name = "PERSONA_ID")
    private Long persona;

    public PerfilPersonaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PerfilDTO getPerfilDTO() {
        return perfilDTO;
    }

    public void setPerfilDTO(PerfilDTO perfilDTO) {
        this.perfilDTO = perfilDTO;
    }

    public Long getPersona() {
        return persona;
    }

    public void setPersona(Long persona) {
        this.persona = persona;
    }
}