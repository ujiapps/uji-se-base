package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_TIPOS_GRUPOS")
public class GrupoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="PROCEDIMIENTO")
    private String procedimiento;

    @Column(name="AUTO")
    private Long auto;

    @OneToMany(mappedBy = "grupoDTO")
    private Set<TipoDTO> tiposDTO;

    public GrupoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProcedimiento() {
        return procedimiento;
    }

    public void setProcedimiento(String procedimiento) {
        this.procedimiento = procedimiento;
    }

    public Long getAuto() {
        return auto;
    }

    public void setAuto(Long auto) {
        this.auto = auto;
    }

    public Set<TipoDTO> getTipos() {
        return tiposDTO;
    }

    public void setTipos(Set<TipoDTO> tiposDTO) {
        this.tiposDTO = tiposDTO;
    }
}