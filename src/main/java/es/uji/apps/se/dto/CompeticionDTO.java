package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_COMPETICIONES")
public class CompeticionDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name="OFERTA_ID")
    private OfertaDTO oferta;

    @OneToMany(mappedBy = "competicion")
    private Set<CompeticionEquipoDTO> competicionEquipos;

    public CompeticionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OfertaDTO getOferta() {
        return oferta;
    }

    public void setOferta(OfertaDTO oferta) {
        this.oferta = oferta;
    }

    public Set<CompeticionEquipoDTO> getCompeticionEquipos() {
        return competicionEquipos;
    }

    public void setCompeticionEquipos(Set<CompeticionEquipoDTO> competicionEquipos) {
        this.competicionEquipos = competicionEquipos;
    }
}