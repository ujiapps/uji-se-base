package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "REC_TIPOS_RECIBO")
public class TipoReciboDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "DIAS_PLAZO_RECIBO")
    private Long diasPlazoRecibo;

    @Column(name = "RECLAMACIONES_AUTO")
    private Long reclamacionesAuto;

    public TipoReciboDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getDiasPlazoRecibo() {return diasPlazoRecibo;}
    public void setDiasPlazoRecibo(Long diasPlazoRecibo) {this.diasPlazoRecibo = diasPlazoRecibo;}

    public Long getReclamacionesAuto() {return reclamacionesAuto;}
    public void setReclamacionesAuto(Long reclamacionesAuto) {this.reclamacionesAuto = reclamacionesAuto;}
}
