package es.uji.apps.se.dto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SE_VW_EQUIPACIONES_MOV")
public class EquipacionesMovimientoVWDTO {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "EQUIPACION_ID")
    private Long equipacionId;

    @Column(name = "TALLA")
    private String talla;

    @Column(name = "DORSAL")
    private Long dorsal;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "CANTIDAD")
    private Long cantidad;

    @Column(name = "CONTABILIZA")
    private Long contabiliza;

    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "MOVIMIENTO")
    private String movimiento;

    @Column(name = "MOVIMIENTO_ID")
    private Long movimientoId;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "EQUIPACION_INSCRIPCION_ID")
    private Long equipacionInscripcionId;

    public EquipacionesMovimientoVWDTO() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getEquipacionId() { return equipacionId; }
    public void setEquipacionId(Long equipacionId) { this.equipacionId = equipacionId; }

    public String getTalla() { return talla; }
    public void setTalla(String talla) { this.talla = talla; }

    public Long getDorsal() { return dorsal; }
    public void setDorsal(Long dorsal) { this.dorsal = dorsal; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public Long getCantidad() { return cantidad; }
    public void setCantidad(Long cantidad) { this.cantidad = cantidad; }

    public Long getContabiliza() { return contabiliza; }
    public void setContabiliza(Long contabiliza) { this.contabiliza = contabiliza; }

    public Date getFecha() { return fecha; }
    public void setFecha(Date fecha) { this.fecha = fecha; }

    public String getMovimiento() { return movimiento; }
    public void setMovimiento(String movimiento) { this.movimiento = movimiento; }

    public Long getMovimientoId() { return movimientoId; }
    public void setMovimientoId(Long movimientoId) { this.movimientoId = movimientoId; }

    public String getObservaciones() { return observaciones; }
    public void setObservaciones(String observaciones) { this.observaciones = observaciones; }

    public Long getPersonaId() { return personaId; }
    public void setPersonaId(Long personaId) { this.personaId = personaId; }

    public String getIdentificacion() { return identificacion; }
    public void setIdentificacion(String identificacion) { this.identificacion = identificacion; }

    public Long getEquipacionInscripcionId() { return equipacionInscripcionId; }
    public void setEquipacionInscripcionId(Long equipacionInscripcionId) { this.equipacionInscripcionId = equipacionInscripcionId; }
}