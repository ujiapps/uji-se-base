package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PER_PERSONAS_SUBVINCULOS")
public class PersonasSubVinculosDTO implements Serializable {
    @EmbeddedId
    private PersonasSubVinculoDTOPK id;

    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;

    public PersonasSubVinculosDTO() {
    }

    public PersonasSubVinculosDTO(PersonasSubVinculoDTOPK id) {
        this.id = id;
    }

    public PersonasSubVinculoDTOPK getId() {
        return id;
    }

    public void setId(PersonasSubVinculoDTOPK id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}
}
