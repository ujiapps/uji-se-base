package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(schema = "GRI_PER", name = "PER_RGPD_ACEP_TRAT")
public class RgpdDTO implements Serializable {

    @Id
    @Column(name = "PER_ID")
    private Long id;

    @Column(name = "CURSO_ACA")
    private Long curso;

    @Column(name = "COD_TRATAMIENTO")
    private String codTratamiento;

    public RgpdDTO(Long id, Long curso) {
    }

    public RgpdDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCurso() {
        return curso;
    }

    public void setCurso(Long curso) {
        this.curso = curso;
    }

    public String getCodTratamiento() {
        return codTratamiento;
    }

    public void setCodTratamiento(String codTratamiento) {
        this.codTratamiento = codTratamiento;
    }
}
