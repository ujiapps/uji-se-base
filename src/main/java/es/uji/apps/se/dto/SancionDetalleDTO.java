package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_SANCIONES_DETALLE")
public class SancionDetalleDTO implements Serializable {
    @Id
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    @Column(name = "ID")
    private Long id;

    @Column(name = "SANCION_ID")
    private Long sancionId;

    @Column(name = "REFERENCIA")
    private Long referencia;

    @Column(name = "MOTIVO")
    private String motivo;

    public SancionDetalleDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getSancionId() {return sancionId;}
    public void setSancionId(Long sancionId) {this.sancionId = sancionId;}

    public Long getReferencia() {return referencia;}
    public void setReferencia(Long referencia) {this.referencia = referencia;}

    public String getMotivo() {return motivo;}
    public void setMotivo(String motivo) {this.motivo = motivo;}
}
