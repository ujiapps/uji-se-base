package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_SANCIONES")
public class SancionVistaDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Column(name = "MOTIVO")
    private String motivo;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "ORIGEN")
    private String origen;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademico;

    @Column(name = "MODIFICABLE")
    private Long modificable;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    @ManyToOne
    @JoinColumn(name = "NIVEL_ID")
    private TipoDTO nivel;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public String getMotivo()
    {
        return motivo;
    }

    public void setMotivo(String motivo)
    {
        this.motivo = motivo;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getCursoAcademico()
    {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }


    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public TipoDTO getNivel()
    {
        return nivel;
    }

    public void setNivel(TipoDTO nivel)
    {
        this.nivel = nivel;
    }

    public Long getModificable() {return modificable;}
    public void setModificable(Long modificable) {this.modificable = modificable;}

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
}