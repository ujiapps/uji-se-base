package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_DEPORTISTAS_ELITE")
public class EliteDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private CursoAcademicoDTO cursoAcademicoDTO;

    @ManyToOne
    @JoinColumn(name="CRITERIO_ADMISION_ID")
    private CriterioAdmisionDTO criterioAdmisionDTO;

    @ManyToOne
    @JoinColumn(name="MODALIDAD_DEPORTIVA_ID")
    private ModalidadDeportivaDTO modalidadDeportivaDTO;

    @OneToMany(mappedBy = "eliteDTO")
    private Set<EliteAvisoDTO> eliteAvisosDTO;

    @OneToMany(mappedBy = "eliteDTO")
    private Set<AlumnoAsignaturaDTO> alumnosAsignatura;

    public EliteDTO() {
    }

    public EliteDTO(Long personaId, Long cursoAcademico) {
        this.personaUjiDTO = new PersonaUjiDTO(personaId);
        this.cursoAcademicoDTO = new CursoAcademicoDTO(cursoAcademico);
    }

    public EliteDTO(Long personaId, Long cursoAcademico, Long modalidadId) {
        this.personaUjiDTO = new PersonaUjiDTO(personaId);
        this.cursoAcademicoDTO = new CursoAcademicoDTO(cursoAcademico);
        ModalidadDeportivaDTO deportivaDTO = new ModalidadDeportivaDTO();
        deportivaDTO.setId(modalidadId);
        this.modalidadDeportivaDTO = deportivaDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public CursoAcademicoDTO getCursoAcademico() {
        return cursoAcademicoDTO;
    }

    public void setCursoAcademico(CursoAcademicoDTO cursoAcademicoDTO) {
        this.cursoAcademicoDTO = cursoAcademicoDTO;
    }

    public CriterioAdmisionDTO getCriterioAdmision() {
        return criterioAdmisionDTO;
    }

    public void setCriterioAdmision(CriterioAdmisionDTO criterioAdmisionDTO) {
        this.criterioAdmisionDTO = criterioAdmisionDTO;
    }

    public ModalidadDeportivaDTO getModalidadDeportiva() {
        return modalidadDeportivaDTO;
    }

    public void setModalidadDeportiva(ModalidadDeportivaDTO modalidadDeportivaDTO) {
        this.modalidadDeportivaDTO = modalidadDeportivaDTO;
    }

    public Set<EliteAvisoDTO> getEliteAvisos() {
        return eliteAvisosDTO;
    }

    public void setEliteAvisos(Set<EliteAvisoDTO> eliteAvisosDTO) {
        this.eliteAvisosDTO = eliteAvisosDTO;
    }

    public Set<AlumnoAsignaturaDTO> getAlumnosAsignatura() {
        return alumnosAsignatura;
    }

    public void setAlumnosAsignatura(Set<AlumnoAsignaturaDTO> alumnosAsignatura) {
        this.alumnosAsignatura = alumnosAsignatura;
    }
}