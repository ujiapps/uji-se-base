package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "se_vw_personas_iban")
public class PersonaIbanDTO implements Serializable {

    @Id
    @Column(name="persona_id")
    private Long persona;

    @Id
    private String iban;

    public PersonaIbanDTO() {
    }

    public Long getPersona() {
        return persona;
    }

    public void setPersona(Long persona) {
        this.persona = persona;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonaIbanDTO that = (PersonaIbanDTO) o;
        return Objects.equals(persona, that.persona) && Objects.equals(iban, that.iban);
    }

    @Override
    public int hashCode() {
        return Objects.hash(persona, iban);
    }
}