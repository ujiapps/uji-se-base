package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_ENVIOS_GENERICOS")
public class EnvioGenericoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="TIPO_ENVIO")
    private String tipoEnvio;

    @Column(name="ASUNTO")
    private String asunto;

    @Lob
    @Column(name="CUERPO")
    private String cuerpo;

    @ManyToOne
    @JoinColumn(name = "PERSONA_SE_ID")
    private PersonaUjiDTO personaUjiDTO;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }
}
