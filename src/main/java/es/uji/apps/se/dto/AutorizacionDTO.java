package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_SCU_AUTORIZACIONES")
public class AutorizacionDTO implements Serializable {
    @Column(name = "AUT_ID")
    private Long id;

    @Column(name = "PER_ID")
    private Long personaId;

    @Id
    @Column(name = "ZOE_ID")
    private Long zonaId;

    @Column(name = "ZONA_NOMBRE")
    private String zonaNombre;

    @Temporal(TemporalType.DATE)
    @Column(name="H_ENTRADA")
    private Date horaEntrada;

    @Temporal(TemporalType.DATE)
    @Column(name="H_SALIDA")
    private Date horaSalida;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="H_INI_RESERVA")
    private Date horaInicioReserva;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="H_FIN_RESERVA")
    private Date horaFinReserva;

    @Column(name="CODIGO_ACTIVIDAD")
    private String actividadId;

    @Column(name="ACTIVIDAD")
    private String actividadNombre;

    @Column(name="ID_ORIGEN")
    private Long origenId;

    @Column(name="ORIGEN")
    private String origenNombre;

    public AutorizacionDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getZonaId() {return zonaId;}
    public void setZonaId(Long zonaId) {this.zonaId = zonaId;}

    public String getZonaNombre() {return zonaNombre;}
    public void setZonaNombre(String zonaNombre) {this.zonaNombre = zonaNombre;}

    public Date getHoraEntrada() {return horaEntrada;}
    public void setHoraEntrada(Date horaEntrada) {this.horaEntrada = horaEntrada;}

    public Date getHoraSalida() {return horaSalida;}
    public void setHoraSalida(Date horaSalida) {this.horaSalida = horaSalida;}

    public Date getHoraInicioReserva() {return horaInicioReserva;}
    public void setHoraInicioReserva(Date horaInicioReserva) {this.horaInicioReserva = horaInicioReserva;}

    public Date getHoraFinReserva() {return horaFinReserva;}
    public void setHoraFinReserva(Date horaFinReserva) {this.horaFinReserva = horaFinReserva;}

    public String getActividadId() {return actividadId;}
    public void setActividadId(String actividadId) {this.actividadId = actividadId;}

    public String getActividadNombre() {return actividadNombre;}
    public void setActividadNombre(String actividadNombre) {this.actividadNombre = actividadNombre;}

    public Long getOrigenId() {return origenId;}
    public void setOrigenId(Long origenId) {this.origenId = origenId;}

    public String getOrigenNombre() {return origenNombre;}
    public void setOrigenNombre(String origenNombre) {this.origenNombre = origenNombre;}
}