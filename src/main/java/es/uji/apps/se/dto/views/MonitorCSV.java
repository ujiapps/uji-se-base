package es.uji.apps.se.dto.views;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_MONITORES_CLASES_CSV")
public class MonitorCSV implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CLASE")
    private String clase;

    @Column(name = "CLASE_ID")
    private Long claseId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DIA")
    private Date dia;

    @Column(name = "HORA_INICIO")
    private String horaInicio;

    @Column(name = "INSTALACION")
    private String instalacion;

    @Column(name = "INSTALACION_ID")
    private Long instalacionId;

    @Column(name = "APELLIDOS_NOMBRE")
    private String apellidosNombre;

    @Column(name="TIPO_ENTRADA")
    private String tipoEntrada;

    @Column(name="PLAZAS")
    private Long plazas;

    @Column(name="RESERVAS")
    private Long reservas;

    @Column(name="ASISTENCIAS")
    private Long asistencias;

    public MonitorCSV() {
    }

    public MonitorCSV(Long personaId) {
        this.id = personaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    public Long getClaseId() {
        return claseId;
    }

    public void setClaseId(Long claseId) {
        this.claseId = claseId;
    }

    public Long getInstalacionId() {
        return instalacionId;
    }

    public void setInstalacionId(Long instalacionId) {
        this.instalacionId = instalacionId;
    }

    public String getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public Long getPlazas() {
        return plazas;
    }

    public Long getReservas() {
        return reservas;
    }

    public Long getAsistencias() {
        return asistencias;
    }
}