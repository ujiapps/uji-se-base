package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "REC_TIPOS_MOVIMIENTOS")
public class TipoMovimientoReciboDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "TIPO_CONCILIA_BANCO")
    private String tipoConciliaBanco;

    @Column(name = "SIGNO")
    private String signo;

    @Column(name = "TIPO_COBRO_ID")
    private Long tipoCobroId;

    @Column(name = "TIPO_TRAMITE")
    private String tipoTramite;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    public TipoMovimientoReciboDTO() { }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getTipoConciliaBanco() {return tipoConciliaBanco;}
    public void setTipoConciliaBanco(String tipoConciliaBanco) {this.tipoConciliaBanco = tipoConciliaBanco;}

    public String getSigno() {return signo;}
    public void setSigno(String signo) {this.signo = signo;}

    public Long getTipoCobroId() {return tipoCobroId;}
    public void setTipoCobroId(Long tipoCobroId) {this.tipoCobroId = tipoCobroId;}

    public String getTipoTramite() {return tipoTramite;}
    public void setTipoTramite(String tipoTramite) {this.tipoTramite = tipoTramite;}

    public String getNombreES() {return nombreES;}
    public void setNombreES(String nombreES) {this.nombreES = nombreES;}
}
