package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_ACTIVIDADES_APTOS")
public class AptoActividadDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="PERIODO_ID")
    private Long periodoId;

    @Column(name="ACTIVIDAD_ID")
    private Long actividadId;

    @Column(name="ACTIVIDAD_ID_ACCESO")
    private Long accesoActividadId;

    @Column(name="NECESITA_APTO")
    private Long necesitaApto;

    public AptoActividadDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPeriodoId() {return periodoId;}
    public void setPeriodoId(Long periodoId) {this.periodoId = periodoId;}

    public Long getActividadId() {return actividadId;}
    public void setActividadId(Long actividadId) {this.actividadId = actividadId;}

    public Long getAccesoActividadId() {return accesoActividadId;}
    public void setAccesoActividadId(Long accesoActividadId) {this.accesoActividadId = accesoActividadId;}

    public Long getNecesitaApto() {return necesitaApto;}
    public void setNecesitaApto(Long necesitaApto) {this.necesitaApto = necesitaApto;}
}
