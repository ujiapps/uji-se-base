package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_ELITE_AVISOS_PROFESORES")
public class ProfesorEliteAvisoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="CURSO_ACA")
    private Long cursoAcademico;

    @Column(name="PERSONA_REALIZA_ID")
    private Long personaId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_SOLICITA_ENVIO")
    private Date fechaSolicitaEnvio;

    @Column(name="ENVIAR_PRUEBA")
    private Boolean enviarPrueba;

    @Column(name="PROFESOR_ID")
    private Long profesor;

    public ProfesorEliteAvisoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Date getFechaSolicitaEnvio() {
        return fechaSolicitaEnvio;
    }

    public void setFechaSolicitaEnvio(Date fechaSolicitaEnvio) {
        this.fechaSolicitaEnvio = fechaSolicitaEnvio;
    }

    public Boolean getEnviarPrueba() {
        return enviarPrueba;
    }

    public void setEnviarPrueba(Boolean enviarPrueba) {
        this.enviarPrueba = enviarPrueba;
    }

    public Long getProfesor() {
        return profesor;
    }

    public void setProfesor(Long profesor) {
        this.profesor = profesor;
    }
}