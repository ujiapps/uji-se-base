package es.uji.apps.se.dto;

import es.uji.apps.se.exceptions.ValidacionException;
import es.uji.apps.se.services.DateExtensions;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_MONITORES_MUSCULACION_FICHA")
public class MonitorMusculacionFichajeDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="FECHA")
    private Date fecha;

    @Column(name="HORA_ENTRADA")
    private String horaEntrada;

    @Column(name="HORA_SALIDA")
    private String horaSalida;

    @ManyToOne
    @JoinColumn(name="MONITOR_MUSCULACION_ID")
    private MonitorMusculacionDTO monitorMusculacionDTO;

    public MonitorMusculacionFichajeDTO() {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getHoraEntrada()
    {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada)
    {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida()
    {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida)
    {
        this.horaSalida = horaSalida;
    }

    public MonitorMusculacionDTO getMonitorMusculacion()
    {
        return monitorMusculacionDTO;
    }

    public void setMonitorMusculacion(MonitorMusculacionDTO monitorMusculacionDTO)
    {
        this.monitorMusculacionDTO = monitorMusculacionDTO;
    }

    public void checkIsValid() throws ValidacionException
    {
        Date fechaHoraEntrada = DateExtensions.getFechaConHora(fecha, horaEntrada);
        Date fechaHoraSalida = DateExtensions.getFechaConHora(fecha, horaSalida);

        if (fechaHoraEntrada != null && fechaHoraSalida != null && fechaHoraEntrada.after(fechaHoraSalida))
        {
            throw new ValidacionException("L'hora de sortida ha de ser posterior a la d'entrada");
        }
    }
}
