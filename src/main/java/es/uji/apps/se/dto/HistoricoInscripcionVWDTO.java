package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_HISTORICO_INSCRIPCIONES")
public class HistoricoInscripcionVWDTO implements Serializable {

    @Column(name = "ACTIVIDAD_NOMBRE")
    private String actividadNombre;

    @Column(name = "PERIODO")
    private String periodo;

    @Column(name = "CURSO_ACA")
    private Long curso;

    @Column(name = "APTO")
    private Long apto;

    @Column(name = "OPERACION")
    private String operacion;

    @Column(name = "DIA_INSCRIPCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date diaInscripcion;

    @Id
    @Column(name = "FECHA_ACCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAccion;

    @Column(name = "FECHA_BAJA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "GRUPO")
    private String grupo;

    @Column(name = "ESTADO_ID")
    private Long estadoId;

    @Column(name = "ESTADO_NOMBRE")
    private String estadoNombre;

    @Column(name = "NUMERO_VECES")
    private Long numeroVeces;

    @Column(name = "ORIGEN")
    private String origen;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "USUARIO_ACCION")
    private String usuarioAccion;

    public HistoricoInscripcionVWDTO() {}

    public String getActividadNombre() {return actividadNombre;}
    public void setActividadNombre(String actividadNombre) {this.actividadNombre = actividadNombre;}

    public String getPeriodo() {return periodo;}
    public void setPeriodo(String periodo) {this.periodo = periodo;}

    public Long getCurso() {return curso;}
    public void setCurso(Long curso) {this.curso = curso;}

    public Long getApto() {return apto;}
    public void setApto(Long apto) {this.apto = apto;}

    public String getOperacion() {return operacion;}
    public void setOperacion(String operacion) {this.operacion = operacion;}

    public Date getDiaInscripcion() {return diaInscripcion;}
    public void setDiaInscripcion(Date diaInscripcion) {this.diaInscripcion = diaInscripcion;}

    public Date getFechaAccion() {return fechaAccion;}
    public void setFechaAccion(Date fechaAccion) {this.fechaAccion = fechaAccion;}

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public String getGrupo() {return grupo;}
    public void setGrupo(String grupo) {this.grupo = grupo;}

    public Long getEstadoId() {return estadoId;}
    public void setEstadoId(Long estadoId) {this.estadoId = estadoId;}

    public String getEstadoNombre() {return estadoNombre;}
    public void setEstadoNombre(String estadoNombre) {this.estadoNombre = estadoNombre;}

    public Long getNumeroVeces() {return numeroVeces;}
    public void setNumeroVeces(Long numeroVeces) {this.numeroVeces = numeroVeces;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observaciones) {this.observaciones = observaciones;}

    public String getUsuarioAccion() {return usuarioAccion;}
    public void setUsuarioAccion(String usuarioAccion) {this.usuarioAccion = usuarioAccion;}
}
