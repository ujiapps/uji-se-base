package es.uji.apps.se.dto.views;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_ACTIVIDADES_ASISTENCIA")
public class VWActividadesAsistenciaDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "HORA_INI")
    private String horaIni;

    @Column(name = "HORA_FIN")
    private String horaFin;

    @Column(name = "DIA")
    @Temporal(TemporalType.DATE)
    private Date dia;

    @Column(name = "ASISTO")
    private String asisto;

    @Column(name = "ASISTENCIA_ID")
    private Long asistenciaId;

    public VWActividadesAsistenciaDTO() {
    }

    public VWActividadesAsistenciaDTO(Long id, String horaIni, String horaFin, Date dia, String asisto, Long asistenciaId) {
        this.id = id;
        this.horaIni = horaIni;
        this.horaFin = horaFin;
        this.dia = dia;
        this.asisto = asisto;
        this.asistenciaId = asistenciaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(String horaIni) {
        this.horaIni = horaIni;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getAsisto() {
        return asisto;
    }

    public void setAsisto(String asisto) {
        this.asisto = asisto;
    }

    public Long getAsistenciaId() {
        return asistenciaId;
    }

    public void setAsistenciaId(Long asistenciaId) {
        this.asistenciaId = asistenciaId;
    }
}
