package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_PARAMETROS")
public class ParametroDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="VALOR")
    private String valor;

    @ManyToOne
    @JoinColumn(name="PERSONA_ID")
    private PersonaUjiDTO persona;

    public ParametroDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public PersonaUjiDTO getPersona() {
        return persona;
    }

    public void setPersona(PersonaUjiDTO persona) {
        this.persona = persona;
    }


}