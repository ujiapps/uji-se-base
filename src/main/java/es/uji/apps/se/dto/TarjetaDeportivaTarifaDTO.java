package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity @Table(name = "SE_TARJETAS_DEPORTIVAS_TARIFAS")
public class TarjetaDeportivaTarifaDTO implements Serializable
{
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "VINCULO_ID")
    private TipoDTO vinculo;

    @Column(name = "IMPORTE")
    private Float importe;

    @ManyToOne
    @JoinColumn(name = "TARJETA_DEPORTIVA_TIPO_ID")
    private TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO;

    public TarjetaDeportivaTarifaDTO()
    {
    }

    public TarjetaDeportivaTarifaDTO(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public TipoDTO getVinculo()
    {
        return vinculo;
    }

    public void setVinculo(TipoDTO vinculo)
    {
        this.vinculo = vinculo;
    }

    public Float getImporte()
    {
        return importe;
    }

    public void setImporte(Float importe)
    {
        this.importe = importe;
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportivaTipo()
    {
        return tarjetaDeportivaTipoDTO;
    }

    public void setTarjetaDeportivaTipo(TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO)
    {
        this.tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoDTO;
    }
}