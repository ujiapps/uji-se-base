package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_MONITORES_CURSOS_ACA")
public class MonitorCursoAcademicoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="COMPUTA_CURSO")
    private Long computaCurso;

    @Column(name="FECHA_ALTA")
    private Date fechaAlta;

    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @ManyToOne
    @JoinColumn(name = "MONITOR_ID")
    private MonitorDTO monitorDTO;

    @ManyToOne
    @JoinColumn(name = "CURSO_ACA")
    private CursoAcademicoDTO cursoAcademicoDTO;

    @OneToMany(mappedBy = "monitorCursoAcademico")
    private Set<MonitorOfertaDTO> monitoresOferta;

    @OneToMany(mappedBy = "monitorCursoAcademico")
    private Set<MonitorClaseCalendarioDTO> monitoresClaseCalendario;

    @OneToMany(mappedBy = "monitorCursoAcademico")
    private Set<MonitorMusculacionDTO> monitoresMusculacion;

    public MonitorCursoAcademicoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getComputaCurso() {
        return computaCurso;
    }

    public void setComputaCurso(Long computaCurso) {
        this.computaCurso = computaCurso;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public MonitorDTO getMonitor() {
        return monitorDTO;
    }

    public void setMonitor(MonitorDTO monitorDTO) {
        this.monitorDTO = monitorDTO;
    }

    public CursoAcademicoDTO getCursoAcademico() {
        return cursoAcademicoDTO;
    }

    public void setCursoAcademico(CursoAcademicoDTO cursoAcademicoDTO) {
        this.cursoAcademicoDTO = cursoAcademicoDTO;
    }

    public Set<MonitorOfertaDTO> getMonitoresOferta() {
        return monitoresOferta;
    }

    public void setMonitoresOferta(Set<MonitorOfertaDTO> monitoresOferta) {
        this.monitoresOferta = monitoresOferta;
    }

    public Set<MonitorClaseCalendarioDTO> getMonitoresClaseCalendario() {
        return monitoresClaseCalendario;
    }

    public void setMonitoresClaseCalendario(Set<MonitorClaseCalendarioDTO> monitoresClaseCalendario) {
        this.monitoresClaseCalendario = monitoresClaseCalendario;
    }

    public Set<MonitorMusculacionDTO> getMonitoresMusculacion() {
        return monitoresMusculacion;
    }

    public void setMonitoresMusculacion(Set<MonitorMusculacionDTO> monitoresMusculacion) {
        this.monitoresMusculacion = monitoresMusculacion;
    }
}