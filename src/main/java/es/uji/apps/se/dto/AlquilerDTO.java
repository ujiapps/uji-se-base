package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_ALQUILERES")
public class AlquilerDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String nombre;

    @Column(name="COMENTARIO_AGENDA")
    private String comentarioAgenda;

    @Column(name="CODIGO_LLAVERO")
    private String codigoLlavero;

    @Column(name="MAIL")
    private String mail;

    @Column(name="TELEFONO")
    private String telefono;

    @Column(name="CONTACTO")
    private String contacto;

    @OneToMany(mappedBy = "alquilerDTO")
    private Set<AlquilerOfertaDTO> ofertasDTO;

    public AlquilerDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getComentarioAgenda() {
        return comentarioAgenda;
    }

    public void setComentarioAgenda(String comentarioAgenda) {
        this.comentarioAgenda = comentarioAgenda;
    }

    public String getCodigoLlavero() {
        return codigoLlavero;
    }

    public void setCodigoLlavero(String codigoLlavero) {
        this.codigoLlavero = codigoLlavero;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Set<AlquilerOfertaDTO> getOfertasDTO() {
        return ofertasDTO;
    }

    public void setOfertasDTO(Set<AlquilerOfertaDTO> ofertasDTO) {
        this.ofertasDTO = ofertasDTO;
    }
}