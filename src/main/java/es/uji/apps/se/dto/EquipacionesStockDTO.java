package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SE_VW_EQUIPACIONES_STOCK")
public class EquipacionesStockDTO {

    @Id
    @Column(name = "EQUIPACION_ID")
    private Long equipacionId;

    @Column(name = "STOCK")
    private Long stock;

    @Column(name = "DORSAL")
    private Long dorsal;

    @Column(name = "TALLA")
    private String talla;

    public EquipacionesStockDTO() { }

    public Long getEquipacionId() { return equipacionId; }
    public void setEquipacionId(Long equipacionId) { this.equipacionId = equipacionId; }

    public Long getStock() { return stock; }
    public void setStock(Long stock) { this.stock = stock; }

    public Long getDorsal() { return dorsal; }
    public void setDorsal(Long dorsal) { this.dorsal = dorsal; }

    public String getTalla() { return talla; }
    public void setTalla(String talla) { this.talla = talla; }
}