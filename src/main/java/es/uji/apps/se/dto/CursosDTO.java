package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_EXT_CURSOS")
public class CursosDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "FECHA_INI")
    private Date fechaIni;
    @Column(name = "FECHA_FIN")
    private Date fechaFin;
    @Column(name = "ANULADO")
    private Long anulado;
    @Column(name = "VISIBLE")
    private Long visible;

    public CursosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Long getAnulado() {
        return anulado;
    }

    public void setAnulado(Long anulado) {
        this.anulado = anulado;
    }

    public Long getVisible() {
        return visible;
    }

    public void setVisible(Long visible) {
        this.visible = visible;
    }
}
