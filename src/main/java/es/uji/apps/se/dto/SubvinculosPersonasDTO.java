package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_EXT_SUBVINCULOS_PERSONAS")
public class SubvinculosPersonasDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private long id;
    @Column(name = "PERSONA_ID")
    private long perId;
    @Column(name = "SUBVINCULO_ID")
    private long subvinculoId;

    public SubvinculosPersonasDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    public long getSubvinculoId() {
        return subvinculoId;
    }

    public void setSubvinculoId(long subvinculoId) {
        this.subvinculoId = subvinculoId;
    }
}
