package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_IND_TARJETAS_USOS")
public class TarjetaUsosDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="TARJETA_DEPORTIVA_TIPO_ID")
    private Long tarjetaDeportivaTipo;

    @Column(name="TARJETA_DEPORTIVA_NOMBRE")
    private String tarjetaDeportivaNombre;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dia;

    @Column(name="hora_inicio")
    private String horaInicio;

    @Column(name="HORA_FIN")
    private String horaFin;

    private Long asiste;

    public TarjetaUsosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTarjetaDeportivaTipo() {
        return tarjetaDeportivaTipo;
    }

    public void setTarjetaDeportivaTipo(Long tarjetaDeportivaTipo) {
        this.tarjetaDeportivaTipo = tarjetaDeportivaTipo;
    }

    public String getTarjetaDeportivaNombre() {
        return tarjetaDeportivaNombre;
    }

    public void setTarjetaDeportivaNombre(String tarjetaDeportivaNombre) {
        this.tarjetaDeportivaNombre = tarjetaDeportivaNombre;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public Long getAsiste() {
        return asiste;
    }

    public void setAsiste(Long asiste) {
        this.asiste = asiste;
    }
}
