package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_DECANOS_ELITE")
public class DecanoEliteDTO implements Serializable {

    @Id
    private Long id;

    private String correo;

    private String cargo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_ENVIO")
    private Date fechaEnvio;

    @Column(name="APELLIDOS_NOMBRE")
    private String apellidosNombre;

    @Column(name="CURSO_ACA")
    private Long cursoAcademico;

    public DecanoEliteDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }
}
