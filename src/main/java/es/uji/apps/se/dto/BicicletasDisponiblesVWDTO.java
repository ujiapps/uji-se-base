package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_VW_BICICLETAS_DISPONIBLES")
public class BicicletasDisponiblesVWDTO implements Serializable {
    @Id
    @Column(name = "NUM_BICIS_DISPONIBLES")
    private Long numBicisDisponibles;

    public BicicletasDisponiblesVWDTO() {
    }

    public Long getNumBicisDisponibles() {
        return numBicisDisponibles;
    }

    public void setNumBicisDisponibles(Long numBicisDisponibles) {
        this.numBicisDisponibles = numBicisDisponibles;
    }
}
