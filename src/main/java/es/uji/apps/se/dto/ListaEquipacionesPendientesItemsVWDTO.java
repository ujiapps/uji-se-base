package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_EQUIPACIONES_MOROSOS_ITEMS")
public class ListaEquipacionesPendientesItemsVWDTO implements Serializable {

    @Id
    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "CURSO_ACA")
    private Long curso;

    @Column(name = "APELLIDOS_NOMBRE")
    private String nombreCompleto;

    @Column(name = "FECHA_DEV")
    @Temporal(TemporalType.DATE)
    private Date fechaDevolucion;

    @Column(name = "TELEFONO")
    private String telefono;

    @Column(name = "NOMBRE")
    private String nombreEquipacion;

    public ListaEquipacionesPendientesItemsVWDTO() {
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Long getCurso() {
        return curso;
    }

    public void setCurso(Long curso) {
        this.curso = curso;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Date getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreEquipacion() {
        return nombreEquipacion;
    }

    public void setNombreEquipacion(String nombreEquipacion) {
        this.nombreEquipacion = nombreEquipacion;
    }
}
