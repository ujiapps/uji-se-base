package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_INSCRIPCIONES")
public class InscripcionDTO implements Serializable {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    private String observaciones;
    private Boolean apto;
    @Column(name="OBSERVACIONES_APTO")
    private String observacionesApto;
    @Column(name="NUMERO_VECES")
    private Long numeroInscripcion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_RECOGIDA_CERTIFICADO")
    private Date fechaCertificado;

    @Column(name="SIN_COSTE")
    private Boolean gratuita;
    private String origen;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID")
    private OfertaDTO ofertaDTO;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    @ManyToOne
    @JoinColumn(name="ESTADO_ID")
    private TipoDTO estado;

    @ManyToOne
    @JoinColumn(name="PERSONA_ID_ADMIN")
    private PersonaUjiDTO inscritoAdmin;

    @ManyToOne
    @JoinColumn(name="PREINSCRIPCION_ID")
    private PreinscripcionDTO preinscripcionDTO;

    @OneToMany(mappedBy = "inscripcionDTO")
    private Set<IncidenciaDTO> incidenciasInscripcion;

    @OneToMany(mappedBy = "inscripcion")
    private Set<CompeticionMiembroDTO> competicionMiembros;

    @OneToMany(mappedBy = "inscripcion")
    private Set<ActividadAsistenciaDTO> actividadAsistencias;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Boolean getApto() {
        return apto;
    }

    public void setApto(Boolean apto) {
        this.apto = apto;
    }

    public String getObservacionesApto() {
        return observacionesApto;
    }

    public void setObservacionesApto(String observacionesApto) {
        this.observacionesApto = observacionesApto;
    }

    public Long getNumeroInscripcion() {
        return numeroInscripcion;
    }

    public void setNumeroInscripcion(Long numeroInscripcion) {
        this.numeroInscripcion = numeroInscripcion;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaCertificado() {
        return fechaCertificado;
    }

    public void setFechaCertificado(Date fechaCertificado) {
        this.fechaCertificado = fechaCertificado;
    }

    public Boolean getGratuita() {
        return gratuita;
    }

    public void setGratuita(Boolean gratuita) {
        this.gratuita = gratuita;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public OfertaDTO getOferta() {
        return ofertaDTO;
    }

    public void setOferta(OfertaDTO ofertaDTO) {
        this.ofertaDTO = ofertaDTO;
    }


    public TipoDTO getEstado() {
        return estado;
    }

    public void setEstado(TipoDTO estado) {
        this.estado = estado;
    }


    public PreinscripcionDTO getPreinscripcion() {
        return preinscripcionDTO;
    }

    public void setPreinscripcion(PreinscripcionDTO preinscripcionDTO) {
        this.preinscripcionDTO = preinscripcionDTO;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public PersonaUjiDTO getInscritoAdmin() {
        return inscritoAdmin;
    }

    public void setInscritoAdmin(PersonaUjiDTO inscritoAdmin) {
        this.inscritoAdmin = inscritoAdmin;
    }

    public Set<IncidenciaDTO> getIncidenciasInscripcion() {
        return incidenciasInscripcion;
    }

    public void setIncidenciasInscripcion(Set<IncidenciaDTO> incidenciasInscripcion) {
        this.incidenciasInscripcion = incidenciasInscripcion;
    }

    public Set<CompeticionMiembroDTO> getCompeticionMiembros() {
        return competicionMiembros;
    }

    public void setCompeticionMiembros(Set<CompeticionMiembroDTO> competicionMiembros) {
        this.competicionMiembros = competicionMiembros;
    }

    public Set<ActividadAsistenciaDTO> getActividadAsistencias() {
        return actividadAsistencias;
    }

    public void setActividadAsistencias(Set<ActividadAsistenciaDTO> actividadAsistencias) {
        this.actividadAsistencias = actividadAsistencias;
    }
}
