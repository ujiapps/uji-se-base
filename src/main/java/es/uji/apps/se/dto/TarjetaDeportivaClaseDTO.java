package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_TARJETA_DEPORTIVA_CLASES")
public class TarjetaDeportivaClaseDTO implements Serializable
{
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TARJETA_DEPORTIVA_ID")
    private TarjetaDeportivaDTO tarjetaDeportivaDTO;

    @ManyToOne
    @JoinColumn(name = "CALENDARIO_CLASE_DIRIGIDA_ID")
    private CalendarioClaseDTO calendarioClase;

    @Column(name = "ASISTE")
    private Boolean asiste;

    @Column(name="PERSONA_ID_ACCION")
    private Long personaAccion;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public TarjetaDeportivaDTO getTarjetaDeportiva()
    {
        return tarjetaDeportivaDTO;
    }

    public void setTarjetaDeportiva(TarjetaDeportivaDTO tarjetaDeportivaDTO)
    {
        this.tarjetaDeportivaDTO = tarjetaDeportivaDTO;
    }

    public CalendarioClaseDTO getCalendarioClase()
    {
        return calendarioClase;
    }

    public void setCalendarioClase(CalendarioClaseDTO calendarioClase)
    {
        this.calendarioClase = calendarioClase;
    }

    public Boolean isAsiste()
    {
        return asiste;
    }

    public void setAsiste(Boolean asiste)
    {
        this.asiste = asiste;
    }

    public Long getPersonaAccion() {
        return personaAccion;
    }

    public void setPersonaAccion(Long personaAccion) {
        this.personaAccion = personaAccion;
    }
}