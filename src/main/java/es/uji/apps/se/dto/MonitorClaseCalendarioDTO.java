package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_MONITORES_CLASE_CAL")
public class MonitorClaseCalendarioDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="TIPO_ENTRADA")
    private String tipoEntrada;

    @ManyToOne
    @JoinColumn(name = "CALENDARIO_CLASE_DIRIGIDA_ID")
    private CalendarioClaseDTO calendarioClaseDTO;

    @ManyToOne
    @JoinColumn(name = "MONITOR_CURSO_ACA_ID")
    private MonitorCursoAcademicoDTO monitorCursoAcademico;

    public MonitorClaseCalendarioDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public CalendarioClaseDTO getCalendarioClase() {
        return calendarioClaseDTO;
    }

    public void setCalendarioClase(CalendarioClaseDTO calendarioClaseDTO) {
        this.calendarioClaseDTO = calendarioClaseDTO;
    }

    public MonitorCursoAcademicoDTO getMonitorCursoAcademico() {
        return monitorCursoAcademico;
    }

    public void setMonitorCursoAcademico(MonitorCursoAcademicoDTO monitorCursoAcademico) {
        this.monitorCursoAcademico = monitorCursoAcademico;
    }

    public String getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }
}