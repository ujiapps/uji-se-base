package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "SE_VW_EQUIPACIONES_USUARIOS")
public class EquipacionesUsuarioDTO implements Serializable {
    @Column(name = "ACTIVIDAD_NOMBRE")
    private String actividadNombre;
    @Id
    @Column(name = "ACTIVIDAD_ID")
    private Long actividadId;
    @Id
    @Column(name = "INSCRIPCION_ID")
    private Long inscripcionId;
    @Column(name = "EQUIPACION_FECHA_FIN")
    private Date equipacionFechaFin;
    @Id
    @Column(name = "EQUIPACION_ID")
    private Long equipacionId;
    @Column(name = "EQUIPACION_NOMBRE")
    private String equipacionNombre;
    @Column(name = "EQUIPACION_GRUPO")
    private String equipacionGrupo;
    @Column(name = "EQUIPACION_DEV")
    private Long equipacionDev;
    @Column(name = "EQUIPACION_STOCK")
    private Long equipacionStock;
    @Id
    @Column(name = "CURSO_ACA")
    private Long cursoAca;
    @Id
    @Column(name = "PERSONA_ID")
    private Long personaId;
    @Column(name = "FECHA_MAX_DEV")
    private Date fechaMaxDev;
    @Column(name = "TIENE_VINCULADAS")
    private Long tieneVinculadas;
    @Column(name = "EQUIPACION_DORSAL")
    private String equipacionDorsal;
    @Column(name = "EQUIPACION_TALLA")
    private String equipacionTalla;
    @Column(name = "EQUIPACION_INSCRIPCION")
    private Long equipacionInscripcion;
    @Column(name = "FECHA_DEV")
    private Date fechaDev;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "TIPO_DEV")
    private Long tipoDev;
    @Column(name = "STOCK")
    private Long stock;

    public EquipacionesUsuarioDTO() {
    }

    public String getActividadNombre() {
        return actividadNombre;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public Long getInscripcionId() {
        return inscripcionId;
    }

    public void setInscripcionId(Long inscripcionId) {
        this.inscripcionId = inscripcionId;
    }

    public Date getEquipacionFechaFin() {
        return equipacionFechaFin;
    }

    public void setEquipacionFechaFin(Date equipacionFechaFin) {
        this.equipacionFechaFin = equipacionFechaFin;
    }

    public Long getEquipacionId() {
        return equipacionId;
    }

    public void setEquipacionId(Long equipacionId) {
        this.equipacionId = equipacionId;
    }

    public String getEquipacionNombre() {
        return equipacionNombre;
    }

    public void setEquipacionNombre(String equipacionNombre) {
        this.equipacionNombre = equipacionNombre;
    }

    public String getEquipacionGrupo() {
        return equipacionGrupo;
    }

    public void setEquipacionGrupo(String equipacionGrupo) {
        this.equipacionGrupo = equipacionGrupo;
    }

    public Long getEquipacionDev() {
        return equipacionDev;
    }

    public void setEquipacionDev(Long equipacionDev) {
        this.equipacionDev = equipacionDev;
    }

    public Long getEquipacionStock() {
        return equipacionStock;
    }

    public void setEquipacionStock(Long equipacionStock) {
        this.equipacionStock = equipacionStock;
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Date getFechaMaxDev() {
        return fechaMaxDev;
    }

    public void setFechaMaxDev(Date fechaMaxDev) {
        this.fechaMaxDev = fechaMaxDev;
    }

    public Long getTieneVinculadas() {
        return tieneVinculadas;
    }

    public void setTieneVinculadas(Long tieneVinculadas) {
        this.tieneVinculadas = tieneVinculadas;
    }

    public String getEquipacionDorsal() {
        return equipacionDorsal;
    }

    public void setEquipacionDorsal(String equipacionDorsal) {
        this.equipacionDorsal = equipacionDorsal;
    }

    public String getEquipacionTalla() {
        return equipacionTalla;
    }

    public void setEquipacionTalla(String equipacionTalla) {
        this.equipacionTalla = equipacionTalla;
    }

    public Long getEquipacionInscripcion() {
        return equipacionInscripcion;
    }

    public void setEquipacionInscripcion(Long equipacionInscripcion) {
        this.equipacionInscripcion = equipacionInscripcion;
    }

    public Date getFechaDev() {
        return fechaDev;
    }

    public void setFechaDev(Date fechaDev) {
        this.fechaDev = fechaDev;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getTipoDev() {
        return tipoDev;
    }

    public void setTipoDev(Long tipoDev) {
        this.tipoDev = tipoDev;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EquipacionesUsuarioDTO that = (EquipacionesUsuarioDTO) o;
        return Objects.equals(actividadId, that.actividadId) &&
               Objects.equals(inscripcionId, that.inscripcionId) &&
               Objects.equals(equipacionId, that.equipacionId) &&
               Objects.equals(cursoAca, that.cursoAca) &&
               Objects.equals(personaId, that.personaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actividadId, inscripcionId, equipacionId, cursoAca, personaId);
    }
}
