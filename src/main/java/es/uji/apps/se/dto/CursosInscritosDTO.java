package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SE_EXT_CURSOS_INSCRITOS")
public class CursosInscritosDTO implements Serializable {
    @Id
    @Column(name = "CURSO_ID")
    private Long cursoAca;
    @Id
    @Column(name = "PERSONA_ID")
    private Long perId;
    @Id
    @Column(name = "CONFIRMACION")
    private Long confirmacion;

    public CursosInscritosDTO() {
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(Long confirmacion) {
        this.confirmacion = confirmacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CursosInscritosDTO that = (CursosInscritosDTO) o;
        return Objects.equals(cursoAca, that.cursoAca) &&
               Objects.equals(perId, that.perId) &&
               Objects.equals(confirmacion, that.confirmacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cursoAca, perId, confirmacion);
    }
}
