package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "se_vw_ind_inscripciones")
public class UsoDeportesPersonaDTO implements Serializable {
    @Id
    @Column(name="INSCRIPCION_ID")
    private Long id;

    @Column(name="PERSONA_ID")
    private Long persona;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_ALTA")
    private Date fechaAlta;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    private String uso;

    @Column(name="VINCULO_NOMBRE")
    private String vinculoNombre;

    private Long sexo;

    public UsoDeportesPersonaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public Long getPersona() {
        return persona;
    }

    public void setPersona(Long persona) {
        this.persona = persona;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getVinculoNombre() {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre) {
        this.vinculoNombre = vinculoNombre;
    }

    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }
}
