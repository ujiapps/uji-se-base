package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_MONITORES_ACT")
public class MonitorOfertaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="PRINCIPAL")
    private Boolean principal;

    @Column(name="FECHA_ALTA")
    private Date fechaAlta;

    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @Column(name="SATISFACCION_USUARIO")
    private Long satisfaccionUsuario;

    @Column(name="ENCUESTAS")
    private Long encuentas;

    @ManyToOne
    @JoinColumn(name = "MONITOR_CURSO_ID")
    private MonitorCursoAcademicoDTO monitorCursoAcademico;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID")
    private OfertaDTO oferta;

    public MonitorOfertaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Long getSatisfaccionUsuario() {
        return satisfaccionUsuario;
    }

    public void setSatisfaccionUsuario(Long satisfaccionUsuario) {
        this.satisfaccionUsuario = satisfaccionUsuario;
    }

    public Long getEncuentas() {
        return encuentas;
    }

    public void setEncuentas(Long encuentas) {
        this.encuentas = encuentas;
    }

    public MonitorCursoAcademicoDTO getMonitorCursoAcademico() {
        return monitorCursoAcademico;
    }

    public void setMonitorCursoAcademico(MonitorCursoAcademicoDTO monitorCursoAcademico) {
        this.monitorCursoAcademico = monitorCursoAcademico;
    }

    public OfertaDTO getOferta() {
        return oferta;
    }

    public void setOferta(OfertaDTO oferta) {
        this.oferta = oferta;
    }


}