package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "PER_DOMICILIOS")
public class DomiciliosDTO implements Serializable {
    @Id
    @Column(name = "PER_ID")
    private Long perId;

    @Id
    @Column(name = "ORDEN_PREFERENCIA")
    private Long ordenPreferencia;

    @Column(name = "TDO_ID")
    private Long tdoId;

    @Column(name = "VIA_ID")
    private String viaId;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "NUMERO")
    private String numero;

    @Column(name = "ESCALERA")
    private String escalera;

    @Column(name = "PISO")
    private String piso;

    @Column(name = "PUERTA")
    private String puerta;

    @Column(name = "CODPOSTAL")
    private String codPostal;

    @Column(name = "PUE_PRO_ID")
    private String provinciaId;

    @Column(name = "PUE_ID")
    private Long puebloId;

    @Column(name = "PAI_ID")
    private String paisId;

    public DomiciliosDTO() {
    }

    public DomiciliosDTO(Long perId, Long ordenPreferencia, Long tdoId, String viaId, String nombre, String numero, String escalera, String piso, String puerta, String codPostal, String provinciaId, Long puebloId, String paisId) {
        this.perId = perId;
        this.ordenPreferencia = ordenPreferencia;
        this.tdoId = tdoId;
        this.viaId = viaId;
        this.nombre = nombre;
        this.numero = numero;
        this.escalera = escalera;
        this.piso = piso;
        this.puerta = puerta;
        this.codPostal = codPostal;
        this.provinciaId = provinciaId;
        this.puebloId = puebloId;
        this.paisId = paisId;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getOrdenPreferencia() {
        return ordenPreferencia;
    }

    public void setOrdenPreferencia(Long ordenPreferencia) {
        this.ordenPreferencia = ordenPreferencia;
    }

    public Long getTdoId() {
        return tdoId;
    }

    public void setTdoId(Long tdoId) {
        this.tdoId = tdoId;
    }

    public String getViaId() {
        return viaId;
    }

    public void setViaId(String viaId) {
        this.viaId = viaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEscalera() {
        return escalera;
    }

    public void setEscalera(String escalera) {
        this.escalera = escalera;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getPuerta() {
        return puerta;
    }

    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getProvinciaId() {
        return provinciaId;
    }

    public void setProvinciaId(String provinciaId) {
        this.provinciaId = provinciaId;
    }

    public Long getPuebloId() {
        return puebloId;
    }

    public void setPuebloId(Long puebloId) {
        this.puebloId = puebloId;
    }

    public String getPaisId() {
        return paisId;
    }

    public void setPaisId(String paisId) {
        this.paisId = paisId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomiciliosDTO that = (DomiciliosDTO) o;
        return Objects.equals(perId, that.perId) && Objects.equals(ordenPreferencia, that.ordenPreferencia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(perId, ordenPreferencia);
    }
}
