package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_BIC_TIPOS_RESERVAS")
public class BicicletaTiposReservasDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "TITULO")
    private String nombre;
    @Column(name = "CURSO_ACA")
    private Long cursoAca;
    @Column(name = "DIAS_DURACION")
    private Long diasDuracion;
    @Column(name = "FECHA_FIN_PRESTAMO")
    private Date fechaFinPrestamo;
    @Column(name = "FECHA_FIN")
    private Date fechaFin;
    @Column(name = "FECHA_INICIO")
    private Date fechaIni;

    public BicicletaTiposReservasDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getDiasDuracion() {
        return diasDuracion;
    }

    public void setDiasDuracion(Long diasDuracion) {
        this.diasDuracion = diasDuracion;
    }

    public Date getFechaFinPrestamo() {
        return fechaFinPrestamo;
    }

    public void setFechaFinPrestamo(Date fechaFinPrestamo) {
        this.fechaFinPrestamo = fechaFinPrestamo;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }
}
