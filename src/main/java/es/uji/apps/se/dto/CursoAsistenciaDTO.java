package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_CURSOS_ASISTENCIAS")
public class CursoAsistenciaDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CALENDARIO_CURSO_ID")
    private Long calendarioCursoId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    public CursoAsistenciaDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getCalendarioCursoId() {return calendarioCursoId;}
    public void setCalendarioCursoId(Long calendarioCursoId) {this.calendarioCursoId = calendarioCursoId;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}
}
