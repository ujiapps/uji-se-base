package es.uji.apps.se.dto;

import es.uji.apps.se.model.IndicadorSatisfaccion;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SE_INDICADORES_SATISFACCION")
public class IndicadorSatisfaccionDTO implements Serializable {

    @Id
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERIODO_ID", referencedColumnName = "ID")
    private PeriodoDTO periodoDTO;

    @ManyToOne
    @JoinColumn(name = "ACTIVIDAD_ID", referencedColumnName = "ID")
    private ActividadDTO actividadDTO;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID", referencedColumnName = "ID")
    private OfertaDTO ofertaDTO;

    @Column(name = "VALOR")
    private Float valor;

    @Column(name = "ENCUESTAS")
    private Long encuestas;

    @Column(name = "POBLACION")
    private Long poblacion;

    @Column(name = "CURSO_ID")
    private Long cursoId;

    @ManyToOne
    @JoinColumn(name = "INDICADOR_TIPO_ID", referencedColumnName = "ID")
    private IndicadoresTiposDTO indicadorTipoDTO;

    @Column(name = "TIPO_TEMPO")
    private Long tipoTempo;

    public IndicadorSatisfaccionDTO() {
    }

    public IndicadorSatisfaccionDTO(IndicadorSatisfaccion indicadorSatisfaccion) {
        this.id = indicadorSatisfaccion.getId();
        this.valor = indicadorSatisfaccion.getValor();
        this.cursoId = indicadorSatisfaccion.getCursoId();
        this.tipoTempo = indicadorSatisfaccion.getTipoTempo();
        this.encuestas = indicadorSatisfaccion.getEncuestas();
        this.poblacion = indicadorSatisfaccion.getPoblacion();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PeriodoDTO getPeriodoDTO() {
        return periodoDTO;
    }

    public void setPeriodoDTO(PeriodoDTO periodoDTO) {
        this.periodoDTO = periodoDTO;
    }

    public ActividadDTO getActividadDTO() {
        return actividadDTO;
    }

    public void setActividadDTO(ActividadDTO actividadDTO) {
        this.actividadDTO = actividadDTO;
    }

    public OfertaDTO getOfertaDTO() {
        return ofertaDTO;
    }

    public void setOfertaDTO(OfertaDTO ofertaDTO) {
        this.ofertaDTO = ofertaDTO;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Long getCursoId() {
        return cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }

    public IndicadoresTiposDTO getIndicadorTipoId() {
        return indicadorTipoDTO;
    }

    public void setIndicadorTipoId(IndicadoresTiposDTO indicadorTipoDTO) {
        this.indicadorTipoDTO = indicadorTipoDTO;
    }

    public Long getTipoTempo() {
        return tipoTempo;
    }

    public void setTipoTempo(Long tipoTempo) {
        this.tipoTempo = tipoTempo;
    }

    public Long getEncuestas() {
        return encuestas;
    }

    public void setEncuestas(Long encuestas) {
        this.encuestas = encuestas;
    }

    public Long getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(Long poblacion) {
        this.poblacion = poblacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndicadorSatisfaccionDTO that = (IndicadorSatisfaccionDTO) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, periodoDTO, actividadDTO, ofertaDTO, valor, cursoId, indicadorTipoDTO, tipoTempo);
    }
}
