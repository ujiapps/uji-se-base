package es.uji.apps.se.dto;

import es.uji.apps.se.model.EquipacionInscripcion;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SE_EQUIPACIONES_INSCRIPCIONES")
public class EquipacionesInscripcionesDTO {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "INSCRIPCION_ID")
    private Long inscripcionId;

    @Column(name = "EQUIPACION_ID")
    private Long equipacionId;

    @Column(name = "TALLA")
    private String talla;

    @Column(name = "DORSAL")
    private String dorsal;

    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "OBSERVACIONES")
    private String observacion;

    @Column(name = "FECHA_DEVOLUCION")
    @Temporal(TemporalType.DATE)
    private Date fechaDev;

    @Column(name = "TIPO_DEVOLUCION")
    private Long tipoDev;

    public EquipacionesInscripcionesDTO() {
    }

    public EquipacionesInscripcionesDTO(EquipacionInscripcion equipacionInscripcion) {
        this.id = equipacionInscripcion.getEquipacionInscripcion();
        this.inscripcionId = equipacionInscripcion.getInscripcionId();
        this.equipacionId = equipacionInscripcion.getEquipacionId();
        this.talla = equipacionInscripcion.getTalla();
        this.dorsal = equipacionInscripcion.getDorsal();
        this.fecha = equipacionInscripcion.getFecha();
        this.observacion = equipacionInscripcion.getObservacion();
        this.fechaDev = equipacionInscripcion.getFechaDev();
        this.tipoDev = equipacionInscripcion.getTipoDev();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInscripcionId() {
        return inscripcionId;
    }

    public void setInscripcionId(Long inscripcionId) {
        this.inscripcionId = inscripcionId;
    }

    public Long getEquipacionId() {
        return equipacionId;
    }

    public void setEquipacionId(Long equipacionId) {
        this.equipacionId = equipacionId;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getDorsal() {
        return dorsal;
    }

    public void setDorsal(String dorsal) {
        this.dorsal = dorsal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaDev() {
        return fechaDev;
    }

    public void setFechaDev(Date fechaDev) {
        this.fechaDev = fechaDev;
    }

    public Long getTipoDev() {
        return tipoDev;
    }

    public void setTipoDev(Long tipoDev) {
        this.tipoDev = tipoDev;
    }
}
