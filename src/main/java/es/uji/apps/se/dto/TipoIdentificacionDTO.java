package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "PER_TIPOS_IDENTIFICACION")
public class TipoIdentificacionDTO implements Serializable {

    @Id
    @Column(name = "ID")

    private Long id;

    @Column(name = "NOMBRE")

    private String nombre;

    public TipoIdentificacionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
