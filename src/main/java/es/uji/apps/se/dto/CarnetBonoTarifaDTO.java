package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_CARNETS_BONOS_TARIFAS")
public class CarnetBonoTarifaDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "TIPO_ID")
    private Long tipoId;

    @Column(name = "VINCULO_ID")
    private Long vinculoId;

    @Column(name = "IMPORTE")
    private Long importe;

    @Column(name = "PLAZOS")
    private Long plazos;

    public CarnetBonoTarifaDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public Long getVinculoId() {return vinculoId;}
    public void setVinculoId(Long vinculoId) {this.vinculoId = vinculoId;}

    public Long getImporte() {return importe;}
    public void setImporte(Long importe) {this.importe = importe;}

    public Long getPlazos() {return plazos;}
    public void setPlazos(Long plazos) {this.plazos = plazos;}
}
