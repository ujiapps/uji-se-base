package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SE_VW_USO_CLA_PER_VIN_TODOS")
public class VinculacionesActivasVWDTO {
    @Id
    @Column(name = "PERSONA_ID")
    private Long perId;

    @Column(name = "VINCULO_NOMBRE")
    private String vinculoNombre;

    public VinculacionesActivasVWDTO() {
    }

    public VinculacionesActivasVWDTO(Long perId, String vinculoNombre) {
        this.perId = perId;
        this.vinculoNombre = vinculoNombre;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public String getVinculoNombre() {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre) {
        this.vinculoNombre = vinculoNombre;
    }
}
