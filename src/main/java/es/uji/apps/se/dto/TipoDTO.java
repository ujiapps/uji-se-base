package es.uji.apps.se.dto;

import es.uji.commons.rest.annotations.DataTag;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_TIPOS")
public class TipoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @DataTag
    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="ORDEN")
    private Long orden;

    @DataTag
    @Column(name="USO")
    private String uso;

    @ManyToOne
    @JoinColumn(name="TIPO_ID")
    private TipoDTO tipoPadreDTO;

    @ManyToOne
    @JoinColumn(name="GRUPO_ID")
    private GrupoDTO grupoDTO;

    @OneToMany(mappedBy = "clasificacion")
    private Set<PeriodoDTO> periodosDTO;

    @OneToMany(mappedBy = "tipoPadreDTO")
    private Set<TipoDTO> tiposPadre;

    @OneToMany(mappedBy = "tipoDTO")
    private Set<ActividadTarifaDTO> tiposTarifas;

    @OneToMany(mappedBy = "vinculo")
    private Set<ActividadTarifaDTO> vinculos;

    @OneToMany(mappedBy = "vinculo")
    private Set<OfertaTarifaDTO> vinculosOferta;

    @OneToMany(mappedBy = "tipoReservaDTO")
    private Set<CalendarioOfertaDTO> calendariosOferta;

    @OneToMany(mappedBy = "estado")
    private Set<InscripcionDTO> estadosInscripcionDTO;

    @OneToMany(mappedBy = "tipoIncidenciaDTO")
    private Set<IncidenciaDTO> tiposIncidenciaDTO;

    @OneToMany(mappedBy = "tipo")
    private Set<ClaseDirigidaTipoDTO> claseDirigidaTiposDTO;

    @OneToMany(mappedBy = "vinculo")
    private Set<PersonaVinculoDTO> personaVinculosDTO;

    @OneToMany(mappedBy = "taxonomia")
    private Set<TarjetaDeportivaTipoDTO> tarjetasDeportivas;

    public TipoDTO() {
    }

    public TipoDTO(Long id) {
        this.id = id;
    }

    public TipoDTO(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public TipoDTO getTipoPadreDTO() {
        return tipoPadreDTO;
    }

    public void setTipoPadreDTO(TipoDTO tipoPadreDTO) {
        this.tipoPadreDTO = tipoPadreDTO;
    }

    public GrupoDTO getGrupo() {
        return grupoDTO;
    }

    public void setGrupo(GrupoDTO grupoDTO) {
        this.grupoDTO = grupoDTO;
    }

    public Set<PeriodoDTO> getPeriodos() {
        return periodosDTO;
    }

    public void setPeriodos(Set<PeriodoDTO> periodosDTO) {
        this.periodosDTO = periodosDTO;
    }

    public Set<TipoDTO> getTiposPadre() {
        return tiposPadre;
    }

    public void setTiposPadre(Set<TipoDTO> tiposPadre) {
        this.tiposPadre = tiposPadre;
    }

    public Set<ActividadTarifaDTO> getTiposTarifas() {
        return tiposTarifas;
    }

    public void setTiposTarifas(Set<ActividadTarifaDTO> tiposTarifas) {
        this.tiposTarifas = tiposTarifas;
    }

    public Set<ActividadTarifaDTO> getVinculos() {
        return vinculos;
    }

    public void setVinculos(Set<ActividadTarifaDTO> vinculos) {
        this.vinculos = vinculos;
    }

    public Set<OfertaTarifaDTO> getVinculosOferta() {
        return vinculosOferta;
    }

    public void setVinculosOferta(Set<OfertaTarifaDTO> vinculosOferta) {
        this.vinculosOferta = vinculosOferta;
    }

    public Set<CalendarioOfertaDTO> getCalendariosOferta() {
        return calendariosOferta;
    }

    public void setCalendariosOferta(Set<CalendarioOfertaDTO> calendariosOferta) {
        this.calendariosOferta = calendariosOferta;
    }

    public Set<InscripcionDTO> getEstadosInscripcion() {
        return estadosInscripcionDTO;
    }

    public void setEstadosInscripcion(Set<InscripcionDTO> estadosInscripcionDTO) {
        this.estadosInscripcionDTO = estadosInscripcionDTO;
    }

    public Set<IncidenciaDTO> getTiposIncidencia() {
        return tiposIncidenciaDTO;
    }

    public void setTiposIncidencia(Set<IncidenciaDTO> tiposIncidenciaDTO) {
        this.tiposIncidenciaDTO = tiposIncidenciaDTO;
    }

    public Set<ClaseDirigidaTipoDTO> getClaseDirigidaTipos() {
        return claseDirigidaTiposDTO;
    }

    public void setClaseDirigidaTipos(Set<ClaseDirigidaTipoDTO> claseDirigidaTiposDTO) {
        this.claseDirigidaTiposDTO = claseDirigidaTiposDTO;
    }

    public Set<PersonaVinculoDTO> getPersonaVinculos()
    {
        return personaVinculosDTO;
    }

    public void setPersonaVinculos(Set<PersonaVinculoDTO> personaVinculosDTO)
    {
        this.personaVinculosDTO = personaVinculosDTO;
    }

    public Set<TarjetaDeportivaTipoDTO> getTarjetasDeportivas() {
        return tarjetasDeportivas;
    }

    public void setTarjetasDeportivas(Set<TarjetaDeportivaTipoDTO> tarjetasDeportivas) {
        this.tarjetasDeportivas = tarjetasDeportivas;
    }
}