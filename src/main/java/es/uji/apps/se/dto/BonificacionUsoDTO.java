package es.uji.apps.se.dto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SE_BONOS_DTOS_USOS")
public class BonificacionUsoDTO {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="VALOR")
    private Long valor;
    @Column(name="ORIGEN")
    private String origen;
    @Column(name="TIPO_ENTRADA")
    private String tipoEntrada;

    @Column(name = "REFERENCIA_ID")
    private Long referencia;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "BONO_DTO_ID")
    private Long bonificacionId;

//    @ManyToOne
//    @JoinColumn(name = "BONO_DTO_ID")
//    private Bonificacion bonificacion;

    public BonificacionUsoDTO() {

    }

    public BonificacionUsoDTO(Long valor, String origen, Long referencia, Date fecha, Long bonificacionId, String tipoEntrada) {
        this.valor = valor;
        this.origen = origen;
        this.referencia = referencia;
        this.fecha = fecha;
        this.bonificacionId = bonificacionId;
        this.tipoEntrada = tipoEntrada;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getBonificacionId() {
        return bonificacionId;
    }

    public void setBonificacionId(Long bonificacionId) {
        this.bonificacionId = bonificacionId;
    }

    //    public Bonificacion getBonificacion() {
//        return bonificacion;
//    }
//
//    public void setBonificacion(Bonificacion bonificacion) {
//        this.bonificacion = bonificacion;
//    }
}
