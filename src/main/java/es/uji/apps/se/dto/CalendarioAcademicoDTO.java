package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_EXT_CALENDARIO")
public class CalendarioAcademicoDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_COMPLETA")
    private Date fecha;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="ANYO")
    private String anyo;

    public CalendarioAcademicoDTO() {
    }

    public Long getId() {
        return id;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public String getAnyo() {
        return anyo;
    }
}