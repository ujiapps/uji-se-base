package es.uji.apps.se.dto;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_MATERIALES")
public class MaterialDTO implements Serializable {


    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="STOCK_MIN")
    private Long stock;

    @Column(name="NOMBRE_FICHERO")
    private String nombreFichero;

    @Column(name="REFERENCIA_FICHERO")
    private String referenciaFichero;

    @Column(name="TIPO_FICHERO")
    private String tipoFichero;

    @OneToMany(mappedBy = "materialDTO")
    private Set<MaterialReservaDTO> materialesReserva;

    public MaterialDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public String getReferenciaFichero() {
        return referenciaFichero;
    }

    public void setReferenciaFichero(String referenciaFichero) {
        this.referenciaFichero = referenciaFichero;
    }

    public String getTipoFichero() {
        return tipoFichero;
    }

    public void setTipoFichero(String tipoFichero) {
        this.tipoFichero = tipoFichero;
    }

    public Set<MaterialReservaDTO> getMaterialesReserva() {
        return materialesReserva;
    }

    public void setMaterialesReserva(Set<MaterialReservaDTO> materialesReserva) {
        this.materialesReserva = materialesReserva;
    }
}
