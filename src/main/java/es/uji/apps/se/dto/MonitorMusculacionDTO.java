package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_MONITORES_MUSCULACION")
public class MonitorMusculacionDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="FECHA_ALTA")
    private Date fechaAlta;

    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @ManyToOne
    @JoinColumn(name="MONITOR_CURSO_ACA_ID")
    private MonitorCursoAcademicoDTO monitorCursoAcademico;

    @OneToMany(mappedBy = "monitorMusculacionDTO")
    private Set<MonitorMusculacionFichajeDTO> monitorMusculacionFichajes;

    public MonitorMusculacionDTO() {
    }

    public MonitorMusculacionDTO(Long monitorMusculacionId)
    {
        this.id = monitorMusculacionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public MonitorCursoAcademicoDTO getMonitorCursoAcademico() {
        return monitorCursoAcademico;
    }

    public void setMonitorCursoAcademico(MonitorCursoAcademicoDTO monitorCursoAcademico) {
        this.monitorCursoAcademico = monitorCursoAcademico;
    }

    public Set<MonitorMusculacionFichajeDTO> getMonitorMusculacionFichajes()
    {
        return monitorMusculacionFichajes;
    }

    public void setMonitorMusculacionFichajes(
            Set<MonitorMusculacionFichajeDTO> monitorMusculacionFichajes)
    {
        this.monitorMusculacionFichajes = monitorMusculacionFichajes;
    }
}
