package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "SE_VMC_ALUMNOS_ELITE")
public class AlumnoAsignaturaDTO implements Serializable {

    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asignatura;

    @Id
    @Column(name = "CURSO_ACA")
    private Long cursoAcademico;

    @Id
    @Column(name = "PROFESOR_ID")
    private Long personaId;

    @Id
    @Column(name = "DEPORTISTA_ELITE_ID")
    private Long deportistaEliteId;

    @Column(name = "APELLIDOS_NOMBRE")
    private String apellidosNombre;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "ENVIADO")
    private Boolean enviado;

    @Column(name = "FECHA_ENVIO")
    private Date fechaEnvio;

    @ManyToOne
    @JoinColumn(name = "PROFESOR_ID")
    private PersonaUjiDTO personaUjiDTO;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPORTISTA_ELITE_ID")
    private EliteDTO eliteDTO;

    public AlumnoAsignaturaDTO() {
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Boolean getEnviado() {
        return enviado;
    }

    public void setEnviado(Boolean enviado) {
        this.enviado = enviado;
    }

    public Boolean isEnviado() {
        return enviado;
    }

    public Long getDeportistaEliteId() {
        return deportistaEliteId;
    }

    public void setDeportistaEliteId(Long deportistaEliteId) {
        this.deportistaEliteId = deportistaEliteId;
    }

    public EliteDTO getElite() {
        return eliteDTO;
    }

    public void setElite(EliteDTO eliteDTO) {
        this.eliteDTO = eliteDTO;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlumnoAsignaturaDTO that = (AlumnoAsignaturaDTO) o;
        return Objects.equals(asignatura, that.asignatura) && Objects.equals(cursoAcademico, that.cursoAcademico) && Objects.equals(personaId, that.personaId) && Objects.equals(deportistaEliteId, that.deportistaEliteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(asignatura, cursoAcademico, personaId, deportistaEliteId);
    }
}