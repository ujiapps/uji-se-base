package es.uji.apps.se.dto;

import es.uji.apps.se.model.DatosPersona;
import es.uji.commons.rest.annotations.DataTag;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_PERSONAS")
public class PersonaDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @DataTag
    @Column(name = "MAIL")
    private String mail;

    @Column(name = "MOVIL_PUBLICO")
    private String movilPublico;

    @DataTag
    @Column(name = "MOVIL")
    private String movil;

    @Column(name = "OTROS_CONTACTOS")
    private String contactos;

    @Column(name = "ARBITRO")
    private Boolean arbitro;

    @Column(name = "ENTRENADOR")
    private Boolean entrenador;

    @Column(name = "PROFESOR")
    private Boolean profesor;

    @Column(name = "RECIBIR_CORREO")
    private Boolean recibirCorreo;

    @Column(name = "LIBRO_FAMILIA")
    private Long libroFamilia;

    @Column(name = "CUENTA_BANCARIA_ID")
    private Long cuentaBancaria;

    @Column(name = "CUENTA_TWITTER")
    private String cuentaTwitter;

    @Column(name = "HORARIO_PREFERENCIA")
    private String horario_preferencia;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @DataTag
    @Column(name = "NOMBRE")
    private String nombre;

    @DataTag
    @Column(name = "APELLIDO1")
    private String apellido1;

    @DataTag
    @Column(name = "APELLIDO2")
    private String apellido2;

    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    @Column(name = "SEXO")
    private Long sexo;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;


    public PersonaDTO() {
    }

    public PersonaDTO(DatosPersona datosPersona) {
        this.identificacion = datosPersona.getIdentificacion();
        this.nombre = datosPersona.getNombre();
        this.apellido1 = datosPersona.getApellido1();
        this.apellido2 = datosPersona.getApellido2();
        this.fechaNacimiento = datosPersona.getFechaNacimiento();
        this.sexo = datosPersona.getSexo().equals("Home") ? 1L : 2L;
        this.arbitro = datosPersona.isArbitro();
        this.mail = datosPersona.getMail();
        this.movil = datosPersona.getMovil();
        this.movilPublico = datosPersona.getMovilPublico();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMovilPublico() {
        return movilPublico;
    }

    public void setMovilPublico(String movilPublico) {
        this.movilPublico = movilPublico;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getContactos() {
        return contactos;
    }

    public void setContactos(String contactos) {
        this.contactos = contactos;
    }

    public Boolean getArbitro() {
        return arbitro;
    }

    public void setArbitro(Boolean arbitro) {
        this.arbitro = arbitro;
    }

    public Boolean getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Boolean entrenador) {
        this.entrenador = entrenador;
    }

    public Boolean getProfesor() {
        return profesor;
    }

    public void setProfesor(Boolean profesor) {
        this.profesor = profesor;
    }

    public Boolean getRecibirCorreo() {
        return recibirCorreo;
    }

    public Boolean isRecibirCorreo() {
        return recibirCorreo;
    }

    public void setRecibirCorreo(Boolean recibirCorreo) {
        this.recibirCorreo = recibirCorreo;
    }

    public Long getLibroFamilia() {
        return libroFamilia;
    }

    public void setLibroFamilia(Long libroFamilia) {
        this.libroFamilia = libroFamilia;
    }

    public Long getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(Long cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }

    public String getCuentaTwitter() {
        return cuentaTwitter;
    }

    public void setCuentaTwitter(String cuentaTwitter) {
        this.cuentaTwitter = cuentaTwitter;
    }

    public String getHorario_preferencia() {
        return horario_preferencia;
    }

    public void setHorario_preferencia(String horario_preferencia) {
        this.horario_preferencia = horario_preferencia;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public String getApellidosNombre() {
        return this.apellido1 + " " + this.apellido2 + ", " + this.nombre;
    }


    public String getNombreApellidos() {
        return this.nombre + " " + this.apellido1 + " " + this.apellido2;
    }

}