package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SE_VW_PROFESORES_ELITE")
public class ProfesorEliteDTO implements Serializable {

    @Id
    @Column(name="ASIGNATURA_ID")
    private String asignatura;

    @Id
    @Column(name="CURSO_ACA")
    private Long cursoAcademico;

    @Id
    @Column(name="PROFESOR_ID")
    private Long personaId;

    @Column(name="IDENTIFICACION")
    private String identificacion;

    @Column(name="APELLIDOS_NOMBRE")
    private String apellidosNombre;

    @Column(name="CORREO")
    private String correo;

    @Column(name="ALUMNOS")
    private Long alumnos;

    @Column(name="AVISOS")
    private Long avisos;

    @ManyToOne
    @JoinColumn(name = "PROFESOR_ID")
    private PersonaUjiDTO personaUjiDTO;

    public ProfesorEliteDTO() {
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public Long getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(Long alumnos) {
        this.alumnos = alumnos;
    }

    public Long getAvisos() {
        return avisos;
    }

    public void setAvisos(Long avisos) {
        this.avisos = avisos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfesorEliteDTO that = (ProfesorEliteDTO) o;
        return Objects.equals(asignatura, that.asignatura) && Objects.equals(cursoAcademico, that.cursoAcademico) && Objects.equals(personaId, that.personaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(asignatura, cursoAcademico, personaId);
    }
}