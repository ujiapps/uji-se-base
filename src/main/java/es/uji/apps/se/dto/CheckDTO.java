package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_CLASES_DIRIGIDAS_CHECKS")
public class CheckDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_CHECK")
    private Date fechaCheck;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_PROCESADO")
    private Date fechaProcesado;

    @Column(name="ESTADO")
    private String estado;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO persona;

    @Column(name="TARJETA_DEPORTIVA_ID")
    private Long tarjetaDeportiva;

    @ManyToOne
    @JoinColumn(name = "CALENDARIO_CLASE_DIRIGIDA_ID")
    private CalendarioClaseDTO calendarioClaseDTO;


    public CheckDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaCheck() {
        return fechaCheck;
    }

    public void setFechaCheck(Date fechaCheck) {
        this.fechaCheck = fechaCheck;
    }

    public CalendarioClaseDTO getCalendarioClase() {
        return calendarioClaseDTO;
    }

    public void setCalendarioClase(CalendarioClaseDTO calendarioClaseDTO) {
        this.calendarioClaseDTO = calendarioClaseDTO;
    }

    public PersonaUjiDTO getPersona() {
        return persona;
    }

    public void setPersona(PersonaUjiDTO persona) {
        this.persona = persona;
    }

    public Date getFechaProcesado() {
        return fechaProcesado;
    }

    public void setFechaProcesado(Date fechaProcesado) {
        this.fechaProcesado = fechaProcesado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getTarjetaDeportiva() {
        return tarjetaDeportiva;
    }

    public void setTarjetaDeportiva(Long tarjetaDeportiva) {
        this.tarjetaDeportiva = tarjetaDeportiva;
    }
}
