package es.uji.apps.se.dto;

import es.uji.commons.db.hibernate.converters.BooleanToSiNoConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_BONOS_DTOS")
public class BonificacionDTO implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "REFERENCIA_ID")
    private Long referencia;

    @Column(name = "CAMPANYA_ID")
    private Long campanyaId;

    @Column(name = "ORIGEN")
    private String origen;

    @Column(name = "USOS")
    private Long usos;

    @Column(name = "IMPORTE")
    private Float importe;

    @Column(name = "PORCENTAJE")
    private Float porcentaje;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INI")
    private Date fechaIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Column(name = "MOSTRAR")
    private Long mostrar;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    public BonificacionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }

    public Long getCampanyaId() {return campanyaId;}
    public void setCampanyaId(Long campanyaId) {this.campanyaId = campanyaId;}

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getUsos() {
        return usos;
    }

    public void setUsos(Long usos) {
        this.usos = usos;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public Float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Float porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Long getMostrar() {return mostrar;}
    public void setMostrar(Long mostrar) {this.mostrar = mostrar;}

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }
}