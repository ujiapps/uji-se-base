package es.uji.apps.se.dto;

import es.uji.apps.se.model.EquipacionesMovimiento;
import es.uji.commons.rest.ParamUtils;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SE_EQUIPACIONES_MOV")
public class EquipacionesMovimientoDTO {

    @Id
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "EQUIPACION_ID")
    private EquipacionDTO equipacionDTO;

    @ManyToOne
    @JoinColumn(name = "MOVIMIENTO_ID")
    private EquipacionTipoMovimientoDTO equipacionTipoMovimientoDTO;

    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "TALLA")
    private String talla;

    @Column(name = "DORSAL")
    private String dorsal;

    @Column(name = "CANTIDAD")
    private Long cantidad;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    public EquipacionesMovimientoDTO() {
    }

    public EquipacionesMovimientoDTO(EquipacionesMovimiento equipacionesMovimiento) {
        this.id = equipacionesMovimiento.getId();
        EquipacionDTO equipacionDTO = new EquipacionDTO();
        equipacionDTO.setId(equipacionesMovimiento.getEquipacionId());
        this.equipacionDTO = equipacionDTO;
        EquipacionTipoMovimientoDTO equipacionTipoMovimientoDTO = new EquipacionTipoMovimientoDTO();
        equipacionTipoMovimientoDTO.setId(equipacionesMovimiento.getMovimientoId());
        this.equipacionTipoMovimientoDTO = equipacionTipoMovimientoDTO;
        this.fecha = equipacionesMovimiento.getFecha();
        this.talla = equipacionesMovimiento.getTalla();
//        this.talla = ParamUtils.isNotNull(equipacionesMovimiento.getTalla()) ? equipacionesMovimiento.getTalla().toUpperCase().replaceAll("\\s+", "") : null;
//        if (ParamUtils.isNotNull(this.talla))
//            if (equipacionesMovimiento.getTalla().equals("TALLAÚNICA")) this.setTalla(null);

        this.dorsal = ParamUtils.isNotNull(equipacionesMovimiento.getDorsal()) ? equipacionesMovimiento.getDorsal().toString() : null;
        this.cantidad = equipacionesMovimiento.getCantidad();
        this.observaciones = equipacionesMovimiento.getObservaciones();
        this.personaId = equipacionesMovimiento.getPersonaId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EquipacionDTO getEquipacionDTO() {
        return equipacionDTO;
    }

    public void setEquipacionDTO(EquipacionDTO equipacionDTO) {
        this.equipacionDTO = equipacionDTO;
    }

    public EquipacionTipoMovimientoDTO getEquipacionTipoMovimientoDTO() {
        return equipacionTipoMovimientoDTO;
    }

    public void setEquipacionTipoMovimientoDTO(EquipacionTipoMovimientoDTO equipacionTipoMovimientoDTO) {
        this.equipacionTipoMovimientoDTO = equipacionTipoMovimientoDTO;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getDorsal() {
        return dorsal;
    }

    public void setDorsal(String dorsal) {
        this.dorsal = dorsal;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }
}