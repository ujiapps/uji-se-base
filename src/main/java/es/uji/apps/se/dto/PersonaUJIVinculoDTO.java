package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_VMC_PERSONAS_VINCULOS")
//@Table(name = "SE_VMC_PERSONAS_VINCULOS")

public class PersonaUJIVinculoDTO implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "vinculo_nombre")
    private String vinculo;

    public PersonaUJIVinculoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVinculo() {
        return vinculo;
    }

    public void setVinculo(String vinculo) {
        this.vinculo = vinculo;
    }
}