package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_MODALIDADES_DEPORTIVAS")
public class ModalidadDeportivaDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    @OneToMany(mappedBy = "modalidadDeportivaDTO")
    private Set<EliteDTO> modalidadesDeportivas;

    public ModalidadDeportivaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<EliteDTO> getModalidadesDeportivas() {
        return modalidadesDeportivas;
    }

    public void setModalidadesDeportivas(Set<EliteDTO> modalidadesDeportivas) {
        this.modalidadesDeportivas = modalidadesDeportivas;
    }
}