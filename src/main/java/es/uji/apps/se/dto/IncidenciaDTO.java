package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_INSCRIPCIONES_INCIDENCIAS")
public class IncidenciaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String comentario;
    private String comentario2;

    @ManyToOne
    @JoinColumn(name = "TIPO_INCIDENCIA_ID")
    private TipoDTO tipoIncidenciaDTO;

    @ManyToOne
    @JoinColumn(name = "INSCRIPCION_ID")
    private InscripcionDTO inscripcionDTO;

}
