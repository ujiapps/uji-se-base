package es.uji.apps.se.dto;

import javax.persistence.*;

@Entity
@Table(name = "SE_COMP_MIEMBROS_SANCIONES")
public class CompeticionSancionDTO {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "PARTIDO_ID")
    private Long partido;

    @Column(name = "TARJETAS")
    private String tarjetas;

    @Column(name = "EXCLUSIONES")
    private Long exclusiones;

    @ManyToOne
    @JoinColumn(name = "MIEMBRO_ID")
    private CompeticionMiembroDTO competicionMiembro;

    public CompeticionSancionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartido() {
        return partido;
    }

    public void setPartido(Long partido) {
        this.partido = partido;
    }

    public String getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(String tarjetas) {
        this.tarjetas = tarjetas;
    }

    public Long getExclusiones() {
        return exclusiones;
    }

    public void setExclusiones(Long exclusiones) {
        this.exclusiones = exclusiones;
    }

    public CompeticionMiembroDTO getCompeticionMiembro() {
        return competicionMiembro;
    }

    public void setCompeticionMiembro(CompeticionMiembroDTO competicionMiembro) {
        this.competicionMiembro = competicionMiembro;
    }
}
