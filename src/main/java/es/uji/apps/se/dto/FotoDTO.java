package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_EXT_FOTOS_BIN")
public class FotoDTO implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO persona;

    @Lob
    @Column(name = "BINARIO")
    private byte[] foto;

    @Column(name = "NOMBRE_FICHERO")
    private String nombre;

    @Column (name = "MIME_TYPE")
    private String mimeType;

    public FotoDTO() {
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public PersonaUjiDTO getPersona() {
        return persona;
    }

    public void setPersona(PersonaUjiDTO persona) {
        this.persona = persona;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
