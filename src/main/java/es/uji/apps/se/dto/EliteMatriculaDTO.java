package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_EXT_ELITE_MATRICULA")
public class EliteMatriculaDTO implements Serializable{

    @Id
    @Column(name="IDENTIFICACION")
    private String identificacion;

    @Column(name="CURSO_ACA")
    private Long cursoAca;

    @Column(name="TITULACION")
    private String titulacion;

    @Column(name="APELLIDOS_NOMBRE")
    private String apellidosNombre;

    @Column(name="MAIL")
    private String mail;

    @Column(name="MOVIL")
    private String movil;

    public EliteMatriculaDTO() {
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public String getTitulacion() {
        return titulacion;
    }

    public void setTitulacion(String titulacion) {
        this.titulacion = titulacion;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }
}
