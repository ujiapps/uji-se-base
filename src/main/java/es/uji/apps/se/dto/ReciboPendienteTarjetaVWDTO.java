package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_RECIBOS_PENDIENTES_TAR")
public class ReciboPendienteTarjetaVWDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="PERSONA_ID")
    private Long personaId;

    @Column(name="TIPO_ID")
    private Long tipoId;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="ORIGEN")
    private String origen;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_INI")
    private Date fechaInicio;

    public ReciboPendienteTarjetaVWDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getTipoId() {return tipoId;}
    public void setTipoId(Long tipoId) {this.tipoId = tipoId;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}
}
