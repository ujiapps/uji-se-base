package es.uji.apps.se.dto;

import javax.persistence.*;

@Entity
@Table(name = "SE_CLA_SANCION_PERIODOS")
public class SancionPeriodoDTO {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="DIA_INICIO")
    private Long diaInicio;

    @Column(name="DIA_FIN")
    private Long diaFin;

    @Column(name="MES_INICIO")
    private Long mesInicio;

    @Column(name="MES_FIN")
    private Long mesFin;

    private Boolean activo;

    public SancionPeriodoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(Long diaInicio) {
        this.diaInicio = diaInicio;
    }

    public Long getDiaFin() {
        return diaFin;
    }

    public void setDiaFin(Long diaFin) {
        this.diaFin = diaFin;
    }

    public Long getMesInicio() {
        return mesInicio;
    }

    public void setMesInicio(Long mesInicio) {
        this.mesInicio = mesInicio;
    }

    public Long getMesFin() {
        return mesFin;
    }

    public void setMesFin(Long mesFin) {
        this.mesFin = mesFin;
    }

    public Boolean getActivo() {
        return activo;
    }

    public Boolean isActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }
}
