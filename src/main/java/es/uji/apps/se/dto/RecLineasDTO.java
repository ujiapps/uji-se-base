package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "REC_LINEAS", schema = "UJI_RECIBOS")
public class RecLineasDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "REC_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "RECIBO_ID")
    private RecRecibosDTO recibo;

    @Column(name = "IMPORTE")
    private Float importe;

    public RecLineasDTO() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public RecRecibosDTO getRecibo() { return recibo; }
    public void setRecibo(RecRecibosDTO recibo) { this.recibo = recibo; }

    public Float getImporte() { return importe; }
    public void setImporte(Float importe) { this.importe = importe; }
}