package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "se_vw_personas_filtro")
public class EnvioPersonaFiltroVwDTO implements Serializable {
    @Id
    @Column(name="PERSONA_ID")
    private Long id;
    private Long key;
    private Long estado;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public EnvioPersonaFiltroVwDTO() {
    }

    public Long getId() {
        return id;
    }

    public Long getKey() {
        return key;
    }

    public Long getEstado() {
        return estado;
    }

    public Date getFecha() {
        return fecha;
    }
}
