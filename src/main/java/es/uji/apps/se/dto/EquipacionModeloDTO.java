package es.uji.apps.se.dto;

import es.uji.apps.se.model.EquipacionesModelo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_EQUIPACIONES_MODELOS")
public class EquipacionModeloDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    public EquipacionModeloDTO() {}

    public EquipacionModeloDTO(EquipacionesModelo equipacionesModelo) {
        setId(equipacionesModelo.getId());
        setNombre(equipacionesModelo.getNombre());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}