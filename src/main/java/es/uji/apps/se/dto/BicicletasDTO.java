package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_BICICLETAS")
public class BicicletasDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "NUM_BICI")
    private Long numBici;
    @Column(name = "ESTADO_ID")
    private Long estadoId;
    @Column(name = "FECHA_BAJA")
    private Date fechaBaja;
    @Column(name = "FECHA_ALTA")
    private Date fechaAlta;

    public BicicletasDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumBici() {
        return numBici;
    }

    public void setNumBici(Long numBici) {
        this.numBici = numBici;
    }

    public Long getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Long estadoId) {
        this.estadoId = estadoId;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }
}
