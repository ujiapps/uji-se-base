package es.uji.apps.se.dto.views;

import es.uji.commons.db.hibernate.converters.BooleanToSiNoConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_INSCRIPCIONES_USUARIO")
public class VWInscripcionUsuarioDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="PERSONA_ID")
    private Long personaId;

    @Column(name="MUESTRA_COMENTARIO")
    private Long muestraComentario;

    @Column(name="CURSO_ACA")
    private Long cursoAcademico;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="GRUPO")
    private String grupo;

    @Column(name="FECHA")
    private Date fecha;

    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @Column(name="PERIODO_ID")
    private Long periodoId;

    @Column(name="NOM_PERIODO")
    private String periodoNombre;

    @Column(name="POS_ESPERA")
    private Long posEspera;

    @Column(name="ESTADO_ID")
    private Long estadoId;

    @Column(name="OFERTA_ID")
    private Long ofertaId;

    @Column(name="NUMERO_VECES")
    private int numeroVeces;

    @Column(name="OBSERVACIONES")
    private String observaciones;

    @Column(name = "APTO")
    private Long apto;

    @Column(name="FECHA_INI")
    @Temporal(TemporalType.DATE)
    private Date fechaInicioActividad;

    @Column(name="FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFinActividad;

    @Column(name="FECHA_RECOGIDA")
    @Temporal(TemporalType.DATE)
    private Date fechaRecogidaCertificado;

    public VWInscripcionUsuarioDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getMuestraComentario() {return muestraComentario;}
    public void setMuestraComentario(Long muestraComentario) {this.muestraComentario = muestraComentario;}

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}

    public Long getPeriodoId() {
        return periodoId;
    }

    public void setPeriodoId(Long periodoId) {
        this.periodoId = periodoId;
    }

    public String getPeriodoNombre() {
        return periodoNombre;
    }

    public void setPeriodoNombre(String periodoNombre) {
        this.periodoNombre = periodoNombre;
    }

    public Long getPosEspera() {
        return posEspera;
    }

    public void setPosEspera(Long posEspera) {
        this.posEspera = posEspera;
    }

    public Long getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Long estadoId) {
        this.estadoId = estadoId;
    }

    public Long getOfertaId() {
        return ofertaId;
    }

    public void setOfertaId(Long ofertaId) {
        this.ofertaId = ofertaId;
    }

    public int getNumeroVeces() {
        return numeroVeces;
    }

    public void setNumeroVeces(int numeroVeces) {
        this.numeroVeces = numeroVeces;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getApto() {
        return apto;
    }

    public void setApto(Long apto) {
        this.apto = apto;
    }

    public Date getFechaInicioActividad() {
        return fechaInicioActividad;
    }

    public void setFechaInicioActividad(Date fechaInicioActividad) {
        this.fechaInicioActividad = fechaInicioActividad;
    }

    public Date getFechaFinActividad() {
        return fechaFinActividad;
    }

    public void setFechaFinActividad(Date fechaFinActividad) {
        this.fechaFinActividad = fechaFinActividad;
    }

    public Date getFechaRecogidaCertificado() {return fechaRecogidaCertificado;}
    public void setFechaRecogidaCertificado(Date fechaRecogidaCertificado) {this.fechaRecogidaCertificado = fechaRecogidaCertificado;}
}
