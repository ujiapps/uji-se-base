package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_CAR_BON_TIPOS_HORARIOS")
public class TipoHorarioCarnetBonoDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="CARNET_BONO_TIPO_ID")
    private Long carnetBonoTipoId;

    @Column(name="DIA_SEMANA")
    private Long diaSemana;

    @Column(name="HORA_INI")
    private String horaInicio;

    @Column(name="HORA_FIN")
    private String horaFin;

    public TipoHorarioCarnetBonoDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getCarnetBonoTipoId() {return carnetBonoTipoId;}
    public void setCarnetBonoTipoId(Long carnetBonoTipoId) {this.carnetBonoTipoId = carnetBonoTipoId;}

    public Long getDiaSemana() {return diaSemana;}
    public void setDiaSemana(Long diaSemana) {this.diaSemana = diaSemana;}

    public String getHoraInicio() {return horaInicio;}
    public void setHoraInicio(String horaInicio) {this.horaInicio = horaInicio;}

    public String getHoraFin() {return horaFin;}
    public void setHoraFin(String horaFin) {this.horaFin = horaFin;}
}
