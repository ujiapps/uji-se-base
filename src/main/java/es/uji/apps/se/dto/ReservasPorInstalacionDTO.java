package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "se_Vw_ind_reservas")
public class ReservasPorInstalacionDTO implements Serializable
{
    @Id
    @Column(name = "RESERVA_INSTALACION_ID")
    private Long id;

    @Column(name="TORNO_ID")
    private Long zonaId;

    private String origen;

    @Column(name="ORIGEN_TIPO")
    private Long origenTipo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dia;

    @Column(name="HORA_INICIO")
    private String horaInicio;

    @Column(name="HORA_FIN")
    private String horaFin;

    @Column(name="HORAS_RESERVA")
    private Float horasReserva;

    private Long dependencia;

    @Column(name="GRUPO_INSTALACIONES_NOMBRE")
    private String grupoInstalacionesNombre;

    @Column(name="GRUPO_INSTALACIONES_ID")
    private Long grupoInstalaciones;


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getZonaId() {
        return zonaId;
    }

    public void setZonaId(Long zonaId) {
        this.zonaId = zonaId;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getOrigenTipo() {
        return origenTipo;
    }

    public void setOrigenTipo(Long origenTipo) {
        this.origenTipo = origenTipo;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public Long getDependencia() {
        return dependencia;
    }

    public void setDependencia(Long dependencia) {
        this.dependencia = dependencia;
    }

    public Float getHorasReserva() {
        return horasReserva;
    }

    public void setHorasReserva(Float horasReserva) {
        this.horasReserva = horasReserva;
    }

    public String getGrupoInstalacionesNombre() {
        return grupoInstalacionesNombre;
    }

    public void setGrupoInstalacionesNombre(String grupoInstalacionesNombre) {
        this.grupoInstalacionesNombre = grupoInstalacionesNombre;
    }

    public Long getGrupoInstalaciones() {
        return grupoInstalaciones;
    }

    public void setGrupoInstalaciones(Long grupoInstalaciones) {
        this.grupoInstalaciones = grupoInstalaciones;
    }
}