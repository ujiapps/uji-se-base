package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "REC_VW_LINEAS")
public class LineaReciboVWDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RECIBO_ID")
    private Long reciboId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "REFERENCIA1_ID")
    private Long tarjetaBonoId;

    @Column(name = "NETO")
    private Long neto;

    @Column(name = "ORIGEN")
    private String origen;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    public LineaReciboVWDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getReciboId() {return reciboId;}
    public void setReciboId(Long reciboId) {this.reciboId = reciboId;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getTarjetaBonoId() {return tarjetaBonoId;}
    public void setTarjetaBonoId(Long tarjetaBonoId) {this.tarjetaBonoId = tarjetaBonoId;}

    public Long getNeto() {return neto;}
    public void setNeto(Long neto) {this.neto = neto;}

    public String getOrigen() {return origen;}
    public void setOrigen(String origen) {this.origen = origen;}

    public Date getFecha() {return fecha;}
    public void setFecha(Date fecha) {this.fecha = fecha;}
}
