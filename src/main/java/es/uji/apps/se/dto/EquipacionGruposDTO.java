package es.uji.apps.se.dto;

import es.uji.apps.se.model.EquipacionGrupos;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_EQUIPACIONES_GRUPOS")
public class EquipacionGruposDTO implements Serializable {

    @Id
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoDTO tipoDTO;

    @Column(name = "HAY_QUE_DEVOLVER")
    private Long hayQueDevolver;

    public EquipacionGruposDTO() { }

    public EquipacionGruposDTO(EquipacionGrupos equipacionGrupos) {
        this.id = equipacionGrupos.getId();
        this.nombre = equipacionGrupos.getNombre();
        TipoDTO tipoDTO = new TipoDTO();
        tipoDTO.setId(equipacionGrupos.getTipoId());
        tipoDTO.setNombre(equipacionGrupos.getTipoNombre());
        this.tipoDTO = tipoDTO;
        this.hayQueDevolver = equipacionGrupos.getHayQueDevolver();
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public TipoDTO getTipo() { return tipoDTO; }
    public void setTipo(TipoDTO tipoDTO) { this.tipoDTO = tipoDTO; }

    public Long getHayQueDevolver() { return hayQueDevolver; }
    public void setHayQueDevolver(Long hayQueDevolver) { this.hayQueDevolver = hayQueDevolver; }
}