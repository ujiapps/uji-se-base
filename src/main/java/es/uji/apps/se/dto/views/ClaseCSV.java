package es.uji.apps.se.dto.views;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_CLASES_DIRIGIDAS_CSV")
public class ClaseCSV implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CLASE")
    private String clase;

    @Column(name = "CLASE_ID")
    private Long claseId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DIA")
    private Date dia;

    @Column(name = "HORA_INICIO")
    private String horaInicio;

    @Column(name = "INSTALACION")
    private String instalacion;

    @Column(name = "INSTALACION_ID")
    private Long instalacionId;

    @Column(name = "PLAZAS")
    private Long plazas;

    @Column(name = "RESERVAS")
    private Long reservas;

    @Column(name = "ASISTENCIAS")
    private Long asistencias;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "MOVIL")
    private String movil;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "APELLIDOS_NOMBRE")
    private String apellidosNombre;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    @Column(name = "SEXO")
    private String sexo;

    @Column(name = "VINCULO_ACTUAL")
    private String vinculoActual;

    @Column(name="TIPO_TARJETA")
    private String tipoTarjeta;

    @Column(name="ASISTE")
    private Boolean asiste;

    public ClaseCSV() {
    }

    public ClaseCSV(Long personaId) {
        this.id = personaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public Long getSexoLong(){
        return (sexo.toLowerCase().equals("dona"))?2L:1L;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public String getVinculoActual() {
        return vinculoActual;
    }

    public void setVinculoActual(String vinculoActual) {
        this.vinculoActual = vinculoActual;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    public Long getPlazas() {
        return plazas;
    }

    public void setPlazas(Long plazas) {
        this.plazas = plazas;
    }

    public Long getReservas() {
        return reservas;
    }

    public void setReservas(Long reservas) {
        this.reservas = reservas;
    }

    public Long getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(Long asistencias) {
        this.asistencias = asistencias;
    }

    public Long getClaseId() {
        return claseId;
    }

    public void setClaseId(Long claseId) {
        this.claseId = claseId;
    }

    public Long getInstalacionId() {
        return instalacionId;
    }

    public void setInstalacionId(Long instalacionId) {
        this.instalacionId = instalacionId;
    }

    public Boolean isAsiste() {
        return asiste;
    }
}