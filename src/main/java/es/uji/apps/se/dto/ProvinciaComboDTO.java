package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "PER_PROVINCIAS")
public class ProvinciaComboDTO implements Serializable {

    @Id
    @Column(name = "ID")

    private String id;

    @Column(name = "NOMBRE")

    private String nombre;

    public ProvinciaComboDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}