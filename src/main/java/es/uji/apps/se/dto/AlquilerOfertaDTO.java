package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_ALQUILERES_OFERTA")
public class AlquilerOfertaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String grupo;

    @Column(name="CURSO_ACA")
    private Long cursoAcademico;
    public AlquilerOfertaDTO() {
    }

    @ManyToOne
    @JoinColumn(name="ALQUILER_ID")
    private AlquilerDTO alquilerDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public AlquilerDTO getAlquilerDTO() {
        return alquilerDTO;
    }

    public void setAlquilerDTO(AlquilerDTO alquilerDTO) {
        this.alquilerDTO = alquilerDTO;
    }
}