package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_CAMPANYAS")
public class CampanyaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "MOSTRAR")
    private Long mostrar;

    @Column(name = "DIAS")
    private Long dias;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;

    public CampanyaDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public Long getMostrar() {return mostrar;}
    public void setMostrar(Long mostrar) {this.mostrar = mostrar;}

    public Long getDias() {return dias;}
    public void setDias(Long dias) {this.dias = dias;}

    public Date getFechaFin() {return fechaFin;}
    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}
}
