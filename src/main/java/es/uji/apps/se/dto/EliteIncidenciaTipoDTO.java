package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_ELITE_INCIDENCIAS_TIPOS")
public class EliteIncidenciaTipoDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;


    @OneToMany(mappedBy = "tipoIncidencia")
    private Set<IncidenciaEliteDTO> tiposIncidenciasElite;

    public EliteIncidenciaTipoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<IncidenciaEliteDTO> getTiposIncidenciasElite() {
        return tiposIncidenciasElite;
    }

    public void setTiposIncidenciasElite(Set<IncidenciaEliteDTO> tiposIncidenciasElite) {
        this.tiposIncidenciasElite = tiposIncidenciasElite;
    }

    public String getNombreEs() {
        return nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk() {
        return nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }

    public String getNombre(String idioma) {
        switch (idioma.toUpperCase()) {
            case "ES":
                return this.nombreEs;
            case "UK":
                return this.nombreUk;
            default:
                return this.nombre;
        }
    }
}