package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity @Table(name = "SE_JAULAS_PERSONAS")
public class JaulaPersonaDTO implements Serializable
{
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUji;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_LLAVERO")
    private Date fechaLlavero;

    public JaulaPersonaDTO()
    {
    }

    public JaulaPersonaDTO(Long personaId)
    {
        PersonaUjiDTO personaUjiDTO = new PersonaUjiDTO();
        personaUjiDTO.setId(personaId);
        this.setPersonaUji(personaUjiDTO);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUji;
    }

    public void setPersonaUji(PersonaUjiDTO personaUji) {
        this.personaUji = personaUji;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaLlavero() {
        return fechaLlavero;
    }

    public void setFechaLlavero(Date fechaLlavero) {
        this.fechaLlavero = fechaLlavero;
    }
}