package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_MONITORES_TITULACIONES")
public class MonitorTitulacionDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="VALOR")
    private Long valor;

    @OneToMany(mappedBy = "monitorTitulacion")
    private Set<MonitorDTO> monitores;

    public MonitorTitulacionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public Set<MonitorDTO> getMonitores() {
        return monitores;
    }

    public void setMonitores(Set<MonitorDTO> monitores) {
        this.monitores = monitores;
    }
}