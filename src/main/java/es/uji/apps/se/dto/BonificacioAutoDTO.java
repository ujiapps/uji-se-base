package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_BONOS_DTOS_AUTO")
public class BonificacioAutoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "REFERENCIA1_ID")
    private Long referencia1Id;

    @Column(name = "ORIGEN1")
    private String origen1;

    @Column(name = "REFERENCIA2_ID")
    private Long referencia2Id;

    @Column(name = "ORIGEN2")
    private String origen2;

    @Column(name = "USOS")
    private Long usos;

    @Column(name = "IMPORTE")
    private Float importe;

    @Column(name = "PORCENTAJE")
    private Float porcentaje;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private CampanyaDTO campanya;

    @Column(name = "FECHA_INI")
    @Temporal(TemporalType.DATE)
    private Date fechaIni;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;

    @Column(name = "MOSTRAR")
    private Long mostrar;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    public BonificacioAutoDTO() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getReferencia1Id() { return referencia1Id; }
    public void setReferencia1Id(Long referencia1Id) { this.referencia1Id = referencia1Id; }

    public String getOrigen1Id() { return origen1; }
    public void setOrigen1Id(String origen1) { this.origen1 = origen1;
    }

    public Long getReferencia2Id() { return referencia2Id; }
    public void setReferencia2Id(Long referencia2Id) { this.referencia2Id = referencia2Id; }

    public String getOrigen2() { return origen2; }
    public void setOrigen2(String origen2) { this.origen2 = origen2; }

    public Long getUsos() { return usos; }
    public void setUsos(Long usos) { this.usos = usos; }

    public Float getImporte() { return importe; }
    public void setImporte(Float importe) { this.importe = importe; }

    public Float getPorcentaje() { return porcentaje; }
    public void setPorcentaje(Float porcentaje) { this.porcentaje = porcentaje; }

    public CampanyaDTO getCampanya() { return campanya; }
    public void setCampanya(CampanyaDTO campanya) { this.campanya = campanya; }

    public Date getFechaIni() { return fechaIni; }
    public void setFechaIni(Date fechaIni) { this.fechaIni = fechaIni; }

    public Date getFechaFin() { return fechaFin; }
    public void setFechaFin(Date fechaFin) { this.fechaFin = fechaFin; }

    public Long getMostrar() { return mostrar; }
    public void setMostrar(Long mostrar) { this.mostrar = mostrar; }

    public String getComentarios() { return comentarios; }
    public void setComentarios(String comentarios) { this.comentarios = comentarios; }
}