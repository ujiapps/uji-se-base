package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_CALEN_CLASES_DIR_INST")
public class CalendarioClaseInstalacionDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CALENDARIO_CLASE_DIRIGIDA_ID")
    private CalendarioClaseDTO calendarioClaseDTO;

    @ManyToOne
    @JoinColumn(name = "INSTALACION_ID")
    private InstalacionDTO instalacionDTO;

    public CalendarioClaseInstalacionDTO() {
    }

    public CalendarioClaseInstalacionDTO(CalendarioClaseDTO calendarioClaseDTO, InstalacionDTO instalacionDTO) {
        this.calendarioClaseDTO = calendarioClaseDTO;
        this.instalacionDTO = instalacionDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalendarioClaseDTO getCalendarioClaseDTO() {
        return calendarioClaseDTO;
    }

    public void setCalendarioClaseDTO(CalendarioClaseDTO calendarioClaseDTO) {
        this.calendarioClaseDTO = calendarioClaseDTO;
    }

    public InstalacionDTO getInstalacionDTO() {
        return instalacionDTO;
    }

    public void setInstalacionDTO(InstalacionDTO instalacionDTO) {
        this.instalacionDTO = instalacionDTO;
    }
}