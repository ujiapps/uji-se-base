package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_DEVOLUCIONES_COVID")
public class DevolucionCovidDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="IBAN")
    private String IBAN;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID")
    private OfertaDTO ofertaDTO;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaDTO personaDTO;


    public DevolucionCovidDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public OfertaDTO getOferta() {
        return ofertaDTO;
    }

    public void setOferta(OfertaDTO ofertaDTO) {
        this.ofertaDTO = ofertaDTO;
    }

    public PersonaDTO getPersona() {
        return personaDTO;
    }

    public void setPersona(PersonaDTO personaDTO) {
        this.personaDTO = personaDTO;
    }
}
