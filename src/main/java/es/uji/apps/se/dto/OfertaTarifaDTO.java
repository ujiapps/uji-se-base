package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_OFERTAS_TARIFAS")
public class OfertaTarifaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private Long importe;

    @Column(name="DIAS_RETRASO_INS")
    private Long diasRetrasoInscripcion;

    @Column(name="DIAS_RETRASO_VALIDA")
    private Long diasRetrasoValidacion;

    @Column(name="IMPORTE_CARNET_DEPORTIVO")
    private Long importeTarjetaDeportiva;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID")
    private OfertaDTO ofertaDTO;

    @ManyToOne
    @JoinColumn(name = "VINCULO_ID")
    private TipoDTO vinculo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getImporte() {
        return importe;
    }

    public void setImporte(Long importe) {
        this.importe = importe;
    }

    public Long getDiasRetrasoInscripcion() {
        return diasRetrasoInscripcion;
    }

    public void setDiasRetrasoInscripcion(Long diasRetrasoInscripcion) {
        this.diasRetrasoInscripcion = diasRetrasoInscripcion;
    }

    public Long getDiasRetrasoValidacion() {
        return diasRetrasoValidacion;
    }

    public void setDiasRetrasoValidacion(Long diasRetrasoValidacion) {
        this.diasRetrasoValidacion = diasRetrasoValidacion;
    }

    public Long getImporteTarjetaDeportiva() {
        return importeTarjetaDeportiva;
    }

    public void setImporteTarjetaDeportiva(Long importeTarjetaDeportiva) {
        this.importeTarjetaDeportiva = importeTarjetaDeportiva;
    }

    public TipoDTO getVinculo() {
        return vinculo;
    }

    public void setVinculo(TipoDTO vinculo) {
        this.vinculo = vinculo;
    }

    public OfertaDTO getOferta() {
        return ofertaDTO;
    }

    public void setOferta(OfertaDTO ofertaDTO) {
        this.ofertaDTO = ofertaDTO;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
}
