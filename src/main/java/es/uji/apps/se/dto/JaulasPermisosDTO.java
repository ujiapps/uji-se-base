package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_JAULAS_PERMISOS")
public class JaulasPermisosDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="VINCULO_ID")
    private Long vinculoId;

    @Transient
    private String vinculoNombre;


    public JaulasPermisosDTO() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getVinculoId() { return vinculoId; }
    public void setVinculoId(Long vinculoId) { this.vinculoId = vinculoId; }

    public String getVinculoNombre() { return vinculoNombre; }
    public void setVinculoNombre(String vinculoNombre) { this.vinculoNombre = vinculoNombre; }
}