package es.uji.apps.se.dto;

import es.uji.commons.rest.ParamUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "SE_CARNETS_BONOS")
public class CarnetBonoDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private CarnetBonoTipoDTO tipo;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_INI")
    private Date fechaInicio;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademico;

    @Column (name = "ESTADO")
    private String estado;

    public CarnetBonoDTO() {}

    public CarnetBonoDTO(Long personaId, Long cursoAcademicoId, CarnetBonoTipoDTO tipoBono) {
        this.setPersonaId(personaId);
        this.setCursoAcademico(cursoAcademicoId);
        this.setTipo(tipoBono);
        this.setFechaInicio(new Date());
        if (ParamUtils.isNotNull(tipoBono.getDias())){
            Calendar fechaFin = Calendar.getInstance();
            fechaFin.setTime(new Date());
            fechaFin.add(Calendar.DAY_OF_YEAR, Math.toIntExact(tipoBono.getDias()));
            this.setFechaFin(fechaFin.getTime());
        }
        this.setEstado("OK");
    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public CarnetBonoTipoDTO getTipo() {return tipo;}
    public void setTipo(CarnetBonoTipoDTO tipo) {this.tipo = tipo;}
    public void setTipo(Long tipo) {
        this.tipo = new CarnetBonoTipoDTO(tipo);
    }

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observable) {this.observaciones = observable;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}

    public Long getCursoAcademico() {return cursoAcademico;}
    public void setCursoAcademico(Long cursoAcademico) {this.cursoAcademico = cursoAcademico;}

    public String estadoTarjetaBono () {
        if (new Date ().after(this.getFechaFin())){
            return "CA";
        }
        if (new Date().before(this.getFechaInicio())){
            return "NA";
        }
        if (new Date().after(this.getFechaBaja())){
            return "BA";
        }
        return "OK";

    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
