package es.uji.apps.se.dto;

import es.uji.commons.rest.annotations.DataTag;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_ACTIVIDADES_OFERTA")
public class OfertaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="GRUPO")
    private String nombre;

    @Column(name="DESCRIPCION")
    private String descripcion;

    @Column(name="PLAZAS")
    private Long plazas;

    @Column(name="PLAZAS_MAX")
    private Long plazasMaximo;

    @Column(name="FECHA_INI_WEB")
    private Date fechaInicioWeb;

    @Column(name="FECHA_FIN_WEB")
    private Date fechaFinWeb;

    @Column(name="FECHA_INI_INS")
    private Date fechaInicioInscripcion;

    @Column(name="FECHA_FIN_INS")
    private Date fechaFinInscripcion;

    @Column(name="FECHA_INI_PREINS")
    private Date fechaInicioPreinscripcion;

    @Column(name="FECHA_FIN_PREINS")
    private Date fechaFinPreinscripcion;

    @Column(name="FECHA_INI_VALIDA")
    private Date fechaInicioValidacion;

    @Column(name="FECHA_FIN_VALIDA")
    private Date fechaFinValidacion;

    @Column(name="FECHA_INI_RENOVACION")
    private Date fechaInicioRenovacion;

    @Column(name="FECHA_FIN_RENOVACION")
    private Date fechaFinRenovacion;

    @Column(name="FECHA_INI_RECIBOS")
    private Date fechaInicioCobroRecibos;

    @Column(name="FECHA_NACIMIENTO_INI")
    private Date fechaInicioNacimiento;

    @Column(name="FECHA_NACIMIENTO_FIN")
    private Date fechaFinNacimiento;

    @Column(name="CCC_IBAN_ABONO")
    private String ibanAbono;

    @Column(name="horario")
    private String horario;

    @Column(name="INSCRIPCION_WEB")
    private Boolean permiteInscripcionWeb;

    @Column(name="MIRAR_SOLAPE")
    private Boolean mirarSolapamientoInstaciones;

    @Column(name="ACEPTA_CREDITOS")
    private Boolean aceptaCreditos;

    @Column(name="ADMITE_PREINSCRIPCION")
    private Boolean premitePreinscripcion;

    @Column(name="CONTABILIZAR_ASISTENCIA")
    private Boolean contabilizarAsistencia;

    @Column(name="DA_EQUIPACION")
    private Boolean daEquipacion;

    @Column(name="FECHA_INI_EQUIPACION")
    private Date fechaInicioEquipacion;

    @Column(name="FECHA_FIN_EQUIPACION")
    private Date fechaFinEquipacion;

    @Column(name="VALOR_POR_DEFECTO_APTO")
    private Boolean valorDefectoApto;

    @Column(name="MOSTRAR_COMENTARIO_ASISTENCIAS")
    private Boolean comentarioAsistencias;

    @ManyToOne
    @JoinColumn(name = "ACTIVIDAD_ID")
    private ActividadDTO actividadDTO;

    @ManyToOne
    @JoinColumn(name = "PERIODO_ID")
    private PeriodoDTO periodoDTO;

    @OneToMany(mappedBy = "oferta")
    private Set<MonitorOfertaDTO> monitoresOferta;

    @OneToMany(mappedBy = "ofertaDTO")
    private Set<OfertaTarifaDTO> ofertaTarifasDTO;

    @OneToMany(mappedBy = "ofertaDTO")
    private Set<CalendarioOfertaDTO> calendariosOferta;

    @OneToMany(mappedBy = "ofertaDTO")
    private Set<InscripcionDTO> inscripciones;

    @OneToMany(mappedBy = "oferta")
    private Set<CompeticionDTO> competiciones;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPlazas() {
        return plazas;
    }

    public void setPlazas(Long plazas) {
        this.plazas = plazas;
    }

    public Long getPlazasMaximo() {
        return plazasMaximo;
    }

    public void setPlazasMaximo(Long plazasMaximo) {
        this.plazasMaximo = plazasMaximo;
    }

    public Date getFechaInicioWeb() {
        return fechaInicioWeb;
    }

    public void setFechaInicioWeb(Date fechaInicioWeb) {
        this.fechaInicioWeb = fechaInicioWeb;
    }

    public Date getFechaFinWeb() {
        return fechaFinWeb;
    }

    public void setFechaFinWeb(Date fechaFinWeb) {
        this.fechaFinWeb = fechaFinWeb;
    }

    public Date getFechaInicioInscripcion() {
        return fechaInicioInscripcion;
    }

    public void setFechaInicioInscripcion(Date fechaInicioInscripcion) {
        this.fechaInicioInscripcion = fechaInicioInscripcion;
    }

    public Date getFechaFinInscripcion() {
        return fechaFinInscripcion;
    }

    public void setFechaFinInscripcion(Date fechaFinInscripcion) {
        this.fechaFinInscripcion = fechaFinInscripcion;
    }

    public Date getFechaInicioPreinscripcion() {
        return fechaInicioPreinscripcion;
    }

    public void setFechaInicioPreinscripcion(Date fechaInicioPreinscripcion) {
        this.fechaInicioPreinscripcion = fechaInicioPreinscripcion;
    }

    public Date getFechaFinPreinscripcion() {
        return fechaFinPreinscripcion;
    }

    public void setFechaFinPreinscripcion(Date fechaFinPreinscripcion) {
        this.fechaFinPreinscripcion = fechaFinPreinscripcion;
    }

    public Date getFechaInicioValidacion() {
        return fechaInicioValidacion;
    }

    public void setFechaInicioValidacion(Date fechaInicioValidacion) {
        this.fechaInicioValidacion = fechaInicioValidacion;
    }

    public Date getFechaFinValidacion() {
        return fechaFinValidacion;
    }

    public void setFechaFinValidacion(Date fechaFinValidacion) {
        this.fechaFinValidacion = fechaFinValidacion;
    }

    public Date getFechaInicioRenovacion() {
        return fechaInicioRenovacion;
    }

    public void setFechaInicioRenovacion(Date fechaInicioRenovacion) {
        this.fechaInicioRenovacion = fechaInicioRenovacion;
    }

    public Date getFechaFinRenovacion() {
        return fechaFinRenovacion;
    }

    public void setFechaFinRenovacion(Date fechaFinRenovacion) {
        this.fechaFinRenovacion = fechaFinRenovacion;
    }

    public Date getFechaInicioCobroRecibos() {
        return fechaInicioCobroRecibos;
    }

    public void setFechaInicioCobroRecibos(Date fechaInicioCobroRecibos) {
        this.fechaInicioCobroRecibos = fechaInicioCobroRecibos;
    }

    public Date getFechaInicioNacimiento() {
        return fechaInicioNacimiento;
    }

    public void setFechaInicioNacimiento(Date fechaInicioNacimiento) {
        this.fechaInicioNacimiento = fechaInicioNacimiento;
    }

    public Date getFechaFinNacimiento() {
        return fechaFinNacimiento;
    }

    public void setFechaFinNacimiento(Date fechaFinNacimiento) {
        this.fechaFinNacimiento = fechaFinNacimiento;
    }

    public String getIbanAbono() {
        return ibanAbono;
    }

    public void setIbanAbono(String ibanAbono) {
        this.ibanAbono = ibanAbono;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Boolean getPermiteInscripcionWeb() {
        return permiteInscripcionWeb;
    }

    public void setPermiteInscripcionWeb(Boolean permiteInscripcionWeb) {
        this.permiteInscripcionWeb = permiteInscripcionWeb;
    }

    public Boolean getMirarSolapamientoInstaciones() {
        return mirarSolapamientoInstaciones;
    }

    public void setMirarSolapamientoInstaciones(Boolean mirarSolapamientoInstaciones) {
        this.mirarSolapamientoInstaciones = mirarSolapamientoInstaciones;
    }

    public Boolean getAceptaCreditos() {
        return aceptaCreditos;
    }

    public void setAceptaCreditos(Boolean aceptaCreditos) {
        this.aceptaCreditos = aceptaCreditos;
    }

    public Boolean getPremitePreinscripcion() {
        return premitePreinscripcion;
    }

    public void setPremitePreinscripcion(Boolean premitePreinscripcion) {
        this.premitePreinscripcion = premitePreinscripcion;
    }

    public Boolean getContabilizarAsistencia() {
        return contabilizarAsistencia;
    }

    public void setContabilizarAsistencia(Boolean contabilizarAsistencia) {
        this.contabilizarAsistencia = contabilizarAsistencia;
    }

    public Boolean getDaEquipacion() {
        return daEquipacion;
    }

    public void setDaEquipacion(Boolean daEquipacion) {
        this.daEquipacion = daEquipacion;
    }

    public Date getFechaInicioEquipacion() {
        return fechaInicioEquipacion;
    }

    public void setFechaInicioEquipacion(Date fechaInicioEquipacion) {
        this.fechaInicioEquipacion = fechaInicioEquipacion;
    }

    public Date getFechaFinEquipacion() {
        return fechaFinEquipacion;
    }

    public void setFechaFinEquipacion(Date fechaFinEquipacion) {
        this.fechaFinEquipacion = fechaFinEquipacion;
    }

    public Boolean getValorDefectoApto() {
        return valorDefectoApto;
    }

    public void setValorDefectoApto(Boolean valorDefectoApto) {
        this.valorDefectoApto = valorDefectoApto;
    }

    public Boolean getComentarioAsistencias() {
        return comentarioAsistencias;
    }

    public void setComentarioAsistencias(Boolean comentarioAsistencias) {
        this.comentarioAsistencias = comentarioAsistencias;
    }

    public ActividadDTO getActividad() {
        return actividadDTO;
    }

    public void setActividad(ActividadDTO actividadDTO) {
        this.actividadDTO = actividadDTO;
    }

    public PeriodoDTO getPeriodo() {
        return periodoDTO;
    }

    public void setPeriodo(PeriodoDTO periodoDTO) {
        this.periodoDTO = periodoDTO;
    }

    public Set<MonitorOfertaDTO> getMonitoresOferta() {
        return monitoresOferta;
    }

    public void setMonitoresOferta(Set<MonitorOfertaDTO> monitoresOferta) {
        this.monitoresOferta = monitoresOferta;
    }

    public Set<OfertaTarifaDTO> getOfertaTarifas() {
        return ofertaTarifasDTO;
    }

    public void setOfertaTarifas(Set<OfertaTarifaDTO> ofertaTarifasDTO) {
        this.ofertaTarifasDTO = ofertaTarifasDTO;
    }

    public Set<CalendarioOfertaDTO> getCalendariosOferta() {
        return calendariosOferta;
    }

    public void setCalendariosOferta(Set<CalendarioOfertaDTO> calendariosOferta) {
        this.calendariosOferta = calendariosOferta;
    }

    public Set<InscripcionDTO> getInscripciones() {
        return inscripciones;
    }

    public void setInscripciones(Set<InscripcionDTO> inscripciones) {
        this.inscripciones = inscripciones;
    }

    public Set<CompeticionDTO> getCompeticiones() {
        return competiciones;
    }

    public void setCompeticiones(Set<CompeticionDTO> competiciones) {
        this.competiciones = competiciones;
    }
}
