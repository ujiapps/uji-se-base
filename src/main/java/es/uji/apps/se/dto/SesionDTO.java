package es.uji.apps.se.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SE_SESIONES")
public class SesionDTO implements Serializable {

    @Id
    @Column(name="ID")
    private String id;

    @Column(name="PERSONA_ID")
    private Long persona;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_CADUCA")
    private Date fechaCaduca;


    public SesionDTO() {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getPersona()
    {
        return persona;
    }

    public void setPersona(Long persona)
    {
        this.persona = persona;
    }

    public Date getFechaCaduca()
    {
        return fechaCaduca;
    }

    public void setFechaCaduca(Date fechaCaduca)
    {
        this.fechaCaduca = fechaCaduca;
    }
}