package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_ACTIVIDADES_TARIFAS")
public class ActividadTarifaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private Float importe;

    @Column(name="DIAS_RETRASO_INS")
    private Long diasRetrasoInscripcion;

    @Column(name="DIAS_RETRASO_VALIDA")
    private Long diasRetrasoValidacion;

    @Column(name="IMPORTE_CARNET_DEPORTIVO")
    private Long importeTarjetaDeportiva;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoDTO tipoDTO;

    @ManyToOne
    @JoinColumn(name = "VINCULO_ID")
    private TipoDTO vinculo;

    @ManyToOne
    @JoinColumn(name = "PERIODO_ID")
    private PeriodoDTO periodoDTO;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @OneToMany(mappedBy = "actividadTarifaDTO")
    private Set<ActividadTarifaDescuentoDTO> actividadTarifaDescuentosDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public Long getDiasRetrasoInscripcion() {
        return diasRetrasoInscripcion;
    }

    public void setDiasRetrasoInscripcion(Long diasRetrasoInscripcion) {
        this.diasRetrasoInscripcion = diasRetrasoInscripcion;
    }

    public Long getDiasRetrasoValidacion() {
        return diasRetrasoValidacion;
    }

    public void setDiasRetrasoValidacion(Long diasRetrasoValidacion) {
        this.diasRetrasoValidacion = diasRetrasoValidacion;
    }

    public Long getImporteTarjetaDeportiva() {
        return importeTarjetaDeportiva;
    }

    public void setImporteTarjetaDeportiva(Long importeTarjetaDeportiva) {
        this.importeTarjetaDeportiva = importeTarjetaDeportiva;
    }

    public TipoDTO getTipo() {
        return tipoDTO;
    }

    public void setTipo(TipoDTO tipoDTO) {
        this.tipoDTO = tipoDTO;
    }

    public TipoDTO getVinculo() {
        return vinculo;
    }

    public void setVinculo(TipoDTO vinculo) {
        this.vinculo = vinculo;
    }

    public PeriodoDTO getPeriodo() {
        return periodoDTO;
    }

    public void setPeriodo(PeriodoDTO periodoDTO) {
        this.periodoDTO = periodoDTO;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Set<ActividadTarifaDescuentoDTO> getActividadTarifaDescuentos() {
        return actividadTarifaDescuentosDTO;
    }

    public void setActividadTarifaDescuentos(Set<ActividadTarifaDescuentoDTO> actividadTarifaDescuentosDTO) {
        this.actividadTarifaDescuentosDTO = actividadTarifaDescuentosDTO;
    }
}
