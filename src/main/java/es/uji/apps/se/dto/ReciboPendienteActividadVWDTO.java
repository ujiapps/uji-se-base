package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_RECIBOS_PENDIENTES_ACT")
public class ReciboPendienteActividadVWDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="PERSONA_ID")
    private Long personaId;

    @Column(name="OFERTA_ID")
    private Long ofertaId;

    @Column(name="NOMBRE")
    private String nombre;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA")
    private Date fecha;

    public ReciboPendienteActividadVWDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getOfertaId() {return ofertaId;}
    public void setOfertaId(Long ofertaId) {this.ofertaId = ofertaId;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public Date getFecha() {return fecha;}
    public void setFecha(Date fecha) {this.fecha = fecha;}
}
