package es.uji.apps.se.dto;

import es.uji.commons.rest.annotations.DataTag;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SE_CLASES_DIRIGIDAS")
public class ClaseDTO
{
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @DataTag
    private String nombre;

    @DataTag
    @Column(name="DESCRIPCION")
    private String descripcion;

    @Column(name="ACTIVA")
    private Boolean activa;

    @DataTag
    @Column(name="PLAZAS")
    private Long plazas;

    @DataTag
    @Column(name="icono")
    private String icono;

    @OneToMany(mappedBy = "claseDTO")
    private Set<CalendarioClaseDTO> calendariosClase;

    @OneToMany(mappedBy = "clase")
    private Set<ClaseDirigidaTipoDTO> claseDirigidaTiposDTO;

    public ClaseDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Boolean getActiva()
    {
        return activa;
    }

    public void setActiva(Boolean activa)
    {
        this.activa = activa;
    }

    public Long getPlazas()
    {
        return (plazas != null) ? plazas : 0;
    }

    public void setPlazas(Long plazas)
    {
        this.plazas = plazas;
    }

    public Boolean isActiva()
    {
        return activa;
    }

    public Set<CalendarioClaseDTO> getCalendariosClase()
    {
        return calendariosClase;
    }

    public void setCalendariosClase(Set<CalendarioClaseDTO> calendariosClase)
    {
        this.calendariosClase = calendariosClase;
    }

    public Set<ClaseDirigidaTipoDTO> getClaseDirigidaTipos() {
        return claseDirigidaTiposDTO;
    }

    public void setClaseDirigidaTipos(Set<ClaseDirigidaTipoDTO> claseDirigidaTiposDTO) {
        this.claseDirigidaTiposDTO = claseDirigidaTiposDTO;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }
}
