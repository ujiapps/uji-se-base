package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_BIC_PERIODOS")
public class BicicletasPeriodosDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "CURSO_ACA")
    private Long cursoAca;
    @Column(name = "FECHA_INI_LISTA_ESPERA")
    private Date fechaINIListaEspera;
    @Column(name = "FECHA_FIN_LISTA_ESPERA")
    private Date fechaFinListaEspera;
    @Column(name = "LISTA_ESPERA_ACTIVA")
    private Long listaEsperaActiva;

    public BicicletasPeriodosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Date getFechaINIListaEspera() {
        return fechaINIListaEspera;
    }

    public void setFechaINIListaEspera(Date fechaINIListaEspera) {
        this.fechaINIListaEspera = fechaINIListaEspera;
    }

    public Date getFechaFinListaEspera() {
        return fechaFinListaEspera;
    }

    public void setFechaFinListaEspera(Date fechaFinListaEspera) {
        this.fechaFinListaEspera = fechaFinListaEspera;
    }

    public Long getListaEsperaActiva() {
        return listaEsperaActiva;
    }

    public void setListaEsperaActiva(Long listaEsperaActiva) {
        this.listaEsperaActiva = listaEsperaActiva;
    }
}
