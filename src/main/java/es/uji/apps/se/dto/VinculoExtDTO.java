package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_VINCULOS_VINEXT")
public class VinculoExtDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "VINCULO_ID")
    private Long vinculoId;

    @Column(name = "VINEXT_ID")
    private Long vinculoExtId;

    public VinculoExtDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getVinculoId() {return vinculoId;}
    public void setVinculoId(Long vinculoId) {this.vinculoId = vinculoId;}

    public Long getVinculoExtId() {return vinculoExtId;}
    public void setVinculoExtId(Long vinculoExtId) {this.vinculoExtId = vinculoExtId;}
}
