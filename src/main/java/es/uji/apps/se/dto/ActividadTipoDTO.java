package es.uji.apps.se.dto;

import javax.persistence.*;

@Entity
@Table(name = "SE_ACTIVIDADES_TIPOS")
public class ActividadTipoDTO {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name="ACTIVIDAD_ID")
    private ActividadDTO actividadDTO;

    @ManyToOne
    @JoinColumn(name="TIPO_ID")
    private TipoDTO tipoDTO;

    public ActividadTipoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ActividadDTO getActividad() {
        return actividadDTO;
    }

    public void setActividad(ActividadDTO actividadDTO) {
        this.actividadDTO = actividadDTO;
    }

    public TipoDTO getTipo() {
        return tipoDTO;
    }

    public void setTipo(TipoDTO tipoDTO) {
        this.tipoDTO = tipoDTO;
    }
}
