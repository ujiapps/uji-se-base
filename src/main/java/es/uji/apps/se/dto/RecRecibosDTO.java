package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "REC_RECIBOS", schema = "UJI_RECIBOS")
public class RecRecibosDTO implements Serializable {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "REC_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "TIPO_RECIBO_ID")
    private Long tipoReciboId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;

    @Column(name = "FECHA_COBRO")
    @Temporal(TemporalType.DATE)
    private Date fechaCobro;

    @Column(name = "TIPO_COBRO_ID")
    private Long tipoCobroId;

    public RecRecibosDTO() { }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getTipoReciboId() {return tipoReciboId;}
    public void setTipoReciboId(Long tipoReciboId) {this.tipoReciboId = tipoReciboId;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Date getFechaCreacion() {return fechaCreacion;}
    public void setFechaCreacion(Date fechaCreacion) {this.fechaCreacion = fechaCreacion;}

    public Date getFechaCobro() {return fechaCobro;}
    public void setFechaCobro(Date fechaCobro) {this.fechaCobro = fechaCobro;}

    public Long getTipoCobroId() {return tipoCobroId;}
    public void setTipoCobroId(Long tipoCobroId) {this.tipoCobroId = tipoCobroId;}
}