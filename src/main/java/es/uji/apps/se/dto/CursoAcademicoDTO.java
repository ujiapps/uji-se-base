package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_CURSOS_ACADEMICOS")
public class CursoAcademicoDTO implements Serializable {
    @Id
    @Column(name="CURSO_ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_INICIO")
    private Date fechaInicio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_FIN")
    private Date fechaFin;

    @Column(name="HORA_INICIO")
    private String horaInicio;

    @Column(name="HORA_FIN")
    private String horaFin;

    @Column(name="HORA_INICIO_E")
    private String horaInicioDiaEspecial;

    @Column(name="HORA_FIN_E")
    private String horaFinDiaEspecial;

    @OneToMany(mappedBy = "cursoAcademicoDTO")
    private Set<PeriodoDTO> periodosDTO;

    @OneToMany(mappedBy = "cursoAcademicoDTO")
    private Set<EliteDTO> elitesCursoAcademico;

    @OneToMany(mappedBy = "cursoAcademicoDTO")
    private Set<DecanoAvisoDTO> decanoAvisosDTO;


    public CursoAcademicoDTO() {
    }

    public CursoAcademicoDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getHoraInicioDiaEspecial() {
        return horaInicioDiaEspecial;
    }

    public void setHoraInicioDiaEspecial(String horaInicioDiaEspecial) {
        this.horaInicioDiaEspecial = horaInicioDiaEspecial;
    }

    public String getHoraFinDiaEspecial() {
        return horaFinDiaEspecial;
    }

    public void setHoraFinDiaEspecial(String horaFinDiaEspecial) {
        this.horaFinDiaEspecial = horaFinDiaEspecial;
    }

    public Set<PeriodoDTO> getPeriodos() {
        return periodosDTO;
    }

    public void setPeriodos(Set<PeriodoDTO> periodosDTO) {
        this.periodosDTO = periodosDTO;
    }

    public Set<EliteDTO> getElitesCursoAcademico() {
        return elitesCursoAcademico;
    }

    public void setElitesCursoAcademico(Set<EliteDTO> elitesCursoAcademico) {
        this.elitesCursoAcademico = elitesCursoAcademico;
    }

    public Set<DecanoAvisoDTO> getDecanoAvisos() {
        return decanoAvisosDTO;
    }

    public void setDecanoAvisos(Set<DecanoAvisoDTO> decanoAvisosDTO) {
        this.decanoAvisosDTO = decanoAvisosDTO;
    }
}