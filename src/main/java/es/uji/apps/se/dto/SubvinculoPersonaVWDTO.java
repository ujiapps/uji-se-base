package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_EXT_SUBVINCULOS_PERSONAS")
public class SubvinculoPersonaVWDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "SUBVINCULO_ID")
    private Long subvinculoId;

    public SubvinculoPersonaVWDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getSubvinculoId() {return subvinculoId;}
    public void setSubvinculoId(Long subvinculoId) {this.subvinculoId = subvinculoId;}
}
