package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_BIC_RESERVAS")
public class BicicletasReservasDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "FECHA")
    private Date fecha;
    @Column(name = "TIPO_ID")
    private Long tipoId;
    @Column(name = "TIPO_RESERVA_ID")
    private Long tipoReservaId;
    @Column(name = "FECHA_PREV_DEVOL")
    private Date fechaPrevDevol;
    @Column(name = "FECHA_INI")
    private Date fechaIni;
    @Column(name = "FECHA_FIN")
    private Date fechaFin;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "BICICLETA_ID")
    private Long bicicletaId;
    @Column(name = "TIPO_USO_ID")
    private Long tipoUsoId;
    @Column(name = "PERSONA_ID")
    private Long perId;

    public BicicletasReservasDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getTipoId() {
        return tipoId;
    }

    public void setTipoId(Long tipoId) {
        this.tipoId = tipoId;
    }

    public Long getTipoReservaId() {
        return tipoReservaId;
    }

    public void setTipoReservaId(Long tipoReservaId) {
        this.tipoReservaId = tipoReservaId;
    }

    public Date getFechaPrevDevol() {
        return fechaPrevDevol;
    }

    public void setFechaPrevDevol(Date fechaPrevDevol) {
        this.fechaPrevDevol = fechaPrevDevol;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getBicicletaId() {
        return bicicletaId;
    }

    public void setBicicletaId(Long bicicletaId) {
        this.bicicletaId = bicicletaId;
    }

    public Long getTipoUsoId() {
        return tipoUsoId;
    }

    public void setTipoUsoId(Long tipoUsoId) {
        this.tipoUsoId = tipoUsoId;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }
}
