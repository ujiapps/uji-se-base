package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_VW_RECIBOS_PEND_ACT_CAP")
public class ReciboPendienteCapitanActividadVWDTO implements Serializable {

    @Id
    @Column(name="INS_ID")
    private Long inscripcionId;

    @Column(name="PERSONA_ID")
    private Long personaId;

    @Column(name="OFERTA_ID")
    private Long ofertaId;

    @Column(name="EQUIPO_ID")
    private Long equipoId;

    @Column(name="EQUIPO_NOMBRE")
    private String equipoNombre;

    @Column(name="ACTIVIDAD_NOMBRE")
    private String actividadNombre;

    public ReciboPendienteCapitanActividadVWDTO() {}

    public Long getInscripcionId() {return inscripcionId;}
    public void setInscripcionId(Long inscripcionId) {this.inscripcionId = inscripcionId;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public Long getOfertaId() {return ofertaId;}
    public void setOfertaId(Long ofertaId) {this.ofertaId = ofertaId;}

    public Long getEquipoId() {return equipoId;}
    public void setEquipoId(Long equipoId) {this.equipoId = equipoId;}

    public String getEquipoNombre() {return equipoNombre;}
    public void setEquipoNombre(String equipoNombre) {this.equipoNombre = equipoNombre;}

    public String getActividadNombre() {return actividadNombre;}
    public void setActividadNombre(String actividadNombre) {this.actividadNombre = actividadNombre;}
}
