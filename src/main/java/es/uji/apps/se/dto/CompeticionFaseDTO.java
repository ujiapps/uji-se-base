package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_COMPETICIONES_FASES")
public class CompeticionFaseDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "COMPETICION_ID")
    private Long competicionId;

    @Column(name = "NUMERO_VUELTAS")
    private Long numeroVueltas;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "MIN_DURACION_PARTIDO")
    private Long minDuracionPartido;

    @Column(name = "NUM_EQUIPOS_PASAN")
    private Long numEquiposPasan;

    @Column(name = "FASE_ID")
    private Long faseId;

    @Column(name = "BLOQUEADA")
    private Long bloqueada;

    @Column(name = "FINALIZADA")
    private Long finalizada;

    @Column(name = "ORDEN")
    private Long orden;

    public CompeticionFaseDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getCompeticionId() {
        return competicionId;
    }

    public void setCompeticionId(Long competicionId) {
        this.competicionId = competicionId;
    }

    public Long getNumeroVueltas() {
        return numeroVueltas;
    }

    public void setNumeroVueltas(Long numeroVueltas) {
        this.numeroVueltas = numeroVueltas;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getMinDuracionPartido() {
        return minDuracionPartido;
    }

    public void setMinDuracionPartido(Long minDuracionPartido) {
        this.minDuracionPartido = minDuracionPartido;
    }

    public Long getNumEquiposPasan() {
        return numEquiposPasan;
    }

    public void setNumEquiposPasan(Long numEquiposPasan) {
        this.numEquiposPasan = numEquiposPasan;
    }

    public Long getFaseId() {
        return faseId;
    }

    public void setFaseId(Long faseId) {
        this.faseId = faseId;
    }

    public Long getBloqueada() {
        return bloqueada;
    }

    public void setBloqueada(Long bloqueada) {
        this.bloqueada = bloqueada;
    }

    public Long getFinalizada() {
        return finalizada;
    }

    public void setFinalizada(Long finalizada) {
        this.finalizada = finalizada;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }
}