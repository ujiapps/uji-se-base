package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_INSCRIP_PARAMETROS")
public class InscripcionParametrosDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "PARAMETRO_ID")
    private Long parametroId;

    @Column(name = "INSCRIPCION_ID")
    private Long inscripcionId;

    @Column(name = "VALOR")
    private String valor;

    public InscripcionParametrosDTO() { }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getParametroId() {return parametroId;}
    public void setParametroId(Long parametroId) {this.parametroId = parametroId;}

    public Long getInscripcionId() {return inscripcionId;}
    public void setInscripcionId(Long inscripcionId) {this.inscripcionId = inscripcionId;}

    public String getValor() {return valor;}
    public void setValor(String valor) {this.valor = valor;}
}