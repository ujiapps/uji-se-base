package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SE_VINCULOS_VINEXT")
public class VinculosVinextDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private long id;
    @Column(name = "VINCULO_ID")
    private long vinculoId;
    @Column(name = "VINEXT_ID")
    private long vinextId;

    public VinculosVinextDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVinculoId() {
        return vinculoId;
    }

    public void setVinculoId(long vinculoId) {
        this.vinculoId = vinculoId;
    }

    public long getVinextId() {
        return vinextId;
    }

    public void setVinextId(long vinextId) {
        this.vinextId = vinextId;
    }
}
