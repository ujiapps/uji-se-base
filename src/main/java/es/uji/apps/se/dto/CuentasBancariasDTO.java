package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "PER_CUENTAS_BANCARIAS")
public class CuentasBancariasDTO implements Serializable {

    @Id
    @Column(name = "PER_ID")
    private Long perId;

    @Id
    @Column(name = "TCT_ID")
    private Long tctId;

    @Column(name = "IBAN")
    private String iban;

    @Column(name = "ENTIDAD")
    private String entidad;

    @Column(name = "SUCURSAL")
    private String sucursal;

    @Column(name = "DIGITOS")
    private String digitos;

    @Column(name = "CUENTA")
    private String cuenta;

    public CuentasBancariasDTO() {
    }

    public CuentasBancariasDTO(Long perId, Long tctId, String iban) {
        this.perId = perId;
        this.tctId = tctId;
        this.iban = iban;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getTctId() {
        return tctId;
    }

    public void setTctId(Long tctId) {
        this.tctId = tctId;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getEntidad() {return entidad;}
    public void setEntidad(String entidad) {this.entidad = entidad;}

    public String getSucursal() {return sucursal;}
    public void setSucursal(String sucursal) {this.sucursal = sucursal;}

    public String getDigitos() {return digitos;}
    public void setDigitos(String digitos) {this.digitos = digitos;}

    public String getCuenta() {return cuenta;}
    public void setCuenta(String cuenta) {this.cuenta = cuenta;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CuentasBancariasDTO that = (CuentasBancariasDTO) o;
        return Objects.equals(perId, that.perId) && Objects.equals(tctId, that.tctId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(perId, tctId);
    }
}
