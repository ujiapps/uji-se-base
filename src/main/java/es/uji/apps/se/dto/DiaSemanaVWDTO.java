package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_VW_DIAS_SEMANA")
public class DiaSemanaVWDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="NOMBRE")
    private String nombre;

    public DiaSemanaVWDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}
}
