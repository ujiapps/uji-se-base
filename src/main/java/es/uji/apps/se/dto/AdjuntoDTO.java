package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_ENVIOS_ADJUNTOS")
public class AdjuntoDTO implements Serializable {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="NOMBRE_FICHERO")
    private String nombre;

    private String referencia;

    @Column(name="TIPO_FICHERO")
    private String tipo;

    @ManyToOne
    @JoinColumn(name = "ENVIO_ID")
    private EnvioDTO envioDTO;

    public AdjuntoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public EnvioDTO getEnvioDTO() {
        return envioDTO;
    }

    public void setEnvioDTO(EnvioDTO envioDTO) {
        this.envioDTO = envioDTO;
    }
}
