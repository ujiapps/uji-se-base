package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_VW_PERSONAS_CORREO")
public class EnvioPersonaCorreoVwDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;
    private String nombre;
    private String identificacion;
    private String mail;
    @Column(name="recibir_correo")
    private Boolean recibirCorreo;
    private Long vinculo;

    public EnvioPersonaCorreoVwDTO() {
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getMail() {
        return mail;
    }

    public Boolean getRecibirCorreo() {
        return recibirCorreo;
    }

    public Boolean isRecibirCorreo() {
        return recibirCorreo;
    }
}
