package es.uji.apps.se.dto;

import es.uji.apps.se.model.EquipacionActividad;

import javax.persistence.*;

@Entity
@Table(name = "SE_EQUIPACIONES_ACTIVIDADES")
public class EquipacionActividadDTO {

    @Id
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    @Column(name="ID")
    private Long id;

    @Column(name="EQUIPACION_ID")
    private Long equipacionId;

    @Column(name="ACTIVIDAD_ID")
    private Long actividadId;

    @Column(name="CURSO_ACA")
    private Long cursoAcademico;

    @Column(name="VEZ")
    private Long vez;

    @Column(name="STOCK_MIN")
    private Long stockMin;

    public EquipacionActividadDTO() {}

    public EquipacionActividadDTO(EquipacionActividad equipacionActividad){
        setId(equipacionActividad.getId());
        setEquipacionId(equipacionActividad.getEquipacionId());
        setActividadId(equipacionActividad.getActividadId());
        setCursoAcademico(equipacionActividad.getCursoAcademico());
        setVez(equipacionActividad.getVez());
        setStockMin(equipacionActividad.getStockMin());
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Long getEquipacionId() { return equipacionId; }
    public void setEquipacionId(Long equipacionId) { this.equipacionId = equipacionId; }

    public Long getActividadId() { return actividadId; }
    public void setActividadId(Long actividadId) { this.actividadId = actividadId; }

    public Long getCursoAcademico() { return cursoAcademico; }
    public void setCursoAcademico(Long cursoAcademico) { this.cursoAcademico = cursoAcademico; }

    public Long getVez() { return vez; }
    public void setVez(Long vez) { this.vez = vez; }

    public Long getStockMin() { return stockMin; }
    public void setStockMin(Long stockMin) { this.stockMin = stockMin; }
}
