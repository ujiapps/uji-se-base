package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_DEPORTISTAS_ELITE_AVISOS")
public class EliteAvisoDTO implements Serializable{

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_ENVIO")
    private Date fechaEnvio;

    @Column(name = "ASIGNATURA_ID")
    private String asignatura;

    @Column(name="CURSO_ACA")
    private Long cursoAcademico;

    @ManyToOne
    @JoinColumn(name = "DEPORTISTA_ELITE_ID")
    private EliteDTO eliteDTO;

    @ManyToOne
    @JoinColumn(name = "PROFESOR_ID")
    private PersonaUjiDTO profesor;

    public EliteAvisoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public EliteDTO getElite() {
        return eliteDTO;
    }

    public void setElite(EliteDTO eliteDTO) {
        this.eliteDTO = eliteDTO;
    }

    public PersonaUjiDTO getProfesor() {
        return profesor;
    }

    public void setProfesor(PersonaUjiDTO profesor) {
        this.profesor = profesor;
    }

    public Long getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }
}
