package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_PERSONAS_NOTIFICACIONES")
public class NotificacionPersonaDTO implements Serializable {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="PERSONA_ID")
    private Long personaId;

    @Column(name="FECHA")
    private Date fecha;

    @Column(name="PARA")
    private String para;

    @Column(name="ASUNTO")
    private String asunto;

    @Column(name="CUERPO_CLOB")
    private String cuerpo;

    @ManyToOne
    @JoinColumn(name = "REMESA_ID")
    private EnvioDTO envioRemesa;

    public NotificacionPersonaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public EnvioDTO getEnvioRemesa() {
        return envioRemesa;
    }

    public void setEnvioRemesa(EnvioDTO envioRemesa) {
        this.envioRemesa = envioRemesa;
    }
}
