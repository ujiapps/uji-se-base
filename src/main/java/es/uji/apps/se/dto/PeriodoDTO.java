package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_PERIODOS")
public class PeriodoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String nombre;

    @Column(name="FECHA_INI")
    private Date fechaInicio;

    @Column(name="FECHA_FIN")
    private Date fechaFin;

    @Column(name="FECHA_INI_WEB")
    private Date fechaInicioWeb;

    @Column(name="FECHA_FIN_WEB")
    private Date fechaFinWeb;

    @Column(name="FECHA_INI_INS")
    private Date fechaInicioInscripcion;

    @Column(name="FECHA_FIN_INS")
    private Date fechaFinInscripcion;

    @Column(name="FECHA_INI_PREINS")
    private Date fechaInicioPreinscripcion;

    @Column(name="FECHA_FIN_PREINS")
    private Date fechaFinPreinscripcion;

    @Column(name="FECHA_INI_VALIDA")
    private Date fechaInicioValidacion;

    @Column(name="FECHA_FIN_VALIDA")
    private Date fechaFinValidacion;

    @Column(name="FECHA_INI_RENOVACION")
    private Date fechaInicioRenovacion;

    @Column(name="FECHA_FIN_RENOVACION")
    private Date fechaFinRenovacion;

    @Column(name="FECHA_INI_RECIBOS")
    private Date fechaInicioCobroRecibos;

    @Column(name="NUM_DIAS_AVISO")
    private Long numeroDiasAviso;

    @ManyToOne
    @JoinColumn(name = "CLASIFICACION_ID")
    private TipoDTO clasificacion;

    @ManyToOne
    @JoinColumn(name="PERIODO_REFERENCIA_ID")
    private PeriodoDTO periodoReferenciaDTO;

    @ManyToOne
    @JoinColumn(name="CURSO_ACA")
    private CursoAcademicoDTO cursoAcademicoDTO;

    @OneToMany(mappedBy = "periodoReferenciaDTO")
    private Set<PeriodoDTO> periodosDTO;

    @OneToMany(mappedBy = "periodoDTO")
    private Set<PeriodoDescuentoDTO> descuentos;

    @OneToMany(mappedBy = "periodoDTO")
    private Set<ActividadTarifaDTO> periodosTarifas;

    @OneToMany(mappedBy = "periodoDTO")
    private Set<OfertaDTO> ofertasPeriodo;

    public PeriodoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaInicioWeb() {
        return fechaInicioWeb;
    }

    public void setFechaInicioWeb(Date fechaInicioWeb) {
        this.fechaInicioWeb = fechaInicioWeb;
    }

    public Date getFechaFinWeb() {
        return fechaFinWeb;
    }

    public void setFechaFinWeb(Date fechaFinWeb) {
        this.fechaFinWeb = fechaFinWeb;
    }

    public Date getFechaInicioInscripcion() {
        return fechaInicioInscripcion;
    }

    public void setFechaInicioInscripcion(Date fechaInicioInscripcion) {
        this.fechaInicioInscripcion = fechaInicioInscripcion;
    }

    public Date getFechaFinInscripcion() {
        return fechaFinInscripcion;
    }

    public void setFechaFinInscripcion(Date fechaFinInscripcion) {
        this.fechaFinInscripcion = fechaFinInscripcion;
    }

    public Date getFechaInicioPreinscripcion() {
        return fechaInicioPreinscripcion;
    }

    public void setFechaInicioPreinscripcion(Date fechaInicioPreinscripcion) {
        this.fechaInicioPreinscripcion = fechaInicioPreinscripcion;
    }

    public Date getFechaFinPreinscripcion() {
        return fechaFinPreinscripcion;
    }

    public void setFechaFinPreinscripcion(Date fechaFinPreinscripcion) {
        this.fechaFinPreinscripcion = fechaFinPreinscripcion;
    }

    public Date getFechaInicioValidacion() {
        return fechaInicioValidacion;
    }

    public void setFechaInicioValidacion(Date fechaInicioValidacion) {
        this.fechaInicioValidacion = fechaInicioValidacion;
    }

    public Date getFechaFinValidacion() {
        return fechaFinValidacion;
    }

    public void setFechaFinValidacion(Date fechaFinValidacion) {
        this.fechaFinValidacion = fechaFinValidacion;
    }

    public Date getFechaInicioRenovacion() {
        return fechaInicioRenovacion;
    }

    public void setFechaInicioRenovacion(Date fechaInicioRenovacion) {
        this.fechaInicioRenovacion = fechaInicioRenovacion;
    }

    public Date getFechaFinRenovacion() {
        return fechaFinRenovacion;
    }

    public void setFechaFinRenovacion(Date fechaFinRenovacion) {
        this.fechaFinRenovacion = fechaFinRenovacion;
    }

    public Date getFechaInicioCobroRecibos() {
        return fechaInicioCobroRecibos;
    }

    public void setFechaInicioCobroRecibos(Date fechaInicioCobroRecibos) {
        this.fechaInicioCobroRecibos = fechaInicioCobroRecibos;
    }

    public Long getNumeroDiasAviso() {
        return numeroDiasAviso;
    }

    public void setNumeroDiasAviso(Long numeroDiasAviso) {
        this.numeroDiasAviso = numeroDiasAviso;
    }

    public TipoDTO getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(TipoDTO clasificacion) {
        this.clasificacion = clasificacion;
    }

    public PeriodoDTO getPeriodoReferencia() {
        return periodoReferenciaDTO;
    }

    public void setPeriodoReferencia(PeriodoDTO periodoReferenciaDTO) {
        this.periodoReferenciaDTO = periodoReferenciaDTO;
    }

    public CursoAcademicoDTO getCursoAcademico() {
        return cursoAcademicoDTO;
    }

    public void setCursoAcademico(CursoAcademicoDTO cursoAcademicoDTO) {
        this.cursoAcademicoDTO = cursoAcademicoDTO;
    }

    public Set<PeriodoDTO> getPeriodos() {
        return periodosDTO;
    }

    public void setPeriodos(Set<PeriodoDTO> periodosDTO) {
        this.periodosDTO = periodosDTO;
    }

    public Set<PeriodoDescuentoDTO> getDescuentos() {
        return descuentos;
    }

    public void setDescuentos(Set<PeriodoDescuentoDTO> descuentos) {
        this.descuentos = descuentos;
    }

    public Set<ActividadTarifaDTO> getPeriodosTarifas() {
        return periodosTarifas;
    }

    public void setPeriodosTarifas(Set<ActividadTarifaDTO> periodosTarifas) {
        this.periodosTarifas = periodosTarifas;
    }

    public Set<OfertaDTO> getOfertasPeriodo() {
        return ofertasPeriodo;
    }

    public void setOfertasPeriodo(Set<OfertaDTO> ofertasPeriodo) {
        this.ofertasPeriodo = ofertasPeriodo;
    }
}