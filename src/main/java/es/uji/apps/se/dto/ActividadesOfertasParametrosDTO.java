package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SE_ACT_OFERTAS_PARAMETROS")
public class ActividadesOfertasParametrosDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID")
    private OfertaDTO oferta;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "RELLENAR_EN")
    private String rellenarEn;

    @Column(name = "ORDEN_LISTADO")
    private Long ordenListado;

    public ActividadesOfertasParametrosDTO() { }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public OfertaDTO getOfertaId() {return oferta;}
    public void setOfertaId(OfertaDTO oferta) {this.oferta = oferta;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public String getRellenarEn() {return rellenarEn;}
    public void setRellenarEn(String rellenarEn) {this.rellenarEn = rellenarEn;}

    public Long getOrdenListado() {return ordenListado;}
    public void setOrdenListado(Long ordenListado) {this.ordenListado = ordenListado;}
}