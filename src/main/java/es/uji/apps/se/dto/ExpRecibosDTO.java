package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(schema = "GRA_EXP", name = "EXP_RECIBOS")
public class ExpRecibosDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "PER_ID")
    private Long perId;
    @Column(name = "MAT_EXP_TIT_ID")
    private Long titId;
    @Column(name = "F_PAGO")
    private Date fechaPago;

    public ExpRecibosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getTitId() {
        return titId;
    }

    public void setTitId(Long titId) {
        this.titId = titId;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }
}
