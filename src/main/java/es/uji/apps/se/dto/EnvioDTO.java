package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name = "SE_PERSONAS_NOT_REMESAS")
public class EnvioDTO implements Serializable {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String nombre;
    private String origen;
    private String cuerpo;

    @Column(name="REFERENCIA_ID")
    private Long referencia;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_ENVIO")
    private Date fechaEnvio;

    private Boolean enviado;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID_CREA")
    private PersonaUjiDTO personaCrea;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID_VALIDA")
    private PersonaUjiDTO personaValida;

    @OneToMany(mappedBy = "envio")
    private Set<EnvioFiltroDTO> envioFiltros;

    @OneToMany(mappedBy = "envioRemesa")
    private Set<NotificacionPersonaDTO> notificacionesPersonaDTO;

    public EnvioDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Boolean getEnviado() {
        return enviado;
    }

    public Boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(Boolean enviado) {
        this.enviado = enviado;
    }

    public PersonaUjiDTO getPersonaCrea() {
        return personaCrea;
    }

    public void setPersonaCrea(PersonaUjiDTO personaCrea) {
        this.personaCrea = personaCrea;
    }

    public PersonaUjiDTO getPersonaValida() {
        return personaValida;
    }

    public void setPersonaValida(PersonaUjiDTO personaValida) {
        this.personaValida = personaValida;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Set<EnvioFiltroDTO> getEnvioFiltros() {
        return envioFiltros;
    }

    public void setEnvioFiltros(Set<EnvioFiltroDTO> envioFiltros) {
        this.envioFiltros = envioFiltros;
    }

    public Set<NotificacionPersonaDTO> getNotificacionesPersonaDTO() {
        return notificacionesPersonaDTO;
    }

    public void setNotificacionesPersonaDTO(Set<NotificacionPersonaDTO> notificacionesPersonaDTO) {
        this.notificacionesPersonaDTO = notificacionesPersonaDTO;
    }
}
