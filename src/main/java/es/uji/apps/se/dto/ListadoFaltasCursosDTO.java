package es.uji.apps.se.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_VW_LISTADO_FALTAS_CURSOS")
public class ListadoFaltasCursosDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "PERSONA_ID")
    private Long perId;
    @Column(name = "CURSO_ID")
    private Long cursoId;
    @Column(name = "DIA")
    private Date dia;
    @Column(name = "HORA_INI")
    private String horaIni;
    @Column(name = "HORA_FIN")
    private String horaFin;
    @Column(name = "ASISTO")
    private String asisto;

    public ListadoFaltasCursosDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Long getCursoId() {
        return cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(String horaIni) {
        this.horaIni = horaIni;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getAsisto() {
        return asisto;
    }

    public void setAsisto(String asisto) {
        this.asisto = asisto;
    }
}
