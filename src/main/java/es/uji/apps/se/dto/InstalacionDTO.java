package es.uji.apps.se.dto;

import es.uji.commons.rest.annotations.DataTag;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SE_INSTALACIONES")
public class InstalacionDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @DataTag
    @Column(name="NOMBRE")
    private String nombre;

    @DataTag
    @Column(name="CODIGO")
    private String codigo;

    @OneToMany(mappedBy = "instalacionDTO")
    private Set<CalendarioClaseInstalacionDTO> calendarioClaseInstalacionesDTO;

    @OneToMany(mappedBy = "instalacionDTO")
    private Set<TarjetaDeportivaReservaDTO> tarjetaDeportivaReservasDTO;

    public InstalacionDTO()
    {
    }

    public InstalacionDTO(Long instalacionId)
    {
        this.id = instalacionId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Set<CalendarioClaseInstalacionDTO> getCalendarioClaseInstalacionesDTO() {
        return calendarioClaseInstalacionesDTO;
    }

    public void setCalendarioClaseInstalacionesDTO(Set<CalendarioClaseInstalacionDTO> calendarioClaseInstalacionesDTO) {
        this.calendarioClaseInstalacionesDTO = calendarioClaseInstalacionesDTO;
    }
}
