package es.uji.apps.se.dto;

import es.uji.apps.se.model.domains.Vinculo;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.annotations.DataTag;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_EXT_PERSONAS")
public class PersonaUjiDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @DataTag
    @Column(name = "MAIL")
    private String mail;

    @DataTag
    @Column(name = "MOVIL")
    private String movil;

    @DataTag
    @Column(name = "TIP_ID")
    private Long tipoIdentificacion;
    @DataTag
    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @DataTag
    @Column(name = "NOMBRE")
    private String nombre;

    @DataTag
    @Column(name = "APELLIDO1")
    private String apellido1;

    @DataTag
    @Column(name = "APELLIDO2")
    private String apellido2;

    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    @Column(name = "SEXO")
    private String sexo;

    @Column(name = "BUSQUEDA")
    private String busqueda;

    @Column(name = "tarjeta_salto")
    private Long tarjetaSalto;

    @Column(name = "DIR_SE_L1")
    private String direccionSeLinea;

    @Column(name = "DIR_SE_CODIGOPOSTAL")
    private String direccionSeCodigoPostal;

    @Column(name = "DIR_SE_PUERTA")
    private String direccionSePuerta;

    @Column(name = "DIR_SE_PISO")
    private String direccionSePiso;

    @Column(name = "DIR_SE_ESCALERA")
    private String direccionSeEscalera;

    @Column(name = "DIR_SE_NUMERO")
    private String direccionSeNumero;

    @Column(name = "DIR_SE_NOMBRE")
    private String direccionSeNombre;

    @Column(name = "DIR_SE_PAIS_ID")
    private String direccionSePais;

    @Column(name = "DIR_SE_VIA_ID")
    private String direccionSeVia;

    @Column(name = "DIR_SE_PROV_ID")
    private String direccionSeProvincia;

    @Column(name = "DIR_SE_PUEBLO_ID")
    private Long direccionSePueblo;

    @Column(name = "TELEFONO")
    private String telefono;

    @Column(name = "DOMICILIO_L2")
    private String domicilioLinea2;

    @Column(name = "DOMICILIO_L1")
    private String domicilioLinea1;

    @Column(name = "LUGAR_NACIMIENTO")
    private String lugarNacimiento;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<PersonaDTO> personasUji;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<AlumnoAsignaturaDTO> alumnoAsignaturasDTO;

    @OneToMany(mappedBy = "profesor")
    private Set<EliteAvisoDTO> profesorAvisos;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<ProfesorEliteDTO> profesoresElite;

    @OneToMany(mappedBy = "admin")
    private Set<MaterialReservaDTO> materialesReserva;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<BonificacionDTO> bonificaciones;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<PersonaTitulacionDTO> titulaciones;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<EliteDTO> elitesDTO;

    @OneToMany(mappedBy = "personaUji")
    private Set<MonitorDTO> personas;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<InscripcionDTO> inscripcionesPersona;

    @OneToMany(mappedBy = "inscritoAdmin")
    private Set<InscripcionDTO> inscripcionesAdmin;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<EnvioGenericoDTO> enviosGenericos;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<SancionVistaDTO> sanciones;

    @OneToOne(mappedBy = "personaUjiDTO")
    private PersonaVinculoDTO personaVinculoDTO;

    @OneToMany(mappedBy = "personaUji")
    private Set<JaulaPersonaDTO> jaulasPersona;

    @OneToMany(mappedBy = "personaUji")
    private Set<TarjetaDeportivaDTO> tarjetasDeportivas;

    @OneToMany(mappedBy = "personaCrea")
    private Set<EnvioDTO> personasCrea;

    @OneToMany(mappedBy = "personaValida")
    private Set<EnvioDTO> personasValida;

    @OneToMany(mappedBy = "personaUjiDTO")
    private Set<PersonaAdminDTO> personasAdminDTO;

    @OneToMany(mappedBy = "persona")
    private Set<FotoDTO> fotosDTO;

    public PersonaUjiDTO() {
    }

    public PersonaUjiDTO(Long personaId) {
        this.id = personaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Long getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Long tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public Long getSexoLong() {
        if (ParamUtils.isNotNull(sexo)) {
            return (sexo.toLowerCase().equals("dona")) ? 2L : 1L;
        } else return null;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getDireccionSeLinea() {
        return direccionSeLinea;
    }

    public void setDireccionSeLinea(String direccionSeLinea) {
        this.direccionSeLinea = direccionSeLinea;
    }

    public String getDireccionSeCodigoPostal() {
        return direccionSeCodigoPostal;
    }

    public void setDireccionSeCodigoPostal(String direccionSeCodigoPostal) {
        this.direccionSeCodigoPostal = direccionSeCodigoPostal;
    }

    public String getDireccionSePuerta() {
        return direccionSePuerta;
    }

    public void setDireccionSePuerta(String direccionSePuerta) {
        this.direccionSePuerta = direccionSePuerta;
    }

    public String getDireccionSePiso() {
        return direccionSePiso;
    }

    public void setDireccionSePiso(String direccionSePiso) {
        this.direccionSePiso = direccionSePiso;
    }

    public String getDireccionSeEscalera() {
        return direccionSeEscalera;
    }

    public void setDireccionSeEscalera(String direccionSeEscalera) {
        this.direccionSeEscalera = direccionSeEscalera;
    }

    public String getDireccionSeNumero() {
        return direccionSeNumero;
    }

    public void setDireccionSeNumero(String direccionSeNumero) {
        this.direccionSeNumero = direccionSeNumero;
    }

    public String getDireccionSeNombre() {
        return direccionSeNombre;
    }

    public void setDireccionSeNombre(String direccionSeNombre) {
        this.direccionSeNombre = direccionSeNombre;
    }

    public String getDireccionSePais() {
        return direccionSePais;
    }

    public void setDireccionSePais(String direccionSePais) {
        this.direccionSePais = direccionSePais;
    }

    public String getDireccionSeVia() {
        return direccionSeVia;
    }

    public void setDireccionSeVia(String direccionSeVia) {
        this.direccionSeVia = direccionSeVia;
    }

    public String getDireccionSeProvincia() {
        return direccionSeProvincia;
    }

    public void setDireccionSeProvincia(String direccionSeProvincia) {
        this.direccionSeProvincia = direccionSeProvincia;
    }

    public Long getDireccionSePueblo() {
        return direccionSePueblo;
    }

    public void setDireccionSePueblo(Long direccionSePueblo) {
        this.direccionSePueblo = direccionSePueblo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDomicilioLinea2() {
        return domicilioLinea2;
    }

    public void setDomicilioLinea2(String domicilioLinea2) {
        this.domicilioLinea2 = domicilioLinea2;
    }

    public String getDomicilioLinea1() {
        return domicilioLinea1;
    }

    public void setDomicilioLinea1(String domicilioLinea1) {
        this.domicilioLinea1 = domicilioLinea1;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public Set<PersonaDTO> getPersonasUji() {
        return personasUji;
    }

    public void setPersonasUji(Set<PersonaDTO> personasUji) {
        this.personasUji = personasUji;
    }

    public Set<AlumnoAsignaturaDTO> getAlumnoAsignaturas() {
        return alumnoAsignaturasDTO;
    }

    public void setAlumnoAsignaturas(Set<AlumnoAsignaturaDTO> alumnoAsignaturasDTO) {
        this.alumnoAsignaturasDTO = alumnoAsignaturasDTO;
    }

    public Set<EliteAvisoDTO> getProfesorAvisos() {
        return profesorAvisos;
    }

    public void setProfesorAvisos(Set<EliteAvisoDTO> profesorAvisos) {
        this.profesorAvisos = profesorAvisos;
    }

    public Set<ProfesorEliteDTO> getProfesoresElite() {
        return profesoresElite;
    }

    public void setProfesoresElite(Set<ProfesorEliteDTO> profesoresElite) {
        this.profesoresElite = profesoresElite;
    }

    public String getApellidosNombre() {
        return this.apellido1 + " " + this.apellido2 + ", " + this.nombre;
    }

    public String getNombreApellidos() {
        return this.nombre + " " + this.apellido1 + " " + this.apellido2;
    }

    public Set<MaterialReservaDTO> getMaterialesReserva() {
        return materialesReserva;
    }

    public void setMaterialesReserva(Set<MaterialReservaDTO> materialesReserva) {
        this.materialesReserva = materialesReserva;
    }

    public Set<BonificacionDTO> getBonificaciones() {
        return bonificaciones;
    }

    public void setBonificaciones(Set<BonificacionDTO> bonificaciones) {
        this.bonificaciones = bonificaciones;
    }

    public Set<PersonaTitulacionDTO> getTitulaciones() {
        return titulaciones;
    }

    public void setTitulaciones(Set<PersonaTitulacionDTO> titulaciones) {
        this.titulaciones = titulaciones;
    }

    public Set<EliteDTO> getElites() {
        return elitesDTO;
    }

    public void setElites(Set<EliteDTO> elitesDTO) {
        this.elitesDTO = elitesDTO;
    }

    public Set<MonitorDTO> getPersonas() {
        return personas;
    }

    public void setPersonas(Set<MonitorDTO> personas) {
        this.personas = personas;
    }

    public Set<InscripcionDTO> getInscripcionesPersona() {
        return inscripcionesPersona;
    }

    public void setInscripcionesPersona(Set<InscripcionDTO> inscripcionesPersona) {
        this.inscripcionesPersona = inscripcionesPersona;
    }

    public Set<InscripcionDTO> getInscripcionesAdmin() {
        return inscripcionesAdmin;
    }

    public void setInscripcionesAdmin(Set<InscripcionDTO> inscripcionesAdmin) {
        this.inscripcionesAdmin = inscripcionesAdmin;
    }

    public Set<EnvioGenericoDTO> getEnviosGenericos() {
        return enviosGenericos;
    }

    public void setEnviosGenericos(Set<EnvioGenericoDTO> enviosGenericos) {
        this.enviosGenericos = enviosGenericos;
    }

    public Set<SancionVistaDTO> getSanciones() {
        return sanciones;
    }

    public void setSanciones(Set<SancionVistaDTO> sanciones) {
        this.sanciones = sanciones;
    }

    public Set<JaulaPersonaDTO> getJaulasPersona() {
        return jaulasPersona;
    }

    public void setJaulasPersona(Set<JaulaPersonaDTO> jaulasPersona) {
        this.jaulasPersona = jaulasPersona;
    }

    public PersonaVinculoDTO getPersonaVinculo() {
        return personaVinculoDTO;
    }

    public void setPersonaVinculo(PersonaVinculoDTO personaVinculoDTO) {
        this.personaVinculoDTO = personaVinculoDTO;
    }

    public Long getTarjetaSalto() {
        return tarjetaSalto;
    }

    public void setTarjetaSalto(Long tarjetaSalto) {
        this.tarjetaSalto = tarjetaSalto;
    }

    public Boolean tieneTarjetaSalto() {
        return (this.tarjetaSalto.equals(0L)) ? Boolean.FALSE : Boolean.TRUE;
    }

    public Set<TarjetaDeportivaDTO> getTarjetasDeportivas() {
        return tarjetasDeportivas;
    }

    public void setTarjetasDeportivas(Set<TarjetaDeportivaDTO> tarjetasDeportivas) {
        this.tarjetasDeportivas = tarjetasDeportivas;
    }

    public Set<PersonaAdminDTO> getPersonasAdminDTO() {
        return personasAdminDTO;
    }

    public void setPersonasAdminDTO(Set<PersonaAdminDTO> personasAdminDTO) {
        this.personasAdminDTO = personasAdminDTO;
    }

    public Set<FotoDTO> getFotosDTO() {
        return fotosDTO;
    }

    public void setFotosDTO(Set<FotoDTO> fotosDTO) {
        this.fotosDTO = fotosDTO;
    }

    public Long getVinculoId() {
        return (ParamUtils.isNotNull(personaVinculoDTO) && ParamUtils.isNotNull(personaVinculoDTO.getVinculo()) && !personaVinculoDTO.getVinculo().getId().equals(-1L)) ? personaVinculoDTO.getVinculo().getId() : PersonaVinculoDTO.SIN_VINCULACION_UJI;
    }

    public SancionVistaDTO getSancionActiva() {
        Date fechaActual = new Date();
        return sanciones.stream()
                .filter(s -> s.getFecha().before(fechaActual) && (!ParamUtils.isNotNull(s.getFechaFin()) || s.getFechaFin().after(fechaActual)) && (s.getTipo().substring(0, 3).equals("ALL") || s.getTipo().substring(0, 3).equals("CLA")))
                .findFirst().orElse(null);
    }

    public boolean isComunidadUniversitaria() {
        Long vinculo = this.getVinculoId();
        return vinculo.equals(Vinculo.PAS.getId()) || vinculo.equals(Vinculo.PDI.getId()) || vinculo.equals(Vinculo.ESTUDIANTE.getId());
    }
}