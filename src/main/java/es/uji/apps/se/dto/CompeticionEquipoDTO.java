package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "SE_COMPETICIONES_EQUIPOS")
public class CompeticionEquipoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    private String nombre;

    @ManyToOne
    @JoinColumn(name="COMPETICION_ID")
    private CompeticionDTO competicion;

    @OneToMany(mappedBy = "competicionEquipo")
    private Set<CompeticionMiembroDTO> competicionMiembros;

    @OneToMany(mappedBy = "equipoCasa")
    private Set<CompeticionPartidoDTO> competicionPartidosCasa;

    @OneToMany(mappedBy = "equipoFuera")
    private Set<CompeticionPartidoDTO> competicionPartidosFuera;

    public CompeticionEquipoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public CompeticionDTO getCompeticion() {
        return competicion;
    }

    public void setCompeticion(CompeticionDTO competicion) {
        this.competicion = competicion;
    }

    public Set<CompeticionMiembroDTO> getCompeticionMiembros() {
        return competicionMiembros;
    }

    public void setCompeticionMiembros(Set<CompeticionMiembroDTO> competicionMiembros) {
        this.competicionMiembros = competicionMiembros;
    }

    public Set<CompeticionPartidoDTO> getCompeticionPartidosCasa() {
        return competicionPartidosCasa;
    }

    public void setCompeticionPartidosCasa(Set<CompeticionPartidoDTO> competicionPartidosCasa) {
        this.competicionPartidosCasa = competicionPartidosCasa;
    }

    public Set<CompeticionPartidoDTO> getCompeticionPartidosFuera() {
        return competicionPartidosFuera;
    }

    public void setCompeticionPartidosFuera(Set<CompeticionPartidoDTO> competicionPartidosFuera) {
        this.competicionPartidosFuera = competicionPartidosFuera;
    }
}