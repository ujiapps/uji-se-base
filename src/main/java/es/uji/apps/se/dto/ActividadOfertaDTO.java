package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_ACTIVIDADES_OFERTA")
public class ActividadOfertaDTO implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "ACTIVIDAD_ID")
    private Long actividadId;

    @Column(name = "PERIODO_ID")
    private Long periodoId;

    @Column(name = "GRUPO")
    private String grupo;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "FECHA_INI_EQUIPACION")
    @Temporal(TemporalType.DATE)
    private Date fechaIniEquipacion;

    @Column(name = "FECHA_FIN_EQUIPACION")
    @Temporal(TemporalType.DATE)
    private Date fechaFinEquipacion;


    public ActividadOfertaDTO() {
    }

    public ActividadOfertaDTO(Long id, Long actividadId, Long periodoId, String grupo, String descripcion, Date fechaIniEquipacion, Date fechaFinEquipacion) {
        this.id = id;
        this.actividadId = actividadId;
        this.periodoId = periodoId;
        this.grupo = grupo;
        this.descripcion = descripcion;
        this.fechaIniEquipacion = fechaIniEquipacion;
        this.fechaFinEquipacion = fechaFinEquipacion;
    }
}
