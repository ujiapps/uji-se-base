package es.uji.apps.se.dto;

import es.uji.commons.rest.annotations.DataTag;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SE_MATERIALES_RESERVAS")
public class MaterialReservaDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="ORIGEN")
    private String origen;

    @Column(name="CANTIDAD")
    private Long cantidad;

    @Column(name="COMENTARIOS")
    private String comentarios;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA")
    private Date fecha;

    @Column(name="VISUALIZAR_AGENDA")
    private Boolean visualizarEnAgenda;

    @Column(name="REFERENCIA_ID")
    private String referencia;

    @Column(name="REFERENCIA2_ID")
    private String referencia2;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUjiDTO;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID_ADMIN")
    private PersonaUjiDTO admin;

    @DataTag
    @ManyToOne
    @JoinColumn(name="MATERIAL_ID")
    private MaterialDTO materialDTO;

    public MaterialReservaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getVisualizarEnAgenda() {
        return visualizarEnAgenda;
    }

    public void setVisualizarEnAgenda(Boolean visualizarEnAgenda) {
        this.visualizarEnAgenda = visualizarEnAgenda;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getReferencia2() {
        return referencia2;
    }

    public void setReferencia2(String referencia2) {
        this.referencia2 = referencia2;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUjiDTO;
    }

    public void setPersonaUji(PersonaUjiDTO personaUjiDTO) {
        this.personaUjiDTO = personaUjiDTO;
    }

    public PersonaUjiDTO getAdmin() {
        return admin;
    }

    public void setAdmin(PersonaUjiDTO admin) {
        this.admin = admin;
    }

    public MaterialDTO getMaterial() {
        return materialDTO;
    }

    public void setMaterial(MaterialDTO materialDTO) {
        this.materialDTO = materialDTO;
    }
}