package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "se_vw_ind_horas_instalacion")
public class HorasPorInstalacionDTO implements Serializable
{
    @Id
    @Column(name = "TIPO_INSTALACION_ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dia;

    @Column(name="HORAS")
    private Float horas;

    @Column(name="TIPO_INSTALACION_NOMBRE")
    private String nombre;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public Float getHoras() {
        return horas;
    }

    public void setHoras(Float horas) {
        this.horas = horas;
    }

    public String getNombre() {
        return nombre;
    }
}