package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "se_vw_ind_accesos_perfil")
public class AccesosPorPerfilDTO implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dia;

    @Column(name="HORA_ENTRADA")
    private String horaEntrada;

    @Column(name="HORA_SALIDA")
    private String horaSalida;

    @Column (name="ZONA_ID")
    private Long zonaId;

    private String zona;

    @Column(name="VINCULO_NOMBRE")
    private String vinculoNombre;

    private Long vinculo;

    public AccesosPorPerfilDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Long getZonaId() {
        return zonaId;
    }

    public void setZonaId(Long zonaId) {
        this.zonaId = zonaId;
    }

    public String getVinculoNombre() {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre) {
        this.vinculoNombre = vinculoNombre;
    }

    public Long getVinculo() {
        return vinculo;
    }

    public void setVinculo(Long vinculo) {
        this.vinculo = vinculo;
    }
}
