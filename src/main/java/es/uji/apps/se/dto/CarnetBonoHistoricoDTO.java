package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "uji_zetadolar.z$_se_carnets_bonos")
public class CarnetBonoHistoricoDTO implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private CarnetBonoTipoDTO tipoHistorico;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_INI")
    private Date fechaInicio;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademico;

    public CarnetBonoHistoricoDTO() {}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Long getPersonaId() {return personaId;}
    public void setPersonaId(Long personaId) {this.personaId = personaId;}

    public CarnetBonoTipoDTO getTipoHistorico() {
        return tipoHistorico;
    }

    public void setTipoHistorico(CarnetBonoTipoDTO tipoHistorico) {
        this.tipoHistorico = tipoHistorico;
    }

    public String getObservaciones() {return observaciones;}
    public void setObservaciones(String observable) {this.observaciones = observable;}

    public Date getFechaInicio() {return fechaInicio;}
    public void setFechaInicio(Date fechaInicio) {this.fechaInicio = fechaInicio;}

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}

    public Date getFechaBaja() {return fechaBaja;}
    public void setFechaBaja(Date fechaBaja) {this.fechaBaja = fechaBaja;}

    public Long getCursoAcademico() {return cursoAcademico;}
    public void setCursoAcademico(Long cursoAcademico) {this.cursoAcademico = cursoAcademico;}

    public String estadoTarjetaBono () {
        if (new Date ().after(this.getFechaFin())){
            return "CA";
        }
        if (new Date().before(this.getFechaInicio())){
            return "NA";
        }
        if (new Date().after(this.getFechaBaja())){
            return "BA";
        }
        return "OK";

    }
}
