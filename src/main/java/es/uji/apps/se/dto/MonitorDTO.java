package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_MONITORES")
public class MonitorDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @Column(name="ANYOS_SE")
    private Long anyosTrabajados;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_ALTA")
    private Date fechaAlta;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_BAJA")
    private Date fechaBaja;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_INI_CLAUER")
    private Date fechaInicioLlavero;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA_FIN_CLAUER")
    private Date fechaFinLlavero;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaUjiDTO personaUji;

    @ManyToOne
    @JoinColumn(name = "TIPO_FORMACION")
    private MonitorTitulacionDTO monitorTitulacion;

    @OneToMany(mappedBy = "cursoAcademicoDTO")
    private Set<MonitorCursoAcademicoDTO> monitoresCursoAcademico;

    public MonitorDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnyosTrabajados() {
        return anyosTrabajados;
    }

    public void setAnyosTrabajados(Long anyosTrabajados) {
        this.anyosTrabajados = anyosTrabajados;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaInicioLlavero() {
        return fechaInicioLlavero;
    }

    public void setFechaInicioLlavero(Date fechaInicioLlavero) {
        this.fechaInicioLlavero = fechaInicioLlavero;
    }

    public Date getFechaFinLlavero() {
        return fechaFinLlavero;
    }

    public void setFechaFinLlavero(Date fechaFinLlavero) {
        this.fechaFinLlavero = fechaFinLlavero;
    }

    public PersonaUjiDTO getPersonaUji() {
        return personaUji;
    }

    public void setPersonaUji(PersonaUjiDTO personaUji) {
        this.personaUji = personaUji;
    }

    public MonitorTitulacionDTO getMonitorTitulacion() {
        return monitorTitulacion;
    }

    public void setMonitorTitulacion(MonitorTitulacionDTO monitorTitulacion) {
        this.monitorTitulacion = monitorTitulacion;
    }

    public Set<MonitorCursoAcademicoDTO> getMonitoresCursoAcademico() {
        return monitoresCursoAcademico;
    }

    public void setMonitoresCursoAcademico(Set<MonitorCursoAcademicoDTO> monitoresCursoAcademico) {
        this.monitoresCursoAcademico = monitoresCursoAcademico;
    }


}