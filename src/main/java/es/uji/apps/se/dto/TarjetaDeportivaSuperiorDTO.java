package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;

@Entity @Table(name = "SE_TARJETAS_DEPORTIVAS_SUP")
public class TarjetaDeportivaSuperiorDTO implements Serializable
{
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TARJETA_DEPORTIVA_TIPO_ID")
    private TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO;

    @ManyToOne
    @JoinColumn(name = "TARJETA_DEPORTIVA_SUPERIOR_ID")
    private TarjetaDeportivaTipoDTO tarjetaDeportivaTipoSuperiorDTO;

    public TarjetaDeportivaSuperiorDTO()
    {
    }

    public TarjetaDeportivaSuperiorDTO(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportivaTipo()
    {
        return tarjetaDeportivaTipoDTO;
    }

    public void setTarjetaDeportivaTipo(TarjetaDeportivaTipoDTO tarjetaDeportivaTipoDTO)
    {
        this.tarjetaDeportivaTipoDTO = tarjetaDeportivaTipoDTO;
    }

    public TarjetaDeportivaTipoDTO getTarjetaDeportivaTipoSuperior() {
        return tarjetaDeportivaTipoSuperiorDTO;
    }

    public void setTarjetaDeportivaTipoSuperior(TarjetaDeportivaTipoDTO tarjetaDeportivaTipoSuperiorDTO) {
        this.tarjetaDeportivaTipoSuperiorDTO = tarjetaDeportivaTipoSuperiorDTO;
    }
}