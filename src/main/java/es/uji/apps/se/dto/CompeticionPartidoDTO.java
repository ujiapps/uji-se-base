package es.uji.apps.se.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "SE_COMPETICIONES_PARTIDOS")
public class CompeticionPartidoDTO implements Serializable {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "SE_IDS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;
    @Column(name="GRUPO_ID")
    private Long grupoId;
    @ManyToOne
    @JoinColumn(name="EQUIPO_CASA_ID")
    private CompeticionEquipoDTO equipoCasa;
    @ManyToOne
    @JoinColumn(name="EQUIPO_FUERA_ID")
    private CompeticionEquipoDTO equipoFuera;
    @Column(name="FECHA")
    private Date fecha;
    @Column(name="RESULTADO_PROVISIONAL_CASA")
    private Long resultadoCasa;
    @Column(name="RESULTADO_PROVISIONAL_FUERA")
    private Long resultadoFuera;

    @Column(name="COMENTARIOS")
    private String comentarios;

    @Column(name="INSTALACION_ID")
    private Long instalacionId;

    @OneToMany(mappedBy = "partido")
    private Set<ActividadAsistenciaDTO> actividadAsistencias;

    public CompeticionPartidoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }

    public CompeticionEquipoDTO getEquipoCasa() {
        return equipoCasa;
    }

    public void setEquipoCasa(CompeticionEquipoDTO equipoCasa) {
        this.equipoCasa = equipoCasa;
    }

    public CompeticionEquipoDTO getEquipoFuera() {
        return equipoFuera;
    }

    public void setEquipoFuera(CompeticionEquipoDTO equipoFuera) {
        this.equipoFuera = equipoFuera;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getResultadoCasa() {
        return resultadoCasa;
    }

    public void setResultadoCasa(Long resultadoCasa) {
        this.resultadoCasa = resultadoCasa;
    }

    public Long getResultadoFuera() {
        return resultadoFuera;
    }

    public void setResultadoFuera(Long resultadoFuera) {
        this.resultadoFuera = resultadoFuera;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Long getInstalacionId() {
        return instalacionId;
    }

    public void setInstalacionId(Long instalacionId) {
        this.instalacionId = instalacionId;
    }


    public Set<ActividadAsistenciaDTO> getActividadAsistencias() {
        return actividadAsistencias;
    }

    public void setActividadAsistencias(Set<ActividadAsistenciaDTO> actividadAsistencias) {
        this.actividadAsistencias = actividadAsistencias;
    }
}