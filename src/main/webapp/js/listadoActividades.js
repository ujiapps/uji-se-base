$(document).ready(function () {
    var _listadoActividades = new ListadoActividades();

});

ListadoActividades = function () {
    var me = this;
    $(document).on('click', '.btn-reserva-clase', this.onClick.bind(this));
}

ListadoActividades.prototype._anularReserva = function (reservaId, boton) {

    let titulo = boton.find('.titulo-evento');

    if (confirm("Estàs segur d'anul·lar la reserva?")) {
        $('#spinner').show();
        $.ajax({
            method: "DELETE",
            url: "/se/rest/app/reservas/" + reservaId,
            success: function () {
                $('#spinner').hide();
                titulo.removeClass('fondo-reservado');
                titulo.addClass('fondo-reservable');
                boton.data('reserva-id', null);
                boton.data('reservable', true);
                boton.data('anulable', false);
            },
            failure: function (response) {
                $('#spinner').hide();
                alert(response.responseJSON.message);
            }
        }).fail(function (response) {
            $('#spinner').hide();
            alert(response.responseJSON.message);
        });
    };
    return false;
}

ListadoActividades.prototype._realizarReserva = function (actividadId, boton) {

    let titulo = boton.find('.titulo-evento');

    $('#spinner').show();
    $.ajax({
        method: "POST",
        dataType: "json",
        contentType: "application/json",
        url: "/se/rest/app/reservas",
        data: JSON.stringify({actividadId: actividadId}),
        success: function (res) {
            $('#spinner').hide();
            titulo.removeClass('fondo-reservable');
            titulo.addClass('fondo-reservado');
            boton.data('reserva-id', res.data.id);
            boton.data('reservable', false);
            boton.data('anulable', true);
        }
    }).fail(function (response) {
        $('#spinner').hide();
        alert(response.responseJSON.message);
    });
    return false;
}

ListadoActividades.prototype.onClick = function (e) {

    let target = $(e.currentTarget);
    let actividadId = target.data('actividad-id'),
        reservaId = target.data("reserva-id"),
        reservable = target.data("reservable"),
        anulable = target.data("anulable"),
        lleno = target.data("lleno");

    if (anulable) {
        return this._anularReserva(reservaId, target);
    }
    if (!reservable) {
        if (lleno) {
            alert("No queden places lliures per a reservar en aquets grup");
            return false;
        } else {
            if (reservaId) {
                alert("No pots anul·lar una reserva passada");
                return false;
            } else {
                alert("No pots reservar l'activitat en aquest moment");
                return false;
            }
        }
    }
    if (reservaId == null) {
        if (confirm("Estàs segur reservar la classe?")) {
            return this._realizarReserva(actividadId, target);
        }
    }
    else{
        return this._anularReserva(reservaId, target);
    }
}
