$(document).ready(function () {
    var _pagos = new Pagos();
});

Pagos = function () {

    var me = this;

    if (screen.width < 1024) {
        $('.titulo-pagos').on('click', function () {
            me.onClickDesplegarContenido(this.getAttribute('data-contenido'), this.getAttribute('data-flecha'))
        });
        $('.contenedor-bonificaciones').hide();
    }

    $('.btn-add-bonificacion').on('click', function () {
        me.onClickAddBonificacion(this.getAttribute('data-bonificacionId'), this.getAttribute('data-bonificacionImporte'),
            this.getAttribute('data-bonificacionPorcentaje'), this.getAttribute('data-importeTarjeta'));
    });

    $('.btn-add-pagos').on('click', function () {
        me.onClickAddMetodoPago(this.getAttribute('data-metodoPago'), this.getAttribute('data-activo'));
    });

    $('.boton-pago').on('click', function () {
        me.onClickAddTarjeta();
    })

    $('.btn-add-cuenta-bancaria').on('click', function () {
        me.visualizarAnyadirCuentaBancaria();
    })

    $('.btn-save-cuenta-bancaria').on('click', this.onClickGuardarCuentaBanco.bind(this));
}

Pagos.prototype.onClickDesplegarContenido = function (contenido, nombreflecha) {

    let contingente = $('.' + contenido),
        flecha = $('.' + nombreflecha);
    if (contingente.is(':visible')) {
        contingente.hide();
        flecha.removeClass("fa-chevron-up");
        flecha.addClass("fa-chevron-down");
    } else {
        contingente.show();
        flecha.removeClass('fa-chevron-down');
        flecha.addClass('fa-chevron-up');
    }
}


Pagos.prototype.onClickAddBonificacion = function (bonificacionId, importeDescuento, porcentajeDescuento, importeTarjeta) {

    let aceptadas = $(".bonificacion-aceptada"),
        bonificacion_aceptada_id = aceptadas.attr("data-bonificacionId");

    aceptadas.removeClass("bonificacion-aceptada");

    if (bonificacion_aceptada_id != bonificacionId) {
        var botonBonificacion = $("[data-bonificacionId = " + bonificacionId + "]");

        botonBonificacion.addClass("bonificacion-aceptada");

        $('.importe-pagos').addClass("importe-aceptada");
        $('.importe-descuento').removeClass('hide');

        if (importeDescuento != null) {
            if (parseFloat(importeDescuento) > parseFloat(importeTarjeta)) {
                $(".importe").text("Preu: 0€");
                $(".descuento").text(" (" + importeTarjeta + "€ de descompte aplicat)");
            } else {
                $(".importe").text((parseFloat(importeTarjeta) - parseFloat(importeDescuento)) + "€");
                $(".descuento").text(" (" + importeDescuento + "€ de descompte aplicat)");
            }

        } else {
            $(".importe").text((porcentajeDescuento * parseFloat(importeTarjeta)) / 100 + "€");
            $(".descuento").text(" (" + porcentajeDescuento + "% de descompte aplicat)");
        }

    } else {
        $('.importe-pagos').removeClass("importe-aceptada");
        $('.importe-descuento').addClass('hide');
    }
};

Pagos.prototype.onClickAddMetodoPago = function (metodoPago, activo) {

    let botonActivado = $("[data-metodoPago = " + metodoPago + "]"),
        metodoPagoElegido = $(".metodoPagoElegido"),
        botones = $('.btn-add-pagos'),
        iconos = botones.find('i');

    iconos.removeClass('fa-check-square');
    iconos.addClass('fa-square');

    metodoPagoElegido.removeClass("metodoPagoElegido");
    $('.boton-pago').attr("disabled", "disabled");
    botones.attr("data-activo", null);

    if (!activo) {
        botonActivado.addClass("metodoPagoElegido");
        botonActivado.attr("data-activo", 1);
        let icono = botonActivado.find('.fa-square');

        icono.addClass('fa-check-square');
        icono.removeClass('fa-square');
        $('.boton-pago').removeAttr("disabled");
    }
};

Pagos.prototype.visualizarAnyadirCuentaBancaria = function () {
    var metodoPagoElegido = $(".metodoPagoElegido");
    metodoPagoElegido.removeClass("metodoPagoElegido");
    $(".boton-pago").attr("disabled", "disabled");
    $('.cuentaBancariaNueva').val("");
    $('.formCuentaBancariaNueva').css("visibility", "visible");
};

Pagos.prototype.onClickAddTarjeta = function () {

    var bonificacion = $(".bonificacion-aceptada").attr('data-bonificacionId'),
        metodoPago = $(".metodoPagoElegido").attr('data-metodopago'),
        tipoTarjetaId = $("#tipoTarjeta").attr('data-tipoTarjeta');

    if (metodoPago) {
        if (confirm("Segur que vols donar d'alta la targeta esportiva?")) {
            $('#app').addClass("loading-mask");
            $.ajax({
                method: "POST",
                dataType: "json",
                url: "/se/rest/app/tarjetas",
                data: {
                    bonificacion: bonificacion,
                    metodoPago: metodoPago,
                    tipoTarjeta: tipoTarjetaId
                }
            }).done(function (response) {
                if (response.success) {
                    $('#app').removeClass("loading-mask");
                    if (metodoPago == "1") {
                        //abrir pestaña nueva de pagos
                        var url = response.data.urlPago;
                        location.href = url;
                    } else {
                        alert("La teua targeta ja està activa");
                        location.href = "/se/rest/app/tarjetas";
                    }
                } else {
                    $('#app').removeClass("loading-mask");
                    alert(response.message);
                }

            }).fail(function (response) {
                $('#app').removeClass("loading-mask");
                alert(response.message);
            });
        }
    }
};

Pagos.prototype.onClickGuardarCuentaBanco = function (f) {
    $.ajax({
        method: "POST",
        url: "/se/rest/app/nuevoiban",
        data: {
            'cuentaBancariaNueva': $('input[class=cuentaBancariaNueva]').val()
        }
    }).done(function (msg) {
        alert("Compte afegit correctament");
        location.reload();
    }).fail(function (response) {
        alert(response.responseJSON.message);
    });
};
