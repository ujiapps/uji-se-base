$(document).ready(function () {
    var _calendarioActividades = new CalendarioActividades();

});

CalendarioActividades = function () {

    var me = this;

    $(document).on('click', '.day-sel', this.onClickCargaDatosDia);
    $(document).on('click', '.day-reservable', this.onClickCargaDatosDia);
}

CalendarioActividades.prototype.onClickCargaDatosDia = function (elem) {
    let dia = elem.currentTarget.getAttribute('data-dia'),
        seleccionados = $(".day-sel"),
        botonDia = $("[data-dia = " + dia + "]");

    seleccionados.removeClass("day-sel");
    seleccionados.addClass("day-reservable");

    botonDia.removeClass("day-reservable");
    botonDia.addClass("day-sel");

    $.ajax({
        type: 'GET',
        url: "/se/rest/app/actividades/" + dia,
        success: function (response) {
            $('.reservas').replaceWith(response);
        }
    });
}
