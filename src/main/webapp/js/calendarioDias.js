$(document).ready(function () {
    var _calendarioDias = new CalendarioDias();

});

CalendarioDias = function () {

    var me = this;

    $(document).on('click', '.mesSiguiente', this.onClickCargaMes);
}

CalendarioDias.prototype.onClickCargaMes = function (elem) {
    let mes = elem.currentTarget.getAttribute('data-mes');
    $('#spinner').show();
    $.ajax({
        type: 'GET',
        url: "/se/rest/app/actividades/calendario/" + mes,
        success: function (response) {
            $('#spinner').hide();
            $('.calendario').replaceWith(response);
            $('.reservas').replaceWith('<div class="reservas"></div>');
        }
    });
}