$(document).ready(function () {
    let _calendarioCalendario = new CalendarioCalendario();
});

CalendarioCalendario = function () {

    var me = this;

    $(document).on('click', '.day-sel', this.onClickCargaDatosDia.bind(this));
    $(document).on('click', '.day-reservable', this.onClickCargaDatosDia.bind(this));
    me.calendarEl = document.getElementById('calendar');

    me._calendar = new FullCalendar.Calendar(me.calendarEl, {
        plugins: ['timeGrid', 'dayGrid'],
        defaultView: 'timeGridDay',
        minTime: '08:00:00',
        maxTime: '22:00:00',
        allDaySlot: false,
        locale: 'ca',
        contentHeight: 'auto',
        displayEventTime: false,
        eventClassNames: 'eventoCalendario',
        header: {
            left: '',
            right: ''
        },

        eventRender: function (info) {
            let icon = info.event.extendedProps.icono,
                title = $(info.el).first('.fc-title-wrap'),
                background = info.event.extendedProps.background;
            if (icon !== undefined) {
                title.prepend("<i class=" + icon + ' icono_peque' + "></i>");
            }
            $(info.el).addClass(background);
        },

        events: function (info, callback) {
            $('#spinner').show();
            $.ajax({
                url: '/se/rest/app/calendario/eventos',
                data: {
                    start: moment(info.start).format('YYYY-MM-DD'),
                    end: moment(info.end).format('YYYY-MM-DD')
                },
                success: function (response) {
                    callback(response.data);
                    $('#spinner').hide();
                },
                error: function (response) {
                    alert(response.responseJSON.message);
                    $('#spinner').hide();
                }
            });
        },
        eventClick: function (info) {
            var c = $("#calendar");
            if (info.event.extendedProps.reservaId !== null) {
                if (confirm("Estàs segur d'anul·lar la reserva?")) {
                    $('#spinner').show();
                    $.ajax({
                        method: "DELETE",
                        url: "/se/rest/app/reservas/" + info.event.extendedProps.reservaId
                    }).done(function (msg) {
                        me._calendar.refetchEvents();
                        // $('#spinner').hide();
                    }).fail(function (response) {
                        alert(response.responseJSON.message);
                        $('#spinner').hide();
                    });
                }
                return;
            }
            if (info.event.extendedProps.reservable == 'false') {
                if (info.event.extendedProps.lleno == 'true') {
                    alert("No queden places lliures per a reservar en aquets grup");
                    return;
                } else {
                    alert("No pots reservar l'activitat en aquest moment");
                    return;
                }
            }

            if (confirm("Estàs segur reservar la classe?")) {
                $('#spinner').show();
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    url: "/se/rest/app/reservas",
                    data: JSON.stringify({actividadId: info.event.id})
                }).done(function (msg) {
                    me._calendar.refetchEvents();
                    // $('#spinner').hide();

                }).fail(function (response) {
                    alert(response.responseJSON.message);
                    $('#spinner').hide();
                });
                return;
            }
        }
    });

    me._calendar.render();
    $("button.fc-timeGridWeek-button").addClass("hide-mobile");
}

CalendarioCalendario.prototype.onClickCargaDatosDia = function (elem) {
    let dia = elem.currentTarget.getAttribute('data-dia'),
        seleccionados = $(".day-sel"),
        botonDia = $("[data-dia = " + dia + "]");

    seleccionados.removeClass("day-sel");
    seleccionados.addClass("day-reservable");

    botonDia.removeClass("day-reservable");
    botonDia.addClass("day-sel");

    this._calendar.gotoDate(dia);
}
