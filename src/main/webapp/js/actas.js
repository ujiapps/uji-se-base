$(document).ready(function () {
    var _actas = new Actas();

    $('#enviarBtn').on('click', function (event) {
        event.preventDefault();

        let observaciones = document.getElementById("observaciones");

        textoActa(observaciones.getAttribute("data-partido"), observaciones.value);

    });
});

Actas = function () {

    var me = this;
    $('.botonAsistencia').on('click', function () {
        me.onClickMarcaAsistencia(this.getAttribute('data-inscripcion'), this.getAttribute('data-partido'), this.classList.contains("sancion"));
    });
}

Actas.prototype.onClickMarcaAsistencia = function (inscripcionId, partidoId, sancion) {
    if (!sancion) {
        let botonAsistencia = $("[data-inscripcion = " + inscripcionId + "]");
        botonAsistencia.toggleClass("asiste");
        $('#spinner').show();
        $.ajax({
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            url: "/se/rest/actacompeticion/asistencia",
            data: JSON.stringify({inscripcionId: inscripcionId, partidoId: partidoId}),
            success: function (res) {
                $('#spinner').hide();
            }
        }).fail(function (response) {
            $('#spinner').hide();
            alert(response.responseJSON.message);
        });
    }
}

function nuevoGol(button) {

    let spanElement = $(button).siblings("span");
    let currentValue = parseInt(spanElement.text());
    let tipo = button.value;

    let newValue = (tipo == "plus") ? currentValue + 1 : ((tipo == "minus" && currentValue > 0) ? currentValue - 1 : currentValue)

    if (newValue != currentValue) {

        spanElement.text(newValue);

        let inscripcionId = button.parentElement.getAttribute("data-miembro");
        let partidoId = button.parentElement.getAttribute("data-partido");
        let equipo = button.parentElement.getAttribute("data-equipo");

        if (inscripcionId)
            actualizarGoles(inscripcionId, partidoId, newValue);
        else
            actualizarGolesFantasma(equipo, partidoId, newValue);

        actualizarGolesTotal(equipo, partidoId, tipo);
    }
}

function actualizaSancion(element) {
    let inscripcionId = element.getAttribute("data-inscripcion-id");
    let partidoId = element.getAttribute("data-partido");
    let sancion = element.value;

    if (sancion == "")
        sancion = null;

    $('#spinner').show();

    $.ajax({
        method: "POST",
        dataType: "text",
        contentType: "application/json",
        url: "/se/rest/actacompeticion/sancion",
        data: JSON.stringify({partidoId: partidoId, inscripcionId: inscripcionId, sancion: sancion}),
        success: function (res) {
            $('#spinner').hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#spinner').hide();
            console.error(xhr.responseJSON.message);
        }
    });
}

function actualizarGolesTotal(equipo, partidoId, tipo) {

    $('#spinner').show();

    $.ajax({
        method: "PUT",
        dataType: "text",
        contentType: "application/json",
        url: "/se/rest/actacompeticion/golesTotales",
        data: JSON.stringify({partidoId: partidoId, equipo: equipo, tipo: tipo}),
        success: function (res) {
            $('#spinner').hide();
            let data = JSON.parse(res);
            let nuevoTotal = data.nuevoTotal;
            let equipoId = data.equipoId;
            let divGoles;

            if (equipoId == 1)
                divGoles = document.getElementById("goles-local");
            else
                divGoles = document.getElementById("goles-visitante");

            divGoles.innerHTML = `<span class="nombre spacing" value="${nuevoTotal != null ? nuevoTotal : '0'}">${nuevoTotal != null ? nuevoTotal : '0'}</span>`
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#spinner').hide();
            console.error(xhr.responseJSON.message);
        }
    });
}


function actualizarGoles(inscripcionId, partidoId, goles) {
    $('#spinner').show();

    $.ajax({
        method: "POST",
        dataType: "text",
        contentType: "application/json",
        url: "/se/rest/actacompeticion/goles",
        data: JSON.stringify({inscripcionId: inscripcionId, partidoId: partidoId, goles: goles}),
        success: function (res) {
            $('#spinner').hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#spinner').hide();
            console.error(xhr.responseJSON.message);
        }
    });
}

function actualizarGolesFantasma(equipo, partidoId, goles) {
    $('#spinner').show();

    $.ajax({
        method: "POST",
        dataType: "text",
        contentType: "application/json",
        url: "/se/rest/actacompeticion/fantasma",
        data: JSON.stringify({equipo: equipo, partidoId: partidoId, goles: goles}),
        success: function (res) {
            $('#spinner').hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#spinner').hide();
            console.error(xhr.responseJSON.message);
        }
    });
}

function textoActa(partId, observaciones) {

    $('#spinner').show();

    $.ajax({
        method: "POST",
        dataType: "text",
        contentType: "application/json",
        url: "/se/rest/actacompeticion/actualizar",
        data: JSON.stringify({partId: partId, observaciones: observaciones}),
        success: function (res) {
            $('#spinner').hide();
            alert("Las observaciones han sido guardadas");
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#spinner').hide();
            console.error(xhr.responseJSON.message);
            alert("Ha habido un error al guardar")
        }
    });
}