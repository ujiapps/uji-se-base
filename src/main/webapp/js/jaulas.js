$(document).ready(function () {
    var _jaulas = new Jaulas();
});

Jaulas = function () {

    var me = this;

    $('.button').on('click', function () {
        me.onClickAddSolicitud();
    })
}

Jaulas.prototype.onClickAddSolicitud = function () {
    $.ajax({
        method: "POST",
        dataType: "json",
        url: "/se/rest/jaula",
    }).done(function (response) {
        if (response.success) {
            location.href = "/se/rest/jaula/";
        }

    });
};
