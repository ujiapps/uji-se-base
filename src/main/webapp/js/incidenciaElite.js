$(document).ready(function () {
    var _incidenciaElite = new IncidenciaElite();
});

/**
 * @constructor
 */
IncidenciaElite = function () {

    var me = this;
    $('#form-incidencia').submit(this.onSubmitForm.bind(this));
    $("#selectTipo").change(this.cambiaTipoFormularioIncidencia.bind(this));


};

IncidenciaElite.prototype.cambiaTipoFormularioIncidencia = function () {
    var ref = this;
    var op = $("#selectTipo option:selected").val();
    $.ajax({
        url: '/se/rest/incidenciaeliteusuario/tipoformulario/' + op,
        type: 'GET',
        success: function (respuesta) {
            $('#formularioTipoIncidencia').replaceWith(respuesta);
            $('#selectCurso').change(ref.cargaAsignaturasUsuario.bind(ref));
            $('#asignatura').change(ref.cargaProfesoresAsignatura.bind(ref));
            $('#guardarIncidencia').removeClass('hide');
        },
        error: function () {
            $('#formularioTipoIncidencia').replaceWith("Error al cargar el formulario, opción no válida");
            $('#guardarIncidencia').toggleClass('hide');
        }
    });
};

IncidenciaElite.prototype.onSubmitForm = function (f) {
    f.preventDefault();
    var me = this, form = $("#form-incidencia");
    form.parent().addClass("loading-mask");
    me._enviaForm();
};

IncidenciaElite.prototype._enviaForm = function () {
    var form = $("#form-incidencia");
    $.ajax({
        url: '/se/rest/incidenciaeliteusuario/',
        type: 'POST',
        data: form.serialize(),
        success: function (respuesta) {
            form.addClass("hide");
            $('#avisoEnvioOk').removeClass("hide");
            $('#guardarIncidencia').addClass('hide');
            form.parent().addClass("loading-mask");

        },
        error: function () {
            form.parent().addClass("loading-mask");
        }
    });
};

IncidenciaElite.prototype.cargaAsignaturasUsuario = function () {

    var me = this;
    var op = $("#selectCurso option:selected").val();

    $("#form-incidencia").parent().addClass("loading-mask");
    $.ajax({
        type: 'GET',
        url: '/se/rest/incidenciaeliteusuario/asignaturascurso/curso/' + op
    }).done(function (value) {
        var options = '<option value=""></option>';
        for (var i = 0; i < value.data.length; i++) {
            options += '<option value="' + value.data[i].asignatura + '">' + value.data[i].asignatura + '</option>';
        }
        $('#asignatura').html(options);

        $("#form-incidencia").parent().removeClass("loading-mask");
    });
};

IncidenciaElite.prototype.cargaProfesoresAsignatura = function () {

    var asignatura = $("#asignatura option:selected").val();
    var curso = $("#selectCurso option:selected").val();
    $("#form-incidencia").parent().addClass("loading-mask");
    $.ajax({
        type: 'GET',
        url: '/se/rest/incidenciaeliteusuario/profesorasignatura/asignatura/' + asignatura + '/curso/' + curso
    }).done(function (value) {
        var options = '<option value=""></option>';
        for (var i = 0; i < value.data.length; i++) {
            options += '<option value="' + value.data[i].id + '">' + value.data[i].apellido1 + ' ' + value.data[i].apellido2 + ', ' + value.data[i].nombre + '</option>';
        }
        $('#profesor').html(options);

        $("#form-incidencia").parent().removeClass("loading-mask");
    });

};

