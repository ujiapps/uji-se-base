/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; Español)
 */
(function($) {
	$.extend($.validator.messages, {
		required: "This field is required.",
		remote: "Please complete this field.",
		email: "Please enter a valid e-mail address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid whole number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid card number.",
		equalTo: "Please enter the same value again.",
		extension: "Please enter a value with a valid extension.",
		maxlength: $.validator.format("Please enter no more than {0} characters."),
		minlength: $.validator.format("Please enter at least {0} characters."),
		rangelength: $.validator.format("Please enter a value between {0} and {1} characters."),
		range: $.validator.format("Please enter a value between {0} and {1}."),
		max: $.validator.format("Please enter a value less than or equal to {0}."),
		min: $.validator.format("Please enter a value greater than or equal to {0}."),
		nifES: "Please enter a valid NIF (tax ID number).",
		nieES: "Please enter a valid NIE (foreigner ID number).",
		cifES: "Please enter a valid CIF (tax ID code)."
	});
}(jQuery));
