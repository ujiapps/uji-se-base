/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: CA (Catalan; català)
 */
(function($) {
	$.extend($.validator.messages, {
		required: "Aquest camp és obligatori.",
		remote: "Per favor, omple aquest camp.",
		email: "Per favor, escriu una adreça de correu vàlida",
		url: "Per favor, escriu una URL vàlida.",
		date: "Per favor, escriu una data vàlida.",
		dateISO: "Per favor, escriu una data (ISO) vàlida.",
		number: "Per favor, escriu un número enter vàlid.",
		digits: "Per favor, escriu només dígits.",
		creditcard: "Per favor, escriu un número de tarjeta vàlid.",
		equalTo: "Per favor, escriu el maateix valor de nou.",
		extension: "Per favor, escriu un valor amb una extensió acceptada.",
		maxlength: $.validator.format("Per favor, no escriguis més de {0} caracters."),
		minlength: $.validator.format("Per favor, no escriguis menys de {0} caracters."),
		rangelength: $.validator.format("Per favor, escriu un valor entre {0} i {1} caracters."),
		range: $.validator.format("Per favor, escriu un valor entre {0} i {1}."),
		max: $.validator.format("Per favor, escriu un valor menor o igual a {0}."),
		min: $.validator.format("Per favor, escriu un valor major o igual a {0}."),
		nifES: "Per favor, escriu un NIF vàlid.",
		nieES: "Per favor, escriu un NIE vàlid.",
		cifES: "Per favor, escriu un CIF vàlid."
	});
}(jQuery));
