--Crea nueva tabla para añadir mas de una instalación a una clase dirigida.
CREATE TABLE "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST"
(	"ID" NUMBER NOT NULL ENABLE,
     "CALENDARIO_CLASE_DIRIGIDA_ID" NUMBER NOT NULL ENABLE,
     "INSTALACION_ID" NUMBER NOT NULL ENABLE,
     CONSTRAINT "SE_CALEN_CLASES_DIR_INST_FK1" FOREIGN KEY ("CALENDARIO_CLASE_DIRIGIDA_ID")
         REFERENCES "UJI_SPORTS"."SE_CALENDARIO_CLASES_DIRIGIDAS" ("ID") ENABLE,
     CONSTRAINT "SE_CALEN_CLASES_DIR_INST_FK2" FOREIGN KEY ("INSTALACION_ID")
         REFERENCES "UJI_SPORTS"."SE_INSTALACIONES" ("ID") ENABLE
);


CREATE UNIQUE INDEX "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST_PK" ON "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST" ("ID");
CREATE UNIQUE INDEX "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST_UN" ON "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST" ("CALENDARIO_CLASE_DIRIGIDA_ID", "INSTALACION_ID");

ALTER TABLE "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST" ADD CONSTRAINT "SE_CALEN_CLASES_DIR_INST_PK" PRIMARY KEY ("ID")
    USING INDEX "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST_PK"  ENABLE;
ALTER TABLE "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST" ADD CONSTRAINT "SE_CALEN_CLASES_DIR_INST_UN" UNIQUE ("CALENDARIO_CLASE_DIRIGIDA_ID", "INSTALACION_ID")
    USING INDEX "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST_UN"  ENABLE;

CREATE INDEX "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST_IN1" ON "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST" ("INSTALACION_ID");
CREATE INDEX "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST_IN2" ON "UJI_SPORTS"."SE_CALEN_CLASES_DIR_INST" ("CALENDARIO_CLASE_DIRIGIDA_ID");

insert into se_calen_clases_dir_inst (id, calendario_clase_dirigida_id, instalacion_id) select se_ids.nextval, id, instalacion_id from se_calendario_clases_dirigidas where instalacion_id is not null;

CREATE OR REPLACE FORCE EDITIONABLE VIEW "UJI_SPORTS"."SE_VW_CALENDARIO_CLASES" ("ID", "DIA", "CLASE", "CLASE_DIRIGIDA_ID", "INSTALACION", "HORA_INICIO", "HORA_FIN", "COMENTARIOS", "PERSONA_ID", "ICONO", "PLAZAS", "OCUPADAS", "RESERVA", "ASISTE", "DIA_HORA") AS
select ccd.id, c.dia, cd.nombre clase, ccd.clase_dirigida_id,
       LISTAGG(i.nombre||'('||i.codigo||')', ' ,' ) instalacion,
       ccd.hora_inicio, ccd.hora_fin, ccd.comentarios, td.persona_id,  cd.icono, cd.plazas, count(distinct tdc.id) ocupadas,
       (select min(tdc2.id) from se_tarjeta_deportiva_clases tdc2 where tdc2.calendario_clase_dirigida_id = ccd.id and tdc2.tarjeta_deportiva_id = td.id) reserva,
       (select min(tdc2.asiste) from se_tarjeta_deportiva_clases tdc2 where tdc2.calendario_clase_dirigida_id = ccd.id and tdc2.tarjeta_deportiva_id = td.id),
       to_date(to_char(c.dia, 'ddmmyy')||ccd.hora_inicio, 'ddmmyyhh24:mi') dia_hora
from se_calendario_clases_dirigidas ccd join se_calendario c on ccd.calendario_id = c.id
                                        join se_clases_dirigidas cd on ccd.clase_dirigida_id = cd.id
                                        left join se_tarjeta_deportiva_clases tdc on tdc.calendario_clase_dirigida_id = ccd.id
                                        left join se_calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = ccd.id
                                        join se_instalaciones i on ccdi.instalacion_id = i.id
                                        join se_clases_dirigidas_tipos cdt on cdt.clase_dirigida_id = cd.id
                                        join se_tipos t on cdt.tipo_id = t.id,
     se_tarjetas_deportivas td join se_tarjetas_deportivas_tipos tdt on td.tarjeta_deportiva_tipo_id = tdt.id and trunc(sysdate) between trunc(td.fecha_alta) and trunc(td.fecha_baja) and tdt.permite_clases_dirigidas = 1
                               join se_tipos t2 on tdt.taxonomia_id = t2.id


where cd.activa = 1
  and t.id = t2.id
group by ccd.id, c.dia, cd.nombre, ccd.clase_dirigida_id, ccd.hora_inicio, ccd.hora_fin, ccd.comentarios, td.persona_id, cd.plazas, cd.icono, td.id
order by 1
;

CREATE OR REPLACE FORCE EDITIONABLE VIEW "UJI_SPORTS"."SE_VW_RESERVAS_POR_INST_CLA" ("RESERVA_INSTALACION_ID", "RESERVA_ID", "INSTALACION_ID", "ORIGEN", "DIA", "HORA_INICIO", "HORA_FIN", "DEPENDENCIA", "MIRAR_SOLAPE", "NOMBRE", "PERSONA_ID", "ASISTENCIA", "COMENTARIOS", "COMENTARIO_AGENDA") AS
select ccd.id,
       ccd.id,
       ccdi.instalacion_id,
       'CLA',
       c.dia,
       ccd.hora_inicio,
       ccd.hora_fin,
       null,
       1,
       cd.nombre,
       null,
       null,
       ccd.comentarios,
       null
from se_calendario_clases_dirigidas ccd join se_calendario c on ccd.calendario_id = c.id
                                        join se_clases_dirigidas cd on ccd.clase_dirigida_id = cd.id
                                        left join se_calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = ccd.id
union all
select ccd.id,
       ccd.id,
       sid.instalacion2_id,
       'CLA',
       c.dia,
       ccd.hora_inicio,
       ccd.hora_fin,
       'D',
       1,
       cd.nombre,
       null,
       null,
       ccd.comentarios,
       null
from se_calendario_clases_dirigidas ccd join se_calendario c on ccd.calendario_id = c.id
                                        join se_clases_dirigidas cd on ccd.clase_dirigida_id = cd.id
                                        join se_calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = ccd.id
                                        join se_inst_dependencias sid on ccdi.instalacion_id = sid.instalacion1_id;


CREATE OR REPLACE FORCE EDITIONABLE VIEW "UJI_SPORTS"."SE_VW_CLASES_DIRIGIDAS_CSV" ("ID", "CLASE", "CLASE_ID", "DIA", "HORA_INICIO", "INSTALACION", "INSTALACION_ID", "PLAZAS", "RESERVAS", "ASISTENCIAS", "APELLIDOS_NOMBRE", "IDENTIFICACION", "MOVIL", "MAIL", "VINCULO_ACTUAL", "SEXO", "FECHA_NACIMIENTO", "TIPO_TARJETA") AS
select cd.id*100000000000+tdc.id id, cd.nombre clase, cd.id clase_id, c.dia, ccd.hora_inicio, i.nombre||' ('||i.codigo||')' instalacion, i.id instalacion_id, cd.plazas, (select count(*) from se_tarjeta_deportiva_clases where calendario_clase_dirigida_id = ccd.id) reservas,
       (select count(*) from se_tarjeta_deportiva_clases where calendario_clase_dirigida_id = ccd.id and asiste = 1) asistencias, p.apellidos_nombre, p.identificacion, nvl(pe.movil, p.movil) movil, nvl(pe.mail, p.mail) mail,
       t.nombre vinculo_actual, p.sexo, p.fecha_nacimiento, tdt.nombre tipo_tarjeta
from se_clases_dirigidas cd join se_calendario_clases_dirigidas ccd on ccd.clase_dirigida_id = cd.id
                            join se_calendario c on ccd.calendario_id = c.id
                            left join se_calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = ccd.id
                            join se_instalaciones i on ccdi.instalacion_id = i.id
                            join se_tarjeta_deportiva_clases tdc on tdc.calendario_clase_dirigida_id = ccd.id
                            join se_tarjetas_deportivas td on td.id = tdc.tarjeta_deportiva_id
                            join se_ext_personas p on td.persona_id = p.id
                            join se_personas pe on pe.persona_id = p.id
                            join se_vmc_personas_vinculos v on v.id = p.id
                            join se_tipos t on t.id = v.vinculo_actual
                            join se_tarjetas_deportivas_tipos tdt on td.tarjeta_deportiva_tipo_id = tdt.id
;

create or replace function            tiene_torno_clase (p_clase_dirigida in number, p_torno in varchar2, p_dia in date) return varchar2 is

    vPertenece     varchar2(1):= 'N';
    vTorno         varchar2(10);

    cursor cTiposIni is
        select it.tipo_id
        from se_calendario_clases_dirigidas ccd join se_calendario c on ccd.calendario_id = c.id
                                                join se_calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = ccd.id
                                                join se_instalaciones_tipos it on it.instalacion_id = ccdi.instalacion_id
        where c.dia = trunc(p_dia)
          and ccd.clase_dirigida_id = p_clase_dirigida;

begin

    for r in cTiposIni loop
            begin

                select CONNECT_BY_ROOT uso
                into vTorno
                from se_tipos
                where id = r.tipo_id
                connect by prior id = tipo_id
                start with uso like 'TORNO_';

            exception
                when others then vTorno := 'X';
            end;

            if vTorno = p_torno then
                vPertenece := 'S';
            end if;
        end loop;

    return  vPertenece;

exception
    when others then return 'N';
end;

CREATE OR REPLACE FORCE EDITIONABLE VIEW "UJI_SPORTS"."SE_VW_EVENTOS_CLASES_DIRIGIDAS" ("EVENTO_ID", "NOMBRE", "INICIO_EVENTO", "FIN_EVENTO", "PLAZAS", "INSTALACION", "OCUPACION") AS
select ccd.id evento_id, cd.nombre, to_date(to_char(c.dia, 'ddmmyy')||ccd.hora_inicio, 'ddmmyyhh24:mi') hora_inicio, to_date(to_char(c.dia, 'ddmmyy')||ccd.hora_fin, 'ddmmyyhh24:mi') hora_fin, cd.plazas,
       decode(i.fichador_multiactividad, 0, i.codigo, 'ZONA'||i.torno_id) instalacion,


       nvl((select count(*)
            from se_tarjeta_deportiva_clases tdc join se_tarjetas_Deportivas td on tdc.tarjeta_deportiva_id = td.id
                                                 join se_calendario_clases_dirigidas ccd2 on ccd2.id = tdc.calendario_clase_dirigida_id
                                                 join se_calendario c2 on ccd2.calendario_id = c2.id
            where ccd2.id = ccd.id), 0) OCUPACION



from se_clases_dirigidas cd join se_calendario_clases_dirigidas ccd on cd.id = ccd.clase_dirigida_id
                            join se_calendario c on ccd.calendario_id = c.id
                            join se_calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = ccd.id
                            join se_instalaciones i on ccdi.instalacion_id = i.id
where trunc(c.dia) between trunc(sysdate) and trunc(sysdate+1);

--En la vista se_vw_personas_filtro cambiar el apartado de tarjetas deportivas que filtra por instalacion
select t.id, t.persona_id, ccdi.instalacion_id, null, c.asiste, f.dia, ca.hora_inicio, ca.hora_fin, 1, null
from se_tarjetas_deportivas t join se_tarjeta_deportiva_clases c on t.id = c.tarjeta_deportiva_id
                              join se_calendario_clases_dirigidas ca on ca.id = c.calendario_clase_dirigida_id
                              join SE_CALEN_CLASES_DIR_INST ccdi on ccdi.calendario_clase_dirigida_id = ca.id
                              join se_calendario f on f.id = ca.calendario_id
where t.fecha_alta <= sysdate and trunc(nvl(t.fecha_baja, sysdate+1)) >= trunc(sysdate);
-- y en la misma vista los monitores:
select -1, m.persona_id, ccdi.instalacion_id, null, decode(mc.tipo_entrada, null, 0, 1), c.dia, cc.hora_inicio, cc.hora_fin, 2, null
from SE_MONITORES_CLASE_CAL mc join se_calendario_clases_dirigidas cc on cc.id = mc.calendario_clase_dirigida_id
                               join se_clases_dirigidas cd on cc.clase_dirigida_id = cd.id
                               join se_Calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = cc.id
                               join se_calendario c on cc.calendario_id = c.id
                               join se_monitores_cursos_aca mca on mc.monitor_curso_aca_id = mca.id
                               join se_monitores m on m.id = mca.monitor_id;

CREATE OR REPLACE FORCE EDITIONABLE VIEW "UJI_SPORTS"."SE_VW_MONITORES_CLASES_CSV" ("ID", "CLASE", "CLASE_ID", "DIA", "HORA_INICIO", "APELLIDOS_NOMBRE", "INSTALACION", "INSTALACION_ID", "TIPO_ENTRADA") AS
select cd.id*100000000000+mcc.id id, cd.nombre clase, cd.id clase_id, c.dia, ccd.hora_inicio, p.apellidos_nombre, i.nombre||' ('||i.codigo||')' instalacion, i.id instalacion_id, mcc.tipo_entrada
from se_clases_dirigidas cd join se_calendario_clases_dirigidas ccd on ccd.clase_dirigida_id = cd.id
                            join se_calendario c on ccd.calendario_id = c.id
                            left join se_calen_clases_dir_inst ccdi on ccdi.calendario_clase_dirigida_id = ccd.id
                            join se_instalaciones i on ccdi.instalacion_id = i.id
                            join se_monitores_clase_cal mcc on mcc.calendario_clase_dirigida_id = ccd.id
                            join se_monitores_cursos_aca mca on mca.id = mcc.monitor_curso_aca_id
                            join se_monitores m on m.id = mca.monitor_id
                            join se_ext_personas p on m.persona_id = p.id
                            join se_personas pe on pe.persona_id = p.id;


--Comprobar tabla UJI_ZETADOLAR después de borrar columna para que no fallen las operaciones en la tabla se_calendario_clases_dirigidas.

