  CREATE OR REPLACE FORCE EDITIONABLE VIEW "UJI_SPORTS"."SE_VW_EQUIPACIONES" ("ID", "ACTIVO", "GRUPO_ID", "MODELO_ID", "GENERO_ID", "EQUIPACION_TIPO_ID", "NOMBRE", "MODELO", "GENERO", "NOMBRE_COMPLETO", "STOCK_MIN", "FECHA_FIN") AS
  select   e."ID",
            CASE
            WHEN trunc(sysdate) BETWEEN trunc(fecha_inicio) AND nvl(fecha_fin, trunc(sysdate+1)) THEN 1
        ELSE 0
    END AS activo,
            e."GRUPO_ID",
            e."MODELO_ID",
            e."GENERO_ID",
            e."EQUIPACION_TIPO_ID",
            et.nombre nombre,
            em.nombre modelo,
            eg.nombre genero,
            et.nombre || ' - ' || em.nombre || ' - ' || eg.nombre,
            e.stock_min,
            e.fecha_fin
     from   se_equipaciones e,
            se_equipaciones_tipos et,
            se_equipaciones_modelos em,
            se_equipaciones_genero eg
    where       e.modelo_id = em.id
            and e.genero_id = eg.id
            and e.equipacion_tipo_id = et.id;